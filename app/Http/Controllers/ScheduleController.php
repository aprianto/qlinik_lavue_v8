<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ScheduleController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $pusher_key = (new Setting)->pusher_key();
        $acl = Session::get('acl');

        return view('schedule.list', compact('id_clinic', 'level', 'lang','api_url','docs_url','id_user','pusher_key','storage','api_token','acl'));
    }

    public function sick($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $pusher_key = (new Setting)->pusher_key();
        $acl = Session::get('acl');
        $name_polyclinic = Session::get('name_polyclinic');
        $id_polyclinic = Session::get('id_polyclinic');
        $name_doctor = Session::get('name_doctor');

        return view('schedule.list-sick', compact('id_clinic', 'level', 'lang','api_url','docs_url','id_user','pusher_key','storage','api_token','acl','name_polyclinic','id_polyclinic','name_doctor'));
    }

    public function healthy($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $pusher_key = (new Setting)->pusher_key();
        $acl = Session::get('acl');
        $name_polyclinic = Session::get('name_polyclinic');
        $id_polyclinic = Session::get('id_polyclinic');
        $name_doctor = Session::get('name_doctor');

        return view('schedule.list-healthy', compact('id_clinic', 'level', 'lang','api_url','docs_url','id_user','pusher_key','storage','api_token','acl','name_polyclinic','id_polyclinic','name_doctor'));
    }

    public function create($lang, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $id_patient = $request->input('patient');
        $storage = (new Setting)->storage();

        return view('schedule.create', compact('id_clinic','id_patient','lang','api_url','id_user','storage','api_token'));
    }

    public function edit($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('schedule.edit', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $level = Session::get('level');
        $acl = Session::get('acl');

        return view('schedule.detail', compact('id_clinic','level','lang','id','api_url','docs_url','id_user','storage','api_token','acl'));
    }

}
