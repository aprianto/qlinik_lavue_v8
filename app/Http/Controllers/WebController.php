<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App;
use Redirect;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class WebController extends Controller
{
    public function language($language) {
        Session::put('language', $language);
        $url = Session::get('url');

        $splitUrl = explode(url('/').'/', $url);
        $getLang = explode('/', $splitUrl[1]);

        if(count($getLang)==2) {
            $url = str_replace("/".$getLang[0]."/","/".$language."/",$url);
        } else {
            $url = str_replace("/".$getLang[0]."","/".$language."",$url);
        }

        return redirect($url);
    }

    public function lang($lang) {
        App::setLocale($lang);
    }

    public function url() {
        $url = url()->full();

        Session::put('url', $url);
    }

    public function index() {
        if(!Session::has('language')) {
            $lang = "id";
            Session::put('language', $lang);
        } else {
            $lang = Session::get('language');
        }

        return redirect(url('/').'/'.$lang);
    }

    public function search($lang, Request $request) {
        $this->lang($lang);
        $this->url();

        $api_token = (new Setting)->api_token();
        $api_url = (new Setting)->api_url();
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();
        $doctor_url = (new Setting)->doctor_url();

        $get_q = $request->input('q');
        $get_type = $request->input('type');
        $get_polyclinic = $request->input('polyclinic');
        $get_province = $request->input('province');
        $get_regency = $request->input('regency');
        $get_page = $request->input('page');

        $client = new Client();
        $polyclinic = json_decode($client->get($api_url.'/'.$lang.'/polyclinic', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $province = json_decode($client->get($api_url.'/'.$lang.'/province', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $regency = json_decode($client->get($api_url.'/'.$lang.'/regency', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('web.search', compact('lang','api_token','api_url','map_key','storage','polyclinic','province','regency','get_q','get_type','get_polyclinic','get_province','get_regency','get_page','app_url','doctor_url'));
    }

    public function clinic($lang, $id, Request $request) {
        $this->lang($lang);
    	$this->url();

        $api_token = (new Setting)->api_token();
        $api_url = (new Setting)->api_url();
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();
        $doctor_url = (new Setting)->doctor_url();

        $id_polyclinic = $request->input('polyclinic');

        $client = new Client();
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $get_doctor = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$clinic->id.'/doctor', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $polyclinic = "";
        $i=0;
        foreach ($clinic->polyclinic as $key => $value) {
            $polyclinic = $polyclinic."".$value->name;

            if($i!=count($clinic->polyclinic)-1) {
                $polyclinic = $polyclinic.", ";
            }
            $i++;
        }

        $doctor = "";
        $i=0;
        foreach ($get_doctor->data as $key => $value) {
            $doctor = $doctor."".$value->name;
            
            if($i!=count($get_doctor->data)-1) {
                $doctor = $doctor.", ";
            }
            $i++;
        }

        $title = $clinic->name;
        $description = $clinic->address." ".$clinic->regency->name.", ".$clinic->phone.", ".$polyclinic.", ".$doctor;
        $keywords = $clinic->address." ".$clinic->regency->name.", ".$clinic->phone.", ".$polyclinic.", ".$doctor;

		return view('web.clinic', compact('lang','api_token','api_url','map_key','storage','clinic','id','id_polyclinic','title','description','keywords','app_url','doctor_url'));
    }

    public function doctor($lang, $id, Request $request) {
        $this->lang($lang);
        $this->url();

        $api_token = (new Setting)->api_token();
        $api_url = (new Setting)->api_url();
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();
        $doctor_url = (new Setting)->doctor_url();

        $id_polyclinic = $request->input('polyclinic');

        $client = new Client();
        $doctor = json_decode($client->get($api_url.'/'.$lang.'/doctor/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        
        $get_clinic = json_decode($client->get($api_url.'/'.$lang.'/doctor/'.$id.'/clinic', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $polyclinic = "";
        $i=0;
        foreach ($doctor->polyclinic as $key => $value) {
            $polyclinic = $polyclinic."".$value->name;

            if($i!=count($doctor->polyclinic)-1) {
                $polyclinic = $polyclinic.", ";
            }
            $i++;
        }

        $clinic = "";
        $i=0;
        foreach ($get_clinic->data as $key => $value) {
            $clinic = $clinic."".$value->name;
            
            if($i!=count($get_clinic->data)-1) {
                $clinic = $clinic.", ";
            }
            $i++;
        }

        $title = $doctor->name;
        $description = $doctor->address." ".$doctor->regency->name.", ".$doctor->phone.", ".$polyclinic.", ".$clinic;
        $keywords = $doctor->address." ".$doctor->regency->name.", ".$doctor->phone.", ".$polyclinic.", ".$clinic;

        return view('web.doctor', compact('lang','api_token','api_url','map_key','storage','doctor','id','id_polyclinic','title','description','keywords','app_url','doctor_url'));
    }

    public function schedule($lang, $id_clinic, $id_doctor, $id_polyclinic, Request $request) {
        $this->lang($lang);
        $this->url();

        $api_token = (new Setting)->api_token();
        $api_url = (new Setting)->api_url();
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();
        $doctor_url = (new Setting)->doctor_url();

        $client = new Client();
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$id_clinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $doctor = json_decode($client->get($api_url.'/'.$lang.'/doctor/'.$id_doctor, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $polyclinic = json_decode($client->get($api_url.'/'.$lang.'/polyclinic/'.$id_polyclinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('web.schedule', compact('lang','api_token','api_url','map_key','storage','clinic','doctor','polyclinic','app_url','doctor_url'));
    }

    public function report_schedule($lang, $id, Request $request) {
        $this->lang($lang);
        $this->url();

        $api_token = (new Setting)->api_token();
        $api_url = (new Setting)->api_url();
        $docs_url = (new Setting)->docs_url();
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();
        $doctor_url = (new Setting)->doctor_url();

        $client = new Client();
        $schedule = json_decode($client->get($api_url.'/'.$lang.'/schedule/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('web.report.schedule', compact('lang','api_token','api_url','docs_url','map_key', 'storage', 'schedule', 'id','app_url','doctor_url'));
    }

    public function error() {
        $storage = (new Setting)->storage();

        return view('web.error', compact('storage'));
    }
}
