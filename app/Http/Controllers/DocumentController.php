<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;
use App\Functions;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use PDF;

class DocumentController extends Controller
{
    public function patient($lang, $type, $id_clinic, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();

        $client = new Client();
        $patient = json_decode($client->get($api_url.'/'.$lang.'/patient/'.$id.'/clinic/'.$id_clinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.patient', compact('patient','lang','storage'));
            return $pdf->download('patient-'.$patient->mrn.'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.patient', compact('patient','lang','storage'));
            return $pdf->stream('patient-'.$patient->mrn.'.pdf');
        } else {
            return view('docs.patient', compact('patient','lang','storage'));
        }
    }

    public function schedule($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();

        $client = new Client();
        $schedule = json_decode($client->get($api_url.'/'.$lang.'/schedule/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.schedule', compact('schedule','lang','storage'));
            return $pdf->download('schedule-'.(new Functions)->formatDate($schedule->date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.schedule', compact('schedule','lang','storage'));
            return $pdf->stream('schedule-'.(new Functions)->formatDate($schedule->date).'.pdf');
        } else {
            return view('docs.schedule', compact('schedule','lang','storage'));
        }
    }

    public function medical_records($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();
        $download_url = (new Setting)->download_url();

        $client = new Client();
        $medical_records = json_decode($client->get($api_url.'/'.$lang.'/medical-records/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.medical_records', compact('medical_records','lang','storage','download_url'));
            return $pdf->download('medical_records-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.medical_records', compact('medical_records','lang','storage','download_url'));
            return $pdf->stream('medical_records-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } else {
            return view('docs.medical_records', compact('medical_records','lang','storage','download_url'));
        }
    }

    public function rujuk_lanjut($lang, $type, $id, $download, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();
        $download_url = (new Setting)->download_url();

        $client = new Client();
        $data = json_decode($client->get($api_url.'/'.$lang.'/bpjs/rujukan/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($download=="reffer") {
            $pdf = PDF::loadView('docs.letter-refer', compact('data','lang','storage','download_url'))->setPaper('A4', 'potrait');
            // return $pdf->stream('rujuk_lanjut-'.$data->data->noRujukan.'.pdf');
            $myFile = $pdf->output(); //change xlsx for the format you want, default is xls

            $response =  array(
                'name' => $data->data->noRujukan, //no extention needed
                'file' => "data:application/pdf;base64,".base64_encode($myFile) //mime type of used format
            );

            return response()->json($response);
        } else {
            $pdf = PDF::loadView('docs.letter', compact('data','lang','storage','download_url'))->setPaper('A4', 'potrait');
            // return $pdf->stream('rujuk_lanjut-'.$data->data->noRujukan.'.pdf');
            $myFile = $pdf->output(); //change xlsx for the format you want, default is xls

            $response =  array(
                'name' => $data->data->noRujukan, //no extention needed
                'file' => "data:application/pdf;base64,".base64_encode($myFile) //mime type of used format
            );

            return response()->json($response);
        }
    }

    public function medical_records_medicine($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();
        $download_url = (new Setting)->download_url();
        $docs_url = (new Setting)->docs_url();

        $client = new Client();
        $medical_records = json_decode($client->get($api_url.'/'.$lang.'/medical-records/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.medical_records_medicine', compact('medical_records','lang','storage','download_url', 'docs_url'));
            return $pdf->download('medical_records_medicine-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.medical_records_medicine', compact('medical_records','lang','storage','download_url', 'docs_url'));
            return $pdf->stream('medical_records_medicine-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } else {
            return view('docs.medical_records_medicine', compact('medical_records','lang','storage','download_url', 'docs_url'));
        }
    }

    public function medical_records_medicine_recipe($lang, $type, $id, $id_medicine) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();
        $download_url = (new Setting)->download_url();
        $client = new Client();
        $medical_records = json_decode($client->get($api_url.'/'.$lang.'/medical-records/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        foreach($medical_records->prescriptions as $value ) {
            if($value->signa->id == $id_medicine) {
                $medicine_recipe = $value;
            }
        }

        if($type=="download") {
            $pdf = PDF::loadView('docs.medical_records_medicine_recipe', compact('medicine_recipe','lang','storage','download_url', 'medical_records'));
            return $pdf->download('medical_records_medicine_recipe-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.medical_records_medicine_recipe', compact('medicine_recipe','lang','storage','download_url', 'medical_records'));
            return $pdf->stream('medical_records_medicine_recipe-'.(new Functions)->formatDate($medical_records->date).'.pdf');
        } else {
            return view('docs.medical_records_medicine_recipe', compact('medicine_recipe','lang','storage','download_url', 'medical_records'));
        }
    }

    public function invoice($lang, $type, $number, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();
        $docs_url = (new Setting)->docs_url();

        $client = new Client();
        if($type=="download") {
            $invoice = json_decode($client->get($api_url.'/'.$lang.'/invoice/number/'.$number.'/public')->getBody()->getContents());
        }else{
            $invoice = json_decode($client->get($api_url.'/'.$lang.'/invoice/number/'.$number, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        }

        $mobile = $request->input('mobile');

        if(isset($mobile)) {
            $view = 'docs.mobile.invoice';
        } else {
            $view = 'docs.invoice';
        }

        if($type=="download") {
            $pdf = PDF::loadView($view, compact('invoice','lang','storage','docs_url'));
            return $pdf->download('invoice-'.$invoice->number.'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView($view, compact('invoice','lang','storage','docs_url'));
            return $pdf->stream('invoice-'.$invoice->number.'.pdf');
        } else {
            return view($view, compact('invoice','lang','storage','docs_url'));
        }

        
    }

    public function clinic_income($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $income = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/income?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_income', compact('clinic','income','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-income-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_income', compact('clinic','income','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-income-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_income', compact('clinic','income','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_income_detail($lang, $type, $id, $no, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $income = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/income?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $income_detail = [];
        foreach ($income->data as $key => $value) {
            if($value->id==$no) {
                $income_detail=$value;
                break;
            }
        }
        

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_income_detail', compact('clinic','income','income_detail','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-income-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_income_detail', compact('clinic','income','income_detail','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-income-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_income_detail', compact('clinic','income','income_detail','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_commission($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $commission = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/commission?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_commission', compact('clinic','commission','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-commission-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_commission', compact('clinic','commission','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-commission-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_commission', compact('clinic','commission','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_commission_detail($lang, $type, $id, $id_doctor, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $commission = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/commission?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $no = 0;
        $commission_detail = [];
        foreach ($commission as $key => $value) {
            $no++;
            if($value->id_doctor==$id_doctor) {
                $commission_detail=$value;
                break;
            }
        }

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_commission', compact('clinic','commission','commission_detail','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-commission-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_commission', compact('clinic','commission','commission_detail','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-commission-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_commission', compact('clinic','commission','commission_detail','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_service($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $service = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/service?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_service', compact('clinic','service','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-service-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_service', compact('clinic','service','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-service-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_service', compact('clinic','service','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_service_detail($lang, $type, $id, $id_service, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $service = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/service?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        $no = 0;
        $service_detail = [];
        foreach ($service as $key => $value) {
            $no++;
            if($value->id_service==$id_service) {
                $service_detail=$value;
                break;
            }
        }

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_service', compact('clinic','service','service_detail','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-service-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_service', compact('clinic','service','service_detail','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-service-detail-'.$no.'-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_service', compact('clinic','service','service_detail','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_medicine($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $medicine = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/medicine?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_medicine', compact('clinic','medicine','from_date','to_date','lang','storage'));
            return $pdf->download('clinic-medicine-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_medicine', compact('clinic','medicine','from_date','to_date','lang','storage'));
            return $pdf->stream('clinic-medicine-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_medicine', compact('clinic','medicine','from_date','to_date','lang','storage'));
        }
    }

    public function clinic_medicine_detail($lang, $type, $id_medicine, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $clinic = Session::get('clinic');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $medicine = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/medicine?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        foreach ($medicine as $value) {
            if($value->id_medicine == $id_medicine) {
                $medicine_detail = $value;
            }
        }

        if($type=="download") {
            $pdf = PDF::loadView('docs.clinic_medicine_detail', compact('clinic','medicine','from_date','to_date','lang','storage', 'medicine_detail'));
            return $pdf->download('clinic-medicine_detail-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.clinic_medicine_detail', compact('clinic','medicine','from_date','to_date','lang','storage', 'medicine_detail'));
            return $pdf->stream('clinic-medicine_detail-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.clinic_medicine_detail', compact('clinic', 'from_date', 'to_date', 'lang', 'storage', 'medicine_detail'));
        }
    }

    public function doctor_invoice($lang, $type, $id, Request $request) {
        (new BaseController)->lang($lang);

        $storage = (new Setting)->storage();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');

        $id_doctor = $id;
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

        $client = new Client();
        $invoice = json_decode($client->get($api_url.'/'.$lang.'/report/clinic/doctor-invoice/'.$id_doctor.'?from_date='.$from_date.'&to_date='.$to_date, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        $invoice = $invoice->data;

        $doctor = json_decode($client->get($api_url.'/'.$lang.'/doctor/'.$id_doctor, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        if($type=="download") {
            $pdf = PDF::loadView('docs.doctor_invoice', compact('doctor','invoice','from_date','to_date','lang','storage'));
            return $pdf->download('doctor-invoice-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } elseif($type=="pdf") {
            $pdf = PDF::loadView('docs.doctor_invoice', compact('doctor','invoice','from_date','to_date','lang','storage'));
            return $pdf->stream('doctor-invoice-'.(new Functions)->formatDate($from_date).'-'.(new Functions)->formatDate($to_date).'.pdf');
        } else {
            return view('docs.doctor_invoice', compact('doctor','invoice','from_date','to_date','lang','storage'));
        }
    }

    public function exportNoDate($lang, $type, $id_clinic, $key, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();

        if($type=="download") {
            $client = new Client();
            $res = json_decode($client->get($api_url.'/'.$lang.'/export-spreadsheet', [
                'headers' => ['Authorization' => "Bearer ".$api_token],
                'query' => [
                'key' => $key,
                'type' => $request->query('type'),
                'startDate' => $request->query('startDate'),
                'endDate' => $request->query('endDate')
                ]
            ])->getBody()->getContents());

            return $storage.'/'.$res->resource;
        } else {
            return view('docs.export_no_date', compact('lang','storage'));
        }
    }

    public function importPatient($lang, $type, $id_clinic) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = (new Setting)->api_token();
        $storage = (new Setting)->storage();

        if ($type == "download") {

        } else {
            return view('docs.import', compact('lang', 'storage'));
        }
    }
}
