<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;
use Redirect;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

//use App\User;

class UserController extends Controller
{

    public function __construct() {
    }

    public function profile($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $acl = Session::get('acl');

        $client = new Client();
        if($level == 'doctor') {
            $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $clinic = Session::get('clinic');
        }else {
            $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        }
        return view('user.profile.index', compact('user','id_user', 'lang' ,'api_url','storage','api_token','acl', 'level', 'clinic'));

    }

    public function login($lang, Request $request) {
        (new BaseController)->lang($lang);

        $method = $request->method();
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $doctor_url = (new Setting)->doctor_url();
        echo $api_token; 
        if ($request->isMethod('post')) {
            $input = $request->all(); 

            $client = new Client();
            $result = $client->post($api_url.'/'.$lang.'/user/login', [
                'headers' => [
                    'Authorization' => "Bearer ".$api_token,
                ],
                'form_params' => [
                    'email' => $input['email'],
                    'password' => $input['password'],
                ]
            ]);

            $response = json_decode($result->getBody()->getContents());

            if($response->success==true) {
                Session::put('id_user', ''.$response->id.'');
                Session::put('level', 'user');
                Session::put('access_token', ''.$response->access_token.'');
                Session::put('acl', $response->acl);
                Session::put('role', $response->role);

                if($response->pcare_credential_setup == true){
                    Session::put('pcare', true);
                }else{
                    Session::put('pcare', false);
                }

                /*try {
                    $user = User::find($response->id);
                    auth()->login($user);
                } catch(\Exception $e){ }*/
            }

            return response()->json($response);
        } else {
            (new BaseController)->auth(true, '', 'login');
            (new BaseController)->url();

            return view('user.login', compact('lang','api_url','storage','api_token','doctor_url'));
        }
    }

    public function logout() {
        Session::flush();

        try {
            auth()->logout();
        } catch(\Exception $e){}

        return redirect('');
    }

    public function register($lang, Request $request) {
        (new BaseController)->lang($lang);
        (new BaseController)->auth(true, '', 'register');
        (new BaseController)->url();

        $storage = (new Setting)->storage();
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $doctor_url = (new Setting)->doctor_url();

        return view('user.register', compact('lang','api_url','storage','api_token','doctor_url')); 
    }

    public function forgot_password($lang, Request $request) {
        (new BaseController)->lang($lang);
        (new BaseController)->auth(true, '', 'login');
        (new BaseController)->url();

        $storage = (new Setting)->storage();
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $doctor_url = (new Setting)->doctor_url();

        return view('user.forgot-password', compact('lang','api_url','storage','api_token','doctor_url'));
    }

    public function reset_password($lang, $code, Request $request) {
        (new BaseController)->lang($lang);
        (new BaseController)->auth(true, '', 'login');
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $doctor_url = (new Setting)->doctor_url();

        $client = new Client();
        $user = json_decode($client->get($api_url.'/'.$lang.'/user/reset-password/'.$code, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('user.reset-password', compact('lang','user','api_url','storage','code','api_token','doctor_url'));
    }

    public function email_verification($lang, $code, Request $request) {
        (new BaseController)->lang($lang);

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        $client = new Client();
        $verification = json_decode($client->get($api_url.'/'.$lang.'/user/email-verification/'.$code, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('user.email-verification', compact('lang','verification','api_url','storage','api_token','code'));

    }

    public function setting($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_user = Session::get('id_user');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        $client = new Client();
        $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('user.profile.edit', compact('id_user','user','lang','api_url','storage','api_token'));
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $role = Session::get('role');
        $storage = (new Setting)->storage();

        return view('user.list', compact('id_clinic','lang','api_url','id_user','storage','api_token','role'));
    }

    public function create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('user.create', compact('id_clinic','lang','api_url','id_user','storage','api_token'));
    }

    public function edit($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('user.edit', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('user.detail', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    /*
       Aprianto
    */
    public function setupsession($lang, Request $request) {
        
        /*
        $response = json_decode($result->getBody()->getContents());
        Session::put('id_user', ''.$response->id.'');
        Session::put('level', 'user');
        Session::put('access_token', ''.$response->access_token.'');
        Session::put('acl', $response->acl);
        Session::put('role', $response->role);

        if($response->pcare_credential_setup == true){
            Session::put('pcare', true);
        }else{
            Session::put('pcare', false);
        }
        */
    }

}
