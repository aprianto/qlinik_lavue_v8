<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class BroadcastController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'admin');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $storage = (new Setting)->storage();

        return view('broadcast.list', compact('id_user','level','api_url','lang','storage','api_token'));
    }
}
