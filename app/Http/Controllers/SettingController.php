<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class SettingController extends Controller
{
    public function __construct() {
        
    }

    public function setting($lang) {
        (new BaseController)->auth(false, 'admin');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('setting.application', compact('lang','api_url','id_user','storage','api_token'));
    }
}
