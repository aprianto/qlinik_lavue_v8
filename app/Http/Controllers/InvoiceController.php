<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class InvoiceController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');

        return view('invoice.list', compact('lang','id_clinic','api_url','docs_url','id_user','storage','api_token'));
    }

    public function payment($lang, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id = $request->input('id');
        $id_medical_records = $request->input('medical');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('invoice.payment', compact('lang','id_clinic', 'id','api_url', 'id_medical_records','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');

        return view('invoice.detail', compact('id_clinic','lang','id','api_url','docs_url','id_user','storage','api_token'));
    }
}
