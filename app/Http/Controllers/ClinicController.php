<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ClinicController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, array('user','admin'), 'clinic');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();

        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $acl = Session::get('acl');

        $preview = false;

        if($level=="user") {

            $client = new Client();

            $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

            if(empty($user->clinic->id) && $level=="user") {            
                return view('clinic.setup', compact('lang','id_user','api_url','map_key','api_token','storage','acl'));
            } elseif($level=="user") {
                $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                return view('clinic.profile.index', compact('user','id_user','level','clinic', 'lang','api_url','map_key', 'preview','storage','api_token','acl'));
            }

        } else {
            return view('clinic.list', compact('lang','id_user','api_url','map_key','storage','level','api_token'));
        }
    }

    public function show($lang, $id) {
        (new BaseController)->auth(false, array('user', 'admin'));
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $map_key = (new Setting)->map_key();

        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $acl = Session::get('acl');
        $storage = (new Setting)->storage();

        $preview = true;

        $client = new Client();

        if($level=="admin") {
            $user = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $get_clinic = $id;
        } else {
            $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $get_clinic = 'profile';
        }

        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$get_clinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        return view('clinic.profile.index', compact('user','id_user','level','clinic', 'lang','api_url','map_key','preview','storage','api_token','acl'));
    }

    public function setting($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        $client = new Client();
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('clinic.profile.edit', compact('id_clinic','id_user','clinic','lang','api_url','storage','api_token'));
    }

    public function location($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $api_token = Session::get('access_token');
        $map_key = (new Setting)->map_key();
        $storage = (new Setting)->storage();

        $client = new Client();
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('clinic.profile.location', compact('id_clinic','id_user','clinic','lang','api_url','map_key','storage','api_token'));
    }

    public function polyclinic($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('clinic.polyclinic', compact('id_clinic','id_user','lang','api_url','storage','api_token'));
    }

    public function insurance($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();
        $level = Session::get('level');


        return view('clinic.insurance', compact('id_clinic','id_user','lang','api_url','storage','api_token', 'level'));
    }

    public function suspended($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();
        
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');

        $client = new Client();
        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
   
        if($clinic->suspended!=true) {
            return redirect(url('/'));
            die();
        } else {            
            return view('clinic.payment.suspended', compact('lang','api_url','id_clinic','clinic','storage','api_token'));
        }
        
    }

}
