<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App;
use Redirect;

use Response;

use Storage;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


class BaseController extends Controller
{
	public function __construct() {
    }

    public function session(Request $request) {
        $value = $request->input('value');

        if ($value === 'true') {
            $value = true;
        } else if ($value === 'false') {
            $value = false;
        }

        Session::put($request->input('session'), $value);
    }

    public function language($language) {
        Session::put('language', $language);
        $url = Session::get('url');

        $splitUrl = explode("/#/", $url);
        $getLang = explode("/", $splitUrl[1]);


        if($getLang[1]=="suspended") {
            $url = url('/').'/'.$language.'/suspended';
        } else {
            $url = str_replace("?","/",$url);
            $url = str_replace("=","/",$url);

            $url = str_replace("/".$getLang[0]."/","/".$language."/",$url);  
        }

        return redirect($url);
    }

    public function lang($lang) {
        App::setLocale($lang);
    }

    public function header($lang, Request $request) {
        $this->lang($lang);
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $pusher_key = (new Setting)->pusher_key();
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $acl = Session::get('acl');
        $role = Session::get('role');
        $id_clinic = Session::get('id_clinic');
        $clinics = Session::get('select_clinic');
        $id_polyclinic = Session::get('id_polyclinic');
        $name_polyclinic = Session::get('name_polyclinic');
        $client = new Client();
        if($level=="admin") {
            $user = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $clinic = "";
        } else if($level == "user") {
            $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        } else {
            $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $clinic = Session::get('clinic');
        }
        
        return view('layouts.header', compact('id_user', 'level', 'user', 'clinic', 'lang','api_url', 'pusher_key', 'storage','api_token','acl','role', 'clinics', 'id_clinic', 'id_polyclinic', 'name_polyclinic'));
        
    }

    public function url() {
        $url = url()->full();
        $url = str_replace(url('/'), url('/').'/#', $url);
        Session::put('url', $url);
    }

    public function auth($redirect, $level, $link='') {
        if(!Session::has('language')) {
            $lang = "id";
            Session::put('language', $lang);
        } else {
            $lang = Session::get('language');
        }
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $level_user = Session::get('level');
        $id_clinic = Session::get('id_clinic');
        if(empty($id_user)) {
            if($link!='login' && $link!='register' && $level_user != 'doctor') {
                echo "<script>redirect('redirect/".$lang."/login')</script>";
                die();
            }
        } else {
            $client = new Client();
            if($level_user=="admin") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            } else if($level_user == "user") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            }else {
                $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            }
            if(empty($user->clinic->id) && $level_user=="user") {
                if($link!="clinic") {
                    echo "<script>redirect('redirect/".$lang."/clinic')</script>";
                }
            }else {
                if(empty($level)) {                    
                    if($redirect==true) {
                        echo "<script>redirect('".url('/')."')</script>";
                        die();
                    }
                } else {
                    if($id_clinic && $level_user == "doctor") return;
                    $count = count($level);
                    if($count==1) {
                        if($level!=$level_user) {
                            echo "<script>redirect('".url('/')."/#/".$lang."/error')</script>";
                            die();
                        } 
                    } else {
                        $error = false;
                        for($i=0;$i<$count;$i++) {
                            if($level[$i]==$level_user) {
                                $error = true;
                            }
                        }
                        if($error==false) {
                            echo "<script>redirect('".url('/')."/#/".$lang."/error')</script>";
                            die();
                        } 
                    }
                }
            }
        }
    }

    public function error() {
        $this->url();
        if(!Session::has('language')) {
            $lang = "id";
            Session::put('language', $lang);
        } else {
            $lang = Session::get('language');
        }
        return view('pages.404', compact('lang'));
    }

    public function server_error() {
        $this->url();
        return view('pages.505');
    }

    public function home(Request $request) { 
        $url = url()->full();
        if(!Session::has('language')) {
            $lang = "id";
            Session::put('language', $lang);
        } else {
            $lang = Session::get('language');
        }
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $id_clinic = Session::get('id_clinic');
        $this->lang($lang);
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $doctor_url = (new Setting)->doctor_url();
        $url = url()->full();
        $url = url('/').'/#/'.$lang.'/login';
        $setup = "";
        $help_setup = "";
        $help_price = "";
        $help_schedule = "";
        $help_action_schedule = "";
        $email_verification = "";
        $user = "";
        $clinic = "";
        if(!empty($id_user) && ($level=="user" || $level=="doctor")) {
            $client = new Client();
            if ($level == "user") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                $email_verification = $user->email_verification;
            } else if($level == "doctor") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                $email_verification = true;
                $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$id_clinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                Session::put('clinic', $clinic);
            }
            
            if(!empty($user->clinic->id) && $level == "user") {
                $id_clinic = $user->clinic->id;
                Session::put('id_clinic', $id_clinic);
                $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                Session::put('clinic', $clinic);
                $setup = $clinic->setting->setup;
                $help_setup = $clinic->setting->help_setup;
                $help_price = $clinic->setting->help_price;
                $help_schedule = $clinic->setting->help_schedule;
                $help_action_schedule = $clinic->setting->help_action_schedule;
                if($clinic->suspended==true) {
                    return redirect(url('/').'/'.$lang.'/suspended');
                    die();
                }
            }
        }

        $pusher_key = (new Setting)->pusher_key();
        $pusher_auth_url = (new Setting)->pusher_auth_url();

        if(!empty($id_user)) {
            $client = new Client();

            if($level=="admin") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                $clinic = "";
            } else if ($level=="user") {
                $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

                if($id_clinic!="") {
                    $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                }
            }else {
                $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());   
                $clinic = Session::get('clinic');
            }
        }

    	return view('layouts.template', compact('id_user', 'level', 'url', 'id_clinic', 'lang','api_token','api_url','clinic','user','setup','help_setup','help_price','help_schedule','help_action_schedule','email_verification','storage',  'pusher_key','doctor_url','pusher_auth_url'));
    }

    public function dashboard($lang, Request $request) {
        $this->auth(true, array('user','admin'), '');
        $this->lang($lang);
    	$this->url();
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $role = Session::get('role');
        $client = new Client();
        if($level=="admin") {
            $user = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
        } else if($level=="user"){
            $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

            if(!empty($user->clinic->id)) {
                $id_clinic = $user->clinic->id;
            }
        }else {
            $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
            $id_clinic = Session::get('id_clinic');
        }

		return view('dashboard.dashboard', compact('lang', 'user', 'id_clinic', 'level','api_url','storage','api_token','role'));
    }

    public function blank(Request $request) {
        $id_clinic = Session::get('id_clinic');
        if(!Session::has('language')){
            $lang = "id";
        } else {
            $lang = Session::get('language');
        }
        $this->auth(false, '');
        $this->lang($lang);
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        if(!empty($id_user)) {
            $level = Session::get('level');
            if($level=="user") {
                $client = new Client();
                $user = json_decode($client->get($api_url.'/'.$lang.'/user/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                if(empty($user->clinic->id) && $level=="user") {
                    echo "<script>redirect('redirect/".$lang."/clinic')</script>";
                } else {
                    echo "<script>redirect('".url('/')."/#/".$lang."/home')</script>";
                }

            }else if($level=="doctor") {
                $client = new Client();
                $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());
                echo "<script>redirect('".url('/')."/#/".$lang."/home')</script>";
            }else {
                echo "<script>redirect('".url('/')."/#/".$lang."/home')</script>";
            }    
        }
    }

    public function redirect($lang, $link, Request $request) {
        $link = str_replace("---", "/", $link);
        return redirect(url('/').'/#/'.$lang.'/'.$link);
    }

    public function download($path, $name){
        $file=(new Setting)->storage().'/file/'.$path;

        $download = tempnam(sys_get_temp_dir(), $name);
        copy($file, $download);

        return Response::download($download, $name);
    }

    public function push(Request $request) {
        $pusher = App::make('pusher');

        $pusher->trigger(
            'chat',
            'App\Events\NewComment', 
            array(
                'message' => 'a',
            )
        );
    }

    public function get_push(Request $request) {
        return view('pages.example', compact('a'));
    }

    public function example(Request $request) {
        $data = "1000";
        $secretKey = "7789";
        $tStamp = strtotime(Date("Y-m-d H:i:s"));
        $signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
        $encodeSignature = base64_encode($signature);

        $client = new Client();
        $return = json_decode($client->get('http://dvlp.bpjs-kesehatan.go.id:8081/devWsLokalRest/Peserta/Peserta/0001101521158', ['headers' => ['X-cons-id' => $data,'X-timestamp' => $tStamp,'X-signature' => $encodeSignature]])->getBody()->getContents(), true);

        return $return;

        //return response()->json($response);
    }

    public function catcher_id_clinic(Request $request) {
        if(!Session::has('language')){
            $lang = "id";
        } else {
            $lang = Session::get('language');
        }

        $id_clinic = $request->query('id_clinic');
        $id_polyclinic = $request->query('id_polyclinic');
        $name_polyclinic = $request->query('name_polyclinic');
        
        Session::put('id_polyclinic', $id_polyclinic);
        Session::put('name_polyclinic', $name_polyclinic);
        Session::put('id_clinic', $id_clinic);
         return redirect ('');
    }
}
