<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;
use Redirect;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class DoctorController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, array('user','admin'));
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $client = new Client();

        $clinic = json_decode($client->get($api_url.'/'.$lang.'/clinic/'.$id_clinic, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('doctor.list', compact('id_clinic','level','lang','api_url','id_user','level','storage','api_token', 'clinic'));
    }

    public function login($lang, Request $request) {
        (new BaseController)->lang($lang);

        $method = $request->method();
        
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        if ($request->isMethod('post')) {
            $input = $request->all(); 

            $client = new Client();
            $result = $client->post($api_url.'/'.$lang.'/doctor/login', [
                'headers' => [
                    'Authorization' => "Bearer ".$api_token,
                ],
                'form_params' => [
                    'email' => $input['email'],
                    'password' => $input['password']
                ]
            ]);

            $response = json_decode($result->getBody()->getContents());
            if($response->success==true) {
                Session::put('id_user', ''.$response->id.'');
                Session::put('level', 'doctor');
                Session::put('access_token', ''.$response->access_token.'');
                Session::put('acl', $response->acl);
                Session::put('role', 'doctor');
                Session::put('select_clinic', $response->clinics);
                Session::put('name_doctor', ''.$response->name.'');
            }
            return response()->json($response);
        } else {
            (new BaseController)->auth(true, '', 'login');
            (new BaseController)->url();

            return view('doctor.login', compact('storage', 'lang', 'api_url'));
        }
    }
    
    public function logout(Request $request) {
        Session::flush();

        try {
            auth()->logout();
        } catch(\Exception $e){}

        return redirect('/#/'.$request->query("lang").'/doctor/login');

    }

    public function forgot_password($lang, Request $request) {
        (new BaseController)->lang($lang);
        (new BaseController)->auth(true, '', 'login');
        (new BaseController)->url();
        
        $api_url = (new Setting)->api_url();
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();

        return view('doctor.forgot-password', compact('lang','api_url','storage','app_url'));
    }

    public function reset_password($lang, $code, Request $request) {
        (new BaseController)->lang($lang);
        (new BaseController)->auth(true, '', 'login');
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $app_url = (new Setting)->app_url();

        $client = new Client();
        $user = json_decode($client->get($api_url.'/'.$lang.'/doctor/reset-password/'.$code, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('doctor.reset-password', compact('lang','user','code','api_url','storage','app_url'));
    }


    public function create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');

        return view('doctor.create', compact('id_clinic','lang','api_url','id_user','storage','api_token'));
    }

    public function edit($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('doctor.edit', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, array('user','admin'));
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $level = Session::get('level');

        return view('doctor.detail', compact('id_clinic','level','lang','id','api_url','id_user','storage','api_token'));
    }

    public function schedule($lang, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $id_user = Session::get('id_user');
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $level = Session::get('level');
        $storage = (new Setting)->storage();

        if($level=="user") {
            $id_doctor = $request->input('id');
        } else {
            $id_doctor = $id_user;
            $id_clinic = $request->input('id');
        }

        return view('doctor.schedule', compact('id_clinic','id_doctor','level','lang','api_url','id_user','storage','api_token'));
    }

    public function calendar($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $clinic = Session::get('clinic');

        return view('doctor.calendar', compact('id_clinic','lang','api_url','id_user','storage','api_token', 'level', 'clinic'));
    }
}
