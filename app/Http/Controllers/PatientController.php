<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PatientController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, array('user','admin'));

        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $acl = Session::get('acl');
        $id_polyclinic = Session::get('id_polyclinic');
        $name_polyclinic = Session::get('name_polyclinic');

        return view('patient.list', compact('id_clinic','lang','api_url','docs_url','id_user','level','storage','api_token','acl', 'id_polyclinic', 'name_polyclinic'));
    }

    public function create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');

        return view('patient.create', compact('id_clinic','lang','api_url','id_user','storage','api_token'));
    }

    public function edit($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('patient.edit', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, array('user','admin'));
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $level = Session::get('level');

        return view('patient.detail', compact('id_clinic','level','lang','id','api_url','docs_url','id_user','storage','api_token'));
    }
    
    public function showBpjs($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $client = new Client();
        
        $bpjs = json_decode($client->get($api_url.'/'.$lang.'/bpjs/find-peserta-with/'.$id, ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('patient.create', compact('id_clinic','lang','api_url','id_user','storage','api_token','bpjs','id'));
    }

    public function bpjs($type, $number, Request $request) {
        if($type=="nik") {
            $type = "nik/";
        } else {
            $type = "";
        }

        $data = "1000";
        $secretKey = "7789";
        $tStamp = strtotime(Date("Y-m-d H:i:s"));
        $signature = hash_hmac('sha256', $data."&".$tStamp, $secretKey, true);
        $encodeSignature = base64_encode($signature);

        $client = new Client();
        $return = json_decode($client->get('http://dvlp.bpjs-kesehatan.go.id:8081/devWsLokalRest/Peserta/Peserta/'.$type.''.$number, ['headers' => ['X-cons-id' => $data,'X-timestamp' => $tStamp,'X-signature' => $encodeSignature]])->getBody()->getContents(), true);

        return $return;
    } 
}
