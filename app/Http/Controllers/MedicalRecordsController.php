<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MedicalRecordsController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $name = $request->input('name');
        $birth_date = $request->input('birth_date');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $acl = Session::get('acl');
        $name_polyclinic = Session::get('name_polyclinic');
        $id_polyclinic = Session::get('id_polyclinic');
        $name_doctor = Session::get('name_doctor');

        return view('medical_records.list', compact('level','lang','id_clinic','api_url','docs_url','id_user','storage','name','birth_date','from_date','to_date','api_token','acl', 'id_polyclinic', 'name_polyclinic', 'name_doctor'));
    }

    public function create($lang, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $download_url = (new Setting)->download_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $id_patient = $request->input('patient');
        $id_schedule = $request->input('schedule');
        $level = Session::get('level');
        $id_polyclinic = Session::get('id_polyclinic');
        $pcare = Session::get('pcare');

        return view('medical_records.create', compact('id_clinic','level','lang','id_patient','id_schedule','api_url','docs_url','download_url','id_user','storage','api_token', 'id_polyclinic', 'pcare'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $download_url = (new Setting)->download_url();
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $preview = $request->input('preview');
        $level = Session::get('level');

        return view('medical_records.detail', compact('id_clinic','level','lang','id','api_url','docs_url','download_url','id_user','preview','storage','api_token'));
    }

    public function patient($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $download_url = (new Setting)->download_url();
        $storage = (new Setting)->storage();
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $level = Session::get('level');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $id_patient = $id;
        $id_polyclinic = Session::get('id_polyclinic');
        $name_polyclinic = Session::get('name_polyclinic');

        return view('medical_records.patient', compact('level','lang','id_clinic','api_url','docs_url','download_url','id_user','storage','id_patient','from_date','to_date','id','api_token', 'name_polyclinic', 'id_polyclinic'));
    }
}
