<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class ServiceController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('service.list', compact('id_clinic','lang','api_url','id_user','storage','api_token'));
    }

    public function create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('service.create', compact('id_clinic','lang','api_url','id_user','storage','api_token'));
    }

    public function edit($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('service.edit', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }

    public function show($lang, $id, Request $request) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();

        return view('service.detail', compact('id_clinic','lang','id','api_url','id_user','storage','api_token'));
    }
}
