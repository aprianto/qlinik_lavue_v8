<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class VitalSignController extends Controller
{
    public function __construct() {
        
    }
    
    public function create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_user = Session::get('id_user');
        $id_clinic = Session::get('id_clinic');
        $storage = (new Setting)->storage();
        $id_polyclinic = Session::get('id_polyclinic');
        $level = Session::get('level');

        return view('vital_sign.create', compact('lang', 'id_clinic','api_url','id_user','storage','api_token', 'id_polyclinic', 'level'));
    }

}
