<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;
use Redirect;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AdminController extends Controller
{

    public function __construct() {
    }

    public function index() {
        if(!Session::has('language')){
            $lang = "id";
        } else {
            $lang = Session::get('language');
        }

        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_user = Session::get('id_user');
        if(!empty($id_user)) {
            return redirect(url('/').'/#/'.$lang.'/home');
        } else {
            return redirect(url('/').'/#/'.$lang.'/admin/login');
        }
    }

    public function login($lang, Request $request) {
        (new BaseController)->lang($lang);
       
        $method = $request->method();
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
         
        if ($request->isMethod('post')) {
            $input = $request->all();   
            $client = new Client();
            $result = $client->post($api_url.'/'.$lang.'/admin/login', [
                'headers' => [
                    'Authorization' => "Bearer ".$api_token,
                ],
                'form_params' => [
                    'username' => $input['username'],
                    'password' => $input['password'],
                ]
            ]);
           
            $response = json_decode($result->getBody()->getContents());
                
            if($response->success==true) {
                Session::put('id_user', ''.$response->id.'');
                Session::put('level', 'admin');
                Session::put('access_token', ''.$response->access_token.'');
            }

            return response()->json($response);
        } else {
            (new BaseController)->auth(true, '', 'login');
            (new BaseController)->url();

            return view('admin.login', compact('lang','api_url','storage','api_token'));
        }
    }

    public function setting($lang) {
        (new BaseController)->auth(false, 'admin');

        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_user = Session::get('id_user');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();

        $client = new Client();
        $admin = json_decode($client->get($api_url.'/'.$lang.'/admin/profile', ['headers' => ['Authorization' => "Bearer ".$api_token]])->getBody()->getContents());

        return view('admin.profile.edit', compact('id_user','admin','lang','api_url','storage','api_token'));
    }

    public function logout() {
        Session::put('id_user', '');
        Session::put('level', '');

        return redirect(url('/').'/admin');
    }

}
