<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PaymentTypeController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'admin');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $storage = (new Setting)->storage();
        $id_user = Session::get('id_user');

        return view('payment_type.list', compact('lang','api_url','id_user','storage','api_token'));
    }

}
