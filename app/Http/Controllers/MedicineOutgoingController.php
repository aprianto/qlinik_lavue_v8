<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class MedicineOutgoingController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('medicine_outgoing.list', compact('lang','api_url','id_user','id_clinic','storage','api_token'));
    }
}
