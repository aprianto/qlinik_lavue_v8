<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

class MedicineController extends Controller
{
    public function __construct() {
        
    }

    public function index($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();
        
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();
        $docs_url = (new Setting)->docs_url();

        return view('medicine.list', compact('lang','api_url','id_user','id_clinic','storage','api_token', 'docs_url'));
    }

    public function unit_create($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();
        
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();

        return view('medicine.create-unit', compact('lang','api_url','id_user','id_clinic','storage','api_token'));
    }
    public function detail_inventory($lang, $id) {

        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();
        
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $id_clinic = Session::get('id_clinic');
        $id_user = Session::get('id_user');
        $storage = (new Setting)->storage();
        $id_inventory = $id;

        return view('medicine.detail-inventory', compact('lang','api_url','id_user','id_clinic','storage','api_token', 'id_inventory'));
    }
}
