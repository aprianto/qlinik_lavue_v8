<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Storage;

class Setting extends Controller
{
    public function app_url()
    {
        return config('app.url');
    }

    public function doctor_url()
    {
        return config('app.doctor_url');
    }

    public function docs_url()
    {
        return config('app.url');
    }

    public function download_url()
    {
        return config('app.url');
    }

    public function api_url()
    {
        return config('app.api_url');
    }

    public function pusher_auth_url()
    {
        return config('app.api_url_pusher');
    }

    public function api_token()
    {
        // return config('app.api_token);
        return Session::get('access_token');
    }

    public function map_key()
    {
        return config('app.map_key');
    }

    public function pusher_key()
    {
        return config('app.pusher_key');
    }

    public function storage()
    {
      return config('app.storage_url') !== null
        ? config('app.storage_url')
        : $this->api_url() . '';
    }
}
