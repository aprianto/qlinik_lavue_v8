<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Http\Requests;
use Session;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ReportController extends Controller
{
    public function __construct() {
        
    }

    public function income($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();

        return view('report.income', compact('id_clinic','lang','api_url','docs_url','storage','api_token'));
    }

    public function commission($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();

        return view('report.commission', compact('id_clinic','lang','api_url','docs_url','storage','api_token'));
    }

    public function service($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();

        return view('report.service', compact('id_clinic','lang','api_url','docs_url','storage','api_token'));
    }

    public function medicine($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();

        return view('report.medicine', compact('id_clinic','lang','api_url','docs_url','storage','api_token'));
    }

    public function illness($lang) {
        (new BaseController)->auth(false, 'user');
        (new BaseController)->lang($lang);
        (new BaseController)->url();

        $id_clinic = Session::get('id_clinic');
        $api_url = (new Setting)->api_url();
        $api_token = Session::get('access_token');
        $docs_url = (new Setting)->docs_url();
        $storage = (new Setting)->storage();

        return view('report.illness', compact('id_clinic', 'lang', 'api_url', 'api_token', 'docs_url', 'storage'));
    }
}
