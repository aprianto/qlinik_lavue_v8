

var app = angular.module('medigoWebApp', ['ngRoute']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider
    
    .when("/", {templateUrl: "blank", controller: "PageCtrl"})
    .when("/:lang/login", {templateUrl: function(url){ return ""+url.lang+"/login"}, controller: "PageCtrl"})
    .when("/:lang/register", {templateUrl: function(url){ return ""+url.lang+"/register"}, controller: "PageCtrl"})
    .when("/:lang/forgot-password", {templateUrl: function(url){ return ""+url.lang+"/forgot-password"}, controller: "PageCtrl"})
    .when("/:lang/reset-password/:code", {templateUrl: function(url){ return ""+ url.lang+"/reset-password/" + url.code}, controller: "PageCtrl"})
    .when("/:lang/email/:code", {templateUrl: function(url){ return ""+ url.lang+"/email/" + url.code}, controller: "PageCtrl"})
    .when("/:lang/home", {templateUrl: function(url){ return ""+url.lang+"/home"}, controller: "PageCtrl"})    
    .when("/:lang/profile", {templateUrl: function(url){ return ""+url.lang+"/profile"}, controller: "PageCtrl"})
    .when("/:lang/profile/setting", {templateUrl: function(url){ return ""+url.lang+"/profile/setting"}, controller: "PageCtrl"})
    .when("/:lang/clinic", {templateUrl: function(url){ return ""+url.lang+"/clinic"}, controller: "PageCtrl"})    
    .when("/:lang/clinic/:id", {templateUrl: function(url){ return ""+url.lang+"/clinic/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/clinic/setting", {templateUrl: function(url){ return ""+url.lang+"/clinic/setting"}, controller: "PageCtrl"})
    .when("/:lang/clinic/location", {templateUrl: function(url){ return ""+url.lang+"/clinic/location"}, controller: "PageCtrl"})
    .when("/:lang/clinic/polyclinic", {templateUrl: function(url){ return ""+url.lang+"/clinic/polyclinic"}, controller: "PageCtrl"})
    .when("/:lang/clinic/insurance", {templateUrl: function(url){ return ""+url.lang+"/clinic/insurance"}, controller: "PageCtrl"})
    .when("/:lang/polyclinic", {templateUrl: function(url){ return ""+url.lang+"/polyclinic"}, controller: "PageCtrl"})
    .when("/:lang/user", {templateUrl: function(url){ return ""+url.lang+"/user"}, controller: "PageCtrl"})
    .when("/:lang/user/create", {templateUrl: function(url){ return ""+url.lang+"/user/create"}, controller: "PageCtrl"})
    .when("/:lang/user/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/user/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/user/:id", {templateUrl: function(url){ return ""+url.lang+"/user/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/service/category/default", {templateUrl: function(url){ return ""+url.lang+"/service/category/default"}, controller: "PageCtrl"})
    .when("/:lang/service/category", {templateUrl: function(url){ return ""+url.lang+"/service/category"}, controller: "PageCtrl"})
    .when("/:lang/service/default", {templateUrl: function(url){ return ""+url.lang+"/service/default"}, controller: "PageCtrl"})
    .when("/:lang/service", {templateUrl: function(url){ return ""+url.lang+"/service"}, controller: "PageCtrl"})
    .when("/:lang/service/create", {templateUrl: function(url){ return ""+url.lang+"/service/create"}, controller: "PageCtrl"})
    .when("/:lang/service/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/service/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/service/:id", {templateUrl: function(url){ return ""+url.lang+"/service/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/doctor", {templateUrl: function(url){ return ""+url.lang+"/doctor"}, controller: "PageCtrl"})
    .when("/:lang/doctor/create", {templateUrl: function(url){ return ""+url.lang+"/doctor/create"}, controller: "PageCtrl"})
    .when("/:lang/doctor/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/doctor/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/doctor/:id", {templateUrl: function(url){ return ""+url.lang+"/doctor/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/doctor/schedule", {templateUrl: function(url){ return ""+url.lang+"/doctor/schedule"}, controller: "PageCtrl"})
    .when("/:lang/doctor/schedule/id/:id", {templateUrl: function(url){ return ""+url.lang+"/doctor/schedule?id=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/doctor/calendar", {templateUrl: function(url){ return ""+ url.lang+"/doctor/calendar"}, controller: "PageCtrl"})
    .when("/:lang/commission", {templateUrl: function(url){ return ""+ url.lang+"/commission"}, controller: "PageCtrl"})
    .when("/:lang/commission/create", {templateUrl: function(url){ return ""+url.lang+"/commission/create"}, controller: "PageCtrl"})
    .when("/:lang/commission/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/commission/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/medicine/default", {templateUrl: function(url){ return ""+ url.lang+"/medicine/default"}, controller: "PageCtrl"})
    .when("/:lang/medicine", {templateUrl: function(url){ return ""+ url.lang+"/medicine"}, controller: "PageCtrl"})
    .when("/:lang/medicine-incoming", {templateUrl: function(url){ return ""+ url.lang+"/medicine-incoming"}, controller: "PageCtrl"})
    .when("/:lang/medicine-outgoing", {templateUrl: function(url){ return ""+ url.lang+"/medicine-outgoing"}, controller: "PageCtrl"})
    .when("/:lang/patient/category/default", {templateUrl: function(url){ return ""+ url.lang+"/patient/category/default"}, controller: "PageCtrl"})
    .when("/:lang/patient/category", {templateUrl: function(url){ return ""+ url.lang+"/patient/category"}, controller: "PageCtrl"})
    .when("/:lang/patient", {templateUrl: function(url){ return ""+ url.lang+"/patient"}, controller: "PageCtrl"})
    .when("/:lang/patient/create", {templateUrl: function(url){ return ""+ url.lang+"/patient/create"}, controller: "PageCtrl"})
    .when("/:lang/patient/create/:id", {templateUrl: function(url){ return ""+ url.lang+"/patient/create/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/patient/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/patient/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/patient/:id", {templateUrl: function(url){ return ""+url.lang+"/patient/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/schedule", {templateUrl: function(url){ return ""+ url.lang+"/schedule"}, controller: "PageCtrl"})
    .when("/:lang/schedule/sick", {templateUrl: function(url){ return ""+ url.lang+"/schedule/sick"}, controller: "PageCtrl"})
    .when("/:lang/schedule/healthy", {templateUrl: function(url){ return ""+ url.lang+"/schedule/healthy"}, controller: "PageCtrl"})
    .when("/:lang/schedule/create", {templateUrl: function(url){ return ""+ url.lang+"/schedule/create"}, controller: "PageCtrl"})
    .when("/:lang/schedule/create/patient/:id", {templateUrl: function(url){ return ""+ url.lang+"/schedule/create?patient=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/schedule/:id/edit", {templateUrl: function(url){ return ""+url.lang+"/schedule/"+url.id+"/edit"}, controller: "PageCtrl"})
    .when("/:lang/schedule/:id", {templateUrl: function(url){ return ""+url.lang+"/schedule/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/group/activities", {templateUrl: function(url){ return ""+ url.lang+"/group/activities"}, controller: "PageCtrl"})
    .when("/:lang/group/activities/create", {templateUrl: function(url){ return ""+ url.lang+"/group/activities/create"}, controller: "PageCtrl"})
    .when("/:lang/group/activities/:id", {templateUrl: function(url){ return ""+url.lang+"/group/activities/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/icd", {templateUrl: function(url){ return ""+ url.lang+"/icd"}, controller: "PageCtrl"})
    .when("/:lang/vital_sign/create", {templateUrl: function(url){ return ""+ url.lang+"/vital_sign/create"}, controller: "PageCtrl"})
    .when("/:lang/medical", {templateUrl: function(url){ return ""+ url.lang+"/medical"}, controller: "PageCtrl"})
    .when("/:lang/medical/create", {templateUrl: function(url){ return ""+ url.lang+"/medical/create"}, controller: "PageCtrl"})
    .when("/:lang/medical/create/patient/:id", {templateUrl: function(url){ return ""+ url.lang+"/medical/create?patient=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/medical/create/schedule/:id", {templateUrl: function(url){ return ""+ url.lang+"/medical/create?schedule=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/medical/:id", {templateUrl: function(url){ return ""+url.lang+"/medical/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/medical/q/:key", {templateUrl: function(url){ return ""+url.lang+"/medical?"+url.key}, controller: "PageCtrl"})
    .when("/:lang/medical/patient/:id", {templateUrl: function(url){ return ""+url.lang+"/medical/patient/"+url.id}, controller: "PageCtrl"})
    .when("/:lang/invoice", {templateUrl: function(url){ return ""+ url.lang+"/invoice"}, controller: "PageCtrl"})
    .when("/:lang/invoice/:id", {templateUrl: function(url){ return ""+ url.lang+"/invoice/" + url.id}, controller: "PageCtrl"})
    .when("/:lang/payment", {templateUrl: function(url){ return ""+ url.lang+"/payment"}, controller: "PageCtrl"})
    .when("/:lang/payment/id/:id", {templateUrl: function(url){ return ""+ url.lang+"/payment?id=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/payment/medical/:id", {templateUrl: function(url){ return ""+ url.lang+"/payment?medical=" + url.id}, controller: "PageCtrl"})
    .when("/:lang/payment/clinic", {templateUrl: function(url){ return ""+ url.lang+"/payment/clinic"}, controller: "PageCtrl"})
    .when("/:lang/payment/type", {templateUrl: function(url){ return ""+ url.lang+"/payment/type"}, controller: "PageCtrl"})
    .when("/:lang/report/income", {templateUrl: function(url){ return ""+ url.lang+"/report/income"}, controller: "PageCtrl"})
    .when("/:lang/report/commission", {templateUrl: function(url){ return ""+ url.lang+"/report/commission"}, controller: "PageCtrl"})
    .when("/:lang/report/service", {templateUrl: function(url){ return ""+ url.lang+"/report/service"}, controller: "PageCtrl"})
    .when("/:lang/report/medicine", {templateUrl: function(url){ return ""+ url.lang+"/report/medicine"}, controller: "PageCtrl"})
    .when("/:lang/report/illness", {templateUrl: function(url){ return ""+ url.lang+"/report/illness"}, controller: "PageCtrl"})
    .when("/:lang/insurance", {templateUrl: function(url){ return ""+ url.lang+"/insurance"}, controller: "PageCtrl"})
    .when("/:lang/admin/login", {templateUrl: function(url){ return ""+url.lang+"/admin/login"}, controller: "PageCtrl"})
    .when("/:lang/admin/setting", {templateUrl: function(url){ return ""+url.lang+"/admin/setting"}, controller: "PageCtrl"})
    .when("/:lang/notifications", {templateUrl: function(url){ return ""+ url.lang+"/notifications"}, controller: "PageCtrl"})
    .when("/:lang/broadcast", {templateUrl: function(url){ return ""+ url.lang+"/broadcast"}, controller: "PageCtrl"})
    .when("/:lang/setting", {templateUrl: function(url){ return ""+ url.lang+"/setting"}, controller: "PageCtrl"})
    .when("/:lang/help/doctor", {templateUrl: function(url){ return ""+url.lang+"/help/doctor"}, controller: "PageCtrl"})
    .when("/:lang/help/commission", {templateUrl: function(url){ return ""+url.lang+"/help/commission"}, controller: "PageCtrl"})
    .when("/:lang/help/service", {templateUrl: function(url){ return ""+url.lang+"/help/service"}, controller: "PageCtrl"})
    .when("/:lang/help/medicine", {templateUrl: function(url){ return ""+url.lang+"/help/medicine"}, controller: "PageCtrl"})
    .when("/:lang/help/patient", {templateUrl: function(url){ return ""+url.lang+"/help/patient"}, controller: "PageCtrl"})
    .when("/:lang/help/schedule", {templateUrl: function(url){ return ""+url.lang+"/help/schedule"}, controller: "PageCtrl"})
    .when("/:lang/help/medical", {templateUrl: function(url){ return ""+url.lang+"/help/medical"}, controller: "PageCtrl"})
    .when("/:lang/help/invoice", {templateUrl: function(url){ return ""+url.lang+"/help/invoice"}, controller: "PageCtrl"})
    .when("/example", {templateUrl: function(url){ return "example"}, controller: "PageCtrl"})
    .when("/:lang/doctor/login", {templateUrl: function(url){return "" + url.lang + "/doctor/login"}, controller: "pageCtrl"})
    .when("/:lang/doctor/forgot-password", {templateUrl: function(url){return "" + url.lang + "/doctor/forgot-password"}, controller: "pageCtrl"})
    .when("/:lang/doctor/reset-password/:code", {templateUrl: function(url){return "" + url.lang + "/doctor/reset-password/" + url.code}, controller: "pageCtrl"})
    .when("/:lang/catcher", {templateUrl: function(url){return "" + url.lang + "/catcher"}, controller: "pageCtrl"})
    
    .when("/:lang/medicine/unit", {templateUrl: function(url){ return "" + url.lang + "/medicine/unit" }, controller: "PageCtrl"})
    .when("/:lang/medicine/detail/:id", {templateUrl: function(url){ return "" + url.lang + "/medicine/detail/" + url.id}, controller: "PageCtrl"})

    .when("/error", {templateUrl: "404", controller: "PageCtrl"})
    .otherwise({
        redirectTo: '/error'
      })
  }]).
  
  controller('MapCtrl', ['$scope', '$location', function($scope, $location) {

    $(document).scrollTop(0);
    
    $scope.mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(41.923, 12.513),
    mapTypeId: google.maps.MapTypeId.TERRAIN
    }
    $scope.map = new google.maps.Map(document.getElementById('map'), $scope.mapOptions);

    var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

  }]).

  controller('TourCtrl', ['$scope', '$location', function($scope, $location) {

    $(document).scrollTop(0);
    
    $(function(){
    var introguide = introJs();
    introguide.setOptions({
    steps: [
    {
      element: '#intro1',
      intro: 'Click Here',
      position: 'bottom'
    },
    {
      element: '#intro2',
      intro: 'With 3D transforms, we can make simple.',
      position: 'top'
    },
    {
      element: '#intro3',
      intro: 'Hover over each title to display a longer description.',
      position: 'right'
    },
    {
      element: '#intro4',
      intro: 'Click the With 3D transforms, we can make simple.',
      position: 'left'
    },
    {
      element: '#intro5',
      intro: "Each demo will link to the previous & next entries.",
      position: 'bottom'
    }
    ]
    });
    introguide.start();
  });



    var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

  }]).

  controller('ProfileCtrl', ['$scope', '$location', function($scope, $location) {

    $(document).scrollTop(0);


    $scope.mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(41.923, 12.513),
    mapTypeId: google.maps.MapTypeId.TERRAIN
    }
    $scope.map = new google.maps.Map(document.getElementById('mapthree'), $scope.mapOptions);

    var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

  }]).

  controller('ImageCtrl', ['$scope', '$location', function($scope, $location) {

    $(document).scrollTop(0);


    var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

  }]).

  controller('ContactCtrl', ['$scope', '$location', function($scope, $location) {

    $(document).scrollTop(0);

    $scope.mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(41.923, 12.513),
    mapTypeId: google.maps.MapTypeId.TERRAIN
    }
    $scope.map = new google.maps.Map(document.getElementById('maptwo'), $scope.mapOptions);


    var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

  }]);

app.controller('PageCtrl', function ($scope, $location, $window) {  
  $window.ga('create', 'UA-129676206-2', 'auto');
  $scope.$on('$routeChangeSuccess', function () {
    $window.ga('send', 'pageview', { page: $location.path() });      
  });
  try {
    if(help_setup=='') {
      $("#main-content").css({
        "-webkit-transition":"all 0s ease 0s",
        "-moz-transition":"all 0s ease 0s",
        "-ms-transition":"all 0s ease 0s",
        "-o-transition":"all 0s ease 0s",
        "transition":"all 0s ease 0s"
      });

      $("#main-content").removeClass("main-step");

      setTimeout(function(){
          $("#main-content").css({
            "-webkit-transition":"all 0.4s ease 0s",
            "-moz-transition":"all 0.4s ease 0s",
            "-ms-transition":"all 0.4s ease 0s",
            "-o-transition":"all 0.4s ease 0s",
            "transition":"all 0.4s ease 0s"
          });
      },500);
    }
  } catch(err) {}

  try {
    
    if(email_verification=='' || email_verification==1) {
      $("#main-content").css({
        "-webkit-transition":"all 0s ease 0s",
        "-moz-transition":"all 0s ease 0s",
        "-ms-transition":"all 0s ease 0s",
        "-o-transition":"all 0s ease 0s",
        "transition":"all 0s ease 0s"
      });

      $("#main-content").removeClass("main-verification");

      setTimeout(function(){
          $("#main-content").css({
            "-webkit-transition":"all 0.4s ease 0s",
            "-moz-transition":"all 0.4s ease 0s",
            "-ms-transition":"all 0.4s ease 0s",
            "-o-transition":"all 0.4s ease 0s",
            "transition":"all 0.4s ease 0s"
          });
      },500);
    }
    if(email_verification == "") {
      $(".main-content").toggleClass("verification-email-active")
    }
  } catch(err) {}

  try {
    setTimeout(function(){
      $("#loading-progressBar").addClass("hide");
    },100);
  } catch(err) {}

  try {
    $(".dialouge").fadeOut();
  } catch(err) {}
    
    try {
      closeNotif();
    } catch(err) {}

    $(document).scrollTop(0);

      var loaded = 0;
    var imgCounter = $(".main-content img").length;
    if(imgCounter > 0){
      function doProgress() {
        $(".main-content img").load(function() {
          loaded++;
          var newWidthPercentage = (loaded / imgCounter) * 100;
          animateLoader(newWidthPercentage + '%');      
        })
      } 
      function animateLoader(newWidth) {
        $("#progressBar").width(newWidth);
        if(imgCounter === loaded){
          setTimeout(function(){
                    $("#progressBar").animate({opacity:0});
                },500);
        }
      }
      doProgress();
    }else{
      setTimeout(function(){
          $("#progressBar").css({
            "opacity":0,
            "width":"100%"
          });
      },500);
    }

    $('[data-toggle="tooltip"]').tooltip(); 

    $('[data-toggle="popover"]').popover(); 

      $('.refresh-content').on("click", function(){
      });

      $('.expand-content').on("click", function(){
        $(this).parent().parent().toggleClass("expand-this");
      });

      $('.close-content').on("click", function(){
        $(this).parent().parent().slideUp();
      });

      $('.tooltip-social').tooltip({
        selector: "a[data-toggle=tooltip]"
      });

      $scope.$on('$viewContentLoaded', function(event) {
        $window.ga('send', 'pageview', { page: $location.url() });
      });
});

function number(e) {
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    (e.keyCode == 65 && e.ctrlKey === true) ||
    (e.keyCode == 67 && e.ctrlKey === true) ||
    (e.keyCode == 88 && e.ctrlKey === true) ||
    (e.keyCode == 188) ||
    (e.keyCode >= 35 && e.keyCode <= 39)) {
       return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function switchButton(from, to, form, val) {
  $("#"+from).attr("class", "non-active");
  $("#"+to).attr("class", "active");
  $("#"+form).prop( "checked", val);
}

function notif(type, text, show) {
  $(".alert").removeClass("hide");
  $(".alert span").html(text);

  if(type==true) {
    $(".alert .notify-alert").removeClass("red-skin");
    $(".alert .notify-alert").addClass("green-skin");
    //$(".alert .notify-alert .notify-content .fa").removeClass("fa-close");
    //$(".alert .notify-alert .notify-content .fa").addClass("fa-check");
  } else {
    $(".alert .notify-alert").removeClass("green-skin");
    $(".alert .notify-alert").addClass("red-skin");
    //$(".alert .notify-alert .notify-content .fa").removeClass("fa-check");
    //$(".alert .notify-alert .notify-content .fa").addClass("fa-close");
  }

  setTimeout(function(){
    $(".alert").css({
      "top":"7%",
      "display":"block"
    });
  }, 10);

  if(show!=true) {
    setTimeout(function(){
      $(".alert").css({
        "top":"-30%"
      });
    },5000);

    setTimeout(function(){
      $(".alert").addClass("hide");
      $(".alert p").html("");
    },5300);
  }
  
}

function closeNotif() {
  setTimeout(function(){
    $(".alert").css({
      "top":"-30%"
    });
  },0);

  setTimeout(function(){
    $(".alert").addClass("hide");
    $(".alert p").html("");
  },10);
}



function redirect(link) {
  document.location.href=(link);
}


function resetValidation() {
  for (var i = 0, j = arguments.length; i < j; i++){
    $("#"+arguments[i]+"-group").removeClass("has-success");
    $("#"+arguments[i]+"-group").removeClass("has-error");
    $("#"+arguments[i]+"-group .help-block").empty();
  }
}

function formValidate() {
  var pos = ""; 
  var posSection = "";
  for (var i = 0, j = arguments.length; i < j; i++){
    if(i!=0) {
      if(arguments[i][1]) {
          $("#"+arguments[i][0]+"-group").removeClass("has-success");
          $("#"+arguments[i][0]+"-group").addClass("has-error");
          $("#"+arguments[i][0]+"-group .help-block").html(arguments[i][1]);
      } else {
          $("#"+arguments[i][0]+"-group").removeClass("has-error");

          if(arguments[i][2]==true) {
            $("#"+arguments[i][0]+"-group").addClass("has-success");
          }
          
          $("#"+arguments[i][0]+"-group .help-block").empty();
      }

      if(pos=="") {
        pos = $("#"+arguments[i][0]+"-group").position().top;
        posSection = arguments[i][0];
      } else if(parseInt($("#"+arguments[i][0]+"-group").position().top)<parseInt(pos)) {
        pos = $("#"+arguments[i][0]+"-group").position().top;
        posSection = arguments[i][0];
      }
    }

  }

  if(arguments[0]==true) {
    $('html, body').animate({
        scrollTop: $("#"+posSection+"-group").offset().top - 70
    }, 0);  
  }

}

function validateElementForm(name, remove, add, data, success) {
  $("#"+name+"-group").removeClass("has-"+remove);
  if(success==true) {    
    $("#"+name+"-group").addClass("has-"+add);  
  }

  $("#"+name+"-group .help-block").html(data);
}







 function loading(url) {
  try {
    var getUrl = document.URL;

    var splitUrl = getUrl.split("#");
    var URL = '#'+splitUrl[1];

    if(url!=URL) {
      $("#loading-progressBar").removeClass("hide");
    }
  } catch(err) {}
}

function loading_content(content, type) {
  if(type=="success") {
    $(content).removeClass("progress-content");
    $(content+" #loading-content").addClass("hide");
    $(content+" select").prop("disabled", false);
  } else if(type=="failed") {
    $(content).addClass("progress-content");
    $(content+" #loading-content").removeClass("hide");
    $(content+" #loading-content").removeClass("loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw");
    $(content+" #loading-content").addClass("reload-content fa fa-refresh");
    $(content+" select").prop("disabled", true);
    setTimeout(function(){
      $("body").focus();
    },100);
  } else if(type=="loading") {
    $(content).addClass("progress-content");
    $(content+" #loading-content").removeClass("hide");
    $(content+" #loading-content").removeClass("reload-content fa fa-refresh");
    $(content+" #loading-content").addClass("loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw");    
    $(content+" select").prop("disabled", true);
  }
  
}




function show_hide(id) {
  var section = $(id).css('display');
  if(section=="block") {
    $(id).addClass("hide");
  } else {    
    $(id).removeClass("hide");
  }
}





function step_progress(type) {
  if(type=="next") {
    if($("#step_1").css('display')!="none") {
      $("#previous-step").removeClass("hide");
      $("#step_1").addClass("hide");
      $("#step_2").removeClass("hide");
    } else if($("#step_2").css('display')!="none") {
      $("#step_2").addClass("hide");
      $("#step_3").removeClass("hide");
    } else if($("#step_3").css('display')!="none") {
      $("#step_3").addClass("hide");
      $("#step_4").removeClass("hide");
    } else if($("#step_4").css('display')!="none") {      
      $("#step_4").addClass("hide");
      $("#step_5").removeClass("hide");
    } else if($("#step_5").css('display')!="none") {
      $("#next-step").addClass("hide");
      $("#step_5").addClass("hide");
      $("#step_6").removeClass("hide");
    }
  }

  if(type=="previous") {
    if($("#step_2").css('display')!="none") {
      $("#previous-step").addClass("hide");
      $("#step_2").addClass("hide");
      $("#step_1").removeClass("hide");
    } else if($("#step_3").css('display')!="none") {
      $("#step_3").addClass("hide");
      $("#step_2").removeClass("hide");
    } else if($("#step_4").css('display')!="none") {
      $("#step_4").addClass("hide");
      $("#step_3").removeClass("hide");
    } else if($("#step_5").css('display')!="none") {
      $("#step_5").addClass("hide");
      $("#step_4").removeClass("hide");
    } else if($("#step_6").css('display')!="none") {
      $("#next-step").removeClass("hide");
      $("#step_6").addClass("hide");
      $("#step_5").removeClass("hide");
    }
  }
}





function back() {
  history.back(1);
}





function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function br2nl(str) {
  if(str!=null) {
    return str.replace(/<br\s*\/?>/mg,"");
  } else {
    return "";
  }
}



function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}




function hiddenChar(name)
{
    var type = $('#'+name+'-group input[name='+name+']').attr('type');

    if(type=='password') {
      $('#'+name+'-group input[name='+name+']').attr('type','text');
      $('#'+name+'-group .hidden-char').removeClass('ion-eye-disabled');
      $('#'+name+'-group .hidden-char').addClass('ion-eye');
    } else {
      $('#'+name+'-group input[name='+name+']').attr('type','password');
      $('#'+name+'-group .hidden-char').removeClass('ion-eye');
      $('#'+name+'-group .hidden-char').addClass('ion-eye-disabled');
    }
}

/**
 * 
 * @param {Array} itemUnits
 * @returns {base: String, ratio: {[unit]: number}} || null 
 */
function getUnitRatio(itemUnits) {
  
  if (!itemUnits || !itemUnits.length) {
    return null;
  }

  const reverseUnit = itemUnits.reverse();
  const length = reverseUnit.length;

  const baseUnit = itemUnits[0].name;

  if(length === 1) {
    return {
      base: baseUnit,
      ratio: {
        [baseUnit] : 1
      }
    }
  }

  const ratio = reverseUnit.reduce(function (acc, cur, i, arr) {

    if(i === 0) {
      acc[arr[i+1].name] = 1 / cur.amount_ratio;
    } else if(i + 1 < length) {
      acc[arr[i+1].name] = acc[cur.name] / cur.amount_ratio;
    } 

    return acc;
  }, {
    [baseUnit]: 1
  });

  return {
    base: baseUnit,
    ratio
  }
}






  