
function pages() {
    $(".pagination").empty();

    
    var number = arguments[0];
    var total = arguments[1];
    var per_page = arguments[2];
    var current_page = arguments[3];
    var last_page = arguments[4];
    var from = arguments[5];
    var to = arguments[6];
    var search = arguments[7];

    var filter = '';
    for (var i = 8, j = arguments.length; i < j; i++){
        filter = filter+',"'+arguments[i]+'"';
    }

    if(total!=0) {

        if(number==1) { 
            $(".pagination").append('<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>');
        } else { 
            $(".pagination").append('<li onclick=\'search("1","false","'+search+'"'+filter+')\'><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>');
        }
    
        var maxPage = 3;
    
        var start = 1;
        var end = maxPage;
    
        if(current_page>=maxPage) {
            start = current_page-1;
                
            if(start<1) {
                start = 1;
            }
    
            end = start+(maxPage-1);
        }
    
        if(end>=last_page) {
            if(last_page!=1) {
                start=last_page-(maxPage-1);    
            }
    
            if(start<1) {
                start = 1;
            }
    
            end=last_page;
        }
    
    
        for (var i = start; i <= end; i++) {
            var activePage = '';
            if(number==i) { activePage='active'; }
    
            $(".pagination").append('<li class="'+activePage+'" onclick=\'search("'+i+'","false","'+search+'"'+filter+')\'><a>'+i+'</a></li>');
        }
    
        if(number==last_page) {  
            $(".pagination").append('<li class="disabled"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>');
        } else {
            $(".pagination").append('<li onclick=\'search("'+last_page+'","false","'+search+'"'+filter+')\'><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>');
        }

    }
}

