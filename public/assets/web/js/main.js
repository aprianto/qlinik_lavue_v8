function show_hide(id) {
	var section = $(id).css("display");
	if(section=="block") {
		$(id).css("display","none");
	} else {    
		$(id).css("display","block");
	}
}

function show_hide_class(id, style) {
	var section = $(id).css("display");

	if(section=="none") {
		$(id).addClass(style);
	} else {    
		$(id).removeClass(style);
	}
}


$(window).scroll(function() {
	if ($(document).scrollTop() >= 55) {
		$("header").removeClass("color");
		$("header").addClass("shadow");
		$("#logo-white").addClass("hide");
		$("#logo-color").removeClass("hide");
	} else {
		$("header").addClass("color");
		$("header").removeClass("shadow");
		$("#logo-white").removeClass("hide");
		$("#logo-color").addClass("hide");
	}

	if ($(document).scrollTop() >= 400) {
		$(".navigation").css("right","-30px");
	} else {
		$(".navigation").css("right","-100px");
	}
});


function feature(value, icon) {
	$("#feature li").removeClass("active");
	$("#icon-feature i").addClass("hide");
	$("#icon-feature img").addClass("hide");
	$(value).addClass("active");
	$(icon).removeClass("hide");
}

function info(tab, slide, bg) {
	$("#tab-info li").removeClass("active");
	$("#info .slide").addClass("hide");
	$("section.section-info-tab .content").css("background","url('"+bg+"') no-repeat center center fixed");
	$(tab).addClass("active");
	$(slide).removeClass("hide");
}

function redirect(link) {
 	document.location.href=(link);
}

function scroll_top() {
    $('html, body').animate({
        scrollTop: 0
    }, 1500);
}

function scroll(section) {
	$('html, body').animate({
        scrollTop: $(section).offset().top - 60
    }, 1500);
}

function number(e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		(e.keyCode == 65 && e.ctrlKey === true) ||
		(e.keyCode == 67 && e.ctrlKey === true) ||
		(e.keyCode == 88 && e.ctrlKey === true) ||
		(e.keyCode >= 35 && e.keyCode <= 39)) {
		return;
	}

	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
}

function notif(type, text, show) {
	$("#alert").removeClass("hide");
	$("#alert p").html(text);

	if(type==true) {
		$("#alert .notify-alert").removeClass("red-skin");
		$("#alert .notify-alert").addClass("green-skin");
		$("#alert .notify-alert .notify-content .fa").removeClass("fa-close");
		$("#alert .notify-alert .notify-content .fa").addClass("fa-check");
	} else {
		$("#alert .notify-alert").removeClass("green-skin");
		$("#alert .notify-alert").addClass("red-skin");
		$("#alert .notify-alert .notify-content .fa").removeClass("fa-check");
		$("#alert .notify-alert .notify-content .fa").addClass("fa-close");
	}

	setTimeout(function(){
		$("#alert").css({
			"top":"2%",
			"display":"block"
		});
	}, 10);

	if(show!=true) {
		setTimeout(function(){
			$(".alert").css({
				"top":"-30%"
			});
		},5000);

		setTimeout(function(){
			$(".alert").addClass("hide");
			$(".alert p").html("");
		},5300);
	}
}

function resetValidation() {
  for (var i = 0, j = arguments.length; i < j; i++){
    $("#"+arguments[i]+"-group").removeClass("has-success");
    $("#"+arguments[i]+"-group").removeClass("has-error");
    $("#"+arguments[i]+"-group .help-block").empty();
  }
}

function formValidate() {
	var pos = ""; 
	var posSection = "";
	for (var i = 0, j = arguments.length; i < j; i++){
		if(i!=0) {
			if(arguments[i][1]) {
				$("#"+arguments[i][0]+"-group").removeClass("has-success");
				$("#"+arguments[i][0]+"-group").addClass("has-error");
				$("#"+arguments[i][0]+"-group .help-block").html(arguments[i][1]);
			} else {
				$("#"+arguments[i][0]+"-group").removeClass("has-error");

				if(arguments[i][2]==true) {
					$("#"+arguments[i][0]+"-group").addClass("has-success");
				}

				$("#"+arguments[i][0]+"-group .help-block").empty();
			}

			if(pos=="") {
				pos = $("#"+arguments[i][0]+"-group").position().top;
				posSection = arguments[i][0];
			} else if(parseInt($("#"+arguments[i][0]+"-group").position().top)<parseInt(pos)) {
				pos = $("#"+arguments[i][0]+"-group").position().top;
				posSection = arguments[i][0];
			}
		}

	}

	if(arguments[0]==true) {
		$('html, body').animate({
			scrollTop: $("#"+posSection+"-group").offset().top - 170
		}, 0);  
	}

}

function loading_content(content, type) {
	if(type=="success") {
		$(content).removeClass("progress-content");
		$(content+" #loading-content").addClass("hide");
		$(content+" select").prop("disabled", false);
	} else if(type=="failed") {
		$(content).addClass("progress-content");
		$(content+" #loading-content").removeClass("hide");
		$(content+" #loading-content").removeClass("loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw");
		$(content+" #loading-content").addClass("reload-content fa fa-refresh");
		$(content+" select").prop("disabled", true);
	} else if(type=="loading") {
		$(content).addClass("progress-content");
		$(content+" #loading-content").removeClass("hide");
		$(content+" #loading-content").removeClass("reload-content fa fa-refresh");
		$(content+" #loading-content").addClass("loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw");    
		$(content+" select").prop("disabled", true);
	}
}

function switchButton(from, to, form, val) {
	$(from).attr("class", "non-active");
	$(to).attr("class", "active");
	$(form).prop( "checked", val);
}

function hideScale(section, type) {
	if(type=="show") {
		setTimeout(function(){
			$(section).removeClass("hide");
		}, 0);

		setTimeout(function(){
			$(section).removeClass("hide-scale");
		}, 500);
	} else {
		setTimeout(function(){
			$(section).addClass("hide-scale");
		}, 0);
		
		setTimeout(function(){
			$(section).addClass("hide");
		}, 500);
	}
	
}


