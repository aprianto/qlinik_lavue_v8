/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap'); 
window.Vue = require('vue');  
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'

Vue.use(VueRouter)
Vue.use(VueAxios, axios)  

const routes =[
{
    name :'home',
    path :'/home'
    
}]

const app = new Vue({
    el: '#app',
});
