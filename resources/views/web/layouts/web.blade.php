<?php

if(empty($title)) {
    $title="Medigo. - Aplikasi Klinik Online - Klinik Moderen Dimulai Dari Sini.";
}

if(empty($description)) {
    $description = "Aplikasi klinik modern untuk kemudahan manajemen klinik, pendaftaran pasien, rekam medis digital, dan administrasi keuangan.";
}

if(empty($keywords)) {
    $keywords = "aplikasi klinik, software klinik online.";
}

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>{{ $title }}</title>
<meta name="Description" content="{{ $description }}">
<meta name="Keywords" content="{{ $keywords }}">
<link rel="icon" href="{{ $storage }}/images/icons/favicon.png" />
<link rel="shortcut icon" href="{{ $storage }}/images/icons/favicon.png" />

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/web/css/bootstrap.min.css') }}" />
<link href="{{ asset('assets/web/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">

<link href="{{ asset('assets/web/css/styles.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/web/css/main.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/web/css/responsive.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/web/css/fonts.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/web/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/web/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{ asset('assets/web/select2/dist/css/select2.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/web/select2/dist/css/select2.css') }}"/>

<link rel="stylesheet" href="{{ asset('assets/web/css/wSelect.css') }}" />

<link href="{{ asset('assets/web/css/jquery-ui.css') }}" rel="stylesheet" type="text/css">

</head>
<body>



<div class="modal modal-profile fade" id="profile" role="dialog" aria-labelledby="profileTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div id="section-header">
                </div>
            </div>
            <div class="modal-body hide" id="loading-profile">
                <div class="loading-sec">
                    <span class="loading fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-profile">
                <div class="loading-sec">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <div class="modal-body" id="section-profile">
            </div>
        </div>
    </div>
</div>

<div class="modal modal-form fade" id="search-patient" role="dialog" aria-labelledby="searchTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1>{{ trans("messages_web.search_patient") }}</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-search-patient">
                <div class="modal-body">
                    <div class="alert hide" id="section-alert">
                        <i class="fa fa-info-circle"></i> <span id="alert-search-patient"></span>
                        <span class="remove ion-close-round" onclick="$(this).parent().addClass('hide')"></span>
                    </div>
                    <div class="form-group" id="name-group">
                        <div class="left">
                            <i class="fa fa-search"></i>
                        </div>
                        <div class="right">
                            <div class="form-group" id="q-group">
                                <label>{{ trans("messages_web.mrn_email_phone") }}</label>
                                <input type="text" name="q" autocomplete="off">
                                <span class="help-block"></span>
                            </div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">                   
                    <button type="button" class="btn-secondary" data-dismiss="modal">{{ trans("messages_web.cancel") }}</button>
                    <button class="btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans("messages_web.search") }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>



<div id="alert" class="hide">
    <div class="notify-alert">
        <button type="button" class="close" aria-hidden="true" onclick="$(this).parent().parent().css({'top':'-30%'});$(this).parent().parent().addClass('hide');"><i class="ion-close-round"></i></button>
        <div class="notify-content">
            <i class="fa fa-check-circle"></i>
            <p>Notif</p>
        </div>
    </div>
</div>

@include('web.layouts.header')


<script src="{{ asset('assets/web/js/jquery-2.1.3.js') }}"></script>
<script src="{{ asset('assets/web/js/main.js') }}"></script>
<script src="{{ asset('assets/web/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('assets/web/js/moment-with-locales.js') }}"></script>
<script src="{{ asset('assets/web/js/bootstrap-datetimepicker.js') }}"></script>

<script src="{{ asset('assets/web/js/wSelect.min.js') }}"></script>

@include('web.library.select2')


<div class="container">
    @yield('content')

    @include('web.layouts.footer')
</div>

<a class="navigation" onclick="scroll_top()">
    <i class="fa fa-chevron-up"></i>
</a>






<script type="text/javascript">

$("select[name='language']").on("change", function() {
    var lang = this.value;
    redirect("{{ url('/') }}/language/web/"+lang);
});









$('#profile').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).attr('data-id');
    var type = $(e.relatedTarget).attr('data-type');
    profile(id, type);    
});

function profile(id, type) {
    $('#loading-profile').removeClass('hide');
    $("#reload-profile").addClass("hide");
    $('#section-header').html("");
    $('#section-profile').addClass('hide');
    $('#section-profile').html("");
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/"+type+"/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading-profile').addClass('hide');

            if(!data.error) {

                if(type=="clinic") {

                    var polyclinic_clinic = "";
                    for(var j=0; j<data.polyclinic.length; j++) {
                        polyclinic_clinic = polyclinic_clinic+'<span class="label">'+data.polyclinic[j].name+'</span>';
                    }
                    
                    $('#section-profile').removeClass('hide');
                    $('#section-header').html('<div class="canvas">'+
                            thumbnail('md','clinic','{{ $storage }}',data.logo,data.name)+
                        '</div>'+
                        '<h1>'+data.name+'</h1>');
                    $('#section-profile').html('<div class="header">'+
                            '<h1>{{ trans("messages_web.information") }}</h1>'+
                        '</div>'+
                        '<div class="info">'+
                            '<div class="text">'+
                                '<span class="fa fa-phone"></span>'+
                                '<p>'+nullToEmpty(data.phone)+'</p>'+
                            '</div>'+
                            '<div class="text">'+
                                '<span class="fa fa-envelope-o"></span>'+
                                '<p>'+nullToEmpty(data.email)+'</p>'+
                            '</div>'+
                            '<div class="text">'+
                                '<span class="fa fa-map-marker"></span>'+
                                '<p>'+nullToEmpty(data.address)+' '+nullToEmpty(data.district.name)+' '+nullToEmpty(data.regency.name)+' '+nullToEmpty(data.province.name)+' '+nullToEmpty(data.country.name)+'</p>'+
                            '</div>'+
                        '</div>'+
                        '<div class="header">'+
                            '<h1>{{ trans("messages_web.poly") }}</h1>'+
                        '</div>'+
                        '<div class="info">'+
                            ''+polyclinic_clinic+''+
                        '</div>');
                } else if(type=="doctor") {
                    var polyclinic_doctor = "";
                    for(var j=0; j<data.polyclinic.length; j++) {
                        polyclinic_doctor = polyclinic_doctor+'<span class="label">'+data.polyclinic[j].name+'</span>';
                    }
                    
                    $('#section-profile').removeClass('hide');
                    $('#section-header').html('<div class="canvas">'+
                            thumbnail('md','doctor','{{ $storage }}',data.photo,data.name)+
                        '</div>'+
                        '<h1>'+data.name+'</h1>');
                    $('#section-profile').html('<div class="header">'+
                            '<h1>{{ trans("messages_web.information") }}</h1>'+
                        '</div>'+
                        '<div class="info">'+
                            '<div class="text">'+
                                '<span class="fa fa-phone"></span>'+
                                '<p>+'+data.phone_code+''+data.phone+'</p>'+
                            '</div>'+
                            '<div class="text">'+
                                '<span class="fa fa-envelope-o"></span>'+
                                '<p>'+data.email+'</p>'+
                            '</div>'+
                        '</div>'+
                        '<div class="header">'+
                            '<h1>{{ trans("messages_web.poly") }}</h1>'+
                        '</div>'+
                        '<div class="info">'+
                            ''+polyclinic_doctor+''+
                        '</div>');
                } else {
                    $('#section-profile').removeClass('hide');
                    $('#section-header').html('<div class="canvas">'+
                            thumbnail('md','patient','{{ $storage }}',data.photo,data.name)+
                        '</div>'+
                        '<h1>'+data.name+'</h1>');
                    $('#section-profile').html('<div class="header">'+
                            '<h1>{{ trans("messages_web.information") }}</h1>'+
                        '</div>'+
                        '<div class="info">'+
                            '<div class="text">'+
                                '<span class="fa fa-phone"></span>'+
                                '<p>+'+data.phone_code+''+data.phone+'</p>'+
                            '</div>'+
                            '<div class="text">'+
                                '<span class="fa fa-envelope-o"></span>'+
                                '<p>'+nullToEmpty(data.email)+'</p>'+
                            '</div>'+
                        '</div>');
                }
                
            } else {
                $("#reload-profile").removeClass("hide");

                $("#reload-profile .reload").click(function(){ profile(id, type); });
            }

        },
        error: function(){
            $('#loading-profile').addClass('hide');
            $("#reload-profile").removeClass("hide");

            $("#reload-profile .reload").click(function(){ profile(id, type); });
        }
    })
}



$.ajaxSetup({
    headers: {
        "Authorization": "Bearer {{ $api_token }}"
    }
});



</script>


</body>
</html>
