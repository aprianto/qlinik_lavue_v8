<header class="color">
    <div class="bottom">
        <div class="content">
            <div class="column">
                <a href="{{ url('/') }}" class="logo">
                    <img src="{{ $storage }}/images/icons/logo.png" id="logo-white" />
                    <img src="{{ $storage }}/images/icons/logo.png" id="logo-color" class="hide" />
                </a>
            </div>
            <div class="column full-width">             
                <i class="menu ion-navicon-round" onclick="show_hide_class('#nav','show-nav')"></i>
                <nav id="nav">
                    <a class="close" onclick="show_hide_class('#nav','show-nav')"><i class="fa fa-arrow-left"></i> {{ trans("messages_web.close_menu") }}</a>
                    <ul>
                        <li><a href="{{ url('/') }}/{{ $lang }}/search">{{ trans("messages_web.find_clinic") }}</a></li>
                        <li><a href="{{ $doctor_url }}">{{ trans("messages.doctor") }}</a></li>
                        <li><a href="{{ $app_url }}/#/{{ $lang }}/register">{{ trans("messages.register") }}</a></li>
                        <li><a href="{{ $app_url }}/#/{{ $lang }}/login">{{ trans("messages.login") }}</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
