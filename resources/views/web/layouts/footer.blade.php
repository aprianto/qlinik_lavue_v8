<footer>
    <div class="language">
        <i class="ion-android-globe"></i>
        <select id="language" name="language">
            <option value="id" @if($lang=="id") selected="selected"  @endif >Indonesia</option>
            <option value="en" @if($lang=="en") selected="selected"  @endif >English</option>
        </select>
    </div>
    <div class="bottom">
        ©2020 Medigo
    </div>
</footer>
