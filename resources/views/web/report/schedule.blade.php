@extends('web.layouts.web')

@section('content')
<section class="section-report">
    <div class="report">
        <div class="content">
            <div class="print" id="print-report">





<html>
<head>
<style type="text/css">

@page {
    size: A5;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0; 
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .header-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #24e2cf;
    height: 30px;
    z-index: 20;
}

#print-report .header-report .before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    background-color: #24e2cf;
    width: 50%;
    height: 30px;
}

#print-report .header-report .after {
    content: "";
    position: absolute;
    top: 0px;
    height: 100%;
    right: 50%;
    background-color: #ffffff;
    width: 30px;
    height: 60px;
    -webkit-transform: skew(-30deg);
    -moz-transform: skew(-30deg);
    -o-transform: skew(-30deg);
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    margin-top: 35px;
    margin-bottom: 30px;
    z-index: 50;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report .table-report {
    display: table;
    width: 100%;
}

#print-report .body-report .table-report .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .table-report .row .left {
    display: table-cell;
    padding: 10px 10px 10px 0;
    vertical-align: top;
    width: 10px;
}

#print-report .body-report .table-report .row .left .logo-report {
    position: relative;
    float: none;
    margin: 0 0;
    padding: 5px 0px 5px 0px;
    text-align: right;
}

#print-report .body-report .table-report .row .left .logo-report img {
    max-height: 100px;
    max-width: 100px;
}

#print-report .body-report .table-report .row .left .logo-report .photo-image {
    margin: 0 auto;
    border-radius: 50px;
    -webkit-border-radius: 50px;
    -moz-border-radius: 50px;
    -ms-border-radius: 50px;
    -o-border-radius: 50px;
    background-color: #85c636;
    color: #ffffff;
    font-weight: normal;
    vertical-align: middle !important;
    text-align: center;
    font-size: 75px;
    padding-top: 0px !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
}

#print-report .body-report .table-report .row .right {
    display: table-cell;
    padding: 10px 10px;
    vertical-align: top;
}


#print-report .body-report h1.title-report {
    margin: 0 0 10px 0;
    padding: 0 0;
    color: #1ab1e5;
    font-size: 30px;
    vertical-align: top;
    text-align: left;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    text-align: center;
    padding: 0 0;
}

#print-report .body-report h1 {
    position: relative;
    float: none;
    width: 100%;
    margin: 2px 0;
    color: #1ab1e5;
    font-size: 50px;
    vertical-align: top;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report h3 {
    position: relative;
    float: none;
    width: 100%;
    margin: 1px 0;
    color: #1ab1e5;
    font-size: 18px;
    vertical-align: top;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report p {
    position: relative;
    float: none;
    margin: 5px 0;
    width: 100%;
    font-size: 14px;
    vertical-align: top;
    color: #000000;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report .top-report {
    position: relative;
    float: none;
    width: 100%;
    text-align: left;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 20px 30px 20px 30px;
    border-bottom: 1px solid #b3babe;
}

#print-report .body-report .bottom-report {
    position: relative;
    float: none;
    width: 100%;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 20px 30px 20px 30px;
}

#print-report .body-report .bottom-report .text-report {
    position: relative;
    float: none;
    width: 100%;
    text-align: center;
    margin: 0 0 20px 0;
}

#print-report .footer-report {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: right;
    font-size: 13px;
    color: #000000;
    padding: 5px 10px;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-top: 1px solid #b3babe;
}

#print-report .footer-report .logo-report {
    text-align: left;
    color: #000000;
    font-size: 13px;
    margin: 0 0;
    padding: 0 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    font-weight: bold;
}

#print-report .footer-report .logo-report span {
    color: #1ab1e5;
}

#print-report .footer-report .footer-logo {
    max-height: 12px;
    margin-left: 3px;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="header-report"><div class="before"></div><div class="after"></div></div>
    <div class="body-report">
        <h1 class="title-report">{{ trans("messages.proof_registration") }}</h1>
        <div class="top-report">
            <div class="table-report">
                <div class="row">
                    <div class="left">
                        <div class="logo-report">
                            <?php echo thumbnail('lg','clinic',$storage,$schedule->clinic->logo,$schedule->clinic->name); ?>
                        </div>
                    </div>
                    <div class="right">
                        <h3>{{ $schedule->clinic->name }}</h3>
                        <p>{{ $schedule->clinic->address }} {{ $schedule->clinic->district }} {{ $schedule->clinic->regency }} {{ $schedule->clinic->province }} {{ $schedule->clinic->country }}</p>
                        <p>{{ $schedule->clinic->phone }}</p>
                        <p>{{ $schedule->clinic->email }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-report">
            <div class="text-report">
                <h3>{{ $schedule->patient->name }}</h3>
                <p>{{ $schedule->patient->mrn }}</p>
            </div>
            <div class="text-report">
                <h3>{{ $schedule->doctor->name }}</h3>
                <p>{{ $schedule->polyclinic->name }}</p>
            </div>
            <div class="text-report">
                <?php

                $day = date('N', strtotime($schedule->date));

                ?>
                @if($schedule->appointment==null)
                <h3>{{ getDay($day) }}, {{ getFormatDate($schedule->date) }} ({{ convertTime($schedule->start_time) }} - {{ convertTime($schedule->end_time) }})</h3>
                <p>{{ trans("messages.serial_number") }}</p>
                <h1>#{{ $schedule->number }}</h1>
                <p>{{ trans("messages.estimate_start_time") }}: {{ convertTime($schedule->estimate) }}</p>
                @else
                <h3>{{ getDay($day) }}, {{ getFormatDate($schedule->date) }} ({{ convertTime($schedule->start_time) }})</h3>
                <p>{{ trans("messages.status") }}: <?php echo confirmation_text($schedule->status->code); ?></p>
                @endif
            </div>
        </div>        
    </div>
    <div class="footer-report">
        {{ trans("messages.powered_by") }} <img class="footer-logo" src="{{ $storage }}/images/icons/logo.png" />
    </div>
</div>

</body>
</html>



            </div>
        </div>          
    </div>
    <div class="btn-sec">
        <a class="button" id="print"><i class="fa fa-print"></i> {{ trans("messages.print") }}</a>
        <a href="{{ $docs_url }}/{{ $lang }}/download/schedule/{{ $id }}" class="button"><i class="fa fa-file-pdf-o"></i> {{ trans("messages.download") }}</a>
    </div>
</section>

<script type="text/javascript">
$("#link-contact_us").attr("href","{{ url('/') }}/{{ $lang }}/contact_us");



$("#print").click(function() {
    var print = $("#print-report").html();
    var divToPrint=$('#print-report').html();
    var newWin=window.open('','Print-Window'); 
    newWin.document.open();
    newWin.document.write(divToPrint+'<script>window.print()<\/script>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
});


</script>
@endsection