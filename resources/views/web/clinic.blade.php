@extends('web.layouts.web')

@section('content')

@include('web.function.function')

<section class="profile-sec">
    <div class="content">
        <div class="banner">
            <div class="shape"></div>
            <div class="button-sec">
                <a onclick="scroll('#action')" class="button">{{ trans("messages_web.register_schedule") }}</a>
            </div>
            <div class="info">
                <div class="canvas">
                    <?php echo thumbnail('lg','clinic',$storage,$clinic->logo,$clinic->name); ?>
                </div>
                <div class="text">
                    <h1>{{ $clinic->name }}</h1>
                    <p><i class="fa fa-map-marker"></i> {{ $clinic->regency->name }}</p>
                </div>
            </div>
        </div>
        <div class="info-sec">
            <div class="header">
                <h1>{{ trans("messages_web.information") }}</h1>
            </div>
            <div class="info">
                <div class="text">
                    <span class="fa fa-phone"></span>
                    <p>{{ $clinic->phone }}</p>
                </div>
                <div class="text">
                    <span class="fa fa-envelope-o"></span>
                    <p>{{ $clinic->email }}</p>
                </div>
                <div class="text">
                    <span class="fa fa-map-marker"></span>
                    <p>{{ $clinic->address }} {{ $clinic->district->name }} {{ $clinic->regency->name }} {{ $clinic->province->name }} {{ $clinic->country->name }}</p>
                </div>
            </div>
        </div>
        <div class="info-sec">
            <div class="header">
                <h1>{{ trans("messages_web.poly") }}</h1>
            </div>
            <div class="info">
                @foreach($clinic->polyclinic as $key => $value)
                <span class="list">{{ $value->name }}</span>
                @endforeach                
            </div>
        </div>
        <div class="action-sec" id="action">
            <div class="header-search">
                <form id="form-search">
                    <div class="row" id="search-clinic">
                        <div class="col-sm-5">
                            <select id="polyclinic" name="polyclinic">
                                <option value="">{{ trans("messages_web.all_poly") }}</option>
                                @foreach($clinic->polyclinic as $key => $value)
                                <option value="{{ $value->id }}" @if($id_polyclinic==$value->id) selected="selected"; @endif >{{ $value->name }}</option>
                                @endforeach 
                            </select>
                        </div>        
                        <div class="col-sm-7">
                            <div class="search-box">
                                <input type="text" placeholder="{{ trans('messages_web.search') }}" id="q" name="q" />
                                <button class="icon-search"><i class="fa fa-search"></i></button>
                            </div>            
                        </div>
                    </div>
                </form>
            </div>
            <div class="info">
                <h2>{{ trans("messages_web.select_doctor") }}</h2>
                <span id="loading-search" class="loading-search fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                <span id="reload-search" class="reload-search fa fa-refresh hide"></span>
                <div class="search" id="search">                    
                </div>
                <div class="pagination-sec">
                    <ul class="pagination" id="page">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script type="text/javascript">


$.ajaxSetup({
    headers: {
        "Authorization": "Bearer {{ $api_token }}"
    }
});


$("#link-contact_us").attr("href","{{ url('/') }}/{{ $lang }}/contact_us");

$("#form-search select[name='polyclinic']").select2();


var keySearch = "";
var keyPoly = "";
var numPage = 1;

function search(page, submit, q, polyclinic) {
    $("#content-clinic").addClass("hide");
    $("#search").html(""); 
    $("#search").addClass("hide");       
    $("#loading-search").removeClass("hide");
    $("#reload-search").addClass("hide");
    $("#page").html(""); 

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/{{ $id }}/doctor?q="+q+"&id_polyclinic="+polyclinic+"&active=true&page="+page+"&per_page=10",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#search").html(""); 
            $("#loading-search").addClass("hide");

            if(!data.error) {
                $("#search").removeClass("hide");


                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    var item = data.data;

                    for (var i = 0; i < item.length; i++) {

                        var schedule = "";
                        if(item[i].schedule!=null) {
                            if(item[i].schedule.daily!=null) {
                                for (var j=0; j < item[i].schedule.daily.length; j++) {
                                    schedule += '<br /><i class="fa fa-clock-o"></i> <span class="schedule">'+getDay(item[i].schedule.daily[j].day)+' ('+convertTime(item[i].schedule.daily[j].start_time)+' - '+convertTime(item[i].schedule.daily[j].end_time)+')</span>';
                                }
                            }
                        }

                        $("#search").append('<div class="listview">'+
                            '<div class="canvas">'+
                               thumbnail('md','doctor','{{ $storage }}',item[i].photo,item[i].name)+
                            '</div>'+
                            '<p>'+
                                '<a data-id="'+item[i].id+'" data-type="doctor" data-toggle="modal" data-target="#profile">'+item[i].name+'</a>'+
                                '<br />'+
                                '<span>'+item[i].polyclinic.name+'</span>'+
                                schedule+
                            '</p>'+
                            '<div class="button-sec">'+
                                '<a href=\'{{ url("/") }}/{{ $lang }}/schedule/clinic/{{ $id }}/doctor/'+item[i].id+'/polyclinic/'+item[i].polyclinic.id+'\' class="button">{{ trans("messages_web.select") }}</a>'+
                            '</div>'+
                        '</div>');
                    }

                } else {
                    $("#search").html('<div id="error-search">'+
                        '<i class="icon fa fa-exclamation-triangle"></i>'+
                        '<h1 class="error">{{ trans("messages_web.no_result_found") }}</h1>'+
                        '<p class="error">{{ trans("messages_web.info_no_result") }}</p>'+
                    '</div>');
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, polyclinic);

            } else {
                $("#reload-search").removeClass("hide");  
                $("#reload-search").click(function(){ search(page,"false", q, polyclinic); }); 
            }
        },
        error: function(){
            $("#loading-search").addClass("hide");
            $("#reload-search").removeClass("hide");

            $("#reload-search").click(function(){ search(page,"false", q, polyclinic); });
        }
    })
}

$("#form-search").submit(function(event) {
    event.preventDefault();

    var q = $("#form-search input[name='q']").val();
    var polyclinic = $("#form-search select[name='polyclinic'] option:selected").val();

    search(1, 'true', q, polyclinic);
});

search(1, 'false', '', '{{ $id_polyclinic }}');

</script>
@endsection	