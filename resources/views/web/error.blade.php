<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Error</title>
<link rel="icon" href="{{ $storage }}/images/icons/favicon.png" />
<link rel="shortcut icon" href="{{ $storage }}/images/icons/favicon.png" />

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/css/bootstrap.min.css') }}" />
<link href="{{ asset('assets/css/main.css') }}" rel="stylesheet" type="text/css">

</head>
<body>

<div class="error">
	<h1>404</h1>
	<h2>Error</h2>
	<h3>We can't find the page you're looking for.</h3>
	<p>Return to the <a href="{{ url('/') }}">home</a> page.</p>
</div>

</body>
</html>