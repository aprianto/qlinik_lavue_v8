@extends('web.layouts.web')

@section('content')

@include('web.function.function')

<input type="hidden" name="latitude" />
<input type="hidden" name="longitude" />

<section class="search-sec">
    <div class="header-search">
        <form id="form-search">
            <div id="search-clinic">
                <div class="row">
                    <div class="col-sm-3">
                        <select id="polyclinic" name="polyclinic">   
                            <option value="">{{ trans("messages_web.all_poly") }}</option>
                            @foreach($polyclinic->data as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>        
                    <div class="col-sm-7">
                        <div class="search-box">
                            <input type="text" placeholder="{{ trans('messages_web.search') }}" id="q" name="q" autocomplete="off" />
                            <button class="icon-search"><i class="fa fa-search"></i></button>
                        </div>            
                    </div>
                    <div class="col-sm-2">
                        <div class="switch">
                            <input type="checkbox" id="search_type" name="search_type" class="hide" checked="true" />
                            <div id="search_clinic" class="active" onclick="switchButton('#search_doctor', '#search_clinic','#search_type',true)">{{ trans("messages_web.clinic") }}</div>
                            <div id="search_doctor" class="non-active" onclick="switchButton('#search_clinic', '#search_doctor','#search_type',false)">{{ trans("messages_web.doctor") }}</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-4 small-select">
                        <select id="province" name="province" class="">   
                            <option value="">{{ trans("messages_web.all_province") }}</option>
                            @foreach($province->data as $key => $value)
                            <option value="{{ ucwords(strtolower($value->name)) }}">{{ ucwords(strtolower($value->name)) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3 small-select">
                        <select id="regency" name="regency">   
                            <option value="">{{ trans("messages_web.all_regency") }}</option>
                            @foreach($regency->data as $key => $value)
                            <option value="{{ ucwords(strtolower($value->name)) }}">{{ ucwords(strtolower($value->name)) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-1">
                    </div>
                </div>
            </div>
        </form>
        <div id="form-search-location">
            <div class="row hide" id="search-location">
                <div class="col-md-12">
                    <div class="search-box">
                        <input type="text" placeholder="{{ trans('messages_web.search_location') }}" name="q" id="q" autocomplete="off" />
                        <button class="icon-search"><i class="fa fa-search"></i></button>
                    </div>            
                </div>
            </div>
        </div>
        <a id="btn-search-location" class="search-type search-location" onclick="search_type('location')"><i class="fa fa-map-marker"></i></a>
        <a id="btn-search-clinic" class="search-type hide-left" onclick="search_type('clinic')"><i class="fa fa-search"></i></a>
    </div>
    <div class="content-search">
        <span id="loading-search" class="loading-search fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
        <span id="reload-search" class="reload-search fa fa-refresh hide"></span>
        <div class="alert hide" id="section-alert-location">
            <span id="alert-location"></span>
            <span class="close ion-close-round" onclick="$(this).parent().addClass('hide')"></span>
        </div>
        <div class="search hide" id="search">
        </div>
        <div id="map" class="hide"></div>
        <div class="pagination-sec">
            <ul class="pagination" id="page">
            </ul>
        </div>
        <div id="content-clinic">
            <i class="icon fa fa-search"></i>
            <h1 class="info">{{ trans("messages_web.title_info_search") }}</h1>
            <p class="info">{{ trans("messages_web.info_search") }}</p>
        </div>
    </div>
    <div id="script"></div>
</section>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script type="text/javascript">

$("#form-search select[name='regency']").select2();

$("#form-search select[name='province']").select2();

function search_type(type) {
    if(type=="clinic") {
        $("#btn-search-clinic").addClass("hide-left");
        $("#btn-search-location").removeClass("hide-left");
        $("#search-clinic").removeClass("hide");
        $("#search-location").addClass("hide");
        $("#search").html("");
        $("#search").addClass("hide");
        $("#content-clinic").removeClass("hide");
        $("#script").html("");
        $("#map").addClass("hide");
        $("#section-alert-location").addClass("hide");
        $("#alert-location").html("");

        $("#form-search input[name='q']").val('');
        $("#form-search select[name='polyclinic']").val('').trigger('change');
        $("#form-search select[name='province']").val('').trigger('change');
        $("#form-search select[name='regency']").val('').trigger('change');
        switchButton('#search_doctor', '#search_clinic','#search_type',true);

        ChangeUrl('', '?');

    } else {
        $("#btn-search-clinic").removeClass("hide-left");
        $("#btn-search-location").addClass("hide-left");
        $("#search-clinic").addClass("hide");
        $("#search-location").removeClass("hide");
        $("#search").html("");
        $("#search").addClass("hide");
        $("#content-clinic").addClass("hide");
        $("#script").html('<script src="https://maps.googleapis.com/maps/api/js?key={{ $map_key }}&callback=map" async defer><\/script>');

        $("#form-search-location input[name='q']").val('');

        ChangeUrl('', '?type=map');
    }

    $("#page").html("");
}

$("#link-contact_us").attr("href","{{ url('/') }}/{{ $lang }}/contact_us");

$("#form-search select[name='polyclinic']").select2();

var keySearch = "";
var keyPoly = "";
var keyProvince = "";
var keyCity = "";
var keyType = "";
var numPage = 1;

function search(page, submit, type, q, polyclinic, province, regency) {
    var url = '';
    if(q!='') {
        url = url+'&q='+q;
    }

    if(polyclinic!='') {
        url = url+'&id_polyclinic='+polyclinic;
    }

    if(province!='') {
        url = url+'&province='+province;
    }

    if(regency!='') {
        url = url+'&regency='+regency;
    }


    ChangeUrl('', '?type='+type+''+url+'&page='+page);

    $("#content-clinic").addClass("hide");
    $("#search").html(""); 
    $("#search").addClass("hide");       
    $("#loading-search").removeClass("hide");
    $("#reload-search").addClass("hide");
    $("#page").html(""); 

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if(province=="" && submit=="false") {
        var province = keyProvince;
    }

    if(regency=="" && submit=="false") {
        var regency = keyCity;
    }

    if(type=="" && submit=="false") {
        var type = keyType;
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/"+type+"?q="+q+"&id_polyclinic="+polyclinic+"&province="+province+"&regency="+regency+"&page="+page+"&per_page=9&suspended=true",
        type: "GET",
        headers: {
            "Authorization": "Bearer {{ $api_token }}"
        },
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#search").html(""); 
            $("#loading-search").addClass("hide");

            if(!data.error) {
                $("#search").removeClass("hide");


                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    var item = data.data;

                    for (var i = 0; i < item.length; i++) {

                        if(type=="clinic") {

                            var banner = "";
                            if(item[i].banner==null || item[i].banner=="") {
                                banner = '<div class="bg-banner"><div class="banner-shape"></div></div>';
                            } else {
                                banner = "<img src='<?php echo $storage.'/images/clinic/banner/"+item[i].banner+"'; ?>' />";
                            }

                            var polyclinic_clinic = "";                        
                            if(item[i].polyclinic.length>0) {
                                var numPoly = 3;
                                if(item[i].polyclinic.length<numPoly) {
                                    numPoly = item[i].polyclinic.length;
                                }

                                for(var j=0; j<numPoly; j++) {
                                    polyclinic_clinic = polyclinic_clinic+item[i].polyclinic[j].name;

                                    if(j!=item[i].polyclinic.length-1) {  
                                        polyclinic_clinic = polyclinic_clinic+", ";             
                                        
                                        if(item[i].polyclinic.length>numPoly) {
                                            if(j==numPoly-1) {
                                                polyclinic_clinic = polyclinic_clinic+"...";   
                                            }
                                        }
                                    }
                                }
                            }

                            var location = "";
                            if(item[i].regency.name!="" && item[i].regency.name!=null) {
                                location = '<span class="button"><i class="fa fa-map-marker"></i> '+item[i].regency.name+'</span>';
                            }

                            var getPoly = "";
                            if(polyclinic!="") {
                                getPoly = "?polyclinic="+polyclinic;
                            }

                            $("#search").append('<a href=\'{{ url("/") }}/{{ $lang }}/clinic/'+item[i].id+'/'+item[i].name.replace(/ /g, "-")+''+getPoly+'\' class="gridview border">'+
                                '<div class="banner">'+
                                    ''+banner+''+
                                '</div>'+
                                '<div class="photo">'+
                                    thumbnail('md','clinic','{{ $storage }}',item[i].logo,item[i].name)+
                                '</div>'+
                                '<h1>'+item[i].name+'</h1>'+
                                '<p>'+polyclinic_clinic+'</p>'+
                                '<div class="button-sec">'+
                                    ''+location+''+
                                '</div>'+
                            '</a>');
                        } else {

                            var polyclinic_doctor = "";                        
                            if(item[i].polyclinic.length>0) {
                                var numPoly = 3;
                                if(item[i].polyclinic.length<numPoly) {
                                    numPoly = item[i].polyclinic.length;
                                }

                                for(var j=0; j<numPoly; j++) {
                                    polyclinic_doctor = polyclinic_doctor+item[i].polyclinic[j].name;

                                    if(j!=item[i].polyclinic.length-1) {  
                                        polyclinic_doctor = polyclinic_doctor+", ";             
                                        
                                        if(item[i].polyclinic.length>numPoly) {
                                            if(j==numPoly-1) {
                                                polyclinic_doctor = polyclinic_doctor+"...";   
                                            }
                                        }
                                    }
                                }
                            }

                            var location = "";
                            if(item[i].regency.name!="" && item[i].regency.name!=null) {
                                location = '<span class="button"><i class="fa fa-map-marker"></i> '+item[i].regency.name+'</span>';
                            }

                            var getPoly = "";
                            if(polyclinic!="") {
                                getPoly = "?polyclinic="+polyclinic;
                            }

                            $("#search").append('<a href=\'{{ url("/") }}/{{ $lang }}/doctor/'+item[i].id+'/'+item[i].name.replace(/ /g, "-")+''+getPoly+'\' class="gridview border profile">'+
                                thumbnail('lg','doctor','{{ $storage }}',item[i].photo,item[i].name)+
                                '<h1>'+item[i].name+'</h1>'+
                                '<p>'+polyclinic_doctor+'</p>'+
                                '<div class="button-sec">'+
                                    ''+location+''+
                                '</div>'+
                            '</a>');
                        }
                    }

                } else {
                    $("#search").html('<div id="error-search">'+
                        '<i class="icon fa fa-exclamation-triangle"></i>'+
                        '<h1 class="info">{{ trans("messages_web.no_result_found") }}</h1>'+
                        '<p class="info">{{ trans("messages_web.info_no_result") }}</p>'+
                    '</div>')
                }

                pages(page, total, per_page, current_page, last_page, from, to, type, q, polyclinic, province, regency);

            } else {
                $("#reload-search").removeClass("hide");  
                $("#reload-search").click(function(){ search(page,"false", type, q, polyclinic, province, regency); }); 
            }
        },
        error: function(){
            $("#loading-search").addClass("hide");
            $("#reload-search").removeClass("hide");

            $("#reload-search").click(function(){ search(page,"false", type, q, polyclinic, province, regency); });
        }
    })
}


$("#form-search").submit(function(event) {
    event.preventDefault();

    var q = $("#form-search input[name='q']").val();
    var polyclinic = $("#form-search select[name='polyclinic'] option:selected").val();
    var province = $("#form-search select[name='province'] option:selected").val();
    var regency = $("#form-search select[name='regency'] option:selected").val();
    var type = $("#form-search input[name='search_type']").prop('checked');

    if(type==true) {
        type = 'clinic';    
    } else {
        type = 'doctor';
    }

    search(1, 'true', type, q, polyclinic, province, regency);
});














var iconClinic = "{{ asset('assets/images/icons/ranger_station.png') }}";

function map() {
    $("#loading-search").removeClass("hide");
    $("#map").addClass("hide");
    $("#reload-search").addClass("hide");
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#loading-search").addClass("hide");

            if(!data.error) {

                $("#map").removeClass("hide");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    var infowindow = new google.maps.InfoWindow();
                    var marker = [];
                    var info_window = [];

                    var defaultPos = new google.maps.LatLng(-7.9818695, 112.6396762);

                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 13,
                        center: defaultPos,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    
                    try {
                        if (navigator.geolocation) {
                            var marker_location = new google.maps.Marker({
                                position: defaultPos,
                                map: map
                            });

                            $("#form-search-location button").click(function (e) {
                                searchLocation(map, marker_location);
                            });

                            $("#form-search-location input[name='q']").keydown(function (e) {
                                if (e.keyCode == 13) {
                                    searchLocation(map, marker_location);
                                }
                            });

                            map.setCenter(defaultPos);

                            google.maps.event.addListener(marker_location, 'click', (function(marker_location) {
                                return function() {
                                    infowindow.setContent('{{ trans("messages_web.your_location") }}');
                                    infowindow.open(map, marker_location);
                                    }
                            })(marker_location));

                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                marker_location.setPosition(pos);

                                map.setCenter(pos);

                            }, function() {
                                handleLocationError(map, marker_location, defaultPos);
                            });
                        } else {
                            var marker_location = new google.maps.Marker({
                                position: defaultPos,
                                map: map
                            });

                            $("#form-search-location #search-location").click(function (e) {
                                searchLocation(map, marker_location);
                            });

                            $("#form-search-location input[name='q']").keydown(function (e) {
                                if (e.keyCode == 13) {
                                    searchLocation(map, marker_location);
                                }
                            });

                            map.setCenter(defaultPos);

                            google.maps.event.addListener(marker_location, 'click', (function(marker_location) {
                                return function() {
                                    infowindow.setContent('{{ trans("messages_web.your_location") }}');
                                    infowindow.open(map, marker_location);
                                    }
                            })(marker_location));

                            alertLocation("{{ trans('messages_web.gps_failed') }}");
                        }

                    } catch(err) {
                        var marker_location = new google.maps.Marker({
                            position: defaultPos,
                            map: map
                        });

                        $("#form-search-location #search-location").click(function (e) {
                            searchLocation(map, marker_location);
                        });

                        $("#form-search-location input[name='q']").keydown(function (e) {
                            if (e.keyCode == 13) {
                                searchLocation(map, marker_location);
                            }
                        });

                        map.setCenter(defaultPos);

                        google.maps.event.addListener(marker_location, 'click', (function(marker_location) {
                            return function() {
                                infowindow.setContent('{{ trans("messages_web.your_location") }}');
                                infowindow.open(map, marker_location);
                                }
                        })(marker_location));

                        alertLocation("{{ trans('messages_web.gps_failed') }}");                        
                    }



                    var item = data.data;
                    for (var i = 0; i < item.length; i++) {  

                        var no = i;

                        var polyclinic = "";
                        var dataPoly = [];
                        for(var j = 0; j < item[i].polyclinic.length; j++) { 
                            polyclinic = polyclinic+'<span class="list">'+item[i].polyclinic[j].name+'</span>';
                            dataPoly[j] = item[i].polyclinic[j].name;
                        }

                        var logo = "";
                        if(item[i].logo==null || item[i].logo=="") {
                            logo = "logo.png";
                        } else {
                            logo = item[i].logo;
                        }

                        info_window[i] = '<div class="window-map">'+
                            '<div class="section-info">'+
                                '<div class="column"><img src="{{ $storage }}/images/clinic/logo/'+logo+'"></div>'+
                                '<div class="column"><h1>'+item[i].name+'</h1></div>'+
                            '</div>'+
                            '<div class="section-list">'+polyclinic+'</div>'+
                            '<div class="section-button">'+
                                '<a class="button" href=\'{{ url("/") }}/id/clinic/'+item[i].id+'/'+item[i].name.replace(/ /g, "-")+'\'>{{ trans("messages_web.schedule") }}</a>'+
                            '</div>'+
                        '</div';  

                        if(item[i].latitude=="" || item[i].longitude=="" || item[i].latitude==0 || item[i].longitude==0 || item[i].latitude==null || item[i].longitude==null) {
                            var address = item[i].address+" "+item[i].district+" "+item[i].regency+" "+item[i].province+" "+item[i].country;

                            geocoder = new google.maps.Geocoder();
                            geocoder.geocode({
                                'address': address
                            }, (function(info_window, i) { return function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {

                                    var geoPosition = results[0].geometry.location;

                                    marker = new google.maps.Marker({
                                        map: map,
                                        position: geoPosition,
                                        icon: iconClinic
                                    });

                                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                        return function() {
                                            infowindow.setContent(''+info_window+'');
                                            infowindow.open(map, marker);
                                            }
                                    })(marker, i));
                                }
                            }}(info_window[i], i)));
                            
                        } else {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(item[i].latitude, item[i].longitude),
                                map: map,
                                icon: iconClinic
                            });

                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(''+info_window[i]+'');
                                    infowindow.open(map, marker);
                                    }
                            })(marker, i));
                        }


                    }

                    

                }

            } else {
                $("#reload-search").removeClass("hide");

                $("#reload-search").click(function(){ map(); });    
            }
            
        },
        error: function(){
            $("#loading-search").addClass("hide");
            $("#reload-search").removeClass("hide");

            $("#reload-search").click(function(){ map(); });
        }
    })
}


function alertLocation(val) {
    $("#section-alert-location").removeClass("hide");
    $("#alert-location").html(val);
}

function searchLocation(resultsMap, marker_location) {
    $("#section-alert-location").addClass("hide");
    $("#alert-location").html("");

    var address = $("#form-search-location input[name='q']").val();

    ChangeUrl('', '?type=map&q='+address);

    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);

            marker_location.setPosition(results[0].geometry.location);
        } else {
            alertLocation("{{ trans('messages_web.location_not_found') }}");
        }
    });
}

function handleLocationError(map, marker_location, defaultPos) {
    alertLocation("{{ trans('messages_web.gps_failed') }}");

    marker_location.setPosition(defaultPos);

    $("#form-search-location #search-location").click(function (e) {
        searchLocation(map, marker_location);
    });

    $("#form-search-location input[name='q']").keydown(function (e) {
        if (e.keyCode == 13) {
            searchLocation(map, marker_location);
        }
    });

    map.setCenter(defaultPos);

    google.maps.event.addListener(marker_location, 'click', (function(marker_location) {
        return function() {
            infowindow.setContent('{{ trans("messages_web.your_location") }}');
            infowindow.open(map, marker_location);
            }
    })(marker_location));
}


$('#search_clinic').click(function() {
    var q = $("#form-search input[name='q']").val();
    var polyclinic = $("#form-search select[name='polyclinic'] option:selected").val();
    var province = $("#form-search select[name='province'] option:selected").val();
    var regency = $("#form-search select[name='regency'] option:selected").val();

    type = 'clinic';

    search(1, 'true', type, q, polyclinic, province, regency);
});


$('#search_doctor').click(function() {
    var q = $("#form-search input[name='q']").val();
    var polyclinic = $("#form-search select[name='polyclinic'] option:selected").val();
    var province = $("#form-search select[name='province'] option:selected").val();
    var regency = $("#form-search select[name='regency'] option:selected").val();

    type = 'doctor';

    search(1, 'true', type, q, polyclinic, province, regency);
});



@if(!empty($get_type))
    @if($get_type=="map")
        search_type('map');
    @else
        $("#form-search input[name='q']").val('{{ $get_q }}');
        $("#form-search select[name='polyclinic']").val('{{ $get_polyclinic }}').trigger('change');
        $("#form-search select[name='province']").val('{{ $get_province }}').trigger('change');
        $("#form-search select[name='regency']").val('{{ $get_regency }}').trigger('change');

        var page = 1;
        @if(!empty($get_page))
        page = '{{ $get_page }}';
        @endif

        @if(!empty($get_type) && $get_type=="clinic")
        switchButton('#search_doctor', '#search_clinic','#search_type',true);
        @else    
        switchButton('#search_clinic', '#search_doctor','#search_type',false);
        @endif

        search(page, 'true', '{{ $get_type }}', '{{ $get_q }}', '{{ $get_polyclinic }}', '{{ $get_province }}', '{{ $get_regency }}');
    @endif
@endif

function ChangeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Page: page, Url: url };
        history.pushState(obj, obj.Page, obj.Url);
    } else {
    }
}

</script>
@endsection	