<script type="text/javascript">

function getTimeAgo(time) {

    var now = new Date();
    var getDate = new Date(time.replace(/\-/g,'/'));
    var getTime = (now - getDate);
    var years = Math.floor(getTime / (86400000*365));
    var months = Math.floor(getTime / (86400000*30));
    var weeks = Math.floor(getTime / (86400000*7));
    var days = Math.floor(getTime / 86400000);
    var hours = Math.floor((getTime % 86400000) / 3600000);
    var minutes = Math.round(((getTime % 86400000) % 3600000) / 60000);

    if(years>=1) {
        var getMonth = new Date(getDate).getMonth()+1;
        timeAgo = getFormatDate(new Date(getDate).getFullYear()+'-'+getMonth+'-'+new Date(getDate).getDate());
    } else if(months>=1) {
        timeAgo = months+" {{ trans('messages.months_ago') }}";
    } else if(weeks>=1) {
        timeAgo = weeks+" {{ trans('messages.weeks_ago') }}";
    } else if(days>=1) {
        timeAgo = days+" {{ trans('messages.days_ago') }}";
    } else if(hours>=1) {
        timeAgo = hours+" {{ trans('messages.hours_ago') }}";
    } else if(minutes>=1) {
        timeAgo = minutes+" {{ trans('messages.minutes_ago') }}";
    } else {
        timeAgo = "{{ trans('messages.seconds_ago') }}";
    }
    
    return timeAgo;


}

function getDay(num) {
    var day = "";

    if(num==1) {
        day = "{{ trans('messages.monday') }}";
    } else if(num==2) {
        day = "{{ trans('messages.tuesday') }}";
    } else if(num==3) {
        day = "{{ trans('messages.wednesday') }}";
    } else if(num==4) {
        day = "{{ trans('messages.thursday') }}";
    } else if(num==5) {
        day = "{{ trans('messages.friday') }}";
    } else if(num==6) {
        day = "{{ trans('messages.saturday') }}";
    } else if(num==0 || num==7) {
        day = "{{ trans('messages.sunday') }}";
    }

    return day;
}

function age(date, section) {
    var DOB = new Date(date);
    var today = new Date();
    var age = today.getTime() - DOB.getTime();
    age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

    if(age!=0) {
        if(age<0) {
            notif(false,"{{ trans('validation.invalid_birth_date') }}");
        } else {
            $(section).val(age);    
        }
    }
}

function getMonth(num) {
    var month = "";

    if(num==1) {
        month = "{{ trans('messages.january') }}";
    } else if(num==2) {
        month = "{{ trans('messages.february') }}";
    } else if(num==3) {
        month = "{{ trans('messages.march') }}";
    } else if(num==4) {
        month = "{{ trans('messages.april') }}";
    } else if(num==5) {
        month = "{{ trans('messages.may') }}";
    } else if(num==6) {
        month = "{{ trans('messages.june') }}";
    } else if(num==7) {
        month = "{{ trans('messages.july') }}";
    } else if(num==8) {
        month = "{{ trans('messages.august') }}";
    } else if(num==9) {
        month = "{{ trans('messages.september') }}";
    } else if(num==10) {
        month = "{{ trans('messages.october') }}";
    } else if(num==11) {
        month = "{{ trans('messages.november') }}";
    } else if(num==12) {
        month = "{{ trans('messages.december') }}";
    }

    return month;
}

function getShortMonth(num) {
    var month = "";

    if(num==1) {
        month = "{{ trans('messages.jan') }}";
    } else if(num==2) {
        month = "{{ trans('messages.feb') }}";
    } else if(num==3) {
        month = "{{ trans('messages.mar') }}";
    } else if(num==4) {
        month = "{{ trans('messages.apr') }}";
    } else if(num==5) {
        month = "{{ trans('messages.may') }}";
    } else if(num==6) {
        month = "{{ trans('messages.jun') }}";
    } else if(num==7) {
        month = "{{ trans('messages.jul') }}";
    } else if(num==8) {
        month = "{{ trans('messages.aug') }}";
    } else if(num==9) {
        month = "{{ trans('messages.sep') }}";
    } else if(num==10) {
        month = "{{ trans('messages.oct') }}";
    } else if(num==11) {
        month = "{{ trans('messages.nov') }}";
    } else if(num==12) {
        month = "{{ trans('messages.dec') }}";
    }

    return month;
}

function getFormatDate(date) {
  if(date=="") {
    getDate="";
  } else {
    var splitDate = date.split("-");

    var month = getMonth(splitDate[1]);

    getDate = splitDate[2]+' '+month+' '+splitDate[0];  
  }

  return getDate;
}

function getShortFormatDate(date) {
  if(date=="") {
    getDate="";
  } else {
    var splitDate = date.split("-");

    var month = getShortMonth(splitDate[1]);

    getDate = splitDate[2]+'-'+month+'-'+(splitDate[0].toString()).slice(-2);  
  }

  return getDate;
}

function getDateMonth(date) {
  if(date=="") {
    getDate="";
  } else {
    var splitDate = date.split("-");

    var month = getShortMonth(splitDate[1]);

    getDate = splitDate[2]+' '+month;  
  }

  return getDate;
}


function convertDate(date) {
  if(date=="") {
    getDate="";
  } else {
    var splitDate = date.split("/");
    getDate = splitDate[2]+'-'+splitDate[1]+'-'+splitDate[0];  
  }

  return getDate;
}

function unconvertDate(date) {
  if(date=="") {
    getDate="";
  } else {
    var splitDate = date.split("-");
    getDate = splitDate[2]+'/'+splitDate[1]+'/'+splitDate[0];  
  }

  return getDate;
}

function convertTime(time) {
  var splitTime = time.split(":");
  getTime = splitTime[0]+':'+splitTime[1];

  return getTime; 
}

function getAge(date) {
    var DOB = new Date(date);
    var today = new Date();
    var age = today.getTime() - DOB.getTime();
    age = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));

    return age;
}

function addDays(dateObj, numDays) {
   dateObj.setDate(dateObj.getDate() + numDays);
   return dateObj;
}

function hospitalizationType(type) {
    if(type=="outpatient") {
        hospitalization = "{{ trans('messages.outpatient') }}";
    } else {
        hospitalization = "{{ trans('messages.inpatient') }}";
    }

    return hospitalization;
}

function checkNull(value) {
    var val = value;
    if(value==null || value=="" || value==0) {
        val = "-";
    }

    return val;
}

function checkZero(value) {
    var val = value;
    if(value==0) {
        val = "";
    }

    return val;
}

function getZero(value) {
    var val = value;
    if(value==null || value=="") {
        val = 0;
    }

    return val;
}


function financing(type) {
    if(type=="insurance") {
        financing = "{{ trans('messages.insurance') }}";
    } else if(type=="agency") {
        financing = "{{ trans('messages.agency') }}";
    } else {
        financing = "{{ trans('messages.personal') }}";
    }

    return financing;
}

function active(type) {
    if(type==1) {
        status = "<span class='status-active'>{{ trans('messages.active') }}</span>";
    } else {
        status = "<span class='status-nonactive'>{{ trans('messages.non_active') }}</span>";
    }

    return status;
}

function keel(type) {
    if(type==1) {
        status = "<span class='status-active'>{{ trans('messages.keel') }}</span>";
    } else {
        status = "<span class='status-nonactive'>{{ trans('messages.not_keel') }}</span>";
    }

    return status;
}

function status(type) {
    if(type==1) {
        val = "<span class='status-active'>{{ trans('messages.success') }}</span>";
    } else {
        val = "<span class='status-nonactive'>{{ trans('messages.failed') }}</span>";
    }

    return val;
}

function confirmation(type) {
    if(type==3) {
        status = "<span class='status-done'>{{ trans('messages.done') }}</span>";
    } else if(type==2) {
        status = "<span class='status-confirmation'>{{ trans('messages.confirmed') }}</span>";
    } else if(type==-1) {
        status = "<span class='status-canceled'>{{ trans('messages.cancel') }}</span>";
    } else {
        status = "<span class='status-pending'>{{ trans('messages.pending') }}</span>"
    }

    return status;
}

function getGender(gender) {
  if(gender==0) {
    gender = "{{ trans('messages.male') }}";  
  } else {
    gender = "{{ trans('messages.female') }}";
  }

  return gender;
}

function getSmoke(type) {
    if(type==0) {
        smoke = "{{ trans('messages.no') }}";
    } else {
        smoke = "{{ trans('messages.yes') }}";
    }

    return smoke;
}

function getImplementer(type) {
    if(type==0) {
        implementer = "{{ trans('messages.no') }}";
    } else {
        implementer = "{{ trans('messages.yes') }}";
    }

    return implementer;
}

function getFinancing(type) {
    if(type=="insurance") {
        financing = "{{ trans('messages.insurance') }}";
    } else if(type=="agency") {
        financing = "{{ trans('messages.agency') }}";
    } else {
        financing = "{{ trans('messages.personal') }}";
    }

    return financing;
}


function formatCurrency(type, currency, num) {
    if(type=="id") {
        return currency+" "+num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+",00";
    } else {
        return currency+" "+num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")+",00";
    }
}

function drill_down(e, id) {
    var detail = $(id).css('display');

    $(".btn-table.btn-blue").html("<span class='fa fa-chevron-down'></span> {{ trans('messages.show_detail') }}");
    $(".row-drill-down").addClass("hide");

    if(detail=="none") {
        $(e).html("<span class='fa fa-chevron-up'></span> {{ trans('messages.hide_detail') }}");
        $(id).removeClass("hide");
    } else {
        $(e).html("<span class='fa fa-chevron-down'></span> {{ trans('messages.show_detail') }}");
        $(id).addClass("hide");
    }
}

function limitChar(text, limit) {
    if(text.length>limit) {
        text = text.substr(0, limit)+' ...';
    }

    return text;
}

function nullToZero(value) {
    var val = value;
    if(value==null) {
        val = 0;
    }

    return val;
}

function nullToEmpty(value) {
    var val = value;
    if(value==null) {
        val = "";
    }

    return val;
}

function thumbnail(view, type, storage, value, char) {
    size = "";
    if(view=="sm") {
        width = 35;
        height = 35;
    } else if(view=="md") {
        width = 60;
        height = 60;
    } else if(view=="lg") {
        width = 100;
        height = 100;
    } else if(view=="xl") {
        width = 180;
        height = 180;
    }
    size = 'width="'+width+'px" height="'+height+'px" style="min-width:'+width+'px;max-width:'+width+'px;width:'+width+'px;min-height:'+height+'px;max-height:'+height+'px;height:'+height+'px"';

    if(type=="user") {
        char = 'U';
        url = storage+'/images/user/';
    } if(type=="clinic") {
        char = 'C';
        url = storage+'/images/clinic/logo/';
    } if(type=="doctor") {
        char = 'D';
        url = storage+'/images/doctor/';
    } else if(type=="patient") {
        char = 'P';
        url = storage+'/images/patient/';
    }

    if(value==null || value=="") {
        if(type=="user") {
            photo = '<img src="'+url+'photo.png" '+size+' />';    
        } else if(type=="clinic") {
            photo = '<img src="'+url+'logo.png" '+size+' />';    
        } else {
            photo = '<div class="photo-image" style="min-width:'+width+'px;max-width:'+width+'px;width:'+width+'px;min-height:'+height+'px;max-height:'+height+'px;height:'+height+'px;">'+char+'</div>';    
        }
    } else {
        photo = '<img src="'+url+''+value+'" '+size+' />';
    }

    return photo;
}

</script>

<?php

function getMonth($num) {
    $month = "";

    if($num==1) {
        $month = trans('messages.january');
    } else if($num==2) {
        $month = trans('messages.february');
    } else if($num==3) {
        $month = trans('messages.march');
    } else if($num==4) {
        $month = trans('messages.april');
    } else if($num==5) {
        $month = trans('messages.may');
    } else if($num==6) {
        $month = trans('messages.june');
    } else if($num==7) {
        $month = trans('messages.july');
    } else if($num==8) {
        $month = trans('messages.august');
    } else if($num==9) {
        $month = trans('messages.september');
    } else if($num==10) {
        $month = trans('messages.october');
    } else if($num==11) {
        $month = trans('messages.november');
    } else if($num  ==12) {
        $month = trans('messages.december');
    }

    return $month;
}

function getFormatDate($date) {
  if($date=="") {
    $getDate="";
  } else {
    $splitDate = explode("-",$date);

    $month = getMonth($splitDate[1]);

    $getDate = $splitDate[2].' '.$month.' '.$splitDate[0];  
  }

  return $getDate;
}

function getGender($gender) {
  if($gender==0) {
    $getGender = trans('messages.male');  
  } else {
    $getGender = trans('messages.female');
  }

  return $getGender;
}

function checkNull($value) {
    $val = $value;
    if($value==null || $value=="") {
        $val = "-";
    }

    return $val;
}


function getFinancing($type) {
    if($type=="insurance") {
        $financing = trans('messages.insurance');
    } else if($type=="agency") {
        $financing = trans('messages.agency');
    } else {
        $financing = trans('messages.personal');
    }

    return $financing;
}

function getAge($date) {
    $dateOfBirth = $date;
    $today = date("Y-m-d");
    $diff = date_diff(date_create($dateOfBirth), date_create($today));

    $age = $diff->format('%y');

    return $age;
}

function getSmoke($type) {
    if($type==0) {
        $smoke = trans('messages.no');
    } else {
        $smoke = trans('messages.yes');
    }

    return $smoke;
}

function getDay($num) {
    $day = "";

    if($num==1) {
        $day = trans('messages.monday');
    } else if($num==2) {
        $day = trans('messages.tuesday');
    } else if($num==3) {
        $day = trans('messages.wednesday');
    } else if($num==4) {
        $day = trans('messages.thursday');
    } else if($num==5) {
        $day = trans('messages.friday');
    } else if($num==6) {
        $day = trans('messages.saturday');
    } else if($num==0 || $num==7) {
        $day = trans('messages.sunday');
    }

    return $day;
}

function convertTime($time) {
    $splitTime = explode(":",$time);
    $getTime = $splitTime[0].':'.$splitTime[1];

    return $getTime; 
}


function formatCurrency($type, $currency, $num) {
    if($type=="id") {
        return $currency." ".number_format($num,2,',','.');
    } else {
        return $currency." ".number_format($num,2,',','.');
    }
}

function confirmation($type) {
    if($type==3) {
        $status = "<span class='status-done'>".trans('messages.done')."</span>";
    } else if($type==2) {
        $status = "<span class='status-confirmation'>".trans('messages.confirmed')."</span>";
    } else if($type==-1) {
        $status = "<span class='status-canceled'>".trans('messages.cancel')."</span>";
    } else {
        $status = "<span class='status-pending'>".trans('messages.pending')."</span>";
    }

    return $status;
}

function confirmation_text($type) {
    if($type==3) {
        $status = trans('messages.done');
    } else if($type==2) {
        $status = trans('messages.confirmed');
    } else if($type==-1) {
        $status = trans('messages.cancel');
    } else {
        $status = trans('messages.pending');
    }

    return $status;
}
















function province() {
    $province = array("Aceh", "Sumatera Utara", "Sumatera Barat", "Riau", "Jambi", "Sumatera Selatan", "Lampung", "Bengkulu", "Bangka Belitung", "Kepulauan Riau", "Jakarta", "Jawa Barat", "Jawa Tengah", "Yogyakarta", "Jawa Timur", "Banten", "Bali", "Nusa Tenggara Barat", "Nusa Tenggara Timur", "Kalimantan Barat", "Kalimantan Timur", "Kalimantan Tengah", "Kalimantan Selatan", "Sulawesi Utara", "Sulawesi Tengah", "Sulawesi Selatan", "Sulawesi Tenggara", "Gorontalo", "Sulawesi Barat", "Maluku", "Maluku Utara", "Papua", "Papua Barat"); 

    sort($province);

    return $province;
}

function regency() {
    $regency = array('Banda Aceh',
                'Langsa',
                'Lhokseumawe',
                'Sabang',
                'Subulussalam',
                'Denpasar',
                'Cilegon',
                'Serang',
                'Tangerang',
                'Tangerang selatan',
                'Bengkulu',
                'Yogyakarta',
                'Jakarta Barat',
                'Jakarta Pusat',
                'Jakarta Selatan',
                'Jakarta Timur',
                'Jakarta Utara',
                'Gorontalo',
                'Jambi',
                'Sungai Penuh',
                'Bandung',
                'Banjar',
                'Bekasi',
                'Bogor',
                'Cimahi',
                'Cirebon',
                'Depok',
                'Sukabumi',
                'Tasikmalaya',
                'Magelang',
                'Pekalongan',
                'Salatiga',
                'Semarang',
                'Surakarta',
                'Tegal',
                'Batu',
                'Blitar',
                'Kediri',
                'Madiun',
                'Malang',
                'Mojokerto',
                'Pasuruan',
                'Probolinggo',
                'Surabaya',
                'Pontianak',
                'Singkawang',
                'Banjarbaru',
                'Banjarmasin',
                'Palangka Raya',
                'Balikpapan',
                'Bontang',
                'Samarinda',
                'Tarakan',
                'Pangkal Pinang',
                'Batam',
                'Tanjung Pinang',
                'Bandar Lampung',
                'Kotabumi',
                'Liwa',
                'Metro',
                'Ambon',
                'Tual',
                'Ternate',
                'Tidore Kepulauan',
                'Bima',
                'Mataram',
                'Kupang',
                'Jayapura',
                'Sorong',
                'Dumai',
                'Pekanbaru',
                'Makassar',
                'Palopo',
                'Parepare',
                'Palu',
                'Bau-Bau',
                'Kendari',
                'Bitung',
                'Kotamobagu',
                'Manado',
                'Tomohon',
                'Bukittinggi',
                'Padang',
                'Padangpanjang',
                'Pariaman',
                'Payakumbuh',
                'Sawahlunto',
                'Solok',
                'Lubuklinggau',
                'Pagar Alam',
                'Palembang',
                'Prabumulih',
                'Binjai',
                'Gunungsitoli',
                'Medan',
                'Padangsidempuan',
                'Pematangsiantar',
                'Sibolga',
                'Tanjungbalai',
                'Tebing Tinggi'); 

    sort($regency);

    return $regency;
}

function country() {
    $country = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"); 

    sort($country);

    return $country;
}

function phone_code() {
    $code = array(
        array("id","62"),
        array("af","93"),
        array("al","355"),
        array("dz","213"),
        array("as","1684"),
        array("ad","376"),
        array("ao","244"),
        array("ai","1264"),
        array("ag","1268"),
        array("ar","54"),
        array("am","374"),
        array("aw","297"),
        array("au","61"),
        array("at","43"),
        array("az","994"),
        array("bs","1242"),
        array("bh","973"),
        array("bd","880"),
        array("bb","1246"),
        array("by","357"),
        array("be","32"),
        array("bz","501"),
        array("bj","229"),
        array("bm","1441"),
        array("bt","975"),
        array("bo","591"),
        array("ba","387"),
        array("bw","267"),
        array("br","55"),
        array("gs","1284"),
        array("bn","673"),
        array("bg","359"),
        array("bf","226"),
        array("bi","257"),
        array("kh","855"),
        array("cm","237"),
        array("ca","1"),
        array("cv","238"),
        array("ky","1345"),
        array("cf","236"),
        array("be","235"),
        array("cl","56"),
        array("cn","86"),
        array("co","57"),
        array("km","269"),
        array("cd","243"),
        array("cg","242"),
        array("ck","682"),
        array("cr","506"),
        array("ci","225"),
        array("hr","385"),
        array("nr","599"),
        array("cy","357"),
        array("cz","420"),
        array("dk","45"),
        array("dj","253"),
        array("dm","1767"),
        array("do","1"),
        array("ec","593"),
        array("eg","20"),
        array("hn","503"),
        array("gq","240"),
        array("er","291"),
        array("ee","372"),
        array("et","251"),
        array("fk","500"),
        array("fo","298"),
        array("fj","679"),
        array("fi","358"),
        array("fr","33"),
        array("gf","594"),
        array("pf","689"),
        array("ga","241"),
        array("gm","220"),
        array("ge","995"),
        array("de","49"),
        array("gh","223"),
        array("gi","350"),
        array("gr","30"),
        array("gl","299"),
        array("gd","1473"),
        array("guadeloupe","590"),
        array("guam","1671"),
        array("gt","502"),
        array("gn","224"),
        array("gw","245"),
        array("gy","592"),
        array("ht","509"),
        array("hn","504"),
        array("hk","852"),
        array("hu","36"),
        array("is","354"),
        array("in","91"),
        array("iq","964"),
        array("ie","353"),
        array("il","972"),
        array("it","39"),
        array("jm","1876"),
        array("jp","81"),
        array("jo","962"),
        array("kz","7"),
        array("ke","254"),
        array("ki","686"),
        array("kw","965"),
        array("kg","996"),
        array("la","856"),
        array("lv","371"),
        array("lb","961"),
        array("ls","266"),
        array("lr","231"),
        array("libya","218"),
        array("li","423"),
        array("lt","370"),
        array("lu","352"),
        array("mo","853"),
        array("mk","389"),
        array("mg","261"),
        array("mw","265"),
        array("my","60"),
        array("mv","960"),
        array("ml","223"),
        array("mt","356"),
        array("mh","692"),
        array("mq","596"),
        array("mr","222"),
        array("yt","262"),
        array("mx","52"),
        array("fm","691"),
        array("md","373"),
        array("mc","377"),
        array("Mongolia","976"),
        array("me","382"),
        array("ms","1664"),
        array("ma","212"),
        array("mz","258"),
        array("Myanmar","95"),
        array("ma","264"),
        array("nr","674"),
        array("np","977"),
        array("nl","31"),
        array("nc2","687"),
        array("nz","64"),
        array("ni","505"),
        array("ne","227"),
        array("ng","234"),
        array("nu","683"),
        array("mp","1670"),
        array("no","47"),
        array("om","698"),
        array("pk","92"),
        array("pw","680"),
        array("ps","970"),
        array("pa","507"),
        array("pg","675"),
        array("py","595"),
        array("pe","51"),
        array("ph","63"),
        array("pl","48"),
        array("pt","351"),
        array("pr","1"),
        array("qa","974"),
        array("re","262"),
        array("ro","40"),
        array("ru","7"),
        array("rw","250"),
        array("sh","290"),
        array("kn","1869"),
        array("lc","1758"),
        array("pm","508"),
        array("vc","1784"),
        array("ws","685"),
        array("sm","378"),
        array("st","239"),
        array("sa","966"),
        array("sn","221"),
        array("rs","381"),
        array("sc","248"),
        array("sl","232"),
        array("sg","65"),
        array("sint marten","1721"),
        array("sk","421"),
        array("si","386"),
        array("sb","677"),
        array("so","252"),
        array("za","27"),
        array("kr","82"),
        array("es","34"),
        array("lk","94"),
        array("sr","594"),
        array("sz","268"),
        array("se","46"),
        array("Switzerland","41"),
        array("tw","886"),
        array("tj","992"),
        array("tz","255"),
        array("th","66"),
        array("tl","670"),
        array("tg","228"),
        array("tk","690"),
        array("to","676"),
        array("tt","1868"),
        array("tn","216"),
        array("tr","90"),
        array("tm","993"),
        array("tc","1649"),
        array("Tuvalu","688"),
        array("vi","1340"),
        array("ug","256"),
        array("ua","380"),
        array("ae","971"),
        array("gb","44"),
        array("us","1"),
        array("uy","598"),
        array("uz","998"),
        array("vu","678"),
        array("va","39"),
        array("ve","58"),
        array("vn","84"),
        array("wf","681"),
        array("ye","967"),
        array("zm","260"),
        array("zw","263"),
    );

    return $code;
}

function level($level) {
    if($level=="admin") {
        $level = trans("messages.admin");
    } elseif($level=="doctor") {
        $level = trans("messages.doctor");
    } elseif($level=="patient") {
        $level = trans("messages.patient");
    } else {
        $level = trans("messages.user");
    }

    return $level;
}

function thumbnail($view, $type, $storage, $value, $char) {
    $size = "";
    if($view=="sm") {
        $width = 35;
        $height = 35;
    } else if($view=="md") {
        $width = 60;
        $height = 60;
    } else if($view=="lg") {
        $width = 100;
        $height = 100;
    } else if($view=="xl") {
        $width = 180;
        $height = 180;
    }

    $size = 'width="'.$width.'px" height="'.$height.'px" style="min-width:'.$width.'px;max-width:'.$width.'px;width:'.$width.'px;min-height:'.$height.'px";max-height:'.$height.'px";height:'.$height.'px"';

    if($type=="user") {
        $char = 'U';
        $url = $storage.'/images/user/';
    } if($type=="clinic") {
        $char = 'C';
        $url = $storage.'/images/clinic/logo/';
    } if($type=="doctor") {
        $char = 'D';
        $url = $storage.'/images/doctor/';
    } else if($type=="patient") {
        $char = 'P';
        $url = $storage.'/images/patient/';
    }

    if($value==null || $value=="") {
        if($type=="user") {
            $photo = '<img src="'.$url.'photo.png" '.$size.' />';    
        } else if($type=="clinic") {
            $photo = '<img src="'.$url.'logo.png" '.$size.' />';    
        } else {
            $photo = '<div class="photo-image" style="min-width:'.$width.'px;max-width:'.$width.'px;width:'.$width.'px;min-height:'.$height.'px;max-height:'.$height.'px;height:'.$height.'px;">'.$char.'</div>';    
        }
    } else {
        $photo = '<img src="'.$url.''.$value.'" '.$size.' />';
    }

    return $photo;
}

?>