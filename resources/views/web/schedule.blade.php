@extends('web.layouts.web')

@section('content')

@include('web.function.function')

<section class="form-sec">
    <div class="content-form">
        <h1>{{ trans("messages_web.register_schedule") }}</h1>
        <div class="info">
            <div class="row">
                <div class="col-sm-4">
                    <a class="list" data-id="{{ $clinic->id }}" data-type="clinic" data-toggle="modal" data-target="#profile">
                        <div class="canvas">
                           <?php echo thumbnail('sm','clinic',$storage,$clinic->logo,$clinic->name); ?>
                        </div>
                        <p>{{ $clinic->name }}</p>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a class="list" data-id="{{ $doctor->id }}" data-type="doctor" data-toggle="modal" data-target="#profile">
                        <div class="canvas">
                           <?php echo thumbnail('sm','doctor',$storage,$doctor->photo,$doctor->name); ?>
                        </div>
                        <p>{{ $doctor->name }}</p>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <form id="form-schedule">
                    <div class="form">
                        <div class="alert hide" id="section-alert">
                            <i class="fa fa-info-circle"></i> <span id="alert-schedule"></span>
                            <span class="close ion-close-round" onclick="$(this).parent().addClass('hide')"></span>
                        </div>
                        <h2>{{ trans("messages_web.poly") }}</h2>
                        <span class="label">{{ $polyclinic->name }}</span>
                        <h2>{{ trans("messages_web.patient") }}</h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <a class="link" data-toggle="modal" data-target="#search-patient"><i class="fa fa-search"></i> {{ trans("messages_web.find_patient") }}</a>
                            </div>
                            <div class="col-sm-6">
                                <a class="link hide" onclick="resetPatient()" id="link-new-patient"><i class="fa fa-plus"></i> {{ trans("messages_web.new_patient") }}</a>
                            </div>
                        </div>
                        <div id="detail-patient">
                        </div>
                        <input type="hidden" name="id_patient" />
                        <div id="form-patient">
                            <div class="form-group" id="email-group">
                                <div class="left">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="right">
                                    <label>{{ trans('messages_web.email') }}</label>
                                    <input type="text" name="email" id="email" />
                                    <span class="help-block"></span>
                                    <span id="loading-email" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="form-group" id="phone-group">
                                <div class="left">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="right">
                                    <label>{{ trans('messages_web.phone') }}</label>
                                    <input type="text" class="phone-input" name="phone" onkeydown="number(event)">
                                    
                                    <?php 
                                        $list_phone = phone_code();
                                    ?>

                                    <select id="phone_code" name="phone_code" class="select-phone-code">
                                        @foreach($list_phone as $phone)
                                            <option value="{{ $phone[1] }}" data-icon="{{ asset('assets/images/flags/'.$phone[0].'.png') }}">+{{ $phone[1] }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block"></span>
                                    <span id="loading-phone" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="form-group" id="name-group">
                                <div class="left">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="right">
                                    <label>{{ trans('messages_web.name') }}</label>
                                    <input type="text" name="name" />
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group" id="birth_date-group">
                                <div class="left">
                                    <i class="fa fa-birthday-cake"></i>
                                </div>
                                <div class="right">
                                    <label>{{ trans('messages_web.birth_date') }}</label>
                                    <div id="birth_date-section">
                                        <div id="datepicker_birth_date" class="input-group datetimepicker">
                                            <input type="text" name="birth_date" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="form-group" id="gender-group">
                                <div class="left">
                                    <i class="fa fa-mars"></i>
                                </div>
                                <div class="right">
                                    <label>{{ trans('messages_web.gender') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="0" name="gender"><span></span> {{ trans('messages_web.male') }}</label>
                                        <label><input type="radio" value="1" name="gender"><span></span> {{ trans('messages_web.female') }}</label>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <h2>Waktu</h2>
                        <div class="form-group" id="schedule-group">
                            <div class="left">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="right">
                                <div class="form-group" id="schedule-group">
                                    <label>{{ trans('messages_web.select_practice_time') }}</label>
                                    <div class="border-group">
                                        <select name="schedule" disabled="disabled">
                                            <option value="">{{ trans('messages_web.no_schedule') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group hide" id="appointment-group">
                                    <div class="sf-check">
                                        <label><input type="checkbox" name="set_appointment"><span></span> {{ trans("messages_web.appointment") }}</label>
                                    </div>
                                    <div id="timepicker_time" class="input-group datetimepicker">
                                        <input data-format="HH:mm" type="text" placeholder="{{ trans('messages_web.time') }}" name="appointment" value="" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group date-disabled" id="date-group">
                            <div class="left">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <div class="right">
                                <label>{{ trans('messages_web.select_date') }}</label>
                                <div id="date-section">
                                    <div id="datepicker_date" class="input-group datetimepicker">
                                        <input class="form-control" type="text" placeholder="{{ trans('messages_web.date') }}" name="date" disabled="disabled" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                                <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="left">
                            </div>
                            <div class="right">
                                <div class="form-group" id="name-group">
                                    <button class="btn-primary">
                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        {{ trans("messages_web.register") }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('assets/js/jquery-ui.js') }}"></script>

<script type="text/javascript">


$.ajaxSetup({
    headers: {
        "Authorization": "Bearer {{ $api_token }}"
    }
});



$("select[name='phone_code']").wSelect();
$("select[name='phone_code']").val('62').change();


$('#datepicker_birth_date').datetimepicker({
    locale: "{{ $lang }}",
    viewMode: 'years',
    format: 'DD/MM/YYYY',
});


$("#datepicker_date input[name='date']").keypress(function(event) {event.preventDefault();});

$('#timepicker_time').datetimepicker({format: 'HH:mm'});

$("#link-contact_us").attr("href","{{ url('/') }}/{{ $lang }}/contact_us");

function resetPatient() {
    resetValidation('form-schedule #email', 'form-schedule #phone', 'form-schedule #name', 'form-schedule #birth_date', 'form-schedule #gender');

    $("#detail-patient").html("");
    $("#form-schedule input[name='id_patient']").val("");
    $("#form-schedule input[name='email']").val("");
    $("#form-schedule select[name='phone_code']").val("62").change();
    $("#form-schedule input[name='phone']").val("");
    $("#form-schedule input[name='name']").val("");
    $("#form-schedule input[name='birth_date']").val("");
    $("#form-schedule input[name='gender']:checked").each(function(){
        $(this).prop('checked', false); 
    });

    $("#form-patient").removeClass("hide");
    $("#link-new-patient").addClass("hide");
}


function getPatient(id, name, patient_photo) {

    $("#form-schedule input[name='id_patient']").val(id);
    $("#form-patient").addClass("hide");
    $("#link-new-patient").removeClass("hide");

    $("#detail-patient").html('<a class="list" data-id="'+id+'" data-type="patient" data-toggle="modal" data-target="#profile">'+
        '<div class="canvas">'+
           thumbnail('sm','patient','{{ $storage }}',patient_photo,name)+
        '</div>'+
        '<p>'+name+'</p>'+
    '</a>');
}

$("#form-schedule input[name='email']").autocomplete({
    minLength: 5,
    source: function( request, response ) {
        var r = [];

        formData= new FormData();
        formData.append("email", request.term);

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/patient/search",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $("#form-schedule #loading-email").removeClass("hide");
            },
            success: function(data){
                $("#form-schedule #loading-email").addClass("hide");

                if(!data.error) {
                    r.push({value:data.id,
                        id:data.id,
                        email:data.email,
                        phone_code:data.phone_code,
                        phone:data.phone,
                        name:data.name,
                        photo:data.photo
                    });

                    response(r);         
                } else {
                    response(r);
                }
                
            },
            error: function(){
                response(r);
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {        
        getPatient(ui.item.id, ui.item.name, ui.item.photo);
        return false;
    },
}).data("ui-autocomplete")._renderItem = function (ul, item) {
    var inner_html = '<div class="autocomplete">'+
        thumbnail('md','patient','{{ $storage }}',item.photo,item.name)+
        '<div class="text">'+
            '<h1>'+item.name+'</h1>'+
            '<p>'+nullToEmpty(item.email)+'</p>'+
            '<p>+'+item.phone_code+''+item.phone+'</p>'+
        '</div>'+
    '</div>';
    return $("<li></li>")
        .data("item.autocomplete", item)
        .append(inner_html)
        .appendTo(ul);
};

$("#form-schedule input[name='phone']").autocomplete({
    minLength: 5,
    source: function( request, response ) {
        var r = [];

        formData= new FormData();
        formData.append("phone", request.term);

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/patient/search",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $("#form-schedule #loading-phone").removeClass("hide");
            },
            success: function(data){
                $("#form-schedule #loading-phone").addClass("hide");

                if(!data.error) {
                    r.push({value:data.id,
                        id:data.id,
                        email:data.email,
                        phone_code:data.phone_code,
                        phone:data.phone,
                        name:data.name,
                        photo:data.photo
                    });

                    response(r);         
                } else {
                    response(r);
                }
                
            },
            error: function(){
                response(r);
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {        
        getPatient(ui.item.id, ui.item.name, ui.item.photo);
        return false;
    },
}).data("ui-autocomplete")._renderItem = function (ul, item) {
    var inner_html = '<div class="autocomplete">'+
        thumbnail('md','patient','{{ $storage }}',item.photo,item.name)+
        '<div class="text">'+
            '<h1>'+item.name+'</h1>'+
            '<p>'+nullToEmpty(item.email)+'</p>'+
            '<p>+'+item.phone_code+''+item.phone+'</p>'+
        '</div>'+
    '</div>';
    return $("<li></li>")
        .data("item.autocomplete", item)
        .append(inner_html)
        .appendTo(ul);
};



function alertSearchPatient(val) {
    $("#form-search-patient #section-alert").removeClass("hide");
    $("#form-search-patient #alert-search-patient").html(val);
}

$('#search-patient').on('show.bs.modal', function (e) {
    resetValidation('form-search-patient #q');
    $("#form-search-patient #section-alert").addClass("hide");
    $("#form-search-patient #alert-search-patient").html("");
    $("#form-search-patient input[name='q']").val("");
});

$('#form-search-patient input[name=q]').focus(function() {
    resetValidation('form-search-patient #q');
});

$("#form-search-patient").submit(function(event) {
    event.preventDefault();
    resetValidation('form-search-patient #q');
    $("#form-search-patient #section-alert").addClass("hide");
    $("#form-search-patient #alert-search-patient").html("");

    var error = false;
    var q = $("#form-search-patient input[name='q']").val();

    if(q=="") {
        error=true;
        formValidate(false, ['form-search-patient #q',' ']);
    }

    if(error==false) {
        $("#form-search-patient .btn-primary").attr("disabled", true);
        $("#form-search-patient .btn-primary").addClass("loading");
        $("#form-search-patient .btn-primary span").removeClass("hide");

        formData = new FormData();
        formData.append("q", q);

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/{{ $clinic->id }}/patient/search",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                

                $("#form-search-patient .btn-primary").attr("disabled", false);

                $("#form-search-patient .btn-primary").removeClass("loading");
                $("#form-search-patient .btn-primary span").addClass("hide");

                if(!data.error) {
                    getPatient(data.id, data.name, data.photo);

                    $("#search-patient").modal("toggle");
                } else {
                    alertSearchPatient("{{ trans('validation.failed_search_patient') }}");
                }
            },
            error: function(){
                $("#form-search-patient .btn-primary").attr("disabled", false);

                $("#form-search-patient .btn-primary").removeClass("loading");
                $("#form-search-patient .btn-primary span").addClass("hide");

                alertSearchPatient("{{ trans('validation.failed') }}");
            }
        })
    }
});


function resetSchedule() {
    $("#form-schedule #appointment-group").addClass("hide");
    $("#form-schedule input[name='set_appointment']").prop("checked",false);
    $("#form-schedule input[name='appointment']").prop("disabled",true);
    $("#form-schedule input[name='appointment']").val("");
    $("#form-schedule input[name='date']").prop("disabled", true);
    $("#form-schedule input[name='date']").val("");
}

function listSchedule(val) {
    resetSchedule();

    doctorEndDate = "31/12/9999";
    doctorStartLeaveDate = "";
    doctorStartEndLeaveDate = "";

    if(val!="") {
        loading_content("#form-schedule #schedule-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/doctor/"+val+"/clinic/{{ $clinic->id }}",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                

                if(!data.error) {
                    loading_content("#form-schedule #schedule-group", "success");
                    if(data.schedule!=null) {
                        item = data.schedule.daily;

                        doctorEndDate = data.schedule.end_date_active;
                        doctorStartLeaveDate = data.schedule.start_date_leave;
                        doctorEndLeaveDate = data.schedule.end_date_leave;

                        $("#form-schedule select[name=schedule]").html("");

                        if(item.length==0) {
                            $("#form-schedule select[name=schedule]").prop("disabled", true);
                            $("#form-schedule select[name=schedule]").append("<option value=''>{{ trans('messages_web.no_schedule') }}</option>");
                            $("#form-schedule select[name=schedule]").select2({
                                placeholder: "{{ trans('messages_web.no_schedule') }}"
                            });
                        } else {
                            $("#form-schedule select[name=schedule]").prop("disabled", false);
                            $("#form-schedule select[name=schedule]").append("<option value=''></option>");

                            var checkSchedule = false;
                            for (var i = 0; i < item.length; i++) {
                                $("#form-schedule select[name=schedule]").append("<option data-day='"+item[i].day+"' data-appointment='"+item[i].appointment+"' value='"+item[i].id+"'>"+getDay(item[i].day)+" ("+convertTime(item[i].start_time)+" - "+convertTime(item[i].end_time)+")</option>");
                            }

                            $("#form-schedule select[name=schedule]").select2({
                                placeholder: "-- {{ trans('messages_web.select_schedule') }} --"
                            });
                            
                        }

                    } else {
                        $("#form-schedule select[name=schedule]").prop("disabled", true);
                        $("#form-schedule select[name=schedule]").append("<option value=''>{{ trans('messages_web.no_schedule') }}</option>");
                        $("#form-schedule select[name=schedule]").select2({
                            placeholder: "{{ trans('messages_web.no_schedule') }}"
                        });
                    }
                } else {
                    loading_content("#form-schedule #schedule-group", "failed");

                    $("#form-schedule #schedule-group #loading-content").click(function(){ listSchedule(val); });
                }
                
            },
            error: function(){
                loading_content("#form-schedule #schedule-group", "failed");

                $("#form-schedule #schedule-group #loading-content").click(function(){ listSchedule(val); });
            }
        });
    } else {
        $("#form-schedule select[name=schedule]").html("");
        $("#form-schedule select[name=schedule]").prop("disabled", true);
        $("#form-schedule select[name=schedule]").append("<option value=''>{{ trans('messages_web.no_schedule') }}</option>");
        $("#form-schedule select[name=schedule]").select2({
            placeholder: "{{ trans('messages_web.no_schedule') }}"
        });
    }
}

listSchedule("{{ $doctor->id }}");


function listDate(val, appointment) {
    if(val!="") {

        if(appointment==1) {
            $("#form-schedule #appointment-group").removeClass("hide");
            $("#form-schedule input[name='set_appointment']").prop("checked",true);
            $("#form-schedule input[name='appointment']").prop("disabled",false);
            $("#form-schedule input[name='appointment']").val("");
        } else {
            $("#form-schedule #appointment-group").addClass("hide");
            $("#form-schedule input[name='set_appointment']").prop("checked",false);
            $("#form-schedule input[name='appointment']").prop("disabled",true);
            $("#form-schedule input[name='appointment']").val("");
        }

        if(val==7) {
            val=0;
        }

        var dateDay = [];
        for(i=0;i<7;i++) {
            if(i!=val) {
                dateDay.push(i);
            }
        }

        $("#form-schedule #date-section").html('<div id="datepicker_date" class="input-group datetimepicker">'+
                                            '<input class="form-control" type="text" placeholder=\'{{ trans("messages_web.date") }}\' name="date" disabled="disabled" />'+
                                            '<span class="input-group-addon">'+
                                                '<span class="glyphicon glyphicon-calendar"></span>'+
                                            '</span>'+
                                        '</div>');

        $("#form-schedule input[name='date']").prop("disabled", false);

        var date = new Date();
        date.setDate(date.getDate());

        var min_date = new Date();
        min_date.setDate(min_date.getDate() - 1);

        var disabled_dates = [];

        if(doctorStartLeaveDate!="0000-00-00" && doctorEndLeaveDate!="0000-00-00") {

            var start_min_date = new Date();
            start_min_date.setDate(start_min_date.getDate()); 

            var end_min_date = new Date();
            end_min_date.setDate(end_min_date.getDate() - 1);  

            for (var d = new Date(doctorStartLeaveDate.replace(/\-/g,'/')); d <= new Date(doctorEndLeaveDate.replace(/\-/g,'/')); d.setDate(d.getDate()+1)) {

                monthLoop = ((d.getMonth()+1)+100).toString().slice(-2);
                dayLoop = (d.getDate()+100).toString().slice(-2);
                dateLoop = d.getFullYear()+'/'+monthLoop+'/'+dayLoop;
                disabled_dates.push(dateLoop);
                
                if(Date.parse(start_min_date)>=Date.parse(new Date(doctorStartLeaveDate.replace(/\-/g,'/'))) && Date.parse(end_min_date)<=Date.parse(new Date(doctorEndLeaveDate.replace(/\-/g,'/')))) {
                    if(Date.parse(min_date)<=Date.parse(new Date(dateLoop))) {
                        min_date.setDate(new Date(dateLoop).getDate());
                    }
                }

            }
        }

        disabled_dates.push(min_date);

        $('#datepicker_date').datetimepicker({
            locale: '{{ $lang }}',
            minDate: min_date,
            maxDate: doctorEndDate.replace(/\-/g,'/'),
            format: 'DD/MM/YYYY',            
            daysOfWeekDisabled: dateDay,
            disabledDates: disabled_dates
        });


    } else {
        $("#form-schedule input[name='date']").prop("disabled", true);
        $("#form-schedule input[name='date']").val("");
    }
}

$('#form-schedule select[name=schedule]').on('change', function() {
    var schedule = $(this).find(':selected').data('day');
    var appointment = $(this).find(':selected').data('appointment');
    
    listDate(schedule, appointment);
});

$('#form-schedule input[name="set_appointment"]').click(function() {    
    var checked = this.checked;
    if(checked==true) {
        $("#form-schedule input[name='appointment']").prop("disabled",false);
        $("#form-schedule input[name='appointment']").val("");
    } else {
        $("#form-schedule input[name='appointment']").prop("disabled",true);
        $("#form-schedule input[name='appointment']").val("");
    }
});


$('#form-schedule input[name=email]').focus(function() {
    resetValidation('form-schedule #email');
});

$('#form-schedule input[name=phone]').focus(function() {
    resetValidation('form-schedule #phone');
});

$('#form-schedule input[name=name]').focus(function() {
    resetValidation('form-schedule #name');
});

$('#form-schedule input[name=gender]').click(function() {
    resetValidation('form-schedule #gender');
});

$('#form-schedule input[name="birth_date"]').focus(function() {
    resetValidation('form-schedule #birth_date');
});

$("#form-schedule #schedule-group .border-group").click(function() {
    resetValidation('form-schedule #schedule');
});

$("#form-schedule #date-group").click(function() {
    resetValidation('form-schedule #date');
});

$('#form-schedule input[name="set_appointment"]').click(function() {
    resetValidation('form-schedule #appointment');
});

$('#form-schedule input[name="appointment"]').focus(function() {
    resetValidation('form-schedule #appointment');
});




function alertSchedule(val) {
    $("#form-schedule #section-alert").removeClass("hide");
    $("#form-schedule #alert-schedule").html(val);
    $('html, body').animate({
        scrollTop: $("#form-schedule #section-alert").offset().top - 100
    }, 0);
}

$("#form-schedule").submit(function(event) {
    $("#form-schedule #section-alert").addClass("hide");
    $("#form-schedule #alert-schedule").html("");

    resetValidation('form-schedule #email', 'form-schedule #phone', 'form-schedule #name', 'form-schedule #birth_date', 'form-schedule #gender', 'form-schedule #schedule', 'form-schedule #date');

    event.preventDefault();
    $("#form-schedule button").attr("disabled", true);

    formData= new FormData();

    var gender = $("#form-schedule input[name='gender']:checked").val();
    if(gender==undefined) {
        gender="";
    }

    formData.append("validate", false);
    formData.append("id_patient", $("#form-schedule input[name='id_patient']").val());
    formData.append("email", $("#form-schedule input[name='email']").val());    
    formData.append("phone_code", $("#form-schedule select[name='phone_code'] option:selected").val());
    formData.append("phone", parseInt($("#form-schedule input[name='phone']").val()));
    formData.append("name", $("#form-schedule input[name='name']").val());
    formData.append("birth_date", convertDate($("#form-schedule input[name='birth_date']").val()));
    formData.append("gender", gender);
    formData.append("type", "outpatient");
    formData.append("id_doctor_daily_schedule", $("#form-schedule select[name='schedule']").val());
    formData.append("appointment", $("#form-schedule input[name='appointment']").val());
    formData.append("id_clinic", "{{ $clinic->id }}");
    formData.append("id_doctor", "{{ $doctor->id }}");
    formData.append("id_polyclinic", "{{ $polyclinic->id }}");

    var date = $("#form-schedule input[name='date']").val();
    if(date!="") {
        date = convertDate(date);
    }
    formData.append("date", date);

    var errorAppointment = false;
    if($("#form-schedule select[name='schedule']").val()!="" && $("#form-schedule input[name='set_appointment']").is(':checked') && $("#form-schedule input[name='appointment']").val()=="") {
        formData.append("validate", true);

        formValidate(true, ['form-schedule #appointment','{{ trans("validation.empty_appointment") }}', true]);

        errorAppointment = true;
    }

    $("#form-schedule button").addClass("loading");
    $("#form-schedule button span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-schedule button").attr("disabled", false);
    
            $("#form-schedule button").removeClass("loading");
            $("#form-schedule button span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    formValidate(true, ['form-schedule #email',data.errors.email, true], ['form-schedule #phone',data.errors.phone, true], ['form-schedule #name',data.errors.name, true], ['form-schedule #birth_date',data.errors.birth_date, true], ['form-schedule #gender',data.errors.gender, true], ['form-schedule #schedule',data.errors.id_doctor_daily_schedule, true], ['form-schedule #date',data.errors.date, true]);

                    if(data.errors.schedule) {
                        alertSchedule(""+data.errors.schedule+"");
                    } 

                    if(data.errors.message) {
                        alertSchedule(""+data.errors.message+"");
                    } 

                } else {
                    if(errorAppointment==false) {
                        alertSchedule('<?php echo trans("validation.success_add_schedule"); ?>');

                        setTimeout(function(){
                            redirect('{{ url("/") }}/{{ $lang }}/schedule/report/'+data.id);
                        },500);
                    }
                }
        
            } else {
                alertSchedule("{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-schedule button").attr("disabled", false);
            
            $("#form-schedule button").removeClass("loading");
            $("#form-schedule button span").addClass("hide");

            alertSchedule("{{ trans('validation.failed') }}");
        }
    })
});


</script>
@endsection	
