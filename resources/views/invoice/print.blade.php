<script type="text/javascript">

function report(id, type) {
    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");

    if(type=="mobile") {
        type = "?mobile=true";
    } else {
        type = "";
    }

    $.ajax({ 
        url: "{{ $docs_url }}/{{ $lang }}/docs/invoice/"+id+""+type,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);
        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");

            $("#reload-document .reload").click(function(){ report(id, type); });
        }
    })
}

function reportInvoice(id, type) {
    if(type=="desktop") {
        $('#report-dialog').attr('class','modal-dialog modal-lg');
        $("input[name='id_report']").val(id);
        $("#report").modal("show");
        report(id, 'desktop');

        $("#desktop-report").addClass("hide");
        $("#mobile-report").removeClass("hide");
    } else {
        $('#report-dialog').attr('class','modal-dialog medium-report');
        $("input[name='id_report']").val(id);
        $("#report").modal("show");
        report(id, 'mobile');

        $("#desktop-report").removeClass("hide");
        $("#mobile-report").addClass("hide");
    }
}

$("#desktop-report").click(function() {
    reportInvoice($("input[name='id_report']").val(), 'desktop');
});

$("#mobile-report").click(function() {
    reportInvoice($("input[name='id_report']").val(), 'mobile');
});

</script>

