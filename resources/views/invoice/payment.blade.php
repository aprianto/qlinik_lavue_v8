@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.pay') }}
                    </h3>
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide">                    
                    <form role="form" class="sec" id="form-add">
                        <input type="hidden" name="category" value="" />
                        @if(empty($id) && empty($id_medical_records))
                        <div class="form-group" id="medical_records-group">
                            <label class="control-label">{{ trans('messages.medical_records') }} <span>*</span></label>
                            <div class="border-group">
                                <input name="medical_records" placeholder= "{{ trans('messages.select_medical_records') }}" type="text" class="form-control"/>
                            </div>
                            <span id="loading-medical-records" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.invoice_no') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.invoice_no') }}" name="invoice_no" disabled="dsabled">
                        </div>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.patient') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="name">
                                    <option value=""></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.number_medical_records') }}</label>
                            <input type="text" class="form-control" placeholder="<?php echo trans('messages.mrn'); ?>" name="mrn" disabled="dsabled">
                        </div>
                        <div class="form-group">
                            <div id="accordion" class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading gray" data-target="#section_detail-patient" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed">
                                            {{ trans('messages.patient_data') }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="section_detail-patient" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="cell hide" id="loading-detail_patient">
                                                <div class="card">
                                                    <span class="three-quarters">Loading&#8230;</span>
                                                </div>
                                            </div>
                                            <div class="cell hide" id="reload-detail_patient">
                                                <div class="card">
                                                    <span class="reload fa fa-refresh"></span>
                                                </div>
                                            </div>
                                            <div  id="panel-detail_patient">
                                                <ul class="detail-profile">
                                                    <div>
                                                        <li class="profile-features">
                                                            <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.phone_number') }}</span>
                                                                    <span id="phone"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.email') }}</span>
                                                                    <span id="email"></span>
                                                                </div>
                                                            </div>
                                                        </li>                        
                                                        <li class="profile-features">  
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.birth_date') }}</span>
                                                                    <span id="birth_date"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">  
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.gender') }}</span>
                                                                    <span id="gender"></span>
                                                                </div>
                                                            </div>
                                                        </li>     
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.no_identity') }}</span>
                                                                    <span id="no_identity"></span>
                                                                </div>
                                                            </div>
                                                        </li>            
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.blood_type') }}</span>
                                                                    <span id="blood_type"></span>
                                                                </div>
                                                            </div>
                                                        </li>                        
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.smoking') }}?</span>
                                                                    <span id="smoke"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <h4>{{ trans("messages.address") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.address') }}</span>
                                                                    <span id="address"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <h4>{{ trans("messages.patient_data") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.patient_category') }}</span>
                                                                    <span id="patient_category"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/payment-plan.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.financing') }}</span>
                                                                    <span id="financing"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="list-service">
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active" data-target="#section_service" data-parent="#accordion" data-toggle="collapse">
                                            <h4 class="panel-title">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.service') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_service">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item">                
                                                </ul>
                                                @if(empty($id)) 
                                                <div class="btn-center-sec">           
                                                    <a data-toggle="modal" id="add-service_true" data-target="#service" data-id=""  class="btn-add hide">+ {{ trans('messages.add_service') }}</a>
                                                    <a id="add-service_false" onclick="notif(false,'<?php echo trans("validation.empty_patient"); ?>')" class="btn-add">+ {{ trans('messages.add_service') }}</a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active" data-target="#section_medicine" data-parent="#accordion" data-toggle="collapse">
                                            <h4 class="panel-title">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.medicine') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_medicine">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item-medicine">
                                                </ul>
                                                @if(empty($id))      
                                                <div class="btn-center-sec">
                                                    <a data-toggle="modal" id="add-medicine_true" data-target="#medicine" data-id=""  class="btn-add hide">+ {{ trans('messages.add_medicine') }}</a>
                                                    <a id="add-medicine_false" onclick="notif(false,'<?php echo trans("validation.empty_patient"); ?>')" class="btn-add">+ {{ trans('messages.add_medicine') }}</a>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active" data-target="#section_vaccine" data-parent="#accordion" data-toggle="collapse">
                                            <h4 class="panel-title">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.vaccine') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_vaccine">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item-vaccine">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="subtotal" value="0" />
                                    <input type="hidden" name="total" value="0" />
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading gray active" data-target="#section_note" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title">
                                          <a class="accordion-toggle collapsed">
                                          {{ trans('messages.note') }}
                                          </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse in" id="section_note">
                                        <div class="panel-body panel-sec">
                                            <label class="control-label">{{ trans('messages.note') }}</label>
                                            <div class="form-group" id="note-group">
                                                <input type="text" class="form-control" placeholder="{{ trans('messages.input_note') }}" name="note" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="payment-sec no-padding" id="item-payment_type">
                            <ul>
                                <li id="section-subtotal">
                                    <div class="list-info">
                                        <div class="left">
                                            <h3>{{ trans("messages.subtotal") }}</h3>
                                        </div>
                                        <div class="right">
                                            <div class="column">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="subtotal">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="new-line" id="section-discount">
                                    <div class="list-info">
                                        <img src="{{ asset('assets/images/icons/info/discount.png') }}" />
                                        <div class="left">
                                            <h3><span>{{ trans("messages.discount") }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column">
                                                <div class="switch">
                                                    <input type="checkbox" id="discount_type" name="discount_type" class="hide" checked="true" />
                                                    <div id="discount_number" class="active" @if(empty($id)) onclick="switchButton('form-add #discount_percent', 'form-add #discount_number','form-add #discount_type',true)" @endif >{{ trans('messages.$') }}</div>
                                                    <div id="discount_percent" class="non-active" @if(empty($id)) onclick="switchButton('form-add #discount_number', 'form-add #discount_percent','form-add #discount_type',false)" @endif >{{ trans('messages.%') }}</div>
                                                </div>
                                            </div>
                                            <div class="column column-right">
                                                <input type="text" id="discount" name="discount" placeholder="0" onkeydown="number(event)" autocomplete="off" class="input-discount">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="section-discount_text" class="new-line hide">
                                    <div class="list-info">
                                        <img src="{{ asset('assets/images/icons/info/discount.png') }}" />
                                        <div class="left">
                                            <h3><span>{{ trans("messages.discount") }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number" id="section-discount_text_type">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-discount_text_value">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>                  
                                <li>
                                    <div class="list-info" style="border-top: 1px solid #f0f0f0;">
                                        <div class="left">
                                            <h3>{{ trans('messages.total') }}</h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="total">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="hide" id="section-insurance">
                                    <div class="list-info">
                                        <div class="left">
                                            <h3><span>{{ trans("messages.insurance") }}</span> (<b id="insurance-name"></b>)</h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_insurance"></span>
                                                <input type="text" id="section-value_insurance" name="insurance_amount" placeholder="0" onkeydown="number(event)" class="input-discount" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="section-prepayment" class="hide">
                                    <div class="list-info">
                                        <div class="left">
                                            <h3><span>{{ trans("messages.prepayment") }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_prepayment">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="section-remaining_payment" class="hide">
                                    <div class="list-info" style="border-top: 1px solid #f0f0f0;">
                                        <div class="left">
                                            <h3>{{ trans("messages.total_payment") }}</h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_remaining_payment">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="section-amount_paid" class="hide">
                                    <div class="list-info">
                                        <div class="left">
                                            <h3><span>{{ trans('messages.amount_paid') }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_amount_paid">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>                                
                                <li id="section-cash">
                                    <div class="list-info">
                                        <img src="{{ asset('assets/images/icons/info/pay-cash.png') }}" />
                                        <div class="left">
                                            <h3><span>{{ trans("messages.cash") }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_cash">
                                                    <input type="text" name="cash" placeholder="0" onkeydown="number(event)" autocomplete="off" oninput="convertNumber(this)">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li id="section-credit_card">
                                    <div class="list-info">
                                        <img src="{{ asset('assets/images/icons/info/pay-credit-card.png') }}" />
                                        <div class="left">
                                            <h3><span>{{ trans("messages.credit_card") }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_credit_card">
                                                    <input type="text" name="credit_card" placeholder="0" onkeydown="number(event)" autocomplete="off" oninput="convertNumber(this)">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="new-line" id="section-pay_online">
                                    <div class="list-info">
                                        <img src="{{ asset('assets/images/Pay_Online.png') }}" />
                                        <div class="left">
                                            <h3><span>{{ trans('messages.pay_online') }}</span></h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="section-text_pay_online">
                                                    <input type="text" name="pay_online" placeholder="0" onkeydown="number(event)" autocomplete="off" oninput="convertNumber(this)">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="new-line hide" id="section-change">
                                    <div class="list-info">
                                        <div class="left">
                                            <h3>{{ trans("messages.change") }}</h3>
                                        </div>
                                        <div class="right">
                                            <div class="column number">Rp</div>
                                            <div class="column column-right">
                                                <span class="number" id="change">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group last-item" id="section-btn_submit">
                            <button class="btn btn-primary" id="add">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}                                
                            </button>
                            <a href="#/{{ $lang }}/invoice" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window window-medium" id="service" role="dialog" aria-labelledby="serviceTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" />
                <h5 class="modal-title" id="title-service">{{ trans('messages.add_service') }}</h5>
            </div>
            <form id="form-service">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id_item" value="" />
                    <input type="hidden" name="id_service" value="" />
                    <div class="form-group" id="service-group">
                        <label class="control-label">{{ trans('messages.service') }} <span>*</span></label>
                        <div class="border-group">
                            <input class="form-control" name='service' placeholder="Cari layanan" data-id="" />
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="service_price-group">
                        <label class="control-label">{{ trans('messages.price') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_price') }}" name="service_price" disabled="dsabled">
                    </div>
                    <div class="form-group hide" id="admin_cost-group">
                        <label class="control-label">{{ trans('messages.admin_cost_symbol') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.admin_cost') }}" name="admin_cost" disabled="dsabled">
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="qty-group">
                                <label class="control-label">{{ trans('messages.qty') }} <span>*</span></label>
                                <input type="text" class="form-control" placeholder="{{ trans('messages.input_qty') }}" name="qty" onkeydown="number(event)" autocomplete="off">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="price-group">
                                <label class="control-label">{{ trans('messages.total_price') }}</label>
                                <input type="text" class="form-control" placeholder="{{ trans('messages.price') }}" name="price" disabled="dsabled">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="implementer-group">
                        <label class="control-label">{{ trans('messages.implementer') }} <span class="hide" id="select_implementer">*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="implementer">
                                <option>{{ trans('messages.select_implementer') }}</option>
                            </select>            
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span> 
                        <span class="help-block"></span>
                        <input type="checkbox" class="hide" name="select_implementer" />
                    </div>
                    <div class="form-group" id="discount-group">
                        <label class="control-label">{{ trans('messages.discount') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_discount') }}" id="discount" name="discount" onkeydown="number(event)" autocomplete="off">
                        <div class="switch switch-right">
                            <input type="checkbox" id="discount_type" name="discount_type" class="hide" checked="true" />
                            <div id="discount_number" class="active" onclick="switchButton('service #discount_percent', 'service #discount_number','service #discount_type',true)">{{ trans('messages.$') }}</div>
                            <div id="discount_percent" class="non-active" onclick="switchButton('service #discount_number', 'service #discount_percent','service #discount_type',false)">{{ trans('messages.%') }}</div>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="total_price-group">
                        <label class="control-label">{{ trans('messages.total') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.total_price') }}" name="total_price" disabled="dsabled">
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('messages.note') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_note') }}" name="note" />
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade window" id="medicine" role="dialog" aria-labelledby="medicineTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="title-medicine">{{ trans('messages.add_medicine') }}</h5>
            </div>
            <form id="form-medicine">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id_item" value="" />
                    <input type="hidden" name="id_medicine" value="" />
                    <input type="hidden" name="id" value="" />
                    
                    <input type="hidden" name="unit_medicine" value="" />
                    <input type="hidden" name="name_signa3" value="" />
                    <input type="hidden" name="val_qty_unit" value="" />
                    <input type="hidden" name="qty_unit_checked" value="" />
                    <input type="hidden" name="id_unit_checked" value="" />
                    
                    <div class="form-group hide" id="check_racikan-group">
                        <div class="sf-check">
                            <label><input type="checkbox" value="1" name="is_racikan" data-trigger="is_racikan"><span></span> {{ trans('messages.concoction_medicine') }}</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group hide is_racikan" id="racikan-group">
                        <label class="control-label">{{ trans('messages.concoction') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('validation.empty_name_concoction') }}" name="racikan" />
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group" id="medicine_name-group">
                        <label class="control-label">{{ trans('messages.inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <input name="medicine_name" class="form-control" multiple="multiple" data-id="" data-code="" placeholder="{{ trans('messages.search_inventory') }}" />
                        </div>
                        <span id="loading-content-name" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('messages.medicine_code') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.medicine_code') }}" name="code" disabled="dsabled">
                    </div>

                    <div class="form-group hide is_racikan" id="unit_medicine-group">
                        <label class="control-label">{{ trans('messages.unit') }}</label>
                        <div class="border-group">
                            <select class="form-control" name="unit_medicine">
                                <option value=""></option>
                            </select>
                        </div>
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group" id="interaction-group">
                        <label class="control-label">{{ trans('messages.record_of_medicine_interactions') }}</label>
                        <input name='notes' type="text" class="form-control" placeholder="{{ trans('messages.add_record_of_medicine_interactions') }}" disabled="disabled" />
                    </div>

                    <div class="form-group" id="medicine_dose-group">
                        <label class="control-label">{{ trans('messages.dose_signa') }} <span>*</span></label>
                        <div class="row" style="margin-bottom: 16px;">
                            <div class="col-xs-3 signa1-group">
                                <input class="form-control" placeholder="{{ trans('messages.dose') }}" name="signa1" onkeydown="number(event)" autocomplete="off" oninput="getDay()" min="1" />
                                <span class="help-block"></span>
                                <p>{{ trans('messages.consumption') }}</p>
                            </div>
                            <div class="col-xs-1 multiplication">
                                <span>x</span>
                            </div>
                            <div class="col-xs-3 signa2-group">
                                <input class="form-control" placeholder="{{ trans('messages.unit') }}" name="signa2" onkeydown="number(event)" autocomplete="off" oninput="getDay()" min="1" />
                                <span class="help-block"></span>
                                <p></p>
                            </div>
                            <div class="col-xs-5 signa3-group" id="one_day-group">
                                <select class="form-control" name="signa3">
                                    <option value=""></option>
                                    <option value="per hari">{{ trans('messages.one_day') }}</option>
                                    <option value="per minggu">{{ trans('messages.one_week') }}</option>
                                    <option value="per bulan">{{ trans('messages.one_month') }}</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div id="duration-group">
                            <label class="control-label">{{ trans('messages.duration') }} <span>*</span></label>
                             <div class="content-field-row">
                                <div>
                                    <input class="form-control" placeholder="{{ trans('messages.add_duration') }}" name="duration" id="duration_day" min="1" />
                                    <span class="help-block"></span>
                                </div>
                                <span>{{ trans('messages.day')}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group hide is_racikan">
                        <div class="row">
                            <div class="col-xs-6" id="medicine_strong-group">
                                <label class="control-label">{{ trans('messages.strength_of_package') }} <span>*</span></label>
                                <input type="number" name='power_medicine' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off" oninput="getQtyMedicine()" />
                                <span class="help-block"></span>
                            </div>
                            <div class="col-xs-6" id="medicine_qty-group">
                                <label class="control-label">{{ trans('messages.quantity_package') }} <span>*</span></label>
                                <input type="number" name='qty_demand' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off" oninput="getQtyMedicine()"/>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hide is_racikan">
                        <div class="row">
                            <div class="col-xs-6" id="qty_item_medicine-group">
                                <label class="control-label">{{ trans('messages.quantity_medicine') }}</label>
                                <input class="form-control" placeholder="{{ trans('messages.enter_of_quantity_medicine') }}" name="qty_item_medicine" disabled onkeydown="number(event)" autocomplete="off" oninput="getDay()"/>
                                <span id="total-stock-racikan" class="notif-notes"></span>
                            </div>
                            <div class="col-xs-6" id="total_day-group">
                                <label class="control-label">{{ trans('messages.total_day') }}</label>
                                <input class="form-control" placeholder="{{ trans('messages.enter_total_day') }}" name="total_day" disabled onkeydown="number(event)" autocomplete="off"/>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hide is_racikan">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">{{ trans('messages.price') }}</label>
                                <input class="form-control" placeholder="{{ trans('messages.input_price') }}" name="price_unit_racikan" disabled />
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <label class="control-label" id="label-qty">{{ trans('messages.qty') }} <span>*</span></label>
                    <div class="form-group hide" id="medicine_bhp">
                        <div class='sf-radio'>
                            <div class='wrapper-label-radio'>
                                <label><input type='radio' name='radio_bhp' id='change_unit_bhp' data-category="" /><span></span></label>
                                <p class='notes'>{{ trans('messages.another_amount') }}</p>
                            </div>
                            <div class='row-title row-title-space'>
                                <div class='col-xs-7 form-group'>
                                    <label class='control-label'>{{ trans('messages.qty') }}</label>
                                    <div>
                                        <input class='form-control' type='text' placeholder="{{ trans('messages.input_quantity') }}" name='value_unit_bhp' onkeydown='number(event)' autocomplete='off' oninput='setNotifStock(event)' />
                                        <span id='notes-change-unit-bhp' class='notif-notes'></span>
                                    </div>
                                </div>
                                <div class='col-xs-5' id="unit_bhp-group">
                                    <label class='control-label'>{{ trans('messages.unit') }}</label>
                                    <div class="border-group">
                                        <select class='form-control' name='unit_bhp'>
                                            <option value=''></option>
                                        </select>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div style="padding: 0px 16px 0px 30px">
                                <label class="control-label">{{ trans('messages.price') }}</label>
                                <div class="form-group">
                                    <input class="form-control" placeholder="{{ trans('messages.input_price') }}" name="price_bhp" disabled="disabeld" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="qty_radio_button-group"></div>
                    <div class="form-group" id="instructions-group">
                        <label class="control-label">{{ trans('messages.instructions_for_use') }} <span>*</span></label>
                        <input class="form-control"  placeholder="{{ trans('messages.add_instructions_for_use') }}" name="instructions" />
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit" id="save-inventory">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="{{ asset('assets/js/function.js') }}"></script>

<script type="text/javascript">


/*
 * VACCINE FEATURE
 */
// Load vaccines data to FE
var vaccineCount = 0;

function processItemVaccine(vaccines, doctorId) {
    vaccineCount = vaccines.length
    vaccines.forEach((item, index) => {
        const total = item.quantity*getZero(item.price).toFixed(0)
        let section = '<div class="item-sec">'+
                        '<div class="left-sec">'+
                            '<h1>'+ item.name +'</h1>'+
                            '<span class="text">{{ trans("messages.quantity") }}: '+ item.quantity +' '+ item.unit_name +'</span>'+
                        '</div>'+
                        '<div class="right-sec text-right">'+
                            '<span class="money">'+formatCurrency('{{ $lang }}','Rp', total)+'</span>'+
                        '</div>'+
                    '</div>'
                    +'<input type="hidden" name="vaccine_id_medicine['+index+']" value="'+item.id_medicine+'">'
                    +'<input type="hidden" name="vaccine_id_unit['+index+']" value="'+item.id_unit+'">'
                    +'<input type="hidden" name="vaccine_batch_no['+index+']" value="'+item.batch_no+'">'
                    +'<input type="hidden" name="vaccine_id_doctor['+index+']" value="'+doctorId+'">'
                    +'<input type="hidden" name="vaccine_price['+index+']" value="'+item.price+'">'
                    +'<input type="hidden" name="vaccine_quantity['+index+']" value="'+item.quantity+'">'
                    +'<input type="hidden" name="vaccine_total['+index+']" value="'+total+'">'
                  
        $("#item-vaccine").append('<li id="item-vaccine-'+ index +'")'+ section +'</li>')

        getSubtotal = $("#form-add input[name='subtotal']").val()
        subtotal = parseInt(getSubtotal) + parseInt(total)
        $("#form-add input[name='subtotal']").val(subtotal)
        $("#form-add #subtotal").html(formatCurrency('{{ $lang }}','',subtotal))
        getTotal()
        getPay()
    })
}

function appendVaccineToFormData(formData) {
    for (let i = 0; i < vaccineCount; i++) {
        const medicineId = $("#form-add input[name='vaccine_id_medicine["+i+"]']").val()
        const unitId = $("#form-add input[name='vaccine_id_unit["+i+"]']").val()
        const batchNo = $("#form-add input[name='vaccine_batch_no["+i+"]']").val()
        const doctorId = $("#form-add input[name='vaccine_id_doctor["+i+"]']").val()
        const price = $("#form-add input[name='vaccine_price["+i+"]']").val()
        const quantity = $("#form-add input[name='vaccine_quantity["+i+"]']").val()
        const total = $("#form-add input[name='vaccine_total["+i+"]']").val()

        formData.append(`vaccines[${i}][id_medicine]`, medicineId)
        formData.append(`vaccines[${i}][id_unit]`, unitId)
        formData.append(`vaccines[${i}][id_signa]`, '')
        formData.append(`vaccines[${i}][batch_no]`, batchNo)
        formData.append(`vaccines[${i}][id_doctor]`, doctorId)
        formData.append(`vaccines[${i}][price]`, price)
        formData.append(`vaccines[${i}][quantity]`, quantity)
        formData.append(`vaccines[${i}][total]`, total)
    }
}
/* END */

var insurance = false;
var insurance_name = "";
var id_medical_records_global = null;

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();

hoursNow = (date.getHours()+100).toString().slice(-2);
minutesNow = (date.getMinutes()+100).toString().slice(-2);
secondsNow = (date.getSeconds()+100).toString().slice(-2);

yearKey = (date.getFullYear()).toString().slice(-1);

var invoice_no = yearKey+""+dayNow+""+hoursNow+""+minutesNow+""+secondsNow+""+monthNow+""+yearNow;

$("#form-add input[name='invoice_no']").val(invoice_no);

var item = 0;
var itemMedicine = 0;

$('#datepicker_date').datetimepicker({
    format: 'DD/MM/YYYY'
});

$("#form-add select[name='medical_records']").select2({
    placeholder: "{{ trans('messages.select_medical_records') }}"
});

$("#form-add select[name='blood_type']").select2({
    placeholder: "{{ trans('messages.blood_type') }}"
});

$("#form-add select[name='id_type']").select2({
    placeholder: "{{ trans('messages.id_type') }}"
});

$("#form-add select[name='district']").select2({
    placeholder: "{{ trans('messages.district') }}"
});

$("#form-add select[name='village']").select2({
    placeholder: "{{ trans('messages.village') }}"
});

$("#form-add select[name='regency']").select2({
    placeholder: "{{ trans('messages.regency') }}"
});

$("#form-add select[name='province']").select2({
    placeholder: "{{ trans('messages.province') }}"
});

$("#form-add select[name='country']").select2({
    placeholder: "{{ trans('messages.country') }}"
});

$("#form-add select[name='category']").select2({
    placeholder: "{{ trans('messages.patient_category') }}"
});

$("#form-add select[name='financing']").select2({
    placeholder: "{{ trans('messages.financing') }}"
});

$("select[name='signa3']").select2({
    placeholder: "{{ trans('messages.one_day') }}"
});

$("select[name='unit_medicine']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}"
});

function convertNumber(field) {
    $(field).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });
}

let timer = "";
$("#form-add input[name='medical_records']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        $("#form-add #loading-medical-records").removeClass("hide");
        let temp = [];
        clearTimeout(timer);
        timer = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/medical-records?unpaid=true&q="+request.term+"&per_page=10",
                success: function(results){
                    $("#form-add #loading-medical-records").addClass("hide");
                    for(i = 0; i < results.data.length; i++) {
                        let medical_record = results.data[i];
                        let data = medical_record.patient.name+" ("+medical_record.doctor.name+" - "+medical_record.polyclinic.name+" - "+unconvertDate(medical_record.date)+" "+nullToEmpty(medical_record.start_time)+")";
                        temp.push({id: medical_record.id, name: data, value: data});
                    }
                    response(temp);
                },
                error: function() {
                    $("#form-add #loading-medical-records").addClass("hide");
                    response(temp);
                }
            });
        }, 2500);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui) {
        $("#form-add input[name='medical_records']").val(ui.item.name);
        $("#form-add input[name='medical_records']").attr("data-id", ui.item.id);
        getMedical(ui.item.id);
        resetData();
        return false;
    }
});

function resetAdd() {
    $("#form-add select[name='name']").val("").trigger("change");
    resetData();

    $("#form-add input[name='note']").val("");
    
}

function resetData() {
    $("#form-add input[name='mrn']").val("");
    $("#form-add input[name='birth_date']").val("");
    $("#form-add input[name='age']").val("");
    $('#form-add input[name="gender"]:checked').each(function(){
        $(this).prop('checked', false); 
    });
    $("#form-add select[name='blood_type']").val("").trigger("change");
    $("#form-add select[name='id_type']").val("").trigger("change");
    $("#form-add input[name='identity']").val("");
    $("#form-add textarea[name='address']").val("");
    $("#form-add input[name='postal_code']").val("");
    $("#form-add select[name='district']").val("").trigger("change");
    $("#form-add select[name='village']").val("").trigger("change");
    $("#form-add select[name='regency']").val("").trigger("change");
    $("#form-add select[name='province']").val("").trigger("change");
    $("#form-add select[name='country']").val("").trigger("change");
    $("#form-add input[name='phone']").val("");
    $("#form-add select[name='category']").val("").trigger("change");
    $("#form-add select[name='financing']").val("").trigger("change");

    $("#form-add #item").html("");
    $("#form-add #item-medicine").html("");
    $("#form-add #item-vaccine").html("");
    $("#form-add #data-medicine").html('<tr>'
            +'<td colspan="6" align="center">{{ trans("messages.no_result") }}</td>'
            +'</tr>');

    $("#form-add input[name='discount']").val("");
    switchButton('form-add #discount_percent', 'form-add #discount_number','form-add #discount_type',true);
    $("#form-add input[name='subtotal']").val("0");
    $("#form-add #subtotal").html("0");
    $("#form-add input[name='prepayment']").val("");
    $("#form-add input[name='cash']").val("");
    $("#form-add input[name='credit_card']").val("");
    $("#form-add input[name='remaining_payment']").val("0");

    $("#form-add #item-payment").remove();

    getTotal();
}

function getPay() {
    let total = parseInt($("#form-add input[name='total']").val());
    let previousPay = $("#form-add input[name='prepayment']").val();
    let cash = $("#form-add input[name='cash']").val();
    let credit_card = $("#form-add input[name='credit_card']").val();
    let pay_online = $("#form-add input[name='pay_online']").val();

    previousPay = previousPay == "" ? 0 : parseInt(previousPay);
    cash = cash == "" ? 0 : parseInt(cash);
    credit_card = credit_card == "" ? 0 : parseInt(credit_card);
    pay_online = pay_online == "" ? 0 : parseInt(pay_online);

    let pay = 0;
    try {
        $('#form-add input[name="payment[]"]').each( function() {
            pay = parseInt(pay)+parseInt(this.value);
        });
    }
    catch(err) {
    }

    let remainingPay = total-previousPay-cash-credit_card-pay_online-pay;

    $("#form-add input[name='remaining_payment']").val(remainingPay);
}

function getTotal(){
    var discount_type = $("#form-add input[name='discount_type']").prop('checked');
    var discount = $("#form-add input[name='discount']").val();

    var subtotal = $("#form-add input[name='subtotal']").val();
    var total = subtotal;

    if(subtotal!=0) {
        if(discount!='') {
            if(discount_type==true) {
                total = total-discount;
            } else {
                total = Math.round(total-((discount/100)*total));
            }
        }
    }

    $("#form-add #total").html(formatCurrency('{{ $lang }}','',total));

    var insurance_amount = $("#form-add input[name='insurance_amount']").val()
    if(insurance_amount=='') {
        insurance_amount = 0;
    }

    var remaining_payment = null;
    if(parseInt(total) != 0 ){
        remaining_payment = parseInt(total)-parseInt(insurance_amount);
    }else{
        remaining_payment = parseInt(total);
    }

    if(insurance==true) {
        $("#form-add #section-remaining_payment").removeClass("hide");
        $("#form-add #section-remaining_payment #section-text_remaining_payment").html(formatCurrency('{{ $lang }}','',remaining_payment));
        @if(!empty($id))
            $("#form-add #section-insurance #section-value_insurance").addClass('hide');
            $("#form-add #section-insurance #section-text_insurance").html(formatCurrency('{{ $lang }}','',insurance_amount));
        @else
            $("#form-add #section-insurance #section-value_insurance").val(insurance_amount);
            $("#form-add #section-insurance #section-text_insurance").addClass('hide');
        @endif
        $("#form-add input[name='total']").val(remaining_payment);
    }else{
        $("#form-add #section-remaining_payment").addClass("hide");
        $("#form-add input[name='total']").val(total);
    }
}

$('.form-elements-sec').removeClass('hide');
$('#loading').addClass('hide');

@if(!empty($id))
    getInvoice('{{ $id }}');
    $("#form-add").submit(function(event) {
        event.preventDefault();
        editPayment('{{ $id }}');
    });
@elseif(!empty($id_medical_records))
    getMedical('{{ $id_medical_records }}');
    $("#form-add").submit(function(event) {
        event.preventDefault();
        addPayment();
    });
@else
    $("#form-add").submit(function(event) {
        event.preventDefault();
        addPayment();
    });
@endif

var insurance_total = 0;

function getMedical(val) {
    $('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $('.form-elements-sec').addClass('hide');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/"+val+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                id_medical_records_global = data.id;
                $('.form-elements-sec').removeClass('hide');
                $('#loading').addClass('hide');
                $("#form-add select[name=name]").append("<option value=''></option>");
                $("#form-add select[name=name]").append("<option value='"+data.patient.id+"' selected='selected'>"+data.patient.name+"</option>");
                $("#form-add select[name=name]").prop("disabled", true);
                
                $("#form-add select[name=name]").select2({
                    placeholder: "{{ trans('messages.select_patient') }}"
                });

                insurance_total = 0;
                if ( data.insurance != null ) {
                    insurance = true;
                    insurance_name = data.insurance.name;
                    $('#section-insurance').removeClass('hide');
                    $("#insurance-name").html(insurance_name);
                }else{
                    insurance = false;
                    $('#section-insurance').addClass('hide');
                }

                getPatient(data.patient.id, '');
                var getPatientCategory = data.patient.id_patient_category;

                for(var i = 0; i < data.service.length; i++) {

                    var item = data.service[i];

                    var cost = "";
                    if(item.service.cost_category.length > 0){
                        for(var j=0;j<item.service.cost_category.length;j++) {
                            if(item.service.cost_category[j].id_patient_category==getPatientCategory) {
                                cost = item.service.cost_category[j].cost;
                            }
                        }
                    }else{
                        notif(false, '{{ trans("validation.empty_service") }}');
                    }

                    if(cost=="") {
                        cost = item.service.cost;
                    }

                    if(item.service.kode_bpjs != null){
                        insurance_total += item.quantity*(cost+nullToZero(item.service.admin_cost));
                    }
                    var total = (item.quantity*(cost+nullToZero(item.service.admin_cost)));

                    processItem(item.id, item.service.name, item.quantity, cost, total, nullToZero(item.service.admin_cost), '', '', data.id_doctor, data.doctor.name, total, '', '');
                }

                for( i = 0; i < data.prescriptions.length; i++) {
                    let item = data.prescriptions[i];
                    let price;
                    let medicine = [];
                    let price_racikan = []
                    let racikan = item.concoction != null && item.concoction.name != null ? item.concoction.name : "";
                    let dosage = item.concoction != null ? item.concoction.dosage : "";
                    let instruction = item.concoction != null ? item.concoction.instruction : "";
                    let concoction = item.concoction != null ? item.concoction : "";
                    let quantity;
                    for (j = 0; j < item.medicines.length; j++) {
                        quantity = item.concoction != null ? item.concoction.total_package : item.medicines[j].quantity;
                        medicine[j] = item.medicines[j];
                        
                        if(item.medicines[j].price_list && item.medicines[j].price_list.length > 0){
                            price = item.medicines[j].price_list.find(price => price.id_patient_category === getPatientCategory && item.medicines[j].id_unit === price.id_unit)
                            price = price ? price.price : 0;
                            price_racikan[j] = concoction != null ? price : 0;
                            if (!price) {
                                notif(false, '{{ trans("messages.price_medicine") }} '+medicine[j].name+' {{ trans("messages.not_set") }}')
                            }
                        }else{
                            notif(false, '{{ trans("validation.empty_price_inventory") }}');
                            price = 0;
                        }

                        if(item.medicines[j].kode_dpho != null){
                            insurance_total += item.quantity * price;
                        }
                    }

                    processItemMedicine(medicine, item.signa.instruction_for_use, 
                        item.signa.dose, item.signa.per_day, item.signa.time_frame, 
                        item.signa.total_day, quantity, racikan, 
                        price, concoction, price_racikan, item.signa.id, '', '');
                }

                /* PROCESS VACCINE DATA TO FE */
                processItemVaccine(data.vaccines, data.id_doctor)
                
                if(data.insurance && data.insurance.name.toLowerCase().includes("bpjs")){
                    $("#form-add input[name='insurance_amount']").val(insurance_total);
                }
                getTotal();
            
            } else {
                $('#loading').addClass('hide');
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ getMedical(val); });
            }
        },
        error: function() {
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ getMedical(val); });
        }
    });
}

function getInvoice(val) {
    $('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $('.form-elements-sec').addClass('hide');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/"+val+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $('.form-elements-sec').removeClass('hide');
                $('#loading').addClass('hide');
                $("#form-add input[name='invoice_no']").val(data.number);
                $("#form-add select[name=name]").append("<option value=''></option>");
                $("#form-add select[name=name]").append("<option value='"+data.patient.id+"' selected='selected'>"+data.patient.name+"</option>");
                $("#form-add select[name=name]").prop("disabled", true);
                $("#form-add select[name=name]").select2({
                    placeholder: "{{ trans('messages.select_patient') }}"
                });

                $("#form-add input[name='discount']").val(data.discount);
                if(data.discount_type == "$") {
                    switchButton('form-add #discount_percent', 'form-add #discount_number','form-add input[name="discount_type"]',true);
                } else {
                    switchButton('form-add #discount_number', 'form-add #discount_percent','form-add input[name="discount_type"]',false);
                }

                getPatient(data.patient.id, true);
                for(i = 0; i < data.service.length; i++) {
                    let item = data.service[i];
                    let total_cost = item.cost*item.quantity;
                    if(item.invoice_doctor == null) {
                        id_doctor = "";
                        doctor = "";                        
                    } else {
                        id_doctor = item.invoice_doctor.id_doctor;
                        doctor = item.invoice_doctor.doctor.name;
                    }

                    processItem(item.id, item.service.name, item.quantity, item.cost, total_cost, nullToZero(item.admin_cost), nullToZero(item.discount), nullToEmpty(item.discount_type), id_doctor, doctor, item.total, nullToEmpty(item.note), '');
                }

                $("#form-add #title-item-service").removeClass("hide");

                if(data.prescriptions) {
                    data.prescriptions.forEach((prescription, key) => {
                        let racikan = prescription.concoction ? prescription.concoction.name : "";
                        let qty = !prescription.concoction ? prescription.medicines[0].quantity : prescription.concoction.total_package
                        let price;
                        let price_racikan = [];
                        let concoction = prescription.concoction ? prescription.concoction : "";
                        
                        prescription.medicines.forEach((medicine, index) => {
                            price = medicine.price;
                            price_racikan[index] = prescription.concoction ? medicine.price : 0;
                        });


                        let instruction_for_use = prescription.signa ? prescription.signa.instruction_for_use : "";
                        let dose = prescription.signa ? prescription.signa.dose : "";
                        let per_day = prescription.signa ? prescription.signa.per_day : "";
                        let time_frame = prescription.signa ? prescription.signa.time_frame : "";
                        let total_day = prescription.signa ? prescription.signa.total_day : "";
                        let signa_id = prescription.signa ? prescription.signa.id : "";

                        processItemMedicine(prescription.medicines, instruction_for_use, dose,per_day, 
                        time_frame, total_day, qty, racikan, price, concoction, price_racikan, signa_id, "", "")
                    });
                }else {
                    $("#form-add #title-item-medicine").removeClass("hide");
                }

                $("#form-add #section-discount").addClass("hide");
                $("#form-add #section-discount_text").removeClass("hide");
                $("#form-add #note-group").html(checkNull(data.note));

                if(nullToZero(data.discount) != 0) {
                    if(data.discount_type == "$") {
                        $("#form-add #section-discount_text #section-discount_text_value").html(formatCurrency('{{ $lang }}','',data.discount));
                    } else {
                        $("#form-add #section-discount_text #section-discount_text_type").html("");
                        $("#form-add #section-discount_text #section-discount_text_value").html(data.discount+"%");
                    }
                } else {
                    $("#form-add #section-discount_text").addClass("hide");
                    $("#form-add #section-subtotal").addClass("new-line");
                }

                $("#section-invoice").removeClass("hide");
                $("#section-status").removeClass("hide");
                $("#section-invoice #invoice_no").html(data.number);
                $("#section-invoice #date").html(getFormatDate(data.date));

                if(data.status.code == true) {
                    if(data.cash != 0) {
                        $("#form-add #section-cash #section-text_cash").html(data.cash);
                    } else {
                        $("#form-add #section-cash").addClass("hide");
                        $("#form-add #section-credit_card #section-credit_card_title").html("{{ trans('messages.amount_paid') }}");
                    }

                    if(data.credit_card!=0) {
                        $("#form-add #section-credit_card #section-text_credit_card").html(data.credit_card);
                    } else {
                        $("#form-add #section-credit_card").addClass("hide");
                    }
                    
                    if(data.online_payment != 0) {
                        $("#form-add #section-text_pay_online").html(data.online_payment);
                    }else {
                        $("#form-add #section-pay_online").addClass("hide");
                    }

                    $("#form-add #section-btn_submit").addClass("hide");

                    let cash = data.cash;
                    let credit_card = data.credit_card;
                    let prepayment = data.prepayment;

                    let insurance_amount = 0;
                    if(data.insurance != null) {
                        insurance_amount = data.insurance.amount;
                        insurance_name = data.insurance.name;

                        $("#form-add #section-insurance").removeClass("hide");
                        @if(!empty($id))
                            $("#form-add #section-insurance #section-value_insurance").addClass('hide');
                            $("#form-add #section-insurance #section-text_insurance").html(formatCurrency('{{ $lang }}','',insurance_amount));
                        @else
                            $("#form-add #section-insurance #section-value_insurance").val(insurance_amount);
                            $("#form-add #section-insurance #section-text_insurance").addClass('hide');
                        @endif
                        $("#form-add #section-insurance #insurance-name").html(insurance_name);
                    }

                    let total = data.total;
                    let amount_paid = parseInt(prepayment)+parseInt(cash)+parseInt(credit_card)+parseInt(data.online_payment)+parseInt(insurance);

                    if(prepayment > 0) {
                        $("#form-add #section-prepayment").removeClass("hide");
                        $("#form-add #section-prepayment #section-text_prepayment").html(formatCurrency('{{ $lang }}','',prepayment));
                    }

                    let change = (parseInt(prepayment)+parseInt(cash)+parseInt(credit_card)+parseInt(data.online_payment)+parseInt(insurance))-total;

                    if(change > 0) {
                        $("#form-add #section-change").removeClass("hide");
                        $("#form-add #section-change #change").html(change);
                    }


                    $("#section-invoice #paid").html(formatCurrency('{{ $lang }}','Rp',amount_paid));
                    if(amount_paid > 0) {
                        $("#section-status #status_pay").removeClass("disable");
                    }

                    $("#section-status #status_keel").removeClass("disable");
                    $("#section-invoice #status").addClass("success");
                    $("#section-invoice #status").html("{{ trans('messages.keel') }}");

                } else {
                    let cash = data.cash;
                    let credit_card = data.credit_card;

                    let insurance_amount = 0;
                    if(data.insurance != null) {
                        insurance_amount = data.insurance.amount;
                        insurance_name = data.insurance.name;

                        $("#form-add #section-insurance").removeClass("hide");
                        @if(!empty($id))
                            $("#form-add #section-insurance #section-value_insurance").addClass('hide');
                            $("#form-add #section-insurance #section-text_insurance").html(formatCurrency('{{ $lang }}','',insurance_amount));
                        @else
                            $("#form-add #section-insurance #section-value_insurance").val(insurance_amount);
                            $("#form-add #section-insurance #section-text_insurance").addClass('hide');
                        @endif
                        $("#form-add #section-insurance #insurance-name").html(insurance_name);
                    }

                    let discount = 0;
                    if(nullToZero(data.discount) != 0) {
                        if(data.discount_type=="$") {
                            discount = data.discount;
                        }else{
                            discount = (data.discount/100)*data.subtotal;
                        }
                    }

                    let total = data.total;
                    let prepayment = parseInt(data.prepayment)+parseInt(cash)+parseInt(credit_card)+parseInt(data.online_payment);
                    let remaining_payment = data.subtotal - (prepayment+parseInt(insurance_amount)+parseInt(discount));

                    if(prepayment > 0) {
                        $("#form-add #section-prepayment").removeClass("hide");
                        $("#form-add #section-prepayment #section-text_prepayment").html(formatCurrency('{{ $lang }}','',prepayment));
                    }
                    $("#form-add #section-remaining_payment").removeClass("hide");
                    $("#form-add #section-remaining_payment #section-text_remaining_payment").html(formatCurrency('{{ $lang }}','',remaining_payment));

                    let amount_paid = parseInt(prepayment)+parseInt(insurance);
                    
                    $("#section-invoice #paid").html(formatCurrency('{{ $lang }}','Rp',amount_paid));
                    if(amount_paid > 0) {
                        $("#section-status #status_pay").removeClass("disable");
                    } else {
                        $("#section-status #status_pay").addClass("disable");
                    }

                    $("#section-status #status_keel").addClass("disable");
                    $("#section-invoice #status").addClass("failed");
                    $("#section-invoice #status").html("{{ trans('messages.not_keel') }}");
                }
                $("#add-service_true").addClass("hide");
                $("#add-service_false").addClass("hide");
                $("#add-medicine_true").addClass("hide");
                $("#add-medicine_false").addClass("hide");
                $("#list-service .icon-sec").addClass("hide");
                $("#list-medicine .link-action").addClass("hide");
            } else {
                $('#loading').addClass('hide');
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ getInvoice(val); });
            }
        },
        error: function() {
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ getInvoice(val); });
        }
    });
}


function listPatient(val) {
    loading_content("#form-add #name-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient?active=true",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                loading_content("#form-add #name-group", "success");

                let item = data.data;

                $("#form-add select[name=name]").html("");
                $("#form-add select[name=name]").append("<option value=''>{{ trans('messages.select_patient') }}</option>");

                for (i = 0; i < item.length; i++) {
                    $("#form-add select[name=name]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name=name]").select2({
                    placeholder: "{{ trans('messages.select_patient') }}",
                    allowClear: false
                });

                if(val != "") {
                    $("#form-add select[name=name]").val(val).trigger("change");
                }

            } else {
                loading_content("#form-add #name-group", "failed");
                $("#form-add #name-group #loading-content").click(function(){ listPatient(val); });
            }
        },
        error: function() {
            loading_content("#form-add #name-group", "failed");
            $("#form-add #name-group #loading-content").click(function(){ listPatient(val); });
        }
    });
}

function getPatient(val, button) {
    if(val != "") {
        $('#form-add #loading-detail_patient').removeClass('hide');
        $('#form-add #reload-detail_patient').addClass('hide');
        $('#form-add #panel-detail_patient').addClass('hide');
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/patient/"+val+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    if(button == "") {
                        $("#form-add #add-service_true").removeClass("hide");
                        $("#form-add #add-service_false").addClass("hide");
                        $("#form-add #add-medicine_true").removeClass("hide");
                        $("#form-add #add-medicine_false").addClass("hide");
                    }

                    $('#form-add #loading-detail_patient').addClass('hide');
                    $('#form-add #panel-detail_patient').removeClass('hide');
                    $("#form-add input[name='mrn']").val(data.mrn);
                    $("#form-add #name").html(data.name);
                    $("#form-add #birth_date").html(getFormatDate(data.birth_date)+" ("+getAge(data.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                    $("#form-add #gender").html(getGender(data.gender));

                    let smoke = data.smoke == 1 ? '{{ trans("messages.smoking") }}' : '{{ trans("messages.no_smoking") }}';

                    $("#form-add #smoke").html(smoke);
                    $("#form-add #blood_type").html(checkNull(data.blood_type));

                    let no_identity = "-";
                    if(data.id_number != "" && data.id_number != null) {
                        no_identity = data.id_number+" ("+data.id_type+")";
                    }
                    $("#form-add #no_identity").html(no_identity);

                    $("#form-add #address").html(nullToEmpty(data.address)+' '+nullToEmpty(data.village.name)+' '+nullToEmpty(data.district.name)+' '+nullToEmpty(data.regency.name)+' '+nullToEmpty(data.province.name)+' '+nullToEmpty(data.country.name));
                    $("#form-add #email").html(checkNull(data.email));
                    $("#form-add #phone").html("+"+data.phone_code+""+data.phone);

                    try {
                        $("#form-add #patient_category").html(data.patient_category.name);
                        $("#form-add input[name='category']").val(data.patient_category.id);
                        listDoctor();
                    } catch(err) {}

                    $("#form-add #financing").html(getFinancing(data.financing));
                } else {
                    $('#form-add #loading-detail_patient').addClass('hide');
                    $('#form-add #reload-detail_patient').removeClass('hide');
                    $("#form-add #reload-detail_patient .reload").click(function(){ getPatient(val, button); });
                }
            },
            error: function(){
                $('#form-add #loading-detail_patient').addClass('hide');
                $('#form-add #reload-detail_patient').removeClass('hide');
                $("#form-add #reload-detail_patient .reload").click(function(){ getPatient(val, button); });
            }
        });
    } else {
        $("#form-add #add-service_true").addClass("hide");
        $("#form-add #add-service_false").removeClass("hide");
        
        resetData();
    }
}

$("#service input[name='service']").autocomplete({
    minLength: 3,
    source: async function(request, response) {
        let temp = [];
        try {
            let list = await listService(request.term)
            for(i = 0; i < list.data.length; i++) {
                temp.push({
                    id: list.data[i].id,
                    name: list.data[i].name,
                    value: list.data[i].name
                })
            }
            response(temp)
        }catch(error) {
            loading_content("#service #service-group", "success");
        }
    },
    focus: function() {
        return false;
    },
    select: function( event, ui) {
        $("#service input[name='service']").val(ui.item.name)
        $("#service input[name='service']").data("id", ui.item.id)
        getService(ui.item.id)
        return false;
    }
});

let timer_service = "";
function listService(event) {
    clearTimeout(timer_service);
    return new Promise((resolve, reject) => {
        timer_service = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/service?unpaid=true&q="+event+"&per_page=10",
                beforeSend: function() {
                    loading_content("#service #service-group", "loading");
                },
                success: function(results){
                    resolve(results)
                    loading_content("#service #service-group", "success");
                },
                error: function(error) {
                    reject(error)
                    loading_content("#service #service-group", "success");
                }
            });
        }, 2000); 
    });
}

function listDoctor(val) {
    loading_content("#service #implementer-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataDoctor){
            if(!dataDoctor.error) {
                loading_content("#service #implementer-group", "success");
                let item = dataDoctor.data;

                $("#service select[name=implementer]").html("");
                $("#service select[name=implementer]").append("<option value=''>{{ trans('messages.select_implementer') }}</option>");

                for (i = 0; i < item.length; i++) {
                    $("#service select[name=implementer]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                } 

                $("#service select[name=implementer]").select2({
                    placeholder: "{{ trans('messages.select_implementer') }}",
                    allowClear: false
                });
            } else {
                loading_content("#service #implementer-group", "failed");
            $("#service #implementer-group #loading-content").click(function(){ listDoctor(val); });            }
        },
        error: function(){
            loading_content("#service #implementer-group", "failed");
            $("#service #implementer-group #loading-content").click(function(){ listDoctor(val); });
        }
    });
}


function getPrice(){
    let discount_type = $("#service input[name='discount_type']").prop('checked');
    let discount = $("#service input[name='discount']").val();

    let service_price = $("#service input[name='service_price']").val();
    let admin_cost = $("#service input[name='admin_cost']").val();
    let qty = $("#service input[name='qty']").val();
    let total = '';
    let total_price = '';

    admin_cost = admin_cost == "" ? 0 : admin_cost;

    if(service_price != '') {
        if(qty == '') {
            total = parseInt(service_price)+parseInt(admin_cost);
        } else {
            total = (parseInt(service_price)+parseInt(admin_cost))*qty;
        }
    }

    $("#service input[name='price']").val(total);

    total = total == "" ? 0 : total;

    total_price = parseInt(total);

    if(discount != '') {
        if(discount_type == true) {
            total_price = total_price-discount;
        } else {
            total_price = total_price-((discount/100)*total_price);
            total_price = Math.round(total_price);
        }
    }

    total_price = total_price == 0 ? "" : total_price;
    
    $("#service input[name='total_price']").val(total_price);
}

function resetService() {
    $("#service input[name='id_item']").val("");
    $("#service input[name='id_service']").val("");
    $("#service input[name='service']").val("");
    $("#service input[name='qty']").val("");
    $("#service input[name='discount']").val("");
    switchButton('service #discount_percent', 'service #discount_number','service #discount_type',true);
    $("#service input[name='price']").val("");
    $("#service #admin_cost-group").addClass("hide");
    $("#service input[name='admin_cost']").val("");
    $("#service select[name='implementer']").val("").trigger("change");
    $("#service input[name='total_price']").val("");
    $("#service input[name='note']").val("");

    resetValidation('service #service','service #qty','service #implementer');
}

function removeItem(section, number) {
    var getSubtotal = $("#form-add input[name='subtotal']").val();
    var subtotal = parseInt(getSubtotal) - parseInt($(""+section+" input[name='total["+number+"]']").val());
    $("#form-add input[name='subtotal']").val(subtotal);
    $("#form-add #subtotal").html(formatCurrency('{{ $lang }}','',subtotal));

    $(""+section+"").remove();

    getTotal();

    getPay();
}


function processItem(service, service_name, qty, price, total_price, admin_cost, discount, discount_type, id_doctor, doctor, total, note, numItem) {

    if(numItem!="") {
        getPrevTotal = $("#form-add input[name='total["+numItem+"]']").val();
    }

    var getDiscount = '';
    var discountType = '';
    if(discount == '' || discount == 0) {
        getDiscount = '-';
    } else if(discount_type == true) {
        getDiscount = formatCurrency('{{ $lang }}','Rp',discount);
        discountType = '$';
    } else {
        getDiscount = discount+'%';
        discountType = '%';
    }

    var getAdmin_cost = '';
    if(admin_cost == '' || admin_cost == 0) {
        getAdmin_cost = '';
    } else {
        getAdmin_cost = '{{ trans("messages.admin_cost") }}: '+formatCurrency('{{ $lang }}','Rp',admin_cost);
    }

    var sectionItem = '';
    if(numItem != "") {
        sectionItem = numItem;
    } else {
        sectionItem = item;
    }

    var section_doctor = "";

    if(doctor != "") {
        section_doctor = '<a href=\'#/{{ $lang }}/doctor/'+id_doctor+'\' onclick=\'loading($(this).attr("href"))\'><h2>'+doctor+'</h2></a>';
    }

    var section = '<div class="item-sec">'+
                        '<div class="left-sec">'+
                            '<h1>'+service_name+'</h1>'+
                            section_doctor+
                            '<span class="text">'+getAdmin_cost+'</span>'+
                            '<span class="text">{{ trans("messages.discount") }}: '+getDiscount+'</span>'+
                            '<span class="text">{{ trans("messages.quantity") }}: '+qty+'</span>'+
                            '<span class="text">'+note+'</span>'+
                        '</div>'+
                        '<div class="right-sec text-right">'+
                            '<span class="money">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="icon-sec">'+
                        '<a class="btn-table btn-orange @if(!empty($id)) hide @endif" data-toggle="modal" data-target="#service" data-id="'+sectionItem+'"><img src=\'{{ asset("assets/images/icons/action/edit.png") }}\' /> {{ trans("messages.edit") }}</a>'+
                        '<a class="btn-table btn-red @if(!empty($id)) hide @endif" data-toggle="modal" data-target="#confirmDelete" data-message=\'{{ trans("messages.info_delete") }}\' data-section="#item-'+sectionItem+'" data-number="'+sectionItem+'" data-type="service"><img src=\'{{ asset("assets/images/icons/action/delete.png") }}\' /> {{ trans("messages.delete") }}</a>'+
                    '</div>'
                    +'<input type="hidden" name="service['+sectionItem+']" value="'+service+'">'
                    +'<input type="hidden" name="service_name['+sectionItem+']" value="'+service_name+'">'
                    +'<input type="hidden" name="qty['+sectionItem+']" value="'+qty+'">'
                    +'<input type="hidden" name="price['+sectionItem+']" value="'+price+'">'
                    +'<input type="hidden" name="total_price['+sectionItem+']" value="'+total_price+'">'
                    +'<input type="hidden" name="admin_cost['+sectionItem+']" value="'+admin_cost+'">'
                    +'<input type="hidden" name="discount['+sectionItem+']" value="'+discount+'">'
                    +'<input type="hidden" name="discount_type['+sectionItem+']" value="'+discountType+'">'
                    +'<input type="hidden" name="implementer['+sectionItem+']" value="'+id_doctor+'">'
                    +'<input type="hidden" name="total['+sectionItem+']" value="'+total+'">'
                    +'<input type="hidden" name="note['+sectionItem+']" value="'+note+'">';

    if(numItem!="") {
        $("#item #item-"+numItem).html(section);

        var getSubtotal = $("#form-add input[name='subtotal']").val();
        var subtotal = (parseInt(getSubtotal) - parseInt(getPrevTotal)) + parseInt(total);
    } else {
        $("#item").append('<li id="item-'+item+'">'+section+'</li>');
        var getSubtotal = $("#form-add input[name='subtotal']").val();
        var subtotal = parseInt(getSubtotal) + parseInt(total);
    
        item++;
    }

    $("#form-add input[name='subtotal']").val(subtotal);
    $("#form-add #subtotal").html(formatCurrency('{{ $lang }}','',subtotal));

    getTotal();

    getPay();

}

$('#service').on('show.bs.modal', async function (e) {
    resetService();

    var id = $(e.relatedTarget).attr('data-id');
    if(id==undefined || id=="") {
        $("#service input[name='id_item']").val("");
        $("#service input[name='id_service']").val("");
        $("#title-service").html('<?php echo trans('messages.add_service'); ?>');
    } else {

        $("#service input[name='id_item']").val(id);
        $("#service input[name='id_service']").val($("#form-add input[name='service["+id+"]']").val());
        $("#service input[name='service']").val($("#form-add input[name='service_name["+id+"]']").val()).change();
                    
        try {
            let list = await listService($("#form-add input[name='service_name["+id+"]']").val())
            let services = list.data.find(service => service.id === $("#form-add input[name='service["+id+"]']").val())
            $("#service input[name='service']").data("id", services.id)
            if(services) {
                getService(services.id)
            }
        } catch (error) {
            console.error(error)
        }
        
        $("#service input[name='qty']").val($("#form-add input[name='qty["+id+"]']").val());
        $("#service input[name='discount']").val($("#form-add input[name='discount["+id+"]']").val());

        if($("#form-add input[name='discount_type["+id+"]']").val()=="%") {
            switchButton('service #discount_number', 'service #discount_percent','service #discount_type',false);
        } else {
            switchButton('service #discount_percent', 'service #discount_number','service #discount_type',true);
        }
        
        
        $("#service input[name='price']").val($("#form-add input[name='price["+id+"]']").val());

        if($("#form-add input[name='admin_cost["+id+"]']").val()!=0) {
            $("#service #admin_cost-group").removeClass("hide");
            $("#service input[name='admin_cost']").val($("#form-add input[name='admin_cost["+id+"]']").val());
        }

        $("#service select[name='implementer']").val($("#form-add input[name='implementer["+id+"]']").val()).trigger("change");

        $("#service input[name='total_price']").val($("#form-add input[name='total["+id+"]']").val());
        $("#service input[name='note']").val(br2nl($("#form-add input[name='note["+id+"]']").val()));

        $("#title-service").html('<?php echo trans('messages.edit_service'); ?>');        
    }    

});

$('#service').on('hidden.bs.modal', function (e) {
    resetService();
    $('html, body').animate({
        scrollTop: $("#list-service").offset().top
    }, 0);
});

$('#form-add select[name=name]').on('change', function() {
    var name = this.value;

    getPatient(name, '');
});


function getService(val) {
    if(val!="") {
        loading_content("#service #service-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/service/"+val+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    loading_content("#service #service-group", "success");
                    if($("#service input[name='qty']").val() == "") {
                        $("#service input[name='qty']").val(1);
                    }

                    var getPatientCategory = $("#form-add input[name='category']").val();

                    var cost = false;
                    for(var i=0;i<data.cost_category.length;i++) {
                        if(data.cost_category[i].id_patient_category==getPatientCategory) {
                            $("#service input[name='service_price']").val(data.cost_category[i].cost);
                            cost = true;
                        }
                    }

                    if(cost==false) {
                         $("#service input[name='service_price']").val(data.cost);
                    }

                    if(nullToZero(data.admin_cost)!=0) {
                        $("#service #admin_cost-group").removeClass("hide");
                        $("#service input[name='admin_cost']").val(data.admin_cost);
                    } else {
                        $("#service #admin_cost-group").addClass("hide");
                        $("#service input[name='admin_cost']").val("");
                    }

                    if(data.implementer==1) {
                        $('#service input[name="select_implementer"]').prop('checked',true);
                        $('#service #select_implementer').removeClass("hide");
                    } else {
                        $('#service input[name="select_implementer"]').prop('checked',false);
                        $('#service #select_implementer').addClass("hide");
                    }
                                         
                    getPrice();
                    
                } else {
                    loading_content("#service #service-group", "failed");

                    $("#service #service-group #loading-content").click(function(){ getService(val); });
                }

            },
            error: function(){
                loading_content("#service #service-group", "failed");

                $("#service #service-group #loading-content").click(function(){ getService(val); });
            }
        });
    } else {
        $("#service input[name='qty']").val("");
        $("#service input[name='service_price']").val("");
        $("#service #admin_cost-group").addClass("hide");
        $("#service input[name='admin_cost']").val("");
        $("#service input[name='price']").val("");
        getPrice();
    }
}

function getDoctor(val) {
    if(val!="") {
        loading_content("#service #implementer-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/doctor/"+val+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    loading_content("#service #implementer-group", "success");
                                         
                    getPrice();
                    
                } else {
                    loading_content("#service #implementer-group", "failed");

                    $("#service #implementer-group #loading-content").click(function(){ getDoctor(val); });
                }

            },
            error: function(){
                loading_content("#service #implementer-group", "failed");

                $("#service #implementer-group #loading-content").click(function(){ getDoctor(val); });
            }
        });
    } else {
        getPrice();
    }
}

$("#service").submit(function(event) {
    let id_item = $("#service input[name='id_item']").val();
    let id_service = $("#service input[name='id_service']").val();
    let service = $("#service input[name='service']").data("id");
    let service_name = $("#service input[name='service']").val();
    let qty = $("#service input[name='qty']").val();
    let price = $("#service input[name='service_price']").val();
    let total_price = $("#service input[name='price']").val();
    let admin_cost = $("#service input[name='admin_cost']").val();    
    let discount = $("#service input[name='discount']").val();
    let discount_type = $("#service input[name='discount_type']").prop('checked');
    let implementer = $("#service select[name='implementer'] option:selected").val();
    let select_implementer = $('#service input[name="select_implementer"]').is(':checked');
    let textImplementer = $("#service select[name='implementer'] option:selected").text();
    let total = $("#service input[name='total_price']").val();
    let note = nl2br($("#service input[name='note']").val());
    

    let error = false;
    let scrollValidate = true;
    
    if(service=='') {
        formValidate(scrollValidate, ['service #service','{{ trans("validation.empty_service") }}', true]);    
        error = true;
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } 

    if(qty=='') {
        formValidate(scrollValidate, ['service #qty','{{ trans("validation.empty_qty") }}', true]);    
        error = true;
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } 

    if(implementer=='' && select_implementer==true) {
        formValidate(scrollValidate, ['service #implementer','{{ trans("validation.empty_implementer") }}', true]);    
        error = true;
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    }

    if(id_item == "") {
        $("#form-add input[name^='service']").each(function(key, value) {
            if(this.value == service) {
                errorService = true;
                notif(false,"{{ trans('validation.service_already_exists') }}");
                error = true;
            }
        });
    }

    if(implementer=="") {
        textImplementer="";
    }

    if(!error) {
        if(id_item!="") {
            processItem(service, service_name, qty, price, total_price, admin_cost, discount, discount_type, implementer, textImplementer, total, note, id_item);
            notif(true,"{{ trans('validation.success_edit_service') }}");
        } else { 
            processItem(service, service_name, qty, price, total_price, admin_cost, discount, discount_type, implementer, textImplementer, total, note, '');
            notif(true,"{{ trans('validation.success_add_service') }}");
        }

        $("#service").modal("toggle");
        resetService();
    }
});

var data_unit = [];

$("#medicine input[name='medicine_name']").autocomplete({
    minLength: 3,
    source: async function( request, response ) {
        let temp = [];
        try {
            let list = await listMedicine(request.term)
            data_unit = list.data;
            for(i = 0; i < list.data.length; i++) {
                let medicine = list.data[i];
                temp.push({id: medicine.id, name: medicine.name, code: medicine.code, value: medicine.name});
            }
            response(temp);

        } catch (error) {
            $("#medicine #loading-content-name").addClass("hide");
        }
    },
    focus: function() {
        return false;
    },
    select: function( event, ui) {
        $("#medicine input[name='medicine_name']").val(ui.item.name);
        $("#medicine input[name='medicine_name']").data("data-id", ui.item.id);
        $("#medicine input[name='medicine_name']").data("data-code", ui.item.code);
        selectMedicine(ui.item.id, ui.item.name, ui.item.code)
        return false;
    }
});

let timer_medicine = "";
function listMedicine(event) {
    clearTimeout(timer_medicine);
    return new Promise((resolve, reject)=> {
        timer_medicine = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/medicine?unpaid=true&q="+event+"&per_page=10",
                beforeSend: function() {
                    $("#medicine #loading-content-name").removeClass("hide");
                },
                success: function(results){
                    resolve(results)
                    $("#medicine #loading-content-name").addClass("hide");
                },
                error: function(error) {
                    reject(error)
                    $("#medicine #loading-content-name").addClass("hide");
                }
            });
        }, 2000);
    });
}

function getMedicineId(id) {
    loading_content("#medicine #name-group", "loading");
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/medicine/" + id,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                resolve(data)
                loading_content("#medicine #name-group", "success");
                eventSelectMedicine(id, data)
            },
            error: function() {
                reject(error)
                loading_content("#medicine #name-group", "failed");
                $("#medicine #name-group #loading-content").click(function(){ getMedicineId(id); });
            }
        });
    })
}

function getDay(){
    let dosis = $('#form-medicine input[name=signa1]').val();
    let dosis1 = $('#form-medicine input[name=signa2]').val();
    let jml = $('#form-medicine input[name=qty_item_medicine]').val();
    let jmlPermintaan = $('#form-medicine input[name=qty_demand]').val();
    let is_racikan = $("#form-medicine input[name='is_racikan']:checked").val();

    let hasil = 0;
    if (is_racikan == '1'){
        hasil = (jmlPermintaan != '' ? jmlPermintaan : 0) / ((dosis != '' ? dosis : 0) * (dosis1 != '' ? dosis1 : 0));
    }
    hasil = !hasil ? 0 : hasil
    $('#form-medicine input[name=total_day]').val(parseInt(hasil));
}

var checked_stock_racikan = false;
function getQtyMedicine(){
    checked_stock_racikan = false;
    let dosis = $('#form-medicine input[name=signa1]').val();
    let dosis1 = $('#form-medicine input[name=signa2]').val();
    let jml1 = $('#form-medicine input[name=power_medicine]').val();
    let jml = $('#form-medicine input[name=qty_demand]').val();
    let hasil = (jml != '' ? jml : 0) * (jml1 != '' ? jml1 : 0);
    let hasil2 = 0;
    let total_price_racikan = 0;
    let is_racikan = $("#form-medicine input[name='is_racikan']:checked").val();
    if (is_racikan == '1'){
        hasil2 = (jml != '' ? jml : 0) / ((dosis != '' ? dosis : 0) * (dosis1 != '' ? dosis1 : 0));
    }
    hasil2 = !hasil2 ? 0 : hasil2;

    $('#form-medicine input[name=qty_item_medicine]').val(parseInt(Math.ceil(hasil)));
    let data_price = $('#form-medicine input[name=qty_item_medicine]').val() * price_list_racikan;
    
    $('#form-medicine input[name=total_day]').val(parseInt(Math.ceil(hasil2)));
    $('#form-medicine input[name=price_unit_racikan]').attr("data-price", data_price);
    $('#form-medicine input[name=price_unit_racikan]').val(formatCurrency('{{ $lang }}','Rp',data_price))
    
    if(temp_unit.stock < Math.ceil(hasil)) {
        $("#total-stock-racikan").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${data_unit_reverse[0].name}`);
        $("#save-inventory").attr("disabled", true);
        checked_stock_racikan = true;
    }else {
        $("#total-stock-racikan").text("");
        $("#save-inventory").attr("disabled", false);
    }
}



var id_patient_filter = [];
var temp_unit = "";
$('#medicine select[name=name]').on('change', async function() {
    if(this.value != "") {
        temp_unit = await getMedicineId(this.value);
    } else {
        $("#medicine input[name='id']").val("");
        $("#medicine input[name='code']").val("");
    }
});

function eventSelectMedicine(id, data) {
    $("#medicine input[name='duration']").val("");
    $("#save-inventory").attr("disabled", false);
    $("select[name='unit_medicine']").html('<option value=""></option>')
    $("select[name='unit_medicine']").val("").trigger("change");
    let id_patient_category = $("#form-add input[name='category']").val();
    id_patient_filter = data.price_list.filter(patient => patient.id_patient_category === id_patient_category)
    $("#medicine input[name='id']").val(data.id);
    $("#medicine input[name='code']").val(data.code);
    $("#medicine input[name='notes'").val(data.notes ? data.notes : '-');
    if(data.stock === 0) {
        notif(false, "{{ trans('validation.empty_stock_inventory') }}")
        $("#save-inventory").attr("disabled", true);
    }

    if(data.unit.length > 0) {
        setQtyUnitMedicine(data);
        for (i = 0; i < data.unit.length; i++) {
            $("select[name='unit_medicine']").append("<option value="+data.unit[i].id+">"+data.unit[i].name+"</option>")
        }
    }else {
        $("#save-inventory").attr("disabled", true);
        $("#qty_radio_button-group").html("")
        notif(false, '{{ trans("validation.no_unit") }}');
        $("#qty_radio_button-group").html("<p>{{ trans('validation.empty_unit') }}</p>")
        $(".signa2-group p").html("");
    }
}

function selectMedicine (id, name, code) {
    $("#medicine input[name='duration']").val("");
    $("#save-inventory").attr("disabled", false);
    $("select[name='unit_medicine']").html('<option value=""></option>')
    $("select[name='unit_medicine']").val("").trigger("change");
    $("input[name='value_unit_bhp']").val("").change()
    $("#notes-change-unit").html("")
    $("#notes-change-unit-bhp").html("")
    $("#qty_radio_button-group").html("")
    $("#medicine input[name='price_bhp']").val("").change()
    
    let id_patient_category = $("#form-add input[name='category']").val();
    temp_unit = data_unit.find(data => data.id === id);
    let price_list = data_unit.find(price => price.id === id).price_list;
    id_patient_filter = price_list.filter(patient => patient.id_patient_category === id_patient_category)
    $("#medicine input[name='id_medicine']").val(id);
    $("#medicine input[name='code']").val(code);
    $("#medicine input[name='notes'").val(temp_unit.notes ? temp_unit.notes : '-');
    
    let medicine_bhp = temp_unit.category.find(category => category.name.toLowerCase() == "obat")

    if(!medicine_bhp) {
        $("#medicine_dose-group").addClass("hide")
        $("#medicine_bhp").removeClass("hide")
        $("#medicine input[name='radio_bhp']").prop("checked", true)
        $("#medicine #change_unit_bhp").data("category", "bhp")
        $("#medicine select[name='unit_bhp']").html("<option value=''></option>")
        $("#medicine select[name='unit_bhp']").select2({
            placeholder: "{{ trans('messages.choice_unit') }}"
        });
        if(temp_unit.unit.length > 0) {
            for (i = 0; i < temp_unit.unit.length; i++) {
                $("#medicine select[name='unit_bhp']").append("<option value="+temp_unit.unit[i].id+">"+temp_unit.unit[i].name+"</option>")
            }
        }else {
            $("#save-inventory").attr("disabled", true);
            notif(false, '{{ trans("validation.no_unit") }}');
        }
    }else {
        $("#medicine_dose-group").removeClass("hide")
        $("#medicine_bhp").addClass("hide")
        $("#medicine input[name='radio_bhp']").prop("checked", false)
        $("#medicine #change_unit_bhp").data("category", "")
        
        if(temp_unit.stock === 0) {
            notif(false, "{{ trans('validation.empty_stock_inventory') }}")
            $("#save-inventory").attr("disabled", true);
        }

        if(temp_unit.unit.length > 0) {
            setQtyUnitMedicine(temp_unit);
            for (i = 0; i < temp_unit.unit.length; i++) {
                $("select[name='unit_medicine']").append("<option value="+temp_unit.unit[i].id+">"+temp_unit.unit[i].name+"</option>")
            }
        }else {
            $("#qty_radio_button-group").html("<p>{{ trans('validation.empty_unit') }}</p>")
            $(".signa2-group p").html("");
        }
    }
}



var price_list_racikan = "";
$("#medicine select[name=unit_medicine]").on('change', function(){
    $("#save-inventory").attr("disabled", false);
    if(this.value != "") {
        price_list_racikan = temp_unit.price_list.find(racikan => racikan.id_unit === this.value)
        price_list_racikan = price_list_racikan ? price_list_racikan.price : null
        if(!price_list_racikan) {
            notif(false , '{{ trans("validation.empty_price_inventory") }}');
            $("#save-inventory").attr("disabled", true);
        }else if(checked_stock_racikan) {
            notif(false, "{{ trans('validation.empty_stock_inventory') }}")
            $("#save-inventory").attr("disabled", true);
        }
    }
});

var data_unit_reverse;

function setQtyUnitMedicine(medicine) {
    $("#qty_radio_button-group").html("")
    var template_qty = "<div class='sf-radio'>" +
                            "<div class='wrapper-label-radio'>" +
                                "<label><input type='radio' name='qty_radio_button' /><span></span></label>" +
                                "<span id='qty-unit'></span>" +
                                "<p class='notes'>{{ trans('messages.item') }}</p>" +
                                "<p id='price_unit' class='list-notes-price'></p>" +
                            "</div>"+
                            "<p id='total-stock' class='notes-radio-button hide'></p>"+
                        "</div>";
    let template_add_qty = "<div class='sf-radio another_unit'>" +
                            "<div class='wrapper-label-radio'>" +
                                "<label><input type='radio' name='qty_radio_button' id='change_unit' /><span></span></label>" +
                                "<p class='notes'>{{ trans('messages.another_amount') }}</p>" +
                            "</div>"+
                            "<div class='wrapper-label-radio last-title hide'>" +
                                "<div class='col-xs-7'>" +
                                    "<label class='control-label'>{{ trans('messages.qty') }}</label>"+
                                    "<div>" +
                                        "<input class='form-control' type='number' placeholder='{{ trans('messages.input_quantity') }}' name='qty_change_unit' onkeydown='number(event)' autocomplete='off' oninput='changeValueQtyUnit(event)' />" +
                                        "<span id='notes-change-unit' class='notif-notes'></span>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='col-xs-5'>" +
                                    "<label class='control-label'>{{ trans('messages.unit') }}</label>" +
                                    "<div>" +
                                        "<select class='form-control' name='unit_add'>" +
                                            "<option value=''></option>" +
                                        "</select>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
    for (i = 0; i < medicine.unit.length; i++) {
        $("#qty_radio_button-group").append(template_qty)
    }
    
    medicine.unit.sort((a, b) =>Number(b.weight) - Number(a.weight));
    data_unit_reverse = medicine.unit;
    
    $("#qty_radio_button-group").find(".sf-radio").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        $("#qty_radio_button-group #type-"+key+" .notes").html(medicine.unit[key].name)
    
        $("#qty_radio_button-group #type-"+key+" input[name='qty_radio_button']").attr("data-name", function(i) {
            return medicine.unit[key].name
        });
        $("#qty_radio_button-group #type-"+key+" input[name='qty_radio_button']").attr("data-id", function(j) {
            return medicine.unit[key].id
        });
    });

    $(".signa2-group p").html(medicine.unit[0].name);

    $("#qty_radio_button-group").append(template_add_qty);
    $("select[name='unit_add']").select2({
        placeholder: "{{ trans('messages.unit') }}"
    });
}

var dose = "";
var unit_dose = "";
var duration = "";
var results = "";
$("input[name='signa1']").on('input', function(e){
    dose = e.target.value;
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration;
        setValueQty(results)
    }
});

$("input[name='signa2']").on('input', function(e) {
    unit_dose = e.target.value.replace(/,/g, '.');
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration;
        setValueQty(results)
    }
});

$("input[name='duration']").on('input',function(e) {
    duration = e.target.value;
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration;
        setValueQty(results)
    }
});


function setValueQty(val) {
    $("select[name='unit_add']").html("<option value=''></option>");
    $("#qty_radio_button-group").find(".sf-radio").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });

        if($("#type-"+key+" input[name='qty_radio_button']").prop("checked") == true) {
            if(temp_unit.stock < Math.ceil(val)) {
                $("#save-inventory").attr("disabled", true)
            }else {
                $("#save-inventory").attr("disabled", false)
            }
        }

        $("#type-"+key+" #total-stock").text("");

        let price_list_unit = id_patient_filter.find(price => price.id_unit === $("#type-"+key+" input[name='qty_radio_button']").data('id'));
        let price_ = price_list_unit ? price_list_unit.price : 0;
        
        if(temp_unit.stock < Math.ceil(val)) {
            $("#type-"+key+" #total-stock").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${data_unit_reverse[0].name}`)
        }

        if (key === 0) {
            let data_price = val * price_;
            $("#type-"+key+" #price_unit").text(`(${formatCurrency('{{ $lang }}','Rp',data_price)})`);
            $("#type-"+key+" input[name='qty_radio_button']").attr("data-total", function(i) {
                return data_price
            });
            $("#type-"+key+" input[name='qty_radio_button']").attr("data-price", function(i) {
                return price_
            });
            $("#type-"+key+" #qty-unit").text(Math.ceil(val));
            $("#type-"+key+" input[name='qty_radio_button']").val(Math.ceil(val));
            
        } else if (key > 0) {
            let sumOf = val / data_unit_reverse[key - 1].amount_ratio;
            if (key - 1 > 0) {
                sumOf = val;
                for (i = 0; i < key; i++) sumOf = sumOf / data_unit_reverse[i].amount_ratio
            }

            let data_price_sumOf = Math.ceil(sumOf) * price_;

            $("#type-"+key+" #price_unit").text(`(${formatCurrency('{{ $lang }}','Rp',data_price_sumOf)})`); 
            $("#type-"+key+" #qty-unit").text(Math.ceil(sumOf));

            if(!$("#type-"+key+" input[name='qty_radio_button']").is("#change_unit")) {
                $("#type-"+key+" input[name='qty_radio_button']").attr("data-total", function(i) {
                    return data_price_sumOf;
                });
                $("#type-"+key+" input[name='qty_radio_button']").attr("data-price", function(i) {
                    return price_;
                });
                $("#type-"+key+" input[name='qty_radio_button']").val(Math.ceil(sumOf));
            }
        }
    });

    for (i = 0; i < data_unit_reverse.length; i++) {
        $("select[name='unit_add']").append("<option value="+data_unit_reverse[i].id+">"+data_unit_reverse[i].name+"</option>")
    }

    setTotalStock()

    $("#qty_radio_button-group input[name='qty_radio_button']").on("change",function() {
        setTotalStock();
        if(temp_unit.stock < Math.ceil(val)) {
            $("#save-inventory").attr("disabled", true)
        }else {
            $("#save-inventory").attr("disabled", false)
        }
    });

    let price_change = 0;
    let qty_change_unit = "";
    let price_list_unit_change = "";
    let value_add_unit = "";
    $("input[name='qty_change_unit']").on('change', function(event) {
        qty_change_unit = event.target.value;
        if(value_add_unit != "") {
            let price_select = setPrice(value_add_unit)
            if(temp_unit.stock < qty_change_unit) {
                $("#notes-change-unit").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${data_unit_reverse[0].name}`)
                $("#save-inventory").attr("disabled", true)
            }else {
                $("#notes-change-unit").text("");
                $("#save-inventory").attr("disabled", false)
            }

            let data_total_input = qty_change_unit * price_select;

            if(price_select == 0) {
                $("#save-inventory").attr("disabled", true)
            }

            $("#change_unit").attr("data-total", function(i) {
                return data_total_input;
            });

            $("#change_unit").val(qty_change_unit);
        }
    });

    $("select[name='unit_add']").on('change', function(e) {
        if($(this).val() != "") {
            $("#save-inventory").attr("disabled", false);
            value_add_unit = $(this).val();
            let price_select = setPrice(value_add_unit);
            $("#change_unit").attr("data-price", function(i) {
                return price_select;
            });
            if(price_select == 0) {
                notif(false, '{{ trans("validation.empty_price_inventory") }}')
                $("#save-inventory").attr("disabled", true)

            }
            if (qty_change_unit != "") {
                let data_total_change = qty_change_unit * price_select; 
                if(temp_unit.stock < qty_change_unit) {
                    $("#notes-change-unit").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${data_unit_reverse[0].name}`)
                    $("#save-inventory").attr("disabled", true)
                }

                $("#change_unit").attr("data-total", function(i) {
                    return data_total_change;
                });
            }
        }
    });
}

let unit_bhp = "";
let value_unit_bhp = "";
let name_unit_bhp = "";
$("input[name='value_unit_bhp']").on("change", function(event) {
    value_unit_bhp = event.target.value;
    if(unit_bhp != "") {
        let price_select = setPrice(unit_bhp)
        $("#change_unit_bhp").attr("data-price", function(i) {
            return price_select;
        });

        if(temp_unit.stock < value_unit_bhp) {
            $("#notes-change-unit-bhp").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${temp_unit.unit[temp_unit.unit.length -1].name}`)
            $("#save-inventory").attr("disabled", true)
        }else {
            $("#notes-change-unit-bhp").text("");
            $("#save-inventory").attr("disabled", false)
        }

        let data_total_input = value_unit_bhp * price_select;
        $("#medicine input[name='price_bhp']").val(formatCurrency('{{ $lang }}','Rp',data_total_input) +" /"+name_unit_bhp+"")

        if(price_select == 0) {
            $("#save-inventory").attr("disabled", true)
        }

        $("#change_unit_bhp").attr("data-total", function(i) {
            return data_total_input;
        });

        $("#change_unit_bhp").val(value_unit_bhp);
    }
})

$("select[name='unit_bhp']").on('change', function(e) {
    if($(this).val() != "") {
        $("#save-inventory").attr("disabled", false);
        unit_bhp = $(this).val();
        name_unit_bhp = $(this).find('option:selected').text()
        let price_select = setPrice(unit_bhp);
        $("#change_unit_bhp").attr("data-price", function(i) {
            return price_select;
        });
        if(price_select == 0) {
            notif(false, '{{ trans("validation.empty_price_inventory") }}')
            $("#save-inventory").attr("disabled", true)

        }
        if (value_unit_bhp != "") {
            let data_total_change = value_unit_bhp * price_select; 
            $("#medicine input[name='price_bhp']").val(formatCurrency('{{ $lang }}','Rp',data_total_change) +" /"+name_unit_bhp+"")

            if(temp_unit.stock < value_unit_bhp) {
                $("#notes-change-unit-bhp").text(`{{ trans('messages.stock_only') }} ${temp_unit.stock} ${temp_unit.unit[temp_unit.unit.length -1].name}`)
                $("#save-inventory").attr("disabled", true)
            }

            $("#change_unit_bhp").attr("data-total", function(i) {
                return data_total_change;
            });
        }
    }
});

function setNotifStock (event) {
    $("input[name='value_unit_bhp']").val(event.target.value).change()
}

function setPrice(val) {
    let temp_val = id_patient_filter.find(price => price.id_unit === val);
    let temp_result = temp_val ? temp_val.price : 0;
    return temp_result;
}

function changeValueQtyUnit(event) {
    $("input[name='qty_change_unit']").val(event.target.value).change()
}

function setTotalStock() {
    $("#qty_radio_button-group input[name='qty_radio_button']").each(function(key, val) {
        if ( $(this).prop("checked") ){
            if($(this).is("#change_unit")) {
                $(".last-title").removeClass("hide");
            }
            $("#type-"+key+" #total-stock").removeClass("hide");
        } else {
            $("#type-"+key+" #total-stock").addClass("hide");
            $(".last-title").addClass("hide");
            $("#notes-change-unit").text("");
            $("input[name='qty_change_unit']").val("");
            $("select[name='unit_add']").val("").trigger("change");
        }
    });
}

$('#medicine input[name=is_racikan]').change(function() {
    var hiddenId = $(this).attr("data-trigger");
    var value = $(this).val();
    if ($(this).is(':checked')) {
        $("." + hiddenId).removeClass('hide');
        $("#interaction-group").addClass('hide');
        $("#unit_medicine-group").removeClass('hide');
        $("#one_day-group").addClass('hide');
        $("#duration-group").addClass('hide');
        $("#qty_radio_button-group").addClass('hide');
        $("#instructions-group").addClass('hide');
        $("#label-qty").addClass('hide');
        $(".signa1-group").attr('class', 'col-xs-5 signa1-group');
        $(".signa2-group").attr('class', 'col-xs-5 signa2-group');
        $('#medicine input[name=total_day]').val("");
        $('#form-medicine input[name=qty_item_medicine]').val("").attr("disabled", true);
        $("#medicine input[name='qty_unit']").val("");
        $("#medicine input[name='qty_radio_button']").attr("checked", false);
        $("#medicine input[name='duration']").val("");
        $("#medicine select[name='signa3']").val("").trigger("change");
    } else {
        $("." + hiddenId).addClass('hide');
        $("#unit_medicine-group").addClass('hide');
        $("#interaction-group").removeClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#duration-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#instructions-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $(".signa1-group").attr('class', 'col-xs-3 signa1-group');
        $(".signa2-group").attr('class', 'col-xs-3 signa2-group');
        $('#medicine input[name=total_day]').val("");
        $('#medicine input[name=qty_item_medicine]').val("");
        $("#medicine input[name='unit_medicine']").val("").trigger("change");
        $("#medicine input[name='power_medicine']").val("");
        $("#medicine input[name='qty_demand']").val("");
        $("#medicine input[name='racikan']").val("");
    }
    resetValidation('medicine #name', 'medicine #duration', 'medicine #instructions', 'medicine #racikan', 'medicine #unit_medicine', 'medicine #medicine_strong', 'medicine #medicine_qty', 'medicine #qty_item_medicine', 'medicine #total_day');

});

function resetMedicine() {
    results = "";
    price_list_racikan = "";
    checked_stock_racikan = false;
    unit_bhp = "";
    value_unit_bhp = "";
    name_unit_bhp = "";
    
    $("#medicine input[name='id_item']").val("");
    $("#medicine input[name='id_medicine']").val("");
    $("#medicine input[name='id']").val("");
    $("#medicine input[name='medicine_name']").val("");
    $("#medicine input[name='code']").val("");
    $("#medicine input[name='is_racikan']").removeAttr('checked');
    $(".is_racikan").addClass('hide');
    $("#medicine_bhp").addClass("hide");
    $("#medicine_dose-group").removeClass("hide");
    $("#medicine input[name='radio_bhp']").prop("checked", false);
    $("#medicine input[name='racikan']").val("");
    $("#medicine input[name='unit_medicine']").val("").trigger("change");
    $("#medicine input[name='signa1']").val("");
    $("#medicine input[name='signa2']").val("");
    $("#medicine select[name='signa3']").val("").trigger("change");
    $("#medicine input[name='duration']").val("");
    $("#medicine input[name='qty_unit']").val("");
    $("#medicine input[name='qty_radio_button']").attr("checked", false);
    $("#qty_radio_button-group").html("")
    $("#medicine input[name='interaction']").val("")
    $("#medicine input[name='power_medicine']").val("");
    $("#medicine input[name='qty_demand']").val("");
    $("#medicine input[name='qty_item_medicine']").val("");
    $("#medicine input[name='total_day']").val("");
    $("#medicine input[name='instructions']").val("");
    $("#save-inventory").attr("disabled", false);
    $("#medicine_dose-group .signa2-group p").html("")
    $("#medicine input[name='price_unit_racikan']").val("");
    $("#total-stock-racikan").text("");
    resetValidation('medicine #medicine_name', 'medicine #duration', 'medicine #instructions', 'medicine #racikan', 'medicine #unit_medicine', 'medicine #medicine_strong', 'medicine #medicine_qty', 'medicine #qty_item_medicine', 'medicine #total_day', 'medicine #unit_bhp');
}

function removeItemMedicine(section, number) {
    var getSubtotal = $("#form-add input[name='subtotal']").val();
    var subtotal = parseInt(getSubtotal) - parseInt($(""+section+" input[name='medicine_total["+number+"]']").val());

    $("#form-add input[name='subtotal']").val(subtotal);
    $("#form-add #subtotal").html(formatCurrency('{{ $lang }}','',subtotal));

    $(""+section+"").remove();
    $("#item-medicine").find("li").each(function(key, value) {
        $(this).attr("id", function() {
            return "item-medicine-" + key;
        });

        $("#item-medicine-"+key+" #action-medicine-edit").attr("data-id", key)
        $("#item-medicine-"+key+" #action-medicine-delete").attr("data-section", "#item-medicine-"+key+"")
        $("#item-medicine-"+key+" #action-medicine-delete").attr("data-number", key)

        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_list']"), 'id_list', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicines']"), 'medicines', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_name']"), 'medicine_name', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_code']"), 'medicine_code', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='unit_medicine']"), 'unit_medicine', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_qty']"), 'medicine_qty', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_total']"), 'medicine_total', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='price_category']"), 'price_category', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='signa1']"), 'signa1', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='signa2']"), 'signa2', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='name_signa3']"), 'name_signa3', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='duration']"), 'duration', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='instructions_for_use']"), 'instructions_for_use', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='unit_medicine']"), 'unit_medicine', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_category']"), 'medicine_category', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_signa']"), 'id_signa', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_medicine_medical']"), 'id_medicine_medical', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='instructions_racikan']"), 'instructions_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='dosage']"), 'dosage', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_package']"), 'id_package', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_unit_package']"), 'id_unit_package', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='racikan']"), 'racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='need_package']"), 'need_package', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='notes']"), 'notes', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='total_package']"), 'total_package', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='medicine_racikan']"), 'medicine_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='qty_racikan']"), 'qty_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='unit_racikan']"), 'unit_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='price_racikan']"), 'price_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='total_racikan']"), 'total_racikan', key)
        resetIndex($("#item-medicine #item-medicine-"+key+" input[name^='id_medicine_medical_racikan']"), 'id_medicine_medical_racikan', key)


    });


    itemMedicine--;

    getTotal();
    getPay();
}

function processItemMedicine(medicine, instruction_for_use, 
        dose, per_day, time_frame, total_day, 
        quantity, racikan, price, concoction, price_racikan, id_signa, medicine_category, numItem) {

    let id_patient = $("#form-add input[name='category']").val();
    
    var sectionItem = '';
    if(numItem != "") {
        sectionItem = numItem;
        getPrevTotal = $("#form-add input[name='medicine_total["+numItem+"]']").val();
    } else {
        sectionItem = itemMedicine;
    }
    let total_no_racikan = quantity * getZero(price).toFixed(0); 
    let name;
    let unit;
    let field_medicine_racikan = "";
    let field_qty_racikan = "";
    let field_unit_racikan = "";
    let field_price = "";
    let field_total = "";
    let field_id_medicine_medical = "";
    let list = "";
    let id;
    let notes;
    let code;
    let list_racikan = "<ul class='list_racikan'>";
    let hide = ""
    let id_unit = "";
    let id_medicine_medical = "";
    let total_price_racikan = 0;

    for (i = 0; i < medicine.length; i++) {
        if(racikan != ""){
            let total_racikan = getZero(price_racikan[i]).toFixed(0) * medicine[i].quantity
            total_price_racikan = total_racikan + total_price_racikan;
            hide = "style='display: none;'"
            name = racikan+ "({{ trans('messages.concoction') }})"
            unit = concoction.instruction
            field_medicine_racikan += '<input type="hidden" name="medicine_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine[i].id_medicine+'" />'
            field_qty_racikan += '<input type="hidden" name="qty_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine[i].quantity+'" />'
            field_unit_racikan += '<input type="hidden" name="unit_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine[i].id_unit+'" />'
            field_price += '<input type="hidden" name="price_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+price_racikan[i]+'" />'
            field_total += '<input type="hidden" name="total_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+total_racikan+'" />'
            field_id_medicine_medical += '<input type="hidden" name="id_medicine_medical_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine[i].id+'" />'
            list_racikan += "<li>"+medicine[i].name+" ("+medicine[i].quantity+" "+medicine[i].unit_name+") </li>"
        }else {
            name = medicine[i].name;
            unit = medicine[i].unit_name;
            id = medicine[i].id_medicine;
            id_unit = medicine[i].id_unit;
            id_medicine_medical = medicine[i].id;
        }
        code = medicine[i].code
    }

    list_racikan += "</ul>"

    let total = concoction != "" ? total_price_racikan : quantity * getZero(price).toFixed(0);

    var section = '<div class="item-sec">'+
                        '<div class="left-sec">'+
                            '<h1>'+name+'</h1>'+
                            (racikan != "" ? list_racikan : "") + 
                            '<span class="text">{{ trans("messages.quantity") }}: '+quantity+' '+unit+'</span>'+
                            '<span class="text">{{ trans("messages.dose") }}: '+ (concoction ? concoction.notes : dose + " x " + per_day + " " + unit + " {{ trans('messages.during')}} " + total_day + " {{ trans('messages.day') }} ") +'</span>' +
                        '</div>'+
                        '<div class="right-sec text-right">'+
                            '<span class="money">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="icon-sec">'+
                        '<a id="action-medicine-edit" class="btn-table btn-orange @if(!empty($id)) hide @endif" data-toggle="modal" data-target="#medicine" data-id="'+sectionItem+'" '+hide+'><img src=\'{{ asset("assets/images/icons/action/edit.png") }}\' /> {{ trans("messages.edit") }}</a>'+
                        '<a id="action-medicine-delete" class="btn-table btn-red @if(!empty($id)) hide @endif" data-toggle="modal" data-target="#confirmDelete" data-message=\'{{ trans("messages.info_delete") }}\' data-section="#item-medicine-'+sectionItem+'" data-number="'+sectionItem+'" data-type="medicine"><img src=\'{{ asset("assets/images/icons/action/delete.png") }}\' /> {{ trans("messages.delete") }}</a>'+
                    '</div>'
                    +field_medicine_racikan
                    +field_qty_racikan
                    +field_unit_racikan
                    +field_price
                    +field_total
                    +field_id_medicine_medical
                    +'<input type="hidden" name="id_list['+sectionItem+']" value="'+sectionItem+'">'
                    +'<input type="hidden" name="medicines['+sectionItem+']" value="'+id+'">'
                    +'<input type="hidden" name="medicine_name['+sectionItem+']" value="'+name+'">'
                    +'<input type="hidden" name="medicine_code['+sectionItem+']" value="'+code+'">'
                    +'<input type="hidden" name="unit_medicine['+sectionItem+']" value="'+id_unit+'">'
                    +'<input type="hidden" name="medicine_qty['+sectionItem+']" value="'+quantity+'">'
                    +'<input type="hidden" name="medicine_total['+sectionItem+']" value="'+total_no_racikan+'">'
                    +'<input type="hidden" name="price_category['+sectionItem+']" value="'+price+'">'
                    +'<input type="hidden" name="signa1['+sectionItem+']" value="'+dose+'">'
                    +'<input type="hidden" name="signa2['+sectionItem+']" value="'+per_day+'">'
                    +'<input type="hidden" name="name_signa3['+sectionItem+']" value="'+time_frame+'">'
                    +'<input type="hidden" name="duration['+sectionItem+']" value="'+total_day+'">'
                    +'<input type="hidden" name="instructions_for_use['+sectionItem+']" value="'+instruction_for_use+'">'
                    +'<input type="hidden" name="medicine_category['+sectionItem+']" value="'+medicine_category+'">'
                    
                    +'<input type="hidden" name="id_signa['+sectionItem+']" value="'+id_signa+'">'
                    +'<input type="hidden" name="id_medicine_medical['+sectionItem+']" value="'+id_medicine_medical+'">'

                    +'<input type="hidden" name="instructions_racikan['+sectionItem+']" value="'+(concoction != "" ? concoction.instruction : "")+'">'
                    +'<input type="hidden" name="dosage['+sectionItem+']" value="'+(concoction != "" ? concoction.dosage : "")+'">'
                    +'<input type="hidden" name="id_package['+sectionItem+']" value="'+(concoction != "" ? concoction.id_inventory_package : "")+'">'
                    +'<input type="hidden" name="id_unit_package['+sectionItem+']" value="'+(concoction != "" ? concoction.id_unit_package : "")+'">'
                    +'<input type="hidden" name="racikan['+sectionItem+']" value="'+(concoction != "" ? concoction.name : "")+'">'
                    +'<input type="hidden" name="need_package['+sectionItem+']" value="'+(concoction != "" ? concoction.need_package : "")+'">'
                    +'<input type="hidden" name="notes['+sectionItem+']" value="'+(concoction != "" ? concoction.notes : "")+'">'
                    +'<input type="hidden" name="total_package['+sectionItem+']" value="'+(concoction != "" ? concoction.total_package : "")+'">'

    if(numItem!="") {
        $("#item-medicine #item-medicine-"+numItem).html(section);
        var getSubtotal = $("#form-add input[name='subtotal']").val();
        var subtotal = (parseInt(getSubtotal) - parseInt(getPrevTotal)) + parseInt(total);
    } else {
        $("#item-medicine").append('<li id="item-medicine-'+itemMedicine+'">'+section+'</li>');
        var getSubtotal = $("#form-add input[name='subtotal']").val();
        var subtotal = parseInt(getSubtotal) + parseInt(total);
        itemMedicine++;
    }

    $("#form-add input[name='subtotal']").val(subtotal);
    $("#form-add #subtotal").html(formatCurrency('{{ $lang }}','',subtotal));

    getTotal();
    getPay();
}

var id_inventory_toggle = "";

$('#medicine').on('show.bs.modal', async function (e) {
    resetMedicine();
    id_inventory_toggle = $(e.relatedTarget).attr('data-id');
    if(id_inventory_toggle == undefined || id_inventory_toggle == "") {
        dose = "";
        unit_dose = "";
        duration = "";
        $("#medicine select[name='name']").val("").trigger("change");
        $("#medicine input[name='id_item']").val("");
        $("#medicine input[name='id_medicine']").val("");
        $("#medicine input[name='id']").val("");
        $("#medicine input[name='unit_medicine']").val("");
        $("#medicine input[name='signa2']").val("");
        $("#medicine input[name='signa1']").val("");
        $("#medicine input[name='duration']").val("");
        $("#interaction-group").removeClass('hide');
        $("#unit_medicine-group").addClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#duration-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#instructions-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $(".signa1-group").attr('class', 'col-xs-3 signa1-group');
        $(".signa2-group").attr('class', 'col-xs-3 signa2-group');
        $("#medicine select[name='signa3']").val("per hari").trigger("change")
        $("#title-medicine").html('<?php echo trans('messages.add_medicine'); ?>');
    } else {
        dose = $("#item-medicine input[name='signa1["+id_inventory_toggle+"]']").val();
        unit_dose = $("#item-medicine input[name='signa2["+id_inventory_toggle+"]']").val();
        duration = $("#item-medicine input[name='duration["+id_inventory_toggle+"]']").val();
        $("#duration-group").removeClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#instructions-group").removeClass('hide');
        $("#interaction-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $("#unit_medicine").addClass('hide');
        $(".signa1-group").attr('class', 'col-xs-3 signa1-group');
        $(".signa2-group").attr('class', 'col-xs-3 signa2-group');
        $("#medicine input[name='medicine_name']").val($("#item-medicine input[name='medicine_name["+id_inventory_toggle+"]']").val()).change();
        
        $("#medicine input[name='id_item']").val(id_inventory_toggle);
        $("#medicine input[name='id_medicine']").val($("#item-medicine input[name='medicines["+id_inventory_toggle+"]']").val());
        $("#medicine input[name='id']").val($("#item-medicine input[name='id_medicine_medical["+id_inventory_toggle+"]']").val());
        $("#medicine input[name='qty']").val($("#item-medicine input[name='medicine_qty["+id_inventory_toggle+"]']").val());
        $("#medicine input[name='price']").val($("#item-medicine input[name='price_category["+id_inventory_toggle+"]']").val());
        $("#medicine input[name='total']").val($("#item-medicine input[name='medicine_total["+id_inventory_toggle+"]']").val());
        
        try {
            let list = await listMedicine($("#item-medicine input[name='medicine_name["+id_inventory_toggle+"]']").val())
            data_unit = list.data;
            let medicines = list.data.find(medicine => medicine.id === $("#item-medicine input[name='medicines["+id_inventory_toggle+"]']").val())
            if(medicines) {
                $("#medicine input[name='medicine_name']").data("id", medicines.id)
                $("#medicine input[name='medicine_name']").data("code", medicines.code)
                selectMedicine(medicines.id, medicines.name, medicines.code)
            }
            $("#medicine #qty_radio_button-group").find('input[name="qty_radio_button"]').each(function(key, value) {
                if(!$(value).is('#change_unit')) {
                    if($(value).val() !== $("#item-medicine input[name='medicine_qty["+id_inventory_toggle+"]']").val()) {
                        $("#change_unit").attr('checked', true)
                    }else {
                        if( $(value).data('id') === $("#item-medicine input[name='unit_medicine["+id_inventory_toggle+"]']").val() ) {
                            $(value).attr('checked', true)
                        }
                    }
                }
            });

            if($("#medicine input[name='radio_bhp']").prop("checked") == true) {
                $("#medicine input[name='value_unit_bhp']").val($("#item-medicine input[name='medicine_qty["+id_inventory_toggle+"]']").val()).change()
                $("#medicine select[name='unit_bhp']").val($("#item-medicine input[name='unit_medicine["+id_inventory_toggle+"]']").val()).trigger("change")

            }

            $("#medicine input[name='signa1']").val(br2nl($("#item-medicine input[name='signa1["+id_inventory_toggle+"]']").val()));
            $("#medicine input[name='signa2']").val(br2nl($("#item-medicine input[name='signa2["+id_inventory_toggle+"]']").val()));
            $("#medicine select[name='signa3']").val($("#item-medicine input[name='name_signa3["+id_inventory_toggle+"]']").val()).trigger("change");
            $("#medicine input[name='duration']").val($("#item-medicine input[name='duration["+id_inventory_toggle+"]']").val());
            $("#medicine input[name='instructions']").val($("#item-medicine input[name='instructions_for_use["+id_inventory_toggle+"]']").val())

            setValueQty($("#item-medicine input[name='signa1["+id_inventory_toggle+"]']").val() * $("#item-medicine input[name='signa2["+id_inventory_toggle+"]']").val() * $("#item-medicine input[name='duration["+id_inventory_toggle+"]']").val())
            $("#medicine select[name='unit_add']").val($("#item-medicine input[name='unit_medicine["+id_inventory_toggle+"]']").val()).trigger('change');
            $("#medicine input[name='qty_change_unit']").val($("#item-medicine input[name='medicine_qty["+id_inventory_toggle+"]']").val()).change()
        
        } catch (error) {
            console.error(error)
        }

        //  DATA OBAT RACIK
        
        // $("#medicine input[name='interaction']").val($("#item-medicine input[name='interaction["+id_inventory_toggle+"]']").val());
        // $("#medicine input[name='racikan']").val($("#item-medicine input[name='racikan["+id_inventory_toggle+"]']").val());
        // if ($("#item-medicine input[name='is_racikan["+id_inventory_toggle+"]']").val() == '1'){
        //     $("#medicine input[name='is_racikan']").click();
        //     $("#medicine input[name='power_medicine']").val($("#item-medicine input[name='power_medicine["+id_inventory_toggle+"]']").val());
        //     $("#medicine input[name='qty_demand']").val($("#item-medicine input[name='qty_demand["+id_inventory_toggle+"]']").val());
        //     $("#medicine select[name='unit_medicine']").val($("#item-medicine input[name='unit_medicine["+id_inventory_toggle+"]']").val()).trigger("change");
        //     $("#medicine input[name='qty_item_medicine']").val($("#item-medicine input[name='medicine_qty["+id_inventory_toggle+"]']").val());
        //     $("#medicine input[name='total_day']").val($("#item-medicine input[name='duration["+id_inventory_toggle+"]']").val());
        //     $("#medicine input[name='price_unit_racikan']").val(formatCurrency('{{ $lang }}','Rp',$("#item-medicine input[name='medicine_total["+id_inventory_toggle+"]']").val()));
        //     $('#form-medicine #medicine_quantity-group label').html("{{ trans('messages.quantity') }}");
        // }

        $("#title-medicine").html('<?php echo trans('messages.edit_medicine'); ?>');        
    }
});

$('#medicine').on('hidden.bs.modal', function (e) {
    resetMedicine();
    $('html, body').animate({
        scrollTop: $("#item-medicine").offset().top-120
    }, 0);
});

$("#medicine").submit(function(event) {
    let id_item = $("#medicine input[name='id_item']").val();
    let id_medicine = $("#medicine input[name='id_medicine']").val();
    let id = $("#medicine input[name='id']").val();
    let name = $("#medicine input[name='medicine_name']").val();
    let code = $("#medicine input[name='code']").val();
    let signa1 = nl2br($("#medicine input[name='signa1']").val());
    let signa2 = nl2br($("#medicine input[name='signa2']").val());
    let signa3 = $("#medicine select[name='signa3'] option:selected").val();
    let qty_unit = $("#medicine input[name='qty_radio_button']:checked").val();
    let instructions_for_use = $("#medicine input[name='instructions']").val();
    let is_racikan = $("#medicine input[name='is_racikan']:checked").val();
    let racikan = $("#medicine input[name='racikan']").val();
    let qty_item_medicine = $("#medicine input[name='qty_item_medicine']").val();
    let total_day = $("#medicine input[name='total_day']").val();
    let unit_name_checked = "";
    let value_unit_name_checked = "";
    let id_unit_checked = "";
    let val_qty_unit = [];
    let total = "";
    let checked_error = true;
    let checked_bhp = true;
    let price = "";
    let medicine = [];
    let medicine_category  = "";
    let id_signa = id_item != "" ? $("#item-medicine input[name='id_signa["+id_item+"]']").val() : "";
    
    $("#qty_radio_button-group").find("input[name='qty_unit']").each(function(key, val) {
        val_qty_unit[key] = $(this).val()
    });
    
    if($("#medicine input[name='radio_bhp']").prop("checked") == true) {
        value_unit_name_checked = $("#medicine input[name='value_unit_bhp']").val()
        unit_name_checked = $("#medicine select[name='unit_bhp'] option:selected").text()
        id_unit_checked = $("#medicine select[name='unit_bhp'] option:selected").val();
        medicine_category = $("#medicine #change_unit_bhp").data('category')
        medicine.push({
            'name': name,
            'unit_name': unit_name_checked,
            'id_medicine': id_medicine,
            'id_unit': id_unit_checked,
            'code': code,
            'id' : id
        });

        price = Number($("#medicine #change_unit_bhp").attr("data-price"))
    }else {
        checked_bhp = false
        $("#medicine input[name^='qty_radio_button']").each(function(key, value) {
            if($(this).prop('checked') == true) {
                price = $(value).data('price');
                total = $(value).data('total');
                if($(this).is("#change_unit")) {
                    unit_name_checked = $("#medicine select[name='unit_add'] option:selected").text();
                    value_unit_name_checked = $("#medicine input[name='qty_change_unit']").val();
                    id_unit_checked = $("#medicine select[name='unit_add'] option:selected").val();
                }else {
                    unit_name_checked = $(value).data('name')
                    value_unit_name_checked = $(value).val();
                    id_unit_checked = $(value).data('id');
                }

            }
        });
        medicine.push({
            'name': name,
            'unit_name': unit_name_checked,
            'id_medicine': id_medicine,
            'id_unit': id_unit_checked,
            'code' : code,
            'id': id
        })
    }

    let unit_name = is_racikan ? $("#medicine select[name='unit_medicine'] option:selected").text(): unit_name_checked;
    let price_medicine = is_racikan ? price_list_racikan : price;
    let duration = is_racikan ? $("#medicine input[name='total_day']").val() : $("#medicine input[name='duration']").val();
    let quantity = is_racikan ? $("#medicine input[name='qty_item_medicine']").val() : value_unit_name_checked;
    let unit = is_racikan ? $("#medicine select[name='unit_medicine'] option:selected").val() : id_unit_checked;
    
    if(duration == 0) {
        duration = 1
    }

    if(!checked_bhp) {
        if(is_racikan != 1) {
            if (!validateMedicine(name, signa1, signa2, signa3, duration, qty_unit, instructions_for_use, total)) return;
        }else {
            if (!validateMedicineRacikan(racikan, name, unit, signa1, signa2, power_medicine, qty_demand)) return;
        }
    }else {
        if(!validateMedicineBhp(name, quantity, unit, instructions_for_use)) return;
    }

    if(id_item == "") {
        $("#item-medicine input[name^='medicines']").each(function(key, value) {
            if(this.value) {
                if (this.value == id_medicine ) {
                    notif(false, "{{ trans('validation.medicine_already_exists') }}");
                    checked_error = false;
                }else {
                    checked_error = true;
                }
            }
        });
    }

    if(checked_error) {
        if (id_item != "") {
            processItemMedicine(medicine, instructions_for_use, 
                signa1, signa2, signa3, duration, 
                quantity, racikan, 
                price,"", "", id_signa, medicine_category, id_item);
            notif(true,"{{ trans('validation.success_edit_medicine') }}");
        } else { 
            processItemMedicine(medicine, instructions_for_use, 
                signa1, signa2, signa3, duration, 
                quantity, racikan, 
                price,"", "" ,id_signa ,medicine_category, "");
            notif(true,"{{ trans('validation.success_add_medicine') }}");
        }
        $("#medicine").modal("toggle");
        resetMedicine();
    }
});

function validateMedicineBhp(name, quantity, unit, instructions_for_use) {
    !name && formValidate(false,['medicine_name', '{{ trans("validation.empty_medicine") }}', true]);
    !quantity && notif(false, "{{ trans('validation.empty_qty_unit') }}")
    !unit && formValidate(false,['unit_bhp', '{{ trans("messages.choice_unit") }}', true]);
    !instructions_for_use && formValidate(false,['instructions', '{{ trans("validation.empty_instruction_for_use") }}', true]);
    return name && quantity && unit && instructions_for_use
}

function validateMedicine(name, signa1, signa2, signa3, duration, qty_unit, instructions_for_use, total) {
	!name && formValidate(false,['medicine_name', '{{ trans("validation.empty_medicine") }}', true]);
    !signa1 && notif(false, "{{ trans('validation.empty_qty_dose') }}")
    !signa2 && notif(false, "{{ trans('validation.empty_unit_amount') }}")
    !signa3 && notif(false, "{{ trans('validation.empty_time') }}")
    !duration && notif(false, "{{ trans('validation.empty_qty_duration') }}")
    !qty_unit && notif(false, "{{ trans('validation.empty_qty_unit') }}")
    !total && notif(false, '{{ trans("validation.empty_price_inventory") }}')
    !instructions_for_use && formValidate(false,['instructions', '{{ trans("validation.empty_instruction_for_use") }}', true]);
	return name && signa1 && signa2 && signa3 && duration && qty_unit && total && instructions_for_use
}

function validateMedicineRacikan(racikan, name, unit, signa1, signa2, power_medicine, qty_demand) {
    !racikan && formValidate(false, ['racikan', '{{ trans("validation.empty_name_concoction") }}', true]);
	!name && formValidate(false,['medicine_name', '{{ trans("validation.empty_medicine") }}', true]);
    !unit && formValidate(false,['unit_medicine', '{{ trans("messages.choice_unit") }}', true]);
    !signa1 && notif(false, "{{ trans('validation.empty_qty_dose') }}")
    !signa2 && notif(false, "{{ trans('validation.empty_unit_amount') }}")
    !power_medicine && formValidate(false, ['medicine_strong', '{{ trans("validation.empty_strength_of_package") }}', true]);
    !qty_demand && formValidate(false, ['medicine_qty', '{{ trans("validation.empty_qty_package") }}', true]);
    return racikan && name && unit && signa1 && signa2 && power_medicine && qty_demand
}

$("#medicine input[name='qty']").focus(function() {
    resetValidation('medicine #qty');
});

$("#medicine input[name='medicine_name']").focus(function() {
    resetValidation('medicine #medicine_name');
});

$("#medicine input[name='instructions']").focus(function(){
    resetValidation('medicine #instructions')
});

$("#medicine #unit_bhp-group .border-group").click(function() {
    resetValidation('medicine #unit_bhp')
})

$('#service select[name=service]').on('change', function() {
    var service = this.value;

    getService(service);
});

$('#service select[name=implementer]').on('change', function() {
    var doctor = this.value;

    getDoctor(doctor);
});

$("#service input[name='qty']").bind("keyup focusout", function () {
    getPrice();
});

$("#service input[name='discount']").bind("keyup focusout", function () {
    getPrice();
});

$("#service #discount_number").click(function() {
    getPrice();
});

$("#service #discount_percent").click(function() {
    getPrice();
});

$("#service #service-group .border-group").click(function() {
    resetValidation('service #service');
});

$("#service input[name='qty']").focus(function() {
    resetValidation('service #qty');
});

$("#service #implementer-group .border-group").click(function() {
    resetValidation('service #implementer');
});

$("#form-add #name-group .border-group").click(function() {
    resetValidation('form-add #name');
});

$("#form-add input[name='discount']").bind("keyup focusout", function () {
    var discount_type = $("#form-add input[name='discount_type']").prop('checked');
    var total = $("#form-add input[name='subtotal']").val();

    if(discount_type != true){
        if(parseInt(this.value) > 100){
            this.value = 100;
        }else if(parseInt(this.value) < 0){
            this.value = 0;
        }else{
            this.value = this.value
        }
    }else{
        if(parseInt(this.value) > total){
            this.value = total;
        }else if(parseInt(this.value) < 0){
            this.value = 0;
        }else{
            this.value = this.value
        }
    }
    getTotal();

    getPay();
});

$("#form-add input[name='insurance_amount']").bind("keyup focusout", function () {
    getTotal();

    getPay();
});

$("#form-add #discount_number").click(function() {
    @if(empty($id))
    getTotal();

    getPay();
    @endif
});

$("#form-add #discount_percent").click(function() {
    getTotal();

    getPay();
});

$("#form-add input[name='prepayment']").bind("keyup focusout", function () {
    getPay();
});

$("#form-add input[name='cash']").bind("keyup focusout", function () {
    getPay();
});



function addPayment() {
    resetValidation('form-add #name');
    
    let name = $("#form-add select[name='name'] option:selected").val();

    if(name == "") {
        formValidate(true, ['form-add #name','{{ trans("validation.empty_patient") }}', true]);
    } else {
        let error = false;

        numFormService = 0;
        formData = new FormData();
        for(i = 0; i < item; i++) {
            let service = $("#form-add input[name='service["+i+"]']").val();
            let qty = $("#form-add input[name='qty["+i+"]']").val();
            let price = $("#form-add input[name='price["+i+"]']").val();
            let total = $("#form-add input[name='total["+i+"]']").val();
            let discount = $("#form-add input[name='discount["+i+"]']").val();
            let discount_type = $("#form-add input[name='discount_type["+i+"]']").val();
            let implementer = $("#form-add input[name='implementer["+i+"]']").val();
            let admin_cost = $("#form-add input[name='admin_cost["+i+"]']").val();
            let note = $("#form-add input[name='note["+i+"]']").val();

            if(total == 0) {
                if(!error){
                    notif(false, '{{ trans("validation.empty_service") }}');
                    error = true;
                }
            }

            try {
                if(service == undefined || qty == undefined || price == undefined || total == undefined || discount == undefined || discount_type == undefined || implementer == undefined ||  admin_cost == undefined || note == undefined) {
                    continue;
                }

                formData.append("service["+numFormService+"][id_service]", service);
                formData.append("service["+numFormService+"][quantity]", qty);
                formData.append("service["+numFormService+"][cost]", price);
                formData.append("service["+numFormService+"][total]", total);
                formData.append("service["+numFormService+"][discount]", discount);
                formData.append("service["+numFormService+"][discount_type]", discount_type);
                formData.append("service["+numFormService+"][admin_cost]", admin_cost);
                formData.append("service["+numFormService+"][note]", note);
                formData.append("service["+numFormService+"][id_doctor]", implementer);

                numFormService++;
                
            }
            catch(err) {
            }             
        }

        numFormMedicine = 0 ;
        let count_no = 0;
        for(i = 1; i <= itemMedicine; i++) {

            let name_racikan = $('input[name="racikan['+numFormMedicine+']"]').val();
            let id_inventory_package = $('input[name="id_package['+numFormMedicine+']"]').val()
            let id_unit_package = $('input[name="id_unit_package['+numFormMedicine+']"]').val()
            let instruction_racikan = $('input[name="instructions_racikan['+numFormMedicine+']"]').val()
            let dosage = $('input[name="dosage['+numFormMedicine+']"]').val()
            let notes = $('input[name="notes['+numFormMedicine+']"]').val()
            let need_package = $('input[name="need_package['+numFormMedicine+']"]').val()
            let total_package = $('input[name="total_package['+numFormMedicine+']"]').val()

            let per_day = $('input[name="signa2['+numFormMedicine+']"]').val();
            let dose = $('input[name="signa1['+numFormMedicine+']"]').val();
            let total_day = $('input[name="duration['+numFormMedicine+']"]').val();
            let instructions_for_use = $('input[name="instructions_for_use['+numFormMedicine+']"]').val()
            let time_frame = $('input[name="name_signa3['+numFormMedicine+']"]').val();
            let id_medical_records = $("#form-add input[name='medical_records']").data("id")
            let id_signa = $("#form-add input[name='id_signa["+numFormMedicine+"]']").val()
            let id_medicine_medical = $("#form-add input[name='id_medicine_medical["+numFormMedicine+"]']").val()

            try {

                if(name_racikan) {
                    count_no++
                    $("#form-add input[name^='medicine_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+key+"][id_medicine]", this.value)
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+key+"][id_medical_records]", id_medical_records_global)
                        }
                    });

                    $("#form-add input[name^='unit_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+key+"][id_unit]", this.value)
                        }
                    });

                    $("#form-add input[name^='qty_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+key+"][quantity]", this.value)
                        }
                    });

                    $("#form-add input[name^='price_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value && this.value != 0) {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][price]", this.value)
                        }else {
                            notif(false, '{{ trans("validation.empty_price_inventory") }}')
                            error = true;
                        }
                    });

                    $("#form-add input[name^='total_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value) {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][total]", this.value)
                        }
                    });

                    $("#form-add input[name='id_medicine_medical_racikan["+numFormMedicine+"]']").each(function(key, value) {
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][id]", this.value ? this.value : "")
                    });

                    formData.append("prescriptions["+ numFormMedicine +"][concoction][id_inventory_package]", id_inventory_package);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][id_unit_package]", id_unit_package);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][instruction]", instruction_racikan);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][dosage]", dosage);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][name]", name_racikan);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][need_package]", need_package);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][total_package]", total_package);
                    formData.append("prescriptions["+ numFormMedicine +"][concoction][notes]", notes);
                    
                    formData.append("prescriptions["+ numFormMedicine +"][signa][id]", id_signa);
                    formData.append("prescriptions["+ numFormMedicine +"][signa][per_day]", per_day);
                    formData.append("prescriptions["+ numFormMedicine +"][signa][dose]", dose);
                    formData.append("prescriptions["+ numFormMedicine +"][signa][total_day]", total_day);
                    formData.append("prescriptions["+ numFormMedicine +"][signa][instruction_for_use]", instructions_for_use);
                    formData.append("prescriptions["+ numFormMedicine +"][signa][time_frame]", time_frame);

                    if(count_no < 10){
                        formData.append("prescriptions["+ numFormMedicine +"][concoction][no_racikan]", "R.0"+count_no+"");
                    }else{
                        formData.append("prescriptions["+ numFormMedicine +"][concoction][no_racikan]", "R."+count_no+"");
                    }
                }else {
                    $("#form-add input[name='medicines["+numFormMedicine+"]']").each(function(key , value) {
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][id_medicine]", this.value);
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+key+"][id_medical_records]", id_medical_records_global)
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][id]", id_medicine_medical)
                    });
                    $('input[name="medicine_qty['+numFormMedicine+']"]').each(function(key , value) {
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][quantity]", this.value)
                    });
                    $('input[name="unit_medicine['+numFormMedicine+']"]').each(function(key ,value) {
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][id_unit]", this.value)
                    });
                    $("input[name='price_category["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value != 0) {
                            formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][price]", this.value)
                        }else {
                            notif(false, '{{ trans("validation.empty_price_inventory") }}')
                            error = true;
                        }
                    });
                    $("input[name='medicine_total["+numFormMedicine+"]']").each(function(key, value) {
                        formData.append("prescriptions["+ numFormMedicine +"][medicines]["+ key +"][total]", this.value)
                    });


                    $("#form-add input[name='medicine_category["+numFormMedicine+"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ numFormMedicine +"][concoction]", "")
                            formData.append("prescriptions["+ numFormMedicine +"][signa]", "");
                        }else {
                            formData.append("prescriptions["+ numFormMedicine +"][signa][id]", id_signa);
                            formData.append("prescriptions["+ numFormMedicine +"][signa][per_day]", per_day);
                            formData.append("prescriptions["+ numFormMedicine +"][signa][dose]", dose);
                            formData.append("prescriptions["+ numFormMedicine +"][signa][total_day]", total_day);
                            formData.append("prescriptions["+ numFormMedicine +"][signa][instruction_for_use]", instructions_for_use);
                            formData.append("prescriptions["+ numFormMedicine +"][signa][time_frame]", time_frame);
                            formData.append("prescriptions["+ numFormMedicine +"][concoction]", "");
                        }
                    });
                }
                numFormMedicine++;
            }
            catch(err) {
                console.error(error)
            }
        }

        if(!error) {
            if(numFormService == 0) {
                notif(false,"{{ trans('validation.min_invoice') }}");
                error = true;
            }
        }

        @if(empty($id) && empty($id_medical_records))

        let medical_records = $("#form-add input[name='medical_records']").data("id");
        
        if(!error) {
            if(medical_records == '') {
                notif(false,"{{ trans('validation.empty_medical_records') }}");
                error = true;
            }
        }
        
        formData.append("id_medical_records", id_medical_records_global);
        @endif

        if(!error) {
            let cash = $("#form-add input[name='cash']").val();
            let credit_card = $("#form-add input[name='credit_card']").val();
            let insurance_amount = $("#form-add input[name='insurance_amount']").val();
            let pay_online = $("#form-add input[name='pay_online']").val();

            cash = cash == "" ? 0 : cash.replace(/\D/g, "");
            credit_card = credit_card == "" ? 0 : credit_card.replace(/\D/g, "");
            insurance_amount = insurance_amount == "" ? 0 : insurance_amount;
            pay_online = pay_online == "" ? 0 : pay_online.replace(/\D/g, "");

            let discount_type = $("#form-add input[name='discount_type']").prop('checked');
            let discount = $("#form-add input[name='discount']").val();
            
            if(discount != '') {
                if(discount_type == true) {
                    discount_type = '$';
                } else {
                    discount_type = '%';
                }
            } else {
                discount_type = '';
            }

            let validate = false;

            @if(!empty($id_medical_records))
                formData.append("id_medical_records", id_medical_records_global);
            @endif

            formData.append("validate", validate);
            formData.append("id_clinic", "{{ $id_clinic }}");
            formData.append("id_patient", name);
            formData.append("number", $("#form-add input[name='invoice_no']").val());
            formData.append("note", nl2br($("#form-add input[name='note']").val()));
            formData.append("discount", discount);
            formData.append("discount_type", discount_type);
            formData.append("subtotal", $("#form-add input[name='subtotal']").val());
            formData.append("total", $("#form-add input[name='total']").val());
            formData.append("prepayment", $("#form-add input[name='prepayment']").val());
            formData.append("cash", cash);
            formData.append("credit_card", credit_card);
            formData.append("online_payment", pay_online);

            if(insurance) {
                formData.append("insurance_amount", insurance_amount);
            }

            formData.append("level", "user");
            formData.append("user", "{{ $id_user }}");

            /* FETCH VACCINE DATA TO FORMDATA */
            appendVaccineToFormData(formData)

            $("#form-add button").attr("disabled", true);
            $("#form-add .btn-primary").addClass("loading");
            $("#form-add .btn-primary span").removeClass("hide");
            $.ajax({
                url: "{{ $api_url }}/{{ $lang }}/invoice",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    
                    $("#form-add button").attr("disabled", false);
                    $("#form-add .btn-primary").removeClass("loading");
                    $("#form-add .btn-primary span").addClass("hide");

                    if(!data.error) {
                        if(!data.success) {
                            if(data.errors.message) {
                                notif(false,""+data.errors.message+"");
                            } else {
                                notif(false,"{{ trans('validation.failed') }}");
                            }
                        } else {

                            resetAdd();

                            $("#form-add #section_detail-patient").collapse("hide");
                            $("#form-add #section_medicine").collapse("hide");
                            $("#form-add #section_note").collapse("hide");
                            
                            notif(true,"{{ trans('validation.success_save_invoice') }}");

                            setTimeout(function(){
                                loading('#/{{ $lang }}/invoice/'+data.id);
                                redirect('#/{{ $lang }}/invoice/'+data.id);
                            }, 500);
                        }
                    } else {
                        notif(false,"{{ trans('validation.failed') }}");
                    }
                     
                },
                error: function(){
                    $("#form-add button").attr("disabled", false);
                    $("#form-add .btn-primary").removeClass("loading");
                    $("#form-add .btn-primary span").addClass("hide");
                    notif(false,"{{ trans('validation.failed') }}");
                }
            });
        }
    }
}


function editPayment(id) {
    let cash = $("#form-add input[name='cash']").val();
    let credit_card = $("#form-add input[name='credit_card']").val();
    let pay_online = $("#form-add input[name='pay_online']").val();
    
    cash = cash == "" ? 0 : cash.replace(/\D/g, "");
    credit_card = credit_card == "" ? 0 : credit_card.replace(/\D/g, "");
    pay_online = pay_online == "" ? 0 : pay_online.replace(/\D/g, "");

    $("#form-add button").attr("disabled", true);

    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("cash", cash);
    formData.append("credit_card", credit_card);
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    formData.append("online_payment", pay_online);


    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-add button").attr("disabled", false);

            
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    notif(false,"{{ trans('validation.failed') }}");
                } else {

                    $("#form-add #section_detail-patient").collapse("hide");
                    $("#form-add #section_medicine").collapse("hide");
                    $("#form-add #section_note").collapse("hide");
                    
                    notif(true,"{{ trans('validation.success_save_invoice') }}");

                    setTimeout(function(){
                        loading('#/{{ $lang }}/invoice/'+data.id);
                        redirect('#/{{ $lang }}/invoice/'+id);
                    }, 500);

                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
             
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
}



$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){

    var section = $section;
    var number = $number;
    var type = $type;

    if(type=="service") {
        removeItem(section, number);
    } else {
        removeItemMedicine(section, number);
    }

    $("#confirmDelete").modal("toggle");
});

$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});

</script>


@include('_partial.confirm_delele')