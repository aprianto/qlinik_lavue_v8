@include('layouts.lib')

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/pembayaran.png') }}" />
                    <h3>
                        {{ trans('messages.invoice') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/invoice"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/payment" onclick="loading($(this).attr('href'));">
                        <div class="image">
                            <div class="canvas">
                                <i class="fa fa-plus"></i>
                            </div>                            
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.add_receipt') }}</span>
                        </div>
                    </a>
                    <div class="dropdown pull-right">
                      <a href="" data-toggle="dropdown">
                          <div class="image">
                              <div class="canvas">
                                  <img src="{{ asset('assets/images/icons/header/more-horizontal.png') }}" />
                              </div>
                          </div>
                          <div class="text">
                              <span>Lainnya</span>
                          </div>
                      </a>
                      <ul class="dropdown-menu" style="z-index: 1;">
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                      </ul>
                    </div>
                </div>
                <div class="dropdown-rs">
                    <ul class="wrapper-dropdown-rs dropdown-scroll">
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                    </ul>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4 form-change">
                                <div class="form-group content-column" id="type-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="border-group">
                                        <select class="form-control" name="type">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form-change">
                                <div class="form-group content-column" id="status-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="border-group">
                                        <select class="form-control" name="status">
                                          <option value=""></option>
                                          <option value="all">Semua Status</option>
                                          <option value="false">Belum Lunas</option>
                                          <option value="true">Lunas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row search">       
                            <div class="col-md-4 form-change">
                                <div class="form-group search-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form-change">
                                <div class="form-group search-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>                 
                            <div class="col-md-4">
                                <div class="form-group search-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Cari nama pasien" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="hide-md">{{ trans('messages.invoice') }}</th>
                                <th>{{ trans('messages.name') }}</th>                                    
                                <th class="hide-lg">{{ trans('messages.total') }}</th>
                                <th class="hide-lg">Total Ditagihkan</th>
                                <th class="hide-lg">{{ trans('messages.status') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')

<script src="{{ asset('assets/js/function.js') }}"></script>
<script>

if("{{Request::is('*invoice*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#invoice-bar").addClass('active-bar')
}else {
    $(".side-menus ul li a").removeClass('active-bar')
}

$(".pull-right").click(function(){ 
    $(".wrapper-dropdown-rs").toggleClass("open-rs")
});

$("input[name='export_key']").val('payments');

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#form-search select[name='type']").select2({
    placeholder: "Pilih jenis pembiayaan",
    allowClear: false
});

$("#form-search select[name='status']").select2({
    placeholder: "Pilih status",
    allowClear: false
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day});
    });
});

$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day});
    });
});

function listInsurance(val) {
    loading_content("#form-search #type-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsurance){
            if(!dataInsurance.error) {
                loading_content("#form-search #type-group", "success");
                var item = dataInsurance.data;
                $("#form-search select[name='type']").html("<option value=''></option> ");
                $("#form-search select[name='type']").append("<option value='all'>Semua jenis pembiayaan</option> ");
                $("#form-search select[name='type']").append("<option value='personal'>{{ trans('messages.personal') }}</option>");
                $("#form-search select[name='type']").append("<option value='agency'>{{ trans('messages.agency') }}</option>");
                for (var i = 0; i < item.length; i++) {
                    $("#form-search select[name='type']").append("<option value='insurance' data-id='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-search select[name='type']").select2({
                    placeholder: "{{ trans('messages.select_financing') }}",
                    allowClear: false
                });
            } else {
                loading_content("#form-search #type-group", "failed");
                $("#form-search #type-group #loading-content").click(function(){ listInsurance(val); });
            }
        },
        error: function(){
            loading_content("#form-search #type-group", "failed");
            $("#form-search #type-group #loading-content").click(function(){ listInsurance(val); });
        }
    })
}

listInsurance('');

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();
dateNowConvert = date.getFullYear()+'-'+monthNow+'-'+dayNow;

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='from_date']").val(unconvertDate(dateNowConvert));
$("#form-search input[name='to_date']").val(unconvertDate(dateNowConvert));

var keySearch = searchRepo;
var keyFromDate = from_dateRepo;
var keyToDate = to_dateRepo;
var numPage = pageRepo;

function search(page, submit, q, from_date, to_date) {
    formData= new FormData();

    if(q == "" && submit == "false") {
        var q = keySearch;
    }

    if(from_date == "" && submit == "false") {
        var from_date = keyFromDate;
    }

    if(to_date == "" && submit == "false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;
    keyFromDate = from_date;
    keyToDate = to_date;

    var financing = $("#form-search select[name=type] option:selected").val();
    var id_insurance = $("#form-search select[name=type] option:selected").attr('data-id');
    var payment = $("#form-search select[name=status] option:selected").val();
    if(financing == undefined){
        financing = '';
    }
    
    if(financing == 'all'){
        financing = '';
    }
    
    if(id_insurance == undefined){
        id_insurance = '';
    }

    if(payment == 'all'){
        payment = '';
    }

    repository(['search',q],['from_date',from_date],['to_date',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice",
        type: "GET",
        data: "q="+q+"&status="+payment+"&financing="+financing+"&id_insurance="+id_insurance+"&from_date="+from_date+"&to_date="+to_date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");
            if(!data.error) {
                $(".table").addClass("table-hover");
                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(data.data.length > 0) {
                    $(".pagination-sec").removeClass("hide");
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(i == 0) {
                                tr = $('<tr class="table-header" />');
                                if(item[i].date==convertDate(dateNow)) {
                                    tr.append("<td colspan='6'>{{ trans('messages.today') }}</td>");
                                } else {
                                    tr.append("<td colspan='6'>"+getFormatDate(item[i].date)+"</td>");
                                }
                                $("#table").append(tr);
                            }
                            
                            var row = "";
                            if(item[i].status.code==true) {
                            } else {
                                row = "row-red"
                            }
                            tr = $('<tr class="'+row+'" />');
                            tr.append("<td class='hide-md'><a href='#/{{ $lang }}/invoice/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].number + "</h1></a><span class='text'>" + getFormatDate(item[i].date) + "</span>"+
                                '<div class="sub show-md">'+
                                    "<span class='general'>{{ trans('messages.total') }}: " + formatCurrency('{{ $lang }}','Rp',item[i].total) + "</span>"+
                                    "<span class='info margin'>" + keel(item[i].status.code) + "</span>"+
                                '</div>'+
                                "</td>");
                            tr.append("<td><a href='#/{{ $lang }}/patient/"+item[i].patient.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].patient.name + "</h1></a><span class='text'>" + item[i].patient.mrn + "</span>"+
                                '<div class="sub show-sm">'+
                                    "<span class='info'><a href='#/{{ $lang }}/invoice/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].number + "</h1></a></span>"+
                                    "<span class='text'>" + getFormatDate(item[i].date) + "</span>"+
                                    "<span class='general'>{{ trans('messages.total') }}: " + formatCurrency('{{ $lang }}','Rp',item[i].total) + "</span>"+
                                    "<span class='info margin'>" + keel(item[i].status.code) + "</span>"+
                                '</div>'+
                                "</td>");
                            tr.append("<td class='hide-lg'>" + formatCurrency('{{ $lang }}','Rp',item[i].subtotal) + "</td>");
                            tr.append("<td class='hide-lg'>" + formatCurrency('{{ $lang }}','Rp',item[i].total) + "</td>");
                            tr.append("<td class='hide-lg'>" + keel(item[i].status.code) + "</td>");

                            var action = "";

                            if(item[i].status.code != true) {
                                action = action+"<a href='#/{{ $lang }}/payment/id/"+item[i].id+"' class='btn-table btn-green' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/pay-green.png') }}\" /> {{ trans('messages.pay') }}</a>";
                            }

                            action = action+"<a href='#/{{ $lang }}/invoice/"+item[i].id+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /> {{ trans('messages.detail') }}</a>"
                                +"<a onclick=\"reportInvoice('"+item[i].number+"','desktop')\" class='btn-table btn-purple'><img src=\"{{ asset('assets/images/icons/action/print.png') }}\" /> {{ trans('messages.print') }}</a>";

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            if(i < to) {
                                if(item[i].date != item[i+1].date) {
                                    tr = $('<tr class="table-header" />');
                                    if(item[i+1].date==convertDate(dateNow)) {
                                        tr.append("<td colspan='6'>{{ trans('messages.today') }}</td>");
                                    } else {
                                        tr.append("<td colspan='6'>"+getFormatDate(item[i+1].date)+"</td>");
                                    }
                                    $("#table").append(tr);
                                }   
                            }
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");
                    if(page == 1 && q == '' && from_date == '' && to_date == '') {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.empty_invoice') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");
                    }   
                }
                pages(page, total, per_page, current_page, last_page, from, to, q, from_date, to_date);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false",q, from_date, to_date); });
            }
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ search(page,"false",q, from_date, to_date); });
        }
    });
}

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val();
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", $("#form-search input[name=q]").val(), convertDate(from_date), convertDate(to_date));
}

$("input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        filterSearch();
    }
});

$("input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        filterSearch();
    }
});

$("#form-search select[name='type']").on('change', function(){
    filterSearch();
});

$("#form-search select[name='status']").on('change', function(){
    filterSearch();
});

$("#form-search").submit(function(event) {
    filterSearch();
});
let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            filterSearch();
        }, 2000);
    }
});

search(pageRepo,"false",searchRepo, dateNowConvert, dateNowConvert);

</script>

@include('invoice.print')

@include('docs.export_with_date')