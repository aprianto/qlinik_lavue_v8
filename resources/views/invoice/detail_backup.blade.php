@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div>


<div class="panel-content">
    <div class="row" id="section-loading">
        <div class="col-md-6">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-8">
            <div class="widget no-color">
                <div class="btn-sec" id="section-status">
                    <ul>
                        <li>                         
                            <a id="status_receipt" class="step"><i class="fa fa-file-o"></i><span>{{ trans("messages.receipt") }}</span></a>
                        </li>
                        <li>
                            <span class="arrow-step fa fa-long-arrow-right"></span>
                        </li>
                        <li>                         
                            <a class="disable step" id="status_pay"><i class="fa fa-money"></i><span>{{ trans("messages.pay") }}</span></a>
                        </li>
                        <li>
                            <span class="arrow-step fa fa-long-arrow-right"></span>
                        </li>
                        <li>                         
                            <a class="disable step" id="status_keel"><i class="fa fa-check-circle"></i><span>{{ trans("messages.keel") }}</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="widget">
                <div class="header" id="section-invoice">
                    <ul>
                        <li>
                            <h5>{{ trans("messages.status") }}</h5>
                            <h3 id="status"></h3>
                        </li>
                        <li>
                            <h5>{{ trans("messages.paid") }}</h5>
                            <h3 id="paid"></h3>
                        </li>
                        <li>
                            <h5>{{ trans("messages.date") }}</h5>
                            <h3 id="date"></h3>
                        </li>
                        <li>
                            <h5>{{ trans("messages.invoice_no") }}</h5>
                            <h3 id="invoice_no"></h3>
                        </li>               
                    </ul>
                </div>
                <div class="payment-sec">
                    <h1 id="total"></h1>
                </div>
                <div class="widget-controls-left">
                    <a id="link-back" onclick="back();loading($(this).attr('href'))"><i class="fa fa-chevron-left"></i></a>
                </div>
                <ul class="detail-profile no-bg-profile" id="section-profile">
                    <li class="profile-header">
                        <img id="photo-patient" src="{{ $storage.'/images/patient/photo.png' }}" />
                        <h3 id="name"></h3>
                        <span id="mrn"></span>
                        <div class="btn-sec"> 
                            <a id="btn-detail-profile">{{ trans("messages.show_detail") }}</a>
                        </div>
                    </li>
                    <div id="detail-profile" class="hide">
                        <li class="profile-features">                            
                            <div class="col-md-6"><i class="icon fa fa-birthday-cake"></i> {{ trans("messages.birth_date") }} :</div> 
                            <div class="col-md-6"><span id="birth_date"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-mars"></i> {{ trans("messages.gender") }} : </div>
                            <div class="col-md-6"><span id="gender"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-magic"></i> {{ trans("messages.smoke") }} : </div> 
                            <div class="col-md-6"><span id="smoke"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-venus"></i> {{ trans("messages.blood_type") }} : </div>
                            <div class="col-md-6"><span id="blood_type"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-file-text-o"></i> {{ trans("messages.no_identity") }} : </div>
                            <div class="col-md-6"><span id="no_identity"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-map-marker"></i> {{ trans("messages.address") }} : </div>
                            <div class="col-md-6"><span id="address"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-envelope-o"></i> {{ trans("messages.email") }} : </div>
                            <div class="col-md-6"><span id="email"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-phone"></i> {{ trans("messages.phone") }} : </div>
                            <div class="col-md-6"><span id="phone"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-users"></i> {{ trans("messages.patient_category") }} : </div> 
                            <div class="col-md-6"><span id="patient_category"></span></div>
                        </li>
                        <li class="profile-features">
                            <div class="col-md-6"><i class="icon fa fa-money"></i> {{ trans("messages.financing") }} : </div>
                            <div class="col-md-6"><span id="financing"></span></div>
                        </li>
                    </div>
                </ul>
                <div class="payment-sec">
                    <h5 class="border-widget hide" id="header-note">
                        {{ trans("messages.note") }}
                    </h5>
                    <p class="note hide" id="note"></p>                    
                    <h5 class="border-widget" id="header_service">
                        {{ trans("messages.service") }}
                    </h5>
                    <ul id="section-service">
                        
                    </ul>
                    <h5 class="border-widget hide" id="header-medicine">
                        {{ trans("messages.medicine") }}
                    </h5>
                    <ul class="hide" id="section-medicine">
                        
                    </ul>
                    <h5 class="border-widget">
                        {{ trans("messages.total") }}
                    </h5>
                    <ul>
                        <li id="section-subtotal">
                            <i class="success fa fa-dollar"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3>{{ trans("messages.subtotal") }}</h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="subtotal"></span>
                                </div>
                            </div>
                        </li>
                        <li class="new-line hide" id="section-discount">
                            <i class="failed fa fa-percent"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.discount") }}</span></h3>
                                </div>
                                <div class="right">
                                    <span class="price minus" id="discount"></span>
                                </div>
                            </div>
                        </li>                        
                        <li>
                            <i class="success fa fa-dollar"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3>{{ trans("messages.total_payment") }}</h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="total_payment"></span>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-insurance">
                            <i class="failed fa fa-certificate"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.insurance") }}</span> (<b id="insurance-name"></b>)</h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="insurance"></span>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-prepayment">
                            <i class="failed fa fa-arrow-left"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.prepayment") }}</span></h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="prepayment"></span>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-cash">
                            <i class="failed fa fa-money"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.cash") }}</span></h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="cash"></span>
                                </div>
                            </div>
                        </li>
                        <li class="new-line hide" id="section-credit_card">
                            <i class="failed fa fa-credit-card"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.credit_card") }}</span></h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="credit_card"></span>
                                </div>
                            </div>
                        </li>
                        <li class="new-line hide" id="section-change">
                            <i class="success fa fa-exchange"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3>{{ trans("messages.change") }}</h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="change"></span>
                                </div>
                            </div>
                        </li>
                        <li class="new-line hide" id="section-remaining_payment">
                            <i class="success fa fa-dollar"></i>
                            <div class="list-info">
                                <div class="left">
                                    <h3>{{ trans("messages.remaining_payment") }}</h3>
                                </div>
                                <div class="right">
                                    <span class="price" id="remaining_payment"></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="action-sec">
                    <div class="content">
                        <div class="action-list">
                            <form id="form-receipt-phone">
                                <input type="text" name="phone" onkeydown="number(event)" />
                                <i class="icon-input fa fa-phone"></i>
                                <button class="btn-input">
                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    {{ trans("messages.send") }}
                                </button>
                            </form>
                        </div>
                        <div class="action-list">      
                            <form id="form-receipt-email">
                                <input type="text" name="email" />
                                <i class="icon-input fa fa-envelope-o"></i>
                                <button class="btn-input">
                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    {{ trans("messages.send") }}
                                </button>
                            </form>
                        </div>
                        <div class="action-list">
                            <a id="link-print"><i class="fa fa-print"></i> {{ trans("messages.print") }}</a>
                        </div>
                        <div class="action-list">
                            <a id="link-receipt" href="#/{{ $lang }}/payment/id/{{ $id }}"" onclick="loading($(this).attr('href'));"><i class="fa fa-money"></i> {{ trans("messages.pay") }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-receipt-email input[name='email']").bind("keyup", function () {
    var email = $("#form-receipt-email input[name='email']").val();

    if(email=="") {
        $("#form-receipt-email button").prop("disabled", true);
    } else {
        $("#form-receipt-email button").prop("disabled", false);
    }
});

$("#form-receipt-phone input[name='phone']").bind("keyup", function () {
    var phone = $("#form-receipt-phone input[name='phone']").val();

    if(phone=="") {
        $("#form-receipt-phone button").prop("disabled", true);
    } else {
        $("#form-receipt-phone button").prop("disabled", false);
    }
});

$("#btn-detail-profile").click(function(){ 
    var section = $("#detail-profile").css('display');
    if(section=="block") {
        $("#btn-detail-profile").html("{{ trans('messages.show_detail') }}");
        $("#detail-profile").addClass("hide");
    } else {
        $("#btn-detail-profile").html("{{ trans('messages.hide_detail') }}");
        $("#detail-profile").removeClass("hide");
    }
});


function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');

                var no_identity = "-";
                if(data.patient.id_number!="" && data.patient.id_number!=null) {
                    no_identity = data.patient.id_number+" ("+data.patient.id_type+")";
                }


                $("#section-profile #photo-patient").replaceWith(thumbnail('lg','patient','{{ $storage }}',data.patient.photo,data.patient.name));
                $("#section-profile #mrn").html(data.patient.mrn);
                $("#section-profile #name").html(data.patient.name);
                $("#section-profile #birth_date").html(getFormatDate(data.patient.birth_date)+" ("+getAge(data.patient.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#section-profile #gender").html(getGender(data.patient.gender));

                var smoke = "";
                if(data.patient.smoke==1) {
                    smoke = '<i class="fa fa-check-circle success"></i>';
                } else {
                    smoke = '<i class="fa fa-minus-circle failed"></i>';
                }

                $("#section-profile #smoke").html(smoke);
                $("#section-profile #blood_type").html(checkNull(data.patient.blood_type));
                $("#section-profile #no_identity").html(no_identity);

                $("#section-profile #address").html(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
                $("#section-profile #email").html(checkNull(data.patient.email));
                $("#section-profile #phone").html("+"+data.patient.phone_code+""+data.patient.phone);

                $("#form-receipt-email input[name='email']").val(data.patient.email);
                if(data.patient.email=="") {
                    $("#form-receipt-email button").prop("disabled", true);
                }

                var phone = data.patient.phone_code+""+data.patient.phone;
                $("#form-receipt-phone input[name='phone']").val(phone);
                if(phone=="") {
                    $("#form-receipt-phone button").prop("disabled", true);
                }

                try {
                    $("#section-profile #patient_category").html(data.patient.patient_category.name);
                } catch(err) {}

                $("#section-profile #financing").html(getFinancing(data.patient.financing));

                if(nullToEmpty(data.note)!="") {
                    $("#header-note").removeClass("hide");
                    $("#note").removeClass("hide");
                    $("#note").html(data.note);
                }

                var insurance = 0;
                if(data.insurance!=null) {
                    insurance = data.insurance.amount;
                }

                var paid = parseInt(data.prepayment)+parseInt(data.cash)+parseInt(data.credit_card)+parseInt(insurance);

                $("#section-invoice #paid").html(formatCurrency('{{ $lang }}','Rp',paid));
                if(paid>0) {
                    $("#section-status #status_pay").removeClass("disable");
                } else {
                    $("#section-status #status_pay").addClass("disable");
                }

                if(data.status.code==true) {
                    $("#link-receipt").addClass("hide");
                    $("#section-status #status_keel").removeClass("disable");
                    $("#section-invoice #status").addClass("success");
                    $("#section-invoice #status").html("{{ trans('messages.keel') }}");
                    $("#total").html('<span class="fa fa-check-circle success"></span> '+formatCurrency('{{ $lang }}','Rp',data.total)+'');
                } else {
                    $("#link-receipt").removeClass("hide");
                    $("#section-status #status_keel").addClass("disable");
                    $("#section-invoice #status").addClass("failed");
                    $("#section-invoice #status").html("{{ trans('messages.not_keel') }}");
                    $("#total").html('<span class="fa fa-check-circle success"></span>');
                    $("#total").html('<span class="fa fa-minus-circle failed"></span> '+formatCurrency('{{ $lang }}','Rp',data.total)+'');

                    var remaining_payment = parseInt(data.total)-parseInt((parseInt(data.prepayment)+parseInt(data.cash)+parseInt(data.credit_card)+parseInt(insurance)));

                    $("#section-remaining_payment").removeClass("hide");
                    $("#remaining_payment").html(formatCurrency('{{ $lang }}','Rp',remaining_payment));
                }

                $("#path").html(data.number);
                $("#section-invoice #invoice_no").html(data.number);
                $("#section-invoice #date").html(getFormatDate(data.date));


                if(data.service!=null) {
                    $("#header-service").removeClass("hide");
                    $("#section-service").removeClass("hide");

                    for(var i=0; i<data.service.length; i++) {
                        var new_line = "";
                        if(nullToZero(data.service[i].discount)==0 && nullToZero(data.service[i].admin_cost)==0) {
                            new_line = "new-line";
                        }

                        var total = parseInt(nullToZero(data.service[i].admin_cost))+parseInt(data.service[i].quantity*data.service[i].cost);

                        $("#section-service").append('<li class="'+new_line+'">'+
                            '<i class="success fa fa-plus-circle"></i>'+
                            '<div class="list-info">'+
                                '<div class="left">'+
                                    '<h3>'+data.service[i].service.name+' <span>'+data.service[i].service.code+'</span></h3>'+
                                    '<p>{{ trans("messages.quantity") }}: '+data.service[i].quantity+'</p>'+
                                '</div>'+
                                '<div class="right">'+
                                    '<span class="price">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                                '</div>'+
                            '</div>'+                      
                        '</li>');

                        if(nullToZero(data.service[i].admin_cost)!=0) {
                            var new_line = "";
                            if(nullToZero(data.service[i].discount)==0) {
                                new_line = "new-line";
                            }

                            $("#section-service").append('<li class="'+new_line+'">'+
                                '<i class="failed fa fa-plus-circle"></i>'+
                                '<div class="list-info">'+
                                    '<div class="left">'+
                                        '<h4>{{ trans("messages.admin_cost") }}: '+formatCurrency('{{ $lang }}','Rp',data.service[i].admin_cost)+'</h4>'+
                                    '</div>'+
                                    '<div class="right">'+
                                    '</div>'+
                                '</div>'+
                            '</li>');
                        }

                        if(nullToZero(data.service[i].discount)!=0) {
                            var discount = "";
                            if(data.service[i].discount_type=="$") {
                                discount = data.service[i].discount;
                            } else {
                                discount = (data.service[i].discount/100)*total;
                            }

                            $("#section-service").append('<li class="new-line">'+
                                '<i class="failed fa fa-minus-circle"></i>'+
                                '<div class="list-info">'+
                                    '<div class="left">'+
                                        '<h3><span>{{ trans("messages.discount") }}: '+formatCurrency('{{ $lang }}','Rp',discount)+'</span></h3>'+
                                    '</div>'+
                                    '<div class="right">'+
                                    '</div>'+
                                '</div>'+
                            '</li>');
                        }
                    }
                } else {
                    $("#header-service").addClass("hide");
                    $("#section-service").addClass("hide");
                }



                if(data.medicine!="") {
                    $("#header-medicine").removeClass("hide");
                    $("#section-medicine").removeClass("hide");

                    for(var i=0; i<data.medicine.length; i++) {

                        $("#section-medicine").append('<li class="new-line">'+
                            '<i class="success fa fa-plus-circle"></i>'+
                            '<div class="list-info">'+
                                '<div class="left">'+
                                    '<h3>'+data.medicine[i].medicine.name+' <span>'+data.medicine[i].medicine.code+'</span></h3>'+
                                    '<p>{{ trans("messages.quantity") }}: '+data.medicine[i].quantity+'</p>'+
                                '</div>'+
                                '<div class="right">'+
                                    '<span class="price">'+formatCurrency('{{ $lang }}','Rp',data.medicine[i].total)+'</span>'+
                                '</div>'+
                            '</div>'+                      
                        '</li>');
                    }
                } else {
                    $("#header-medicine").addClass("hide");
                    $("#section-medicine").addClass("hide");
                }


                $("#subtotal").html(formatCurrency('{{ $lang }}','Rp',data.subtotal));
                $("#total_payment").html(formatCurrency('{{ $lang }}','Rp',data.total));


                if(nullToZero(data.discount)!=0) {
                    $("#section-discount").removeClass("hide");

                    var discount = "";
                    if(data.discount_type=="$") {
                        discount = data.discount;
                    } else {
                        discount = (data.discount/100)*data.subtotal;
                    }
                    $("#discount").html("-"+formatCurrency('{{ $lang }}','Rp',discount)); 
                } else {
                    $("#section-discount").addClass("hide");
                    $("#section-subtotal").addClass("new-line");
                }

                if(data.insurance!=null) {
                    $("#section-insurance").removeClass("hide");
                    $("#insurance").html(formatCurrency('{{ $lang }}','Rp',data.insurance.amount));
                    $("#insurance-name").html(data.insurance.name);
                } else {
                    $("#section-insurance").addClass("hide");
                }

                if(data.prepayment!=0) {
                    $("#section-prepayment").removeClass("hide");
                    $("#prepayment").html(formatCurrency('{{ $lang }}','Rp',data.prepayment));
                } else {
                    $("#section-prepayment").addClass("hide");
                }

                if(data.cash!=0) {
                    $("#section-cash").removeClass("hide");
                    $("#cash").html(formatCurrency('{{ $lang }}','Rp',data.cash));
                } else {
                    $("#section-cash").addClass("hide");
                }

                if(data.credit_card!=0) {
                    $("#section-credit_card").removeClass("hide");
                    $("#credit_card").html(formatCurrency('{{ $lang }}','Rp',data.credit_card));
                } else {
                    $("#section-credit_card").addClass("hide");

                    $("#section-cash").addClass("new-line");
                }

                if(paid>data.total) {
                    var change = parseInt(paid)-parseInt(data.total);

                    $("#section-change").removeClass("hide");
                    $("#change").html(formatCurrency('{{ $lang }}','Rp',change));
                } else {
                    $("#section-change").addClass("hide");
                }

                $("#link-print").attr("onclick", "reportInvoice('"+data.number+"','desktop')");


            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

detail("{{ $id }}");

$("#form-receipt-phone").submit(function(event) {
    event.preventDefault();

    $("#form-receipt-phone button").attr("disabled", true);

    formData= new FormData();

    formData.append("phone", $("#form-receipt-phone input[name=phone]").val());
    $("#form-receipt-phone button").addClass("loading");
    $("#form-receipt-phone button span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/receipt/sms/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-receipt-phone button").attr("disabled", false);
    
            $("#form-receipt-phone button").removeClass("loading");
            $("#form-receipt-phone button span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    if(data.errors.message) {
                        notif(false, ""+data.errors.message+"" );
                    } else {
                        notif(false, ""+data.errors.phone+"" );    
                    }                    
                } else {
                    notif(true,"{{ trans('validation.success_send_invoice_receipt') }}");            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-receipt-phone button").attr("disabled", false);

            $("#form-receipt-phone button").removeClass("loading");
            $("#form-receipt-phone button span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#form-receipt-email").submit(function(event) {
    event.preventDefault();

    $("#form-receipt-email button").attr("disabled", true);

    formData= new FormData();

    formData.append("email", $("#form-receipt-email input[name='email']").val());
    $("#form-receipt-email button").addClass("loading");
    $("#form-receipt-email button span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/receipt/email/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-receipt-email button").attr("disabled", false);
    
            $("#form-receipt-email button").removeClass("loading");
            $("#form-receipt-email button span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    if(data.errors.message) {
                        notif(false, ""+data.errors.message+"" );
                    } else {
                        notif(false, ""+data.errors.email+"" );    
                    }  
                } else {
                    notif(true,"{{ trans('validation.success_send_invoice_receipt') }}");            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-receipt-email button").attr("disabled", false);

            $("#form-receipt-email button").removeClass("loading");
            $("#form-receipt-email button span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>

@include('invoice.print')