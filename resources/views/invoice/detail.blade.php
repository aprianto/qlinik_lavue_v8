@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div>

<div class="panel-content">
    <div class="row" id="section-loading">
        <div class="col-md-6">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-8">
            <div class="widget widget-detail">
                <div class="widget-header radius">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.detail') }}
                    </h3>
                </div>
                <div class="header" id="section-header">
                    <ul>
                        <li>
                            <h5>{{ trans("messages.date") }}</h5>
                            <h3 id="date"></h3>
                        </li>
                        <li>
                            <h5>{{ trans("messages.paid") }}</h5>
                            <h3 id="paid"></h3>
                        </li>
                        <li class="right">
                            <h5>{{ trans("messages.status") }}</h5>
                            <h3 id="status"></h3>
                        </li> 
                    </ul>
                </div>
                <div class="form-elements-sec detail-form" id="section-invoice">
                    <form role="form" class="sec" id="form-add">
                        <input type="hidden" name="category" value="" />
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.invoice_no') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.invoice_no') }}" name="invoice_no" id="invoice_no" disabled="dsabled">
                        </div>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.patient') }}</label>
                            <input type="text" class="form-control" placeholder="<?php echo trans('messages.patient'); ?>" name="name" id="name" disabled="dsabled">
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.number_medical_records') }}</label>
                            <input type="text" class="form-control" placeholder="<?php echo trans('messages.mrn'); ?>" name="mrn" id="mrn" disabled="dsabled">
                        </div>
                        <div class="form-group">
                            <div id="accordion" class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading gray">
                                        <h4 class="panel-title" data-target="#section_detail-patient" data-parent="#accordion" data-toggle="collapse">
                                            <a class="accordion-toggle collapsed">
                                            {{ trans('messages.patient_data') }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="section_detail-patient" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="cell hide" id="loading-detail_patient">
                                                <div class="card">
                                                    <span class="three-quarters">Loading&#8230;</span>
                                                </div>
                                            </div>
                                            <div class="cell hide" id="reload-detail_patient">
                                                <div class="card">
                                                    <span class="reload fa fa-refresh"></span>
                                                </div>
                                            </div>
                                            <div  id="panel-detail_patient">
                                                <ul class="detail-profile">
                                                    <div>
                                                        <li class="profile-features">
                                                            <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.phone_number') }}</span>
                                                                    <span id="phone"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.email') }}</span>
                                                                    <span id="email"></span>
                                                                </div>
                                                            </div>
                                                        </li>                        
                                                        <li class="profile-features">  
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.birth_date') }}</span>
                                                                    <span id="birth_date"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">  
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.gender') }}</span>
                                                                    <span id="gender"></span>
                                                                </div>
                                                            </div>
                                                        </li>     
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.no_identity') }}</span>
                                                                    <span id="no_identity"></span>
                                                                </div>
                                                            </div>
                                                        </li>            
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.blood_type') }}</span>
                                                                    <span id="blood_type"></span>
                                                                </div>
                                                            </div>
                                                        </li>                        
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.smoking') }}?</span>
                                                                    <span id="smoke"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <h4>{{ trans("messages.address") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.address') }}</span>
                                                                    <span id="address"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="profile-features">
                                                            <h4>{{ trans("messages.patient_data") }}</h4>
                                                        </li>
                                                        <li class="profile-features">
                                                            <div class="detail-sec">
                                                                <div class="icon-sec">
                                                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                                                </div>
                                                                <div class="info-sec">
                                                                    <span class="title">{{ trans('messages.patient_category') }}</span>
                                                                    <span id="patient_category"></span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="list-service">
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active">
                                            <h4 class="panel-title" data-target="#section_service" data-parent="#accordion" data-toggle="collapse">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.service') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_service">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item">                
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active">
                                            <h4 class="panel-title" data-target="#section_medicine" data-parent="#accordion" data-toggle="collapse">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.medicine') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_medicine">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item-medicine">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading gray active">
                                            <h4 class="panel-title" data-target="#section_vaccine" data-parent="#accordion" data-toggle="collapse">
                                              <a class="accordion-toggle collapsed">
                                              {{ trans('messages.vaccine') }}
                                              </a>
                                            </h4>
                                        </div>
                                        <div class="panel-collapse in" id="section_vaccine">
                                            <div class="panel-body panel-sec">
                                                <ul class="list-item" id="item-vaccine">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading gray active">
                                        <h4 class="panel-title" data-target="#section_note" data-parent="#accordion" data-toggle="collapse">
                                          <a class="accordion-toggle collapsed">
                                          {{ trans('messages.note') }}
                                          </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse in" id="section_note">
                                        <div class="panel-body panel-sec">
                                            <div class="form-group" id="note-group">
                                                <label class="control-label">{{ trans('messages.note') }}</label>
                                                <p id="note">-</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="payment-sec summary-sec">
                    <ul>
                        <li id="section-subtotal">
                            <div class="list-info">
                                <div class="left">
                                    <h3>{{ trans("messages.subtotal") }}</h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="subtotal"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="new-line hide" id="section-discount">
                            <div class="list-info">
                                <img src="{{ asset('assets/images/icons/info/discount.png') }}" />
                                <div class="left">
                                    <h3><span>{{ trans("messages.discount") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="discount"></span>
                                    </div>
                                </div>
                            </div>
                        </li>   
                        <li class="hide" id="section-insurance-total">
                            <div class="list-info" style="border-top: 1px solid #f0f0f0;">
                                <div class="left">
                                    <h3>Total</h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="insurance-total"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-insurance">
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.insurance") }} (<span id="insurance-name"></span>)</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="insurance"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="list-info" style="border-top: 1px solid #f0f0f0;">
                                <div class="left">
                                    <h3>{{ trans("messages.total_payment") }}</h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="total_payment"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-prepayment">
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.prepayment") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="prepayment"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-cash">
                            <div class="list-info">
                                <img src="{{ asset('assets/images/icons/info/pay-cash.png') }}" />
                                <div class="left">
                                    <h3><span>{{ trans("messages.cash") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="cash"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-credit_card">
                            <div class="list-info">
                                <img src="{{ asset('assets/images/icons/info/pay-credit-card.png') }}" />
                                <div class="left">
                                    <h3><span>{{ trans("messages.credit_card") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="credit_card"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-pay_online">
                            <div class="list-info">
                                <img src="{{ asset('assets/images/Pay_Online.png') }}" />
                                <div class="left">
                                    <h3><span>{{ trans('messages.pay_online') }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column number">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="pay_online"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-change">
                            <div class="list-info" style="border-top: 1px solid #f0f0f0;">
                                <div class="left">
                                    <h3><span>{{ trans("messages.change") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="change"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-remaining_payment">
                            <div class="list-info">
                                <div class="left">
                                    <h3><span>{{ trans("messages.remaining_payment") }}</span></h3>
                                </div>
                                <div class="right">
                                    <div class="column">Rp</div>
                                    <div class="column column-right">
                                        <span class="number" id="remaining_payment"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="hide" id="section-pay">
                            <div class="list-info">
                                <div class="right">
                                    <a href="#/{{ $lang }}/payment/id/{{ $id }}" class="btn btn-primary" onclick="loading($(this).attr('href'));"><img src="{{ asset('assets/images/icons/action/pay-white.png') }}" /> {{ trans('messages.pay') }}</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="action-sec">
                    <div class="content">
                        <div class="action-list">
                            <form id="form-receipt-phone">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>{{ trans('messages.phone_number') }}</label>
                                                <input type="text" name="phone" placeholder="{{ trans('messages.input_phone_number') }}" onkeydown="number(event)" />
                                            </div>
                                            <div class="col-md-6">
                                                <button class="btn-input">
                                                    <img src="{{ asset('assets/images/icons/action/icon-action-send-sms.png') }}" />
                                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                    {{ trans("messages.send_via_sms") }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="action-list">      
                            <form id="form-receipt-email">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>{{ trans('messages.email') }}</label>
                                                <input type="text" name="email" placeholder="{{ trans('messages.input_email') }}" />
                                            </div>
                                            <div class="col-md-6">
                                                <button class="btn-input">
                                                    <img src="{{ asset('assets/images/icons/action/icon-action-send-email.png') }}" />
                                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                    {{ trans("messages.send_via_email") }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="action-sec">
                    <a id="link-print"><img src="{{ asset('assets/images/icons/action/print-blue.png') }}" /> {{ trans("messages.print_receipt") }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-receipt-email input[name='email']").bind("keyup", function () {
    var email = $("#form-receipt-email input[name='email']").val();

    if(email == "") {
        $("#form-receipt-email button").prop("disabled", true);
    } else {
        $("#form-receipt-email button").prop("disabled", false);
    }
});

$("#form-receipt-phone input[name='phone']").bind("keyup", function () {
    var phone = $("#form-receipt-phone input[name='phone']").val();

    if(phone == "") {
        $("#form-receipt-phone button").prop("disabled", true);
    } else {
        $("#form-receipt-phone button").prop("disabled", false);
    }
});


function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');
                $("#section-detail #date").html(getFormatDate(data.date));

                var insurance = 0;
                if(data.insurance != null) {
                    insurance = data.insurance.amount;
                }

                $("#section-detail #invoice_no").val(data.number);
                $("#section-detail #mrn").val(data.patient.mrn);
                $("#section-detail #name").val(data.patient.name);
                $("#section-detail #birth_date").html(getFormatDate(data.patient.birth_date)+" ("+getAge(data.patient.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#section-detail #gender").html(getGender(data.patient.gender));

                var smoke = (data.patient.smoke == 1) ? '{{ trans("messages.smoking") }}' : '{{ trans("messages.no_smoking") }}';

                $("#section-detail #smoke").html(smoke);
                $("#section-detail #blood_type").html(checkNull(data.patient.blood_type));
                $("#section-detail #no_identity").html(no_identity);
                $("#section-detail #address").html(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
                $("#section-detail #email").html(checkNull(data.patient.email));
                $("#section-detail #phone").html("+"+data.patient.phone_code+""+data.patient.phone);

                try {
                    $("#section-detail #patient_category").html(data.patient.patient_category.name);
                } catch(err) {}

                $("#section-detail #financing").html(getFinancing(data.patient.financing));
                $("#form-receipt-email input[name='email']").val(data.patient.email);

                if(data.patient.email == "" || data.patient.email == null) {
                    $("#form-receipt-email button").prop("disabled", true);
                }

                var phone = (data.patient.phone_code != null ? data.patient.phone_code : '')+""+(data.patient.phone != null ? data.patient.phone : '');
                $("#form-receipt-phone input[name='phone']").val(phone);

                if(phone == "" || data.patient.phone == null) {
                    $("#form-receipt-phone button").prop("disabled", true);
                }
                
                $("#section-detail #note").html(checkNull(data.note));

                if(data.service != null) {
                    for(i = 0; i < data.service.length; i++) {

                        var service_name = data.service[i].service.name;
                        var qty = data.service[i].quantity;
                        var admin_cost = data.service[i].admin_cost;
                        var discount = data.service[i].discount;
                        var discount_type = data.service[i].discount_type;
                        var total = parseInt(nullToZero(data.service[i].admin_cost))+parseInt(data.service[i].quantity*data.service[i].cost);
                        var note = nullToEmpty(data.service[i].note);
                        var getDiscount = '';
                        var discountType = '';
                        if( discount == '' || discount == 0 || discount == null ) {
                            getDiscount = '-';
                        } else if(discount_type == "$") {
                            getDiscount = formatCurrency('{{ $lang }}','Rp',discount);
                            discountType = '$';
                            total = total-discount;
                        } else {
                            getDiscount = discount+'%';
                            discountType = '%';
                            total = Math.round(total-((discount/100)*total));
                        }

                        var getAdmin_cost = '';
                        if(admin_cost == '' || admin_cost == 0 || admin_cost == null) {
                            getAdmin_cost = '';
                        } else {
                            getAdmin_cost = '{{ trans("messages.admin_cost") }}: '+formatCurrency('{{ $lang }}','Rp',admin_cost);
                        }

                        var section = '<div class="item-sec">'+
                            '<div class="left-sec">'+
                                '<h1>'+service_name+'</h1>'+
                                '<span class="text">'+getAdmin_cost+'</span>'+
                                '<span class="text">{{ trans("messages.discount") }}: '+getDiscount+'</span>'+
                                '<span class="text">{{ trans("messages.quantity") }}: '+qty+'</span>'+
                                '<span class="text">'+note+'</span>'+
                            '</div>'+
                            '<div class="right-sec text-right">'+
                                '<span class="money">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                            '</div>'+
                        '</div>';
                        $("#section-detail #item").append('<li>'+section+'</li>');
                    }
                } else {
                    $("#section-detail #item").append('<li><span class="no-data">{{ trans("messages.no_service") }}</span></li>');
                }

                if(data.prescriptions.length > 0) {
                    $("#header-medicine").removeClass("hide");
                    $("#section-medicine").removeClass("hide");

                    data.prescriptions.forEach((prescription, index) => {
                        let medicineList = ''
                        let name = prescription.concoction == null || prescription.concoction == undefined  ? prescription.medicines[0].name : prescription.concoction.name + " ({{ trans('messages.concoction') }})"
                        let unitName = prescription.concoction == null || prescription.concoction == undefined ? prescription.medicines[0].unit_name : prescription.concoction.instruction
                        let qty = prescription.concoction == null || prescription.concoction == undefined ? prescription.medicines[0].quantity : prescription.concoction.total_package
                        let total = 0

                        prescription.medicines.forEach((medicine, index) => {
                            total += parseInt(medicine.quantity * medicine.price)

                            if (prescription.concoction !== null || prescription.concoction == undefined) {
                                medicineList += '<li>' + medicine.name +' ('+medicine.quantity+' '+(medicine.unit_name ? medicine.unit_name : '-')+')</li>';
                            }
                        })

                        let sectionToRender = '';
                        if(prescription.signa === null || prescription.signa === undefined) {
                            sectionToRender = '<div class="item-sec">'+
                                '<div class="left-sec">'+
                                    '<h1>'+name+'</h1>'+
                                    '<ul class="list_racikan">'+medicineList+'</ul>'+
                                    '<span class="text">{{ trans("messages.quantity") }}: '+ qty +' '+(unitName || '-')+'</span>'+
                                '</div>'+
                                '<div class="right-sec text-right">'+
                                    '<span class="money">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                                '</div>'+
                                '</div>';
                        } else {
                            sectionToRender = '<div class="item-sec">'+
                                '<div class="left-sec">'+
                                    '<h1>'+name+'</h1>'+
                                    '<ul class="list_racikan">'+medicineList+'</ul>'+
                                    '<span class="text">{{ trans("messages.quantity") }}: '+ qty +' '+(unitName || '-')+'</span>'+
                                    '<span class="text">{{ trans("messages.dose") }}: '+(prescription.concoction ? prescription.concoction.notes : prescription.signa.dose + " x " + prescription.signa.per_day + " " + unitName + " {{ trans('messages.during')}} " + prescription.signa.total_day + " {{ trans('messages.day') }} ")+'</span>' +
                                '</div>'+
                                '<div class="right-sec text-right">'+
                                    '<span class="money">'+formatCurrency('{{ $lang }}','Rp',total)+'</span>'+
                                '</div>'+
                                '</div>';
                        }
                        
                        $("#section-detail #item-medicine").append('<li>'+sectionToRender+'</li>');
                    })
                } else {
                    $("#section-detail #item-medicine").append('<li><span class="no-data">{{ trans("messages.no_medicine") }}</span></li>');
                }

                /* ADD RENDER VACCINE ITEM */
                if (data.vaccines.length > 0) {
                    data.vaccines.forEach((vaccine) => {
                        let section = '<div class="item-sec">'+
                            '<div class="left-sec">'+
                                '<h1>'+vaccine.name+'</h1>'+
                                '<span class="text">{{ trans("messages.quantity") }}: '+vaccine.quantity+'</span>'+
                            '</div>'+
                            '<div class="right-sec text-right">'+
                                '<span class="money">'+formatCurrency('{{ $lang }}','Rp',(vaccine.price*vaccine.quantity))+'</span>'+
                            '</div>'+
                        '</div>';
                        $("#section-detail #item-vaccine").append('<li>'+section+'</li>');
                    })
                } else {
                    $("#section-detail #item-vaccine").append('<li><span class="no-data">{{ trans("messages.no_vaccine") }}</span></li>');
                }
                /* END */

                $("#subtotal").html(formatCurrency('{{ $lang }}','',data.subtotal));
                $("#total_payment").html(formatCurrency('{{ $lang }}','',data.total));

                if(nullToZero(data.discount) != 0) {
                    $("#section-discount").removeClass("hide");
                    var discount = "";
                    if(data.discount_type == "$") {
                        discount = data.discount;
                    } else {
                        discount = (data.discount/100)*data.subtotal;
                    }
                    $("#discount").html("-"+formatCurrency('{{ $lang }}','',discount)); 
                } else {
                    var discount = 0;
                    $("#section-discount").addClass("hide");
                }

                if(data.insurance != null) {
                    $("#section-insurance-total").removeClass("hide");
                    $("#section-insurance").removeClass("hide");
                    $("#insurance-total").html(formatCurrency('{{ $lang }}','',data.subtotal-discount));
                    $("#insurance").html(formatCurrency('{{ $lang }}','',data.insurance.amount));
                    $("#insurance-name").html(data.insurance.name);
                } else {
                    $("#section-insurance-total").addClass("hide");
                    $("#section-insurance").addClass("hide");
                }

                if(data.prepayment!= 0) {
                    $("#section-prepayment").removeClass("hide");
                    $("#prepayment").html(formatCurrency('{{ $lang }}','',data.prepayment));
                } else {
                    $("#section-prepayment").addClass("hide");
                }

                if(data.cash != 0) {
                    $("#section-cash").removeClass("hide");
                    $("#cash").html(formatCurrency('{{ $lang }}','',data.cash));
                } else {
                    $("#section-cash").addClass("hide");
                }
                if(data.online_payment != 0) {
                    $("#section-pay_online").removeClass("hide");
                    $("#pay_online").html(formatCurrency('{{ $lang }}', '', data.online_payment));
                }else {
                    $("#section-pay_online").addClass("hide");
                }

                if(data.credit_card != 0) {
                    $("#section-credit_card").removeClass("hide");
                    $("#credit_card").html(formatCurrency('{{ $lang }}','',data.credit_card));
                } else {
                    $("#section-credit_card").addClass("hide");
                }

                var paid = parseInt(data.prepayment)+parseInt(data.cash)+parseInt(data.credit_card)+parseInt(data.online_payment)+parseInt(insurance)+parseInt(discount);

                $("#section-detail #paid").html(formatCurrency('{{ $lang }}','Rp',paid));

                if(data.status.code == true) {
                    $("#section-detail #link-receipt").addClass("hide");
                    $("#section-detail #section-pay").addClass("hide");
                    $("#section-detail #status").addClass("success");
                    $("#section-detail #status").html("{{ trans('messages.keel') }}");
                } else {
                    $("#section-detail #link-receipt").removeClass("hide");
                    $("#section-detail #section-pay").removeClass("hide");
                    $("#section-detail #status").addClass("failed");
                    $("#section-detail #status").html("{{ trans('messages.not_keel') }}");

                    var remaining_payment = parseInt(data.subtotal)-parseInt((parseInt(data.prepayment)+parseInt(data.cash)+parseInt(data.credit_card)+parseInt(data.online_payment)+parseInt(insurance)+parseInt(discount)));

                    $("#section-remaining_payment").removeClass("hide");
                    $("#remaining_payment").html(formatCurrency('{{ $lang }}','',remaining_payment));
                }

                if(paid > data.subtotal) {
                    var change = parseInt(paid)-parseInt(data.subtotal);
                    $("#section-change").removeClass("hide");
                    $("#change").html(formatCurrency('{{ $lang }}','',change));
                } else {
                    $("#section-change").addClass("hide");
                }
                $("#link-print").attr("onclick", "reportInvoice('"+data.number+"','desktop')");
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");
                $("#reload-detail .reload").click(function(){ detail(id); });
            }
        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");
            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    });
}

detail("{{ $id }}");

$("#form-receipt-phone").submit(function(event) {
    event.preventDefault();

    $("#form-receipt-phone button").attr("disabled", true);

    formData= new FormData();
    formData.append("phone", $("#form-receipt-phone input[name=phone]").val());
    
    $("#form-receipt-phone button").addClass("loading");
    $("#form-receipt-phone button img").addClass("hide");
    $("#form-receipt-phone button span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/receipt/sms/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-receipt-phone button").attr("disabled", false);
            $("#form-receipt-phone button").removeClass("loading");
            $("#form-receipt-phone button img").removeClass("hide");
            $("#form-receipt-phone button span").addClass("hide");
            if(!data.error) {
                if(!data.success) {
                    if(data.errors.message) {
                        notif(false, ""+data.errors.message+"" );
                    } else {
                        notif(false, ""+data.errors.phone+"" );    
                    }                    
                } else {
                    notif(true,"{{ trans('validation.success_send_invoice_receipt') }}");            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-receipt-phone button").attr("disabled", false);
            $("#form-receipt-phone button").removeClass("loading");
            $("#form-receipt-phone button img").removeClass("hide");
            $("#form-receipt-phone button span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#form-receipt-email").submit(function(event) {
    event.preventDefault();

    $("#form-receipt-email button").attr("disabled", true);

    formData= new FormData();
    formData.append("email", $("#form-receipt-email input[name='email']").val());
    
    $("#form-receipt-email button").addClass("loading");
    $("#form-receipt-email button img").addClass("hide");
    $("#form-receipt-email button span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/invoice/receipt/email/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-receipt-email button").attr("disabled", false);
            $("#form-receipt-email button").removeClass("loading");
            $("#form-receipt-email button img").removeClass("hide");
            $("#form-receipt-email button span").addClass("hide");
            if(!data.error) {
                if(!data.success) {
                    if(data.errors.message) {
                        notif(false, ""+data.errors.message+"" );
                    } else {
                        notif(false, ""+data.errors.email+"" );    
                    }  
                } else {
                    notif(true,"{{ trans('validation.success_send_invoice_receipt') }}");            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-receipt-email button").attr("disabled", false);
            $("#form-receipt-email button").removeClass("loading");
            $("#form-receipt-email button img").removeClass("hide");
            $("#form-receipt-email button span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});

$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});

</script>

@include('invoice.print')