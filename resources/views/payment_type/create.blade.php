<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_payment_type') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">                
                <div class="modal-body">
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.name') }}" name="name">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="price-group">
                        <label class="control-label">{{ trans('messages.price') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.price') }}" name="price" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="month-group">
                        <label class="control-label">{{ trans('messages.month') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.month') }}" name="month" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <div class="sf-check">
                            <label><input type="checkbox" name="limit_access"><span></span> {{ trans('messages.limit') }}</label>
                        </div>
                    </div>
                    <div class="form-group hide" id="limit_doctor-group">
                        <label class="control-label">{{ trans('messages.limit_doctor') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.limit_doctor') }}" name="limit_doctor" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group hide" id="limit_next_doctor-group">
                        <label class="control-label">{{ trans('messages.limit_next_doctor') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.limit_next_doctor') }}" name="limit_next_doctor" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

function resetAdd() {
    resetValidation('form-add #name','form-add #price','form-add #month','form-add #limit_doctor','form-add #limit_next_doctor');
    $("#form-add input[name=name]").val("");
    $("#form-add input[name=price]").val("");
    $("#form-add input[name=month]").val("");
    $('#form-add input[name="limit_access"]').prop('checked',false);
    $("#form-add #limit_doctor-group").addClass('hide');
    $("#form-add input[name='limit_doctor']").val('');
    $("#form-add #limit_next_doctor-group").addClass('hide');
    $("#form-add input[name='limit_next_doctor']").val('');
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add input[name="limit_access"]').click(function() {    
    var checked = this.checked;
    if(checked==true) {
        $("#form-add #limit_doctor-group").removeClass('hide');
        $("#form-add #limit_next_doctor-group").removeClass('hide');
    } else {
        $("#form-add #limit_doctor-group").addClass('hide');
        $("#form-add input[name='limit_doctor']").val('');
        $("#form-add #limit_next_doctor-group").addClass('hide');
        $("#form-add input[name='limit_next_doctor']").val('');
    }
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$('#form-add input[name=price]').focus(function() {
    resetValidation('form-add #price');
});

$('#form-add input[name=month]').focus(function() {
    resetValidation('form-add #month');
});

$('#form-add input[name=limit_doctor]').focus(function() {
    resetValidation('form-add #limit_doctor');
});

$('#form-add input[name=limit_next_doctor]').focus(function() {
    resetValidation('form-add #limit_next_doctor');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #name','form-add #price','form-add #month','form-add #limit_doctor','form-add #limit_next_doctor');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();
    formData.append("validate", false);
    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("price", $("#form-add input[name=price]").val());
    formData.append("month", $("#form-add input[name=month]").val());

    var limit_access = $('#form-add input[name="limit_access"]').is(':checked');
    if(limit_access==true) {
        limit_access = 1;
    } else {
        limit_access = 0;
    }

    formData.append("limit_access", limit_access);

    if(limit_access==1) {
        formData.append("limit_doctor", $("#form-add input[name=limit_doctor]").val());
        formData.append("limit_next_doctor", $("#form-add input[name=limit_next_doctor]").val());
    }
    
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment/type",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.name, true], ['form-add #price',data.errors.price, true], ['form-add #month',data.errors.month, true], ['form-add #limit_doctor',data.errors.limit_doctor, true], ['form-add #limit_next_doctor',data.errors.limit_next_doctor, true]);
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_add_payment_type') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>