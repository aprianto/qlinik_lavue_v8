<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-pencil"></i> {{ trans('messages.edit_payment_type') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="hide">                
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.name') }}" name="name">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="price-group">
                        <label class="control-label">{{ trans('messages.price') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.price') }}" name="price" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="month-group">
                        <label class="control-label">{{ trans('messages.month') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.month') }}" name="month" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <div class="sf-check">
                            <label><input type="checkbox" name="limit_access"><span></span> {{ trans('messages.limit') }}</label>
                        </div>
                    </div>
                    <div class="form-group hide" id="limit_doctor-group">
                        <label class="control-label">{{ trans('messages.limit_doctor') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.limit_doctor') }}" name="limit_doctor" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group hide" id="limit_next_doctor-group">
                        <label class="control-label">{{ trans('messages.limit_next_doctor') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.limit_next_doctor') }}" name="limit_next_doctor" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

function resetEdit() {
    resetValidation('form-edit #name','form-edit #price','form-edit #month','form-edit #limit_doctor','form-edit #limit_next_doctor');
    $("#form-edit input[name=name]").val("");
    $("#form-edit input[name=price]").val("");
    $("#form-edit input[name=month]").val("");
    $('#form-edit input[name="limit_access"]').prop('checked',false);
    $("#form-edit #limit_doctor-group").addClass('hide');
    $("#form-edit input[name='limit_doctor']").val('');
    $("#form-edit #limit_next_doctor-group").addClass('hide');
    $("#form-edit input[name='limit_next_doctor']").val('');
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment/type/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading-edit').addClass('hide');

            if(!data.error) {
                $('#form-edit').removeClass('hide');

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=name]").val(data.name);
                $("#form-edit input[name=price]").val(data.price);
                $("#form-edit input[name=month]").val(data.month);

                if(data.limit_access==1) {
                    $('#form-edit input[name="limit_access"]').prop('checked',true);
                    $("#form-edit #limit_doctor-group").removeClass('hide');
                    $("#form-edit input[name='limit_doctor']").val(data.limit_doctor);
                    $("#form-edit #limit_next_doctor-group").removeClass('hide');
                    $("#form-edit input[name='limit_next_doctor']").val(data.limit_next_doctor);
                } else {
                    $('#form-edit input[name="limit_access"]').prop('checked',false);
                    $("#form-edit #limit_doctor-group").addClass('hide');
                    $("#form-edit input[name='limit_doctor']").val('');
                    $("#form-edit #limit_next_doctor-group").addClass('hide');
                    $("#form-edit input[name='limit_next_doctor']").val('');
                }             

            } else {
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    })
}

$('#edit').on('show.bs.modal', function (e) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    resetEdit();

    var id = $(e.relatedTarget).attr('data-id');
    edit(id);
});

$('#form-edit input[name="limit_access"]').click(function() {    
    var checked = this.checked;
    if(checked==true) {
        $("#form-edit #limit_doctor-group").removeClass('hide');
        $("#form-edit #limit_next_doctor-group").removeClass('hide');
    } else {
        $("#form-edit #limit_doctor-group").addClass('hide');
        $("#form-edit input[name='limit_doctor']").val('');
        $("#form-edit #limit_next_doctor-group").addClass('hide');
        $("#form-edit input[name='limit_next_doctor']").val('');
    }
});

$('#form-edit input[name=name]').focus(function() {
    resetValidation('form-edit #name');
});

$('#form-edit input[name=price]').focus(function() {
    resetValidation('form-edit #price');
});

$('#form-edit input[name=month]').focus(function() {
    resetValidation('form-edit #month');
});

$('#form-edit input[name=limit_doctor]').focus(function() {
    resetValidation('form-edit #limit_doctor');
});

$('#form-edit input[name=limit_next_doctor]').focus(function() {
    resetValidation('form-edit #limit_next_doctor');
});

$("#form-edit").submit(function(event) {
    event.preventDefault();
    resetValidation('form-edit #name','form-edit #price','form-edit #month','form-edit #limit_doctor','form-edit #limit_next_doctor');

    $("#form-edit button").attr("disabled", true);

    var id = $("#form-edit input[name=id]").val();
    formData= new FormData();
    formData.append("validate", false);
    formData.append("_method", "PATCH");
    formData.append("name", $("#form-edit input[name=name]").val());
    formData.append("price", $("#form-edit input[name=price]").val());
    formData.append("month", $("#form-edit input[name=month]").val());

    var limit_access = $('#form-edit input[name="limit_access"]').is(':checked');
    if(limit_access==true) {
        limit_access = 1;
    } else {
        limit_access = 0;
    }

    formData.append("limit_access", limit_access);

    if(limit_access==1) {
        formData.append("limit_doctor", $("#form-edit input[name=limit_doctor]").val());
        formData.append("limit_next_doctor", $("#form-edit input[name=limit_next_doctor]").val());
    }
    
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment/type/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-edit button").attr("disabled", false);
    
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-edit #name',data.errors.name, true], ['form-edit #price',data.errors.price, true], ['form-edit #month',data.errors.month, true], ['form-edit #limit_doctor',data.errors.limit_doctor, true], ['form-edit #limit_next_doctor',data.errors.limit_next_doctor, true]);
                } else {

                    resetEdit();

                    $("#edit").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_edit_payment_type') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);

            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>