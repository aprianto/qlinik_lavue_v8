<div id="progressBar">
    <div class="loader"></div>
</div>

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">
                    @if($lang=='id')
                    <h1>Dokter</h1>
                    <p>Halaman dokter berisi daftar dari dokter yang ada di klinik dan berisi informasi tentang nama
                        dokter, jam praktik, dan status dokter hari ini. Di halaman dokter terdapat menu - menu sebagai
                        berikut:</p>
                    <ol>
                        <li><b>Tambah dokter</b> untuk menambah dokter di klinik.</li>
                        <li><b>Jadwal dokter</b> untuk mengatur jadwal dari dokter di klinik.</li>
                        <li><b>Komisi dokter</b> untuk menambah komisi dari dokter.</li>
                        <li><b>Tampilan kalender</b> untuk menampilkan dalam bentuk kalender dari jadwal dokter yang ada
                            di klinik. Ketika dalam bentuk kalender, menu ini akan berubah menjadi <b>Tampilan
                                daftar</b> dan ketika di klik akan merubah tampilan dari bentuk kalender ke daftar
                            dokter.</li>
                    </ol>

                    <h2>Tambah dokter</h2>
                    <p>Halaman tambah dokter digunakan untuk menambah dokter baru. Untuk menambah dokter, Anda perlu
                        mengisi data - data seperti <b>Poli, Email</b> (apabila data email sudah ada di data dokter,
                        maka akan muncul pilihan dari data dokter dan apabila data dokter tersebut dipilih, maka data
                        dokter akan otomatis terisi dari data dokter yang dipilih tersebut)<b>, No telepon</b> (apabila
                        data no telepon sudah ada di data dokter, maka akan muncul pilihan dari data dokter dan apabila
                        data dokter tersebut dipilih, maka data dokter akan otomatis terisi dari data dokter yang
                        dipilih tersebut)<b>, Alamat, Negara, Provinsi, Kota/Kabupaten, Kecamatan, Kode pos, </b> dan
                        <b>Catatan</b>. Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b>
                        untuk menyimpan data dokter.</p>

                    <h2>Jadwal dokter</h2>
                    <p>Halaman jadwal dokter digunakan untuk mengatur jadwal dari dokter di klinik. Untuk mengatur
                        jadwal tersebut, Anda perlu mengisi data - data seperti <b>Poli, Dokter, Periode Aktif,
                            Jadwal,</b> dan <b>Cuti</b>. Setelah data yang diperlukan telah diisi, Anda bisa klik tombol
                        <b>Simpan</b> untuk menyimpan data jadwal dokter.</p>
                    <ul>
                        <li>Untuk pengisian <b>Periode Aktif</b>, tanggal akhir aktif tidak harus diisi. Bila tanggal
                            kosong, maka dokter akan dianggap aktif untuk seterusnya.</li>
                        <li>Untuk pengisian <b>Jadwal</b>, bisa melakukan centang pada hari yang ada jadwal. Bila jadwal
                            di hari itu ada banyak, maka bisa meng-klik tombol <span class="fa fa-plus"></span> untuk
                            menambah lebih dari 1 jadwal. Dalam pengisian jadwal terdapat <b>Waktu mulai</b> (waktu
                            mulai dokter praktik), <b>Waktu selesai</b> (waktu kapan prakter berakhir), <b>Maksimal
                                daftar</b> (kuota banyaknya pasien yang bisa daftar), <b>Estimasi pelayanan (Menit)</b>
                            (waktu untuk perkiraan lamanya dokter melakukan perawatan pada setiap pasien), dan apabila
                            pada jadwal tersebut bisa melakukan janji dengan dokter di jam tertentu, maka bisa centang
                            pada <b>Dengan perjanjian</b>. Sehingga pada waktu pasien mendaftar pada jadwal tersebut,
                            pasein bisa juga mengatur waktu rawat dengan dokter sesuai keinginan. Dan dokter perlu
                            melakukan konfirmasi apakah dokter tersebut bersedia melakukan perawatan pada pasien di
                            waktu yang telah ditentukan tersebut.</li>
                        <li>Untuk pengisian <b>Cuti</b>, bisa diisi atau tidak. Bila diisi, maka dokter akan di anggap
                            tidak aktif pada range tanggal cuti tersebut.</li>
                    </ul>

                    <h2>Komisi dokter</h2>
                    <p>Halaman komisi dokter berguna untuk mengatur jumlah komisi pada saat dokter melakukan perawat di
                        layanan tertentu. Jadi komisi akan masuk ke laporan komisi dokter apabila pasien melakukan
                        pembayaran pada layanan yang ada hubungannya dengan dokter pada komisi tersebut. Untuk lebih
                        lanjut bisa dibaca <a href="#/{{ $lang }}/help/commission">disini</a>.</p>

                    <h2>Tampilan kalender</h2>
                    <p>Halaman tampilan kalender berguna untuk menampilkan dalam bentuk kalender dari jadwal dokter yang
                        ada di klinik. Jadwal akan ditampilkan dalam bentuk kalender dimulai dari tanggal sekarang
                        sampai 7 hari kedepan. Jadwal waktu dokter prakter akan di tampilkan pada kalender tersebut,
                        mulai dari waktu mulai praktik sampai selesai praktik. Dan bisa juga melakukan filter kalender
                        dari tanggal berapa sesuai dengan keinginan.</p>

                    <br /><br />
                    <p>Di bagian kanan pada daftar dokter, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                    <ol>
                        <li><b>Jadwal</b> untuk mengatur jadwal dari dokter di klinik.</li>
                        <li><b>Detail</b> untuk melihat detail data dokter.</li>
                        <li><b>Ubah</b> untuk mengubah data dokter.</li>
                        <li><b>Hapus</b> untuk menghapus dokter dari klinik.</li>
                    </ol>
                    @else
                    <h1> Doctor </h1>
                    <p> The doctor's page contains a list of the doctors in the clinic and the information about the
                        doctor's name, practice schedule, and doctor's status today. In the doctor's page there are
                        several menus that showed as follows: </p>
                    <ol>
                        <li> <b> Add a doctor </b> to add a doctor in the clinic. </li>
                        <li> <b> Doctor's schedule </b> to arrange a schedule from the doctor in the clinic. </li>
                        <li> <b> Doctor's commission </b> to increase doctor's commission. </li>
                        <li> <b> Calendar view </b> to display in the form of calendar from the doctor's schedule in the
                            clinic. When in the form of a calendar, this menu will be change to <b> List view </b> and
                            when it is getting clicked it will change the appearance of the calendar form to the list of
                            the doctors. </li>
                    </ol>

                    <h2> Add doctor </h2>
                    <p> The doctor's added page is used to add a new doctor. To add a doctor, you need to fill in data
                        such as <b> Poly, Email </b> (if the email data is already filled in the doctor's data, there is
                        a choice that will appear from the doctor's data and if the doctor's data is selected, then the
                        doctor's data will be automatically filled by the data of the selected doctor) <b>, Phone number
                        </b> (if the telephone number data is already filled in the doctor's data, there is a choice
                        will appear from the doctor's data and if the doctor's data is selected, then the doctor's data
                        will automatically be filled from data of the selected doctor) <b>, Address, Country, Province,
                            City / Regency, District, Postal Code, </b> and <b> Note </b>. After the required data has
                        been filled in, you can click the <b> Save </b> button to save the doctor's data. </p>

                    <h2> Doctor's schedule </h2>
                    <p> The doctor's schedule page is used to set the schedule of doctors in the clinic. To set the
                        schedule, you need to fill in data such as <b> Poly, Doctor, Active Period, Schedule, </b> and
                        <b> Leave </b>. After the required data has been filled in, you can click the <b> Save </b>
                        button to save the doctor's schedule data. </p>
                    <ul>
                        <li> To load <b> Active Period </b>, the active's end date does not have to be filled in. If the
                            date is empty, the doctor will be considered as an active period for the remaining period.
                        </li>
                        <li> To load the <b> Schedule </b>, you can check on the scheduled day. If there are many
                            schedules on that day, you can click the <span class="fa fa-plus"> </span> button to add
                            more than one schedule. In the schedule filling section there are <b> Starting time </b>
                            (doctor's start time practice), <b> End time </b> (time when the practice ends), <b> Maximum
                                list </b> (quota's space for the patients that can register), <b> Estimated service
                                (Minutes) </b> (time for estimated length of time the doctor carries out treatment for
                            each patient), and if on that schedule can make an appointment with a doctor at certain
                            hours, then can tick the button <b> With agreement </b>. So that when the patient registers
                            on the schedule, patients also can arrange the time of treatment with the doctor as it is
                            required. And the doctor needs to confirm whether the doctor is willing to treat the patient
                            at the specified time. </li>
                        <li> To load <b> Leave </b>, it can be filled or not. When it is filled, the doctor will be
                            considered as an inactive doctor in the range of the leave date. </li>
                    </ul>

                    <h2> Doctor's commission </h2>
                    <p> The doctor's commission page is useful for managing the amount of commission when a doctor
                        performs a treatment at a particular service. So, the commission will moves into the doctor's
                        commission report if the patient makes a payment for a service that has something to do with the
                        doctor on the commission. For more information, please read <a
                            href="#/{{ $lang }}/help/commission"> here </a>. </p>

                    <h2> Calendar view </h2>
                    <p> The calendar display page is useful for displaying in the form of a calendar from the doctor's
                        schedule in the clinic. The schedule will be displayed in the form of a calendar starting from
                        the current date to the next 7 days. The doctor's practice time schedule for the practicing
                        doctor will be displayed on the calendar, starting from the time of the practice until the
                        completion of the practice. And also can choose the calendar filter related on the date that you
                        needed. </p>

                    <br /> <br />
                    <p> On the right side of the doctor list, there is an action button that functions as follows: </p>
                    <ol>
                        <li> <b> Schedule </b> to set the doctor's schedule in the clinic. </li>
                        <li> <b> Details </b> to see doctor's data details. </li>
                        <li> <b> Change </b> to change doctor's data. </li>
                        <li> <b> Delete </b> to delete the doctor from the clinic. </li>
                    </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
