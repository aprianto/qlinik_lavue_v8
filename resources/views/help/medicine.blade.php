<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">     
                    @if($lang=='id')           	
                	<h1>Obat</h1>
                	<p>Halaman obat berisi daftar dari obat yang ada di klinik dan berisi informasi tentang nama obat dan kode obat. Di halaman obat terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Tambah obat</b> untuk menambah obat di klinik.</li>
                	</ol>

                	<h2>Tambah obat</h2>
                	<p>Halaman tambah obat digunakan untuk menambah obat baru. Untuk menambah obat, Anda perlu mengisi data - data seperti <b>Nama obat, Kode obat, Perusahaan obat, Harga obat</b> (harga bisa ditambahkan beberapa harga berdasarkan kategori pasien yang tersedia di klinik dengan cara klik <span class="fa fa-plus"></span> <b>Tambah harga</b>). Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data obat.</p>

                	<br /><br />
                	<p>Di bagian kanan pada daftar obat, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Detail</b> untuk melihat detail data obat.</li>
                		<li><b>Ubah</b> untuk mengubah data obat.</li>
                		<li><b>Hapus</b> untuk menghapus obat dari klinik.</li>                		
                	</ol>
                    @else
                    <h1> Medication </h1>
                	<p> The medicine page contains a list of medicines in the clinic and information about the name of the medicine and the medicine code. On the medicine page there are menus which have different functions. Those different functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Add medicine </b> to add medication at the clinic. </li>
                	</ol>

                	<h2> Add medicine </h2>
                	<p> The added medicine page is used to add a new medicine. To add medicine, you need to fill in the required data such as; <b> Name of medicine, Code of medicine, Company of medicine, price of medicine </b> (price can be added based on patient category available in the clinic by clicking <span class = " fa fa-plus "> </span> <b> Add price</b>). After the required data has been filled in, you can click the <b> Save </b> button to save the medicine data. </p>

                	<br /> <br />
                	<p> On the right side of the medication list, there is an action button that have different functions. Those functions will be detailed  as follows: </p>
                	<ol>
                	<li> <b> Details </b> to see detailed medicine data. </li>
                	<li> <b> Change </b> to change medicine data. </li>
                	<li> <b> Delete </b> to remove medication from the clinic. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
