<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">           
                    @if($lang=='id')     	
                	<h1>Pasien</h1>
                	<p>Halaman pasien berisi daftar dari pasien yang ada di klinik dan berisi informasi tentang nama pasien, kategori, tanggal lahir, dan status. Di halaman pasien terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Tambah pasien</b> untuk menambah pasien di klinik.</li>
                		<li><b>Rawat jalan</b> untuk ke menu rawat jalan di klinik.</li>
                	</ol>

                	<h2>Tambah pasien</h2>
                	<p>Halaman tambah pasien digunakan untuk menambah pasien baru. Untuk menambah pasien, Anda perlu mengisi data - data seperti <b>Email</b> (apabila data email sudah ada di data pasien, maka akan muncul pilihan dari data pasien dan apabila data pasien tersebut dipilih, maka data pasien akan otomatis terisi dari data pasien yang dipilih tersebut)<b>, No telepon</b> (apabila data no telepon sudah ada di data pasien, maka akan muncul pilihan dari data pasien dan apabila data pasien tersebut dipilih, maka data pasien akan otomatis terisi dari data pasien yang dipilih tersebut)<b>, Nama lengkap, Tanggal lahir, Umur</b> (terisi otomatis sesuai tanggal lahir)<b>, Jenis kelamin, Merokok, Jenis ID, No ID, Golongan darah, Alamat, Negara, Provinsi, Kota/Kabupaten, Kecamatan, Kode pos, Kategori pasien</b> (klik tombol <span class="fa fa-plus"></span> untuk menambah kategori pasien baru)<b>,</b> dan <b>Pembiayaan</b>. Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data pasien.</p>

                	<br /><br />
                	<p>Di bagian kanan pada daftar pasien, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Rekam medis</b> untuk melakukan rekam medis pada pasien.</li>
                        <li><b>Detail</b> untuk melihat detail data pasien.</li>
                		<li><b>Ubah</b> untuk mengubah data pasien.</li>
                		<li><b>Cetak</b> untuk mencetak kartu pasien.</li>
                        <li><b>Nonaktif</b> untuk menonaktifkan pasien.</li>
                        <li><b>Aktif</b> untuk mengaktifkan pasien.</li>                		
                	</ol>
                    @else
                    <h1> Patient </h1>
                	<p> The patient page contains a list of patients in the clinic and information about the patient's name, category, date of birth, and status. On the patient's page there are several menus that can be used for the user. Those menus will be detailed  as follows: </p>
                	<ol>
                	<li> <b> Add patients </b> to add patients in the clinic. </li>
                	<li> <b> Outpatient </b> to go to the outpatient menu in the clinic. </li>
                	</ol>

                	<h2> Add patients </h2>
                	<p> The patient add page is used for adding new patients. To add the patient, you need to fill in the required data such as; <b> Email </b> (if the email is already filled on the box field of patient's email, then it will shows the option from patient's email data that already filled from the previous section (filled email) ,Patient's email data will automatically filled from the selected email's data) <b>, telephone number </b> (if the telephone number's data is already written or filled on the box field of phone number, the phone number data will appeared and if the patient's phone number data is selected in this section, the patient's phone number data will be automatically filled from the data that is selected) <b>, Full name, Date of birth, Age </b> (automatically filled based on patient's date of birth) <b>, Gender, Smoking, Type of ID, No ID, Blood Type, Address, Country, Province, City / Regency, District, Postal Code, Patient Category </b> (click the <span class = "fa fa-plus"> </span> button to add new patient categories) <b>, </b> and <b > Financing </b>. After the required data has been filled in, you can click the <b> Save </b> button to save patient data. </p>

                	<br /> <br />
                	<p> On the right side of the patient list, there is an action button that has different functions. Those functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Medical records </b> to do medical records for patients. </li>
                	        <li> <b> Details </b> to see patient data details. </li>
                	<li> <b> Change </b> to change patient data. </li>
                	<li> <b> Print </b> to print patient cards. </li>
                	        <li> <b> Disabled </b> to deactivate the patient. </li>
                	        <li> <b> Active </b> to activate the patient. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


