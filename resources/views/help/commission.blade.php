<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc"> 
                    @if($lang=='id')               	
                	<h1>Komisi Dokter</h1>
                	<p>Komisi dokter berguna untuk mengatur jumlah komisi pada saat dokter melakukan perawatan di layanan tertentu. Jadi komisi akan masuk ke laporan komisi dokter apabila pasien melakukan pembayaran pada layanan yang ada hubungannya dengan dokter pada komisi tersebut. Halaman komisi dokter berisi daftar dari komisi dokter yang ada di klinik dan berisi informasi tentang nama komisi, poli dari komisi, layanan dari komisi, dokter dari komisi, dan jumlah komisi. Di halaman komisi terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Tambah komisi dokter</b> untuk menambah dokter di klinik.</li>
                	</ol>

                	<h2>Tambah komisi dokter</h2>
                	<p>Halaman tambah komisi dokter digunakan untuk menambah komisi dokter baru. Untuk menambah komisi dokter, Anda perlu mengisi data - data seperti <b>Nama komisi, Poli, Layanan</b> (layanan bisa memilih semua layanan yang ada di poli yang sudah dipilih dengan cara centang <b>Semua layanan</b> atau bisa juga hanya memilih beberapa layanan saja)<b>, Dokter</b> (dokter bisa memilih semua dokter yang ada di poli yang sudah dipilih dengan cara centang <b>Semua dokter</b> atau bisa juga hanya memilih beberapa dokter saja)<b>,</b> dan <b>Komisi</b> (komisi bisa dalam bentuk nilai dengan pilih Rp/$ dan dalam bentuk persen dengan pilih % pada sebelah kanan pilihan). Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data komisi dokter.</p>

                	<br /><br />
                	<p>Di bagian kanan pada daftar komisi dokter, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Ubah</b> untuk mengubah data komisi dokter.</li>
                		<li><b>Hapus</b> untuk menghapus komisi dokter.</li>                		
                	</ol>
                    @else
                    <h1> Doctor's Commission </h1>
                	<p> The doctor's commission is used to regulate the amount of commission when the doctor performs the treatment in a particular service. So the commission will be sent into the doctor's commission report if the patient makes a payment for a service that has something to do with the doctor on the commission. The doctor's commission page lists the doctor's commission in the clinic and contains information about the name of the commission, the poly from the commission, the services of the commission, the doctor's commission, and the amount of commission. On the commission page there are menus that have different functions. Those menus will be detailed as follows: </p>
                	<ol>
                	<li> <b> Add a doctor's commission </b> to add a doctor in the clinic. </li>
                	</ol>

                	<h2> Add doctor commission </h2>
                	<p> The add page of the doctor's commission is used to add to the new doctor's commission. To add to the doctor's commission, you need to fill in the required data such as; <b> Name of commission, Poly, Service </b> (the service can select all services that have been selected by poly by checking <b> All services </b> or you can only choose a few service) <b>, Doctor </b> (doctors can choose all doctors in the poly who have already selected by tick off the field <b> All doctors </b> or you can only choose a few doctor only) <b>, </b> and <b> Commission </b> (commissions could be  formed in the form of values ​​by choosing Rp / $ and in percent form by selecting% on the right hand side of the option). After the required data has been filled in, you can click the <b> Save </b> button to save doctor commission data. </p>

                	<br /> <br />
                	<p> On the right side of the doctor's commission list, there is an action button that have different functions. Those functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Change </b> to change doctor's commission data. </li>
                	<li> <b> Delete </b> to delete the doctor's commission. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
