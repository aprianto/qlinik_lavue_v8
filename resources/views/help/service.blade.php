<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">     
                    @if($lang=='id')           	
                	<h1>Layanan</h1>
                	<p>Halaman layanan berisi daftar dari layanan yang ada di klinik dan berisi informasi tentang nama layanan, kategori, dan poli. Di halaman layanan terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Tambah layanan</b> untuk menambah layanan di klinik.</li>
                	</ol>

                	<h2>Tambah layanan</h2>
                	<p>Halaman tambah layanan digunakan untuk menambah layanan baru. Untuk menambah layanan, Anda perlu mengisi data - data seperti <b>Nama layanan, Kode layanan, Poli, Kategori layanan</b> (klik tombol <span class="fa fa-plus"></span> untuk menambah kategori layanan baru)<b>, Tarif</b> (tarif bisa ditambahkan beberapa tarif berdasarkan kategori pasien yang tersedia di klinik dengan cara klik <span class="fa fa-plus"></span> <b>Tambah tarif</b>)<b>, Biaya administrasi</b> (apabila biaya administrasi di centang, maka akan muncul kotak untuk memasukkan biaya administrasi dan biaya administrasi tersebut akan ditambahkan pada saat pembayaran)<b>,</b> dan <b>Wajib pilih pelaksana</b> (apabila di centang, maka pada saat pilih layanan di pembayaran, harus pilih dokter yang melayani di layanan tersebut). Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data layanan.</p>

                	<br /><br />
                	<p>Di bagian kanan pada daftar layanan, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Detail</b> untuk melihat detail data layanan.</li>
                		<li><b>Ubah</b> untuk mengubah data layanan.</li>
                		<li><b>Hapus</b> untuk menghapus layanan dari klinik.</li>                		
                	</ol>
                    @else
                    <h1> Service </h1>
                	<p> The list of the service page in the clinic and contains information about service names, categories, and poly. On the service page there are menus that have different functions. Those menus will be detailed as follows: </p>
                	<ol>
                	<li> <b> Add service </b> to add services at the clinic. </li>
                	</ol>

                	<h2> Add service </h2>
                	<p> The service’s add page is used to add new services. To add services, you need to fill in several required data such as; <b> Service name, Service code, Poly, Service category </b> (click the <span class = "fa fa-plus"> </span> button to add a service new category) <b>, Tarif </b> (rates can be added by several rates based on patient's categories  that available in the clinic by clicking <span class = "fa fa-plus"> </span> <b> Add rates</b >)<b>, Administration fee </b> (if the administration fee is checked, the field box of payment will appear to enter administrative fee and the administrative fee will be added at the time of the payment) <b>, </b> and <b> Required to select the executor </b> (if checked, When you are choosing a service in payment, you must select the doctor who does the service). After the required data is filled in, you can click the <b> Save </b> button to save service data. </p>

                	<br /> <br />
                	<p> On the right side of the service list, there is an action button that that have several functions. Those functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Details </b> to see details of data of service. </li>
                	<li> <b> Change </b> to change data of service. </li>
                	<li> <b> Delete </b> to delete services from the clinic. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

