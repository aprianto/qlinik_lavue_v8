<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">     
                    @if($lang=='id')              
                    <h1>Pembayaran</h1>
                    <p>Halaman pembayaran berisi daftar dari pembayaran yang ada di klinik dan berisi informasi tentang no pembayaran, nama pasien, total, dan status pembayaran. Di halaman pembayaran terdapat menu - menu sebagai berikut:</p>
                    <ol>
                        <li><b>Tambah kuitansi</b> untuk menambah pembayaran.</li>
                    </ol>

                    <h2>Tambah kuitansi</h2>
                    <p>Halaman tambah kuitansi digunakan untuk menambah pembayaran baru. Untuk menambah pembayaran, Anda perlu mengisi data - data seperti <b>No pembayaran</b> (otomatis terisi)<b>, Pasien</b> (klik tombol <span class="fa fa-plus"></span> untuk menambah pasien baru)<b>, Layanan, Obat, Catatan, Diskon, dan Total</b> yang dibayar <b>(Tunai/Kredit)</b>. Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data pembayaran.</p>

                    <ul>
                        <li><b>Tab Detail data pasien</b> berisi tentang detail data dari pasien yang dipilih.</li>
                        <li><b><span class="fa fa-plus"></span> Tambah layanan</b> untuk menambah layanan dan bisa menambah lebih dari 1 layanan. <br />Pada saat menambah layanan harus pilih <b>Layanan</b>, mengisi <b>Jumlah</b>, pilih <b>Pelaksana</b> (dokter yang melukan perawatan di layanan tersebut), mengisi <b>Diskon </b>(diskon bisa dalam bentuk nilai dengan pilih Rp/$ dan dalam bentuk persen dengan pilih % pada sebelah kanan pilihan), dan <b>Catatan</b>. kemudian tekan <b>Simpan</b> untuk menambahkan layanan ke daftar.</li>
                        <li><b><span class="fa fa-plus"></span> Tambah obat</b> untuk menambah obat dan bisa menambah lebih dari 1 obat. <br />Pada saat menambah obat harus pilih <b>Obat</b> dan <b>Jumlah</b>. kemudian tekan <b>Simpan</b> untuk menambahkan obat ke daftar.</li>
                        <li><b>Tab Catatan</b> berguna untuk menambah catatan pada pembayaran.</li>
                        <li><b>Diskon</b> berguna untuk menambah diskon total pada pembayaran (diskon total bisa dalam bentuk nilai dengan pilih Rp/$ dan dalam bentuk persen dengan pilih % pada sebelah kanan pilihan).</li>
                        <li><b>Tunai</b> berguna untuk memasukkan nilai pembayaran dalam bentuk tunai.</li>
                        <li><b>Kartu kredit</b> berguna memasukkan nilai pembayaran apabila dalam bentuk kartu kredit.</li>
                    </ul>

                    <br /><br />
                    <p>Di bagian kanan pada daftar pembayaran, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                    <ol>
                        <li><b>Kuitansi</b> untuk melanjutkan pembayaran apabila belum lunas dan apabila sudah lunas maka akan dalam bentuk tampilan detail pembayaran saja.</li>
                        <li><b>Detail</b> untuk melihat detail data pembayaran.</li>
                        <li><b>Cetak</b> untuk mencetak hasil dari pembayaran.</li>                
                    </ol>
                    @else
                    <h1> Payment </h1>
                	    <p> The payment page contains a list of payments in the clinic and contains information about payment number, patient name, total, and payment status. On the payment page there are several menus that have different functions. Those menus will be explained as follows: </p>
                	    <ol>
                	        <li> <b> Add receipt </b> to add payment. </li>
                	    </ol>

                	    <h2> Add receipt </h2>
                	    <p> The add receipt page is used to add the new payments. To add payment, you need to fill in several data that needed such as; <b> Payment numbers </b> (automatically filled in)<b>, Patient </b> (click the <span class = "fa fa-plus"> </span > to add new patients)<b>, Services, Medications, Notes, Discounts, and Total </b> paid <b> (Cash / Credit)</b>. After the required data has been filled, you can click the <b> Save </b> button to save payment data. </p>

                	    <ul>
                	        <li> <b> Detail tab for patient's data </​​b> contains detailed data from selected patients. </li>
                	        <li> <b> <span class = "fa fa-plus"> </span> Add service </b> to add services and can add more than one service. <br /> When you add a service, you must select <b> Service</b>, fill in <b> Amount</b>, choose <b> Implementer </b> (the doctor who applies treatment at the service), fill <b> Discounts </b> (discounts can be in the form of values ​​by choosing Rp / $ and in percentage form by selecting% on the right side of the selection), and <b> Notes </b>. then press <b> Save </b> to add services to the list. </li>
                	        <li> <b> <span class = "fa fa-plus"> </span> Add medicines </b> to add medicine and can add more than one medicine. <br /> When you add the medicine, you should select <b> Medication </b> and <b> Amount</b>. then press <b> Save </b> to add medicine to the list. </li>
                	        <li> <b> Note Tab </b> is useful for user when he/she is adding notes to the payments. </li>
                	        <li> <b> Discount </b> is useful for user when he/she is adding total discounts on payments (total discounts can be in the form of values ​​by choosing Rp / $ and in percentage form by selecting % on the right hand side of the selection). </li>
                	        <li> <b> Cash </b> is useful for inputs payment values ​​in cash. </li>
                	        <li> <b> Credit cards </b> are useful for entering payment values ​​if in the form of a credit card. </li>
                	    </ul>

                	    <br /> <br />
                	    <p> On the right side of the payment list, there is an action button that has functions for several points. Those points are listed as follows: </p>
                	    <ol>
                	        <li> <b> Receipt </b> to continue payment if it has not paid off and if it has been paid off it will be in the form of displaying payment details only. </li>
                	        <li> <b> Details </b> to see payment data details. </li>
                	        <li> <b> Print </b> to print results from payments. </li>
                	    </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
