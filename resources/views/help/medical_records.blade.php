<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">         
                    @if($lang=='id')       	
                	<h1>Rekam medis</h1>
                	<p>Halaman rekam medis berisi daftar dari rekam medis yang ada di klinik dan berisi informasi tentang pasien, dokter, dan tanggal rekam medis. Di halaman rekam medis terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Tambah rekam medis</b> untuk menambah rekam medis.</li>
                		<li><b>Tanda - tanda vital</b> untuk mengisi tanda - tanda vital dari pasien untuk keperluan rekam medis.</li>
                	</ol>

                	<h2>Tambah rekam medis</h2>
                	<p>Halaman tambah rekam medis digunakan untuk menambah rekam medis baru. Untuk menambah rekam medis, Anda perlu mengisi data - data seperti <b>Poli, Dokter, Pasien</b> (klik tombol <span class="fa fa-plus"></span> untuk menambah pasien baru)<b>, Diagnosa, Layanan, Obat, dan Lampiran</b>. Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data rekam medis.</p>

                    <ul>
                        <li><b>Tab Detail data pasien</b> berisi tentang detail data dari pasien yang dipilih.</li>
                        <li><b>Tab Histori Pasien</b> berisi daftar dari histori rekam medis dari pasien yang pernah dilakukan oleh pasien tersebut. klik tombol <b>Detail</b> untuk melihat detail dari histori rekam medis dan untuk melihat semua histori rekam medis dari pasien tersebut, bisa klik <b>Lihat semua histori</b>, maka akan muncul semua data rekam medis dari pasien tersebut.</li>
                        <li><b>Tab Tanda - tanda vital</b> berisi tanda - tanda vital dari pasien yang pernah diisi sebelumnya, bisa dari hari - hari sebelumnya atau hari ini. Apabila hari ini tanda - tanda vital dari pasien tersebut belum diisi, maka tanda - tanda vital pasien tersebut bisa diisi di kotak pada bagian setiap dari tanda - tanda vital.</li>
                        <li><b>Tab Diagnosa</b> berguna untuk mengisi <b>ICD</b> (bisa pilih 1 atau lebih ICD yang telah disediakan sesuai dengan standar ICD) sesuai dengan gejala - gejala pada pasien. <b>Anamnese</b> berisi keterangan Anamnese dari hasil rekam medis, dan <b>Diagnosa</b> dari hasil rekam medis.</li>
                        <li><b>Tab Layanan/tindakan</b> untuk menambah layanan/tindakan dengan klik <b><span class="fa fa-plus"></span> Tambah layanan/tindakan</b> dan bisa menambah lebih dari 1 layanan/tindakan. <br />Pada saat menambah layanan/tindakan harus pilih <b>Layanan/tindakan</b> dan mengisi <b>Jumlah</b> kemudian tekan <b>Tambah</b> untuk menambahkan layanan/tindakan ke daftar.</li>
                        <li><b>Tab Obat</b> berguna untuk menambah obat dengan klik <b><span class="fa fa-plus"></span> Tambah obat</b> dan bisa menambah lebih dari 1 obat. <br />Pada saat menambah obat harus pilih <b>Obat,</b> mengisi <b>Dosis,</b> dan <b>Jumlah</b> kemudian tekan <b>Tambah</b> untuk menambahkan obat ke daftar.</li>
                        <li><b>Tab Lampiran</b> berguna untuk menambah lampiran dengan klik <b><span class="fa fa-plus"></span> Tambah berkas</b> dan bisa menambah lebih dari 1 lampiran.</li>
                    </ul>

                	<br /><br />
                	<p>Di bagian kanan pada daftar rekam medis, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Detail</b> untuk melihat detail data rekam medis.</li>
                		<li><b>Cetak</b> untuk mencetak hasil dari rekam medis.</li>           		
                	</ol>
                    @else
                    <h1> Medical record </h1>
                	<p> The medical record page contains a list of medical records in the clinic and information about patients, doctors, and dates of medical records. On the medical record page there are several menus that have different functions. Those menus will be detailed as follows: </p>
                	<ol>
                	<li> <b> Add medical records </b> to add medical records. </li>
                	<li> <b> Vital signs </b> to fill vital signs from a patient for medical records. </li>
                	</ol>

                	<h2> Add medical record </h2>
                	<p> The medical record added page is used to add new medical records. To add medical records, you need to fill in data such as <b> Poly, Doctor, Patient </b> (click the <span class = "fa fa-plus"> </span> button to add new patients) <b> , Diagnosis, Service, Medication, and Attachments </b>. After the required data has been filled, you can click the <b> Save </b> button to save the medical record data. </p>

                	    <ul>
                	        <li> <b> Detail tab for patient's data </​​b> contains detailed data from the selected patients. </li>
                	        <li> <b> Patient History Tab </b> contains a list of the patient's medical record history that has been filled by the patient. click the <b> Details </b> button to see the details of the medical record history and to see all of the patient's medical records history, click <b> See all history </b>, then all of medical records data from the patient will be appeared. this. </li>
                	        <li> <b> Tab of Vital signs </b> contains vital signs from patients that have been filled before, can be from previous or today days. If today the vital signs of the patient have not been filled, the vital signs of the patient can be filled at the box from each part of the vital signs section. </li>
                	        <li> <b> Diagnosis Tab </b> is useful for filling <b> ICD </b> (can select 1 or more ICDs provided based ICD standard regulation) according to the symptoms in the patient. <b> Anamnese </b> contains anamnese information from the results of the medical record, and <b> Diagnosis </b> from the results of the medical record. </li>
                	        <li> <b> Service / action tab </b> to add services / actions by clicking <b> <span class = "fa fa-plus"> </span> Add services / actions </b> and can add more than 1 service / action. <br /> When adding services / actions must select <b> Services / actions </b> and fill in <b> Amount </b> then press <b> Add </b> to add services / actions to the list. </li>
                	        <li> <b> Medication Tab </b> is useful for adding drugs by clicks the button <b> <span class = "fa fa-plus"> </span> Add medicine </b> and can add more than one drug. <br /> When you adding drugs, you should select <b> Medication, </b> fill in <b> Dosage, </b> and <b> Amount </b> then press <b> Add </b> to add medicine to list. </li>
                	        <li> <b> Attachment Tab </b> is useful for adding attachments by clicking <b> <span class = "fa fa-plus"> </span> Add file </b> and you can add more than 1 attachment. </li>
                	    </ul>

                	<br /> <br />
                	<p> On the right side of the medical record list, there is an action button that has several functions. Those functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Details </b> to see detailed medical record data. </li>
                	<li> <b> Print </b> to print results from medical records. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
