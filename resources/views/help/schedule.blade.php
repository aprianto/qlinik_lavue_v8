<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>                    
                    <h3 class="right">
                        {{ trans('messages.information') }}
                    </h3>
                </div>
                <div class="doc">     
                    @if($lang=='id')           	
                	<h1>Rawat jalan</h1>
                	<p>Halaman rawat jalan berisi daftar dari rawat jalan yang ada di klinik dan berisi informasi tentang pasien, dokter, jadwal rawat jalan, no urut, dan status. Rawat jalan akan dikelompokan berdasarkan waktu dan dokter sehingga di atas daftar rawat jalan yang dikelompokan akan muncul informasi waktu, total pasien, dan total dokter. Di sebelah kanan daftar rawat jalan terdapat informasi total rawat jalan yang ada di daftar rawat jalan. Rawat jalan ketika pertama kali dibuka otomatis menampilkan rawat jalan hari ini. Di halaman rawat jalan terdapat menu - menu sebagai berikut:</p>
                	<ol>
                		<li><b>Pendaftaran rawat jalan</b> untuk melakukan pendaftaran rawat jalan.</li>
                	</ol>

                	<h2>Pendaftaran rawat jalan</h2>
                	<p>Halaman pendaftaran rawat jalan digunakan untuk menambah rawat jalan baru. Untuk menambah rawat jalan, Anda perlu mengisi data - data seperti <b>Jenis</b> (hanya bisa rawat jalan karena fitur rawat inap masih belum tersedia)<b>, pasien, poli, dokter, jam praktek dokter</b> (apabila jam praktek dokter menyediakan <b>Janji</b>, maka pasien bisa memilih dengan janji atau tidak dengan cara centang <b>Dengan perjanjian</b> apabila ingin dengan janji dan kemudian akan muncul kotak untuk memasukkan waktu perjanjian, apabila dengan janji, maka dokter perlu untuk mengkonfirmasi jadwal tersebut terlebih dahulu tetapi apabila tidak dengan janji maka jadwal tersebut akan otomatis mendapat nomor antrian)<b>,</b> dan <b>tanggal</b> (pilihan tanggal yang tersedia hanya mulai tanggal hari ini dan hari sesuai dengan hari praktik dokter yang dipilih). Setelah data yang diperlukan telah diisi, Anda bisa klik tombol <b>Simpan</b> untuk menyimpan data rawat jalan.</p>

                	<br /><br />
                	<p>Di bagian kanan pada daftar rawat jalan, terdapat tombol aksi yang berfungsi sebagai berikut:</p>
                	<ol>
                		<li><b>Konfirmasi</b> untuk melakukan konfirmasi pada rawat jalan tersebut.</li>
                		<li><b>Batalkan</b> untuk melakukan pembatalan pada rawat jalan tersebut.</li>
                		<li><b>Rekam medis</b> untuk melakukan rekam medis dari jadwal rawat jalan tersebut.</li>
                		<li><b>Detail</b> untuk melihat detail data rawat jalan.</li>
                		<li><b>Ubah</b> untuk mengubah data rawat jalan.</li>
                		<li><b>Hapus</b> untuk menghapus rawat jalan.</li>                		
                	</ol>
                    @else
                    <h1> Outpatient </h1>
                	<p> Outpatient page contains a list of outpatient service in the clinic and information about; patients, doctors, outpatient schedules, sequence numbers, and status. Outpatient service type will be categorized based on the times and doctor so on the top of outpatient list that categorized will appear the information of  the time, total patient, and total doctor. To the right of the outpatient service list there is a total of outpatient service information on the outpatient service list. When the first opened of the outpatient service, it will display today's outpatient service automatically. On the outpatient page there are several menus to be used. Those menus will be detailed as follows: </p>
                	<ol>
                	<li> <b> Outpatient service registration </b> for outpatient registration. </li>
                	</ol>

                	<h2> Outpatient service registration </h2>
                	<p> The outpatient service registration page is used for adding a new outpatient care. To add an outpatient care, you need to fill in some required data such as; <b> Type </b> (can only be an outpatient because the inpatient feature is not available yet)<b>, patient, poly, doctor, doctor's practice hours </b> (if the doctor's practice hours provide <b> appointment</b>, then the patient can choose with an appointment or not by tick <b> By agreement </b> if you wanted an appointment and then there is a box that  will appear to input the time of appointment, if with an appointment, the doctor needs to confirm the schedule first, but if without the appointment, the schedule will get the queue number automatically)<b>, </b> and <b> date </b> (The date’s option that is provided it is only starts on today’s practice and the day that related with the selected doctor’s schedule). After the required data has been filled, you can click the <b> Save </b> button to save the outpatient service data. </p>
                	<br /> <br />
                	<p> On the right side of the outpatient list, there is an action button that has several functions. Those several functions will be detailed as follows: </p>
                	<ol>
                	<li> <b> Confirmation </b> to confirm the outpatient service. </li>
                	<li> <b> Cancel </b> to cancel the outpatient service. </li>
                	<li> <b> Medical records </b> to do medical records of the outpatient service schedule. </li>
                	<li> <b> Details </b> to see outpatient service data details. </li>
                	<li> <b> Change </b> to change outpatient service data. </li>
                	<li> <b> Delete </b> to delete outpatient service. </li>
                	</ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

