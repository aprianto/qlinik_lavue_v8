<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_clinic_payment') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">                
                <div class="modal-body">
                    <input type="hidden" name="name" />
                    <input type="hidden" name="limit_access" />
                    <input type="hidden" name="limit_doctor" />
                    <input type="hidden" name="limit_next_doctor" />
                    <div class="form-group" id="clinic-group">
                        <label class="control-label">{{ trans('messages.clinic') }} <span>*</span></label>
                        <div class="border-group">
                            <select name="clinic" id="clinic" class="form-control">
                            </select>                                
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="payment_type-group">
                        <label class="control-label">{{ trans('messages.payment_type') }} <span>*</span></label>
                        <div class="border-group">
                            <select name="payment_type" id="payment_type" class="form-control">
                            </select>                                
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="start_date-group">
                        <label class="control-label">{{ trans('messages.start_date') }} <span>*</span></label>
                        <div id="datepicker_start_date" class="input-group date">
                            <input class="form-control" type="text" placeholder="{{ trans('messages.start_date') }}" name="start_date" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <span class="help-block"></span>  
                    </div>
                    <div class="form-group" id="end_date-group">
                        <label class="control-label">{{ trans('messages.end_date') }} <span>*</span></label>
                        <div id="datepicker_end_date" class="input-group date">
                            <input class="form-control" type="text" placeholder="{{ trans('messages.end_date') }}" name="end_date" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="total-group">
                        <label class="control-label">{{ trans('messages.total_pay') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.total') }}" name="total" autocomplete="off" onkeydown="number(event)">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

$('#datepicker_start_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$('#datepicker_end_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

function listClinic(val) {
    loading_content("#form-add #clinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #clinic-group", "success");

                var item = data.data;

                $("#form-add select[name='clinic']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='clinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"\n a</option>");
                }

                $("#form-add select[name='clinic']").select2({
                    placeholder: "-- {{ trans('messages.select_clinic') }} --",
                    allowClear: true
                });

                if(val!="") {
                    $("#form-add select[name='clinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #clinic-group", "failed");

                $("#form-add #clinic-group #loading-content").click(function(){ listClinic(val); });
            }
        },
        error: function(){
            loading_content("#form-add #clinic-group", "failed");

            $("#form-add #clinic-group #loading-content").click(function(){ listClinic(val); });
        }
    })
}


function listPaymentType() {
    loading_content("#form-add #payment_type-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment/type",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #payment_type-group", "success");

                var item = data.data;

                $("#form-add select[name='payment_type']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='payment_type']").append("<option data-name='"+item[i].name+"' data-price='"+item[i].price+"' data-month='"+item[i].month+"' data-limit_access='"+item[i].limit_access+"' data-limit_doctor='"+item[i].limit_doctor+"' data-limit_next_doctor='"+item[i].limit_next_doctor+"' value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name='payment_type']").select2({
                    placeholder: "-- {{ trans('messages.select_payment_type') }} --",
                    allowClear: true
                });

                if(val!="") {
                    $("#form-add select[name='payment_type']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #payment_type-group", "failed");

                $("#form-add #payment_type-group #loading-content").click(function(){ listPaymentType(); });
            }
        },
        error: function(){
            loading_content("#form-add #payment_type-group", "failed");

            $("#form-add #payment_type-group #loading-content").click(function(){ listPaymentType(); });
        }
    })
}

$('#form-add select[name=payment_type]').on('change', function() {
    var name = $(this).find(':selected').data('name');
    var price = $(this).find(':selected').data('price');
    var month = $(this).find(':selected').data('month');
    var limit_access = $(this).find(':selected').data('limit_access');
    var limit_doctor = $(this).find(':selected').data('limit_doctor');
    var limit_next_doctor = $(this).find(':selected').data('limit_next_doctor');

    if(this.value=='') {
        $("#form-add input[name=name]").val("");
        $("#form-add input[name=total]").val("");
        $("#form-add input[name='start_date']").val("");
        $("#form-add input[name='end_date']").val("");

        $("#form-add input[name=limit_access]").val("");
        $("#form-add input[name=limit_doctor]").val("");
        $("#form-add input[name=limit_next_doctor]").val("");
    } else {
        $("#form-add input[name=name]").val(name);
        $("#form-add input[name=total]").val(price);

        var getDate = new Date();
        var getMonth = new Date(getDate).getMonth()+1;
        var dateNow = new Date(getDate).getFullYear()+'-'+getMonth+'-'+new Date(getDate).getDate();

        var getLastDate = addDays(new Date(), parseInt(month*30));
        var getLastMonth = new Date(getLastDate).getMonth()+1;
        var lastDate = new Date(getLastDate).getFullYear()+'-'+getLastMonth+'-'+new Date(getLastDate).getDate();

        $("#form-add input[name='start_date']").val(unconvertDate(dateNow));
        $("#form-add input[name='end_date']").val(unconvertDate(lastDate));

        $("#form-add input[name=limit_access]").val(limit_access);
        $("#form-add input[name=limit_doctor]").val(limit_doctor);
        $("#form-add input[name=limit_next_doctor]").val(limit_next_doctor);

    }
    
});

function resetAdd() {
    resetValidation('form-add #clinic','form-add #payment_type','form-add #start_date','form-add #end_date','form-add #total');
    $("#form-add select[name=clinic]").val("").trigger("change");
    $("#form-add select[name=payment_type]").val("").trigger("change");
    $("#form-add input[name=start_date]").val("");
    $("#form-add input[name=end_date]").val("");
    $("#form-add input[name=total]").val("");
}

$('#add').on('show.bs.modal', function (e) {
    //listClinic('');
    listPaymentType();
    resetAdd();
});

$("#clinic-group .border-group").click(function() {
    resetValidation('clinic');
});

$("#payment_type-group .border-group").click(function() {
    resetValidation('payment_type');
});

$('input[name=start_date]').focus(function(event) {
    resetValidation('start_date');
});

$('input[name=end_date]').focus(function(event) {
    resetValidation('end_date');
});

$('#form-add input[name=total]').focus(function() {
    resetValidation('form-add #total');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #clinic','form-add #payment_type','form-add #start_date','form-add #end_date','form-add #total');

    var start_date = $('#form-add input[name=start_date]').val();
    var end_date = $('#form-add input[name=end_date]').val();

    var error = false;
    var scrollValidate = true;

    if(start_date!='' && end_date!='') {
        var split_start_date = start_date.split("/");
        check_start_date = Date.parse(split_start_date[2]+'/'+split_start_date[1]+'/'+split_start_date[0]);

        var split_end_date = end_date.split("/");
        check_end_date = Date.parse(split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0]);

        if(check_start_date>=check_end_date) {
            formValidate(scrollValidate, ['form-add #end_date','{{ trans("validation.invalid_start_end_date") }}', false]);
            error = true;
            if(scrollValidate==true) {
                scrollValidate = false;
            }
        } else {
            resetValidation('end_date');
        }
    }

    if(error==false) {

        $("#form-add button").attr("disabled", true);

        formData= new FormData();

        formData.append("validate", false);
        formData.append("id_clinic", $('select[name=clinic] option:selected').val())


        formData.append("name", $("#form-add input[name=name]").val());

        var limit_access = $("#form-add input[name=limit_access]").val();
        var type = "";
        if(limit_access==1) {
            type = "limit";
        } else {
            type = "full";
        }

        formData.append("type", type);
        formData.append("limit_access", limit_access);

        if(limit_access==1) {
            formData.append("limit_doctor", $("#form-add input[name=limit_doctor]").val());
            formData.append("limit_next_doctor", $("#form-add input[name=limit_next_doctor]").val());
        }


        formData.append("start_date", convertDate(start_date));
        formData.append("end_date", convertDate(end_date));
        formData.append("total", $("#form-add input[name=total]").val());
        formData.append("level", "admin");
        formData.append("user", "{{ $id_user }}");
        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic-payment",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                

                $("#form-add button").attr("disabled", false);
        
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                if(!data.error) {
                    if(!data.success) {
                        formValidate(true, ['form-add #clinic',data.errors.id_clinic, true], ['form-add #payment_type',data.errors.id_payment_type, true], ['form-add #start_date',data.errors.start_date, true], ['form-add #end_date',data.errors.end_date, true], ['form-add #total',data.errors.total, true]);
                    } else {

                        $("#add").modal("toggle");
                        
                        notif(true,"{{ trans('validation.success_save_clinic_payment') }}");

                        @if($list=='payment')
                        resetAdd();
                        search(numPage, "false", keySearch, keyFromDate, keyToDate);
                        @else
                        search(numPage,"false",keySearch, keyOrderBy, keyOrderType);
                        @endif
                
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        })
    }
});

function formatRepoClinic(repo) {
  if (repo.loading) return repo.text;
  var markup = "<span class='select2-text'>"+repo.name + "</span><br /><span class='select2-subtext'>"+nullToEmpty(repo.email)+"&nbsp;</span>";

  return markup;
}

function formatRepoSelectionClinic(repo) {
  return repo.name || repo.text;
}

$("#form-add select[name='clinic']").select2({
  ajax: {
    url: "{{ $api_url }}/{{ $lang }}/clinic",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term, 
        page: params.page,
        per_page: 10
      };
    },
    processResults: function (data, params) {
      params.page = params.page || 1;

      return {
        results: data.data,
        pagination: {
          more: (params.page * 30) < data.total
        }
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; },
  placeholder: "-- {{ trans('messages.select_clinic') }} --",
  minimumInputLength: 0,
  templateResult: formatRepoClinic,
  templateSelection: formatRepoSelectionClinic,
});


</script>