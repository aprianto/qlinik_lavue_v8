@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div>

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-dollar"></i>
                    <h3>
                        {{ trans('messages.clinic_payment') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_payment") }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.from_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.to_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_clinic') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2">{{ trans('messages.clinic') }}</th>                              
                                    <th>{{ trans('messages.name') }}</th>
                                    <th>{{ trans('messages.type') }}</th>
                                    <th>{{ trans('messages.limit') }}</th>
                                    <th>{{ trans('messages.total_pay') }}</th>  
                                    <th>{{ trans('messages.period') }}</th>                                 
                                    <th>{{ trans('messages.action') }}</th>
                                </tr>
                            </thead>
                            <tbody id="table">
                                
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="8" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="8" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$('#datepicker_from_date').datetimepicker({
    format: 'DD/MM/YYYY'
});
$('#datepicker_to_date').datetimepicker({
    format: 'DD/MM/YYYY'
});

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='from_date']").val(unconvertDate(from_dateRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_dateRepo));

var keySearch = searchRepo;
var numPage = pageRepo;
var keyFromDate = from_dateRepo;
var keyToDate = to_dateRepo;

function search(page, submit, q, from_date, to_date) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;

    repository(['search',q],['from_date',from_date],['to_date',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment",
        type: "GET",
        data: "q="+q+"&from_date="+from_date+"&to_date="+to_date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    var item = data.data;
                    var tr;
                    var no = from;

                    var type = "";
                    var limit = "";

                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(item[i].type=='full') {
                                type = "<span class='status-active'>"+item[i].type+"</span>";
                            } else {
                                type = "<span class='status-pending'>"+item[i].type+"</span>";
                            }

                            limit = "-";

                            if(item[i].setting.limit_access==1) {
                                limit = item[i].setting.limit_doctor+" {{ trans('messages.doctor') }}";
                            }

                            tr = $('<tr/>');

                            tr.append("<td class='photo'>"+thumbnail('sm','clinic','{{ $storage }}',item[i].clinic.logo,item[i].clinic.name)+"</td>");   
                            tr.append("<td><a href='#/{{ $lang }}/clinic/"+item[i].clinic.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].clinic.name + "</h1></a><span class='type'>" + nullToEmpty(item[i].clinic.email) + "</span><br /><span class='text'>" + nullToEmpty(item[i].clinic.phone) + "</span></td>");                                                
                            tr.append("<td><span class='text'>" + item[i].name + "</span></td>");
                            tr.append("<td>"+type+"</td>");
                            tr.append("<td>"+limit+"</td>");
                            tr.append("<td><span class='money'>" + formatCurrency('{{ $lang }}','Rp',item[i].total) + "</span></td>");
                            tr.append("<td><span class='text'>" + getFormatDate(item[i].start_date) + " - " + getFormatDate(item[i].end_date) + "</span></td>");
                            tr.append("<td>"
                                +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><span class='fa fa-trash-o'></span> {{ trans('messages.delete') }}</a>"
                                +"</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='8'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, from_date, to_date);

            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date); });
        }
    })
}


$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false",searchRepo, from_dateRepo, to_dateRepo);


$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic-payment/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch, keyFromDate, keyToDate);
                    notif(true,"{{ trans('validation.success_delete_clinic_payment') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

</script>

@include('payment.create', ['list' => 'payment'])

@include('_partial.confirm_delele')