@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                    <h3>
                        {{ trans('messages.medicine_outgoing') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.list_medicine') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-incoming" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-masuk.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_incoming") }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-outgoing" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['page','1']);" class="active">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-keluar.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_outgoing") }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-3">
                                <div class="form-group content-column" id="type-group">
                                    <div class="border-group">
                                        <select class="form-control" name="type">
                                            <option value="all">Semua Obat</option>
                                            <option value="dpho">Obat DPHO</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group column-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_medicine_outgoing') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-md">{{ trans('messages.batch_no') }}</th>
                                <th class="hide-lg">{{ trans('messages.exp_date') }}</th>
                                <th class="hide-md">{{ trans('messages.quantity') }}</th>
                                <th class="hide-md">{{ trans('messages.unit') }}</th>
                                <th class="hide-lg">{{ trans('messages.date') }}</th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>
$("#form-search select[name='type']").select2({
    placeholder: "Silahkan Pilih"
});

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day})
    });
});

$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day})
        
    });
});

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='from_date']").val(unconvertDate(from_dateRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_dateRepo));

var keySearch = searchRepo;
var keyFromDate = from_dateRepo;
var keyToDate = to_dateRepo;
var numPage = pageRepo;

function search(page, submit, q, from_date, to_date) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    var type = $("#form-search select[name=type] option:selected").val();
    if(type == 'dpho') {
        type = true;
    }else{
        type = '';
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;

    repository(['search',q],['from_date',from_date],['to_date',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-outgoing",
        type: "GET",
        data: "q="+q+"&is_dpho="+type+"&page="+page+"&per_page=10&from_date="+from_date+"&to_date="+to_date+"",
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");

            if ( !data.error ) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        tr = $('<tr/>');                        
                        tr.append("<td>" + "<span class='title-list-table'>" + item[i].medicine.name + "</span>" + "<p class='small-text-list'>" + item[i].medicine.code + " "+(item[i].medicine.kode_dpho != null ? "/ "+ item[i].medicine.kode_dpho : '')+"</p>" +
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>No. batch : " + item[i].batch_no + "</span>"+
                                '</div>'+
                                '<div class="sub show-md">'+
                                    "<span class='general-rs'>{{ trans('messages.exp_date') }}: " + getFormatDate(item[i].exp_date) + "</span>"+
                                '</div>'+
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>{{ trans('messages.quantity') }}: " + item[i].quantity + "</span>"+
                                '</div>'+
                                '<div class="sub show-md">'+
                                    "<span class='general-rs'>{{ trans('messages.date') }}: " + getFormatDate(item[i].date) + "</span>"+
                                '</div>'+
                                "</td>");
                        tr.append("<td class='hide-md'>" + item[i].batch_no + "</td>");
                        tr.append("<td class='hide-lg'>" + getFormatDate(item[i].exp_date) + "</td>");
                        tr.append("<td class='hide-md'>" + item[i].quantity + "</td>");
                        tr.append("<td class='hide-md'>" + item[i].unit.name + "</td>");
                        tr.append("<td class='hide-lg'>" + getFormatDate(item[i].date) + "</td>");

                        $("#table").append(tr);
                        no++;
                    }
                } else {

                    $(".pagination-sec").addClass("hide");
                        
                    if(page==1 && q=='' && from_date=='' && to_date=='') {
                        $("#table").append("<tr><td align='center' colspan='12'>{{ trans('messages.empty_medicine_outgoing') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='12'>{{ trans('messages.no_result') }}</td></tr>");
                    }   
                }
                pages(page, total, per_page, current_page, last_page, from, to, q, from_date, to_date);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date); });
        }
    })
}

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val();
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", $("#form-search input[name=q]").val(), convertDate(from_date), convertDate(to_date));
}

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
        }, 2000);
    }
});

$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false",searchRepo,keyFromDate, keyToDate);


</script>