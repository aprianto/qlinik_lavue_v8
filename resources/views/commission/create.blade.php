@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_commission') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form id="form-add" role="form" class="sec">                        
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.commission_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_commission_name') }}" name="name">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="polyclinic-group">
                            <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                            <div class="border-group">
                                <select id="polyclinic" name="polyclinic"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                        
                        <div class="form-group next-group" id="service-group">
                            <label class="control-label">{{ trans('messages.service') }} <span>*</span></label>                            
                            <div class="border-group">
                                <select id="service" name="service[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="all_service"><span></span> {{ trans('messages.all_service') }}</label>
                            </div>
                        </div>
                        <div class="form-group next-group" id="doctor-group">
                            <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>                            
                            <div class="border-group">
                                <select id="doctor" name="doctor[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="all_doctor"><span></span> {{ trans('messages.all_doctor') }}</label>
                            </div>
                        </div>
                        <h3 class="group-label">{{ trans('messages.commission') }}</h3> 
                        <div class="form-group next-group">
                            <div class="sf-radio"> 
                                <label><input type="radio" value="1" name="commission_type"><span></span> {{ trans('messages.nominal_symbol') }}</label>
                                <label><input type="radio" value="0" name="commission_type"><span></span> {{ trans('messages.percentage_symbol') }}</label>
                            </div>
                        </div>
                        <div class="form-group" id="commission-group">
                            <label class="control-label">{{ trans('messages.commission') }} <span>*</span></label>                            
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_commission') }}" name="commission" onkeydown="number(event)" autocomplete="off" disabled>
                            <!--<div class="switch switch-right">
                                <input type="checkbox" name="commission_type" class="hide" checked="true" />
                                <div id="commission_number" class="active" onclick="switchButton('form-add #commission_percent', 'form-add #commission_number','form-add input[name=commission_type]',true)">{{ trans('messages.$') }}</div>
                                <div id="commission_percent" class="non-active" onclick="switchButton('form-add #commission_number', 'form-add #commission_percent','form-add input[name=commission_type]',false)">{{ trans('messages.%') }}</div>
                            </div>-->
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/commission" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


var checked;
var valid = false;
$("#form-add input[name='commission_type']").click(function(){
    checked = $(this).val()
    $("input[name=commission]").prop('disabled', false)
});
$("#form-add input[name='commission']").keyup(function(e){
    formValidate(false, ['commission',""]);
    if(checked == 1) {
        if(event.which >= 37 && event.which <= 40) return
        $(this).val(function(index, value) {
            return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
        });
        valid = true;
    }else if(checked == 0 && e.target.value > 100) {
        $(this).val(function(index, value){
            return value.replace(/[^0-9]/g, '');
        });
        valid = false;
        formValidate(true, [ 'commission',"Persentase tidak boleh lebih dari 100%", true]);
    }else if(checked == 0 && e.target.value < 100) {
        valid = true;
    }
});

$("#form-add #polyclinic-group #polyclinic").select2({
    placeholder: "{{ trans('messages.select_polyclinic') }}"
});

$("#form-add #service-group #service").select2({
    placeholder: "{{ trans('messages.select_service') }}"
});

$("#form-add #doctor-group #doctor").select2({
    placeholder: "{{ trans('messages.select_doctor') }}"
});

function listPoly(val) {
    loading_content("#form-add #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-add #polyclinic-group", "success");              

                $("#form-add #polyclinic-group .border-group select").html("<option value=''></option>");

                item = data.data;

                for (var i = 0; i < item.length; i++) {
                    $("#form-add #polyclinic-group #polyclinic").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add #polyclinic-group #polyclinic").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });
                

            } else {
                loading_content("#form-add #polyclinic-group", "failed");

                $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-add #polyclinic-group", "failed");

            $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listService(val, polyclinic) {
    resetValidation('form-add #service');
    loading_content("#form-add #service-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/service?id_polyclinic="+polyclinic+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-add #service-group", "success");              

                $("#form-add #service-group .border-group select").html("");
                $('#form-add input[name=all_service]').prop('checked',false);
                $("#form-add input[name=all_service]").prop("disabled", false);
                $("#form-add #service-group .border-group select").prop("disabled", false); 

                if(polyclinic=="") {                    
                    $("#form-add input[name=all_service]").prop("disabled", true);
                    $("#form-add #service-group .border-group select").prop("disabled", true);
                    $("#form-add #service-group .border-group select").val("").trigger("change");
                    $("#form-add #service-group #service").select2({
                        placeholder: "{{ trans('messages.select_service') }}"
                    });
                } else {                    
                    item = data.data;

                    if(item.length>0) {
                        for (var i = 0; i < item.length; i++) {
                            if(val.length>0) {
                                var selected = "";
                                for (var j = 0; j < val.length; j++) {
                                    if(item[i].id==val[j]) {
                                        selected = "selected='selected'";
                                    }

                                }
                            }

                            $("#form-add #service-group #service").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                        }

                        $("#form-add #service-group #service").select2({
                            placeholder: "{{ trans('messages.select_service') }}",
                            allowClear: true
                        });                        

                    } else {
                        $("#form-add input[name=all_service]").prop("disabled", true);
                        $("#form-add #service-group .border-group select").prop("disabled", true);
                        $("#form-add #service-group .border-group select").val("").trigger("change");
                        $("#form-add #service-group .border-group select").select2({
                            placeholder: "{{ trans('messages.no_service') }}"
                        });
                    }
                }
                

            } else {
                loading_content("#form-add #service-group", "failed");

                $("#form-add #service-group #loading-content").click(function(){ listService(val, polyclinic); });
            }
        },
        error: function(){
            loading_content("#form-add #service-group", "failed");

            $("#form-add #service-group #loading-content").click(function(){ listService(val, polyclinic); });
        }
    })
}

function listDoctor(val, polyclinic) {
    resetValidation('form-add #doctor');
    loading_content("#form-add #doctor-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+polyclinic+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-add #doctor-group", "success");              

                $("#form-add #doctor-group .border-group select").html("");
                $('#form-add input[name=all_doctor]').prop('checked',false);
                $("#form-add input[name=all_doctor]").prop("disabled", false);
                $("#form-add #doctor-group .border-group select").prop("disabled", false); 

                if(polyclinic=="") {                    
                    $("#form-add input[name=all_doctor]").prop("disabled", true);
                    $("#form-add #doctor-group .border-group select").prop("disabled", true);
                    $("#form-add #doctor-group .border-group select").val("").trigger("change");
                    $("#form-add #doctor-group #service").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}"
                    });
                } else {                    
                    item = data.data;

                    if(item.length>0) {
                        for (var i = 0; i < item.length; i++) {
                            if(val.length>0) {
                                var selected = "";
                                for (var j = 0; j < val.length; j++) {
                                    if(item[i].id==val[j]) {
                                        selected = "selected='selected'";
                                    }

                                }
                            }

                            $("#form-add #doctor-group #doctor").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                        }

                        $("#form-add #doctor-group #doctor").select2({
                            placeholder: "{{ trans('messages.select_doctor') }}",
                            allowClear: true
                        });                        

                    } else {
                        $("#form-add input[name=all_doctor]").prop("disabled", true);
                        $("#form-add #doctor-group .border-group select").prop("disabled", true);
                        $("#form-add #doctor-group .border-group select").val("").trigger("change");
                        $("#form-add #doctor-group .border-group select").select2({
                            placeholder: "{{ trans('messages.no_doctor') }}"
                        });
                    }
                }
                

            } else {
                loading_content("#form-add #doctor-group", "failed");

                $("#form-add #doctor-group #loading-content").click(function(){ listDoctor(val, polyclinic); });
            }
        },
        error: function(){
            loading_content("#form-add #doctor-group", "failed");

            $("#form-add #doctor-group #loading-content").click(function(){ listDoctor(val, polyclinic); });
        }
    })
}

listPoly('');
listService('','');
listDoctor('','');

$('#form-add select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;
    
    listService('', polyclinic);
    listDoctor('', polyclinic);
});

$('#form-add input[name=all_service]').click(function() {
    var checked = this.checked;
    if(checked==true) {
        $("#form-add #service-group .border-group select").prop("disabled", true);  
        $("#form-add #service-group .border-group select > option").prop("selected","selected");
        $("#form-add #service-group .border-group select").trigger("change");
    } else {
        $("#form-add #service-group .border-group select").prop("disabled", false);  
        $("#form-add #service-group .border-group select").val("").trigger("change");
    }

    $("#form-add #service-group .border-group select").select2({
        placeholder: "{{ trans('messages.select_service') }}",
        allowClear: true
    });

    resetValidation('form-add #service');
});

$('#form-add input[name=all_doctor]').click(function() {
    var checked = this.checked;
    if(checked==true) {
        $("#form-add #doctor-group .border-group select").prop("disabled", true);  
        $("#form-add #doctor-group .border-group select > option").prop("selected","selected");
        $("#form-add #doctor-group .border-group select").trigger("change");
    } else {
        $("#form-add #doctor-group .border-group select").prop("disabled", false);  
        $("#form-add #doctor-group .border-group select").val("").trigger("change");
    }

    $("#form-add #doctor-group .border-group select").select2({
        placeholder: "{{ trans('messages.select_doctor') }}",
        allowClear: true
    });

    resetValidation('form-add #doctor');
});

$('#form-add input[name=name]').focus(function() { resetValidation('form-add #name'); });
$('#form-add #polyclinic-group .border-group').click(function() { resetValidation('form-add #polyclinic'); });
$('#form-add #service-group .border-group').click(function() { resetValidation('form-add #service'); });
$('#form-add #doctor-group .border-group').click(function() { resetValidation('form-add #doctor'); });
$('#form-add input[name=commission]').focus(function() { resetValidation('form-add #commission'); });

$("#form-add").submit(function(event) {
    resetValidation('form-add #name', 'form-add #polyclinic', 'form-add #service', 'form-add #doctor', 'form-add #commission');

    event.preventDefault();

    var commission_type = $("#form-add input[name='commission_type']:checked").val();
    var commission = $("#form-add input[name=commission]").val().replace(/\D/g, "");

    
    if(commission_type==1) {
        commission_type = "$";
    } else {
        commission_type = "%";
    }

    formData= new FormData();

    var validate = false;

    var scrollValidate = true;

    var service = $('#form-add select[name="service[]"]').val();
    if(service==null) {
        validate = true;
        formValidate(scrollValidate, ['form-add #service','{{ trans("validation.min_service") }}', true]);
        formData.append("service", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectService = new Array(); 
        $('#form-add select[name="service[]"] :selected').each(function (i, selected) {
            selectService[i] = $(selected).val();
            formData.append("service["+i+"]", selectService[i]);
        });
    }

    var doctor = $('#form-add select[name="doctor[]"]').val();
    if(doctor==null) {
        validate = true;
        formValidate(scrollValidate, ['form-add #doctor','{{ trans("validation.min_doctor") }}', true]);
        formData.append("doctor", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectDoctor = new Array(); 
        $('#form-add select[name="doctor[]"] :selected').each(function (i, selected) {
            selectDoctor[i] = $(selected).val();
            formData.append("doctor["+i+"]", selectDoctor[i]);
        });
    }
    if(commission == ""){
        formValidate(scrollValidate, ['form-add #commission','Masukkan jumlah komisi', true]);
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    }

    formData.append("validate", validate);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("id_polyclinic", $("#form-add select[name=polyclinic]").val());
    formData.append("commission_type", commission_type);
    formData.append("commission", commission);
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");


    if(valid) {
        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");
        $("#form-add button").attr("disabled", true);
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/commission",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                
                $("#form-add button").attr("disabled", false);
        
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                if(!data.error) {

                    if(!data.success) {
                        formValidate(scrollValidate, ['form-add #name',data.errors.name, true], ['form-add #polyclinic',data.errors.id_polyclinic, true],['form-add #commission',data.errors.commission, true]);
                    } else {

                        if(validate==false) {

                            $("#form-add input[name=name]").val("");
                            $("#form-add input[name=commission]").val("");

                            $("#form-add #polyclinic-group .border-group select").val("").trigger("change");

                            $('#form-add input[name=all_service]').prop('checked',false);
                            $("#form-add #service-group .border-group select").prop("disabled", false);  
                            $("#form-add #service-group .border-group select").val("").trigger("change");                      

                            $('#form-add input[name=all_doctor]').prop('checked',false);
                            $("#form-add #doctor-group .border-group select").prop("disabled", false);
                            $("#form-add #doctor-group .border-group select").val("").trigger("change");

                            resetValidation('form-add #name', 'form-add #polyclinic', 'form-add #service', 'form-add #doctor', 'form-add #commission');
                            //switchButton('form-add #commission_percent', 'form-add #commission_number','form-add input[name=commission_type]',true);
                            $('#form-add input[name="commission_type"]:checked').each(function(){
                                $(this).prop('checked', false); 
                            });
                            
                            notif(true,"{{ trans('validation.success_add_commission_doctor') }}");

                            if(setup=="commission") {
                                setup = "medicine";

                                $('#previous-step').removeClass('hide');
                                $('#step_1').addClass('hide');
                                $('#step_2').addClass('hide');
                                $('#step_3').addClass('hide');
                                $('#step_4').addClass('hide');
                                $('#step_5').addClass('done');
                                $('#step_5').addClass('hide');
                                $('#step_6').removeClass('hide');
                                $('#step_6').removeClass('disable');
                                $('#next-step').addClass('hide');

                                setup_clinic('medicine');

                                setTimeout(function(){
                                    loading('#/{{ $lang }}/commission');
                                    redirect('#/{{ $lang }}/commission');
                                }, 500);
                            } else {
                                setTimeout(function(){
                                    back();loading($(this).attr('href'))
                                }, 500);
                            }
                        }
                
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-add button").attr("disabled", false);
                
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
});

</script>