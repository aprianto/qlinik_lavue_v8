@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 


<div class="panel-content">
    <div class="row hide" id="tutorial">
        <div class="col-md-12">
            <div class="widget">
                <div class="welcome-bar">
                    <div id="text-tutorial">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/link/komisi-dokter.png') }}" />
                    <h3>
                        {{ trans('messages.commission_doctor') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/commission"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/commission/create" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_commission") }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group search-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_commission_name') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-lg">{{ trans('messages.polyclinic') }}</th>
                                <th class="hide-lg">{{ trans('messages.service') }}</th>
                                <th class="hide-lg">{{ trans('messages.doctor') }}</th>
                                <th class="hide-md">{{ trans('messages.nominal') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if(setup=="commission") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_commission_add_commission", ["link"=>"#/".$lang."/commission/create"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('medicine');");
} else if(setup=="medicine") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_commission_add_medicine", ["link"=>"#/".$lang."/medicine"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('');");

}

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/commission",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();


            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    $(".pagination-sec").removeClass("hide");                    

                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            tr = $('<tr/>');
                            
                            var service = "";
                            var itemService = item[i].service;
                            for (var j = 0; j < itemService.length; j++) {
                                service += "<span>"+itemService[j].name+"</span>";
                                if(j!=itemService.length-1) {
                                    service += ', ';
                                }
                            }

                            var doctor = "";
                            var itemDoctor = item[i].doctor;
                            for (var j = 0; j < itemDoctor.length; j++) {
                                doctor += "<a class='multi' href='#/{{ $lang }}/doctor/"+itemDoctor[j].id+"' onclick='loading($(this).attr(\"href\"))'><h1 class='multi'>"+itemDoctor[j].name+"</h1></a>";
                                if(j!=itemDoctor.length-1) {
                                    doctor += '<span class="multi">,&nbsp;</span>';
                                }
                            }

                            var commission = "";
                            if(item[i].commission_type=="$") {
                                commission = formatCurrency('{{ $lang }}','Rp',item[i].commission);
                            } else {
                                commission = item[i].commission+"%";
                            }

                            tr.append("<td>" + item[i].name + 
                                '<div class="sub show-md">'+
                                    '<span class="type">'+item[i].polyclinic.name+'</span>'+
                                    '<span class="type"><br /><br />{{ trans("messages.service") }}</span>'+
                                    '<span class="general">' + service + '</span>'+
                                    '<span class="type"><br /><br /><br />{{ trans("messages.doctor") }}</span>'+
                                    '<span class="general">' + doctor + '</span>'+
                                    '<span class="general show-sm"><br />{{ trans("messages.commission") }}: ' + commission + '</span>'+
                                '</div>'+
                                "</td>");
                            tr.append("<td class='hide-lg'>"+item[i].polyclinic.name+"</td>");
                            tr.append("<td class='hide-lg'>" + service + "</td>");
                            tr.append("<td class='hide-lg'>" + doctor + "</td>");

                            tr.append("<td class='hide-md'>" + commission + "</td>");

                            var action = "<a href='#/{{ $lang }}/commission/"+item[i].id+"/edit' class='btn-table btn-orange' onclick='loading($(this).attr(\"href\"))'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /> {{ trans('messages.edit') }}</a>"
                                +"<a data-toggle='modal' data-target='#confirmDelete' data-title=\"{{ trans('messages.delete') }}\" data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /> {{ trans('messages.delete') }}</a>";

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");

                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {

                    $(".pagination-sec").addClass("hide");

                    if(page==1 && q=='') {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.empty_commission') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); }); 
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/commission/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch);
                    notif(true,"{{ trans('validation.success_delete_commission_doctor') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$("#form-search").submit(function(event) {
    search(1, "true", $("#form-search input[name='q']").val());
});

let timer = "";
$("#form-search input[name='q']").on("input", function(e) {
    clearTimeout(timer)
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1, "true", $("#form-search input[name='q']").val());
        }, 2000);
    }
});

search(pageRepo, "false",searchRepo);
</script>


@include('_partial.confirm_delele')

