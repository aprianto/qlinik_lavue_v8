@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.edit_commission') }}
                    </h3>
                </div>
                <div class="cell" id="loading-edit">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload-edit">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide" id="section-edit">
                    <form id="form-edit" role="form" class="sec">                        
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.commission_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_commission_name') }}" name="name">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="polyclinic-group">
                            <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                            <div class="border-group">
                                <select id="polyclinic" name="polyclinic"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                        
                        <div class="form-group next-group" id="service-group">
                            <label class="control-label">{{ trans('messages.service') }} <span>*</span></label>                            
                            <div class="border-group">
                                <select id="service" name="service[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="all_service"><span></span> {{ trans('messages.all_service') }}</label>
                            </div>
                        </div>
                        <div class="form-group next-group" id="doctor-group">
                            <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>                            
                            <div class="border-group">
                                <select id="doctor" name="doctor[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="all_doctor"><span></span> {{ trans('messages.all_doctor') }}</label>
                            </div>
                        </div>
                        <h3 class="group-label">{{ trans('messages.commission') }}</h3> 
                        <div class="form-group next-group">
                            <div class="sf-radio"> 
                                <label><input type="radio" value="1" name="commission_type"><span></span> {{ trans('messages.nominal_symbol') }}</label>
                                <label><input type="radio" value="0" name="commission_type"><span></span> {{ trans('messages.percentage_symbol') }}</label>
                            </div>
                        </div>
                        <div class="form-group" id="commission-group">
                            <label class="control-label">{{ trans('messages.commission') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_commission') }}" name="commission" onkeydown="number(event)" autocomplete="off">
                            <!--<div class="switch switch-right">
                                <input type="checkbox" name="commission_type" class="hide" checked="true" />
                                <div id="commission_number" class="active" onclick="switchButton('form-edit #commission_percent', 'form-edit #commission_number','form-edit input[name=commission_type]',true)">{{ trans('messages.$') }}</div>
                                <div id="commission_percent" class="non-active" onclick="switchButton('form-edit #commission_number', 'form-edit #commission_percent','form-edit input[name=commission_type]',false)">{{ trans('messages.%') }}</div>
                            </div>-->
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/commission" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

$("#form-edit #polyclinic-group #polyclinic").select2({
    placeholder: "{{ trans('messages.select_polyclinic') }}"
});

$("#form-edit #service-group #service").select2({
    placeholder: "{{ trans('messages.select_service') }}"
});

$("#form-edit #doctor-group #doctor").select2({
    placeholder: "{{ trans('messages.select_doctor') }}"
});

function listPoly(val) {
    loading_content("#form-edit #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-edit #polyclinic-group", "success");              

                $("#form-edit #polyclinic-group .border-group select").html('');

                item = data.data;

                for (var i = 0; i < item.length; i++) {
                    $("#form-edit #polyclinic-group #polyclinic").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>")
                }

                $("#form-edit #polyclinic-group #polyclinic").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit #polyclinic-group #polyclinic").val(val).trigger("change");
                }
                

            } else {
                loading_content("#form-edit #polyclinic-group", "failed");

                $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #polyclinic-group", "failed");

            $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listService(val, polyclinic) {
    resetValidation('form-edit #service');
    loading_content("#form-edit #service-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/service?id_polyclinic="+polyclinic+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-edit #service-group", "success");              

                $("#form-edit #service-group .border-group select").html("");
                $('#form-edit input[name=all_service]').prop('checked',false);
                $("#form-edit input[name=all_service]").prop("disabled", false);
                $("#form-edit #service-group .border-group select").prop("disabled", false); 

                if(polyclinic=="") {                    
                    $("#form-edit input[name=all_service]").prop("disabled", true);
                    $("#form-edit #service-group .border-group select").prop("disabled", true);
                    $("#form-edit #service-group .border-group select").val("").trigger("change");
                    $("#form-edit #service-group #service").select2({
                        placeholder: "{{ trans('messages.select_service') }}"
                    });
                } else {                    
                    item = data.data;

                    if(item.length>0) {
                        for (var i = 0; i < item.length; i++) {
                            if(val.length>0) {
                                var selected = "";
                                for (var j = 0; j < val.length; j++) {
                                    if(item[i].id==val[j]) {
                                        selected = "selected='selected'";
                                    }

                                }
                            }

                            $("#form-edit #service-group #service").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                        }

                        $("#form-edit #service-group #service").select2({
                            placeholder: "{{ trans('messages.select_service') }}",
                            allowClear: true
                        });                        

                    } else {
                        $("#form-edit input[name=all_service]").prop("disabled", true);
                        $("#form-edit #service-group .border-group select").prop("disabled", true);
                        $("#form-edit #service-group .border-group select").val("").trigger("change");
                        $("#form-edit #service-group .border-group select").select2({
                            placeholder: "{{ trans('messages.no_service') }}"
                        });
                    }
                }
                

            } else {
                loading_content("#form-edit #service-group", "failed");

                $("#form-edit #service-group #loading-content").click(function(){ listService(val, polyclinic); });
            }
        },
        error: function(){
            loading_content("#form-edit #service-group", "failed");

            $("#form-edit #service-group #loading-content").click(function(){ listService(val, polyclinic); });
        }
    })
}

function listDoctor(val, polyclinic) {
    resetValidation('form-edit #doctor');
    loading_content("#form-edit #doctor-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+polyclinic+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-edit #doctor-group", "success");              

                $("#form-edit #doctor-group .border-group select").html("");
                $('#form-edit input[name=all_doctor]').prop('checked',false);
                $("#form-edit input[name=all_doctor]").prop("disabled", false);
                $("#form-edit #doctor-group .border-group select").prop("disabled", false); 

                if(polyclinic=="") {                    
                    $("#form-edit input[name=all_doctor]").prop("disabled", true);
                    $("#form-edit #doctor-group .border-group select").prop("disabled", true);
                    $("#form-edit #doctor-group .border-group select").val("").trigger("change");
                    $("#form-edit #doctor-group #service").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}"
                    });
                } else {                    
                    item = data.data;

                    if(item.length>0) {
                        for (var i = 0; i < item.length; i++) {
                            if(val.length>0) {
                                var selected = "";
                                for (var j = 0; j < val.length; j++) {
                                    if(item[i].id==val[j]) {
                                        selected = "selected='selected'";
                                    }

                                }
                            }

                            $("#form-edit #doctor-group #doctor").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                        }

                        $("#form-edit #doctor-group #doctor").select2({
                            placeholder: "{{ trans('messages.select_doctor') }}",
                            allowClear: true
                        });                        

                    } else {
                        $("#form-edit input[name=all_doctor]").prop("disabled", true);
                        $("#form-edit #doctor-group .border-group select").prop("disabled", true);
                        $("#form-edit #doctor-group .border-group select").val("").trigger("change");
                        $("#form-edit #doctor-group .border-group select").select2({
                            placeholder: "{{ trans('messages.no_doctor') }}"
                        });
                    }
                }
                

            } else {
                loading_content("#form-edit #doctor-group", "failed");

                $("#form-edit #doctor-group #loading-content").click(function(){ listDoctor(val, polyclinic); });
            }
        },
        error: function(){
            loading_content("#form-edit #doctor-group", "failed");

            $("#form-edit #doctor-group #loading-content").click(function(){ listDoctor(val, polyclinic); });
        }
    })
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#section-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/commission/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading-edit').addClass('hide');
                $('#section-edit').removeClass('hide');

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=name]").val(data.name);
                $("#form-edit input[name=commission]").val(data.commission);

                if(data.commission_type=="$") {
                    //switchButton('form-edit #commission_percent', 'form-edit #commission_number','form-edit input[name=commission_type]',true);
                    $("#form-edit input[name='commission_type'][value=1]").prop('checked', true);
                } else {
                    //switchButton('form-edit #commission_number', 'form-edit #commission_percent','form-edit input[name=commission_type]',false);
                    $("#form-edit input[name='commission_type'][value=0]").prop('checked', true);
                }

                listPoly(data.id_polyclinic);

                var getService = [];
                for(i=0; i<data.service.length; i++) {
                    getService.push(data.service[i].id);
                }
                listService(getService, data.id_polyclinic);

                var getDoctor = [];
                for(i=0; i<data.doctor.length; i++) {
                    getDoctor.push(data.doctor[i].id);
                }
                listDoctor(getDoctor, data.id_polyclinic);

                $('#form-edit select[name=polyclinic]').on('change', function() {
                    var polyclinic = this.value;

                    if(polyclinic==data.id_polyclinic) {
                        listService(getService, polyclinic);
                        listDoctor(getDoctor, polyclinic);
                    } else {
                        listService('', polyclinic);
                        listDoctor('', polyclinic);
                    }
                });

            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }
            

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    });
}

edit("{{ $id }}");

$('#form-edit input[name=all_service]').click(function() {
    var checked = this.checked;
    if(checked==true) {
        $("#form-edit #service-group .border-group select").prop("disabled", true);  
        $("#form-edit #service-group .border-group select > option").prop("selected","selected");
        $("#form-edit #service-group .border-group select").trigger("change");
    } else {
        $("#form-edit #service-group .border-group select").prop("disabled", false);  
        $("#form-edit #service-group .border-group select").val("").trigger("change");
    }

    $("#form-edit #service-group .border-group select").select2({
        placeholder: "{{ trans('messages.select_service') }}",
        allowClear: true
    });

    resetValidation('form-edit #service');
});

$('#form-edit input[name=all_doctor]').click(function() {
    var checked = this.checked;
    if(checked==true) {
        $("#form-edit #doctor-group .border-group select").prop("disabled", true);  
        $("#form-edit #doctor-group .border-group select > option").prop("selected","selected");
        $("#form-edit #doctor-group .border-group select").trigger("change");
    } else {
        $("#form-edit #doctor-group .border-group select").prop("disabled", false);  
        $("#form-edit #doctor-group .border-group select").val("").trigger("change");
    }

    $("#form-edit #doctor-group .border-group select").select2({
        placeholder: "{{ trans('messages.select_doctor') }}",
        allowClear: true
    });

    resetValidation('form-edit #doctor');
});

$('#form-edit input[name=name]').focus(function() { resetValidation('form-edit #name'); });
$('#form-edit #polyclinic-group .border-group').click(function() { resetValidation('form-edit #polyclinic'); });
$('#form-edit #service-group .border-group').click(function() { resetValidation('form-edit #service'); });
$('#form-edit #doctor-group .border-group').click(function() { resetValidation('form-edit #doctor'); });
$('#form-edit input[name=commission]').focus(function() { resetValidation('form-edit #commission'); });

$("#form-edit").submit(function(event) {
    resetValidation('form-edit #name', 'form-edit #polyclinic', 'form-edit #service', 'form-edit #doctor', 'form-edit #commission');

    event.preventDefault();
    $("#form-edit button").attr("disabled", true);

    //var commission_type = $('#form-edit input[name=commission_type]').prop('checked');
    var commission_type = $("#form-edit input[name='commission_type']:checked").val();

    if(commission_type==1) {
        commission_type = "$";
    } else {
        commission_type = "%";
    }

    formData= new FormData();

    var validate = false;

    var scrollValidate = true;

    var service = $('#form-edit select[name="service[]"]').val();
    if(service==null) {
        validate = true;
        formValidate(scrollValidate, ['form-edit #service','{{ trans("validation.min_service") }}', true]);
        formData.append("service", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectService = new Array(); 
        $('#form-edit select[name="service[]"] :selected').each(function (i, selected) {
            selectService[i] = $(selected).val();
            formData.append("service["+i+"]", selectService[i]);
        });
    }

    var doctor = $('#form-edit select[name="doctor[]"]').val();
    if(doctor==null) {
        validate = true;
        formValidate(scrollValidate, ['form-edit #doctor','{{ trans("validation.min_doctor") }}', true]);
        formData.append("doctor", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectDoctor = new Array(); 
        $('#form-edit select[name="doctor[]"] :selected').each(function (i, selected) {
            selectDoctor[i] = $(selected).val();
            formData.append("doctor["+i+"]", selectDoctor[i]);
        });
    }

    var id = $("#form-edit input[name=id]").val();

    formData.append("validate", validate);
    formData.append("_method", "PATCH");
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("id_polyclinic", $("#form-edit select[name=polyclinic]").val());
    formData.append("name", $("#form-edit input[name=name]").val());
    formData.append("commission_type", commission_type);
    formData.append("commission", $("#form-edit input[name=commission]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/commission/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-edit button").attr("disabled", false);
    
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            if(!data.success) {
                formValidate(scrollValidate,['form-edit #name',data.errors.name, true],['form-edit #polyclinic',data.errors.id_polyclinic, true],['form-edit #commission',data.errors.commission, true]);
            } else {

                if(validate==false) {                    
                    notif(true,"{{ trans('validation.success_edit_commission_doctor') }}");

                    setTimeout(function(){
                        back();loading($(this).attr('href'))
                    }, 500);
                }
        
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
    
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>