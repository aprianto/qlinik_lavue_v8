<?php if($errors->any()) { ?>
<ul class="alert alert-danger">
	<?php foreach($errors->all() as $error) { ?>
		<li><?=$error;?></li>
	<?php } ?>
</ul>
<?php } ?>