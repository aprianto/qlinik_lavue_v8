<div id="verification" class="verification">
    <div class="message-verification">
        <span id="icon-resend_email"></span>
        <span class="icon-verify" id="change-icon"><img src="{{ asset('assets/images/alertVerify.png') }}" id="img-icon"/> {{ trans('messages.email_verification') }}</span>
    </div>
    <span id="resend_email" class="email-verification-button">{{ trans('messages.resend') }}</span>
</div>
