<footer>
    <div class="footer-wrapper">
        <p>© 2020 {{ trans('messages.app_name') }}</p>
    </div>
</footer>
