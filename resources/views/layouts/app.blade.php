<!DOCTYPE html>
<html class="no-js" data-ng-app="medigoWebApp">
<head>

    <title>{{ trans('messages.app_name') }}</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ $storage }}/images/icons/favicon.png" />
    <link rel="shortcut icon" href="{{ $storage }}/images/icons/favicon.png" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:700,400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/colors.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/help.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/select2/dist/css/select2.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/select2/dist/css/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/wSelect.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}"/>

</head>
<body>

<script type="text/javascript">

if("{{$level == 'user'}}") {
    var setup = "{{ $setup }}";
    var help_setup = "{{ $help_setup }}";
    var email_verification = "{{ $email_verification }}";
    var help_price = "{{ $help_price }}";
    var help_schedule = "{{ $help_schedule }}";
    var help_action_schedule = "{{ $help_action_schedule }}";
}else if("{{$level =='doctor'}}") {
    var setup = null;
    var help_setup = "subscribe";
    var email_verification = true;
    var help_price = "";
    var help_schedule = 0;
    var help_action_schedule = 0;
}
</script>

<script src="{{ asset('assets/js/push.min.js') }}"></script>
<script src="{{ asset('assets/js/pusher.min.js') }}"></script>

<script type="text/javascript">

var pusher = new Pusher('{{ $pusher_key }}', {
  authEndpoint: '{{ $pusher_auth_url }}'
})

id_pusher = "{{ $id_user }}";


var channelVerification = pusher.subscribe('private-App.User.'+id_pusher);
channelVerification.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
  
  var getNum = $("#num-notification").html();

  var numNotif = 0;

  var text = "";
  if("{{ $lang }}"=="en") {
      text = data.data.message.en;
  } else {
      text = data.data.message.id;
  }
  

  try {
      $('input[name="id_notif[]"]').each( function() {
          numNotif++;
      });
  } catch(err) {}

  if(numNotif==0) {
      $("#section-notification").html("");
  }

  if(numNotif>=3) {
      $("#section-notification a:last").remove();
  }

  var num = parseInt(getNum)+1;
  $("#num-notification").removeClass('hide');
  $("#num-notification").html(num);

  var link = "";
  var type = "";
  var icon = '<i class="fa fa-question"></i>';
  var img = '{{ $storage }}/images/icons/icon-notif.png';
  var title = "";
  var titlePush = "{{ trans('messages.app_name') }}";

  if(data.data.type=="clinic_add") {
      icon = '<i class="fa fa-check-circle"></i>';
      type = "no-cursor";
  }

  if(data.data.type=="clinic_schedule") {
      title = '<h1>'+data.data.name+'</h1>';
      icon = thumbnail('sm','patient','{{ $storage }}',data.data.photo,'P');

      link = 'href="#/{{ $lang }}/schedule/'+data.data.id+'" onclick=\'loading($(this).attr("href"));\'';

      if(data.data.photo!=null) {
          img = '{{ $storage }}/images/patient/'+data.data.photo;
      }

      titlePush = data.data.name;
  }

  if(data.data.type=="clinic_doctor_confirm_schedule" || data.data.type=="clinic_doctor_cancel_schedule") {
      title = '<h1>'+data.data.name+'</h1>';
      icon = thumbnail('sm','doctor', '{{ $storage }}',data.data.photo,'D');

      link = 'href="#/{{ $lang }}/schedule/'+data.data.id+'" onclick=\'loading($(this).attr("href"));\'';

      if(data.data.photo!=null) {
          img = '{{ $storage }}/images/doctor/'+data.data.photo;
      }

      titlePush = data.data.name;
  }

  if(data.data.type=="broadcast") {
      title = '<h1>{{ trans("messages.notification") }}</h1>';
      icon = '<i class="fa fa-info"></i>';
      link = 'href="#/{{ $lang }}/notifications" onclick=\'loading($(this).attr("href"));\'';

      text = limitChar(text, 120);
  }

  

  $("#section-notification").prepend('<a '+link+' id="notif-'+data.id+'" title="" class="menu read '+type+'">'
  +'<input type="hidden" name="id_notif[]" value="'+data.id+'">'
  +'<input type="hidden" name="read_notif[]" value="read">'
  +'<input type="hidden" id="notifTime-'+data.id+'" value="'+data.data.time+'">'
  +'<div class="thumb">'+icon+'</div>'
  +'<div class="info">'
  +'<div class="text">'
  +''+title+''
  +''+text+''
  +'<p><i class="fa fa-bell"></i><span id="notifTimeAgo-'+data.id+'">'+getTimeAgo(data.data.time)+'</span></p>'
  +'</div>'
  +'</div>'
  +'</a>');

  id_notif.push(data.id);

  var newNotif = parseInt($("#new-notification").html());

  if(newNotif>=3) {
      newNotif = 3;
  } else {
      newNotif = newNotif+1;
  }

  $("#new-notification").html(newNotif);

  var url = "{{ url('/') }}";
  var lang = "{{ $lang }}";

  var text = text;
  text = text.replace(/(<([^>]+)>)/ig,'');

  Push.create(titlePush, {
      body: text,
      icon: img,
      timeout: 5000,
      onClick: function () {
          window.location=url+'/#/'+lang+'/notifications';
          this.close();
      }
  });
});

</script>

@include('_partial.flash_message')
@include('setting.repository')


<div id="loading-progressBar" class="hide">
    <div class="loading-progress"></div>
</div>

@if(!empty($id_user))
    @if($level=="admin" || ($level=="user" && $id_clinic!="" || ($level=="doctor")))
        <div data-ng-include="'{{ $lang }}/header'"></div>
        @yield('main')
    @else 
        @yield('main')
    @endif
@else
    @yield('main')
@endif

<script src="{{ asset('assets/js/jquery-2.1.3.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/moment-with-locales.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('assets/js/angular.min.js') }}"></script>
<script src="{{ asset('assets/js/angular-route.min.js') }}"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/common.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
<script src="{{ asset('assets/js/wSelect.min.js') }}"></script>
<script type="text/javascript">

function close_help_setup() {
  help_setup = 0;

  $('#step-progress').addClass('hide');
  $('header').removeClass('header-step');
  $('#main-content').removeClass('main-step');
  
  formData= new FormData();

  formData.append("validate", "false");
  formData.append("_method", "PATCH");
  formData.append("help_setup", "0");
  formData.append("user", "{{ $id_user }}");

  $.ajax({
      url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(data){

          if(!data.error) {

              if(!data.success) {
              } else {                
              }
          } else {
          }
      },
      error: function(){
      }
  })
}

function setup_clinic(type) {
    setup = type; 

    if(type=='') {
        close_help_setup();
    }

    formData= new FormData();

    formData.append("validate", "false");
    formData.append("_method", "PATCH");
    formData.append("setup", type);
    formData.append("user", "{{ $id_user }}");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {                
                if(!data.success) {
                } else {
                                   
                }
            } else {
            }
        },
        error: function(){
        }
    })
}

</script>


@include('report.report')

@include('library.analytics')

<script>


/*$.ajaxSetup({
    headers: {
        "Authorization": "Bearer {{ $api_token }}"
    }
});*/


</script>


@include('function.auth')

@if($user)
<script>
window.intercomSettings = {
    app_id: "xzxfxgwa",
    name: "{{ $user->name }}", // Full name
    email: "{{ $user->email }}", // Email address
    created_at: Math.round(new Date().getTime()/1000) // Signup date as a Unix timestamp
};
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xzxfxgwa';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
@else
<script>
window.intercomSettings = {
    app_id: "xzxfxgwa",
    name: "Klinik", // Full name
    email: "hello@medigo.id", // Email address
    created_at: Math.round(new Date().getTime()/1000) // Signup date as a Unix timestamp
};
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xzxfxgwa';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
@endif

</body>
</html>



