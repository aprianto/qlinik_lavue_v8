@include('layouts.lib')


<div class="top-bar">
  @if($level=="user" && isset($user->email_verification) && $user->email_verification == false)
    @include('layouts.verification')
  @endif
    <div class="header-top">
			<div class="logo app">
					<a href="#/" title="" class="text" onclick="loading($(this).attr('href'))">
						<img src="{{ $storage }}/images/icons/logo.png" />
					</a>
					<div class="menu-options"><span class="menu-action"><i></i></span></div>
					@if($level=="doctor")
						<div class="select-clinic">
							<div id="dLabel" class="dropdown-select" data-toggle="dropdown" aria-expanded="false"><span>{{$clinic->name}}</span><img src="{{asset('assets/images/Path.png')}}" class="arrow-select-clinic" /></div>
							<ul class="dropdown-menu" id="dropdown-menu-clinic" aria-labelledby="dLabel">
								<?php foreach ($clinics as $key => $value) { $no=$key+1; ?>
									@if($clinic->id != $value->id)
										<li onclick="changeClinic('{{ $value->id }}', '{{ $value->id_polyclinic }}', '{{$value->indonesian_name}}')">{{$value->name}}</li>
									@endif
									<?php } ?>
							</ul>
						</div>
					@endif
			</div>
			<div class="quick-links app">
					<ul>
						<li class="language-wrapper">
							<a title="" class="dropdown">
								@if($lang=="id")
									<img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-flag" /><span class="text">IDN</span> 
								@else
									<img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-flag" /><span class="text">EN</span>
								@endif
									<img src="{{ asset('assets/images/icons/nav/chevron-dm.png') }}" class="icon-arrow" />
							</a>
							<div class="dialouge" style="display: none;">
								<div class="menu-sec">
										<a class="menu menu-flag" href="language/id"><img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-link flag" /> <span>Indonesia</span></a>
								</div>
								<div class="menu-sec">
										<a class="menu menu-flag" href="language/en"><img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-link flag" /> <span>English</span></a>
								</div>
							</div>
						</li>
						@if($level=="user")
						<li id="notifications">
							<a title="">
								<img src="{{ asset('assets/images/icons/header/notification.png') }}" class="icon-menu" />
								<span id="num-notification" class="num-notification hide">0</span>
							</a>
							<div class="dialouge notifications" style="display: none;">
								<span class="header"><span id="new-notification">0</span> {{ trans('messages.new_notifications') }}</span>
								<div class="card" id="loading-notification">
										<span class="three-quarters">Loading&#8230;</span>
								</div>
								<div class="card hide" id="reload-notification">
										<span class="reload fa fa-refresh"></span>
								</div>
								<div id="section-notification" class="hide">
								</div>
								<a class="footer" href="#/{{ $lang }}/notifications" onclick="loading($(this).attr('href'))">{{ trans('messages.view_all_notifications') }}</a>
							</div>
						</li>
						@endif
						<li class="sidebar-responsive">
							<a title="" class="menu-sidebar-right"><img src="{{ asset('assets/images/icons/header/more.png') }}" class="icon-menu" /></a>
						</li>
						
						<!-- ======================= MENU PROFILE & SETTING DEKSTOP ============================ -->
						@if($level != 'doctor')
						<li class="menu-right-wrapper"><a title=""><img src="{{ asset('assets/images/icons/header/more.png') }}" class="icon-menu" /></a>
							<div class="dialouge">
								@if($level=="admin")
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/admin/setting" onclick="loading($(this).attr('href'))"><i class="fa fa-user"></i>&nbsp; <span>{{ trans('messages.account_setting') }}</span></a>
								</div>
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/setting" onclick="loading($(this).attr('href'))"><i class="fa fa-cogs"></i>&nbsp; <span>{{ trans('messages.application') }}</span></a>
								</div>
								<div class="menu-sec">
										<a class="menu" title="" href="admin/logout"><i class="fa fa-sign-out"></i>&nbsp; <span>{{ trans('messages.logout') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/clinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-klinik.png') }}" /> <span>{{ trans('messages.clinic') }}</span></a>
								</div>
								@if(isset($acl->clinic_polyclinic))
								@if($acl->clinic_polyclinic->read==true && $acl->clinic_polyclinic->update==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/clinic/polyclinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" /> <span>{{ trans('messages.polyclinic') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/clinic/polyclinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" /> <span>{{ trans('messages.polyclinic') }}</span></a>
								</div>
								@endif
								@endif
								@if(isset($acl->clinic_insurance))
								@if($acl->clinic_insurance->read==true && $acl->clinic_insurance->update==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/clinic/insurance" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-asuransi.png') }}" /> <span>{{ trans('messages.insurance') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/clinic/insurance" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-asuransi.png') }}" /> <span>{{ trans('messages.insurance') }}</span></a>
								</div>
								@endif
								@endif
								@if(isset($acl->user))
								@if($acl->user->read==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/user" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" /> <span>{{ trans('messages.user') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/user" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" /> <span>{{ trans('messages.user') }}</span></a>
								</div>
								@endif
								@endif
								@if(isset($acl->service_category))
								@if($acl->service_category->read==true && $acl->service_category->create==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/service/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-layanan.png') }}" /> <span>{{ trans('messages.service_category') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/service/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-layanan.png') }}" /> <span>{{ trans('messages.service_category') }}</span></a>
								</div>
								@endif
								@endif
								@if(isset($acl->patient_category))
								@if($acl->patient_category->read==true && $acl->patient_category->create==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/patient/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" /> <span>{{ trans('messages.patient_category') }}</span></a>
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/patient/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" /> <span>{{ trans('messages.patient_category') }}</span></a>
								</div>
								@endif
								@endif
								@if(isset($acl->service))
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/service" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" /> <span>{{ trans('messages.service') }}</span></a>
								</div>
								@endif
								@if(isset($acl->medicine))
								@if($acl->medicine->read==true && $acl->medicine->create==true)
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" /> <span>{{ trans('messages.inventory') }}</span></a> 
								</div>
								@else
								<div class="menu-sec">
										<a class="menu" title="" href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" /> <span>{{ trans('messages.inventory') }}</span></a> 
								</div>
								@endif
								@endif
								@endif
							</div>
						</li>
						@endif
						<!-- ======================= END MENU PROFILE & SETTING DEKSTOP ======================== -->

						@if($level=="user" || $level=="doctor")
						<li class="profile-wrapper">
							<a class="icon">
                                <?php echo thumbnail('sm', $level, $storage, $user->photo, $user->name); ?>
							</a>
							<div class="dialouge profile" style="display: none;">
								<div class="list">
									<div class="thumb">
										<div class="canvas">
                                            <?php echo thumbnail('md', $level, $storage, $user->photo, $user->name); ?>
										</div>
									</div>
									<div class="info">
										<h3>{{ $user->name }}</h3>
										<a class="button" href="#/{{ $lang }}/profile" onclick="loading($(this).attr('href'))">{{ trans("messages.view_profile") }}</a>
									</div>
								</div>
								<div class="list nav">
									@if($level == "doctor")
										<a href="doctor/logout?lang={{$lang}}" class="right button">{{ trans("messages.logout") }}</a>
									@else
									<a href="logout" class="right button">{{ trans("messages.logout") }}</a>          
									@endif
								</div>
							</div>
						</li>
						@endif
						<li class="hide">
							<a title="" class="sky-skin">
								<i class="fa fa-info">
									<span id="num-notification" class="num-notification hide">0</span>
								</i>
							</a>
							<div class="dialouge notification" style="display: none;">
								<span><span id="new-notification">0</span> {{ trans('messages.new_notifications') }}</span>
								<div class="card" id="loading-notification">
										<span class="three-quarters">Loading&#8230;</span>
								</div>
								<div id="section-notification" class="hide">
								</div>
								<a class="view-all" href="#/{{ $lang }}/notifications" onclick="loading($(this).attr('href'))">{{ trans('messages.view_all_notifications') }}</a>
							</div>
						</li>
					</ul>
			</div>
      @if($level=="user")
        <div class="step-progress hide" id="step-progress">
          <a class="arrow arrow-left" id="previous-step" onclick="step_progress('previous')"><span class="fa fa-chevron-left"></span></a>
          <h1 id="step_1" class="done hide"><span><i class="fa fa-check"></i></span> {{ trans("messages.add_clinic") }}</h1>
          <h1 id="step_2" class=""><span><i class="fa fa-check"></i></span> {{ trans("messages.add_doctor") }}. <a href="#/{{ $lang }}/doctor/create" onclick="loading($(this).attr('href'))">{{ trans("messages.click_here") }}</a></h1>
          <h1 id="step_3" class="disable hide"><span><i class="fa fa-check"></i></span> {{ trans("messages.add_doctor_schedule") }}. <a href="#/{{ $lang }}/doctor/schedule" onclick="loading($(this).attr('href'))">{{ trans("messages.click_here") }}</a></h1>
          <h1 id="step_4" class="disable hide"><span><i class="fa fa-check"></i></span> {{ trans("messages.add_service") }}. <a href="#/{{ $lang }}/service/create" onclick="loading($(this).attr('href'))">{{ trans("messages.click_here") }}</a></h1>
          <h1 id="step_5" class="disable hide"><span><i class="fa fa-check"></i></span> {{ trans("messages.add_commission_doctor") }}. <a href="#/{{ $lang }}/commission/create" onclick="loading($(this).attr('href'))">{{ trans("messages.click_here") }}</a></h1>
          <h1 id="step_6" class="disable hide"><span><i class="fa fa-check"></i></span> {{ trans("messages.add_medicine") }}. <a href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);">{{ trans("messages.click_here") }}</a></h1>
          <a class="arrow arrow-right" id="next-step" onclick="step_progress('next')"><span class="fa fa-chevron-right"></span></a>
          <a class="close-help" onclick="close_help_setup()"><span class="fa fa-times"></span></a>
      	</div>
      @endif
    </div>
</div>

<!-- =========================================== SIDEBAR ========================================= -->
<div class="bg-sidebar"></div>
<header class="side-header light-skin opened-menu @if($level=='user') @if($user->email_verification!=true) header-verification @endif @endif">
    <a class="admin-details" href="#/{{ $lang }}/clinic" onclick="loading($(this).attr('href'))"></a>
    <div class="menu-scroll">
        <div class="side-menus">
            <nav>
                <ul>
                    @if($level=='admin')
                    <li><a href="#/{{ $lang }}/clinic" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-hospital-o"></i> <span>{{ trans('messages.clinic') }}</span></a></li>
                    <li class="menu-item-has-children">
                        <a href="" title=""><i class="fa fa-dollar"></i> <span>{{ trans('messages.payment') }}</a>
                        <ul>
                            <li><a href="#/{{ $lang }}/payment/clinic" title="" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['page','1']);"><i class="fa fa-dollar"></i> <span>{{ trans('messages.clinic_payment') }}</span></a></li>
                            <li><a href="#/{{ $lang }}/payment/type" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-dollar"></i> <span>{{ trans('messages.payment_type') }}</span></a></li>
                        </ul>
                    </li>
                    <li><a href="#/{{ $lang }}/doctor" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-user-md"></i> <span>{{ trans('messages.doctor') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/patient" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-users"></i> <span>{{ trans('messages.patient') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/broadcast" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-bullhorn"></i> <span>{{ trans('messages.broadcast') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/polyclinic" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-wheelchair"></i> <span>{{ trans('messages.polyclinic') }}</span></a></li>
                    <li class="menu-item-has-children">
                        <a href="" title=""><i class="fa fa-ambulance"></i> <span>{{ trans('messages.service') }}</a>
                        <ul>                            
                            <li><a href="#/{{ $lang }}/service/category/default" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-file-text-o"></i> <span>{{ trans('messages.service_category') }}</span></a></li>
                            <li><a href="#/{{ $lang }}/service/default" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-file-text-o"></i> <span>{{ trans('messages.service') }}</span></a></li>
                        </ul>
                    </li>
                    <li><a href="#/{{ $lang }}/patient/category/default" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-users"></i> <span>{{ trans('messages.patient_category') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/medicine/default" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-medkit"></i> <span>{{ trans('messages.medicine') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/icd" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-stethoscope"></i> <span>{{ trans('messages.icd') }}</span></a></li>
                    <li><a href="#/{{ $lang }}/insurance" title="" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><i class="fa fa-certificate"></i> <span>{{ trans('messages.insurance') }}</span></a></li>
                    @else
                    <div class="collapse-select-clinic">
											<h5>{{ $clinic->name }}</h5>
											<img src="{{ asset('assets/images/Chevron-down.png') }}" class="@if($level != 'doctor') hide @endif" />
										</div>
                    <div class="wrapper-collapse-clinic">
                        @if($level=="doctor")
														<?php foreach ($clinics as $key => $value) { $no=$key+1; ?>
															@if($clinic->id != $value->id)
                              <li onclick="changeClinic('{{ $value->id }}', '{{ $value->id_polyclinic }}', '{{$value->indonesian_name}}')">{{$value->name}}</li>
															@endif
															<?php } ?>
                        @endif
                    </div>
                    <hr class="border-bottom-clinic" />
                    <li><a id="link-dashboard" href="#/{{ $lang }}/home" title="" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/sidebar/dashboard.png') }}" /><span>{{ trans('messages.dashboard') }}</span></a></li>
                    @if(isset($acl->doctor) && $level != 'doctor')
                    @if($acl->doctor->read==true && $acl->doctor->create==true)
                    <li><a id="link-doctor" href="#/{{ $lang }}/doctor" title="" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['date',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/dokter.png') }}" /><span>{{ trans('messages.doctor') }}</span></a></li>
                    @endif
                    @endif
                    @if(isset($acl->patient))
                    @if($acl->patient->read==true && $acl->patient->create==true)
                    <li><a id="patient-bar" href="#/{{ $lang }}/patient" title="" onclick="loading($(this).attr('href'));repository(['search',''],['birth_date',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" /><span>{{ trans('messages.patient') }}</span></a></li>
                    @endif
                    @if($acl->patient->read==true && $acl->patient->create==false)
                    <li><a href="#/{{ $lang }}/patient" title="" onclick="loading($(this).attr('href'));repository(['search',''],['birth_date',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" /><span>{{ trans('messages.patient') }}</span></a></li>
                    @endif
                    @endif
                    @if(isset($acl->schedule))
                    @if($acl->schedule->read==true)
                    <li><a id="schedule-bar" href="#/{{ $lang }}/schedule/sick" title="" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['doctor',''],['status',''],['from_date_now','now'],['to_date_now','now'],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/rawat-jalan.png') }}" /><span>Pendaftaran</span></a></li>
                    @endif
                    @endif

                    @if(Session::get('pcare'))
                    @if(isset($acl->groups))
                    @if($acl->groups->read==true && $acl->groups->create==true)
                    <li><a id="group-bar" href="#/{{ $lang }}/group/activities" title="" onclick="loading($(this).attr('href'));repository(['search',''],['from_date_now','now'],['to_date_now','now'],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/kelompok.png') }}" /><span>Kegiatan Kelompok</span></a></li>
                    @endif
                    @endif
                    @endif
                    @if(isset($acl->medical_records))
                    @if($acl->medical_records->read==true && $acl->medical_records->create==true) 
                    <li><a id="medical-records-bar" href="#/{{ $lang }}/medical" title="" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['doctor',''],['from_date',''],['to_date',''],['birth_date',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/rekam-medis.png') }}" /><span>{{ trans('messages.medical_records') }}</span></a></li>
                    @endif
                    @endif
                    @if(isset($acl->invoice) && $level != 'doctor')
                    @if($acl->invoice->read==true && $acl->invoice->create==true)
                    <li><a id="invoice-bar" href="#/{{ $lang }}/invoice" title="" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pembayaran.png') }}" /><span>{{ trans('messages.invoice') }}</span></a></li>
                    @endif
                    @endif
                    @if(isset($acl->report) && $level != 'doctor')
                    @if($acl->report->read==true)
                    <li class="menu-item-has-children">
											<a href="" title=""><img src="{{ asset('assets/images/icons/sidebar/laporan.png') }}" /><span>{{ trans('messages.report') }}</a>
											<ul>
												@if($level=="doctor")
												<li class="first-item"><a id="report-income-bar" href="#/{{ $lang }}/report/income" title="" onclick="loading($(this).attr('href'));repository(['from_date_month','now'],['to_date_month','now'],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pendapatan.png') }}" /><span>{{ trans('messages.transaction') }}</span></a></li>
												@else
												<li class="first-item"><a id="report-income-bar" href="#/{{ $lang }}/report/income" title="" onclick="loading($(this).attr('href'));repository(['from_date_month','now'],['to_date_month','now'],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pendapatan.png') }}" /><span>{{ trans('messages.title_income_report') }}</span></a></li>
												<li><a id="report-commission-bar" href="#/{{ $lang }}/report/commission" title="" onclick="loading($(this).attr('href'));repository(['from_date_month','now'],['to_date_month','now'],['page','1']);"><img src="{{ asset('assets/images/icons/link/komisi-dokter.png') }}" /><span>{{ trans('messages.title_commission_report') }}</span></a></li>
												<li><a id="report-service-bar" href="#/{{ $lang }}/report/service" title="" onclick="loading($(this).attr('href'));repository(['from_date_month','now'],['to_date_month','now'],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" /><span>{{ trans('messages.title_service_report') }}</span></a></li>
												<li><a id="report-medicine-bar" href="#/{{ $lang }}/report/medicine" title="" onclick="loading($(this).attr('href'));repository(['from_date_month','now'],['to_date_month','now'],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" /><span>{{ trans('messages.title_medicine_report') }}</span></a></li>
												<li class="space-expands"><a id="report-illness-bar" href="#/{{ $lang }}/report/illness" title="" onclick=""><img src="{{ asset('assets/images/icons/sidebar/penyakit.png') }}" /><span>{{ trans('messages.title_illness_report') }}</span></a></li>
												@endif
											</ul>
                    </li>
                    @endif
                    @endif
                    @endif
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- ========================================= END SIDEBAR ======================================== -->

<!-- ====================== SIDEBAR PROFILE & SETTING MOBILE RESPONSIVE ===========================-->
<div class="bg-transparent" id="action-click"></div>
<div class="container-menu-right">
    <div class="wrapper-menu-right">
      @if($level=="user" || $level=="doctor")
        <div class="canvas">
            <?php echo thumbnail('md', $level, $storage, $user->photo, $user->name); ?>
        </div>    
        <li class="collapse-profile">
          <a class="info"><h5>{{ $user->name }}</h5></a>
        </li>
		<div class="action-item" id="action-click"><a class="button" href="#/{{ $lang }}/profile" onclick="loading($(this).attr('href'))">{{ trans("messages.view_profile") }}</a></div>
          <div class="wrapper-collapse-profile">
              <div class="action-button" id="action-click">
                @if($level == "doctor")
                  <a href="doctor/logout?lang={{$lang}}" class="btn btn-primary right button">{{ trans("messages.logout") }}</a>
                @else
                  <a href="logout" class=" btn btn-primary right button">{{ trans("messages.logout") }}</a>          
                @endif
              </div>
          </div>
        @endif
        <li class="collapse-language">
          <div class="wrapper-language">
            <a class="info">
            @if($lang=="id")
              <img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-flag" /><span class="text">Indonesia</span> 
            @else
              <img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-flag" /><span class="text">English</span>
            @endif
            </a>
            <img src="{{ asset('assets/images/icons/nav/chevron-dm.png') }}" class="icon-arrow" />
          </div>
        </li>
        <div class="wrapper-collapse-language">
          <div class="menu-sec" id="action-click">
            @if($lang == "id")
            <a class="menu menu-flag" href="language/en"><img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-link flag" /> <span>English</span></a>
            @else
            <a class="menu menu-flag" href="language/id"><img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-link flag" /> <span>Indonesia</span></a>
            @endif
          </div>
        </div>
        @if($level=="admin")
        <div class="menu-sec" id="action-click">
            <a class="menu-sec" title="" href="#/{{ $lang }}/admin/setting" onclick="loading($(this).attr('href'))"><i class="fa fa-user"></i>&nbsp; <span>{{ trans('messages.account_setting') }}</span></a>
        </div>
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/setting" onclick="loading($(this).attr('href'))"><i class="fa fa-cogs"></i>&nbsp; <span>{{ trans('messages.application') }}</span></a>
        </div>
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="admin/logout"><i class="fa fa-sign-out"></i>&nbsp; <span>{{ trans('messages.logout') }}</span></a>
        </div>
        @else
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/clinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-klinik.png') }}" /> <span>{{ trans('messages.clinic') }}</span></a>
        </div>
        @if(isset($acl->clinic_polyclinic))
        @if($acl->clinic_polyclinic->read==true && $acl->clinic_polyclinic->update==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/clinic/polyclinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" /> <span>{{ trans('messages.polyclinic') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->clinic_insurance))
        @if($acl->clinic_insurance->read==true && $acl->clinic_insurance->update==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/clinic/insurance" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/settings/menu-asuransi.png') }}" /> <span>{{ trans('messages.insurance') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->user))
        @if($acl->user->read==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/user" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" /> <span>{{ trans('messages.user') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->service_category))
        @if($acl->service_category->read==true && $acl->service_category->create==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/service/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-layanan.png') }}" /> <span>{{ trans('messages.service_category') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->patient_category))
        @if($acl->patient_category->read==true && $acl->patient_category->create==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/patient/category" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" /> <span>{{ trans('messages.patient_category') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->service))
        @if($level != 'doctor')
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/service" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" /> <span>{{ trans('messages.service') }}</span></a>
        </div>
        @endif
        @endif
        @if(isset($acl->medicine))
        @if($acl->medicine->read==true && $acl->medicine->create==true)
        <div class="menu-sec" id="action-click">
            <a class="menu" title="" href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);"><img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" /> <span>{{ trans('messages.inventory') }}</span></a> 
        </div>
        @endif
        @endif
        @endif
    </div>
</div>
<!-- ====================== END SIDEBAR PROFILE & SETTING MOBILE RESPONSIVE =======================-->




<script type="text/javascript">

if("{{count($clinics) == 1}}") {
	$("#dLabel").removeAttr('data-toggle')
}

if("{{$level == 'user' && isset($user->email_verification) && $user->email_verification == false }}"){
		$(".header-top").toggleClass('header-up');
}

$('.menu-sidebar-right').click(function(){
  $('.container-menu-right').toggleClass('slider-menu');
  $('.bg-transparent').toggleClass('slider-menu');
});

$('.bg-transparent').click(function(){
  hideSidebar();
});

$("#action-click a").on("click", function(){
  hideSidebar();
});

function hideSidebar() {
	$('.container-menu-right').removeClass('slider-menu');
	$('.bg-transparent').removeClass('slider-menu');
}

$(".wrapper-menu-right .collapse-profile a h5").click(function() {
  $('.wrapper-menu-right .wrapper-collapse-profile').toggleClass("slider-menu");
});

$(".wrapper-menu-right .collapse-language .wrapper-language").click(function() {
  $('.wrapper-menu-right .wrapper-collapse-language').toggleClass("slider-menu");
});

$(".collapse-select-clinic h5").click(function(){
  $(".wrapper-collapse-clinic").toggleClass("slider-menu")
});

@if($level == "doctor")

function changeClinic(id_clinic, id_polyclinic, name_polyclinic) {
  var queryParam = '?id_clinic=' + id_clinic + '&id_polyclinic=' + id_polyclinic + '&name_polyclinic=' + name_polyclinic;
  redirect('{{$lang}}/catcher' + queryParam);
  $(".side-header").removeClass("slide-menu");
  $(".menu-options").removeClass("active");
  $(".bg-sidebar").removeClass("slider-menu");
}

@endif

@if($level=="user")
	var id_notif = [];
	if(setup!='' && help_setup == 1) {
		$('#step-progress').removeClass('hide');
		$('header').addClass('header-step');
		$('#main-content').addClass('main-step');

		if(setup=='schedule') {
				$('#step_2').addClass('done');
				$('#step_2').addClass('hide');
				$('#step_3').removeClass('hide');
				$('#step_3').removeClass('disable');
		} else if(setup=='service') {
				$('#step_2').addClass('done');
				$('#step_2').addClass('hide');
				$('#step_3').addClass('done');
				$('#step_3').addClass('hide');
				$('#step_3').removeClass('disable');
				$('#step_4').removeClass('hide');
				$('#step_4').removeClass('disable');
		} else if(setup=='commission') {
				$('#step_2').addClass('done');
				$('#step_2').addClass('hide');
				$('#step_3').addClass('hide');
				$('#step_3').addClass('done');
				$('#step_3').addClass('hide');
				$('#step_3').removeClass('disable');
				$('#step_4').addClass('done');
				$('#step_4').removeClass('disable');
				$('#step_5').removeClass('hide');
				$('#step_5').removeClass('disable');
		} else if(setup=='medicine') {
				$('#step_2').addClass('done');
				$('#step_2').addClass('hide');
				$('#step_3').addClass('done');
				$('#step_3').addClass('hide');
				$('#step_3').removeClass('disable');
				$('#step_4').addClass('done');
				$('#step_4').removeClass('disable');
				$('#step_5').addClass('done');
				$('#step_5').removeClass('disable');
				$('#step_6').removeClass('hide');
				$('#step_6').removeClass('disable');
				$('#next-step').addClass('hide');
		}
	}
  $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/notification?read=false",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
              var total = data.total;
              if(total > 0) {
                $("#num-notification").removeClass('hide');
                $("#num-notification").html(total);
              }
            } else {
            }
        },
        error: function(){
        }
    });


	function notifications() {
			$("#loading-notification").removeClass("hide");
			$("#reload-notification").addClass("hide");
			$("#section-notification").addClass("hide");
			
			$.ajax({
					url: "{{ $api_url }}/{{ $lang }}/notification?per_page=3",
					type: "GET",
					processData: false,
					contentType: false,
					success: function(data){
							if (!data.error) {
								$("#loading-notification").addClass("hide");
								$("#section-notification").removeClass("hide");
								var total = data.total;
								var per_page = data.per_page;
								var current_page = data.current_page;
								var last_page = data.last_page;
								var from = data.from;
								var to = data.to;
								if (total > 0) {	
										var item = data.data;
										var newNotif = 0;
										for(var i = 0; i < item.length; i++) {
												var text = item[i].notification.message;
												var read = "";
												if (item[i].read_at == null) {
													read = "read";
													newNotif++;
												}
												id_notif.push(item[i].id);
												var link = "";
												var type = "";
												var icon = '<i class="fa fa-question"></i>';
												if (item[i].data.type == "clinic_add") {
													icon = '<i class="fa fa-check-circle"></i>';
													type = "no-cursor";
												}
												if (item[i].data.type == "add_user" || item[i].data.type == "change_role_user") {
													icon = '<i class="fa fa-info"></i>';
													type = "no-cursor";
												}
												if (item[i].data.type == "clinic_schedule") {
													icon = thumbnail('sm','patient','{{ $storage }}',item[i].notification.photo,'P');
													link = 'href="#/{{ $lang }}/schedule/'+item[i].data.id+'" onclick=\'loading($(this).attr("href"));\'';
												}
												if (item[i].data.type == "clinic_doctor_confirm_schedule" || item[i].data.type == "clinic_doctor_cancel_schedule") {
														icon = thumbnail('sm','doctor','{{ $storage }}',item[i].notification.photo,'D');

														link = 'href="#/{{ $lang }}/schedule/'+item[i].data.id+'" onclick=\'loading($(this).attr("href"));\'';
												}
												var title = "";
												if (item[i].data.type == "clinic_schedule" || item[i].data.type == "clinic_doctor_confirm_schedule" || item[i].data.type == "clinic_doctor_cancel_schedule") {
														title = '<h1>'+item[i].notification.name+'</h1>';
												}
												if (item[i].data.type == "broadcast") {
													title = '<h1>{{ trans("messages.notification") }}</h1>';
													icon = '<i class="fa fa-info"></i>';
													link = 'href="#/{{ $lang }}/notifications" onclick=\'loading($(this).attr("href"));\'';
													text = limitChar(text, 120);
												}
												$("#section-notification").append('<a '+link+' id="notif-'+item[i].id+'" title="" class="menu '+read+' '+type+'">'
													+'<input type="hidden" name="id_notif[]" value="'+item[i].id+'">'
													+'<input type="hidden" name="read_notif[]" value="'+read+'">'
													+'<input type="hidden" id="notifTime-'+item[i].id+'" value="'+item[i].created_at.date+'">'
													+'<div class="thumb">'+icon+'</div>'
													+'<div class="info">'
													+'<div class="text">'
													+''+title+''
													+''+text+''
													+'<p><i class="fa fa-bell"></i><span id="notifTimeAgo-'+item[i].id+'">'+getTimeAgo(item[i].created_at.date)+'</span></p>'
													+'</div>'
													+'</div>'
													+'</a>');
										}
										$("#new-notification").html(newNotif);
								} else {
										$("#section-notification").append('<a class="menu no-hover">'
											+'<div class="info">'
											+'<div class="text">'
											+'{{ trans("messages.no_notifications") }}'
											+'</div>'
											+'</div>'
											+'</a>');
								}
							} else {
								$('#loading-notification').addClass('hide');
								$("#reload-notification").removeClass("hide");
								$("#reload-notification .reload").click(function(){ notifications(); });
							}
					},
					error: function(){
						$('#loading-notification').addClass('hide');
						$("#reload-notification").removeClass("hide");
						$("#reload-notification .reload").click(function(){ notifications(); });
					}
			});
	}
notifications();
setInterval(function(){
  for(i = 0; i < id_notif.length; i++) {
    try {
        $("#notifTimeAgo-"+id_notif[i]).html(getTimeAgo($("#notifTime-"+id_notif[i]).val()));
    } catch(err) {}
  }
}, 10000);
    @if($level=="user")
    $("#resend_email").on("click",function(){
      $("#img-icon").remove();
      $("#icon-resend_email").addClass("fa fa-circle-o-notch fa-spin fa-1x fa-fw");
	  $(".icon-verify").css("display", "inline-block");
      $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/user/resend-email-verification",
          type: "GET",
          processData: false,
          contentType: false,
          success: function(data){
            $("#icon-resend_email").removeClass("fa fa-circle-o-notch fa-spin fa-1x fa-fw");
			$(".icon-verify").append("<img src='{{ asset('assets/images/alertVerify.png') }}' id='img-icon' />");
			$(".icon-verify").css({'display': 'flex', 'flex-direction': 'row-reverse', 'justify-content': 'flex-end'});
              if (!data.error) {
                if (!data.success) {
                  notif(false,"{{ trans('validation.failed') }}");                
                } else {
                  notif(true,"{{ trans('validation.success_resend_email') }}");
                }
              } else {
                notif(false,"{{ trans('validation.failed') }}");
              }
          },
          error: function(){
            $("#icon-resend_email").removeClass("fa fa-circle-o-notch fa-spin fa-1x fa-fw");
			$(".icon-verify").append("<img src='{{ asset('assets/images/alertVerify.png') }}' id='img-icon' />");
			$(".icon-verify").css({'display': 'flex', 'flex-direction': 'row-reverse', 'justify-content': 'flex-end'});
            notif(false,"{{ trans('validation.failed') }}");
          }
      });
    });
    @endif
@endif
$(".side-menus li.menu-item-has-children > a").on("click",function(){
		$(this).parent().siblings().children("ul").slideUp();
		$(this).parent().siblings().removeClass("active");
		$(this).parent().children("ul").slideToggle();
		$(this).parent().toggleClass("active");
		return false;
});

$(".bg-sidebar").click(function(){
	$(".side-header.opened-menu").removeClass('slide-menu');
	$(".bg-sidebar").removeClass("slider-menu");
	$(".menu-options").removeClass("active");
});

$('.menu-options').on("click", function(){
	$(".side-header.opened-menu").toggleClass('slide-menu');
	$(".bg-sidebar").toggleClass('slider-menu');
	if("{{$level == 'user' && isset($user->email_verification) && $user->email_verification == false }}"){
		$(".main-content.verification-email-active").toggleClass('up');
	}else{
		$(".main-content").toggleClass('wide-content');
	}
	$("footer").toggleClass('wide-footer');
	$(".menu-options").toggleClass('active');
});

$(window).scroll(function() {    
	var scroll = $(window).scrollTop();
	if (scroll >= 10) {
		$(".side-header").addClass("sticky");
	}
	else{
		$(".side-header").removeClass("sticky");
		$(".side-header").addClass("");
	}
}); 

$(".side-menus nav > ul > li ul li > a").on("click", function(){
	$(".side-header").removeClass("slide-menu");
	$(".menu-options").removeClass("active");
	$(".bg-sidebar").removeClass("slider-menu");
});

$(".side-menus nav > ul > li > a").not('.side-menus nav > ul > li.menu-item-has-children > a').on("click", function(){
	$(".side-header").removeClass("slide-menu");
	$(".menu-options").removeClass("active");
	$(".bg-sidebar").removeClass("slider-menu");
});

$('.show-stats').on("click", function(){
	$(".toggle-content").addClass('active');
});

$('.toggle-content > span').on("click", function(){
	$(".toggle-content").removeClass('active');
});

$('.quick-links > ul > li > a').on("click", function(){
	$(this).parent().siblings().find('.dialouge').fadeOut();
	$(this).next('.dialouge').fadeIn();
	return false;
});

@if($level=="user")
	$('#notifications > a').on("click", function(){
		var numRead = 0;
		var id_notif = [];
		$('input[name="id_notif[]"]').each( function() {
				var read = $("#notif-"+this.value+" input[name='read_notif[]']").val();
				if (read == "read") {
					numRead++;
					id_notif.push(this.value);
					$("#notif-"+this.value+" input[name='read_notif[]']").val("");
				}
		});
		if (numRead > 0) {
			var get = "?level=user"
			for(i = 0;i < id_notif.length; i++) {
				get = get+"&id[]="+id_notif[i];
			}
			$.ajax({
					url: "{{ $api_url }}/{{ $lang }}/notification/read"+get+"",
					type: "GET",
					processData: false,
					contentType: false,
					success: function(data){
							if (data.error) {
							}
					},
					error: function(){
					}
			});
			var getNum = $("#num-notification").html();
			var num = parseInt(getNum)-numRead;
			if (num == 0) {
				$("#num-notification").addClass("hide");  
			}
			$("#num-notification").html(num);
		}
	});
@endif
$("html").on("click", function(){
	$(".dialouge").fadeOut();
	var showNotif = $("#notifications .dialouge").css("display");
	if (showNotif == "block") {
		$('input[name="id_notif[]"]').each( function() {
				$("#notif-"+this.value).removeClass("read");
		});
		$("#new-notification").html("0");
	}
});
$(".quick-links > ul > li > a, .dialouge").on("click",function(e){
	e.stopPropagation();
});

function goFullScreen() {
	var el = document.documentElement, rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;
	rfs.call(el);
}
$("#toolFullScreen").on("click",function() {
		goFullScreen();
});

$(function(){
	$('.side-menus').slimScroll({
			height: '80vh',
			wheelStep: 10,
			size: '2px'
	});
});
$(".data-attributes span").peity("donut");
</script>

@include('medicine.catalog')
