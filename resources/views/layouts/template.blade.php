@extends('layouts.app')

@section('main')
	<div id="main-content" class="main-content @if($level=='user') @if($user->email_verification!=true) main-verification @endif @endif @if($setup!='' && $help_setup==1) main-step @endif " data-ng-view></div>
@stop

@section('footer')
	@include('layouts.footer')
@stop