<div class="account-top-bar">
	<div class="container">
		<div class="logo-main-header">
	        <a href="#/" title="" class="text" onclick="loading($(this).attr('href'))">
	        	<img src="{{ $storage }}/images/icons/logo.png" />
	        </a>
	    </div>
	    <ul class="account-main-header">
	    	<li>
		    	<div class="quick-links">
					<ul>
						<li>
							<a title="" class="">
								@if($lang=="id")
								<img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-flag" /><span class="text">ID</span> 
								@else
								<img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-flag" /><span class="text">EN</span>
								@endif
								<!--<i class="fa fa-chevron-down"></i>-->
								<img src="{{ asset('assets/images/icons/nav/chevron-dm.png') }}" class="icon-arrow" />
							</a>
							<div class="dialouge" style="display: none;">
								<div class="menu-sec">
									<a class="menu menu-flag" href="language/id"><img src="{{ asset('assets/images/icons/header/flag/flag-id.png') }}" class="icon-link flag" /> <span>Indonesia</span></a>
								</div>
								<div class="menu-sec">
									<a class="menu menu-flag" href="language/en"><img src="{{ asset('assets/images/icons/header/flag/flag-en.png') }}" class="icon-link flag" /> <span>English</span></a>
								</div>
							</div>
						</li>
					</ul>
	            </div>
            </li>
	    	@if(!empty($id_user))
	    	<li class="action-logout-rs"><a href="logout">{{ trans('messages.logout') }}</a></li>
	    	@else
	    	<!--<li><a title="" href="{{ url('/') }}/{{ $lang }}/search" onclick="loading($(this).attr('href'))">{{ trans('messages_web.find_clinic') }}</a></li>-->
	    	<li id="link-login" class="action-login"><a title="" href="#/{{ $lang }}/doctor/login" onclick="loading($(this).attr('href'))">{{ trans('messages.login_app_doctor') }}</a></li>
	    	@endif
	    </ul>
	</div>
</div>


<script type="text/javascript">
	
$('.quick-links > ul > li > a').on("click", function(){
	$(this).parent().siblings().find('.dialouge').fadeOut();
	$(this).next('.dialouge').fadeIn();
	return false;
});

$("html").on("click", function(){
	$(".dialouge").fadeOut();
});

</script>