@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
        	<div class="notification-sec">
        		<span class="header">{{ trans("messages.notifications") }}</span>  
                <div id="header-notifications" class="filter">
                    <a id="all_notification" class="active" onclick="allNotification()">{{ trans("messages.all") }}</a>
                    <a id="unread_notification" onclick="unreadNotification()">{{ trans("messages.unread") }}</a>
                    <a onclick="markAllReadNotification()">{{ trans("messages.mark_all_read") }}</a>
                </div>              
                <div id="section-notifications">
                    
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>

<script type="text/javascript">

var id_notifications = [];

var keyRead = "";

function search(page, submit, read) {
    $("#view-more").remove();
    $('#loading').removeClass('hide');
    $('#reload').addClass('hide');

    if(read=="" && submit=="false") {
        var read = keyRead;
    }

    if(submit=="true") {
        $("#section-notifications").html("");
    }

    keyRead = read;

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/notification",
        type: "GET",
        data: "read="+read+"&per_page=10&page="+page,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading').addClass('hide');
                $('#reload').addClass('hide');

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    var item = data.data;
                    var no=from;
                    var id_notif = [];
                    for (var i = 0; i < item.length; i++) {
                    	var read = "";
                        if(item[i].read_at==null) {
                          read = "read";
                          id_notif.push(item[i].id);
                        }

                        var link = "";
                        var type = "";
                        var icon = '<i class="fa fa-question"></i>';
                        if(item[i].data.type=="clinic_add") {
                            icon = '<i class="fa fa-check-circle"></i>';
                            type = "no-cursor";
                        }

                        if(item[i].data.type=="add_user" || item[i].data.type=="change_role_user") {
                            icon = '<i class="fa fa-info"></i>';
                            type = "no-cursor";
                        }

                        if(item[i].data.type=="clinic_schedule") {
                            icon = thumbnail('sm','patient','{{ $storage }}',item[i].notification.photo,'P');

                            link = 'href="#/{{ $lang }}/schedule/'+item[i].data.id+'" onclick=\'loading($(this).attr("href"));\'';
                        }

                        if(item[i].data.type=="clinic_doctor_confirm_schedule" || item[i].data.type=="clinic_doctor_cancel_schedule") {
                            icon = thumbnail('sm','doctor','{{ $storage }}',item[i].notification.photo,'D');

                            link = 'href="#/{{ $lang }}/schedule/'+item[i].data.id+'" onclick=\'loading($(this).attr("href"));\'';
                        }

                        var title = "";
                        if(item[i].data.type=="clinic_schedule" || item[i].data.type=="clinic_doctor_confirm_schedule" || item[i].data.type=="clinic_doctor_cancel_schedule") {
                            title = '<h1>'+item[i].notification.name+'</h1>';
                        }

                        if(item[i].data.type=="broadcast") {
                            title = '<h1>{{ trans("messages.notification") }}</h1>';
                            icon = '<i class="fa fa-info"></i>';
                        }



                        $("#section-notifications").append('<a '+link+' class="menu '+read+' '+type+' ">'+
                            '<div class="thumb">'+
                                ''+icon+''+
                            '</div>'+
                            '<div class="info">'+
                                '<div class="text">'+
                                    ''+title+''+
                                    ''+item[i].notification.message+''+
                                    '<input type="hidden" id="time-'+item[i].id+'" value="'+item[i].created_at.date+'">'+
                                    '<p><i class="fa fa-bell"></i> <span id="timeAgo-'+item[i].id+'">'+getTimeAgo(item[i].created_at.date)+'</span></p>'+
                                '</div>'+
                            '</div>'+
                            //'<div class="close" onclick=\'$(this).parent().slideUp();deleteNotif("'+item[i].id+'");\'>x</div>'+
                        '</a>');

                        if(item[i].read_at==null) {
                            id_notifications.push(item[i].id);
                        }


                        no++;
                    }

                    var get = "?level={{ $level }}"
    				for(j=0;j<id_notif.length;j++) {
    					get = get+"&id[]="+id_notif[j];

                        try {
                            var check_id_notif = $("#notif-"+id_notif[j]+" input[name='read_notif[]']").val();

                            if(check_id_notif=="read") {
                                $("#notif-"+id_notif[j]).removeClass('read');
                                $("#new-notification").html(parseInt($("#new-notification").html())-1);
                            }
                        } catch(err) {}
    				}

    				$.ajax({
    					url: "{{ $api_url }}/{{ $lang }}/notification/read"+get+"",
    					type: "GET",
    					processData: false,
    					contentType: false,
    					success: function(dataRead){

                            if(dataRead.error) {
                            }

                            getNumRead();

    					},
                        error: function(){
                        }
    				});



                    if(total>=no) {
                    	var nextPage = current_page+1;
                    	$("#section-notifications").append('<div onclick=\'search('+nextPage+',"false","'+keyRead+'")\' id="view-more" class="footer">'
                            +'<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'
                            +'{{ trans("messages.view_more") }}'
    					+'</div>');
                    }
                } else {
                    $("#section-notifications").html('<div class="menu no-hover">'+
                            '<div class="info">'+
                                '<div class="text">'+
                                    '{{ trans("messages.no_notifications") }}'+
                                '</div>'+
                            '</div>'+
                        '</div>');
                }
            } else {
                $('#loading').addClass('hide');
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", keyRead); });
            }
            
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", keyRead); });
        }
    })
}

function deleteNotif(id) {
    formData= new FormData();
    formData.append("_method", "DELETE");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/notification/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(data.error) {
            }
        },
        error: function(){
        }
    });
}

search(1, "false", "");

function allNotification() {
    $("#all_notification").addClass("active");
    $("#unread_notification").removeClass("active");
    search(1, "true", "");    
}

function unreadNotification() {
    $("#all_notification").removeClass("active");
    $("#unread_notification").addClass("active");
    search(1, "true", "false");
}

function markAllReadNotification() {
    $("#section-notifications").html("");
    $('#loading').removeClass('hide');

    var get = "?all=true"

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/notification/read"+get,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataRead){

            if(dataRead.error) {
            }

            allNotification();
            getNumRead();

            $("#section-notification .menu").removeClass('read');
            $("#new-notification").html("0");

        },
        error: function(){
            allNotification();
        }
    });
}



setInterval(function(){
  for(i=0;i<id_notifications.length;i++) {
    try {
        $("#timeAgo-"+id_notifications[i]).html(getTimeAgo($("#time-"+id_notifications[i]).val()));
    } catch(err) {}
  }
}, 10000);


function getNumRead() {
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/notification?read=false",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {

              var total = data.total;

              if(total>0) {
                $("#num-notification").removeClass('hide');
              } else {
                $("#num-notification").addClass('hide');
              }

              $("#num-notification").html(total);
            } else {
            }
        },
        error: function(){
        }
    });
}

</script>
