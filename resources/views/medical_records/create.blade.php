@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 
<div class="panel-content">
    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_medical_records') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-add">
                        <div class="form-group" id="schedule-group">
                            <label class="control-label">{{ trans('messages.schedule') }}<span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="schedule">
                                    <option value=""></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="cell hide" id="loading">
                            <div class="card">
                                <span class="three-quarters">Loading&#8230;</span>
                            </div>
                        </div>
                        <div class="cell hide" id="reload">
                            <div class="card">
                                <span class="reload fa fa-refresh"></span>
                            </div>
                        </div>
                        <div id="wrapper-patient">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group content-column" id="polyclinic-group">
                                        <label class="control-label">{{ trans('messages.polyclinic') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="polyclinic" disabled="disabled" id="polyclinic">
                                                <option value="">{{ trans('messages.no_polyclinic') }}</option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group content-column" id="doctor-group">
                                        <label class="control-label">{{ trans('messages.doctor') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="doctor" disabled="disabled">
                                                <option value="">{{ trans('messages.no_doctor') }}</option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>                        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="name-group">
                                        <label class="control-label">{{ trans('messages.patient') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="name" disabled="disabled">
                                                <option value="">{{ trans('messages.no_patient') }}</option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{ trans('messages.type_of_care') }}</label>
                                        <select class="form-control" name="type_kunjungan" disabled="disabled">
                                            <option value="">{{ trans('messages.select_type_of_care') }}</option>
                                            <option value="outpatient">{{ trans('messages.outpatient') }}</option>
                                            <option value="inpatient">{{ trans('messages.inpatient') }}</option>
                                            <option value="promotif preventif">{{ trans('messages.preventive_promotive') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group hide">
                                <label class="control-label">{{ trans('messages.mrn') }}</label>
                                <input type="text" class="form-control" placeholder="<?php echo trans('messages.mrn'); ?>" name="mrn" disabled="disabled">
                                <input type="hidden" name="card_number" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="accordion" class="panel-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading gray" data-target="#section_detail-patient" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed">
                                            <div class="title-accordion">
                                                {{ trans('messages.subjective') }} <h6 class="detail-title-accordion"> {{ trans('messages.detail_subjective')}} </h6>
                                            </div>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="section_detail-patient" style="height: 0px;">
                                        <div class="panel-body" id="wrapper-loading-patient">
                                            <div class="cell hide" id="loading-detail_patient">
                                                <div class="card">
                                                    <span class="three-quarters">Loading&#8230;</span>
                                                </div>
                                            </div>
                                            <div class="cell hide" id="reload-detail_patient">
                                                <div class="card">
                                                    <span class="reload fa fa-refresh"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-patient">
                                            <div id="panel-detail_patient" class="hide">
                                                <div class="wrapper-patient">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <ul class="detail-profile">
                                                                <div>
                                                                    <li class="profile-features">
                                                                        <h4 class="first-item">{{ trans("messages.patient_data") }}</h4>
                                                                    </li>
                                                                    <li class="profile-features">  
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.birth_date') }}</span>
                                                                                <span id="birth_date"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features">  
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.gender') }}</span>
                                                                                <span id="gender"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.no_identity') }}</span>
                                                                                <span id="no_identity"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>            
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.phone_number') }}</span>
                                                                                <span id="phone"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.email') }}</span>
                                                                                <span id="email"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.address') }}</span>
                                                                                <span id="address"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <ul class="detail-profile">
                                                                <div>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/status.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.patient_status') }}</span>
                                                                                <span id="active"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.patient_category') }}</span>
                                                                                <span id="patient_category"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class="profile-features" style="margin-bottom: 20px;">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/payment-plan.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.financing') }}</span>
                                                                                <span id="financing"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <hr class="line-patient"/>
                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.blood_type') }}</span>
                                                                                <span id="blood_type"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li class="profile-features">
                                                                        <div class="detail-sec">
                                                                            <div class="icon-sec">
                                                                                <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                                                            </div>
                                                                            <div class="info-sec">
                                                                                <span class="title">{{ trans('messages.smoking') }}?</span>
                                                                                <span id="smoke"></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-top: 24px;">
                                                    <label class="control-label">{{ trans('messages.history_of_illness') }}</label>
                                                    <div class="form-group" id="riwayat-group">
                                                        <div class="border-group">
                                                            <select id="riwayat" name="riwayat[]" multiple="multiple"></select>
                                                        </div>
                                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                        <span class="help-block"></span>                  
                                                    </div>
                                                    <button type="button" id="icon-add" class="button-add" data-toggle="modal" data-target="#add-history">
                                                        <span class="fa fa-plus"></span> 
                                                        {{ trans('messages.add_history_of_illness') }}
                                                    </button>
                                                </div>
                                                <div class="form-group" style="margin-top: 24px;">
                                                    <label class="control-label">{{ trans('messages.history_of_allergy') }}</label>
                                                    <div class="form-group" id="riwayat-alergi-group" style="margin-bottom: 0px !important;">
                                                        <div class="border-group">
                                                            <select id="riwayat-alergi" name="riwayat-alergi[]" multiple="multiple"></select>
                                                        </div>
                                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                        <span class="help-block"></span>                  
                                                    </div>
                                                    <button type="button" id="icon-add-allergy" class="button-add" data-toggle="modal" data-target="#add-history-allergy">
                                                        <span class="fa fa-plus"></span> 
                                                        {{ trans('messages.add_history_of_allergy') }}
                                                    </button>
                                                </div>
                                                <div class="form-group" id="complaint-group">
                                                    <label class="control-label">{{ trans('messages.complaints')}} <span style="margin-left: 2px; color: var(--error);">*</span></label>
                                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_complaint') }}" name="complaint" id="complaint" />
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group" id="anamnese-group">
                                                    <label class="control-label">{{ trans('messages.anamnese') }} <span style="margin-left: 2px; color: var(--error);">*</span></label>
                                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_anamnese') }}" name="anamnese" id="anamnese" />
                                                    <span class="help-block"></span>
                                                </div>
                                                <label class="control-label">{{ trans('messages.history_of_vaccination') }}</label>
                                                <div class="panel-vaccine-history panel-vital">
                                                    <div class="form-gorup table-reponsive" id="list-vaccine-history">
                                                        <table class="table table-hove mini-table">
                                                            <thead>
                                                                <tr id="title-vaccine-history">
                                                                    <th>{{trans('messages.vaccine_name')}}</th>
                                                                    <th>{{trans('messages.vaccine_given_date')}}</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="data-vaccine-history">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade window" id="add-vaccine-history" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <img src="{{ asset('assets/images/icons/settings/add-vaccine.png') }}" />
                                                    <h5 class="modal-title" id="title-vaccine">{{ trans('messages.add_history_of_vaccination') }}</h5>
                                                    <button class="close" type="button" id="close-vaccine-history">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group" id="vaccine-group">
                                                        <input type="hidden" name="vaccine_id" value="" />
                                                        <label class="control-label">{{ trans('messages.vaccine_name') }}</label>
                                                        <input type="text" class="form-control" placeholder="{{ trans('messages.vaccine_name') }}" name="vaccine_name" id="vaccine_name" autocomplete="off" disabled >
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="form-group" id="vaccine-group">
                                                        <label class="control-label">{{ trans('messages.given_number') }}</label>
                                                        <input name="vaccine_dosage_sequence" id="vaccine_dosage_sequence" class="form-control" onkeydown="number(event)" value="" autocomplete="off" placeholder="{{ trans('messages.given_number_placeholder') }}" maxLength="5" />
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="form-group" id="vaccine-group">
                                                        <label class="control-label">{{ trans('messages.given_date') }}</label>
                                                        <div id="datepicker-vaccine-given-date" class="input-group date">
                                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="vaccine_given_date" id="vaccine_given_date" />
                                                            <span class="input-group-addon">
                                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                                            </span>
                                                        </div>
                                                        <br>
                                                        <span class="help-block"></span>  
                                                    </div>
                                                </div>
                                                <div class="modal-footer">                    
                                                    <button class="btn btn-primary" id="save-vaccine-history">
                                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                        {{ trans('messages.save') }}
                                                    </button>
                                                    <button class="btn btn-secondary" id="cancel-vaccine-history">{{ trans('messages.cancel') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade window" id="add-history" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                                        <i class="fa fa-plus"></i>
                                                        {{ trans('messages.add_history_of_illness') }}
                                                    </h5>
                                                    <button class="close" type="button" id="close-illness">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">               
                                                    <div class="form-group" id="illness-group">
                                                        <label class="control-label">{{ trans('messages.history_of_illness') }}</label>
                                                        <input type="text" class="form-control" placeholder="{{ trans('messages.history_of_illness') }}" name="illness"  autocomplete="off" id="illness" >
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">                    
                                                    <button class="btn btn-primary" id="add-illness">
                                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                        {{ trans('messages.save') }}
                                                    </button>
                                                    <button class="btn btn-secondary" id="cancel-illness">{{ trans('messages.cancel') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade window" id="add-history-allergy" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                                        <i class="fa fa-plus"></i>
                                                        {{ trans('messages.add_history_of_allergy') }}
                                                    </h5>
                                                    <button class="close" type="button" id="close-allergy">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group" id="allergy-category-group">
                                                        <label class="control-label">{{ trans('messages.category') }}</label>
                                                        <div class="border-group">
                                                            <select class="form-control" name="allergy-category" id="allergy-category">
                                                                <option value="" disabled selected>{{ trans('messages.select_allergy_category') }}</option>
                                                            </select>
                                                        </div>
                                                        <span class="help-block"></span>
                                                    </div>               
                                                    <div class="form-group" id="allergy-group">
                                                        <label class="control-label">{{ trans('messages.history_of_allergy') }}</label>
                                                        <input type="text" class="form-control" placeholder="{{ trans('messages.history_of_allergy') }}" name="allergy"  autocomplete="off" id="allergy" >
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">                    
                                                    <button class="btn btn-primary" id="add-allergy">
                                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                        {{ trans('messages.save') }}
                                                    </button>
                                                    <button class="btn btn-secondary" id="cancel-allergy">{{ trans('messages.cancel') }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading gray" data-target="#section_objective" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title" >
                                            <a class="accordion-toggle collapsed">
                                                <div class="title-accordion">
                                                    {{ trans('messages.objective') }} <h6 class="detail-title-accordion"> {{ trans('messages.detail_objective')}} </h6>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="section_objective" style="height: 0px;">
                                        <div class="panel-body">
                                            <div class="cell hide" id="loading-history">
                                                <div class="card">
                                                    <span class="three-quarters">Loading&#8230;</span>
                                                </div>
                                            </div>
                                            <div class="cell hide" id="reload-history">
                                                <div class="card">
                                                    <span class="reload fa fa-refresh"></span>
                                                </div>
                                            </div>
                                            <div class="hide" id="wrapper-objective">
                                                <div class="panel-table" id="panel-history">
                                                    <table class="table mini-table">
                                                        <thead>
                                                            <tr>
                                                                <th>{{ trans('messages.consultation') }}</th>
                                                                <th class="hide-lg">{{ trans('messages.diagnosis') }}</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="history">
                                                            <tr>
                                                                <td colspan="3" align="center">{{ trans('messages.no_result') }}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <a id="link-history" class="btn-add hide" target="_blank" onclick="repository(['polyclinic',''],['from_date',''],['to_date',''],['page','1']);">{{ trans('messages.view_all_history') }}</a>
                                                </div>
                                                <div id="panel-vital_sign" class="panel-vital">
                                                    <div class="table-vital">
                                                        <table class="table table-hover mini-table">
                                                            <thead>
                                                                <tr id="title-vital_sign">
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <tr id="data-sistole">
                                                                    <td class="middle">{{ trans('messages.sistole') }}</td>
                                                                </tr>
                                                                <tr id="data-diastole">
                                                                    <td class="middle">{{ trans('messages.diastole') }}</td>
                                                                </tr>
                                                                <tr id="data-pulse">
                                                                    <td class="middle">{{ trans('messages.pulse') }}</td>
                                                                </tr>                             
                                                                <tr id="data-respiratory_frequency">
                                                                    <td class="middle">{{ trans('messages.respiratory_frequency') }}</td>
                                                                </tr>                           
                                                                <tr id="data-body_temperature">
                                                                    <td class="middle">{{ trans('messages.body_temperature') }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3"><br/><h3 class="group-label first-label">{{trans('messages.physical_exam')}}</h3></td>
                                                                </tr>                         
                                                                <tr id="data-height">
                                                                    <td class="middle">{{ trans('messages.height') }}</td>
                                                                </tr>     
                                                                <tr id="data-head">
                                                                    <td class="middle">{{ trans('messages.head_circumference') }}</td>
                                                                </tr>                     
                                                                <tr id="data-weight">
                                                                    <td class="middle">{{ trans('messages.weight') }}</td>
                                                                </tr>
                                                                <tr id="data-abdomen">
                                                                    <td class="middle">{{ trans('messages.abdominal_circumference') }}</td>
                                                                </tr>
                                                                <tr id="data-imt">
                                                                    <td class="middle">IMT</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="panel-diagnostic">
                                                    <div class="form-group table-responsive" id="list-diagnostic">
                                                        <table class="table table-hover mini-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>{{ trans('messages.helper_diagnostic') }}</th>
                                                                    <th class="hide-md">{{ trans('messages.data_or_results') }}<span id="date-diagnostic" style="margin-left: 5px;"></span></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="data-diagnostic">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div id="list-file" class="list-file-sec"></div>
                                                    <div class="action-diagnostic">
                                                        <a id="add-file" class="btn-add hide">+ {{ trans('messages.add_attachment') }}</a>
                                                        <span type="button" data-toggle="modal" data-target="#diagnostic" id="add-diagnostik" class="btn-add" style="cursor: pointer;">+ {{ trans('messages.add_data_helper') }}</span>
                                                    </div>
                                                </div>
                                                <div class="panel-physical-examination panel-vital">
                                                    <div class="form-gorup table-reponsive">
                                                        <table class="table table-hove mini-table">
                                                            <thead>
                                                                <tr id="title-physical-examination">
                                                                    <th>{{trans('messages.physical_examination')}}</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id='form-physical-examination-table'></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="consciousness-group">
                                                    <label class="control-label">{{ trans('messages.consciousness') }} <span>*</span></label>
                                                    <div class="border-group">
                                                        <select class="form-control" name="consciousness" id="consciousness">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                    <span class="help-block"></span>
                                                </div>
                                                <div class="form-group" id="add-note">
                                                    <label class="control-label">{{ trans('messages.additional_notes') }}</label>
                                                    <input class="form-control" placeholder="{{ trans('messages.additional_notes') }}" name="note" id="notes" />
                                                    <span class="help-block"></span>
                                                </div>
                                                <div>
                                                    <div id="images-to-upload" class="wrapper-import"></div>
                                                        <input style="display: none;" type="file" name="image[]" id="images" multiple />
                                                    <div class="title-import-file p-4 text-center">
                                                        <h5>{{ trans('messages.upload_helper_documents') }}</h5>
                                                        <h6>{{ trans('validation.failed_size_import') }}</h6>
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header-import">
                                                                <img src="{{ asset('assets/images/Exportupload.png') }}" />
                                                                <h5 class="font-weight-bold m-0">{{ trans('messages.upload_data_patient') }}</h5>
                                                            </div>
                                                            <div class="modal-body" id="body-description">
                                                            </div>
                                                            <div class="" id="footer-action">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading gray" data-target="#section_diagnosis" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title" id="collapse_diagnosis">
                                            <a class="accordion-toggle collapsed">
                                            <div class="title-accordion">
                                                {{ trans('messages.assessment') }} <h6 class="detail-title-accordion"> {{ trans('messages.detail_assessment')}} </h6>
                                            </div>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="section_diagnosis">
                                        <div class="panel-body panel-form">
                                            <div class="form-group" id="diagnosis-group">
                                                <label class="control-label">{{ trans('messages.diagnosis') }} <span>*</span></label>
                                                <input type="text" class="form-control" placeholder="{{ trans('messages.input_diagnosis') }}" name="diagnosis" autocomplete="off" />
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="form-group content-column" id="prognosis-group">
                                                <label class="control-label">{{ trans('messages.prognosis') }} </label>
                                                <div class="border-group">
                                                    <select class="form-control" name="prognosis" disabled>
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                                <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="form-group content-collapse" id="icd-group">
                                                <label class="control-label">{{ trans('messages.icd') }} <span>*</span></label>
                                                <input  type="text" name='select_icd' class="form-control" placeholder="{{ trans('messages.select_icd') }}" />
                                                <span id="loading-icd" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="form-group table-responsive table-sub hide" id="list-icd">
                                                <table class="table table-hover mini-table">
                                                    <thead>
                                                        <tr>
                                                            <th width="100px">{{ trans('messages.code') }}</th>
                                                            <th>{{ trans('messages.icd') }}</th>
                                                            <th>{{ trans('messages.type') }}</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="data-icd">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default" style="border-bottom: 1px solid #f0f0f0;">
                                    <div class="panel-heading gray" data-target="#panel-planning" data-parent="#accordion" data-toggle="collapse">
                                        <h4 class="panel-title"  id="collapse_service">
                                            <a class="accordion-toggle collapsed">
                                                <div class="title-accordion">
                                                    {{ trans('messages.planning') }} <h6 class="detail-title-accordion"> {{ trans('messages.detail_planning')}} </h6>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-collapse collapse" id="panel-planning" style="height: 0px;">
                                        <div class="panel-body" style="text-align: center;">
                                            <div class="panel-list-service">
                                                <div class="form-group table-responsive hide" id="list-service">
                                                    <table class="table table-hover mini-table">
                                                        <thead>
                                                            <tr>
                                                                <th>{{ trans('messages.service_action') }}</th>
                                                                <th class="hide-md">{{ trans('messages.dependents_type') }}</th>
                                                                <th class="hide-md">{{ trans('messages.quantity') }}</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>    
                                                        <tbody id="data-service">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if(empty($id_clinic))
                                                <a id="add-service_true" data-toggle="modal" data-target="#service" class="btn-add hide">+ {{ trans('messages.add_service_action') }}</a>
                                                <a id="add-service_false" onclick="notif(false,'<?php echo trans("validation.empty_clinic"); ?>')" class="btn-add">+ {{ trans('messages.add_service_action') }}</a>
                                                @else
                                                <a data-toggle="modal" data-target="#service" class="btn-add">+ {{ trans('messages.add_service_action') }}</a>
                                                @endif
                                            </div>
                                            <br />
                                            <div class="panel-list-medicine">
                                                <div class="form-group table-responsive hide" id="list-medicine">
                                                    <table class="table table-hover mini-table">
                                                        <thead>
                                                            <tr>
                                                                <th class="header-table-mini">{{ trans('messages.medicine') }}</th>
                                                                <th class="hide-md header-table-mini">R/</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.dose') }}</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.day') }}</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.unit') }}</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.qty') }}</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="data-medicine">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if(empty($id_clinic))
                                                <a id="add-medicine_true" data-toggle="modal" data-target="#medicine" class="btn-add hide">+ {{ trans('messages.add_medicine_medical_records') }}</a>
                                                <a id="add-medicine_false" onclick="notif(false,'<?php echo trans("validation.empty_clinic"); ?>')" class="btn-add">+ {{ trans('messages.add_medicine_medical_records') }}</a>
                                                @else
                                                <a data-toggle="modal" data-target="#medicine" class="btn-add">+ {{ trans('messages.add_medicine_medical_records') }}</a>
                                                @endif
                                            </div>
                                            <br />
                                            <div class="panel-list-vaccine">
                                                <div class="form-group table-responsive hide" id="list-vaccine-item">
                                                    <table class="table table-hover mini-table">
                                                        <thead>
                                                            <tr>
                                                                <th class="header-table-mini">{{ trans('messages.vaccine') }}</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.dose') }}</th>
                                                                <th class="hide-md header-table-mini">{{ trans('messages.lot_no') }}</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="data-vaccine-item">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if(empty($id_clinic))
                                                <a id="add-vaccine_true" data-toggle="modal" data-target="#vaccine" class="btn-add hide">+ {{ trans('messages.add_vaccine') }}</a>
                                                <a id="add-vaccine_false" onclick="notif(false,'<?php echo trans("validation.empty_clinic"); ?>')" class="btn-add">+ {{ trans('messages.add_vaccine') }}</a>
                                                @else
                                                <a data-toggle="modal" data-target="#add-vaccine-item" class="btn-add">+ {{ trans('messages.add_vaccine') }}</a>
                                                @endif
                                             </div>
                                        </div>
                                        <div class="form-group" id="drug-therapy-group">
                                            <label class="control-label">{{ trans('messages.drug_therapy') }}</label>
                                            <textarea name="drug-therapy" class="form-control" rows="6" placeholder="{{ trans('messages.note_drug_therapy') }}"></textarea>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group" id="non-drug-therapy-group">
                                            <label class="control-label">{{ trans('messages.non_drug_therapy') }}</label>
                                            <textarea name="non-drug-therapy" class="form-control" rows="6" placeholder="{{ trans('messages.note_monitoring') }}"></textarea>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-select">
                            <div class="col-md-6 content-select-wrapper" id="status-parent">
                                <div class="form-group content-column" id="status-group">
                                    <label class="control-label">{{ trans('messages.return_home_status') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="status" disabled>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group hide" id="date_home-group">
                                    <label class="control-label">{{ trans('messages.date_of_return') }}</label>
                                    <div id="datepicker_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="date_end" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group hide" id="search_rujukan-group">
                                    <a data-toggle="modal" data-target="#rujukan" id="add-rujukan" class="btn-add">{{ trans('messages.find_references') }}</a>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" id="reference_selected-group">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">PPK {{ trans('messages.references') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.entry_of_data') }}" name="name_ppk" disabled/>
                                    <input type="hidden" placeholder="{{ trans('messages.entry_of_data') }}" name="code_reference_hospital" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.specialist_subspecialist') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.entry_of_data') }}" name="name_specialist" disabled/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.planned_date_of_visit') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.entry_of_data') }}" name="date_to_reference_string" disabled/>
                                    <input type="hidden" class="form-control" placeholder="{{ trans('messages.entry_of_data') }}" name="date_to_reference"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="notes-group">
                                    <label class="control-label">{{ trans('messages.note') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.entry_of_data') }}" name="notes"/>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" id="diagnosa-text">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="badge-lightgrey"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" />{{ trans('messages.diagnostic_information') }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" id="tacc-group">
                            <div class="col-md-12">
                                <div class="form-group" id="gender-group">
                                    <label class="control-label">{{ trans('messages.type_of_visit') }} <span>*</span></label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="dengan tacc" name="status_tacc" data-trigger="tacc"><span></span> {{ trans('messages.refer_to_tacc') }}</label>
                                        <label><input type="radio" value="tanpa tacc" name="status_tacc" data-trigger="noTacc"><span></span> {{ trans('messages.refer_without_tacc') }}</label>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row group-tacc hide" id="tacc">
                            <div class="col-md-6">
                                <div class="form-group content-column" id="factor-group">
                                    <label class="control-label">{{ trans('messages.referral_factor') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="faktor_rujuk">
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group content-column hide" id="reason-group">
                                    <label class="control-label" id="text-alasan">{{ trans('messages.reason') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="reason">
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group hide" id="reason_complication-group">
                                    <label class="control-label" id="text-alasan_complication">{{ trans('messages.reason') }} <span>*</span></label>
                                    <input  type="text" name='reason_complication' class="form-control" placeholder="{{ trans('messages.select_icd') }}" />
                                    <span id="loading-reason_complication" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group table-responsive table-sub hide" id="list-complicated">
                                    <table class="table table-hover mini-table">
                                        <thead>
                                            <tr>
                                                <th width="100px">{{ trans('messages.code') }}</th>
                                                <th>{{ trans('messages.icd') }}</th>
                                                <th>{{ trans('messages.type') }}</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="data-complicated">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row group-tacc hide" id="noTacc">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.reason') }}<span>*</span></label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.enter_of_reason') }}" name="reason_notacc"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ ucwords(trans('messages.save')) }}
                            </button>
                            <a href="#/{{ $lang }}/medical" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade window" id="service" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" />
                <h5 class="modal-title" id="title-service">{{ trans('messages.add_service_action') }}</h5>
            </div>
            <form id="form-service">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id" value="" />
                    <input type="hidden" name="id_item" value="" />
                    <input type="hidden" name="id_service" value="" />
                    <div class="form-group" id="service_name-group">
                        <label class="control-label">{{ trans('messages.service_action') }} <span>*</span></label>
                        <div class="border-group">
                            <input name="service_name" id='input_service_name' placeholder= "{{ trans('messages.select_service_action') }}" type="text" class="form-control"/>
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="service_code-group">
                        <label class="control-label">{{ trans('messages.service_code') }}</label>
                        <input name='service_code' type="text" class="form-control" placeholder="{{ trans('messages.service_code') }}" disabled="disabled" />
                        <input name='kode_bpjs' type="hidden"/>
                    </div>
                    <div class="form-group hide" id="gestational_age-group">
                        <label class="control-label">{{ trans('messages.pregnancy_age_weeks') }} <span>*</span></label>
                        <table>
                            <tr>
                                <td id="age1-group">
                                    <input type="number" class="form-control" placeholder="{{ trans('messages.enter_pregnancy_age') }}" name="age1" onkeydown="number(event)" autocomplete="off" />
                                    <span class="help-block"></span>
                                </td>
                                <td><span style="margin: 5px 15px;">s/d</span></td>
                                <td id="age2-group">
                                    <input type="number" class="form-control" placeholder="{{ trans('messages.enter_pregnancy_age') }}" name="age2" onkeydown="number(event)" autocomplete="off" />
                                    <span class="help-block"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="form-group" id="service_quantity-group">
                        <label class="control-label">{{ trans('messages.quantity') }} <span>*</span></label>
                        <input  type="number" name='service_quantity' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off" />
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade window" id="medicine" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="title-medicine">{{ trans('messages.add_medicine_medical_records') }}</h5>
            </div>
            <form id="form-medicine">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id" value="" />
                    <input type="hidden" name="id_item" value="" />
                    <input type="hidden" name="id_medicine" value="" />
                    <input type="hidden" name="unit_medicine" value="" />
                    <input type="hidden" name="name_signa3" value="" />
                    <input type="hidden" name="val_qty_unit" value="" />
                    <input type="hidden" name="qty_unit_checked" value="" />
                    <input type="hidden" name="id_unit_checked" value="" />
                    <div class="form-group" id="check_racikan-group">
                        <div class="sf-check">
                            <label><input type="checkbox" value="1" name="is_racikan" data-trigger="is_racikan"><span></span> Obat Racikan</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group hide is_racikan" id="racikan-group">
                        <label class="control-label">{{ trans('messages.concoction') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="Masukkan nama racikan" name="racikan" />
                        <span class="help-block"></span>
                    </div>
                    <div class="container-medicine hide is_racikan">
                        <div id='content_'>
                        <div class="list_content">
                            <label class="control-label" id="label_number">{{ trans('messages.medicine') }} 1</label>
                            <div class="content-medicine">
                                <div class="form-group column-group" id="medicine_racikan-group">
                                    <label class="control-label">{{ trans('messages.inventory') }} <span>*</span></label>
                                    <div class="border-group">
                                        <input name="medicine_racikan" id="medicine_racikan-1" placeholder= "{{ trans('messages.select_medicine') }}" type="text" class="form-control medicine_racikan" data-id="" />
                                    </div>
                                    <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <label class="control-label" id="title-notes"></label>
                                </div>
                                <div class="col-xs-6 list-l">
                                    <div>
                                        <label class="control-label">{{ trans('messages.qty') }} <span>*</span></label>
                                        <input class="form-control" placeholder="{{ trans('messages.input_quantity') }}" name="qty_racikan" oninput="setNotifStock(this)" type="text" onkeydown="number(event)" autocomplete="off" onchange="setNotifStock(this)" />
                                        <p id="notif-stock-concoction" class="notif-empty"></p>
                                    </div>
                                </div>
                                <div class="col-xs-6 list-r">
                                    <div>
                                        <label class="control-label">{{ trans('messages.unit') }} <span>*</span></label>
                                        <div class="border-group">
                                            <select class="form-control" name="unit_racikan">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="action-add-medicine hide is_racikan">
                        <button class="btn btn-secondary" type="button" id="add_medicine">+ {{ trans('messages.add_medicine_medical_records') }}</button>
                    </div>

                    <div class="form-group hide is_racikan" id="instruction_racikan-group">
                        <label class="control-label">{{ trans('messages.instruction_concoction') }} <span>*</span></label>
                        <input class="form-control" placeholder="{{ trans('messages.enter_of_instruction_concoction') }}" name="instruction_racikan"/>
                        <span class="help-block"></span>
                    </div>

                    <div class="form-group column-group" id="medicine_name-group">
                        <label class="control-label">{{ trans('messages.medicine') }} <span>*</span></label>
                        <div class="border-group">
                            <input name="medicine_name" placeholder= "{{ trans('messages.select_medicine') }}" type="text" class="form-control" data-id="" />
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="medicine_code-group">
                        <label class="control-label">{{ trans('messages.medicine_code_medical_records') }}</label>
                        <input name='medicine_code' type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_code_medical_records') }}" disabled="disabled" />
                    </div>

                    <div class="form-group" id="interaction-group">
                        <label class="control-label">{{ trans('messages.record_of_medicine_interactions') }}</label>
                        <input name='interaction' type="text" class="form-control" placeholder="{{ trans('messages.add_record_of_medicine_interactions') }}" disabled="disabled" />
                    </div>

                    <div class="form-group" id="medicine_dose-group">
                        <div class="flex-content">
                            <label class="control-label">{{ trans('messages.dose_signa') }} <span>*</span></label>
                            <label class="control-label hide is_racikan right">{{ trans('messages.unit_or_dose') }} <span>*</span></label>
                        </div>
                        <div class="row" style="margin-bottom: 16px;">
                            <div class="col-xs-3 signa1-group">
                                <input type="text" class="form-control" placeholder="{{ trans('messages.dose') }}" name="signa1" onkeydown="number(event)" autocomplete="off" oninput="setNotesConcoction(this)" />
                                <span class="help-block"></span>
                            </div>
                            <div class="col-xs-1 multiplication">
                                <span>x</span>
                            </div>
                            <div class="col-xs-3 signa2-group">
                                <input class="form-control" placeholder="unit" name="signa2" onkeydown="number(event)" autocomplete="off" oninput="setNotesConcoction(this)" />
                                <span class="help-block"></span>
                                <p></p>
                            </div>
                            <div class="col-xs-5 signa3-group" id="one_day-group">
                                <select class="form-control" name="signa3">
                                    <option value=""></option>
                                    <option value="per hari">{{ trans('messages.one_day') }}</option>
                                    <option value="per minggu">{{ trans('messages.one_week') }}</option>
                                    <option value="per bulan">{{ trans('messages.one_month') }}</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-xs-5 hide is_racikan" id="unit_dose-group">
                                <input class="form-control" placeholder="{{ trans('messages.unit_or_dose') }}" name="unit_dose" oninput="setNotesConcoction(this)"/>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div id="duration-group">
                            <label class="control-label">{{ trans('messages.duration') }} <span>*</span></label>
                             <div class="content-field-row">
                                <div>
                                    <input type="number" class="form-control" placeholder="{{ trans('messages.add_duration') }}" name="duration" id="duration_day" onkeydown="number(event)" min="1" />
                                    <span class="help-block"></span>
                                </div>
                                <span>{{ trans('messages.day')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hide is_racikan">
                        <div class="row">
                            <div class="col-xs-6">
                                <label class="control-label">{{ trans('messages.time') }}</label>
                                <select class="form-control" name="time" onchange="setNotesConcoction(this)">
                                    <option value=""></option>
                                    <option value="per hari">{{ trans('messages.one_day') }}</option>
                                    <option value="per minggu">{{ trans('messages.one_week') }}</option>
                                    <option value="per bulan">{{ trans('messages.one_month') }}</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <div class="field" id="duration_racikan-group">
                                    <label class="control-label">{{ trans('messages.duration') }} <span>*</span></label>
                                    <input class="form-control" placeholder="{{ trans('messages.add_duration') }}" name="duration_racikan" onkeydown="number(event)" oninput="setNotesConcoction(this)" autocomplete="off" />
                                    <span class="hidden-char text-input-value">{{ trans('messages.day') }}</span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hide is_racikan">
                        <label class="control-label">{{ trans('messages.information') }}</label>
                        <div class="wrapper-notes">
                            <span id="notes_concoction"></span>
                        </div>
                    </div>
                    <div class="hide is_racikan">
                        <div class="sf-check">
                            <label><input type="checkbox" value="1" name="package" data-trigger="package"><span></span> {{ trans('messages.new_package') }}</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group hide package">
                        <div class="row">
                            <div class="col-xs-6" id="qty_package-group">
                                <label class="control-label">{{ trans('messages.quantity_package') }} <span>*</span></label>
                                <input type="text" name='qty_package' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off"/>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-xs-6" id="type_package-group">
                                <label class="control-label">{{ trans('messages.type_package') }}</label>
                                <div class="border-group">
                                    <select class="form-control" name="type_package">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <label class="control-label" id="label-qty">{{ trans('messages.qty') }} <span>*</span></label>
                    <div class="form-group" id="qty_radio_button-group"></div>
                    <div class="form-group" id="instructions-group">
                        <label class="control-label">{{ trans('messages.instructions_for_use') }} <span>*</span></label>
                        <input class="form-control"  placeholder="{{ trans('messages.add_instructions_for_use') }}" name="instructions" />
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit" id="save-inventory">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade window" id="add-vaccine-item" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-layanan.png') }}" />
                <h5 class="modal-title" id="title-vaccine">{{ trans('messages.add_vaccine') }}</h5>
            </div>
            <form id="form-vaccine-item">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="vaccine_item_id" value="" />
                    <input type="hidden" name="vaccine_item_old_id" value="" />
                    <div class="form-group column-group" id="vaccine_item_name-group">
                        <label class="control-label">{{ trans('messages.choice_inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <input name="vaccine_item_name" placeholder= "{{ trans('messages.choice_inventory') }}" type="text" class="form-control" data-id="" />
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="vaccine_item_code-group">
                        <label class="control-label">{{ trans('messages.medicine_code') }}</label>
                        <input name="vaccine_item_code" type="text" class="form-control" placeholder="{{ trans('messages.medicine_code') }}" disabled="disabled" />
                    </div>
                    <div class="form-group" id="vaccine_item_lot_no-group">
                        <label class="control-label">{{ trans('messages.batch_lot_no') }}</label>
                        <input name='vaccine_item_lot_no' type="text" class="form-control" placeholder="{{ trans('messages.input_lot_no') }}" />
                    </div>
                    <div class="form-group" id="vaccine_item_quantity_unit-group">
                        <div class="row" style="margin-bottom: 16px;">
                            <div class="col-xs-6 vaccine_item_quantity-group">
                                <label class="control-label">{{ trans('messages.quantity_or_dose') }}</label>
                                <input type="text" class="form-control" placeholder="{{ trans('messages.input_quantity') }}" name="vaccine_item_quantity" onkeydown="number(event)" autocomplete="off"/>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-xs-6 vaccine_item_unit-group" id="vaccine_item_unit-group">
                                <label class="control-label">{{ trans('messages.choice_unit') }}</label>
                                <select class="form-control" name="vaccine_item_unit">
                                    <option value="">{{ trans('messages.choice_unit') }}</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit" id="save-vaccine-item">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade window-confirm" id="confirmAccept" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/confirm/confirm-activate.png') }}" />
                    <h1>{{ trans('messages.confirm_accept_diagnostic') }}</h1>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary button-confirmation" onclick="submitDataAdd()" id="confirm">
                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                    {{ trans('messages.confirmation') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade window-confirm" id="printLetter" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                    <h1>{{ trans('messages.print_referral') }}</h1>
                </div>
                <form class="sec form-bpjs" role="form" style="margin-top: 25px;" id="formCheck">
                    <div class="form-group" id="letter-group">
                        <label class="control-label" style="font-size: 12px;">{{ trans('messages.select_print_results') }}</label>
                        <div class="sf-radio"> 
                            <label style="font-size: 14px;"><input type="radio" value="0" name="letter"><span></span> {{ trans('messages.letter_of_further_reference') }}</label>
                            <label style="font-size: 14px;"><input type="radio" value="1" name="letter"><span></span> {{ trans('messages.letter_of_further_reference') }} + {{ trans('messages.referral_back') }}</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary " id="confirm">
                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                    {{ trans('messages.confirmation') }}
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>


<script type="text/javascript">

listAllergyCategories();

/**
 * VACCINE FEATURE
 */ 

// VACCINE HISTORY //
var vaccinesCache = [];

function addVaccineCache(data) {
    vaccinesCache.push(data);
}

function removeVaccineCache(data) {
    const iToDelete = [];
    vaccinesCache.forEach(function(item, index) {
        if (item.id == data.id && item.dosage_sequence == data.dosage_sequence) {
            iToDelete.push(index);
        }
    });
    iToDelete.sort(function(a, b) { return b - a });
    iToDelete.forEach(function(item) {
        vaccinesCache.splice(item, 1);
    });
}

function appendVaccineHistoryToFormData(formData) {
    for (let i in vaccinesCache) {
        formData.append(`vaccine[history][${i}][id]`, vaccinesCache[i].id);
        formData.append(`vaccine[history][${i}][dosage_sequence]`, vaccinesCache[i].dosage_sequence);
        formData.append(`vaccine[history][${i}][date_given]`, vaccinesCache[i].date_given);
    }
}

function showAddVaccineHistory(id, name) {
    $("#add-vaccine-history").modal("show");
    $("#vaccine-group input[name='vaccine_id']").val(id);
    $("#vaccine-group input[name='vaccine_name']").val(name);
}

// add date time picker
$('#datepicker-vaccine-given-date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

// close modal on cancel
$("#close-vaccine-history").click(function(event){
    event.preventDefault();
    $("#cancel-vaccine-history").click();
});

// reset modal form input on cancel
$("#cancel-vaccine-history").click(function(event){
    event.preventDefault();
    resetVaccineHistory();
    $("#add-vaccine-history").modal("hide");
});

function resetVaccineHistory() {
    $("#add-vaccine-history").attr("disabled", false);
    $("#add-vaccine-history").removeClass("loading");
    resetValidation('vaccine-group #vaccine_dosage_sequence');
    resetValidation('vaccine-group #vaccine_given_date');
    $("#vaccine-group input[name=vaccine_id]").val("");
    $("#vaccine-group input[name=vaccine_name]").val("");
    $("#vaccine-group input[name=vaccine_dosage_sequence]").val("");
    $("#vaccine-group input[name=vaccine_given_date]").val("");
}

function listVaccineHistory(vaccineHistories) {
    $("#form-add #data-vaccine-history").html("");
    for (let i in vaccineHistories) {
        processVaccineHistory(vaccineHistories[i], false);
    }
}

function processVaccineHistory(vaccineHistory, isPush) {
    let title = '<td>' + vaccineHistory.name + '</td><td id="vaccine-row-' + vaccineHistory.id + '">';
    let action = '<div class="chip chip-add" id="chip-action-' + vaccineHistory.id + '">'
                    + '<span onClick="showAddVaccineHistory(\'' + vaccineHistory.id + '\',\'' + vaccineHistory.name + '\');">'
                    +   '<svg class="chip-add-svg" focusable="false" viewBox="0 0 6 6" aria-hidden="true"><path d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z"></path></svg>'
                    + '</span>'
                    + '</div>';
    
    if (isPush) {
        $('#chip-action-' + vaccineHistory.id).remove();
        $('#form-add #vaccine-row-' + vaccineHistory.id).append(
            '<div class="chip" id="' + vaccineHistory.id + '_' + vaccineHistory.dosage_sequence + '">'
            +   '<div class="chip-content">' + vaccineHistory.dosage_sequence + ' &bull; ' + getShortFormatDate(vaccineHistory.date_given) + '</div>'
            +   '<div>'
            +       '<span onClick="deleteVaccineHistory(\'' + vaccineHistory.id + '_' + vaccineHistory.dosage_sequence + '\');">' 
            +           '<svg class="chip-close-svg" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>'
            +       '</span>'
            +   '</div>'
            + '</div>'
            + action);
        addVaccineCache(vaccineHistory);
        return 0;
    }

    // here is to initiate data
    let section = '';
    for (let i in vaccineHistory.history) {
        section +=   '<div class="chip">'
                    +   '<div class="chip-content">' + vaccineHistory.history[i].dosage_sequence + ' &bull; ' + getShortFormatDate(vaccineHistory.history[i].date_given) + '</div>'
                    + '</div>';
    }

    $("#form-add #list-vaccine-history").removeClass("hide");
    $("#form-add #data-vaccine-history").append('<tr class="template_vaccine_history" id="vaccine-history-' + vaccineHistory.id + '">' + title + section + action + '</td></tr>');
}

function deleteVaccineHistory(identifier) {
    $('#form-add #' + identifier).remove();
    const id = identifier.split('_');
    removeVaccineCache({ id: id[0], dosage_sequence: id[1] });
    console.log(vaccinesCache);
}

$("#save-vaccine-history").click(function(event) {
    event.preventDefault();
    let id = $("#vaccine-group input[name='vaccine_id']").val();
    let name = $("#vaccine-group input[name='vaccine_name']").val();
    let dosageSequence = $("#vaccine-group input[name='vaccine_dosage_sequence']").val();
    let givenDate = $("#vaccine-group input[name='vaccine_given_date']").val();
    
    if (dosageSequence != "" && givenDate != "") {
        $("#add-vaccine-history").attr("disabled", true);
        $("#add-vaccine-history").addClass("loading");
    } else {
        formValidate(false, ['vaccine-group #vaccine_dosage_sequence','{{ trans("validation.empty_vaccination_dosage_sequence") }}', true]);
        formValidate(false, ['vaccine-group #vaccine_given_date','{{ trans("validation.empty_vaccination_given_date") }}', true]);
        return 0;
    }

    processVaccineHistory({
        id: id,
        name: name,
        dosage_sequence: dosageSequence,
        date_given: givenDate.split("/").reverse().join("-")
    }, true);

    notif(true, "{{ trans('validation.success_add_vaccination_history') }}");
    $("#add-vaccine-history").modal("toggle");
    resetVaccineHistory();
});


// VACCINE ITEM //

var vaccineItemCache = {};
const vaccineItemObject = {};
const vaccineItemNameObject = {};

function addVaccineItemCache(vaccineItem) {
    vaccineItemCache[vaccineItem.vaccineId] = vaccineItem;
}

function removeVaccineItemCache(vaccineId) {
    delete vaccineItemCache[vaccineId];
}

function isEmptyVaccineItemCache() {
    return Object.entries(vaccineItemCache).length === 0;
}

function getVaccineItemCache(vaccineId) {
    return vaccineItemCache[vaccineId];
}

function updateVaccineItemCache(oldVaccineId, data) {
    if (oldVaccineId == data.vaccineId) {
        vaccineItemCache[data.vaccineId] = data;
    } else {
        removeVaccineItemCache(oldVaccineId);
        addVaccineItemCache(data);
    }
}

function appendVaccineItemToFormData(formData) {
    let i = 0;
    for (let key in vaccineItemCache) {
        formData.append(`vaccine[item][${i}][id_medicine]`, vaccineItemCache[key].vaccineId);
        formData.append(`vaccine[item][${i}][id_signa]`, '');
        formData.append(`vaccine[item][${i}][id_unit]`, vaccineItemCache[key].unitId);
        formData.append(`vaccine[item][${i}][batch_no]`, vaccineItemCache[key].batchNo);
        formData.append(`vaccine[item][${i}][quantity]`, vaccineItemCache[key].quantity);
        i++;
    }
}

function getCachedVaccineItemByName(name) {
    const id = vaccineItemNameObject[name];
    return getCachedVaccineById(id);
}

function getCachedVaccineById(id) {
    return vaccineObject[id || '-'];
}

let timerVaccineItem = "";
function getVaccineItem(question, perPage = 10) {
    clearTimeout(timerVaccineItem);
    return new Promise((resolve, reject) => {
        if (question && vaccineItemObject[question]) {
            return resolve({
                total: 1,
                perPage,
                currentPage: 1,
                lastPage: 1,
                from: 1,
                to: 1,
                data: [vaccineItemObject[question]],
                error: false
            })
        }
        timerVaccineItem = setTimeout(() => {
            $.ajax({
                url: `{{ $api_url }}/{{ $lang }}/medicine?category=vaccine${filter_clinic}&q=${question}&per_page=${perPage}`,
                type: "GET",
                processData: false,
                contentType: false,
                success: function(data, textStatus, jqXHR) {
                    if (data && data.data) {
                        data.data.forEach(vaccineItem => {
                            vaccineItemObject[vaccineItem.id] = vaccineItem;
                            vaccineItemNameObject[vaccineItem.name] = vaccineItem.id;
                        });
                    }
                    return resolve(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    return reject(jqXHR);
                }
            })
        }, 2000);
    });
}

// autocomplete vaccine item
let selectedVaccineItem;
$('#form-vaccine-item input[name=vaccine_item_name]').autocomplete({
    source: async function(request, response) {
        let temp = [];
        try {
            if (request.term.length > 2) {
                loading_content("#form-vaccine-item #vaccine_item_name-group", "loading");
                const vaccineItemRequest = await getVaccineItem(request.term);
                loading_content("#form-vaccine-item #vaccine_item_name-group", "success");
                if (!vaccineItemRequest.error && vaccineItemRequest.data) {
                    return response(vaccineItemRequest.data.map(v => {
                        return {
                            ...v,
                            id: v.id,
                            name: v.name,
                            value: v.name,
                        }
                    }));
                }
            }
        } catch (error)  {
            loading_content("#form-vaccine-item #vaccine_item_name-group", "failed");
            response(temp);
        }
    },
    focus: function() {
        return false;
    },
    select: function(event, ui) {
        $('#form-vaccine-item input[name=vaccine_item_name]').val(ui.item.name);
        $('#form-vaccine-item input[name=vaccine_item_name]').data('id', ui.item.id);
        changeVaccineItem(ui.item);
        return false;
    }
});

var vaccineItemSelect = "";
function changeVaccineItem(vaccineItem) {
    resetVaccineItem();
    
    if (vaccineItem) {
        const {id, name, code, notes} = vaccineItem;
        vaccineItemSelect = vaccineItem;

        $("#form-vaccine-item input[name='vaccine_item_id']").val(id);
        $("#form-vaccine-item input[name='vaccine_item_name']").val(name);
        $("#form-vaccine-item input[name='vaccine_item_code']").val(code);
        if (vaccineItemSelect.unit.length > 0) {
            $("#save-vaccine-item").attr("disabled", false);
            vaccineItemSelect.unit.sort((a, b) => Number(a.weight) - Number(b.weight));
            $("#form-vaccine-item select[name='vaccine_item_unit']").append("<option value='' selected disabled>{{ trans('messages.choice_unit') }}</option>");
            for (let i = 0; i < vaccineItemSelect.unit.length; i++) {
                $("#form-vaccine-item select[name='vaccine_item_unit']").append("<option value="+vaccineItemSelect.unit[i].id+">"+vaccineItemSelect.unit[i].name+"</option>");
            }
        } else {
            resetVaccineItem();
            notif(false, '{{ trans("validation.no_unit")}}');
        }
    } else {
        resetVaccineItem();
        notif(false, '{{ trans("validation.no_unit")}}');
    }
}

function resetVaccineItem() {
    $("#form-vaccine-item input[name='vaccine_item_id']").val("");
    $("#form-vaccine-item input[name='vaccine_item_name']").val("");
    $("#form-vaccine-item input[name='vaccine_item_code']").val("");
    $("#form-vaccine-item input[name='vaccine_item_lot_no']").val("");
    $("#form-vaccine-item input[name='vaccine_item_quantity']").val("");
    $("#form-vaccine-item select[name='vaccine_item_unit']").select2("val", "");
    $("#form-vaccine-item select[name='vaccine_item_unit']").empty();
    $("#save-vaccine-item").attr("disabled", true);
}

$('#add-vaccine-item').on('hidden.bs.modal', function (e) {
    resetVaccineItem();
    
});

$('#add-vaccine-item').on('show.bs.modal', async function (e) {
    resetVaccineItem();
    const vaccineId = $(e.relatedTarget).attr('data-id');
    if (vaccineId) {
        const vaccineItem = getVaccineItemCache(vaccineId);
        const unit = vaccineItemObject[vaccineId].unit;
        if (unit.length > 0) {
            unit.sort((a, b) => Number(a.weight) - Number(b.weight));
            for (let i = 0; i < unit.length; i++) {
                $("#form-vaccine-item select[name='vaccine_item_unit']").append("<option value="+vaccineItemSelect.unit[i].id+">"+vaccineItemSelect.unit[i].name+"</option>");
            }
        }
        $("#form-vaccine-item input[name='vaccine_item_old_id']").val(vaccineItem.vaccineId);
        $("#form-vaccine-item input[name='vaccine_item_id']").val(vaccineItem.vaccineId);
        $("#form-vaccine-item input[name='vaccine_item_name']").val(vaccineItem.name);
        $("#form-vaccine-item input[name='vaccine_item_code']").val(vaccineItem.code);
        $("#form-vaccine-item input[name='vaccine_item_lot_no']").val(vaccineItem.batchNo);
        $("#form-vaccine-item input[name='vaccine_item_quantity']").val(vaccineItem.quantity);
        $("#form-vaccine-item select[name='vaccine_item_unit']").val(vaccineItem.unitId).trigger("change");
        $("#save-vaccine-item").attr("disabled", false);
    }
});

$("select[name='vaccine_item_unit']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}"
});

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    removeVaccineItem($section, $id);
})

function removeVaccineItem(section, vaccineId) {
    $(""+section+"").remove();
    removeVaccineItemCache(vaccineId);
    if (isEmptyVaccineItemCache()) {
        $("#form-add #data-vaccine-item").html("");
        $("#form-add #list-vaccine-item").addClass("hide");
    }
    $("#confirmDelete").modal("hide");
}

function processVaccineItem(oldVaccineId, vaccineId, name, code, batchNo, quantity, unitId) {
    const isUpdate = oldVaccineId;
    $("#form-add #list-vaccine-item").removeClass("hide");
    let action = '<a id="action-vaccine-item-delete" data-toggle="modal" data-target="#confirmDelete"  data-message=\'{{ trans("messages.info_delete") }}\' data-section="#vaccine-item-' + vaccineId + '" data-id="' + vaccineId + '" class="btn-table btn-red btn-right"><img src=\'{{ asset("assets/images/icons/action/delete.png") }}\' /> </a>'
        + '<a id="action-vaccine-item-edit" data-toggle="modal" data-target="#add-vaccine-item" data-id="' + vaccineId + '" class="btn-table btn-blue btn-right"><img src=\'{{ asset("assets/images/icons/action/edit.png") }}\' /> </a>';
    let section = '<td class="hide-md list-table-mini">' + name + '</td>'
        + '<td class="hide-md list-table-mini">' + quantity + '</td>'
        + '<td class="hide-md list-table-mini">' + batchNo + '</td>'
        + '<td>'
        + action
        + '</td>';
    if (isUpdate) {
        // here is to edit existing data
        if (oldVaccineId != vaccineId) {
            $("#vaccine-item-"+oldVaccineId+"").remove();
            $("#form-add #data-vaccine-item").append('<tr class="template_vaccine_item" id="vaccine-item-' + vaccineId + '">' + section + '</tr>');
        }
        $("#form-add #data-vaccine-item #vaccine-item-" + oldVaccineId).html(section);
        updateVaccineItemCache(oldVaccineId, {
            vaccineId: vaccineId,
            name: name,
            code: code,
            unitId: unitId,
            batchNo: batchNo,
            quantity: quantity
        });
    } else {
        // here is to add data
        $("#form-add #data-vaccine-item").append('<tr class="template_vaccine_item" id="vaccine-item-' + vaccineId + '">' + section + '</tr>');
        addVaccineItemCache({
            vaccineId: vaccineId,
            name: name,
            code: code,
            unitId: unitId,
            batchNo: batchNo,
            quantity: quantity
        });
    }
}

$("#add-vaccine-item").submit(function(event) {
    let oldId = $("#add-vaccine-item input[name='vaccine_item_old_id']").val();
    let id = $("#add-vaccine-item input[name='vaccine_item_id']").val();
    let name = $("#add-vaccine-item input[name='vaccine_item_name']").val();
    let code = $("#add-vaccine-item input[name='vaccine_item_code']").val();
    let batchNo = $("#add-vaccine-item input[name='vaccine_item_lot_no']").val();
    let quantity = $("#add-vaccine-item input[name='vaccine_item_quantity']").val();
    let unitId = $("#add-vaccine-item select[name='vaccine_item_unit'] option:selected").val();

    processVaccineItem(oldId, id, name, code, batchNo, quantity, unitId);
    notif(true, "{{ trans('validation.success_add_medicine') }}");
    $("#add-vaccine-item").modal("toggle");
    resetVaccineItem();
});

// END //



$("select[name='signa3']").select2({
    placeholder: "{{ trans('messages.one_day') }}"
});

$("select[name='signa3']").select2({
    placeholder: "{{ trans('messages.one_day') }}"
});

$("select[name='signa3']").select2({
    placeholder: "{{ trans('messages.one_day') }}"
});

$("select[name='unit_medicine']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}"
});

$("input[name=diagnosis]").on("keyup", function(e){
    if(e.keyCode === 40) {
        $("input[name=select_icd]").focus();
        $("input[name=select_icd]").click();
    }
});

function downClickTTV(event) {
    $("input[name='"+event.name+"']").keydown(function(e){
        if(e.keyCode !== 38 && e.keyCode !== 40) {
            return
        }else {
            $("input[name='"+event.name+"']").off("keydown")
            changeField(event, e.keyCode)
        }
    });
}

function changeField(event, keyCode) {
    var x = $("#panel-vital_sign").find("input");
    for(i = 0; i < x.length; i++) {
        if(event == x[i]) {
            if(keyCode == 38 && i > 0) {
                i--
                $(".input"+i+"").focus();
                $(".input"+i+"").click();
            }else if(keyCode == 40 && i < 6) {
                i++
                $(".input"+i+"").focus();
                $(".input"+i+"").click();
            }
            break;
        }
    }
}

var data_import = [];

function formatFileSize(bytes,decimalPoint) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
    dm = decimalPoint || 2,
    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

$(".title-import-file").click(function(){
    $("#images").click();
});

$("#images").on('change', function(e){
    var checked_size = false;
    if (e.target.files){
        $("#images-to-upload").html("");
        var files = e.target.files;
        $.each(files, function(i, file) {
            if (file.size > 10000000) {
                notif(false, '{{ trans("validation.failed_size_import")}}');
                checked_size = true;
                $.each(data_import, function(i, file){
                    template_result(i, file)
                });
            }else {
                data_import.push(file);
                checked_size = false;
            }
        });
        if (checked_size == false) {
            $.each(data_import, function(i, file){
                template_result(i, file)
            });
        }
    }
});

function deleted(index) {
    $("#images-to-upload").html("");
    data_import.splice(index, 1);
    $.each(data_import, function(i, file) {
        template_result(i, file);
    });
    checked = false;
}

function template_result(index, data) {
    var template = '<div class="detail-import">' +
                '<span class="font-weight-bold" style="margin-right: 10px; color: #1AB1E5;">' + data.name + '</span>' +
                '<span class="mr-3 font-weight-bold" style="margin-right: 10px">' + formatFileSize(data.size) + '</span>' +
                '<img src="{{ asset("assets/images/Delete.png") }}" onclick="deleted('+ index +')" />' +
            '</div>';
    $("#images-to-upload").append(template);
}

$("#close-illness").click(function(event){
    event.preventDefault();
    $("#cancel-illness").click();
});

$("#cancel-illness").click(function(event){
    event.preventDefault();
    $("#add-history").modal("hide");
    resetValidation('form-add #illness');
    $("#illness-group input[name=illness]").val("");
});

$("#add-illness").click(function(event) {
	event.preventDefault();
    var name_illness = $("#illness-group input[name=illness]").val();
    if (name_illness != ""){
        $("#add-illness").attr("disabled", true);
        $("#add-illness").addClass("loading");
        $("#add-illness span").removeClass("hide");
        addIllness(name_illness);
    }else {
        formValidate(false, ['form-add #illness','{{ trans("validation.empty_illness") }}', true]);
    }
});

function addIllness(illness){
    formData = new FormData();
    formData.append("indonesian_name", illness);
    formData.append("english_name", "");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/illness",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(response){
            var id = $('#riwayat').val() || [];
            if (!response.error) {
                $("#add-illness").attr("disabled", false);
                $("#add-illness").removeClass("loading");
                $("#add-illness span").addClass("hide");
                $("#cancel-illness").click();
                id.push(response.data);
                listIllness(id);
            }else {
                $("#add-illness").attr("disabled", false);
                $("#add-illness").removeClass("loading");
                $("#add-illness span").addClass("hide");
                formValidate(false, ['form-add #illness', response.message, true]);
            }
        }
    });
}

$("#riwayat").select2({
    placeholder: "{{ trans('messages.history_of_illness') }}",
    allowClear: true
});

$("#close-allergy").click(function(event){
    event.preventDefault();
    $("#cancel-allergy").click();
});

$("#cancel-allergy").click(function(event){
    event.preventDefault();
    $("#add-history-allergy").modal("hide");
    resetValidation('form-add #allergy');
    $("#allergy-group input[name=allergy]").val("");
});

$("#add-allergy").click(function(event) {
	event.preventDefault();

    const id_allergy_category = $("#allergy-category-group select[name=allergy-category]").val();
    const name_allergy = $("#allergy-group input[name=allergy]").val();
    if (!id_allergy_category) {
        formValidate(false, ['form-add #allergy-category','{{ trans("validation.empty_allergy_category") }}', true]);
    }
    if (!name_allergy) {
        formValidate(false, ['form-add #allergy','{{ trans("validation.empty_allergy") }}', true]);
    }

    if(id_allergy_category && name_allergy) {
        $("#add-allergy").attr("disabled", true);
        $("#add-allergy").addClass("loading");
        $("#add-allergy span").removeClass("hide");
        addAllergy(id_allergy_category, name_allergy);
    }
    
});

function addAllergy(id_allergy_category, name_allergy) {
    formData = new FormData();
    formData.append("indonesian_name", name_allergy);
    formData.append("english_name", "");
    formData.append("id_allergy_category", id_allergy_category);
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/allergies",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(response){
            var id = $('#riwayat-alergi').val() || [];
            if (!response.error) {
                $("#add-allergy").attr("disabled", false);
                $("#add-allergy").removeClass("loading");
                $("#add-allergy span").addClass("hide");
                $("#cancel-allergy").click();
                id.push(response.data);
                listAllergies(id);
            }else {
                $("#add-allergy").attr("disabled", false);
                $("#add-allergy").removeClass("loading");
                $("#add-allergy span").addClass("hide");
                formValidate(false, ['form-add #allergy', response.message, true]);
            }
        },
        error: function (xhr, statusText) {
            $("#add-allergy").attr("disabled", false);
            $("#add-allergy").removeClass("loading");
            $("#add-allergy span").addClass("hide");
            formValidate(false, ['form-add #allergy', statusText, true]);
        }
    });
}

$("#riwayat-alergi").select2({
    placeholder: "{{ trans('messages.history_of_allergy') }}",
    allowClear: true
});

function listIllness(val) {
    loading_content("#riwayat-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/illness",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data_illness){
            if (!data_illness.error) {
                var data = data_illness.data;
                $("#riwayat").html("");
                loading_content("#riwayat-group", "success");
                for (var i = 0; i < data.length; i++) {
                    if (val && val.length > 0) {
                        var selected = "";
                        for (var j = 0; j < val.length; j++) {
                            if(data[i].id === val[j].id || data[i].id === val[j]) {
                                selected = "selected='selected'";
                            }
                        }
                    }
                    $("#riwayat").append("<option value='"+data[i].id+"' "+selected+">"+data[i].name+"</option>")
                }
                $("#riwayat").select2({
                    placeholder: "{{ trans ('messages.history_of_illness') }}",
                    allowClear: true
                });
            } else {
                loading_content("#riwayat-group", "failed");
                $("#riwayat-group #loading-content").click(function(){ listIllness(val); });
            }
        },
        error: function(){
            loading_content("#riwayat-group", "failed");
            $("#riwayat-group #loading-content").click(function(){ listIllness(val); });
        }
    });
}

function listAllergyCategories() {
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/allergy_categories",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
            if (!data.error) {
                $("#allergy-category").html("");
                $("#allergy-category").append(`<option value="" disabled selected>{{ trans('messages.select_allergy_category') }}</option>`);
                data.data.forEach(value => {
                    let selected = '';
                    $("#allergy-category").append("<option value='"+value.id+"' "+selected+">"+value.name+"</option>");
                });
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            notif(false, textStatus);
        }
    });
}

function listAllergies(val) {
    loading_content("#riwayat-alergi-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/allergies",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data, textStatus, jqXHR){
            if (!data.error) {
                loading_content("#riwayat-alergi-group", "success");
                $("#riwayat-alergi").html("");
                data.data.forEach(allergy => {
                    let selected = '';
                    val && val.forEach(value => {
                        if(value.id === allergy.id || allergy.id === value) {
                            selected = "selected='selected'";
                        }
                    });
                    $("#riwayat-alergi").append("<option value='"+allergy.id+"' "+selected+">"+allergy.name+" ("+allergy.name_category+")"+"</option>")
                });

                $("#riwayat-alergi").select2({
                    placeholder: "{{ trans('messages.history_of_allergy') }}",
                    allowClear: true
                });
            } else {
                loading_content("#riwayat-alergi-group", "failed");
                $("#riwayat-alergi-group #loading-content").click(function(){ listAllergies(val); });
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            loading_content("#riwayat-alergi-group", "failed");
            $("#riwayat-alergi-group #loading-content").click(function(){ listAllergies(val); });
        }
    });
}

var isBPJS = false;

function getPatient(clinic, val) {
    
    loading_content("#riwayat-group", "loading");
    loading_content("#riwayat-alergi-group", "loading");
    resetObjective();
    resetSubjective();
    resetService();
    resetMedicine();
    resetAssessment();
    resetPlan();
    
    data_import = [];
    var queryParam = '';
    if ("{{ $level == 'doctor' }}") {
        queryParam = "?id_clinic={{ $id_clinic }}";
    }
    if (val!="") {
        $("#wrapper-loading-patient").removeClass("hide");
        $('#form-add #loading-detail_patient').removeClass('hide');
        $('#form-add #loading-vital_sign').removeClass('hide');
        $('#form-add #reload-detail_patient').addClass('hide');
        $('#form-add #panel-detail_patient').addClass('hide');
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/patient/" + val + queryParam,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    listIllness(data.illnesses_history);
                    listHistory(val);
                    listAllergies(data.allergies_history || '');
                    listVaccineHistory(data.vaccine_history);

                    var typeKunjungan = $("#form-add select[name='type_kunjungan']").val();

                    $("#wrapper-loading-patient").addClass("hide");
                    $('#form-add #loading-detail_patient').addClass('hide');
                    $('#form-add #loading-vital_sign').addClass('hide');
                    $('#form-add #panel-detail_patient').removeClass('hide');
                    if ("{{ $pcare }}") {
                        if (data.insurance != null && data.insurance.name.toLowerCase().includes('bpjs')) {
                            isBPJS = true
                        } else {
                            isBPJS = false
                        }
                    } else {
                        if (data.insurance != null && data.insurance.name.toLowerCase().includes('bpjs')) {
                            notif(false, '{{ trans("validation.invalid_pcare_credential") }}');
                            isBPJS = true
                        } else {
                            isBPJS = false
                        }
                    }
                    if (isBPJS == true){
                        $('#form-diagnostic #ppk-parent').removeClass('hide');
                        typeKunjungan == 'outpatient' ? getStatusHome("false") : getStatusHome("true");
                    } else {
                        $('#form-diagnostic #ppk-parent').addClass('hide');
                        typeKunjungan == 'outpatient' ? getStatusHome("false") : getStatusHome("true");
                    }
                    listConsciousness('');
                    $("#form-add input[name='mrn']").val(data.mrn);
                    $("#form-add input[name='card_number']").val(data.card_number);
                    $("#form-add #name").html(data.name);
                    $("#form-add #birth_date").html(getFormatDate(data.birth_date)+" ("+getAge(data.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                    $("#form-add #gender").html(getGender(data.gender));
                    var smoke = "";
                    if (data.smoke==1) {
                        smoke = '{{ trans("messages.smoking") }}';
                    } else {
                        smoke = '{{ trans("messages.no_smoking") }}';
                    }
                    $("#form-add #smoke").html(smoke);
                    var active = "";
                    if (data.active==1) {
                        active = '{{ trans("messages.active") }}';
                    } else {
                        active = '{{ trans("messages.inactive") }}';
                    }
                    $("#form-add #active").html(active);
                    $("#form-add #blood_type").html(checkNull(data.blood_type));
                    var no_identity = "-";
                    if (data.id_number!="" && data.id_number!=null) {
                        no_identity = data.id_number+" ("+data.id_type+")";
                    }
                    $("#form-add #no_identity").html(no_identity);
                    $("#form-add #address").html(nullToEmpty(data.address)+' '+nullToEmpty(data.village.name)+' '+nullToEmpty(data.district.name)+' '+nullToEmpty(data.regency.name)+' '+nullToEmpty(data.province.name)+' '+nullToEmpty(data.country.name));
                    $("#form-add #email").html(checkNull(data.email));
                    $("#form-add #phone").html("+"+data.phone_code+""+data.phone);
                    try {
                        $("#form-add #patient_category").html(data.patient_category.name);
                    } catch(err) {}
                    var insurance = "";
                    if (data.insurance != null){
                        insurance = ' - ' + data.insurance.name;
                    }
                    var card_number = "";
                    if (data.card_number != null){
                        card_number = ' - ' + data.card_number;
                    }
                    $("#form-add #financing").html(getFinancing(data.financing) + insurance + card_number);
                } else {
                    $("#wrapper-loading-patient").removeClass("hide");
                    $('#form-add #loading-detail_patient').addClass('hide');
                    $('#form-add #reload-detail_patient').removeClass('hide');

                    $("#form-add #reload-detail_patient .reload").click(function(){ getPatient(clinic, val); });
                }
            },
            error: function(){
                $('#form-add #loading-detail_patient').addClass('hide');
                $('#form-add #reload-detail_patient').removeClass('hide');
                $("#form-add #reload-detail_patient .reload").click(function(){ getPatient(clinic, val); });
            }
        });
    } else {
        resetData();
    }
}

$("#riwayat").select2({
    placeholder: "{{ trans('messages.history_of_illness') }}",
    allowClear: true
});

$('#form-add input[name=status_tacc]').click(function() {  
    var hiddenId = $(this).attr("data-trigger");
    $(".group-tacc").not("#" + hiddenId).addClass('hide');
    $("#" + hiddenId).removeClass('hide');
});

$('#datepicker_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('.datepicker_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#form-add select[name=schedule]').on('change', function() {
    var schedule = this.value;
    if (schedule != "" && schedule != null) {
        getSchedule(''+schedule+'', false)
    } else {
        $("#form-add select[name=polyclinic]").val('').trigger("change");
        $("#form-add select[name=doctor]").val('').trigger("change");
        $("#form-add select[name=name]").val('').trigger("change");
        onChangeService();
        $('textarea[name=drug-therapy]').val("")
    }
});

$("#form-add select[name='schedule']").select2({
    placeholder: "{{ trans('messages.no_schedule') }}"
});

$("#form-diagnostic select[name='ppk']").select2({
    placeholder: "{{ trans('messages.select_ppk') }}"
});

$("#form-add select[name='blood_type']").select2({
    placeholder: "{{ trans('messages.blood_type') }}"
});

$("#form-add select[name='id_type']").select2({
    placeholder: "{{ trans('messages.id_type') }}"
});

$("#form-add select[name='district']").select2({
    placeholder: "{{ trans('messages.district') }}"
});

$("#form-add select[name='village']").select2({
    placeholder: "{{ trans('messages.village') }}"
});

$("#form-add select[name='regency']").select2({
    placeholder: "{{ trans('messages.regency') }}"
});

$("#form-add select[name='province']").select2({
    placeholder: "{{ trans('messages.province') }}"
});

$("#form-add select[name='country']").select2({
    placeholder: "{{ trans('messages.country') }}"
});

$("#form-add select[name='patient_category']").select2({
    placeholder: "{{ trans('messages.patient_category') }}"
});

$("#form-add select[name='consciousness']").select2({
    placeholder: "{{ trans('messages.select_awareness_status') }}"
});

$("#form-add select[name='status']").select2({
    placeholder: "{{ trans('messages.select_return_status') }}"
});

$("#form-add select[name='prognosis']").select2({
    placeholder: "{{ trans('messages.select_prognosis') }}"
});

$("#form-add select[name='financing']").select2({
    placeholder: "{{ trans('messages.financing') }}"
});

var date_kunjungan = "";
var get_id_doctor = "";
var get_id_patient = "";
var get_id_polyclinic = "";

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

function listSchedule(scheduleId) {
    loading_content("#form-add #schedule-group", "loading");
    var queryParams = ("{{ $level == 'doctor' }}") ? "&id_polyclinic={{ $id_polyclinic }}&id_clinic={{ $id_clinic }}" : "";
    
    if (scheduleId != "") {
        var url = "{{ $api_url }}/{{ $lang }}/schedule/"+scheduleId;
    } else {
        var url = "{{ $api_url }}/{{ $lang }}/schedule?to_date="+convertDate(dateNow)+"&status=10&order=desc&medic=true" + queryParams;
    }
    $.ajax({
        url: url,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataSchedule){
            if(!dataSchedule.error) {
                loading_content("#form-add #schedule-group", "success");
                var item = [];
                $("#form-add select[name='schedule']").html("<option value=''></option> ");
                if (scheduleId != "") {
                    item[0] = dataSchedule;
                } else {
                    item = dataSchedule.data;
                }
                
                for (var i = 0; i < item.length; i++) {
                    try {
                    $("#form-add select[name='schedule']").append("<option value='"+item[i].id+"'>"+item[i].patient.name+" ("+item[i].doctor.name+" - "+item[i].polyclinic.name+" - "+unconvertDate(item[i].date)+" "+ (item[i].start_time != null ? item[i].start_time : "") +")</option>");
                    } catch(err) {}
                }
                $("#form-add select[name='schedule']").select2({
                    placeholder: "{{ trans('messages.select_schedule') }}",
                    allowClear: false
                });
                if (scheduleId != "") {
                    $("#form-add select[name=schedule]").val(scheduleId).trigger("change");
                }
            } else {
                loading_content("#form-add #schedule-group", "failed");

                $("#form-add #schedule-group #loading-content").click(function(){ listSchedule(scheduleId); });
            }
        },
        error: function(){
            loading_content("#form-add #schedule-group", "failed");
            $("#form-add #schedule-group #loading-content").click(function(){ listSchedule(scheduleId); });
        }
    });
}

function listPoly(polyclinic) {
    $("#form-add select[name='polyclinic']").prop("disabled", true);
    $("#form-add select[name='polyclinic']").html("<option value=''></option> ");
    $("#form-add select[name='polyclinic']").append("<option value='"+polyclinic.id+"'>"+polyclinic.name+"</option>");
    $("#form-add select[name=polyclinic]").val(polyclinic.id).trigger("change");
}

function listConsciousness(val) {
    loading_content("#form-add #consciousness-group", "loading");
    if (!isBPJS) {
        loading_content("#form-add #consciousness-group", "success");
        $("#form-add select[name='consciousness']").html("<option value=''></option> ");
        $("#form-add select[name='consciousness']").append("<option value='compos mentis'>Compos mentis</option>");
        $("#form-add select[name='consciousness']").append("<option value='somnolence'>Somnolence</option>");
        $("#form-add select[name='consciousness']").append("<option value='sopor'>Sopor</option>");
        $("#form-add select[name='consciousness']").append("<option value='coma'>Coma</option>");
        $("#form-add select[name='consciousness']").select2({
            placeholder: "Pilih status kesadaran",
            allowClear: false
        });
        
        if (val != "") {
            $("#form-add select[name=consciousness]").val(val).trigger("change");
        }

        return 0;
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-kesadaran",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                loading_content("#form-add #consciousness-group", "success");
                var item = data.list;
                $("#form-add select[name='consciousness']").html("<option value=''></option> ");

                    for (var i = 0; i < item.length; i++) {
                        $("#form-add select[name='consciousness']").append("<option value='"+item[i].kdSadar+"'>"+item[i].nmSadar+"</option>");
                    }
                    $("#form-add select[name='consciousness']").select2({
                        placeholder: "Pilih status kesadaran",
                        allowClear: false
                    });

                if (val != "") {
                    $("#form-add select[name=consciousness]").val(val).trigger("change");
                }
            } else {
                loading_content("#form-add #consciousness-group", "failed");
                $("#form-add #consciousness-group #loading-content").click(function(){ listConsciousness(val); });
            }
        },
        error: function(){
            loading_content("#form-add #consciousness-group", "failed");
            $("#form-add #consciousness-group #loading-content").click(function(){ listConsciousness(val); });
        }
    });
}

function listTACC(val) {
    loading_content("#form-add #factor-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-tacc",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                loading_content("#form-add #factor-group", "success");
                var item = data.list;
                $("#form-add select[name='faktor_rujuk']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='faktor_rujuk']").append("<option value='"+item[i].kdTacc+"'>"+item[i].nmTacc+"</option>");
                }
                $("#form-add select[name='faktor_rujuk']").select2({
                    placeholder: "{{ trans('messages.select_awareness_status') }}",
                    allowClear: false
                });
                if (val != "") {
                    $("#form-add select[name=faktor_rujuk]").val(val).trigger("change");
                }
            } else {
                loading_content("#form-add #factor-group", "failed");
                $("#form-add #factor-group #loading-content").click(function(){ listConsciousness(val); });
            }
        },
        error: function(){
            loading_content("#form-add #factor-group", "failed");
            $("#form-add #factor-group #loading-content").click(function(){ listConsciousness(val); });
        }
    });
}

/** Prognosis */
listPrognisis('bonam');

function listPrognisis(val) {
    const prognosis = [
        {
            value: 'sanam',
            text: '{{trans("messages.sanam")}}'
        },
        {
            value: 'bonam',
            text: '{{trans("messages.bonam")}}'
        },
        {
            value: 'malam',
            text: '{{trans("messages.malam")}}'
        },
        {
            value: 'dubia-ad-sanam-bolam',
            text: '{{trans("messages.dubia-ad-sanam-bolam")}}'
        },
        {
            value: 'dubia-ad-malam',
            text: '{{trans("messages.dubia-ad-malam")}}'
        }
    ];

    $("#form-add select[name='prognosis']").prop("disabled", false);
    $("#form-add select[name='prognosis']").html("<option value=''></option> ");
    prognosis.forEach(v => {
        $("#form-add select[name='prognosis']").append(`<option value='${v.value}' ${val === v.value ? 'selected' : ''}>${v.text}</option>`);
    });

    $("#form-add select[name='prognosis']").select2({
        placeholder: "{{ trans('messages.select_prognosis') }}",
        allowClear: false
    });
}

function getStatusHome(val) {
    loading_content("#form-add #status-group", "loading");

    if (!isBPJS) {
        loading_content("#form-add #status-group", "success");
        $("#form-add select[name='status']").prop("disabled", false);
        $("#form-add select[name='status']").html("<option value=''></option> ");
        $("#form-add select[name='status']").append("<option value='berobat jalan'>{{ trans('messages.get_treatment') }}</option>");
        $("#form-add select[name='status']").append("<option value='sehat'>{{ trans('messages.get_well') }}</option>");
        $("#form-add select[name='status']").append("<option value='rujuk'>{{ trans('messages.refer') }}</option>");
        $("#form-add select[name='status']").append("<option value='meninggal'>{{ trans('messages.died') }}</option>");
        $("#form-add select[name='status']").select2({
            placeholder: "{{ trans('messages.select_return_status') }}",
            allowClear: false
        });

        if (val != "") {
            $("#form-add select[name=status]").val(val).trigger("change");
        }

        return 0;
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-status-pulang/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if (!data.error) {
                loading_content("#form-add #status-group", "success");
                var item = data.list;
                $("#form-add select[name='status']").prop("disabled", false);
                $("#form-add select[name='status']").html("<option value=''></option> ");

                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='status']").append("<option value='"+item[i].kdStatusPulang+"'>"+item[i].nmStatusPulang+"</option>");
                }

                $("#form-add select[name='status']").select2({
                    placeholder: "{{ trans('messages.select_return_status') }}",
                    allowClear: false
                });
                
                if (val != "") {
                    $("#form-add select[name=status]").val(val).trigger("change");
                }
            } else {
                loading_content("#form-add #status-group", "failed");
                $("#form-add #status-group #loading-content").click(function(){ listConsciousness(val); });
            }
        },
        error: function(){
            loading_content("#form-add #status-group", "failed");
            $("#form-add #status-group #loading-content").click(function(){ listConsciousness(val); });
        }
    });
}

function getReason(val, text) {
    loading_content("#form-add #reason-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-type-tacc/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if (!data.error) {
                loading_content("#form-add #reason-group", "success");
                var item = data.list;
                $("#form-add select[name='reason']").prop("disabled", false);
                $("#form-add select[name='reason']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='reason']").append("<option value='"+item[i]+"'>"+item[i]+"</option>");
                }
                $("#form-add select[name='reason']").select2({
                    placeholder: "Pilih "+text,
                    allowClear: false
                });
                if (val != "") {
                    $("#form-add select[name=reason]").val(val).trigger("change");
                }
            } else {
                loading_content("#form-add #reason-group", "failed");
                $("#form-add #reason-group #loading-content").click(function(){ listConsciousness(val); });
            }
        },
        error: function(){
            loading_content("#form-add #reason-group", "failed");
            $("#form-add #reason-group #loading-content").click(function(){ listConsciousness(val); });
        }
    });
}

function listDoctor(doctor) {
    $("#form-add select[name=doctor]").prop("disabled", true);
    $("#form-add select[name=doctor]").append("<option value=''>{{ trans('messages.select_doctor') }}</option>");
    $("#form-add select[name=doctor]").append("<option value='"+doctor.id+"'>"+doctor.name+"</option>");
    $("#form-add select[name=doctor]").val(doctor.id).trigger("change");
}

$('#form-add select[name=faktor_rujuk]').on('change', function() {
    var value = this.value;
    var text = $('#form-add select[name=faktor_rujuk] option:selected').text();;
    if(value != "" && value != null) {
        if(value != "3" && value != "4"){
            $("#form-add #text-alasan").html(text);
            $("#form-add #reason-group").removeClass("hide");
            $("#form-add #reason_complication-group").addClass("hide");
            $("#form-add #list-complicated").addClass("hide");
            getReason(value, text)
        }else{
            numICDReason = 0;
            $("#form-add #data-complicated").html("");
            $("#form-add #text-alasan_complication").html("{{ trans('messages.icd') }}");
            $("#form-add #reason-group").addClass("hide");
            $("#form-add #reason_complication-group").removeClass("hide");
            if(numICDReason == 0) {
                $("#form-add #list-complicated").addClass("hide");
            }else{
                $("#form-add #list-complicated").removeClass("hide");
            }
        }
    }else{
        $("#form-add select[name=reason]").val('').trigger("change");
    }
});

function listPatient(patient) {
    $("#form-add select[name=name]").html("");
    $("#form-add select[name=name]").prop("disabled", true);
    $("#form-add select[name=name]").append("<option value=''>{{ trans('messages.select_patient') }}</option>");
    $("#form-add select[name=name]").append("<option value='"+patient.id+"'>"+patient.name+"</option>");
    $("#form-add select[name=name]").val(patient.id).trigger("change");
}

function getSchedule(val, loading) {
    if (loading == true) {
        $('#loading').removeClass('hide');
        $("#reload").addClass("hide");
        $('#wrapper-patient').addClass('hide');
    }
    loading_content('#form-add #name-group', "loading");
    loading_content('#form-add #polyclinic-group', "loading");
    loading_content('#form-add #doctor-group', "loading");
    resetValidation('form-add #schedule','form-add #clinic','form-add #name','form-add #polyclinic','form-add #doctor');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+val+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){            
            if (!data.error) {
                get_id_doctor = data.id_doctor;
                date_kunjungan = data.date;
                $("#form-add select[name='type_kunjungan']").val(data.type_kunjungan).trigger("change");
                if (data.type_kunjungan == 'inpatient'){
                    $("#form-add #date_home-group").removeClass("hide");
                }else{
                    $("#form-add #date_home-group").addClass("hide");
                }
                loading_content('#form-add #name-group', "success");
                loading_content('#form-add #polyclinic-group', "success");
                loading_content('#form-add #doctor-group', "success");
                listPatient(data.patient);
                listPoly(data.polyclinic);
                listDoctor(data.doctor);
                $('#loading').addClass('hide');
                $('#wrapper-patient').removeClass('hide');
            } else {
                $('#loading').addClass('hide');
                $("#reload").removeClass("hide");
                $('#wrapper-patient').addClass('hide');
                $("#reload .reload").click(function(){ getSchedule(val, loading = true); });
            }
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");
            $('#wrapper-patient').addClass('hide');
            $("#reload .reload").click(function(){ getSchedule(val, loading = true); });
        }
    });
}

@if(!empty($id_schedule))
    listSchedule('{{ $id_schedule }}');
    getSchedule('{{ $id_schedule }}', true);
    listTACC('');
@else
    listSchedule('');
    listTACC('');
@endif

$("#link-add-patient").attr("onclick","loading($(this).attr('href'));");

function resetData() {
    $("#form-add input[name='mrn']").val("");
    $("#form-add input[name='birth_date']").val("");
    $("#form-add input[name='age']").val("");
    $('#form-add input[name="gender"]:checked').each(function(){
        $(this).prop('checked', false); 
    });
    $('#form-add input[name="smoke"]:checked').each(function(){
        $(this).prop('checked', false); 
    });
    $("#form-add select[name='blood_type']").val("").trigger("change");
    $("#form-add select[name='id_type']").val("").trigger("change");
    $("#form-add input[name='identity']").val("");
    $("#form-add textarea[name='address']").val("");
    $("#form-add input[name='postal_code']").val("");
    $("#form-add select[name='district']").val("").trigger("change");
    $("#form-add select[name='village']").val("").trigger("change");
    $("#form-add select[name='regency']").val("").trigger("change");
    $("#form-add select[name='province']").val("").trigger("change");
    $("#form-add select[name='country']").val("").trigger("change");
    $("#form-add input[name='email']").val("");
    $("#form-add input[name='phone']").val("");
    $("#form-add select[name='patient_category']").val("").trigger("change");
    $("#form-add select[name='financing']").val("").trigger("change");
    $("#form-add #title-vital_sign").html("<th></th>");
    $("#form-add #data-sistole").html("<td class='middle'>{{ trans('messages.sistole') }}</td>");
    $("#form-add #data-diastole").html("<td class='middle'>{{ trans('messages.diastole') }}</td>");
    $("#form-add #data-pulse").html("<td class='middle'>{{ trans('messages.pulse') }}</td>");
    $("#form-add #data-respiratory_frequency").html("<td class='middle'>{{ trans('messages.respiratory_frequency') }}</td>");
    $("#form-add #data-body_temperature").html("<td class='middle'>{{ trans('messages.body_temperature') }}</td>");
    $("#form-add #data-height").html("<td class='middle'>{{ trans('messages.height') }}</td>");  
    $("#form-add #data-head").html("<td class='middle'>{{ trans('messages.head_circumference') }}</td>");          
    $("#form-add #data-weight").html("<td class='middle'>{{ trans('messages.weight') }}</td>");
    $("#history").html("<tr><td align='center' colspan='3'>{{ trans('messages.no_result') }}</td></tr>");
}

function resetAdd() {
    $("#form-add select[name='schedule']").val("").trigger("change");
    $("#form-add select[name='polyclinic']").val("").trigger("change");
    $("#form-add select[name='doctor']").val("").trigger("change");
    $("#form-add select[name='name']").val("").trigger("change");
    $("#form-add input[name='select_icd']").val("");
    $("#form-add input[name='reason_complication']").val("");
    $("#form-add input[name='reason_notacc']").val("");
    $("#form-add input[name='anamnese']").val("");
    $("#form-add input[name='complaint']").val("");
    $("#form-add input[name='diagnosis']").val("");
    $("#form-add #list-icd").addClass("hide");
    $("#form-add #data-icd").html("");
    $("#form-add #list-complicated").addClass("hide");
    $("#form-add #data-complicated").html("");
    $("#form-add #list-medicine").addClass("hide");
    $("#form-add #data-medicine").html("");
    $("#form-add #list-vaccine-item").addClass("hide");
    $("#form-add #data-vaccine-item").html("");
    $("#form-add #list-file").html("");
    $("#form-add #section_detail-patient").collapse("hide");
    $("#form-add #section_patient-history").collapse("hide");
    $("#form-add #section_diagnosis").collapse("hide");
    $("#form-add #section_vital-sign").collapse("hide");
    $("#form-add #section_medicine").collapse("hide"); 
    $("#form-add #section_file").collapse("hide"); 
    resetData();
}

function listVitalSign(val) {
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/vital-sign/patient/"+val+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataVitalSign){
            if (!dataVitalSign.error) {
                $('#form-add #loading-history').addClass('hide');
                $('#form-add #reload-history').addClass('hide');
                $('#form-add #wrapper-objective').removeClass('hide');
                $("#form-add #title-vital_sign").html("<th>{{ trans('messages.vital_sign') }}</th>");  
                $("#form-add #data-sistole").html("<td class='middle'>Sistole (mmHg)</td>");
                $("#form-add #data-diastole").html("<td class='middle'>Diastole (mmHg)</td>");
                $("#form-add #data-pulse").html("<td class='middle'>{{ trans('messages.pulse') }}</td>");
                $("#form-add #data-respiratory_frequency").html("<td class='middle'>{{ trans('messages.respiratory_frequency') }}</td>");
                $("#form-add #data-body_temperature").html("<td class='middle'>{{ trans('messages.body_temperature') }}</td>");
                $("#form-add #data-height").html("<td class='middle'>{{ trans('messages.height') }}</td>");
                $("#form-add #data-head").html("<td class='middle'>{{ trans('messages.head_circumference') }}</td>");
                $("#form-add #data-weight").html("<td class='middle'>{{ trans('messages.weight') }}</td>");
                $("#form-add #data-abdomen").html("<td class='middle'>{{ trans('messages.abdominal_circumference') }}</td>");
                $("#form-add #data-imt").html("<td class='middle'>IMT</td>");
                
                item = dataVitalSign.data;
                var date = new Date();
                var getMonth = ((date.getMonth()+1)+100).toString().slice(-2);
                var getDate = date.getFullYear()+"-"+getMonth+"-"+(date.getDate()+100).toString().slice(-2);
                var today = false;

                const numberKey = ['blood_sistole', 'blood_diastole', 'pulse', 'respiratory_frequency', 'body_temperature', 'height', 'weight', 'abdomen', 'imt', 'head'];
                const ttvObject = item && item.reduce((ttv, current, index) => {
                    delete current.patient;

                    const selectedTTV = ttv[current.date] || {};

                    Object.keys(current).forEach(key => {
                        if(numberKey.includes(key)){
                            selectedTTV[key] = current[key] || selectedTTV[key] || 0;
                            selectedTTV[key] = isNaN(selectedTTV[key]) ? 0.0 : selectedTTV[key];
                        } else {
                            selectedTTV[key] =  current[key] || selectedTTV[key] || null;
                        }
                    });

                    ttv[current.date] = selectedTTV;

                    return ttv;
                }, {});

                item = Object.keys(ttvObject).sort().map(key => {
                    return ttvObject[key];
                });
                
                for (var i = 0; i < item.length; i++) {
                    let imt = item[i].imt || checkZero(calcIMT(item[i].weight, item[i].height));
                    imt = !isNaN(imt) ? parseFloat(imt) : 0.0;
                    const imtStatus = calcIMTStatus(imt);

                    if (item[i].date == getDate) {
                        today = true;
                        $("#form-add #title-vital_sign").append("<th>{{ trans('messages.today') }}</th>");
                        $("#form-add #data-sistole").append("<td><input name='blood_sistole' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].blood_sistole)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-diastole").append("<td><input name='blood_diastole' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].blood_diastole)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-pulse").append("<td><input name='pulse' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].pulse)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-respiratory_frequency").append("<td><input name='respiratory_frequency' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].respiratory_frequency)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-body_temperature").append("<td><input name='body_temperature' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].body_temperature)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-height").append("<td><input name='height' class='form-control short-input' onkeydown='number(event)' onchange='onChangeIMT(event)' id='data-height-"+i+"' value='"+checkZero(item[i].height)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-head").append("<td><input name='head' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].head)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-weight").append("<td><input name='weight' class='form-control short-input' onkeydown='number(event)' onchange='onChangeIMT(event)' id='data-weight-"+i+"' value='"+checkZero(item[i].weight)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-abdomen").append("<td><input name='abdomen' class='form-control short-input' onkeydown='number(event)' value='"+checkZero(item[i].abdomen)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #data-imt").append("<td><input name='imt' disabled class='form-control short-input' onkeydown='number(event)' id='data-imt-"+i+"' data-value='"+(!isNaN(imt) ? imt : imt)+"' value='"+(!isNaN(imt) ? imt.toFixed(1) : imt)+"' autocomplete='off' placeholder='0' maxLength='5' onclick='downClickTTV(this)' /></td>");
                        $("#form-add #complaint").val(dataVitalSign.data[dataVitalSign.data.length -1].complaint);
                    } else {
                        $("#form-add #title-vital_sign").append("<th align='center'>"+getShortFormatDate(item[i].date)+"</th>");  
                        $("#form-add #data-sistole").append("<td align='center' class='middle'>"+checkNull(item[i].blood_sistole)+"</td>");
                        $("#form-add #data-diastole").append("<td align='center' class='middle'>"+checkNull(item[i].blood_diastole)+"</td>");
                        $("#form-add #data-pulse").append("<td align='center' class='middle'>"+checkNull(item[i].pulse)+"</td>");
                        $("#form-add #data-respiratory_frequency").append("<td align='center' class='middle'>"+checkNull(item[i].respiratory_frequency)+"</td>");
                        $("#form-add #data-body_temperature").append("<td align='center' class='middle'>"+checkNull(item[i].body_temperature)+"</td>");
                        $("#form-add #data-height").append("<td align='center' class='middle'>"+checkNull(item[i].height)+"</td>");
                        $("#form-add #data-head").append("<td align='center' class='middle'>"+checkNull(item[i].head)+"</td>");
                        $("#form-add #data-weight").append("<td align='center' class='middle'>"+checkNull(item[i].weight)+"</td>"); 
                        $("#form-add #data-abdomen").append("<td align='center' class='middle'>"+checkNull(item[i].abdomen)+"</td>");
                        $("#form-add #data-imt").append("<td align='center' class='middle'>"+(!isNaN(imt) ? imt.toFixed(1) : imt)+"</td>");
                    }
                }
                if (today == false) {
                    $("#form-add #title-vital_sign").append("<th>{{ trans('messages.today') }}</th>");
                    $("#form-add #data-sistole").append("<td><input name='blood_sistole' class='form-control short-input input0' onkeydown='number(event)' autocomplete='off' placeholder='0' id='sistole' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-diastole").append("<td><input name='blood_diastole' class='form-control short-input input1' onkeydown='number(event)' autocomplete='off' placeholder='0' id='diastole' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-pulse").append("<td><input name='pulse' class='form-control short-input input2' onkeydown='number(event)' autocomplete='off' placeholder='0' id='pulse' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-respiratory_frequency").append("<td><input name='respiratory_frequency' class='form-control short-input input3' onkeydown='number(event)' autocomplete='off' placeholder='0' id='respiratory' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-body_temperature").append("<td><input name='body_temperature' class='form-control short-input input4' onkeydown='number(event)' autocomplete='off' placeholder='0' id='body_temp' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-height").append("<td><input name='height' class='form-control short-input input5' onkeydown='number(event)' onchange='onChangeIMT(event)' autocomplete='off' placeholder='0' id='height' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-head").append("<td><input name='head' class='form-control short-input input9' onkeydown='number(event)' autocomplete='off' placeholder='0' id='head' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-weight").append("<td><input name='weight' class='form-control short-input input6' onkeydown='number(event)' onchange='onChangeIMT(event)' autocomplete='off' placeholder='0' id='weight' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-abdomen").append("<td><input name='abdomen' class='form-control short-input input7' onkeydown='number(event)' autocomplete='off' placeholder='0' id='abdomen' maxLength='5' onclick='downClickTTV(this)' /></td>");
                    $("#form-add #data-imt").append("<td><input name='imt' disabled class='form-control short-input input8' onkeydown='number(event)' autocomplete='off' placeholder='0' id='imt' maxLength='5' onclick='downClickTTV(this)' /></td>");
                }
            } else {
                $('#form-add #loading-history').addClass('hide');
                $('#form-add #reload-history').removeClass('hide');
                $('#form-add #wrapper-objective').addClass('hide');
                $("#form-add #reload-history .reload").click(function(){ listHistory(val); });
            }
        },
        error: function(){
            $('#form-add #loading-history').addClass('hide');
            $('#form-add #reload-history').removeClass('hide');
            $('#form-add #wrapper-objective').addClass('hide');
            $("#form-add #reload-history .reload").click(function(){ listHistory(val); });
        }
    });
}

function calcIMT(weight, height) {
    if(!isNaN(height) && !isNaN(weight) && height && weight) {

        return weight / Math.pow(height * 0.01, 2);
    }

    return 0.0;
}

function calcIMTStatus(imt) {
    if(!imt) {
        return {
            text: '-',
            color: '#000000'
        }
    }

    if (imt < 17) {
        return {
            text: 'Sangat kurus',
            color: '#DB0062',
        }
    }

    if (imt <= 18.5) {
        return {
            text: 'Kurus',
            color: '#FFA332',
        }
    }

    if (imt <= 25.0) {
        return {
            text: 'Normal',
            color: '#046E89',
        }
    }

    if (imt <= 27.0) {
        return {
            text: 'Gemuk',
            color: '#FFA332',
        }
    }

    return {
        text: 'Sangat gemuk',
        color: '#DB0062',
    }
}

function onChangeIMT(event) {
    const isHeight = event.target.id.includes('height');
    const id = event.target.id;
    const neighbourId = id.replace( (!isHeight ? 'weight' : 'height'), (isHeight ? 'weight' : 'height'));
    const imtId = id.replace( (!isHeight ? 'weight' : 'height'), 'imt');

    const height = $(`#${(isHeight ? id : neighbourId)}`).val();
    const weight = $(`#${(!isHeight ? id : neighbourId)}`).val();

    if(!isNaN(height) && !isNaN(weight)) {
    
        let imt = null;    
        if(height) {
            imt = calcIMT(weight, height);
            $(`#${imtId}`).val(imt.toFixed(1));
            $(`#${imtId}`).attr('data-value', imt);
        } else {
            $(`#${imtId}`).val(0);
        }
    }

}

function listHistory(val) {
    $('#form-add #loading-history').removeClass('hide');
    $('#form-add #reload-history').addClass('hide');
    $('#form-add #wrapper-objective').addClass('hide');
    var url = ("{{ $level == 'doctor' }}") ? "/clinic/{{ $id_clinic }}" : "";
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/history/patient/" + val + url + "?view=3",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataMedical){
            if (!dataMedical.error) {
                listVitalSign(val);
                item = dataMedical.data;
                $("#history").html("");
                if (item.length > 0) {
                    for (var i = 0; i < item.length; i++) {
                        try {
                            tr = $('<tr/>');
                            tr.append("<td><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick=\"loading($(this).attr('href'));\"><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span><br /><span class='type'>" + getFormatDate(item[i].date)+"</span>" + 
                                '<div class="sub show-md">'+
                                    "<span class='info'><br />{{ trans('messages.diagnosis') }}</span>"+
                                    "<span class='general'>"+item[i].diagnosis+"</span>"+
                                '</div>'+
                                "</td>");
                            tr.append("<td class=''>"+item[i].diagnosis+" <br/><span class='type'>"+item[i].icd[0].name+"</span></td>");
                            var action = "<a onclick=\"reportMedical('"+item[i].id+"')\" class='btn-table btn-blue btn-right'><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /> {{ trans('messages.detail') }}</a>";
                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#history").append(tr);
                        } catch(err) {}
                    }
                    $("#link-history").removeClass("hide");
                    $("#link-history").attr("href","#/{{ $lang }}/medical/patient/"+item[0].id_patient);
                } else {
                    $("#history").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                    $("#link-history").addClass("hide");
                }
            } else {
                $('#form-add #loading-history').addClass('hide');
                $('#form-add #reload-history').removeClass('hide');
                $("#form-add #reload-history .reload").click(function(){ listHistory(val); });
            }
        },
        error: function(){
            $('#form-add #loading-history').addClass('hide');
            $('#form-add #reload-history').removeClass('hide');

            $("#form-add #reload-history .reload").click(function(){ listHistory(val); });
        }
    });
}

$("#form-add input[name='reason_complication']").autocomplete({
    minLength: 0,
    source: function( request, response ) {
        var r = [];
        $.ajax({
            type: "GET",
            url: "{{ $api_url }}/{{ $lang }}/icd?q="+request.term+"&per_page=10",
            beforeSend: function(){
                $("#form-add #loading-reason_complication").removeClass("hide");
            },
            success: function(dataSearch){
                $("#form-add #loading-reason_complication").addClass("hide");
                var itemSearch = dataSearch.data;
                for (var i = 0; i < itemSearch.length; i++) {
                    var ICD = itemSearch[i].code+" "+itemSearch[i].name+" - "+(itemSearch[i].specialist != 0 ? "Spesialis" : "Non Spesialis");
                    r.push({id:itemSearch[i].id,category:itemSearch[i].category,subcategory:itemSearch[i].subcategory,code:itemSearch[i].code,icd:itemSearch[i].name,specialist:itemSearch[i].specialist,value:ICD});
                }
                response(r);
            },
            error: function(){
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = "";
        addICDReason(ui.item.id, ui.item.code, ui.item.icd, ui.item.specialist);
        return false;
    }
});

/** Physical Examinations*/

const physicalData = {
    eyes: { title: '{{trans("messages.eyes")}}', selected: '' },
    nose: { title: '{{trans("messages.nose")}}', selected: '' },
    ears: { title: '{{trans("messages.ears")}}', selected: '' },
    oropharynx: { title: '{{trans("messages.oropharynx")}}', selected: '' },
    neck: { title: '{{trans("messages.neck")}}', selected: '' },
    abdomen: { title: '{{trans("messages.abdomen")}}', selected: '' },
    lungs: { title: '{{trans("messages.lungs")}}', selected: '' },
    heart: { title: '{{trans("messages.heart")}}', selected: '' },
    upper_limb: { title: '{{trans("messages.upper_limb")}}', selected: '' },
    lower_limb: { title: '{{trans("messages.lower_limb")}}', selected: '' },
}

makePhysicalExaminationForm();

function onClickPhysicalExamination(value) {
    if(value) {
        const isNormal = !value.includes('abnormal-');
        const type = value && value.split('-').reverse().slice(0,1)[0];

        if(type) {
            if(!isNormal) {
                $(`#physical-data-${type}`).removeClass('hide');
            } else {
                $(`#physical-data-${type}`).addClass('hide');
                $(`#physical-value-${type}`).val('');
            }

            physicalData[type].selected = isNormal ? 'normal' : 'abnormal';
        }
    }
}

function makePhysicalExaminationForm() {

    $('#form-physical-examination-table').html('');

    Object.keys(physicalData).forEach(key => {
        physicalData[key].selected = 'normal';
        const append = `
        <tr>
            <td>
                ${physicalData[key].title}
            </td>
            <td>
                <label><input class="physical-examination-data" type="radio" value="normal-${key}" name="physical-data-${key}" onclick="onClickPhysicalExamination('normal-${key}')" checked><span></span> {{trans("messages.within_normal_limit")}}</label>
            </td>
            <td>
                <label><input class="physical-examination-data" type="radio" value="abnormal-${key}" name="physical-data-${key}" onclick="onClickPhysicalExamination('abnormal-${key}')"><span></span> {{trans("messages.there_is_an_abnormality")}}</label>
            </td>
        </tr>
        <tr>
        </tr>
        <tr class="hide" id="physical-data-${key}">
            <td colspan='3'>
                <div class="form-group content-column" id="physical-value-${key}-group">
                    <label class="control-label"><b>{{trans('messages.condition')}}*</b></label>
                    <input type="text" class="form-control" placeholder="{{trans('messages.input_abnormality')}}" name="physical-value" id="physical-value-${key}" / >
                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                    <span class="help-block"></span>
                </div>
            </td>
        </tr>        
        `
        $('#form-physical-examination-table').append(append);
    })

}

/**ICD */

var numICDReason = 0;
var checkReason = 0;

function addICDReason(id, code, name, specialist) {
    if (specialist == 0){
        checkReason = 1;
    }else{
        checkReason = 0;
    }
    var errorICDReason = false;
    $("#form-add input[name^='icdReason']").each(function(key, value) {
        var test = code+' - '+name;
        if (this.value == test) {
            errorICDReason = true;
        }
    });
    if(errorICDReason == true) {
        notif(false,"{{ trans('validation.icd_already_exists') }}");
    } else {
        numICDReason=numICDReason+1;
        $("#form-add #list-complicated").removeClass("hide");
        $("#form-add #data-complicated").append('<tr>'+
            '<td>'+code+'</td>'+
            '<td>'+name+' <input type="hidden" name="icdReason[]" value="'+code+' - '+name+'" /></td>'+
            '<td>'+(specialist != 0 ? 'Spesialis' : 'Non Spesialis')+'</td>'+
            '<td><a class="link-action" onclick="$(this).parent().parent().remove();removeICDReason();"><img src=\'{{ asset("assets/images/icons/nav/delete-m.png") }}\' /></a></td>'+
        '</tr>');
    }
}

function removeICDReason() {
    numICDReason=numICDReason-1;
    if (numICDReason == 0) {
        $("#form-add #list-complicated").addClass("hide");
    }
}

let timer_icd = "";
$("#form-add input[name='select_icd']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        var r = [];
        clearTimeout(timer_icd);
        timer_icd = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/icd?q="+request.term+"&per_page=10",
                beforeSend: function(){
                    $("#form-add #loading-icd").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-add #loading-icd").addClass("hide");
                    var itemSearch = dataSearch.data;
                    for (var i = 0; i < itemSearch.length; i++) {
                        var ICD = itemSearch[i].code+" "+itemSearch[i].name+" - "+(itemSearch[i].specialist != 0 ? "Spesialis" : "Non Spesialis");
                        r.push({id:itemSearch[i].id,category:itemSearch[i].category,subcategory:itemSearch[i].subcategory,code:itemSearch[i].code,icd:itemSearch[i].name,specialist:itemSearch[i].specialist,value:ICD});
                    }
                    response(r);
                },
                error: function(){
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = "";
        addICD(ui.item.id, ui.item.code, ui.item.icd, ui.item.specialist);
        checkingTACC();
        return false;
    }
});

var numICD = 0;
var check = [];

function addICD(id, code, name, specialist) {
    if (specialist == 0){
        check.push({"is_specialist":0,"code":code});
    }else{
        check.push({"is_specialist":1,"code":code});
    }
    var errorICD = false;
    $("#form-add input[name^='icd']").each(function(key, value) {
        if (this.value == id) {
            errorICD = true;
        }
    });
    if (errorICD == true) {
        notif(false,"{{ trans('validation.icd_already_exists') }}");
    } else {
        numICD = numICD+1;
        $("#form-add #list-icd").removeClass("hide");
        $("#form-add #data-icd").append('<tr>'+
            '<td>'+code+'</td>'+
            '<td>'+name+' <input type="hidden" name="icd[]" value="'+id+'" /></td>'+
            '<td>'+(specialist != 0 ? 'Spesialis' : 'Non Spesialis')+'</td>'+
            '<td><a class="link-action" onclick="$(this).parent().parent().remove();removeICD(\''+code+'\');"><img src=\'{{ asset("assets/images/icons/nav/delete-m.png") }}\' /></a></td>'+
        '</tr>');
    }
}

function removeICD(code) {
    numICD = numICD-1;
    var index = check.map(obj => obj.code).indexOf(code);
    check.splice(index, 1);
    if (numICD == 0) {
        $("#form-add #list-icd").addClass("hide");
    }
    checkingTACC();
}

var itemService = 0;
var numService = 0;

let timer_service = "";
$('#form-service input[name=service_name]').autocomplete({
    minLength: 3,
    source: async function(request, response) {
        loading_content("#form-service #service_name-group", "loading");
        let temp = [];
        clearTimeout(timer_service);

        let filter_polyclinic = '';
        
        /** Legacy */
        // const id_polyclinic = $('#polyclinic').val();
        // const type_kunjungan = $('select[name=type_kunjungan]').val();
        
        // if(id_polyclinic) {
        //     filter_polyclinic += `&id_polyclinic=${id_polyclinic}`;
        // }

        // if(type_kunjungan) {
        //     filter_polyclinic += `&type_kunjungan=${type_kunjungan}`;
        // }
        
        if ('{{ $level == "doctor" }}') {
            filter_polyclinic += '&id_clinic={{ $id_clinic }}'
        }

        timer_service = setTimeout(() => {
            $.ajax({
                url: "{{ $api_url }}/{{ $lang }}/service?q="+request.term+filter_polyclinic+"&per_page=10",
                type: "GET",
                processData: false,
                contentType: false,
                minLength: 3,
                success: function(data){
                    if(!data.error) {
                        loading_content("#form-service #service_name-group", "success");
                        response(data.data && data.data.map(v => {
                            return {
                                ...v,
                                id: v.id,
                                name: v.name,
                                value: v.name
                            }
                        }));
                    } else {
                        loading_content("#form-service #service_name-group", "failed");
                        response(temp);
                    }
                },
                error: function(){
                    loading_content("#form-service #service_name-group", "failed");
                    response(temp);
                }
            });
        }, 2000); 
    },
    focus: function() {
        return false;
    },
    select: function (event, ui) {
        const service = ui.item;
        $('#form-service input[name=service_name]').val(service.name);
        $('#form-service input[name=service_name]').attr('data-id', service.id);
        $('#form-service input[name=service_name]').attr('data-code', service.code);
        $('#form-service input[name=service_name]').attr('data-name', service.name);
        $('#form-service input[name=service_name]').attr('data-bpjs', service.kode_bpjs);
        onChangeService(service.id, service.name, service.code, service.kode_bpjs);
        return false;
    }
});

function onChangeService(id, name, code, bpjs) {
    var name = name || $('#form-service input[name=service_name]').data('name');
    var code = code || $('#form-service input[name=service_name]').data('code');
    var bpjs = bpjs || $('#form-service input[name=service_name]').data('bpjs');
    var text = name || '';
    if (id) {
        $("#form-service input[name='id']").val(id);
        $("#form-service input[name='service_code']").val(code);
        $("#form-service input[name='kode_bpjs']").val(bpjs);
    } else {
        $("#form-service input[name='id']").val("");
        $("#form-service input[name='service_code']").val("");
        $("#form-service input[name='kode_bpjs']").val("");
    }
    if (text.toLowerCase().includes(" anc ") || text.toLowerCase().includes(" pnc ")){
        $("#form-service #gestational_age-group").removeClass('hide');
    }else{
        $("#form-service #gestational_age-group").addClass('hide');
    }
}

function resetService() {
    $("#service input[name='id_item']").val("");
    $("#service input[name='id_service']").val("");
    $("#service input[name='id']").val("");
    $("#service input[name='service_name']").val("");
    $("#service input[name='service_code']").val("");
    $("#service input[name='kode_bpjs']").val("");
    $("#service input[name='service_quantity']").val("");
    $("#service input[name='age1']").val("");
    $("#service input[name='age2']").val("");
    onChangeService();
    resetValidation('service #service_name','service #service_quantity','service #age1','service #age2');
}

function removeItemService(section, number) {
    $("#confirmDelete").modal('show');
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(""+section+"").remove();
        numService--;
        if (numService == 0) {
            $("#form-add #data-service").html("");
            $("#form-add #list-service").addClass("hide");
        }
        $("#confirmDelete").modal('hide');
    });
}

function processItemService(id_service, service_name, service_code, service_quantity, numItem, kode_bpjs, service_hasil, age1, age2) {
    var sectionItem = '';
    if (numItem != "") {
        sectionItem = numItem;
    } else {
        sectionItem = itemService;
    }
    if (numService == 0) {
        $("#form-add #data-service").html("");
        $("#form-add #list-service").removeClass("hide");
    }
    $("#form-add #list-service").removeClass("hide");
    var action = '<a onclick="removeItemService(\'#item_service-'+sectionItem+'\',\''+sectionItem+'\')" class="btn-table btn-red btn-right"><img src=\'{{ asset("assets/images/icons/action/delete.png") }}\' /></a><a data-toggle="modal" data-target="#service" data-id="'+sectionItem+'" class="btn-table btn-blue btn-right"><img src=\'{{ asset("assets/images/icons/action/edit.png") }}\' /></a>';
    var section = '<td>'+
            '<a><h1 class="no-hover">' + service_name + '</h1></a>'
            +'<span class="type">' + service_code + '</span>'
            +'<span class="general show-md">{{ trans("messages.quantity") }}: ' + service_quantity + '</span>'
            +'</td>'
            +'<td class="hide-md">'+(kode_bpjs != '' ? 'BPJS' : 'Umum')+'</td>'
            +'<td class="hide-md">'+service_quantity+'</td>'
            +'<td>'
            +action+
            '<div class="dropdown dropdown-sm">'+
                '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                '<ul class="dropdown-menu dropdown-menu-right">'+
                    action+
                '</ul>'+
            '</div>'     
            +'<input type="hidden" name="id_service['+sectionItem+']" value="'+id_service+'">'
            +'<input type="hidden" name="service_name['+sectionItem+']" value="'+service_name+'">'
            +'<input type="hidden" name="service_code['+sectionItem+']" value="'+service_code+'">'
            +'<input type="hidden" name="service_quantity['+sectionItem+']" value="'+service_quantity+'">'
            +'<input type="hidden" name="service_hasil['+sectionItem+']" value="'+service_hasil+'">'
            +'<input type="hidden" name="age1['+sectionItem+']" value="'+age1+'">'
            +'<input type="hidden" name="age2['+sectionItem+']" value="'+age2+'">'
            +'</td>';
    if (numItem != "") {
        $("#form-add #data-service #item_service-"+numItem).html(section);
    } else {
        $("#form-add #data-service").append('<tr id="item_service-'+itemService+'">'+section+'</tr>');
        itemService++;
        numService++;
    }
}

$('#service').on('show.bs.modal', function (e) {
    resetService();
    var id = $(e.relatedTarget).attr('data-id');
    if (id == undefined || id == "") {
        $("#service input[name='id_item']").val("");
        $("#service input[name='id_service']").val("");
        $("#service input[name='id']").val("");
        $("#title-service").html('<?php echo trans('messages.add_service_action'); ?>');
    } else {
        $("#service input[name='service_name']").val($("#form-add input[name='service_name["+id+"]']").val());
        $("#service input[name='id']").val($("#form-add input[name='id_service["+id+"]']").val());
        $("#service input[name='id_service']").val($("#form-add input[name='id_service["+id+"]']").val());
        $("#service input[name='id_item']").val(id);
        $("#service input[name='service_code']").val($("#form-add input[name='service_code["+id+"]']").val());
        $("#service input[name='kode_bpjs']").val($("#form-add input[name='kode_bpjs["+id+"]']").val());
        $("#service input[name='service_quantity']").val($("#form-add input[name='service_quantity["+id+"]']").val());
        $("#service input[name='age1']").val($("#form-add input[name='age1["+id+"]']").val());
        $("#service input[name='age2']").val($("#form-add input[name='age2["+id+"]']").val());
        $("#title-service").html('<?php echo trans('messages.edit_service_action'); ?>');
    }
});

$('#service').on('hidden.bs.modal', function (e) {
    resetService();
    $('html, body').animate({
        scrollTop: $("#list-service").offset().top-100
    }, 0);
});

$("#service").submit(function(event) {
    var id_item = $("#service input[name='id_item']").val();
    var id_service = $("#service input[name='id_service']").val();
    var id = $("#service input[name='id']").val();
    var name = $("#service input[name='service_name']").val();
    var code = $("#service input[name='service_code']").val();
    var kode_bpjs = $("#service input[name='kode_bpjs']").val();
    var quantity = $("#service input[name='service_quantity']").val();
    var age1 = $("#service input[name='age1']").val();
    var age2 = $("#service input[name='age2']").val();
    var service_hasil = age2-age1;
    var error = false;
    var scrollValidate = true;

    if (name == '') {
        formValidate(scrollValidate, ['service #service_name','{{ trans("validation.empty_service") }}', true]);    
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (quantity == '') {
        formValidate(scrollValidate, ['service #service_quantity','{{ trans("validation.empty_qty") }}', true]);    
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (id_item == "" || id_service != id) {
        var errorService = false;
        $("#form-add input[name^='id_service']").each(function(key, value) {
            if (this.value == id) {
                errorService = true;
            }
        });
        if (errorService == true) {
            notif(false,"{{ trans('validation.service_already_exists') }}");
            error=true;
        }
    }
    if (error == false) {
        if (id == "") {
            notif(false,"{{ trans('validation.invalid_service') }}");
        } else {
            if (id_item != "") {
                processItemService(id, name, code, quantity, id_item, kode_bpjs, service_hasil, age1, age2);
                notif(true,"{{ trans('validation.success_edit_service') }}");
            } else { 
                processItemService(id, name, code, quantity, '', kode_bpjs, service_hasil, age1, age2);
                notif(true,"{{ trans('validation.success_add_service') }}");
            }
            $("#service").modal("toggle");
            resetService();
        }
    }
});

$("#service #service_name-group .border-group").click(function() {
    resetValidation('service #service_name');
});

$("#service input[name='service_quantity']").focus(function() {
    resetValidation('service #service_quantity');
});

function templateConcoction() {
    let temp_ = `
    <div class="list_content"> 
        <div class="flex-content">
            <label class="control-label" id="label_number">{{ trans("messages.medicine") }} ${num_medicine}</label>
            <span style="cursor: pointer;" onclick="deleteRacik(this)"><img src="{{asset("assets/images/Shape.png")}}" /></span>
        </div>
        <div class="content-medicine">
            <div class="form-group column-group">
                <label class="control-label">{{ trans("messages.inventory") }} <span>*</span></label>
                <div class="border-group">
                    <input name="medicine_racikan" id="medicine_racikan-${num_medicine}" placeholder= "{{ trans('messages.select_medicine') }}" type="text" class="form-control medicine_racikan" data-id="" />
                </div>
                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                <label class="control-label" id="title-notes"></label>
            </div>
            <div class="col-xs-6 list-l">
                <div class="">
                    <label class="control-label">{{ trans("messages.qty") }} <span>*</span></label>
                    <input class="form-control" placeholder="{{ trans("messages.input_quantity") }}" name="qty_racikan" type="text" onkeydown="number(event)" oninput="setNotifStock(this)" onchange="setNotifStock(this)" />
                    <p id="notif-stock-concoction" class="notif-empty"></p>
                </div>
            </div>
            <div class="col-xs-6 list-r">
                <div class="">
                    <label class="control-label">{{ trans("messages.unit") }} <span>*</span></label>
                    <div class="border-group">
                        <select class="form-control" name="unit_racikan">
                            <option value=""></option>
                        </select>
                    </div>
                    <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                </div>
            </div>
        </div>
    </div>`;
    return temp_;
}

var num_medicine = 1;
$("#add_medicine").click(function() {
    
    num_medicine++
    $("#content_").append(templateConcoction());
    

    $("#medicine select[name='unit_racikan']").select2({
        placeholder: "{{ trans('messages.unit') }}"
    });

    let key_ = "";
    $("#content_").find(".list_content").each(function(key, val) {
        $(this).attr("id", function() {
            return "racik-"+key+"";
        });
        if(key > 0) {
            key_ = $(this).attr('id')
        }
    });

    $('.container-medicine').animate({
        scrollTop: $('#content_').height()
    }, 500)

    bindRacikanAutocomplete();
});

function deleteRacik(event) {
    $(event).parent().parent().remove();

    $("#content_").find(".list_content").each(function(key, val) {
        $(this).attr("id", function() {
            return "racik-"+key+"";
        });

        if(key > 0) {
            $("#racik-"+key+" #label_number").html('{{ trans("messages.medicine") }} '+ ++key +'')
        }
    });

    num_medicine--
}

function setNotesConcoction(event) {
    if($(event).val() != "") {
        let dose = $("#medicine input[name='signa1']").val();
        let per_day = $("#medicine input[name='signa2']").val();
        let dosage = $("#medicine input[name='unit_dose']").val();
        let time_frame = $("#medicine select[name='time'] option:selected").text();
        let duration_ = $("#medicine input[name='duration_racikan']").val();
        if(dose != "") {
            $("#notes_concoction").html("{{ trans('messages.this_dose_concoction') }} "+dose+" x "+per_day+" "+dosage+" "+time_frame+", {{ trans('messages.during') }} "+duration_+" {{ trans('messages.day') }}")
        }
    }
}

async function addTemplateConcoction(medicine, qty, unit) {
    num_medicine = 1
    for (i = 0; i < medicine.length; i++) {
        if(i > 0) {
            num_medicine ++
            $("#content_").append(templateConcoction());
        }
    }

    $("#medicine select[name='unit_racikan']").select2({
        placeholder: "{{ trans('messages.unit') }}"
    });

    $("#content_").find(".list_content").each( async function(key, val) {
        $(this).attr("id", function() {
            return "racik-"+key+"";
        });

        $("#racik-"+key+" input[name='medicine_racikan']").val(medicine[key]).change()

        await autoCompleteAddRacikan(medicine[key], $("#racik-"+key+" input[name='medicine_racikan']").attr("id"))

        $("#racik-"+key+" input[name='medicine_racikan']").attr("id")
        $("#racik-"+key+" input[name='qty_racikan']").val(qty[key]).change()
        $("#racik-"+key+" select[name='unit_racikan']").val(unit[key]).trigger("change")

    });
    bindRacikanAutocomplete()
}



$('#form-medicine input[name=is_racikan]').change(function() {
    let hiddenId = $(this).attr("data-trigger");
    if ($(this).is(':checked')) {
        $("#notes_concoction").html("");
        $("." + hiddenId).removeClass('hide');
        $("#interaction-group").addClass('hide');
        $("#one_day-group").addClass('hide');
        $("#duration-group").addClass('hide');
        $("#qty_radio_button-group").addClass('hide');
        $("#label-qty").addClass('hide');
        $("#total-stock").addClass("hide");
        $("#medicine_code-group").addClass("hide");
        $("#medicine_name-group").addClass("hide");
        $("#medicine input[name='instruction_racikan']").val("");
        $("#medicine input[name='unit_dose']").val("");
        $("#medicine select[name='time']").val("per hari").trigger("change");
        $("#medicine input[name='qty_package']").val("");
        $("#medicine select[name='type_package']").val("").trigger("change");
        $("#medicine #qty-unit").text("");
        $("#medicine input[name='qty_radio_button']").attr("checked", false);
        $("#medicine input[name='duration']").val("");
        $("#medicine select[name='signa3']").val("").trigger("change");
        $("#form-medicine input[name='package']").prop("checked", true).change();
    } else {
        $("." + hiddenId).addClass('hide');
        $("#notes_concoction").html("");
        $("#form-medicine input[name='package']").prop("checked", false).change();
        $("#medicine_code-group").removeClass("hide");
        $("#medicine_name-group").removeClass("hide");
        $("#interaction-group").removeClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#duration-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $("#medicine input[name='racikan']").val("");
        $("#total-stock-racikan").text("");
        $("#medicine select[name='signa3']").val("per hari").trigger("change");

    }
    resetValidation('medicine #medicine_name', 'medicine #duration', 
        'medicine #instructions', 'medicine #racikan', 
        'medicine #unit_medicine', 'medicine #medicine_strong', 
        'medicine #medicine_qty', 'medicine #qty_item_medicine', 
        'medicine #total_day', 'medicine #duration_racikan', 'medicine #instruction_racikan' );

});

$("#form-medicine input[name='package']").on('change', function() {
    resetValidation('medicine #type_package', 'medicine #qty_package')
    let trigger = $(this).attr("data-trigger");
    if($(this).is(':checked')) {
        $("." + trigger).removeClass('hide')
    }else {
        $("." + trigger).addClass('hide')
    }
})

var numMedicine = 0;

/** Medicine */

const medicinesObject = {};
const medicinesNameObject = {};

function getCachedMedicineByName(name) {
    const id = medicinesNameObject[name];
    return getCachedMedicineById(id); 
}

function getCachedMedicineById(id) {
    return medicinesObject[id || '-'];
}

let timer_medicine = "";
function getMedicine(question, per_page = 10) {
    clearTimeout(timer_medicine)
    return new Promise((resolve, reject) => {
        // fake request
        if(question && medicinesObject[question]) {
            return resolve({
                total: 1,
                per_page,
                current_page: 1,
                last_page: 1,
                from: 1,
                to: 1,
                data: [medicinesObject[question]],
                error: false
            })
        }
        timer_medicine = setTimeout(() => {
            $.ajax({
                url: `{{ $api_url }}/{{ $lang }}/medicine?category=obat${filter_clinic}&q=${question}&per_page=${per_page}`,
                type: "GET",
                processData: false,
                contentType: false,
                success: function(data, textStatus, jqXHR) {
                    if(data && data.data) {
                        data.data.forEach(medicine  => {
                            medicinesObject[medicine.id] = medicine;
                            medicinesNameObject[medicine.name] = medicine.id;
                        })
                    }
                    return resolve(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    return reject(jqXHR);
                }
            })
        }, 2000);
    });
}



/** Handle autocomplete medicine */
let selectedMedicine;
$('#form-medicine input[name=medicine_name]').autocomplete({
    source: async function(request, response) {
        let temp = [];
        try {
            if(request.term.length > 2) {
                loading_content("#form-medicine #medicine_name-group", "loading");
                const medicineRequest = await getMedicine(request.term);
                loading_content("#form-medicine #medicine_name-group", "success");
                if(!medicineRequest.error && medicineRequest.data) {
                    return response(medicineRequest.data.map(v => {
                        return {
                            ...v,
                            id: v.id,
                            name: v.name,
                            value: v.name,
                        }
                    }));
                }
            }
        } catch (error) {
            loading_content("#form-medicine #medicine_name-group", "failed");
            response(temp);
        }
    },
    focus: function() {
        return false;
    },
    select: function (event, ui) {
        $('#form-medicine input[name=medicine_name]').val(ui.item.name);
        $('#form-medicine input[name=medicine_name]').data('id', ui.item.id);
        changeMedicine(ui.item);
        return false;
    }
});

bindRacikanAutocomplete();


function bindRacikanAutocomplete() {
    $('.medicine_racikan').unbind('autocomplete');
    $('.medicine_racikan').autocomplete({
        source: async function(request, response) {
            let temp = [];
            try {
                if( request.term.length > 2 ) {
                    loading_content("#form-medicine #medicine_racikan-group", "loading");
                    const medicineRequest = await getMedicine(request.term);
                    loading_content("#form-medicine #medicine_racikan-group", "success");
                    if(!medicineRequest.error && medicineRequest.data) {
                        return response(medicineRequest.data.map(v => {
                            return {
                                ...v,
                                id: v.id,
                                name: v.name,
                                value: v.name,
                            }
                        }));
                    }
                }
            } catch (error) {
                loading_content("#form-medicine #medicine_racikan-group", "success");
                response(temp);
            }
        },
        focus: function() {
            return false;
        },
        select: function(event, ui) {
            const targetId = event.target.id;
            const medicine = ui.item;
            setUnitRacikan(medicine.id, targetId);
            return false
        }
    });
}

async function autoCompleteAddRacikan(val, targetId) {
    try {
        let list = await getterConcoction(val)
        await setUnitRacikan(list.data[0].id, targetId)

    } catch (error) {
        console.error(error)
    }
}

function getterConcoction(val, per_page = 10) {
    return new Promise((resolve, reject) => {
        if(val && medicinesObject[val]) {
            return resolve({
                total: 1,
                per_page,
                current_page: 1,
                last_page: 1,
                from: 1,
                to: 1,
                data: [medicinesObject[val]],
                error: false
            })
        }
        $.ajax({
            url: `{{ $api_url }}/{{ $lang }}/medicine?category=obat${filter_clinic}&q=${val}&per_page=${per_page}`,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
                if(data && data.data) {
                    data.data.forEach(medicine  => {
                        medicinesObject[medicine.id] = medicine;
                        medicinesNameObject[medicine.name] = medicine.id;
                    })
                }
                return resolve(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                return reject(jqXHR);
            }
        })
    });
}

async function setUnitRacikan(id_medicine, targetId) {
    let medicine
    try {
        medicine = await getCachedMedicineById(id_medicine);
    } catch (error) {
        console.error(error);
    }

    if(medicine && targetId) {
        let temp_ = [];
        $("#content_").find(".list_content").each(function(key, val) {
            $(this).attr("id", function() {
                return "racik-"+key+"";
            });
        });

        const targetNumberStr = targetId.split('-').reverse()[0];
        const id = !isNaN(targetNumberStr) ? `racik-${(parseInt(targetNumberStr) - 1)}` : null;
        if(id) {
            const stock = $(`#${id}`).find('option:selected').data('stock');

            $(`#${id} #${targetId}`).val(medicine.name);
            $(`#${id} #${targetId}`).data('id', medicine.id);
            $(`#${id} #${targetId}`).attr('data-name', medicine.name);
            $(`#${id} #${targetId}`).attr('data-stock', medicine.stock);
            $(`#${id} #${targetId}`).attr('data-code', medicine.code);
            $("#"+id+" select[name='unit_racikan']").html('<option value=""></option>');
            $("#"+id+" select[name='unit_racikan']").val("").trigger("change");
            $("#"+id+" input[name='qty_racikan']").val("");
            $("#"+id+" #title-notes").html("");
            $("#"+id+" #notif-stock-concoction").html("");
            $("#"+id+" #title-notes").html("{{ trans('messages.inventory')}} : "+$(`#${id} input[name='medicine_racikan']`).val()+"");
            medicine.unit.forEach(unit => {
                $("#"+id+" select[name='unit_racikan']").append("<option value="+unit.id+">"+unit.name+"</option>")
            });
        }
    }

}

@if(!empty($id_clinic))
filter_clinic = '{{ $level == "doctor" }}' ? '&id_clinic={{ $id_clinic }}' : '';
@endif

listMedicinePackage()

function listMedicinePackage() {
    loading_content("#form-medicine #type_package-group", "loading");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine?category=Package",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if (!data.error) {
                loading_content("#form-medicine #type_package-group", "success");
                $("#medicine select[name='type_package']").html("<option value=''></option>");
                for(i = 0; i < data.data.length; i++) {
                    let unit_id = ""
                    if(data.data[i].unit.length > 0 ) {
                        for (j = 0; j < data.data[i].unit.length; j++) {
                            unit_id = data.data[i].unit[data.data[i].unit.length -1].id;
                        }
                    }
                    $("#medicine select[name='type_package']").append('<option value="'+data.data[i].id+'" data-unit="'+unit_id+'">'+data.data[i].name+'</option>')
                }         
                $("#medicine select[name='type_package']").select2({
                    placeholder: "{{ trans('messages.select_type_package') }}"
                });
            } else {
                loading_content("#form-medicine #type_package-group", "failed");
                $("#form-medicine #type_package-group #loading-content").click(function(){ listMedicinePackage(); });
            }
        },
        error: function(){
            loading_content("#form-medicine #type_package-group", "failed");
            $("#form-medicine #type_package-group #loading-content").click(function(){ listMedicinePackage(); });
        }
    });
}

var medicine_select = "";

function changeMedicine(medicine) {
    $("#medicine input[name='duration']").val("");
    $("select[name='unit_medicine']").html('<option value=""></option>')
    $("select[name='unit_medicine']").val("").trigger("change");
    $("#total-stock-racikan").text("");
    $("#medicine input[name='power_medicine']").val("");
    $("#medicine input[name='qty_demand']").val("");
    $("#medicine input[name='qty_item_medicine']").val("");
    $("#medicine input[name='total_day']").val("");
    
    if(medicine) {
        const {id, name, code, notes} = medicine;
        medicine_select = medicine;

        $("#form-medicine input[name='id']").val(id);
        $("#form-medicine input[name='medicine_code']").val(code);
        $("#form-medicine input[name='interaction']").val(notes ? notes : "-");
        if(medicine_select.unit.length > 0) {
            $("#save-inventory").attr("disabled", false);
            setQtyUnitMedicine(medicine_select);
            for (i = 0; i < medicine_select.unit.length; i++) {
                $("select[name='unit_medicine']").append("<option value="+medicine_select.unit[i].id+">"+medicine_select.unit[i].name+"</option>")
            }
        }else {
            $("#form-medicine input[name='id']").val("");
            $("#form-medicine input[name='medicine_code']").val("");
            $("#form-medicine input[name='interaction']").val("");
            notif(false, '{{ trans("validation.no_unit")}}');
            $("#qty_radio_button-group").html("<p>{{ trans('validation.empty_unit') }}</p>");
            $("#save-inventory").attr("disabled", true);
            $(".signa2-group p").html("");
        }
    }else {
        $("#form-medicine input[name='id']").val("");
        $("#form-medicine input[name='medicine_code']").val("");
        $("#form-medicine input[name='interaction']").val("");
        notif(false, '{{ trans("validation.no_unit")}}');
        $("#qty_radio_button-group").html("<p>{{ trans('validation.empty_unit') }}</p>");
        $("#save-inventory").attr("disabled", true);
        $(".signa2-group p").html("");
    }
}

$("select[name='unit_medicine']").change(function(e){
    $("#medicine_dose-group .signa2-group p").html($(this).find(':selected').text())
});

async function setNotifStock(event) {
    let parents = $(event).closest(".list_content")[0]
    let id_parent = $(parents).attr("id");
     if(id_parent) {
         let medicine_select_racikan = getCachedMedicineByName($("#"+id_parent+" input[name='medicine_racikan']").val());
         let stock = medicine_select_racikan.stock;
         medicine_select_racikan.unit.sort((a,b) => Number(b.weight) - Number(a.weight));
         if(stock < $(event).val()) {
             $("#"+id_parent+" #notif-stock-concoction").html("{{ trans('messages.stock_only') }} "+stock+" "+medicine_select_racikan.unit[0].name+"")
         }else {
             $("#"+id_parent+" #notif-stock-concoction").html("")
         }
     }
 }

var data_unit_reverse;
function setQtyUnitMedicine(medicine) {
    $("#qty_radio_button-group").html("")
    let template_qty = "<div class='sf-radio'>" +
                            "<div class='wrapper-label-radio'>" +
                                "<label><input type='radio' name='qty_radio_button' /><span></span></label>" +
                                "<span id='qty-unit'></span>" +
                                "<p class='notes'>{{ trans('messages.item') }}</p>" +
                            "</div>"+
                            "<p id='total-stock' class='notes-radio-button hide'></p>"+
                        "</div>";
    let template_add_qty = "<div class='sf-radio another_unit'>" +
                            "<div class='wrapper-label-radio'>" +
                                "<label><input type='radio' name='qty_radio_button' id='change_unit' /><span></span></label>" +
                                "<p class='notes'>{{ trans('messages.another_amount') }}</p>" +
                            "</div>"+
                            "<div class='wrapper-label-radio last-title hide'>" +
                                "<div class='col-xs-7'>" +
                                    "<label class='control-label'>{{ trans('messages.qty') }}</label>"+
                                    "<div>" +
                                        "<input class='form-control' type='number' placeholder='{{ trans('messages.input_quantity') }}' name='qty_change_unit' />" +
                                        "<span id='notes-change-unit' class='notif-notes'></span>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='col-xs-5'>" +
                                    "<label class='control-label'>{{ trans('messages.unit') }}</label>" +
                                    "<div>" +
                                        "<select class='form-control' name='unit_add'>" +
                                            "<option value=''></option>" +
                                        "</select>" +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
    for (i = 0; i < medicine.unit.length; i++) {
        $("#qty_radio_button-group").append(template_qty)
    }

    medicine.unit.sort((a, b) =>Number(b.weight) - Number(a.weight));
    data_unit_reverse = medicine.unit;
    $("#qty_radio_button-group").find(".sf-radio").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        $("#qty_radio_button-group #type-"+key+" .notes").html(medicine.unit[key].name)
        $("#qty_radio_button-group #type-"+key+" input[name='qty_radio_button']").attr("data-name", function(i) {
            return medicine.unit[key].name
        });
        $("#qty_radio_button-group #type-"+key+" input[name='qty_radio_button']").attr("data-id", function(j) {
            return medicine.unit[key].id
        });
    });

    $(".signa2-group p").html(medicine.unit[0].name);
    $("#qty_radio_button-group").append(template_add_qty);

    $("select[name='unit_add']").select2({
        placeholder: "{{ trans('messages.unit') }}"
    });
}

var dose = "";
var unit_dose = "";
var duration = "";
var results = "";

$("input[name='signa1']").on('input', function(e){
    dose = e.target.value;
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration
        setValueQty(results)
    }
});

$("input[name='signa2']").on('input', function(e) {
    unit_dose = e.target.value.replace(/,/g, '.');
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration
        setValueQty(results)
    }
    
});

$("input[name='duration']").on('input',function(e) {
    duration = e.target.value
    if(dose != "" && unit_dose != "" && duration != "") {
        results = dose * unit_dose * duration
        setValueQty(results)
    }
});


function setValueQty(val) {
    $("select[name='unit_add']").html("<option value=''></option>");
    $("#qty_radio_button-group").find(".sf-radio").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });

        if(medicine_select.stock < Math.ceil(val)) {
            $("#type-"+key+" #total-stock").text(`{{ trans('messages.stock_only') }} ${medicine_select.stock} ${data_unit_reverse[0].name}`)
        }else {
            $("#type-"+key+" #total-stock").text("");
        } 

        if (key === 0) {
            $("#type-"+key+" #qty-unit").text(Math.ceil(val));
            $("#type-"+key+" input[name='qty_radio_button']").val(Math.ceil(val))
        } else if (key > 0) {
            let sumOf = val / data_unit_reverse[key - 1].amount_ratio

            if (key - 1 > 0) {
                sumOf = val
                for (i = 0; i < key; i++) sumOf = sumOf / data_unit_reverse[i].amount_ratio
            }

            $("#type-"+key+" #qty-unit").text(Math.ceil(sumOf));
            $("#type-"+key+" input[name='qty_radio_button']").val(Math.ceil(sumOf));
        }
    });

    for (i = 0; i < data_unit_reverse.length; i++) {
        $("select[name='unit_add']").append("<option value="+data_unit_reverse[i].id+">"+data_unit_reverse[i].name+"</option>")
    }
    
    setTotalStock()

    $("#qty_radio_button-group input[name='qty_radio_button']").on('change', function() {
        setTotalStock();
    });

    $("input[name='qty_change_unit']").on("input", function(e) {
        if(medicine_select.stock < e.target.value) {
            $("#notes-change-unit").text(`{{ trans('messages.stock_only') }} ${medicine_select.stock} ${data_unit_reverse[0].name}`)
        }else {
            $("#notes-change-unit").text("")
        }
    });
}

function setTotalStock() {
    $("#qty_radio_button-group input[name^='qty_radio_button']").each(function(key, val) {
        if ( $(this).prop("checked") ){
            if($(this).is("#change_unit")) {
                $(".last-title").removeClass("hide");
            }
            $("#type-"+key+" #total-stock").removeClass("hide");
        } else {
            $("#type-"+key+" #total-stock").addClass("hide");
            $(".last-title").addClass("hide");
            $("#notes-change-unit").text("");
            $("input[name='qty_change_unit']").val("");
            $("select[name='unit_add']").val("").trigger("change");
        }
    });
}

function resetMedicine() {
    results = "";
    dose = ""
    unit_dose = ""
    duration = ""

    $("#medicine input[name='id_item']").val("");
    $("#medicine input[name='id_medicine']").val("");
    $("#medicine input[name='id']").val("");
    $("#medicine input[name='medicine_name']").val("").trigger("change");
    $("#medicine input[name='medicine_code']").val("");
    $("#medicine input[name='is_racikan']").removeAttr('checked');
    $(".is_racikan").addClass('hide');
    $(".package").addClass("hide");
    $("#medicine_code-group").removeClass("hide");
    $("#medicine_name-group").removeClass("hide");
    $("#medicine input[name='racikan']").val("");
    $("#medicine input[name='signa1']").val("");
    $("#medicine input[name='signa2']").val("");
    $("#medicine input[name='duration']").val("");
    $("#medicine #qty-unit").text("");
    $("#medicine input[name='qty_radio_button']").attr("checked", false);
    $("#qty_radio_button-group").html("");
    $("#medicine input[name='interaction']").val("");
    $("#medicine input[name='instructions']").val("");
    $("#medicine input[name='unit_dose']").val("");
    $("#medicine input[name='qty_package']").val("");
    $("#medicine select[name='type_package']").val("").trigger("change");
    $("#medicine input[name='instruction_racikan']").val("");
    $("#total-stock-racikan").text("");
    $("#save-inventory").attr("disabled", false);
    $("#medicine_dose-group .signa2-group p").html("");
    $("#medicine input[name='medicine_racikan']").val("");
    $("#medicine input[name='qty_racikan']").val("");
    $("#medicine select[name='unit_racikan']").html("<option value=''></option>")
    $("#medicine select[name='unit_racikan']").val("").trigger("change");
    $("#notif-stock-concoction").html("");

    resetValidation('medicine #medicine_name', 'medicine #duration', 
        'medicine #instructions', 'medicine #racikan', 
        'medicine #unit_medicine', 'medicine #medicine_strong', 
        'medicine #medicine_qty', 'medicine #qty_package', 
        'medicine #type_package', 'medicine #unit_dose', 
        'medicine #duration_racikan', 'medicine #instruction_racikan');
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    removeItemMedicine($section)
})

function removeItemMedicine(section) {
        $(""+section+"").remove();
        removeMedikamentosa(section);
        
        if (numMedicine == 0) {
            $("#form-add #data-medicine").html("");
            $("#form-add #list-medicine").addClass("hide");
        }

        $("#data-medicine").find(".template_medicine").each(function(key, value) {
            $(this).attr("id", function() {
                return "item_medicine-" + key;
            });

            $("#item_medicine-"+key+" #action-medicine-edit").attr("data-id", key)
            $("#item_medicine-"+key+" #action-medicine-delete").attr("data-section", "#item_medicine-"+key+"")
            $("#item_medicine-"+key+" #action-medicine-delete").attr("data-number", key)

            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='qty_unit_checked']"), 'qty_unit_checked', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='id_medicine']"), 'id_medicine', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='medicine_name']"), 'medicine_name', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='medicine_code']"), 'medicine_code', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='id_unit']"), 'id_unit', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='id_item']"), 'id_item', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='is_racikan']"), 'is_racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='racikan']"), 'racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='instructions']"), 'instructions', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='interaction']"), 'interaction', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='signa1']"), 'signa1', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='signa2']"), 'signa2', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='name_signa3']"), 'name_signa3', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='duration']"), 'duration', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='unit_dose']"), 'unit_dose', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='time']"), 'time', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='qty_package']"), 'qty_package', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='type_package']"), 'type_package', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='id_unit_package']"), 'id_unit_package', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='notes_racikan']"), 'notes_racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='is_package']"), 'is_package', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='unit_name_checked']"), 'unit_name_checked', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='instruction_racikan']"), 'instruction_racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='another_qty']"), 'another_qty', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='medicine_racikan']"), 'medicine_racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='qty_racikan']"), 'qty_racikan', key)
            resetIndex($("#data-medicine #item_medicine-"+key+" input[name^='unit_racikan']"), 'unit_racikan', key)
        });
        
        numMedicine--
        $("#confirmDelete").modal('hide');
}

function processItemMedicine(id, name, code, instructions, 
        interaction, signa1, signa2, signa3, 
        duration, qty_unit, is_racikan, racikan, 
        unit_dose_racikan, time_racikan, qty_package, 
        type_package, qty_unit_name, unit_name_checked, value_unit_name_checked, 
        id_unit_checked, another_qty, stock, 
        value_stock_all, instruction_racikan, medicine_racikan,
        qty_racikan, unit_racikan, is_package, id_unit_package, 
        notes_racikan, first_unit, id_item) {

    let sectionItem = '';
    let notif_stock = "";
    let field_medicine_racikan = "";
    let field_qty_racikan = "";
    let field_unit_racikan = "";
    let field_text_medicine_racikan = "";
    let list = "";

    if (id_item != "") {
        sectionItem = id_item;
    } else {
        sectionItem = numMedicine;
    }
    
    if(is_racikan == '1') {
        for(i = 0; i <  medicine_racikan.length; i++) {
            if(medicine_racikan[i].stock < qty_package || medicine_racikan[i].stock < qty_racikan[i]) {
                notif_stock = '<span class="tooltip-notif" data-tip="{{ trans("messages.insufficient_stock") }}"><img src="{{ asset("assets/images/Warning.png") }}" /></span>'
            }
            field_text_medicine_racikan += '<input type="hidden" name="text_medicine_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine_racikan[i].text_medicine_racikan+'" />'
            field_medicine_racikan += '<input type="hidden" name="medicine_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+medicine_racikan[i].id_medicine_racikan+'" />'
            field_qty_racikan += '<input type="hidden" name="qty_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+qty_racikan[i]+'" />'
            field_unit_racikan += '<input type="hidden" name="unit_racikan['+sectionItem+']" data-id="id-'+i+'" value="'+unit_racikan[i].id_unit_racikan+'" />'
            list += '<li>'+medicine_racikan[i].text_medicine_racikan+'('+qty_racikan[i]+' '+unit_racikan[i].text_unit_racikan+')</li>' 
        }
    }else {
        if(stock < value_stock_all) {
            notif_stock = '<span class="tooltip-notif" data-tip="{{ trans("messages.stock_only") }} '+stock+' '+first_unit+'"><img src="{{ asset("assets/images/Warning.png") }}" /></span>'
        }
    }

    $("#form-add #list-medicine").removeClass("hide");
    let action = '<a id="action-medicine-delete" data-toggle="modal" data-target="#confirmDelete"  data-message=\'{{ trans("messages.info_delete") }}\' data-section="#item_medicine-'+sectionItem+'" data-number="'+sectionItem+'" class="btn-table btn-red btn-right"><img src=\'{{ asset("assets/images/icons/action/delete.png") }}\' /> </a><a id="action-medicine-edit" data-toggle="modal" data-target="#medicine" data-id="'+sectionItem+'" class="btn-table btn-blue btn-right"><img src=\'{{ asset("assets/images/icons/action/edit.png") }}\' /> </a>';
    let section = '<td><a><h1 class="no-hover">' + (racikan == '' ? name : racikan+'('+instruction_racikan+')') + '</h1></a>'
            +(racikan == '' ? '' : '<ul class="space-list">'+list+'</ul>')
            +'<p class="notes-line">Pemakaian: '+instructions+'</p>'
            +(racikan == '' ? '<p class="notes-line">Catatan: '+interaction+'</p>' : '')
            +'</td>'
            +'<td class="hide-md list-table-mini">'+(racikan != '' ? racikan : '&nbsp;-&nbsp;')+'</td>'
            +'<td class="hide-md list-table-mini">'+signa1+' x '+signa2+'</td>'
            +'<td class="hide-md list-table-mini">'+duration+'</td>'
            +'<td class="hide-md list-table-mini">'+(racikan != '' ? unit_dose_racikan : unit_name_checked)+'</td>'
            +'<td class="hide-md list-table-mini">'+(racikan != '' ? qty_package : value_unit_name_checked)+' '+notif_stock+'</td>'
            +'<td>'
            +action
            +field_text_medicine_racikan
            +field_medicine_racikan
            +field_qty_racikan
            +field_unit_racikan
            +'<input type="hidden" name="qty_unit_checked['+sectionItem+']" value="'+value_unit_name_checked+'">'
            +'<input type="hidden" name="id_medicine['+sectionItem+']" value="'+id+'">'
            
            +'<input type="hidden" name="id_item['+sectionItem+']" value="'+sectionItem+'">'
            
            +'<input type="hidden" name="medicine_name['+sectionItem+']" value="'+name+'">'
            +'<input type="hidden" name="medicine_code['+sectionItem+']" value="'+code+'">'
            +'<input type="hidden" name="id_unit['+sectionItem+']" value="'+id_unit_checked+'">'
            +'<input type="hidden" name="is_racikan['+sectionItem+']" value="'+(is_racikan == '1' ? '1' : '')+'">'
            +'<input type="hidden" name="racikan['+sectionItem+']" value="'+racikan+'">'
            +'<input type="hidden" name="instructions['+sectionItem+']" value="'+instructions+'">'
            +'<input type="hidden" name="interaction['+sectionItem+']" value="'+interaction+'">'
            +'<input type="hidden" name="signa1['+sectionItem+']" value="'+signa1+'">'
            +'<input type="hidden" name="signa2['+sectionItem+']" value="'+signa2+'">'
            +'<input type="hidden" name="name_signa3['+sectionItem+']" value="'+signa3+'">'
            +'<input type="hidden" name="duration['+sectionItem+']" value="'+duration+'">'
            +'<input type="hidden" name="unit_dose['+sectionItem+']" value="'+unit_dose_racikan+'">'
            +'<input type="hidden" name="time['+sectionItem+']" value="'+time_racikan+'">'
            +'<input type="hidden" name="qty_package['+sectionItem+']" value="'+qty_package+'">'
            +'<input type="hidden" name="type_package['+sectionItem+']" value="'+type_package+'">'
            +'<input type="hidden" name="id_unit_package['+sectionItem+']" value="'+id_unit_package+'">'
            +'<input type="hidden" name="notes_racikan['+sectionItem+']" value="'+notes_racikan+'">'
            +'<input type="hidden" name="is_package['+sectionItem+']" value="'+is_package+'">'
            +'<input type="hidden" name="unit_name_checked['+sectionItem+']" value="'+unit_name_checked+'">'
            +'<input type="hidden" name="instruction_racikan['+sectionItem+']" value="'+instruction_racikan+'">'
            +'<input type="hidden" name="another_qty['+sectionItem+']" value="'+another_qty+'">'
            +'</td>';
    if (id_item != "") {
        $("#form-add #data-medicine #item_medicine-"+id_item).html(section);
    } else {
        $("#form-add #data-medicine").append('<tr class="template_medicine" id="item_medicine-'+numMedicine+'">'+section+'</tr>');
        numMedicine++;
    }

    addMedikamentosa(
        `#item_medicine-${sectionItem}`,
        (racikan == '' ? name : racikan+'('+instruction_racikan+')'), 
        signa1+' x '+signa2,
        (racikan != '' ? qty_package : value_unit_name_checked),
        (racikan != '' ? {'takaran':unit_dose_racikan, 'instruction': instruction_racikan} : unit_name_checked),
        id_item
    );
}

$('#medicine').on('show.bs.modal', async function (e) {
    num_medicine = 1;

    $("#medicine select[name='unit_racikan']").select2({
        placeholder: "{{ trans('messages.unit') }}"
    });

    $("#medicine select[name='time']").select2({
        placeholder: "{{ trans('messages.input_time') }}"
    });

    $(".list_content").each(function(key, val){
        if(key > 0) {
            val.remove()
        }
    });
    
    resetMedicine();
    let id = $(e.relatedTarget).attr('data-id');
    if (id == undefined || id == "") {
        dose = "";
        unit_dose = "";
        duration = "";
        $("#medicine input[name='medicine_racikan']").val("").trigger("change");
        $("#medicine input[name='qty_racikan']").val("");
        $("#medicine select[name='unit_racikan']").val("").trigger("change");
        $("#title-notes").html("");
        $("#notif-stock-concoction").html("");
        $("#medicine input[name='id_item']").val("");
        $("#medicine input[name='id_medicine']").val("");
        $("#medicine input[name='id']").val("");
        $("#medicine input[name='unit_medicine']").val("");
        $("#medicine input[name='signa2']").val("");
        $("#medicine input[name='signa1']").val("");
        $("#medicine input[name='duration']").val("");
        $("#medicine input[name='duration_racikan']").val("");
        $("#interaction-group").removeClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#duration-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $("#medicine select[name='signa3']").val("per hari").trigger("change");
        $("#medicine select[name='time']").val("per hari").trigger("change");
        $("#title-medicine").html('<?php echo trans('messages.add_medicine_medical_records'); ?>');
    } else {
        $("#duration-group").removeClass('hide');
        $("#one_day-group").removeClass('hide');
        $("#qty_radio_button-group").removeClass('hide');
        $("#interaction-group").removeClass('hide');
        $("#label-qty").removeClass('hide');
        $("#medicine input[name='signa1']").val(br2nl($("#form-add input[name='signa1["+id+"]']").val()));
        $("#medicine input[name='signa2']").val(br2nl($("#form-add input[name='signa2["+id+"]']").val()));
        $("#medicine input[name='instructions']").val($("#form-add input[name='instructions["+id+"]']").val());
        $("#medicine input[name='id_item']").val(id);
        
        if ($("#form-add input[name='is_racikan["+id+"]']").val() == '1'){
            $("#medicine input[name='racikan']").val($("#form-add input[name='racikan["+id+"]']").val());
            $("#medicine input[name='is_racikan']").click();
            
            $("#medicine input[name='instruction_racikan']").val($("#form-add input[name='instruction_racikan["+id+"]']").val());
            $("#medicine input[name='unit_dose']").val($("#form-add input[name='unit_dose["+id+"]']").val());
            $("#medicine select[name='time']").val($("#form-add input[name='time["+id+"]']").val()).trigger("change");
            $("#medicine input[name='duration_racikan']").val($("#form-add input[name='duration["+id+"]']").val());

            if($("#form-add input[name='is_package["+id+"]']").val() == '1') {
                $("#medicine input[name='package']").prop("checked", true).change();
                $("#medicine input[name='qty_package']").val($("#form-add input[name='qty_package["+id+"]']").val());
                $("#medicine select[name='type_package']").val($("#form-add input[name='type_package["+id+"]']").val()).trigger("change");
            }else {
                $("#medicine input[name='package']").prop("checked", false).change();
                $("#medicine input[name='qty_package']").val("");
                $("#medicine select[name='type_package']").val("").trigger("change");
            }

            let medicine = [];
            let quantity = [];
            let unit = [];
            $("#form-add input[name='text_medicine_racikan["+id+"]']").each(function(key, val) {
                medicine[key] = $(val).val();
            });
            $("#form-add input[name='qty_racikan["+id+"]']").each(function(key, val) {
                quantity[key] = $(val).val();
            });
            $("#form-add input[name='unit_racikan["+id+"]']").each(function(key, val) {
                unit[key] = $(val).val();
            });

            try {
                await addTemplateConcoction(medicine, quantity, unit);
            } catch(err) {
                console.error(err)
            }
        }else {
            let medicine
            try {
                const medicineRequest = await getMedicine($("#form-add input[name='id_medicine["+id+"]']").val());
                medicine = medicineRequest.data[0];
            } catch (error) {
                console.error(error);
            }

            dose = $("#form-add input[name='signa1["+id+"]']").val();
            unit_dose = $("#form-add input[name='signa2["+id+"]']").val();
            duration = $("#form-add input[name='duration["+id+"]']").val();
            $("#medicine input[name='medicine_name']").val($("#form-add input[name='medicine_name["+id+"]']").val()).trigger("change");
            $("#medicine input[name='id']").val($("#form-add input[name='id_medicine["+id+"]']").val());
            $("#medicine input[name='id_medicine']").val($("#form-add input[name='id_medicine["+id+"]']").val());
            $("#medicine input[name='medicine_code']").val($("#form-add input[name='medicine_code["+id+"]']").val());
            $("#medicine select[name='signa3']").val($("#form-add input[name='name_signa3["+id+"]']").val()).trigger("change");
            $("#medicine input[name='duration']").val($("#form-add input[name='duration["+id+"]']").val())
            
            medicine && setQtyUnitMedicine(medicine);

            $("#medicine #qty_radio_button-group").find('input[name="qty_radio_button"]').each(function(key, val) {
                if( $(this).is("#change_unit") && $("#form-add input[name='another_qty["+id+"]']").val() == '1') {
                    $(this).attr('checked', true);
                }else {
                    if( $(this).data('id') === $("#form-add input[name='id_unit["+id+"]']").val() ) {
                        $(this).attr('checked', true)
                    }
                }
            });

            setValueQty($("#medicine input[name='signa1']").val() * $("#medicine input[name='signa2']").val() * $("#medicine input[name='duration']").val())
            
            $("#medicine select[name='unit_add']").val($("#form-add input[name='id_unit["+id+"]']").val()).trigger('change');
            $("#medicine input[name='qty_change_unit']").val($("#form-add input[name='qty_unit_checked["+id+"]']").val());
            $("#medicine input[name='interaction']").val($("#form-add input[name='interaction["+id+"]']").val());
        }
        $("#title-medicine").html('<?php echo trans('messages.edit_medicine_medical_records'); ?>'); 
    }
});

$('#medicine').on('hidden.bs.modal', function (e) {
    resetMedicine();
    $('html, body').animate({
        scrollTop: $("#list-medicine").offset().top-100
    }, 0);
});

$("#medicine").submit(function(event) {
    let id_item = $("#medicine input[name='id_item']").val();
    let id_medicine = $("#medicine input[name='id_medicine']").val();
    let id = $("#medicine input[name='id']").val();
    let name = $("#medicine input[name='medicine_name']").val();
    let code = $("#medicine input[name='medicine_code']").val();
    let interaction = $("#medicine input[name='interaction']").val();
    let signa1 = nl2br($("#medicine input[name='signa1']").val());
    let signa2 = nl2br($("#medicine input[name='signa2']").val());
    let signa3 = $("#medicine select[name='signa3'] option:selected").val();
    let duration_medicine = $("#medicine input[name='duration']").val();
    let qty_unit = $("#medicine input[name='qty_radio_button']:checked").val();
    let instructions = $("#medicine input[name='instructions']").val();
    let is_racikan = $("#medicine input[name='is_racikan']:checked").val();
    let racikan = $("#medicine input[name='racikan']").val();

    let unit_dose_racikan = $("#medicine input[name='unit_dose']").val();
    let time_racikan = $("#medicine select[name='time'] option:selected").val();
    let duration_racikan = $("#medicine input[name='duration_racikan']").val();
    let qty_package = $("#medicine input[name='qty_package']").val();
    let type_package = $("#medicine select[name='type_package'] option:selected").val();
    let instruction_racikan = $("#medicine input[name='instruction_racikan']").val();
    let is_package = $("#medicine input[name='package']:checked").val();
    let id_unit_package = $("#medicine select[name='type_package'] option:selected").data('unit');
    let notes_racikan = $("#medicine #notes_concoction").text();

    let qty_unit_name = [];
    let unit_name_checked = "";
    let value_unit_name_checked = "";
    let id_unit_checked = "";
    let another_qty = 0;
    let checked_error = true;
    let stock = medicine_select.stock;
    let value_stock_all = "";
    let first_unit = "";
    let medicine_racikan = [];
    let qty_racikan = [];
    let unit_racikan = [];
    let duration = "";
    let check_same_medicine = [];
    
    $("#medicine input[name='qty_radio_button']").each(function(key, value) {
        if(key === 0) {
            value_stock_all = $(value).val();
            first_unit = $(value).data('name');
        }
        qty_unit_name[key] = $(value).data('name')
        if($(this).prop('checked') == true) {
            if($(this).is("#change_unit")) {
                another_qty = 1;
                unit_name_checked = $("#medicine select[name='unit_add'] option:selected").text();
                value_unit_name_checked = $("#medicine input[name='qty_change_unit']").val();
                id_unit_checked = $("#medicine select[name='unit_add'] option:selected").val();
            }else {
                unit_name_checked = $(value).data('name')
                value_unit_name_checked = $(value).val();
                id_unit_checked = $(value).data('id');
            }
        }
    });

    if(is_racikan == '1') {
        duration = duration_racikan
        $("#medicine input[name='medicine_racikan']").each(function(key, value) {
            if($(value).val() != "") {
                medicine_racikan[key] = {
                    'id_medicine_racikan': $(value).data('id'),
                    'text_medicine_racikan': $(value).val(),
                    'stock': $(value).find("option:selected").data('stock')
                };
                check_same_medicine[key] = $(value).data('id'); 
            }else {
                notif(false, "{{ trans('validation.invalid_medicine_concoction') }}")
                checked_error = false;
            }
            
        });

        $("#medicine input[name='qty_racikan']").each(function(key, value) {
            if($(value).val() != "") {
                qty_racikan[key] = $(value).val()
            }else {
                notif(false, "{{ trans('validation.invalid_medicine_concoction') }}")
                checked_error = false;
            }
        });

        $("#medicine select[name='unit_racikan']").each(function(key, value) {
            if($(value).val() != "") {
                unit_racikan[key] = {
                    'id_unit_racikan' : $(value).val(),
                    'text_unit_racikan' : $(value).find('option:selected').text()
                }
            }else {
                notif(false, "{{ trans('validation.invalid_medicine_concoction') }}")
                checked_error = false;
            }
        });

        if(is_package == '1') {
            if(qty_package == "") {
                formValidate(false, ['qty_package', "{{ trans('validation.empty_qty_package') }}", true]);
                checked_error = false;
            } 
            if(type_package == "") {
                formValidate(false, ['type_package', "{{ trans('validation.empty_type_package') }}", true]);
                checked_error = false;
            }
        }else {
            qty_package = 1;
            type_package = "";
            id_unit_package = "";
            is_package = '0';
        }
        
        let temp_ = check_same_medicine.slice().sort();
        for(i = 0; i < temp_.length - 1; i++) {
            if(temp_[i + 1] == temp_[i]) {
                notif(false, "{{ trans('validation.invalid_select_medicine') }}")
                checked_error = false;
            }
        }

        if (!validateMedicineRacikan(racikan, instruction_racikan, signa1, signa2, unit_dose_racikan, duration, instructions)) return;
    }else {
        duration = duration_medicine;
        if (!validateMedicine(name, signa1, signa2, signa3, duration, qty_unit, instructions, value_unit_name_checked, id_unit_checked)) return;
        
        $("#form-add input[name^='id_medicine']").each(function(key, value) {
            if(id_item != "" && key != id_item || id_item == "") {
                if (this.value == id) {
                    notif(false,"{{ trans('validation.medicine_already_exists') }}");
                    checked_error = false;
                }else {
                    checked_error = true;
                }
            }
        });
    }

    if(checked_error) {
        
        processItemMedicine(
            id, name, code, instructions, 
            interaction, signa1, signa2, signa3, 
            duration, qty_unit, is_racikan, racikan, 
            unit_dose_racikan, time_racikan, qty_package, 
            type_package, qty_unit_name, unit_name_checked, 
            value_unit_name_checked, id_unit_checked, 
            another_qty, stock, value_stock_all, 
            instruction_racikan, medicine_racikan,
            qty_racikan, unit_racikan, is_package, id_unit_package, 
            notes_racikan, first_unit, id_item || ""
        );

        if (id_item != "") {
            notif(true,"{{ trans('validation.success_edit_medicine') }}");
        } else { 
            notif(true,"{{ trans('validation.success_add_medicine') }}");
        }
        $("#medicine").modal("toggle");
        resetMedicine();
    }
});

function validateMedicine(name, signa1, signa2, signa3, duration, qty_unit, instructions, value_unit_name_checked, id_unit_checked) {
	!name && formValidate(false,['medicine_name', '{{ trans("validation.empty_medicine") }}', true]);
    !signa1 && notif(false, "{{ trans('validation.empty_qty_dose') }}")
    !signa2 && notif(false, "{{ trans('validation.empty_unit_amount') }}")
    !signa3 && notif(false, "{{ trans('validation.empty_time') }}")
    !duration && notif(false, "{{ trans('validation.empty_qty_duration') }}")
    !qty_unit && notif(false, "{{ trans('validation.empty_qty_unit') }}")
    !value_unit_name_checked && notif(false, "{{ trans('validation.empty_qty_change_unit') }}")
    !id_unit_checked && notif(false, "{{ trans('validation.empty_select_change_unit') }}")
    !instructions && formValidate(false ,['instructions', '{{ trans("validation.empty_instruction_for_use") }}', true]);
	return name && signa1 && signa2 && signa3 && duration && qty_unit && instructions && value_unit_name_checked && id_unit_checked
}

function validateMedicineRacikan(racikan, instruction_racikan, signa1, signa2, unit_dose_racikan, duration, instructions) {
    !racikan && formValidate(false, ['racikan', '{{ trans("validation.empty_name_concoction") }}', true]);
    !instruction_racikan && formValidate(false,['instruction_racikan', "{{ trans('messages.enter_of_instruction_concoction') }}", true]);
    !signa1 && notif(false, "{{ trans('validation.empty_qty_dose') }}")
    !signa2 && notif(false, "{{ trans('validation.empty_unit_amount') }}")
    !unit_dose_racikan && notif(false, "{{ trans('messages.input_of_unit_or_dose') }}")
    !duration && formValidate(false, ['duration_racikan', "{{ trans('messages.add_duration') }}", true])
    !instructions && formValidate(false, ['instructions', '{{ trans("validation.empty_instruction_for_use") }}', true]);
    return racikan && instruction_racikan && signa1 && signa2 && unit_dose_racikan && duration && instructions
}

$("#medicine #medicine_name-group .border-group").click(function() {
    resetValidation('medicine #medicine_name');
});

$("#medicine input[name='signa1']").focus(function() {
    resetValidation('medicine #signa1');
});

$("#medicine input[name='signa2']").focus(function() {
    resetValidation('medicine #signa2');
});

$("#medicine input[name='racikan']").focus(function() {
    resetValidation('medicine #racikan')
});

$("#medicine input[name='instructions']").focus(function() {
    resetValidation('medicine #instructions')
});

$('#form-add select[name=name]').on('change', function() {
    var name = this.value;
    getPatient("{{ $id_clinic }}", name);
});

$("#form-add #schedule-group .border-group").click(function() {
    resetValidation('form-add #schedule');
});

$("#form-add #clinic-group .border-group").click(function() {
    resetValidation('form-add #clinic');
});

$("#form-add #name-group .border-group").click(function() {
    resetValidation('form-add #name');
});

$("#form-add #polyclinic-group .border-group").click(function() {
    resetValidation('form-add #polyclinic');
});

$("#form-add #doctor-group .border-group").click(function() {
    resetValidation('form-add #doctor');
});

$("#form-add input[name='diagnosis']").focus(function() {
    resetValidation('form-add #diagnosis');
});

$("#medicine #type_package-group .border-group").click(function() {
    resetValidation('medicine #type_package')
});

$("#medicine input[name='qty_package']").focus(function() {
    resetValidation('medicine #qty_package')
});

$("#medicine input[name='duration_racikan']").focus(function() {
    resetValidation('medicine #duration_racikan')
});

$("#medicine input[name='unit_dose']").focus(function() {
    resetValidation('medicine #unit_dose')
});

$("#medicine input[name='instruction_racikan']").focus(function() {
    resetValidation('medicine #instruction_racikan')
});


var numFile = 0;
$("#add-file").click(function(){  
  $("#list-file").append('<div id="upload-file_'+numFile+'">'+
    '<input type="file" name="file[]"  style="visibility: hidden;position:absolute;" />'+
    '</div>');

  setTimeout(function(){
    var num = numFile-1;
    $("#upload-file_"+num+" input").click();
    },100);
    
    $("#upload-file_"+numFile+" input").change(function () {
        var num = numFile-1;
        if ($(this)[0].files[0].size > 1000000) {
            notif(false,"{{ trans('validation.max_file_size') }}");
            $("#upload-file_"+num).remove();
        } else {
            $("#upload-file_"+num).append('<div class="list-file"><div class="icon-sec"><img src=\'{{ asset("assets/images/icons/info/attachment.png") }}\' class="icon" /></div> <div class="info-sec">'+$(this)[0].files[0].name+'</div> <a class="remove-file" onclick="$(this).parent().parent().remove()"><img src=\'{{ asset("assets/images/icons/nav/delete-s.png") }}\' /></a></div>'+'</div>');
        }
    });
  numFile++;
});

var kode_kunjungan_bpjs;
var id_mdc;

$("#form-add").submit(function(event) {
    var status = $("#form-add select[name='status'] option:selected").val();
    if(status == '4'){
        $('#confirmAccept').modal('show');
    }else{
        submitDataAdd();
    }
});

$("#form-add input[name='select_icd']").on('input', function(e){
    if (e.target.value){
        resetValidation('form-add #icd');
    }
});

$("#form-add input[name='diagnosis']").on('input', function(e){
    if (e.target.value){
        resetValidation('form-add #diagnosis');
    }
});

$('#form-add select[name="consciousness"]').on('change', function(e){
    if (e.target.value) {
        resetValidation('form-add #consciousness')
    }
});

$('#form-add select[name="status"]').on('change', function(e){
    if (e.target.value) {
        resetValidation('form-add #status')
    }
});

$('#form-add input[name="anamnese"]').on('input', function(e){
    if (e.target.value) {
        resetValidation('form-add #anamnese')
    }
});

$('#form-add input[name="complaint"]').on('input', function(e){
    if (e.target.value) {
        resetValidation('form-add #complaint')
    }
});

$('#form-add input[name="illness"]').on('input', function(e){
    if (e.target.value) {
        resetValidation('form-add #illness')
    }
});

/** Medikamentosa */
let medikamentosa = [];

function addMedikamentosa(section, name, dose, quantity, unit, index) {
    if(index === "") {
        medikamentosa.push({
            "id" : section,
            "name" : name,
            "dose" : dose,
            "quantity" : quantity,
            "unit" : unit
        });
    }else {
        medikamentosa[index] = {
            "id" : section,
           "name" : name,
            "dose" : dose,
            "quantity" : quantity,
            "unit" : unit
        }
    }

    updateMedikamentosa();
}

function removeMedikamentosa(section) {
    if(section) {
        section = section.replace( /^\D+/g, '');
        medikamentosa.splice(section, 1)
    }

    updateMedikamentosa();
}

function updateMedikamentosa() {
    let drugTherapies = '';

    medikamentosa.forEach((section, index) => {
        let {name, dose, unit, quantity} = medikamentosa[index];
        let unit_ = unit.takaran ? unit.takaran : unit;
        let instruction = unit.instruction ? unit.instruction : unit; 
        drugTherapies += `${index ? '\n': ''}${name} - ${dose} ${unit_}, ${quantity} ${instruction}`;
    });
    $('textarea[name=drug-therapy]').val(drugTherapies);
}

function getPrescriptionsFromTable(tableId) {

    let prescriptions = [];

    let counter = 0
    for(i = 1, c = 0; i <= numMedicine; i++, c++) {
        const prescription = {
            signa: {},
            medicines: [],
            concoction: {},
            table_id: i,
        };
        
        let id_medicine = $("#form-add input[name='id_medicine["+i+"]']").val();
        let unit_medicine =  $("#form-add input[name='id_unit["+i+"]']").val();
        let quantity = $("#form-add input[name='qty_unit_checked["+i+"]']").val();
        let type_package = $("#form-add input[name='type_package["+i+"]']").val();
        let unit_package = $("#form-add input[name='id_unit_package["+i+"]']").val();
        let instruction = $("#form-add input[name='instruction_racikan["+i+"]']").val();
        let dosage = $("#form-add input[name='unit_dose["+i+"]']").val();
        let racikan = $("#form-add input[name='racikan["+i+"]']").val();
        let is_package = $("#form-add input[name='is_package["+i+"]']").val();
        let quantity_package = $("#form-add input[name='qty_package["+i+"]']").val();
        let is_racikan = $("#form-add input[name='is_racikan["+i+"]']").val();
        let per_day = $("#form-add input[name='signa2["+i+"]']").val();
        let dose = $("#form-add input[name='signa1["+i+"]']").val();
        let total_day = $("#form-add input[name='duration["+i+"]']").val();
        let instruction_for_use = $("#form-add input[name='instructions["+i+"]']").val();
        let time_frame = $("#form-add input[name='name_signa3["+i+"]']").val();
        let time_frame_racikan = $("#form-add input[name='time["+i+"]']").val();
        let notes_racikan = $("#form-add input[name='notes_racikan["+i+"]']").val();
        let unit_name_checked = $("#form-add input[name='unit_name_checked["+i+"]']").val();

        prescription['signa']['per_day'] = per_day;
        prescription['signa']['dose'] = dose;
        prescription['signa']['total_day'] = total_day;
        prescription['signa']['instruction_for_use'] = instruction_for_use;
        prescription['signa']['unit_name_checked'] = unit_name_checked;

        if(is_racikan == '1') {
            let index = counter + 1
            $("#form-add input[name^='medicine_racikan["+ i +"']").each(function(key, value) {
                if(this.value != "") {  
                    prescription['medicines'][key] = prescription['medicines'][key] || {};
                    prescription['medicines'][key]['id_medicine'] = this.value;
                }
            });
            $("#form-add input[name^='unit_racikan["+ i +"]']").each(function(key, value) {
                if(this.value != "") {
                    prescription['medicines'][key] = prescription['medicines'][key] || {};
                    prescription['medicines'][key]['id_unit'] = this.value;
                }
            });
            $("#form-add input[name^='qty_racikan["+ i +"]']").each(function(key, value) {
                if(this.value != "") {
                    prescription['medicines'][key] = prescription['medicines'][key] || {};
                    prescription['medicines'][key]['quantity'] = this.value;
                }
            });

            prescription['concoction']['id_inventory_package'] = type_package;
            prescription['concoction']['id_unit_package'] = unit_package
            prescription['concoction']['instruction'] = instruction
            prescription['concoction']['dosage'] = dosage;
            prescription['concoction']['name'] = racikan;
            prescription['concoction']['need_package'] = is_package;
            prescription['concoction']['total_package'] = quantity_package;
            prescription['concoction']['notes'] = notes_racikan;
            prescription['signa']['time_frame'] = time_frame_racikan;

            let count_no = 0;
            $("#form-add input[name^='is_racikan']").each(function(key, value) {
                if(this.value == "1"){
                    count_no++
                    if(key < 10){
                        prescription['concoction']['no_racikan'] = "R.0"+count_no;
                    }else{
                        prescription['concoction']['no_racikan'] =  "R."+count_no;
                    }
                }
            });
        } else {
            prescription['signa']['time_frame'] = time_frame;
            prescription['medicines'][c] = {};
            prescription['medicines'][c]['id_unit'] = unit_medicine;
            prescription['medicines'][c]['id_medicine'] = id_medicine;
            prescription['medicines'][c]['quantity'] = quantity;
            prescription['concoction'] = null;
        }

        prescriptions.push(prescription)
        
    }

    if(tableId) {
        prescriptions = prescriptions.filter(v => v.table_id === tableId);
    }

    return prescriptions;
}

function submitDataAdd(){

    resetValidation('form-add #schedule','form-add #clinic','form-add #name','form-add #polyclinic','form-add #doctor','form-add #diagnosis', 'form-add #anamnese', 'form-add #complaint', 'form-add #icd');
    var clinic = "{{ $id_clinic }}";
    var schedule = $("#form-add select[name='schedule'] option:selected").val();
    var polyclinic = $("#form-add select[name='polyclinic'] option:selected").val();
    var doctor = $("#form-add select[name='doctor'] option:selected").val();
    var name = $("#form-add select[name='name'] option:selected").val();
    var status = $("#form-add select[name='status'] option:selected").val();
    var status_title = $("#form-add select[name='status'] option:selected").text();
    var anamnese = $("#form-add input[name='anamnese']").val();
    var complaint = $("#form-add input[name='complaint']").val();
    var note = $("#form-add input[name='note']").val();
    var diagnosis = $("#form-add input[name='diagnosis']").val();
    var sistole = $("#form-add input[name='blood_sistole']").val() && $("#form-add input[name='blood_sistole']").val().replace(/,/g, '.');
    var diastole = $("#form-add input[name='blood_diastole']").val() && $("#form-add input[name='blood_diastole']").val().replace(/,/g, '.');
    var pulse = $("#form-add input[name='pulse']").val() && $("#form-add input[name='pulse']").val().replace(/,/g, '.');
    var respiratory = $("#form-add input[name='respiratory_frequency']").val() && $("#form-add input[name='respiratory_frequency']").val().replace(/,/g, '.');
    var body_temp = $("#form-add input[name='body_temperature']").val() && $("#form-add input[name='body_temperature']").val().replace(/,/g, '.');
    var height = $("#form-add input[name='height']").val() && $("#form-add input[name='height']").val().replace(/,/g, '.');
    var head = $("#form-add input[name='head']").val() && $("#form-add input[name='head']").val().replace(/,/g, '.');
    var weight = $("#form-add input[name='weight']").val() && $("#form-add input[name='weight']").val().replace(/,/g, '.');
    var abdomen = $("#form-add input[name='abdomen']").val() && $("#form-add input[name='abdomen']").val().replace(/,/g, '.');
    var imt = $("#form-add input[name='imt']").val() && $("#form-add input[name='imt']").data('value');
    var data_consciousness = $("#form-add select[name='consciousness'] option:selected").val();
    var factor_rujuk = $("#form-add select[name='faktor_rujuk'] option:selected").html() ? $("#form-add select[name='faktor_rujuk'] option:selected").html().toLocaleLowerCase() : "";
    var error = false;
    var scrollValidate = true;
    var prognosis = $("#form-add select[name='prognosis'] option:selected").val();

    
    const physicalDataList = Object.keys(physicalData).map(key => {
        resetValidation(`physical-choose-${key}`, `physical-value-${key}`);
        return {
            type: key,
            selected: physicalData[key].selected
        }
    })
    
    physicalDataList.filter(v => v.selected === 'abnormal' && !$(`#physical-value-${v.type}`).val()).forEach(value => {
        error = true;
        formValidate(true, [`physical-value-${value.type}`, `{{trans('messages.condition')}} harus diisi.`, true])
        notif(false, `{{trans('messages.condition')}} abnormal ${physicalData[value.type].title.toLowerCase()} harus diisi`);
    })



    if (schedule == "") {
        formValidate(scrollValidate, ['form-add #schedule','{{ trans("validation.empty_schedule") }}', true]);
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (clinic == "") {
        formValidate(scrollValidate, ['form-add #clinic','{{ trans("validation.empty_clinic") }}', true]);
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (polyclinic == "") {
        formValidate(scrollValidate, ['form-add #polyclinic','{{ trans("validation.empty_polyclinic") }}', true]);
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (doctor == "") {
        formValidate(scrollValidate, ['form-add #doctor','{{ trans("validation.empty_doctor") }}', true]);
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (name == "") {
        formValidate(scrollValidate, ['form-add #name','{{ trans("validation.empty_patient") }}', true]);
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (anamnese == "") {
        formValidate(scrollValidate, ['form-add #anamnese','{{ trans("validation.empty_anamnese") }}', true]);
        $("#section_detail-patient").collapse('show');
        error = true;
        if (scrollValidate == true){
            scrollValidate = false;
        }
    }

    if (complaint == "") {
        formValidate(scrollValidate, ['form-add #complaint','{{ trans("validation.empty_complaint") }}', true]);
        $("#section_detail-patient").collapse('show');
        error = true;
        if (scrollValidate == true){
            scrollValidate = false;
        }
    }

    if (sistole == "" ||
        diastole == "" ||
        pulse == "" ||
        respiratory == "" ||
        body_temp == "" ||
        height == "" ||
        head == "" ||
        weight == ""
        ) {
            notif(false, "{{ trans('validation.empty_vital') }}");
            $("#section_objective").collapse('show');       
            error = true;
            if (scrollValidate == true){
                scrollValidate = false;
            }
    }
    if (data_consciousness == "") {
        formValidate(scrollValidate, ['form-add #consciousness','{{ trans("validation.empty_consciousness") }}', true]);
        $("#section_objective").collapse('show');
        error = true;
        if (scrollValidate == true){
            scrollValidate = false;
        }
    }
    var minService = false;
    $("#form-add input[name^='id_service']").each(function(key, value) {
        minService = true;
    });
    var icd = false;
    $("#form-add input[name^='icd']").each(function(key, value) {
        icd = true;
    });
    if (error == false) {
        if (minService == false) {
            notif(false,"{{ trans('validation.min_service') }}");
            $("#panel-planning").collapse("show");
            error = true;
        }
        if (icd == false) {
            formValidate(false, ['form-add #icd',"{{ trans('validation.min_icd') }}", true]);
            $("#section_diagnosis").collapse("show");   
            error = true;
            if (scrollValidate == true){
                scrollValidate = false;
            }
        }
    }
    if (diagnosis == "") {
        formValidate(false, ['form-add #diagnosis',"{{ trans('validation.empty_diagnosis')}}", true]);
        $("#section_diagnosis").collapse("show");
        error = true;
        if (scrollValidate == true){
            scrollValidate = false;
        } 
    }
    if(status == "") {
        formValidate(scrollValidate, ['form-add #status','{{ trans("validation.empty_status") }}', true]);   
        error = true;
        if (scrollValidate == true) {
            scrollValidate = false;
        }
    }
    if (error == false) {
        formData= new FormData();
        var validate = false;
        var icd = false;
        $("#form-add input[name^='icd']").each(function(key, value) {
            icd = true;
            formData.append("icd["+key+"]", this.value);
        });
        
        if (isBPJS == true){
            formData.append("status", status_title);
            formData.append("bpjs_leave_status_code", status);
            @if(Session::get('pcare'))
                formData.append("consciousness",$("#form-add select[name='consciousness'] option:selected").html().toLocaleLowerCase());
                formData.append("consciousness_kode_bpjs", $("#form-add select[name='consciousness'] option:selected").val());
            @else
                formData.append("consciousness",$("#form-add select[name='consciousness'] option:selected").html().toLocaleLowerCase());
                formData.append("consciousness_kode_bpjs", '');
            @endif
        }else{
            formData.append("consciousness",$("#form-add select[name='consciousness'] option:selected").html().toLocaleLowerCase());
            formData.append("consciousness_kode_bpjs", '');
            formData.append("status", status);
            formData.append("bpjs_leave_status_code", "");
        }

        let counter = 0;
        for(i = 1; i <= numMedicine; i++) {
                let id_medicine = $("#form-add input[name='id_medicine["+ counter +"]']").val();
                let unit_medicine =  $("#form-add input[name='id_unit["+ counter +"]']").val();
                let quantity = $("#form-add input[name='qty_unit_checked["+ counter +"]']").val();
                let type_package = $("#form-add input[name='type_package["+ counter +"]']").val();
                let unit_package = $("#form-add input[name='id_unit_package["+ counter +"]']").val();
                let instruction = $("#form-add input[name='instruction_racikan["+ counter +"]']").val();
                let dosage = $("#form-add input[name='unit_dose["+ counter +"]']").val();
                let racikan = $("#form-add input[name='racikan["+ counter +"]']").val();
                let is_package = $("#form-add input[name='is_package["+ counter +"]']").val();
                let quantity_package = $("#form-add input[name='qty_package["+ counter +"]']").val();
                let is_racikan = $("#form-add input[name='is_racikan["+ counter +"]']").val();
                let per_day = $("#form-add input[name='signa2["+ counter +"]']").val();
                let dose = $("#form-add input[name='signa1["+ counter +"]']").val();
                let total_day = $("#form-add input[name='duration["+ counter +"]']").val();
                let instruction_for_use = $("#form-add input[name='instructions["+ counter +"]']").val();
                let time_frame = $("#form-add input[name='name_signa3["+ counter +"]']").val();
                let time_frame_racikan = $("#form-add input[name='time["+ counter +"]']").val();
                let notes_racikan = $("#form-add input[name='notes_racikan["+ counter +"]']").val();

                formData.append("prescriptions["+ counter +"][signa][per_day]", per_day);
                formData.append("prescriptions["+ counter +"][signa][dose]", dose);
                formData.append("prescriptions["+ counter +"][signa][total_day]", total_day);
                formData.append("prescriptions["+ counter +"][signa][instruction_for_use]", instruction_for_use);

                if(is_racikan == '1') {
                    $("#form-add input[name^='medicine_racikan["+ counter +"']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ counter +"][medicines]["+key+"][id_medicine]", this.value)
                        }
                    });

                    $("#form-add input[name^='unit_racikan["+ counter +"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ counter +"][medicines]["+key+"][id_unit]", this.value)
                        }
                    });

                    $("#form-add input[name^='qty_racikan["+ counter +"]']").each(function(key, value) {
                        if(this.value != "") {
                            formData.append("prescriptions["+ counter +"][medicines]["+key+"][quantity]", this.value)
                        }
                    });

                    formData.append("prescriptions["+ counter +"][concoction][id_inventory_package]", type_package);
                    formData.append("prescriptions["+ counter +"][concoction][id_unit_package]", unit_package ? unit_package : "");
                    formData.append("prescriptions["+ counter +"][concoction][instruction]", instruction);
                    formData.append("prescriptions["+ counter +"][concoction][dosage]", dosage);
                    formData.append("prescriptions["+ counter +"][concoction][name]", racikan);
                    formData.append("prescriptions["+ counter +"][concoction][need_package]", is_package);
                    formData.append("prescriptions["+ counter +"][concoction][total_package]", quantity_package);
                    formData.append("prescriptions["+ counter +"][concoction][notes]", notes_racikan);
                    formData.append("prescriptions["+ counter +"][signa][time_frame]", time_frame_racikan);

                    let count_no = 0;
                    $("#form-add input[name^='is_racikan']").each(function(key, value) {
                        if(this.value == "1"){
                            count_no++
                            if(key < 10){
                                formData.append("prescriptions["+ counter +"][concoction][no_racikan]", "R.0"+count_no+"");
                            }else{
                                formData.append("prescriptions["+ counter +"][concoction][no_racikan]", "R."+count_no+"");
                            }
                        }
                    });
                }else {
                    formData.append("prescriptions["+ counter +"][signa][time_frame]", time_frame);
                    formData.append("prescriptions["+ counter +"][medicines]["+ counter +"][id_unit]", unit_medicine)
                    formData.append("prescriptions["+ counter +"][medicines]["+ counter +"][id_medicine]", id_medicine);
                    formData.append("prescriptions["+ counter +"][medicines]["+ counter +"][quantity]", quantity)
                    formData.append("prescriptions["+ counter +"][concoction]", "")
                }

                counter++
        }
        
        
        var service = false;
        $("#form-add input[name^='id_service']").each(function(key, value) {
            service = true;
            formData.append("service["+key+"][0]", this.value);
        });

        $("#form-add input[name^='service_quantity']").each(function(key, value) {
            formData.append("service["+key+"][1]", this.value);
        });

        $("#form-add input[name^='service_hasil']").each(function(key, value) {
            formData.append("service["+key+"][2]", this.value);
        });
        
        if (service == false) {
           formData.append("service", ""); 
        }
        if(validate == false) {
            var keyFile = 0;
            $("#form-add input[name^='file']").each(function(key, value) {
               try {
                    formData.append("attachment["+keyFile+"]", this.files[0]);
                    keyFile++;
                }
                catch(err) {}
            });
        }
        @if(!empty($id_schedule))
            formData.append("id_schedule", "{{ $id_schedule }}");
        @endif
        var status_tacc = $("#form-add input[name='status_tacc']:checked").val();
        if (status_tacc == undefined) {
            status_tacc = "";
        }
        var specialist = $("#form-rujukan input[name='is_specialist']:checked").val();
        if (specialist == undefined) {
            specialist = "";
        }else if(specialist == '1'){
            formData.append("sub_specialist", $("#form-rujukan select[name='subspecialist'] option:selected").val());
            formData.append("sarana", $("#form-rujukan select[name='tool'] option:selected").val());
        }else if(specialist == '0'){
            var code = $("#form-rujukan select[name='category_bpjs'] option:selected").val();
            var jmlReason = $("#form-add input[name='reason_notacc']").val();
            if (status_tacc == 'tanpa tacc'){
                if(jmlReason.length < 20){
                    formValidate(scrollValidate, ['form-add #reason_notacc', 'Minimal alasan 20 karakter', true]);
                }else{
                    formData.append("khusus", code);
                    if(code == "THA" || code == "HEM"){
                        formData.append("sub_specialist", $("#form-rujukan select[name='subcategory_bpjs'] option:selected").val());
                    }
                }
            }else{
                formData.append("khusus", code);
                if(code == "THA" || code == "HEM"){
                    formData.append("sub_specialist", $("#form-rujukan select[name='subcategory_bpjs'] option:selected").val());
                }
            }
        }
        formData.append("id_schedule", schedule);
        formData.append("validate", validate);
        formData.append("id_clinic", "{{ $id_clinic }}");
        formData.append("id_polyclinic", polyclinic);
        formData.append("id_doctor", doctor);
        formData.append("id_patient", name);
        formData.append("anamnese", nl2br(anamnese));
        formData.append("complaint", nl2br(complaint));
        formData.append("diagnosis", nl2br(diagnosis));
        formData.append("prognosis", prognosis);
        formData.append("blood_sistole", sistole)
        formData.append("blood_diastole", diastole);
        formData.append("pulse", pulse);
        formData.append("respiratory_frequency", respiratory);
        formData.append("body_temperature", body_temp);
        formData.append("height", height);
        formData.append("head", head);
        formData.append("weight", weight);
        formData.append("abdomen", abdomen);
        formData.append("imt", imt);
        formData.append("additional_notes", note);
        var arr = [];
        $("#form-diagnostic input[name=type_diagnostic]:checked").each(function(){
            arr.push($(this).val());
        });
        formData.append("type_diagnostic", arr);
        $.each(data_import, function(i, file){
            formData.append("attachment["+i+"]", file)
        });
        if (validate == false) {
            var keyFile = 0;
            $("#form-diagnostic [name^='diagnostic_helper']").each(function(key, value) {
                try {
                    if(keyFile == 0){
                        if(isBPJS == true){
                            @if(Session::get('pcare'))
                                formData.append("diagnostic_helper["+keyFile+"]", ($("#form-diagnostic select[name='diagnostic_helper["+keyFile+"]'] option:selected").val() != undefined ? $("#form-diagnostic select[name='diagnostic_helper["+keyFile+"]'] option:selected").val() : ''));
                            @else
                                formData.append("diagnostic_helper["+keyFile+"]", '');
                            @endif
                        }else{
                            formData.append("diagnostic_helper["+keyFile+"]", '');
                        }
                    }else if(keyFile == 1){
                        formData.append("diagnostic_helper["+keyFile+"]", convertDate($("#form-diagnostic input[name='diagnostic_helper["+keyFile+"]']").val()));
                    }else if(keyFile == 29){
                        formData.append("diagnostic_helper["+keyFile+"]", $("#form-diagnostic textarea[name='diagnostic_helper["+keyFile+"]']").val());
                    }else if(keyFile == 30){
                        if(isBPJS == true){
                            @if(Session::get('pcare'))
                                formData.append("diagnostic_helper["+keyFile+"]", ($("#form-diagnostic select[name='diagnostic_helper[0]'] option:selected").html() != undefined ? $("#form-diagnostic select[name='diagnostic_helper[0]'] option:selected").html() : ''));
                            @else
                                formData.append("diagnostic_helper["+keyFile+"]", '');
                            @endif
                        }else{
                            formData.append("diagnostic_helper["+keyFile+"]", '');
                        }
                    }else{
                        formData.append("diagnostic_helper["+keyFile+"]", $("#form-diagnostic input[name='diagnostic_helper["+keyFile+"]']").val());
                    }
                    keyFile++;
                }
                catch(err) {}
            });
        }
        var data_illness = new Array();
        $('#riwayat :selected').each(function (i, selected) {
            data_illness[i] = $(selected).val();
            formData.append("illnesses_history["+i+"]", data_illness[i]);
        });
        const data_allergy = new Array();
        $('#riwayat-alergi :selected').each(function (i, selected) {
            data_allergy[i] = $(selected).val();
            formData.append("allergies_history["+i+"]", data_allergy[i]);
        });
        formData.append("drug_therapy", $("#form-add textarea[name='drug-therapy']").val() || "");
        formData.append("non_drug_therapy", $("#form-add textarea[name='non-drug-therapy']").val() || "");
        formData.append("code_reference_hospital", $("#form-add input[name='code_reference_hospital']").val());
        formData.append("specialist", specialist);
        formData.append("date_to_reference", $("#form-add input[name='date_to_reference']").val());
        formData.append("date_end", convertDate($("#form-add input[name='date_end']").val()));
        formData.append("notes", $("#form-add input[name='notes']").val());
        formData.append("name_specialist", $("#form-add input[name='name_specialist']").val());
        formData.append("status_tacc", status_tacc);
        formData.append("faktor_rujuk", factor_rujuk);
        formData.append("faktor_rujuk_kode_bpjs", $("#form-add select[name='faktor_rujuk'] option:selected").val() || "");
        if (status_tacc == 'tanpa tacc'){
            formData.append("reason", $("#form-add input[name='reason_notacc']").val());
        }else if(status_tacc == 'dengan tacc'){
            if ($("#form-add select[name='faktor_rujuk'] option:selected").val() != 3){
                formData.append("reason", $("#form-add select[name='reason'] option:selected").val());
            }else{
                var reason = '';
                $("#form-add input[name^='icdReason']").each(function(key, value) {
                    if(key != 0 ){
                        reason += ', '+this.value
                    }else{
                        reason += this.value
                    }
                });
                formData.append("reason", reason);
            }
        }
        formData.append("date_kunjungan", date_kunjungan);
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");
        if ("{{ $level == 'doctor' }}") {
            formData.append("id_clinic", "{{ $id_clinic }}");
            formData.append("id_polyclinic", "{{ $id_polyclinic }}");
        }
        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");
        $(".button-confirmation").addClass("loading");
        $(".button-confirmation span").removeClass("hide");
        event.preventDefault();
        $("#form-add button").attr("disabled", true);
        $(".button-confirmation").attr("disabled", true);

        // physical examinations
        Object.keys(physicalData).filter(key => physicalData[key].selected).forEach((key, index) => {
            formData.append("physical_examinations["+index+"][type]", key);
            formData.append("physical_examinations["+index+"][value]", $(`#physical-value-${key}`).val());    
        });        
    
        // load vaccine item cache to formdata
        appendVaccineItemToFormData(formData);

        // load vaccine history cache to formData
        appendVaccineHistoryToFormData(formData);

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/medical-records",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $("#form-add button").attr("disabled", false);
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");
                $(".button-confirmation").removeClass("loading");
                $(".button-confirmation span").addClass("hide");
                $(".button-confirmation").attr("disabled", false);

                if (!data.error) {
                    if (!data.success) {
                        if (data.errors){
                            formValidate(scrollValidate, ['form-add #diagnosis',data.errors.diagnosis, true]);
                        }
                        if (data.message) {
                            notif(false,""+data.message+"");
                        }
                        $("#form-add #section_diagnosis").collapse("show");
                    } else {
                        if (validate == false) {
                            kode_kunjungan_bpjs = data.kode_kunjungan_bpjs;
                            id_mdc = data.id;
                            resetAdd();
                            notif(true,"{{ trans('validation.success_save_medical_records') }}");
                            if (status == '4'){
                                $('#printLetter').modal('show');
                                $('#confirmAccept').modal('hide');
                            }else{
                                setTimeout(function(){
                                    loading('#/{{ $lang }}/medical/'+data.id+'');
                                    redirect('#/{{ $lang }}/medical/'+data.id+'');               
                                },500);
                            }
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-add button").attr("disabled", false);
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");
                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
}

$('#printLetter').on('hidden.bs.modal', function () {
    setTimeout(function(){
        loading('#/{{ $lang }}/medical/'+id_mdc+'');
        redirect('#/{{ $lang }}/medical/'+id_mdc+'');               
    },500);
});

$('#printLetter #confirm').click(function() {
    var value = $("#formCheck input[name='letter']:checked").val();
    if(value){
        if (value != '0'){
            $("#printLetter #confirm").addClass("loading");
            $("#printLetter #confirm span").removeClass("hide");
            $("#printLetter #confirm").attr("disabled", true);
            rujuk(kode_kunjungan_bpjs);
        }else{
            $("#printLetter #confirm").addClass("loading");
            $("#printLetter #confirm span").removeClass("hide");
            $("#printLetter #confirm").attr("disabled", true);
            rujukRefer(kode_kunjungan_bpjs);
        }
    }else {
        formValidate(false, ['formCheck #letter' ,"{{ trans('validation.empty_printLetter') }}", true])
    }
});

$("#printLetter input[name='letter']").on('input', function(e){
    if(e.target.value) {
        resetValidation("formCheck #letter");
    }
});

$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});

$('#form-add select[name=status]').on('change', function() {
    var value = $(this).val();
    if (value == '4'){
        $("#form-add #search_rujukan-group").removeClass("hide");
    }else{
        $("#form-add #search_rujukan-group").addClass("hide");
    }
    checkingTACC();
});

function checkingTACC(){
    var value = $("#form-add select[name='status'] option:selected").val();
    var validation = check.some(e => e.is_specialist === 1);
    if (check.length != 0){
        if (value =='4' && !validation) {
            $("#form-add #diagnosa-text").removeClass("hide");
            $('#form-add #tacc-group').removeClass('hide');
            $('#form-add #tacc').removeClass('hide');
            $('#form-add #noTacc').addClass('hide');
            $('#form-add #reason-group').addClass('hide');
            $('#form-add #reason_complication-group').addClass('hide');
        } else {
            numICDReason = 0;
            $("#form-add #reference_selected-group").addClass("hide");
            $("#form-add #diagnosa-text").addClass("hide");
            $('#form-add #tacc-group').addClass('hide');
            $('#form-add #tacc').addClass('hide');
            $('#form-add #noTacc').addClass('hide');
            $('#form-add #reason-group').addClass('hide');
            $('#form-add #reason_complication-group').addClass('hide');
            $("#form-add #list-complicated").addClass("hide");
            $("#form-add input[name='status_tacc']").prop('checked', false);
            $("#form-add input[name='reason_notacc']").val('');
            $("#form-add select[name=faktor_rujuk]").val('').trigger("change");
            $("#form-add select[name=reason]").val('').trigger("change");
            $("#form-add #data-complicated").html("");
            $("#form-add input[name='name_ppk']").val('');
            $("#form-add input[name='name_specialist']").val('');
            $("#form-add input[name='date_to_reference_string']").val('');
            $("#form-add input[name='notes']").val('');
        }
    } else {
        numICDReason = 0;
        $("#form-add #diagnosa-text").addClass("hide");
        $('#form-add #tacc-group').addClass('hide');
        $('#form-add #tacc').addClass('hide');
        $('#form-add #noTacc').addClass('hide');
        $('#form-add #reason-group').addClass('hide');
        $('#form-add #reason_complication-group').addClass('hide');
        $("#form-add #list-complicated").addClass("hide");
        $("#form-add input[name='status_tacc']").prop('checked', false);
        $("#form-add input[name='reason_notacc']").val('');
        $("#form-add select[name=faktor_rujuk]").val('').trigger("change");
        $("#form-add select[name=reason]").val('').trigger("change");
        $("#form-add #data-complicated").html("");
    }
}


function resetSubjective() {
    $("#form-add input[name='anamnese']").val("");
    $("#form-add input[name='complaint']").val("");
    $("#riwayat-group .border-group select").html('');
}
function resetObjective() {
    var keyFile = 0
    $("#anamnese").val("");
    $("#complaint").val("");
    $("#notes").val("");
    $("#consciousness").val("").trigger("change");    
    $("#images-to-upload").html("");
    $("#data-diagnostic").html("");
    $("#add-diagnostik").html("+ Tambah data penunjang");
    $("#date-diagnostic").text('');
    $("#form-diagnostic [name^='diagnostic_helper']").each(function(key, value) {
        $("#form-diagnostic select[name='diagnostic_helper["+keyFile+"]']").val("").trigger("change");;
        $("#form-diagnostic input[name='diagnostic_helper["+keyFile+"]']").val("");
        $("#form-diagnostic textarea[name='diagnostic_helper["+keyFile+"]']").val("");
        keyFile++
    });
    $("#form-diagnostic input[name=type_diagnostic]").prop('checked', false);
    $("#blood_pressure-group").addClass("hide");
    $("#radiologi-group").addClass("hide");
    $("#routine_blood-group").addClass("hide");
    $("#blood_fat-group").addClass("hide");
    $("#blood_sugar-group").addClass("hide");
    $("#liver-group").addClass("hide");
    $("#kidney-group").addClass("hide");
    $("#heart-group").addClass("hide");
}

function resetAssessment() {
    $("#form-add #data-icd").html("");
    $("#form-add #list-icd").addClass("hide");
    $("#form-add input[name='diagnosis']").val("");
}

function resetPlan() {
    $("#form-add #data-service").html("");
    $("#form-add #list-service").addClass("hide");
    $("#form-add #data-medicine").html("");
    $("#form-add #list-medicine").addClass("hide");
    $("#form-add input[name='monitoring']").val("");
    $("#form-add textarea[name='drug-therapy']").val("")
    $("#form-add #list-complicated").addClass("hide");
    $("#form-add #data-complicated").html("");
    medikamentosa = [];
}


</script>

@include('medical_records.print')
@include('medical_records.history')
@include('medical_records.surat_rujukan')
@include('_partial.rujukan')
@include('_partial.diagnostic')
@include('_partial.confirm_delele')