@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/rekam-medis.png') }}" />
                    <h3>
                        {{ trans('messages.medical_records') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/medical"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>

                @if(isset($acl->medical_records))
                @if($acl->medical_records->create==true)
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/medical/create" onclick="loading($(this).attr('href'));">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.add_medical_records') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/vital_sign/create" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/tanda-vital.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.preliminary_examination') }}</span>
                        </div>
                    </a>
                </div>
                @endif
                @endif

                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="polyclinic-group">
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="doctor-group">
                                    <div class="border-group">
                                        <select class="form-control" name="doctor">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span> 
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="payment-group">
                                    <div class="border-group">
                                        <select class="form-control" name="payment">
                                            <option value=""></option>
                                            <option value="all">Semua Status</option>
                                            <option value="unpaid">Belum Bayar</option>
                                            <option value="halfpaid">Belum Lunas</option>
                                            <option value="paidoff">Lunas</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span> 
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group column-group">
                                    <div id="datepicker_birth_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_birth_date') }}" name="birth_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row search">
                            <div class="col-md-4 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_name_mrn') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.patient') }}</th>
                                <th class="hide-lg">{{ trans('messages.doctor') }}</th>
                                <th class="hide-md">{{ trans('messages.date') }}</th>
                                <th class="hide-md">{{ trans('messages.status') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')


<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if("{{Request::is('*medical*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#medical-records-bar").addClass('active-bar')
}else {
    $(".side-menus ul li a").removeClass('active-bar')
}

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_birth_date').datetimepicker({
    locale: "{{ $lang }}",
    viewMode: 'years',
    format: 'DD/MM/YYYY',
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day})
    });
});

$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day})
        
    });
});

$("#datepicker_birth_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({birth_day: day})
    });
});

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();
dateNowConvert = date.getFullYear()+'-'+monthNow+'-'+dayNow;

$("input[name='from_date']").val(unconvertDate(dateNowConvert));
$("input[name='to_date']").val(unconvertDate(dateNowConvert));

@if($level != 'doctor')
    function listPoly(val) {
        loading_content("#form-search #polyclinic-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(dataPoly){

                if(!dataPoly.error) {
                    loading_content("#form-search #polyclinic-group", "success");

                    var item = dataPoly.data;

                    $("#form-search select[name='polyclinic']").html("<option value=''></option><option value='all'>{{ trans('messages.all_polyclinic') }}</option>");
                    for (var i = 0; i < item.length; i++) {
                        $("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-search select[name='polyclinic']").select2({
                        placeholder: "{{ trans('messages.select_polyclinic') }}",
                        allowClear: false
                    });

                    if(val!="") {
                        $("#form-search select[name='polyclinic']").val(val).trigger("change");
                    }

                    
                } else {
                    loading_content("#form-search #polyclinic-group", "failed");

                    $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
                }
            },
            error: function(){
                loading_content("#form-search #polyclinic-group", "failed");

                $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        })
    }
    listPoly(polyclinicRepo);

    function listDoctor(polyclinic, val) {
        loading_content("#form-search #doctor-group", "loading");

        var search = "";
        if(polyclinic!="") {
            search = "?id_polyclinic="+polyclinic;
        }

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/doctor"+search+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(dataDoctor){
                if(!dataDoctor.error) {
                    loading_content("#form-search #doctor-group", "success");

                    var item = dataDoctor.data;

                    $("#form-search select[name=doctor]").html("");

                    if(item.length==0) {
                        $("#form-search select[name=doctor]").prop("disabled", true);
                        $("#form-search select[name=doctor]").append("<option value=''>{{ trans('messages.no_doctor') }}</option>");
                        $("#form-search select[name=doctor]").select2({
                            placeholder: "{{ trans('messages.no_doctor') }}",
                            allowClear: false
                        });
                    } else {
                        $("#form-search select[name=doctor]").prop("disabled", false);

                        $("#form-search select[name=doctor]").append("<option value=''></option><option value='all'>{{ trans('messages.all_doctor') }}</option>");

                        for (var i = 0; i < item.length; i++) {
                            $("#form-search select[name=doctor]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                        }

                        $("#form-search select[name=doctor]").select2({
                            placeholder: "{{ trans('messages.select_doctor') }}",
                            allowClear: false
                        });

                        if(val!="") {
                            $("#form-search select[name='doctor']").val(val).trigger("change");
                        }
                    }
                } else {
                    loading_content("#form-search #doctor-group", "failed");

                    $("#form-search #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
                }
                
            },
            error: function(){
                loading_content("#form-search #doctor-group", "failed");

                $("#form-search #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
            }
        });
    }
    listDoctor(polyclinicRepo, doctorRepo);

@else
    $("#form-search select[name='polyclinic']").html("<option value='all'>{{$name_polyclinic}}</option>");
    $("#form-search select[name=polyclinic]").prop("disabled", true);
    $("#form-search select[name='doctor']").html("<option value='all'>{{$name_doctor}}</option>");
    $("#form-search select[name=doctor]").prop("disabled", true);
@endif

$('#form-search select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;

    if(polyclinic=="all") {
        polyclinic = "";
    }
    filterSearch("", "","");
    listDoctor(polyclinic, doctorRepo);
});

$("#form-search select[name='payment']").select2({
    placeholder: "{{ trans('messages.select_payment_status') }}",
    allowClear: false
});


@if(empty($name))
    var keySearch = searchRepo;
@else
    if(searchRepo == "") {
        var keySearch = "{{ $name }}";        
    } else {
        var keySearch = searchRepo;
    }
@endif

@if(empty($birth_date))
    var keyBirthDate = birth_dateRepo;
@else
    if(birth_dateRepo == "") {
        var keyBirthDate = "{{ $birth_date }}";       
    } else {
        var keyBirthDate = birth_dateRepo;
    }
@endif

var keyPoly = polyclinicRepo;
var keyDoctor = doctorRepo;
var numPage = pageRepo;


function search(page, submit, q, polyclinic, doctor, from_date, to_date, birth_date) {
    formData= new FormData();
    var clinic = "";

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if(doctor=="" && submit=="false") {
        var doctor = keyDoctor;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    if(birth_date=="" && submit=="false") {
        var birth_date = keyBirthDate;
    }

    if("{{$level == 'doctor'}}") {
        var doctor = "{{$id_user}}";
        var polyclinic = "{{$id_polyclinic}}";
        clinic = "{{$id_clinic}}";
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyPoly = polyclinic;
    keyDoctor = doctor;
    keyFromDate = from_date;
    keyToDate = to_date;
    keyBirthDate = birth_date;
    numPage = page;
    var payment = $("#form-search select[name=payment] option:selected").val();

    if (payment == 'all') {
        payment = ''
    }

    repository(['search',q],['clinic', clinic], ['polyclinic',polyclinic],['doctor',doctor], ['from_date',from_date],['to_date',to_date],['birth_date',birth_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records",
        type: "GET",
        data: "q="+q+"&id_clinic="+clinic+"&id_polyclinic="+polyclinic+"&id_doctor="+doctor+"&payment="+payment+"&from_date="+from_date+"&to_date="+to_date+"&birth_date="+birth_date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(i==0) {
                                tr = $('<tr class="table-header" />');
                                if(item[i].date==convertDate(dateNow)) {
                                    tr.append("<td colspan='5'>{{ trans('messages.today') }}</td>");
                                } else {
                                    tr.append("<td colspan='5'>"+getFormatDate(item[i].date)+"</td>");
                                }
                                $("#table").append(tr);
                            }

                            var status = "";
                            if(item[i].payment_status.code==1) {
                                status = keel(item[i].payment_status.code)
                            } else {
                                status = keel(item[i].payment_status.code)
                            }

                            tr = $('<tr/>');

                            //tr.append("<td class='photo'>"+thumbnail('sm','patient','{{ $storage }}',item[i].patient.photo,item[i].patient.name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/patient/"+item[i].patient.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].patient.name + "</h1></a><span class='type'>" + item[i].patient.mrn + "</span>"+
                                '<div class="sub show-md">'+
                                    "<span class='info'><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></span>"+
                                    "<span class='general show-sm'>" + getFormatDate(item[i].date) + " "+getFormatBetweenTime(item[i].start_time, item[i].end_time)+"</span>"+
                                    '<span class="info show-sm margin">' + status + '</span>'+
                                '</div>'+
                                "</td>");
                            
                            //tr.append("<td class='photo hide-lg'>"+thumbnail('sm','doctor','{{ $storage }}',item[i].doctor.photo,item[i].doctor.name)+"</td>");
                            tr.append("<td class='hide-lg'><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></td>");

                            tr.append("<td class='hide-md'>" + getFormatDate(item[i].date) + " <br />"+getFormatBetweenTime(item[i].start_time, item[i].end_time)+"</td>");
                            tr.append("<td id='status' class='hide-md'>"+status+"</td>");

                            var action = "<a href='#/{{ $lang }}/medical/"+item[i].id+"'  onclick=\"loading($(this).attr('href'));\" class='btn-table btn-blue'><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /> {{ trans('messages.detail') }}</a>"
                                +"<a onclick=\"reportMedical('"+item[i].id+"')\" class='btn-table btn-purple'><img src=\"{{ asset('assets/images/icons/action/print.png') }}\" /> {{ trans('messages.print') }}</a>";

                            if(item[i].payment!=true) {
                                action = action+"<a href='#/{{ $lang }}/payment/medical/"+item[i].id+"'  onclick=\"loading($(this).attr('href'));\" class='btn-table btn-green'><img src=\"{{ asset('assets/images/icons/action/pay-green.png') }}\" /> {{ trans('messages.pay') }}</a>";
                            }

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            if(i<to) {
                                if(item[i].date!=item[i+1].date) {
                                    tr = $('<tr class="table-header" />');
                                    if(item[i+1].date==convertDate(dateNow)) {
                                        tr.append("<td colspan='5'>{{ trans('messages.today') }}</td>");
                                    } else {
                                        tr.append("<td colspan='5'>"+getFormatDate(item[i+1].date)+"</td>");
                                    }
                                    $("#table").append(tr);
                                }   
                            }

                            no++;
                        } catch(err) {}
                    }
                } else {

                    $(".pagination-sec").addClass("hide");
                        
                    if(page==1 && q=='' && polyclinic=='' && doctor=='' && payment=='' && from_date=='' && to_date=='' && birth_date=='') {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.empty_medical_records') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    }

                }

                pages(page, total, per_page, current_page, last_page, from, to, q, polyclinic, doctor, from_date, to_date, birth_date);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, doctor, from_date, to_date, birth_date); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, doctor, from_date, to_date, birth_date); });
        }
    })
}

function filterSearch(arg) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val();
    var birth = $("#form-search input[name='birth_date']").val();
    var doctor = $("#form-search select[name=doctor] option:selected").val();
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }else if (arg.birth_day){
            birth = arg.birth_day
        }
    }
    if (polyclinic == "all") {
        polyclinic = "";
    }else if (doctor == "all") {
        doctor = "";
    }
    
    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, convertDate(from_date), convertDate(to_date), convertDate(birth));
}

$("#form-search select[name='doctor']").on('change', function(){
    filterSearch()
});

$("#form-search select[name='payment']").on('change', function(){
    filterSearch()
});

$("#form-search").submit(function(event) {
    filterSearch();
});


let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if (e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            filterSearch();
        }, 2000);
    }
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        var doctor = $("#form-search select[name=doctor] option:selected").val();
        if(doctor == "all") {
            doctor = "";
        }

        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name='birth_date']").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        var doctor = $("#form-search select[name=doctor] option:selected").val();
        if(doctor == "all") {
            doctor = "";
        }
        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name='birth_date']").val()));
    }
});

$("#form-search input[name='birth_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic == "all") {
            polyclinic = "";
        }

        var doctor = $("#form-search select[name=doctor] option:selected").val();
        if(doctor=="all") {
            doctor = "";
        }
        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name='birth_date']").val()));
    }
});

search(numPage,"false",keySearch,keyPoly,keyDoctor, dateNowConvert, dateNowConvert, keyBirthDate);

</script>

@include('medical_records.print')