@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/rekam-medis.png') }}" />
                    <h3>
                        {{ trans('messages.medical_records') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/medical"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>                
                <div class="search-sec">
                    <div id="section-profile" class="detail-profile-form hide">
                        <div class="column" id="patient-photo">
                        </div>
                        <div class="column">
                            <h1><a id="patient-name"></a></h1>
                            <p id="patient-email"></p>
                            <p id="patient-phone"></p>
                        </div>
                    </div>
                    <div class="card" id="loading-profile">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                    <div class="card hide" id="reload-profile">
                        <span class="reload fa fa-refresh"></span>
                    </div>    
                    <form  id="form-search">            
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group label-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group content-column label-group column-group" id="polyclinic-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group column-group">
                                    <div class="input-group date">
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.doctor') }}</th>
                                <!-- <th class="hide-lg">{{ trans('messages.clinic') }}</th> -->
                                <th class="hide-md">{{ trans('messages.date') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();
dateNowSecond = date.getFullYear()+'-'+monthNow+'-'+dayNow;


@if($level != "doctor")

function listPoly(val) {
    loading_content("#form-search #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            


            if(!dataPoly.error) {
                loading_content("#form-search #polyclinic-group", "success");

                var item = dataPoly.data;
                    $("#form-search select[name='polyclinic']").html("<option value=''></option><option value='all'>{{ trans('messages.all_polyclinic') }}</option>");
                    for (var i = 0; i < item.length; i++) {
                        $("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-search select[name='polyclinic']").select2({
                        placeholder: "{{ trans('messages.select_polyclinic') }}",
                    });

                if(val!="") {
                    $("#form-search select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-search #polyclinic-group", "failed");

                $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-search #polyclinic-group", "failed");

            $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

listPoly(polyclinicRepo);
@else
    $("#form-search select[name='polyclinic']").html("<option value='all'>{{$name_polyclinic}}</option>");
    $("#form-search select[name=polyclinic]").prop("disabled", true);
@endif

$('#form-search select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;

    if(polyclinic=="all") {
        polyclinic = "";
    }
});

var keyPoly = polyclinicRepo;
var numPage = pageRepo;

$("#form-search input[name='from_date']").val(unconvertDate(dateNowSecond));
$("#form-search input[name='to_date']").val(unconvertDate(dateNowSecond));


function search(page, submit, polyclinic, from_date, to_date) {
    formData= new FormData();
    var clinic = "";

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if("{{$level == 'doctor'}}") {
        polyclinic = "{{$id_polyclinic}}";
        clinic = "{{$id_clinic}}";
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keyPoly = polyclinic;
    numPage = page;

    repository(['clinic', clinic], ['polyclinic',polyclinic],['from_date',from_date],['to_date',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/patient/{{ $id_patient }}",
        type: "GET",
        data: "id_clinic="+clinic+"&id_polyclinic="+polyclinic+"&from_date="+from_date+"&to_date="+to_date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;

                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(i==0) {
                                tr = $('<tr class="table-header" />');
                                if(item[i].date==convertDate(dateNow)) {
                                    tr.append("<td colspan='4'>{{ trans('messages.today') }}</td>");
                                } else {
                                    tr.append("<td colspan='4'>"+getFormatDate(item[i].date)+"</td>");
                                }
                                $("#table").append(tr);
                            }

                            tr = $('<tr/>');

                            //tr.append("<td class='photo'>"+thumbnail('sm','doctor','{{ $storage }}',item[i].doctor.photo,item[i].doctor.name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span>"+
                                '<div class="sub show-md">'+
                                    // "<span class='info'><a href='#/{{ $lang }}/clinic/"+item[i].clinic.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].clinic.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></span>"+
                                    "<span class='general show-sm'>" + getFormatDate(item[i].date) + " "+getFormatBetweenTime(item[i].start_time, item[i].end_time)+"</span>"+
                                '</div>'+
                                "</td>");

                            //tr.append("<td class='photo hide-lg'>"+thumbnail('sm','clinic','{{ $storage }}',item[i].clinic.logo,item[i].clinic.name)+"</td>");
                            // tr.append("<td class='hide-lg'><a href='#/{{ $lang }}/clinic/"+item[i].clinic.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].clinic.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></td>");
                            

                            tr.append("<td class='hide-md'>" + getFormatDate(item[i].date) + " <br />"+getFormatBetweenTime(item[i].start_time, item[i].end_time)+"</td>");

                            //var action = "<a data-toggle='modal' data-target='#detail' data-id='"+item[i].id+"' class='btn-table btn-blue'><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /> {{ trans('messages.detail') }}</a>";

                            var action = "<a onclick=\"reportMedical('"+item[i].id+"')\" class='btn-table btn-blue'><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /> {{ trans('messages.detail') }}</a>";

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            if(i<to) {
                                if(item[i].date!=item[i+1].date) {
                                    tr = $('<tr class="table-header" />');
                                    if(item[i+1].date==convertDate(dateNow)) {
                                        tr.append("<td colspan='4'>{{ trans('messages.today') }}</td>")
                                    } else {
                                        tr.append("<td colspan='4'>"+getFormatDate(item[i+1].date)+"</td>");
                                    }
                                    $("#table").append(tr);
                                }   
                            }

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");

                    $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                }

                pages(page, total, per_page, current_page, last_page, from, to, polyclinic, from_date, to_date);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", polyclinic, from_date, to_date); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", polyclinic, from_date, to_date); });
        }
    })
}

$("#form-search").submit(function(event) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    if(polyclinic=="all") {
        polyclinic = "";
    }

    search(1,"true", polyclinic, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        search(1,"true", polyclinic, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        search(1,"true", polyclinic, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search select[name='polyclinic']").change(function(e) {
    if(this.value == "all") {
        this.value = ""
    }
    search(1, "true", this.value,  convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
});

search(numPage,"false",keyPoly,dateNowSecond, dateNowSecond);



function profile(id) {
    $('#section-profile').addClass('hide');
    $('#loading-profile').removeClass('hide');
    $("#reload-profile").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading-profile').addClass('hide');
                $('#section-profile').removeClass('hide');
                let phone_code = data.phone_code != null ? "+"+data.phone_code : "";
                let phone = data.phone != null ? data.phone : "";
                $("#section-profile #patient-photo").html(thumbnail('hw','patient','{{ $storage }}',data.photo,data.name, 80, 80));
                $("#section-profile #patient-name").html(data.name);
                $("#section-profile #patient-email").html(nullToEmpty(data.email));
                $("#section-profile #patient-phone").html(""+phone_code+""+phone+"");

                $("#section-profile #patient-name").attr("href","#/{{ $lang }}/patient/"+data.id);
                $("#section-profile #patient-name").attr("onclick","loading($(this).attr('href'))");

            } else {
                $('#loading-profile').addClass('hide');
                $("#reload-profile").removeClass("hide");

                $("#reload-profile .reload").click(function(){ profile(id); });
            }

        },
        error: function(){
            $('#loading-profile').addClass('hide');
            $("#reload-profile").removeClass("hide");

            $("#reload-profile .reload").click(function(){ profile(id); });
        }
    })
}

profile("{{ $id }}");

</script>

@include('medical_records.print')
@include('medical_records.history')