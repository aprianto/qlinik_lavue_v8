<script type="text/javascript">


function reportMedicine(id) {
    $("#document-copyright").html('');
    $("#document-copyright").addClass("hide");
    $("#loading-document-copyright").removeClass("hide");
    $("#reload-document-copyright").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medical/records/"+id+"/medicine",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            $("#loading-document-copyright").addClass("hide");
            $("#document-copyright").removeClass("hide");
            $("#document-copyright").html(data);
        },
        error: function(){
            $("#loading-document-copyright").addClass("hide");
            $("#reload-document-copyright").removeClass("hide");
            $("#reload-document-copyright .reload").click(function(){ reportMedicine(id); });
        }
    });
}

function reportMedicalMedicine(id) {
    $('#report-dialog-copyright').attr('class','modal-dialog modal-lg');
    $("input[name='id_report_copyright']").val(id);
    $("#report-copyright").modal("show");
    reportMedicine(id);
}

function reportRecipe(id, id_medicine) {
    $("#document").html('');
    $(".type-"+id_medicine+" i.fa").removeClass('hide');
    $(".type-"+id_medicine+"").attr("disabled", true);
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medical/records/"+id+"/medicine/"+id_medicine,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            $(".type-"+id_medicine+" i.fa").addClass('hide');
            $(".type-"+id_medicine+"").attr("disabled", false);
            $("#document").html(data);
            setTimeout(() => {
                printRecipe();
            }, 500);
        },
        error: function(){
            $("#reload-document .reload").click(function(){ reportRecipe(id, id_medicine); });
        }
    });
}

function printRecipe() {
    var print = $("#document").html();
    var divToPrint=$('#document').html();
    var newWin=window.open('','Print-Window');
    newWin.document.write(divToPrint);
    newWin.document.close();
    newWin.focus();
    newWin.print();
    setTimeout(() => {
        newWin.close();
    }, 500);

    return true;
}

</script>