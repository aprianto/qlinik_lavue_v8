@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row" id="section-loading">
        <div class="col-md-12">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-button">
        <div class="col-md-12">
            <div class="widget no-color">                
                <div class="btn-sec">
                    <ul>
                        <li id="link-payment" class="hide float-left">                         
                            <a class='btn-color' href='#/{{ $lang }}/payment/medical/{{ $id }}' onclick='loading($(this).attr("href"));' class="hover">
                                <img src="{{ asset('assets/images/icons/action/pay-white.png') }}" />
                                <span>{{ trans("messages.pay_consultation") }}</span>
                            </a>
                        </li>
                        <li class="float-right">
                            <a onclick="reportMedical('{{ $id }}')"> 
                                <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                                <span>{{ trans("messages.print_medical_records") }}</span>
                            </a>
                        </li>
                        <li class="float-right hide" id="btnLetter">
                            <a data-toggle="modal" data-target="#printLetter"> 
                                <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                                <span>Print Surat Rujukan</span>
                            </a>
                        </li>
                    </ul>
                </div>                
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-8">
            <div class="admin-follow no-bg ">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.medical_records_detail') }}
                    </h3>
                </div>
                <div class="search-sec">
                    <div id="section-profile" class="detail-profile-form" style="display: flex; flex-direction: row; align-items: center;">
                        <div class="column" id="patient-photo"><img src="{{ $storage.'/images/patient/photo.png' }}" width="80px" height="80px" style="min-width:80px;max-width:80px;width:80px;min-height:80px;max-height:80px;height:80px;border: 1px solid #ededed;"></div>
                        <div class="column text-left">
                            <h3 id="name" style="font-size: 20px; line-height: 1.4; margin-bottom: 4px; margin-top: 0;"></h3>
                            <p id="mrn"></p>
                        </div>
                        <div class="column" style="margin-left: auto;">
                            <div class="btn-profile-sec">
                                <a id="btn-detail-profile">{{ trans("messages.show_detail") }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card hide" id="loading-profile">
                        <span class="three-quarters">Loading…</span>
                    </div>
                    <div class="card hide" id="reload-profile">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div id="detail-profile" class="hide widget widget-detail">
                    <div class="twitter-account">
                        <h3>{{ trans("messages.info") }}</h3>                    
                    </div>
                    <ul class="detail-profile">
                        <div id="detail-profile">
                            <li class="profile-features">
                                <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.phone_number') }}</span>
                                        <span id="phone"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.email') }}</span>
                                        <span id="email"></span>
                                    </div>
                                </div>
                            </li>                        
                            <li class="profile-features">  
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.birth_date') }}</span>
                                        <span id="birth_date"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">  
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.gender') }}</span>
                                        <span id="gender"></span>
                                    </div>
                                </div>
                            </li>     
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.no_identity') }}</span>
                                        <span id="no_identity"></span>
                                    </div>
                                </div>
                            </li>            
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.blood_type') }}</span>
                                        <span id="blood_type"></span>
                                    </div>
                                </div>
                            </li>                        
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.smoking') }}?</span>
                                        <span id="smoke"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <h4>{{ trans("messages.address") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.address') }}</span>
                                        <span id="address"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <h4>{{ trans("messages.patient_data") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.patient_category') }}</span>
                                        <span id="patient_category"></span>
                                    </div>
                                </div>
                            </li>
                        </div>
                    </ul>
                </div>
            </div>
            <div>
                <div id="accordion" class="panel-group detail-tabs">
                    <div class="panel panel-default panel-content-subjective">
                        <div class="panel-heading gray panel-list" data-target="#section_detail-subjective" data-parent="#accordion" data-toggle="collapse">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed">
                                <div class="title-accordion">
                                    {{ trans('messages.subjective') }}
                                </div>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="section_detail-subjective" style="height: 0px;">
                            <div class="panel-body panel-subjective">
                                <div class="detail-subjective">
                                    <div class="form-group">
                                        <p>{{ trans('messages.complaints')}}</p>
                                        <span id="complaint"></span>              
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.anamnese') }}</p>
                                        <span id="anamnese"></span>              
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.history_of_illness') }}</p>
                                        <span id="history_illness"></span>                        
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.history_of_allergy') }}</p>
                                        <span id="history_allergies"></span>                        
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.history_of_vaccination') }}</p>                 
                                        <div id="panel-vaccine-history" class="panel-vital">
                                            <div class="table-vital">
                                                <table class="table table-hover mini-table">
                                                    <thead>
                                                        <tr id="head-vaccine-history">
                                                            <th>{{ trans('messages.vaccine_name') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="vaccine-history">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-content-subjective">
                        <div class="panel-heading gray panel-list" data-target="#section_detail-objective" data-parent="#accordion" data-toggle="collapse">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed">
                                <div class="title-accordion">
                                    {{ trans('messages.objective') }}
                                </div>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="section_detail-objective" style="height: 0px;">
                            <div class="panel-body panel-subjective">
                                <div id="panel-vital_sign" class="panel-vital">
                                    <div class="table-vital">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr id="title-vital_sign">
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody> 
                                                <tr id="data-body_temperature">
                                                    <td class="middle">{{ trans('messages.body_temperature') }}</td>
                                                </tr> 
                                                <tr colspan="3" id="title-blood-pressure">
                                                    <td class="middle group-label first-label"><b>{{ trans('messages.blood_pressure_title') }}</b></td>
                                                </tr>                      
                                                <tr id="data-sistole">
                                                    <td class="middle">{{ trans('messages.sistole') }}</td>
                                                </tr>
                                                <tr id="data-diastole">
                                                    <td class="middle">{{ trans('messages.diastole') }}</td>
                                                </tr>
                                                <tr id="data-pulse">
                                                    <td class="middle">{{ trans('messages.pulse') }}</td>
                                                </tr>                             
                                                <tr id="data-respiratory_frequency">
                                                    <td class="middle">{{ trans('messages.respiratory_frequency') }}</td>
                                                </tr>
                                                <tr id="title-physical-exam" colspan="3">
                                                    <td class="middle group-label first-label"><b>{{ trans('messages.physical_exam') }}</b></td>
                                                </tr>                   
                                                <tr id="data-height">
                                                    <td class="middle">{{ trans('messages.height') }}</td>
                                                </tr>
                                                <tr id="data-head">
                                                    <td class="middle">{{ trans('messages.head_circumference') }}</td>
                                                </tr>
                                                <tr id="data-weight">
                                                    <td class="middle">{{ trans('messages.weight') }}</td>
                                                </tr>
                                                <tr id="data-abdominal_circumference">
                                                    <td class="middle">{{ trans('messages.abdominal_circumference') }}</td>
                                                </tr>
                                                <tr id="data-imt">
                                                    <td class="middle">IMT</td>
                                                </tr>
                                                <tr id="data-imt-status">
                                                    <td class="middle" style="color: #757575 !important;font-size: 12px !important;">Kategori IMT menurut kemenkes RI</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group table-responsive" id="list-diagnostic">
                                    <div class="table-diagnostic">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr>
                                                    <th>Penunjang Diagnostik</th>
                                                    <th class="blue-title">Data/Hasil<span id="date-diagnostic" style="margin-left: 5px;"></span></th>
                                                </tr>
                                            </thead>
                                            <tbody id="data-diagnostic">
                                                <tr id="blood-pressure">
                                                    <td>{{ trans('messages.blood_pressure') }}</td>
                                                </tr>
                                                <tr id="radiologi">
                                                    <td>{{ trans('messages.radiologi') }}</td>
                                                </tr>
                                                <tr id="routine-blood">
                                                    <td>{{ trans('messages.routine_blood') }}</td>
                                                </tr>
                                                <tr id="blood-fat">
                                                    <td>{{ trans('messages.blood_fat') }}</td>
                                                </tr>
                                                <tr id="blood-sugar">
                                                    <td>{{ trans('messages.blood_sugar') }}</td>
                                                </tr>
                                                <tr id="liver-function">
                                                    <td>{{ trans('messages.liver_function') }}</td>
                                                </tr>
                                                <tr id="kidney-function">
                                                    <td>{{ trans('messages.kidney_function') }}</td>
                                                </tr>
                                                <tr id="heart-function">
                                                    <td>{{ trans('messages.heart_function') }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="panel-physical-examination hide" id="panel-physical-examination">
                                    <div class="table-reponsive">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr id="title-physical-examination"></tr>                                      </tr>
                                            </thead>
                                            <tbody id='form-physical-examination-table'></tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="detail-objective">
                                    <div class="form-group">
                                        <p>{{ trans('messages.consciousness') }}</p>
                                        <span id="consciousness"></span>
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.additional_notes') }}</p>
                                        <span id="additional_notes"></span>
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.attachment') }}</p>
                                        <ul id="attachment" class="list-attach"></ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-content-subjective">
                        <div class="panel-heading gray panel-list" data-target="#section_detail-assessment" data-parent="#accordion" data-toggle="collapse">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed">
                                <div class="title-accordion">
                                    {{ trans('messages.assessment') }}
                                </div>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="section_detail-assessment" style="height: 0px;">
                            <div class="panel-body panel-subjective">
                                <div class="detail-assessment">
                                    <div class="form-group">
                                        <p>{{ trans('messages.diagnosis') }}</p>
                                        <span id="diagnosis"></span>  
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.prognosis') }}</p>
                                        <span id="prognosis"></span>  
                                    </div>
                                    <div class="form-group">
                                        <p>{{ trans('messages.icd') }}</p>
                                        <div class="map-table">
                                            <h3>{{ trans("messages.icd_code") }}</h3><h3>{{ trans("messages.icd") }}</h3>
                                            <ul id="icd"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-content-subjective">
                        <div class="panel-heading gray panel-list" data-target="#section_detail-plan" data-parent="#accordion" data-toggle="collapse">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed">
                                <div class="title-accordion">
                                    {{ trans('messages.planning') }}
                                </div>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="section_detail-plan" style="height: 0px;">
                            <div class="panel-body panel-subjective">
                                <div class="detail-plan">
                                    <div class="table-service-plan">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans("messages.service_action") }}</th>
                                                    <th class="blue-title">{{ trans('messages.qty') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody id="service">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-plan-medicine">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr id="head-medicine">
                                                    <th>{{ trans("messages.medicine") }}</th>
                                                </tr>
                                            </thead>
                                            <tbody id="medicine">
                                            </tbody>
                                        </table>
                                        <div class="action-btn-print hide">
                                            <a class="button-actions" onclick = "reportMedicalMedicine('{{ $id }}')">{{ trans('messages.print_recipe') }}</a>
                                        </div>
                                    </div>
                                    <div class="table-plan-vaccine-item">
                                        <table class="table table-hover mini-table">
                                            <thead>
                                                <tr id="head-vaccine-item">
                                                    <th>{{ trans('messages.vaccine') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody id="vaccine-item">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="detail-plan">
                                        <div class="form-group">
                                            <p>{{ trans('messages.drug_therapy') }}</p>
                                            <span id="drug_therapy">-</span>
                                        </div>

                                        <div class="form-group">
                                            <p>{{ trans('messages.non_drug_therapy') }}</p>
                                            <span id="non-drug-therapy">-</span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget widget-detail right-detail-menu">
                <div class="user-sec">
                    <ul id="section-clinic" class="client-list">
                        <li class="first-sec">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/consult-date.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.date') }}</span>
                                    <h3 id="date"></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.clinic') }}</span>
                                    <h3><a title="" id="clinic"></a></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/doctor-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.doctor') }}</span>
                                    <h3><a title="" id="doctor"></a>  • <i id="polyclinic"></i></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/status.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.status') }}</span>
                                    <h3>{{ trans("messages.done") }}</h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-status">
                                <p>{{ trans('messages.return_home_status') }}</p>
                                <span id="return_home_status"></span>
                            </div>
                        </li>
                    </ul>                
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm" id="printLetter" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                    <h1>Cetak Rujukan</h1>
                </div>
                <form class="sec form-bpjs" role="form" style="margin-top: 25px;" id="formCheck">
                    <div class="form-group" id="type-group">
                        <label class="control-label" style="font-size: 12px;">Piih hasil cetak</label>
                        <div class="sf-radio"> 
                            <label style="font-size: 14px;"><input type="radio" value="0" name="letter"><span></span> Surat Rujuk Lanjut</label>
                            <label style="font-size: 14px;"><input type="radio" value="1" name="letter"><span></span> Surat Rujuk Lanjut + Rujuk Balik</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm">Konfirmasi</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#section_detail-subjective").collapse("show");
$("#section_detail-objective").collapse("show");
$("#section_detail-assessment").collapse("show");
$("#section_detail-plan").collapse("show");

$(function() {
    $(".line").peity("line", {
      fill: ["#f2f7f9"],
      height: ["27px"],
      width: ["100"],
      stroke: ["#7dc9f0"]
    })

});


$("#btn-detail-profile").click(function(){ 
    var section = $("#detail-profile").css('display');
    if(section=="block") {
        $("#btn-detail-profile").html("{{ trans('messages.show_detail') }}");
        $("#detail-profile").addClass("hide");
    } else {
        $("#btn-detail-profile").html("{{ trans('messages.hide_detail') }}");
        $("#detail-profile").removeClass("hide");
    }
});

var copyright = false;
var kode_kunjungan_bpjs;

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-button').addClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');
                $('#section-button').removeClass('hide');
                $("#section-detail #date").html(getFormatDate(data.date)+" "+getFormatBetweenTime(data.start_time, data.end_time));
                $("#section-detail #logo").replaceWith(thumbnail('md','clinic','{{ $storage }}',data.clinic.logo,data.clinic.name));   
                $("#section-detail #clinic").attr("href","#/{{ $lang }}/clinic/"+data.id_clinic);
                $("#section-detail #clinic").attr("onclick","loading($(this).attr('href'))");
                $("#section-detail #clinic").html(data.clinic.name);
                $("#section-detail #photo-doctor").replaceWith(thumbnail('md','doctor','{{ $storage }}',data.doctor.photo,data.doctor.name));
                $("#section-detail #doctor").attr("href","#/{{ $lang }}/doctor/"+data.id_doctor);
                $("#section-detail #doctor").attr("onclick","loading($(this).attr('href'))");
                $("#section-detail #doctor").html(data.doctor.name);
                $("#section-detail #polyclinic").html(data.polyclinic.name);
                $("#title-vital_sign").html("<th>{{ trans('messages.vital_sign') }}</th>");

                if(data.patient.illnesses_history && data.patient.illnesses_history.length > 0) {
                    data.patient.illnesses_history.forEach((illness, i) => {
                        $("#history_illness").append("<span>"+(i ? ', ': '')+illness.name+"</span>")
                    })
                } else {
                    $("#history_illness").text('-')
                }

                if(data.patient.allergies_history && data.patient.allergies_history.length) {
                    data.patient.allergies_history.forEach((allergy, i) => {
                        $("#history_allergies").append(`<span>${i ? ', ': ''}${allergy.name}${allergy.name_category ? '('+allergy.name_category+')' : ''}</span>`)
                    })
                } else {
                    $("#history_allergies").text('-')
                }

                // handle table physical examination
                $('#title-physical-examination').html('');
                $('#title-physical-examination').append(`
                    <th>{{trans('messages.physical_examination')}}</th>
                    <th><center>${getShortFormatDate(data.date)}</center></th>
                `);
                detailPhysicalExaminations(data.physical_examinations)

                if(data.code_reference_hospital!="" && data.code_reference_hospital!=null){
                    $("#btnLetter").removeClass('hide');
                }else{
                    $("#btnLetter").addClass('hide');
                }
                kode_kunjungan_bpjs = data.kode_kunjungan_bpjs;

                var no_identity = "-";
                if(data.patient.id_number!="" && data.patient.id_number!=null) {
                    no_identity = data.patient.id_number+" ("+data.patient.id_type+")";
                }

                $("#path").html(data.patient.name);
                $("#section-detail #mrn").html(data.patient.mrn);
                $("#section-detail #name").html(data.patient.name);
                $("#section-detail #birth_date").html(getFormatDate(data.patient.birth_date)+" ("+getAge(data.patient.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#section-detail #gender").html(getGender(data.patient.gender));

                var smoke = "";
                if(data.patient.smoke == 1) {
                    smoke = '{{ trans("messages.smoking") }}';
                } else {
                    smoke = '{{ trans("messages.no_smoking") }}';
                }

                $("#section-detail #smoke").html(smoke);
                $("#section-detail #blood_type").html(checkNull(data.patient.blood_type));
                $("#section-detail #no_identity").html(no_identity);
                $("#section-detail #address").html(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
                $("#section-detail #email").html(checkNull(data.patient.email));
                $("#section-detail #phone").html("+"+data.patient.phone_code+""+data.patient.phone);

                try {
                    $("#section-detail #patient_category").html(data.patient.patient_category.name);
                } catch(err) {}

                $("#section-detail #financing").html(getFinancing(data.patient.financing));

                var icd = "";
                if(data.icd.length > 0) {
                    for(var i = 0;i < data.icd.length; i++) {
                        no = i+1;
                        icd = icd+"<li><span>"+data.icd[i].code+"</span><i>"+data.icd[i].name+"</i></li>";
                    }
                } else {
                    icd = icd+"<li><span>{{ trans('messages.no_icd') }}</span><i></i></li>";
                }

                $("#icd").html(icd);

                var data_anamnese = data.anamnese != null ? data.anamnese : "-";
                $("#anamnese").html(checkNull(data_anamnese));

                var data_complaint = data.complaint || '-';
                $('#complaint').html(data_complaint);
                
                var data_diagnosis = data.diagnosis != null ? data.diagnosis : "-";
                $("#diagnosis").html(data_diagnosis);

                var data_prognosis = transformPrognosis(data.prognosis);
                $("#prognosis").html(data_prognosis);

                if(data.vital_sign_history.length > 0) {
                    for(var i = 0; i < data.vital_sign_history.length; i++) {
                        $("#data-body_temperature").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].body_temperature)+"</td>");
                        $("#title-vital_sign").append("<th align='center' style='text-align: center;'>"+getShortFormatDate(data.vital_sign_history[i].date)+"</th>");  
                        $("#data-sistole").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].blood_sistole)+"</td>");
                        $("#data-diastole").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].blood_diastole)+"</td>");
                        $("#data-pulse").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].pulse)+"</td>");
                        $("#data-respiratory_frequency").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].respiratory_frequency)+"</td>");
                        $("#data-height").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].height)+"</td>");
                        $("#data-head").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].head)+"</td>");
                        $("#data-weight").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].weight)+"</td>");
                        $("#data-abdominal_circumference").append("<td align='center' class='middle'>"+checkNull(data.vital_sign_history[i].abdomen)+"</td>");
                        $("#title-blood-pressure").append("<td></td>");
                        $("#title-physical-exam").append("<td></td>");
                        
                        let imt = data.vital_sign_history[i].imt && !isNaN(data.vital_sign_history[i].imt) && data.vital_sign_history[i].imt.toFixed(1) || 0;
                        if(!imt) {
                            const tempIMT = calcIMT(data.vital_sign_history[i].weight, data.vital_sign_history[i].height);
                            imt = tempIMT && tempIMT.toFixed(1) || '-';
                        }

                        $('#data-imt').append("<td align='center' class='middle'>"+imt+"</td>")
                        const imtStatus = transformIMT(imt !== '-' ? imt : null);
                        $('#data-imt-status').append(`<td align='center' class='middle' style='color:${imtStatus.color} !important;font-weight: bold !important;'>${imtStatus.text}</td>`)
                    }
                }

                if(data.diagnostic_helper.date != null) {
                    $("#date-diagnostic").text('('+getShortFormatDate(data.diagnostic_helper.date)+')')
                }

                var blood_diastole = data.diagnostic_helper.blood_diastole != null ? data.diagnostic_helper.blood_diastole : 0;
                var blood_sistole = data.diagnostic_helper.blood_sistole != null ? data.diagnostic_helper.blood_sistole : 0;
                var radiology_information = data.diagnostic_helper.radiology_information != null ? data.diagnostic_helper.radiology_information : "-";
                var hemoglobin = data.diagnostic_helper.hemoglobin != null ? data.diagnostic_helper.hemoglobin : 0; 
                var leukocytes = data.diagnostic_helper.leukocytes != null ? data.diagnostic_helper.leukocytes : 0;
                var hematocrit = data.diagnostic_helper.hematocrit != null ? data.diagnostic_helper.hematocrit : 0;
                var erythrocytes = data.diagnostic_helper.erythrocytes !=null ? data.diagnostic_helper.erythrocytes : 0;
                var sedimentation_blood = data.diagnostic_helper.sedimentation_blood != null ? data.diagnostic_helper.sedimentation_blood : 0;
                var platelets = data.diagnostic_helper.platelets != null ? data.diagnostic_helper.platelets : 0;
                var hdl = data.diagnostic_helper.hdl != null ? data.diagnostic_helper.hdl : 0;
                var ldl = data.diagnostic_helper.ldl != null ? data.diagnostic_helper.ldl : 0;
                var total_cholesterol = data.diagnostic_helper.total_cholesterol != null ? data.diagnostic_helper.total_cholesterol : 0;
                var triglyceride = data.diagnostic_helper.triglyceride != null ? data.diagnostic_helper.triglyceride : 0;
                var fasting_blood_sugar = data.diagnostic_helper.fasting_blood_sugar != null ? data.diagnostic_helper.fasting_blood_sugar : 0;
                var post_paradial_blood_sugar = data.diagnostic_helper.post_paradial_blood_sugar != null ? data.diagnostic_helper.post_paradial_blood_sugar : 0;
                var current_blood_sugar = data.diagnostic_helper.current_blood_sugar != null ? data.diagnostic_helper.current_blood_sugar : 0;
                var hba1c = data.diagnostic_helper.hba1c != null ? data.diagnostic_helper.hba1c : 0;
                var sgot = data.diagnostic_helper.sgot != null ? data.diagnostic_helper.sgot : 0;
                var sgpt = data.diagnostic_helper.sgpt != null ? data.diagnostic_helper.sgpt : 0;
                var gamma_gt = data.diagnostic_helper.gamma_gt != null ? data.diagnostic_helper.gamma_gt : 0;
                var protkual = data.diagnostic_helper.protkual != null ? data.diagnostic_helper.protkual : 0;
                var creatinin = data.diagnostic_helper.creatinin != null ? data.diagnostic_helper.creatinin : 0;
                var ureum = data.diagnostic_helper.ureum != null ? data.diagnostic_helper.ureum : 0;
                var uric_acid = data.diagnostic_helper.uric_acid != null ? data.diagnostic_helper.uric_acid : 0;
                var abi = data.diagnostic_helper.abi != null ? data.diagnostic_helper.abi : 0;
                var ekg = data.diagnostic_helper.ekg != null ? data.diagnostic_helper.ekg : 0;
                var echo = data.diagnostic_helper.echo != null ? data.diagnostic_helper.echo : 0;

                var blood_pressure = '<ul class="list-content-tabs"><li>{{ trans("messages.diastole") }} : '+blood_diastole+'</li><li>{{ trans("messages.sistole") }} : '+blood_sistole+'</li></ul>';
                $('#data-diagnostic #blood-pressure').append("<td class='list-table-center'>"+blood_pressure+"</td>");

                var radiologi ='<ul class="list-content-tabs"><li>Keterangan : '+radiology_information+'</li></ul>';
                $('#data-diagnostic #radiologi').append("<td>"+radiologi+"</td>");

                var routine_blood = '<ul class="list-content-tabs"><li>Hemoglobin (gr %) : '+hemoglobin+'</li><li>Leukosit (/mm3) : '+leukocytes+'</li><li>Hematocrit : '+hematocrit+'</li><li>Eritrosit (juta/m3) : '+erythrocytes+'</li><li>Trombosit : '+platelets+'</li><li>Laju Endap Darah (mm/jam) : '+sedimentation_blood+'</li></ul>'
                $("#routine-blood").append("<td>"+routine_blood+"</td>");

                var blood_fat = '<ul class="list-content-tabs"><li>HDL : '+hdl+'</li><li>LDL : '+ldl+'</li><li>Cholesterol Total : '+total_cholesterol+'</li><li>Trigliserid : '+triglyceride+'</li></ul>'
                $("#blood-fat").append("<td>"+blood_fat+"</td>");
                
                var blood_sugar = '<ul class="list-content-tabs"><li>Gula Darah Sewaktu : '+current_blood_sugar+'</li><li>Gula Darah Puasa : '+fasting_blood_sugar+'</li><li>Gula Darah Post Pradial : '+post_paradial_blood_sugar+'</li><li>HbA1C : '+hba1c+'</li></ul>'
                $("#blood-sugar").append("<td>"+blood_sugar+"</td>");

                var liver_function = '<ul class="list-content-tabs"><li>SGOT : '+sgot+'</li><li>SGPT : '+sgpt+'</li><li>Gamma GT : '+gamma_gt+'</li><li>ProtKual : '+protkual+'</li></ul>'
                $("#liver-function").append("<td>"+liver_function+"</td>");

                var kidney_function = '<ul class="list-content-tabs"><li>Creatinin : '+creatinin+'</li><li>Ureum : '+ureum+'</li><li>Asam Urat : '+uric_acid+'</li></ul>'
                $("#kidney-function").append("<td>"+kidney_function+"</td>");

                var heart_function = '<ul class="list-content-tabs"><li>ABI : '+abi+'</li><li>EKG : '+ekg+'</li><li>Echo : '+echo+'</li></ul>'
                $("#heart-function").append("<td>"+heart_function+"</td>");

                var data_consciousness = data.consciousness != null ? data.consciousness : "-" 
                $("#consciousness").text(data_consciousness);

                var data_additional_notes = data.additional_notes != null ? data.additional_notes : "-";
                $("#additional_notes").text(data_additional_notes);

                var drug_therapy = data.drug_therapy || '-';
                $('#drug_therapy').html(nl2br(drug_therapy));

                var non_drug_therapy = data.non_drug_therapy || '-';
                $('#non-drug-therapy').html(nl2br(non_drug_therapy));

                if(data.attachments.length > 0) {
                    var link = "{{$download_url}}"
                    for(var i = 0; i < data.attachments.length; i++) {
                        $("#attachment").append("<li><a href="+link+'/download/'+data.attachments[i].link+'/'+data.attachments[i].name.replace(/ /g,"_")+">"+data.attachments[i].name+"</a></li>")
                    }
                }else {
                    $("#attachment").text("-")
                }

                if(data.service.length == 0) {
                    $("#service").html('<div class="empty-info">'+'<span>{{ trans("messages.no_service") }}</span>'+'</div>');
                } else {
                    var service = "";
                    for(var i = 0;i < data.service.length;i++) {
                        no = i+1;
                        var style = "";
                        if(i == 0) {
                            style = "first-sec";
                        }
                        service = service+
                        '<tr><td class="'+style+'"><span>'+data.service[i].service.name+'</span><br/><small>'+data.service[i].service.code+'</small>'+'</td>' + 
                        '<td>'+data.service[i].quantity+'</td></tr>';
                    }
                    $("#service").html(service);
                }

                // Fetch vaccine items to table
                if (data.vaccines.length == 0) {
                    $("#vaccine-item").html('<div class="empty-info">'+'<span>{{ trans("messages.no_vaccine") }}</span>'+'</div>');
                } else {
                    $("#head-vaccine-item").append("<th class='blue-title'>{{ trans('messages.dose') }}</th>");
                    $("#head-vaccine-item").append("<th class='blue-title'>{{ trans('messages.lot_no') }}</th>");

                    let vaccineHtml = '';
                    for (let i = 0; i < data.vaccines.length; i++) {
                        const vaccine = data.vaccines[i];
                        vaccineHtml += '<tr><td><span class="title">' + vaccine.name + '</span></td>'
                            + '<td>' + vaccine.quantity + '</td>'
                            + '<td>' + vaccine.batch_no + '</td></tr>';
                    }
                    $("#vaccine-item").html(vaccineHtml);
                }

                // Fetch vaccine history to table
                if (data.patient.vaccine_history.length == 0) {
                    $("#vaccine-history").html('<div class="empty-info">'+'<span>{{ trans("messages.no_vaccine_history") }}</span>'+'</div>');
                } else {
                    $("#head-vaccine-history").append("<th class='blue-title'>{{ trans('messages.vaccine_given_date') }}</th>");

                    let vaccineHistoryHtml = '';
                    for (let i = 0; i < data.patient.vaccine_history.length; i++) {
                        const vaccine = data.patient.vaccine_history[i];
                        let histories = '';
                        for (j = 0; j < vaccine.history.length; j++) {
                            const history = vaccine.history[j];
                            histories += '<div class="chip">'
                                        +   '<div class="chip-content">' + history.dosage_sequence + ' &bull; ' + getShortFormatDate(history.date_given) + '</div>'
                                        + '</div>';
                        }
                        vaccineHistoryHtml += '<tr><td><span class="title">' + vaccine.name + '</span></td><td>' + histories + '</td></tr>';
                    }

                    $("#vaccine-history").html(vaccineHistoryHtml);
                }

                if(data.prescriptions.length == 0) {
                    $("#medicine").html('<div class="empty-info">'+'<span>{{ trans("messages.no_medicine") }}</span>'+'</div>');
                    $(".action-btn-print").addClass("hide");

                } else {
                    $("#head-medicine").append("<th class='blue-title'>{{ trans('messages.dose') }}</th>");
                    $("#head-medicine").append("<th class='blue-title'>{{ trans('messages.day') }}</th>");
                    $("#head-medicine").append("<th class='blue-title'>{{ trans('messages.unit') }}</th>");
                    $("#head-medicine").append("<th class='blue-title'>{{ trans('messages.qty') }}</th>");
                    $(".action-btn-print").removeClass("hide");
                    
                    let medicine = "";

                    for(i = 0; i < data.prescriptions.length; i++) {
                        let prescriptions = data.prescriptions[i];
                        let title = "";
                        if(prescriptions.concoction != null)  {
                            title = "<span class='title'>"+prescriptions.concoction.name+" ("+prescriptions.concoction.instruction+")</span>";
                            let list_medicine = "";
                            
                            for (j = 0; j < prescriptions.medicines.length; j++) {
                                list_medicine += "<li>"+prescriptions.medicines[j].name+" ("+prescriptions.medicines[j].quantity+" "+prescriptions.medicines[j].unit_name+")</li>"
                            }

                            medicine = medicine+
                            '<tr><td>'+title+'<ul class="list-bullet">'+list_medicine+'</ul><small>{{ trans("messages.usage") }} : '+prescriptions.signa.instruction_for_use+'</small></td>' +
                            '<td>'+prescriptions.signa.dose+' x '+prescriptions.signa.per_day+'</td>'+
                            '<td>'+prescriptions.signa.total_day+'</td>'+
                            '<td>'+prescriptions.concoction.dosage+'</td>' +
                            '<td>'+prescriptions.concoction.total_package+'</td></tr>';
                        }else {
                            
                            for (j = 0; j < prescriptions.medicines.length; j++) {
                                title = "<span class='title'>"+prescriptions.medicines[j].name+"</span>";
                                medicine = medicine+
                                '<tr><td>'+title+'<small>{{ trans("messages.usage") }} : '+prescriptions.signa.instruction_for_use+'</small></td>' +
                                '<td>'+prescriptions.signa.dose+' x '+prescriptions.signa.per_day+'</td>'+
                                '<td>'+prescriptions.signa.total_day+'</td>'+
                                '<td>'+prescriptions.medicines[j].unit_name+'</td>' +
                                '<td>'+prescriptions.medicines[j].quantity+'</td></tr>';
                            }
                        }
                    }

                    $("#medicine").html(medicine);
                }
                
                $("#return_home_status").text(data.status)

                if(data.payment != true) {
                    $("#link-payment").removeClass("hide");    
                }

                copyright = data.copyright;
                
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

detail("{{ $id }}");


function calcIMT(weight, height) {
    return weight && height && (weight / Math.pow(height * 0.01, 2)) || 0;
}

function transformIMT(imt) {
    if(!imt) {
        return {
            text: '-',
            color: '#000000'
        }
    }

    if (imt < 17) {
        return {
            text: 'Sangat kurus',
            color: '#DB0062',
        }
    }

    if (imt <= 18.5) {
        return {
            text: 'Kurus',
            color: '#FFA332',
        }
    }

    if (imt <= 25.0) {
        return {
            text: 'Normal',
            color: '#046E89',
        }
    }

    if (imt <= 27.0) {
        return {
            text: 'Gemuk',
            color: '#FFA332',
        }
    }

    return {
        text: 'Sangat gemuk',
        color: '#DB0062',
    }
}

function transformPrognosis(value) {
    const prognosis = [
        {
            value: 'sanam',
            text: '{{trans("messages.sanam")}}'
        },
        {
            value: 'bonam',
            text: '{{trans("messages.bonam")}}'
        },
        {
            value: 'malam',
            text: '{{trans("messages.malam")}}'
        },
        {
            value: 'dubia-ad-sanam-bolam',
            text: '{{trans("messages.dubia-ad-sanam-bolam")}}'
        },
        {
            value: 'dubia-ad-malam',
            text: '{{trans("messages.dubia-ad-malam")}}'
        }
    ];

    if(!value) {
        return '-';
    }

    const seletedPrognosis = prognosis.filter(v => v.value === value)[0];

    return seletedPrognosis && seletedPrognosis.text || '-';
}

$('#printLetter').on('hidden.bs.modal', function () {
    $("#formCheck input[name='letter']").prop('checked', false);
});

$('#printLetter #confirm').click(function() {
    var value = $("#formCheck input[name='letter']:checked").val();
    
    if(value != '0'){
        rujuk(kode_kunjungan_bpjs);
    }else{
        rujukRefer(kode_kunjungan_bpjs);
    }
});


</script>

<div class="modal fade report modal-scroll" id="report-copyright" role="dialog" aria-labelledby="reportTitle" aria-hidden="true">
    <div id="report-dialog-copyright" class="modal-dialog modal-lg" role="document">        
        <div class="modal-content">
            <div class="modal-header">                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="id_report_copyright" />
            <div class="modal-body" id="loading-document-copyright">
                <div class="cell">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="reload-document-copyright">
                <div class="cell">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="document-copyright">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-secondary" id="cancel-print-copyright" data-dismiss="modal" aria-label="Close">
                    {{ trans("messages.cancel") }}
                </button>
                <button type="button" class="btn-primary" id="print-copyright">
                    {{ trans("messages.print") }}
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm" id="confirmCopyright" role="dialog" aria-labelledby="confirmCopyrightLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/confirm/confirm-activate.png') }}" />
                    <h1>{{ trans('messages.info_copyright') }}</h1>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" onclick="setCopyright(true)">{{ trans('messages.yes') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.no') }}</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


$("#print-copyright").click(function() {
    var print = $("#document-copyright").html();
    var divToPrint=$('#document-copyright').html();
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write(divToPrint+'<script>window.print()<\/script>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
});

$('#report-copyright').on('hidden.bs.modal', function () {
    if(copyright==false) {
        $("#confirmCopyright").modal("show");
    }
});


function setCopyright(val) {
    var id = "{{ $id }}";
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/"+id+"/copyright/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    //notif(true,"{{ trans('validation.success_copyright') }}");    

                    copyright = true;
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmCopyright").modal("toggle");
}

const physicalData = {
    eyes: { title: '{{trans("messages.eyes")}}', selected: '' },
    nose: { title: '{{trans("messages.nose")}}', selected: '' },
    ears: { title: '{{trans("messages.ears")}}', selected: '' },
    oropharynx: { title: '{{trans("messages.oropharynx")}}', selected: '' },
    neck: { title: '{{trans("messages.neck")}}', selected: '' },
    abdomen: { title: '{{trans("messages.abdomen")}}', selected: '' },
    lungs: { title: '{{trans("messages.lungs")}}', selected: '' },
    heart: { title: '{{trans("messages.heart")}}', selected: '' },
    upper_limb: { title: '{{trans("messages.upper_limb")}}', selected: '' },
    lower_limb: { title: '{{trans("messages.lower_limb")}}', selected: '' },
}


function detailPhysicalExaminations(data = []) {

    $('#form-physical-examination-table').html('');

    if(data && data.length) {
        $('#panel-physical-examination').removeClass("hide");
        data.forEach(value => {
            const key = value.type;
            const isNormal = !value.value;
            const append = `
            <tr>
                <td>
                    ${physicalData[key].title}
                </td>
                <td>
                    <center>${isNormal ? '{{trans("messages.within_normal_limit")}}' : value.value}</center>
                </td>
            </tr>        
            `
            $('#form-physical-examination-table').append(append);
        })
    } else {
        const dummyData = Object.keys(physicalData).map(value => {
            return {
                type: value,
                subtype: null, // TODO
                value: '-', // no data
            };
        });

        detailPhysicalExaminations(dummyData);
    }


}

</script>

@include('medical_records.print')
@include('medical_records.print_medicine')
@include('medical_records.surat_rujukan')
