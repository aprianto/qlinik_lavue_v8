<div class="modal fade window window-main window-medium" id="detail" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row" id="section-loading">
                    <div class="col-md-12">
                        <div class="widget no-color" id="loading-detail">
                            <div class="card">
                                <span class="three-quarters">Loading&#8230;</span>
                            </div>
                        </div>
                        <div class="widget no-color" id="reload-detail">
                            <div class="card">
                                <span class="reload fa fa-refresh"></span>
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="row hide" id="section-detail">
                    <div class="col-md-12">
                        <div class="widget no-color">
                            <div class="mini-stats ">
                                <span class="red-skin"><i class="fa fa-calendar-o"></i></span>
                                <p><i class="fa fa-clock-o up"></i> {{ trans("messages.date") }}</p>
                                <h3 id="date"></h3>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.patient") }}
                        </h5>
                        <div class="widget no-color">
                            <ul class="detail-profile no-bg-profile">
                                <li class="profile-header">
                                    <div id="photo-patient"></div>
                                    <h3 id="name"></h3>
                                    <span id="mrn"></span>
                                    <div class="btn-sec"> 
                                        <a id="btn-detail-profile">{{ trans("messages.show_detail") }}</a>
                                    </div>
                                </li>
                                <div id="detail-profile" class="hide">
                                    <li class="profile-features">                            
                                        <div class="col-md-6"><i class="icon fa fa-birthday-cake"></i> {{ trans("messages.birth_date") }} :</div> 
                                        <div class="col-md-6"><span id="birth_date"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-mars"></i> {{ trans("messages.gender") }} : </div>
                                        <div class="col-md-6"><span id="gender"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-magic"></i> {{ trans("messages.smoke") }} : </div> 
                                        <div class="col-md-6"><span id="smoke"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-venus"></i> {{ trans("messages.blood_type") }} : </div>
                                        <div class="col-md-6"><span id="blood_type"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-file-text-o"></i> {{ trans("messages.no_identity") }} : </div>
                                        <div class="col-md-6"><span id="no_identity"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-map-marker"></i> {{ trans("messages.address") }} : </div>
                                        <div class="col-md-6"><span id="address"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-envelope-o"></i> {{ trans("messages.email") }} : </div>
                                        <div class="col-md-6"><span id="email"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-phone"></i> {{ trans("messages.phone") }} : </div>
                                        <div class="col-md-6"><span id="phone"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-users"></i> {{ trans("messages.patient_category") }} : </div> 
                                        <div class="col-md-6"><span id="patient_category"></span></div>
                                    </li>
                                    <li class="profile-features">
                                        <div class="col-md-6"><i class="icon fa fa-money"></i> {{ trans("messages.financing") }} : </div>
                                        <div class="col-md-6"><span id="financing"></span></div>
                                    </li>
                                </div>
                            </ul>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.clinic") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="user-sec">
                                <ul id="section-clinic" class="client-list">
                                    <li>
                                        <div id="logo"></div>
                                        <div class="client-info">
                                            <h3><a href="" title="" id="clinic"></a></h3>
                                            <p>&nbsp;</p>
                                        </div>
                                    </li>
                                </ul>                    
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.doctor") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="user-sec">
                                <ul id="section-doctor" class="client-list">
                                    <li>
                                        <div id="photo-doctor"></div>
                                        <div class="client-info">
                                            <h3><a href="" title="" id="doctor"></a></h3>
                                            <p id="polyclinic"></p>
                                        </div>
                                    </li>
                                </ul>                    
                            </div>
                            <div class="footer">
                                <ul>
                                    <li>
                                        <h5>{{ trans("messages.status") }}</h5>
                                        <h3 class="success">{{ trans("messages.done") }}</h3>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.diagnosis") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="visitor-country">
                                <div class="visitor-country-list">
                                    <div class="map-table">
                                        <h3>{{ trans("messages.code") }}</h3><h3>{{ trans("messages.icd") }}</h3>
                                        <ul id="icd">
                                                                             
                                        </ul>
                                    </div>                                                       
                                    <h2>{{ trans("messages.anamnese") }}</h2>
                                    <span class="text" id="anamnese"></span>
                                    <h2>{{ trans("messages.diagnosis") }}</h2>
                                    <span class="text" id="diagnosis"></span>
                                </div>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.service_action") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="our-clients-sec">
                                <ul id="service" class="client-list">
                                </ul>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.medicine") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="our-clients-sec">
                                <ul id="medicine" class="client-list">
                                </ul>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.vital_sign") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="revenew-browser">
                                <ul>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.blood_pressure') }}</h3>
                                            <span id="blood_pressure"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.pulse') }}</h3>
                                            <span id="pulse"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.respiratory_frequency') }}</h3>
                                            <span id="respiratory_frequency"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.body_temperature') }}</h3>
                                            <span id="body_temperature"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.height') }}</h3>
                                            <span id="height"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                    <li>
                                        <div class="browser-detail">
                                            <h3>{{ trans('messages.weight') }}</h3>
                                            <span id="weight"></span>
                                        </div>
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h5 class="border-widget">
                            {{ trans("messages.attachment") }}
                        </h5>
                        <div class="widget no-color">
                            <div class="file-sec" id="attachment">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function() {
    $(".line").peity("line", {
      fill: ["#f2f7f9"],
      height: ["27px"],
      width: ["100"],
      stroke: ["#7dc9f0"]
    })

});

$("#btn-detail-profile").click(function(){ 
    var section = $("#detail-profile").css('display');
    if(section=="block") {
        $("#btn-detail-profile").html("{{ trans('messages.show_detail') }}");
        $("#detail-profile").addClass("hide");
    } else {
        $("#btn-detail-profile").html("{{ trans('messages.hide_detail') }}");
        $("#detail-profile").removeClass("hide");
    }
});

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');

                $("#section-detail #date").html(getFormatDate(data.date)+" "+getFormatBetweenTime(data.start_time, data.end_time));


                $("#logo").replaceWith(thumbnail('md','clinic','{{ $storage }}',data.clinic.logo,data.clinic.name));

                $("#section-detail #clinic").attr("href","#/{{ $lang }}/clinic/"+data.id_clinic);
                $("#section-detail #clinic").attr("onclick","loading($(this).attr('href'))");
                $("#section-detail #clinic").html(data.clinic.name);

                $("#photo-doctor").replaceWith(thumbnail('md','doctor','{{ $storage }}',data.doctor.photo,data.doctor.name));      
                $("#section-detail #doctor").html(data.doctor.name);
                $("#section-detail #doctor").attr("href","#/{{ $lang }}/doctor/"+data.id_doctor);
                $("#section-detail #doctor").attr("onclick","loading($(this).attr('href'));$('#detail').modal('toggle');");
                $("#section-detail #polyclinic").html(data.polyclinic.name);


                var no_identity = "-";
                if(data.patient.id_number!="" && data.patient.id_number!=null) {
                    no_identity = data.patient.id_number+" ("+data.patient.id_type+")";
                }

                $("#photo-patient").replaceWith(thumbnail('lg','patient','{{ $storage }}',data.patient.photo,data.patient.name));

                $("#path").html(data.patient.name);
                $("#section-detail #mrn").html(data.patient.mrn);
                $("#section-detail #name").html(data.patient.name);
                $("#section-detail #birth_date").html(getFormatDate(data.patient.birth_date)+" ("+getAge(data.patient.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#section-detail #gender").html(getGender(data.patient.gender));

                var smoke = "";
                if(data.patient.smoke==1) {
                    smoke = '<i class="fa fa-check-circle success"></i>';
                } else {
                    smoke = '<i class="fa fa-minus-circle failed"></i>';
                }

                $("#section-detail #smoke").html(smoke);
                $("#section-detail #blood_type").html(checkNull(data.patient.blood_type));
                $("#section-detail #no_identity").html(no_identity);
                
                $("#section-detail #address").html(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
                $("#section-detail #email").html(checkNull(data.patient.email));
                $("#section-detail #phone").html("+"+data.patient.phone_code+""+data.patient.phone);
                
                try {
                    $("#section-detail #patient_category").html(data.patient.patient_category.name);
                } catch(err) {}

                $("#section-detail #financing").html(getFinancing(data.patient.financing));


                var icd = "";
                if(data.icd.length>0) {
                    for(var i=0;i<data.icd.length;i++) {
                        no = i+1;
                        icd = icd+"<li><span>"+data.icd[i].code+"</span><i>"+data.icd[i].name+"</i></li>";
                    }
                } else {
                    icd = icd+"<li><span>{{ trans('messages.no_icd') }}</span><i></i></li>";
                }

                $("#section-detail #icd").html(icd);
                $("#section-detail #anamnese").html(checkNull(data.anamnese));
                $("#section-detail #diagnosis").html(data.diagnosis);

                var vital_sign = "-";
                if(data.vital_sign!=null) {
                    $("#section-detail #blood_pressure").html(checkNull(data.vital_sign.blood_pressure));
                    $("#section-detail #pulse").html(checkNull(data.vital_sign.pulse));
                    $("#section-detail #respiratory_frequency").html(checkNull(data.vital_sign.respiratory_frequency));
                    $("#section-detail #body_temperature").html(checkNull(data.vital_sign.body_temperature));
                    $("#section-detail #height").html(checkNull(data.vital_sign.height));
                    $("#section-detail #weight").html(checkNull(data.vital_sign.weight));
                }

                if(data.service.length==0) {
                    $("#section-detail #service").html('<li>'+
                            '<i class="fa fa-minus-circle failed"></i>'+
                            '<div class="client-info">'+
                                '<h3>{{ trans("messages.no_service") }}</h3>'+
                            '</div>'+
                        '</li>');
                } else {
                    var service = "";
                    for(var i=0;i<data.service.length;i++) {
                        no = i+1;

                        service = service+
                        '<li>'+
                            '<i class="fa fa-check-circle"></i>'+
                            '<div class="client-info">'+
                                '<h3>'+data.service[i].service.name+' <span>'+data.service[i].service.code+'</span></h3>'+
                                '<p>{{ trans("messages.quantity") }}: '+data.service[i].quantity+'</p>'+
                            '</div>'+
                        '</li>';
                    }

                    $("#section-detail #service").html(service);
                }

                if(data.medicine.length==0) {
                    $("#section-detail #medicine").html('<li>'+
                            '<i class="fa fa-minus-circle failed"></i>'+
                            '<div class="client-info">'+
                                '<h3>{{ trans("messages.no_medicine") }}</h3>'+
                            '</div>'+
                        '</li>');
                } else {
                    var medicine = "";
                    for(var i=0;i<data.medicine.length;i++) {
                        no = i+1;

                        medicine = medicine+
                        '<li>'+
                            '<i class="fa fa-check-circle"></i>'+
                            '<div class="client-info">'+
                                '<h3>'+data.medicine[i].medicine.name+' <span>'+data.medicine[i].medicine.code+'</span> ('+data.medicine[i].quantity+')</h3>'+
                                '<p>'+data.medicine[i].dose+'</p>'+
                            '</div>'+
                        '</li>';
                    }

                    $("#section-detail #medicine").html(medicine);
                }

                var attachment = "";
                if(data.attachment.length==0) {
                    attachment = "{{ trans('messages.no_attachment') }}";
                }
                for(var i=0;i<data.attachment.length;i++) {
                    no = i+1;

                    var url = "{{ $download_url }}";
                    var typeImage = "";
                    var type = data.attachment[i].attachment.split(".");
                    if(type[1]=="png" || type[1]=="jpg" || type[1]=="jpeg" || type[1]=="gif" || type[1]=="bmp") {
                        typeImage = "image.ico";
                    } else {
                        typeImage = "file.ico";
                    }

                    attachment = attachment+''+'<a href="'+url+'/download/'+data.attachment[i].name+'/'+data.attachment[i].attachment+'">'+
                        '<div class="image">'+
                            '<div class="canvas">'+
                                '<img src="assets/images/'+typeImage+'" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="text">'+
                            '<span>'+data.attachment[i].attachment+'</span>'+
                        '</div>'+
                    '</a>';
                }
                $("#section-detail #attachment").html(attachment);
                
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

$('#detail').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).attr('data-id');
    detail(id);
});


</script>