<script type="text/javascript">

function rujuk(id) {
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medical/records/bpjs/"+id+"/all",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(response, textStatus, request){
            $("#printLetter #confirm").removeClass("loading");
            $("#printLetter #confirm span").addClass("hide");
            $("#printLetter #confirm").attr("disabled", false);
            var a = document.createElement("a");
			a.href = response.file; 
			a.download = response.name;
			document.body.appendChild(a);
			a.click();
			a.remove();
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
            $("#printLetter #confirm").removeClass("loading");
            $("#printLetter #confirm span").addClass("hide");
            $("#printLetter #confirm").attr("disabled", false);
        }
    })
}

function rujukRefer(id) {
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medical/records/bpjs/"+id+"/reffer",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(response, textStatus, request){
            $("#printLetter #confirm").removeClass("loading");
            $("#printLetter #confirm span").addClass("hide");
            $("#printLetter #confirm").attr("disabled", false);
            var a = document.createElement("a");
			a.href = response.file; 
			a.download = response.name;
			document.body.appendChild(a);
			a.click();
			a.remove();
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
            $("#printLetter #confirm").removeClass("loading");
            $("#printLetter #confirm span").addClass("hide");
            $("#printLetter #confirm").attr("disabled", false);
        }
    });
}

</script>