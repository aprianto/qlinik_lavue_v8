<script type="text/javascript">


function report(id) {
    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medical/records/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);

        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");

            $("#reload-document .reload").click(function(){ report(id); });
        }
    })
}

function reportMedical(id) {
    $("#desktop-report").addClass("hide");
    $("#mobile-report").addClass("hide");
    
    $('#report-dialog').attr('class','modal-dialog modal-lg');
    $("input[name='id_report']").val(id);
    $("#report").modal("show");
    report(id);
}

</script>