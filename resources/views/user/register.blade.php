@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 
<div class="account-user-sec registration-sec">
	<div class="account-sec">
		@include('layouts.main-header')
		<div class="acount-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="contact-sec">
							<div class="row">
								<div class="col-md-8">	
									<div class="widget-title">
										<h3>{{ trans('messages.title_register') }}</h3>
										<span>{{ trans('messages.text_register') }}</span>
									</div>
									<div class="account-form alert-notif">
										<form>
											<div class="row">
												<div class="col-md-6 feild input-feild-register" id="name-group">
													<label>{{ trans('messages.name') }}</label>
													<input type="text" id="name" name="name" placeholder="{{ trans('messages.input_name') }}" />
													<span class="help-block"></span>
												</div>
												<div class="col-md-6 feild input-feild-register" id="email-group">
													<label>{{ trans('messages.email') }}</label>
													<input type="text" id="email" name="email" placeholder="{{ trans('messages.input_email') }}" />
													<span class="help-block"></span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 feild input-feild-register" id="password-group">
													<label>{{ trans('messages.password') }}</label>
													<input type="password" id="password" name="password" placeholder="{{ trans('messages.password') }}" />
													<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password')"></span>
													<span class="help-block"></span>
												</div>
												<div class="col-md-6 feild input-feild-register" id="password_confirmation-group">
													<label>{{ trans('messages.password_confirmation') }}</label>
													<input type="password" id="password_confirmation" name="password_confirmation" placeholder="{{ trans('messages.input_password_confirmation') }}" />
													<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password_confirmation')"></span>
													<span class="help-block"></span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 feild last-item" id="phone-group">
													<label>{{ trans('messages.phone_number') }}</label>
													<?php 
						                                $list_phone = phone_code();
						                            ?>

													<select id="phone_code" name="phone_code" class="select-phone-code">
														@foreach($list_phone as $phone)
						                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
						                                @endforeach
													</select>
													<input type="text" id="phone" name="phone" placeholder="{{ trans('messages.input_phone_number') }}" class="phone" onkeydown="number(event)" />
													<span class="help-block"></span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 feild-info" id="license-group">
													<div class="sf-check no-label">
						                                <label>
						                                	<input type="checkbox" name="type_admin_cost"><span></span> {{ trans('messages.text_license') }} 
						                                	<a title="" data-toggle='modal' data-target='#license_agreemen'>{{ trans('messages.license_agreemen') }}</a>
						                                </label>
						                            </div>
												</div>
											</div>
											<div class="row">
												<div class="feild last-item">
													<button type="submit" class="button-login">
						                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                                {{ trans('messages.register') }}
						                            </button>
												</div>
											</div>
										</form>
									</div>
									<div class="more-option">
										<span>{{ trans('messages.or') }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="creat-an-account">
										<h3>{{ trans('messages.have_account') }}</h3>
										<span>{{ trans('messages.info_have_account') }}</span>
										<button title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/login'" class="button-login">{{ trans('messages.login') }}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					@include('layouts.footer-login')
				</div>
			</div>
		</div>
	</div>
</div>
@include('_partial.license')


<script>


$("select[name='phone_code']").wSelect();
$("select[name='phone_code']").val('62').change();

$("input[name=name]").bind("focus", function () {
    resetValidation('name');
});

$("input[name=email]").bind("focus", function () {
    resetValidation('email');
});

$("input[name=password]").bind("focus", function () {
    resetValidation('password');        
});

$("input[name=password_confirmation]").bind("focus", function () {
    resetValidation('password_confirmation');
});

$("input[name=phone]").bind("focus", function () {
    resetValidation('phone');
});

var license = false;
$("input[name='type_admin_cost']").click(function(e){
	license = e.target.checked ? e.target.checked : false;
});

$("form").submit(function(event) {
	event.preventDefault();
	resetValidation('name','email', 'password', 'password_confirmation', 'phone');
	$("form button").attr("disabled", true);

	var name = $("#name").val();
	var email = $("#email").val();
	var password = $("#password").val();
	var password_confirmation = $("#password_confirmation").val();
	var phone_code = $('#phone_code option:selected').val();
	var phone = $("#phone").val();

	if (!validate(name, email, password, password_confirmation, phone, license)) return;

    formData= new FormData();

    formData.append("name", name);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("password_confirmation", password_confirmation);
    formData.append("phone_code", phone_code)
    formData.append("phone", parseInt(phone));

    $(".feild button").addClass("loading");
    $(".feild button span").removeClass("hide");

	$.ajax({
		url: "{{ $api_url }}/{{ $lang }}/user/register",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: function(data){
			$("form button").attr("disabled", false);
			$(".feild button").removeClass("loading");
			$(".feild button span").addClass("hide");

			if(!data.error) {
				if(!data.success) {
					formValidate(false,['name',data.errors.name, true], ['email',data.errors.email, true], ['password',data.errors.password, true], ['password_confirmation',data.errors.password_confirmation, true], ['phone',data.errors.phone, true]);
				} else {
					notif(true,'<?php echo trans("validation.success_register",["link"=>"#/".$lang."/login"]); ?>', true);
				}
			} else {
				notif(false,"{{ trans('validation.failed') }}");
			}
		},
		error: function(){
			$("form button").attr("disabled", false);
			$(".feild button").removeClass("loading");
			$(".feild button span").addClass("hide");
			notif(false,"{{ trans('validation.failed') }}");
		}
	}); 
});

function validate(name, email, password, password_confirmation, phone, licence) {
	!name && formValidate(false,['name', '{{ trans("validation.empty_name") }}', true]);
	!email && formValidate(false,['email', '{{ trans("validation.empty_email") }}', true]);
	!password && formValidate(false,['password', '{{ trans("validation.empty_password") }}', true]);
	!phone && formValidate(false,['phone', '{{ trans("validation.empty_phone") }}', true]);
	!license && notif(false,"{{ trans('validation.license') }}");
	$("form button").attr("disabled", false);
	return name && email && password && phone && license
}

</script>

