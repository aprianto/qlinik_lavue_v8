@if($user->error==true)
<script>
	notif(false,"{{ trans('validation.error_code') }}");
	setTimeout(function(){
		redirect('redirect/{{ $lang }}/forgot-password');
	}, 1000);   
</script>
@else
@include('layouts.lib')
<div id="progressBar">
	<div class="loader"></div>
</div>
<div class="maintainance">
	<div class="container">
		<div class="maintainance-mode">
			<div class="logo">
		        <a href="#/" title="" class="text" onclick="loading($(this).attr('href'))"><img src="{{ $storage }}/images/icons/logo.png" /></a>
		    </div>
		    <h2>{{ trans('messages.text_reset_password') }}</h2>
		    <span>{{ $user->email }}</span>
			<div class="fancy-social">
				<span>{{ $user->name }}</span>
				<?php echo thumbnail('sm','user',$storage,$user->photo,$user->name); ?>
			</div>
			<form class="subscribtion">
				<table>
					<tr>
						<td width="75%"><input type="password" name="password" placeholder="{{ trans('messages.password') }}"></td>
						<td rowspan="2">
							<button type="submit">
								<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
								{{ trans('messages.save') }}
							</button>
						</td>
					</tr>
					<tr>
						<td><input type="password" name="password_confirmation" placeholder="{{ trans('messages.password_confirmation') }}"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script>
$("form").submit(function(event) {
	event.preventDefault();
	$("form button").attr("disabled", true);

    formData= new FormData();

    formData.append("password", $("input[name='password']").val());
    formData.append("password_confirmation", $("input[name='password_confirmation']").val());
    formData.append("validate", false);
    $(".subscribtion button").addClass('loading');
	$(".subscribtion button span").removeClass('hide');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/reset-password/{{ $code }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("form button").attr("disabled", false);
    
            $(".subscribtion button").removeClass('loading');
			$(".subscribtion button span").addClass('hide');

			if(!data.error) {
                if(!data.success) {
                	if(data.errors.password) {
                		notif(false,""+data.errors.password+"");
                	} else if(data.errors.password_confirmation) {
                		notif(false,""+data.errors.password_confirmation+"");
                	}
                } else {
                	notif(true,"{{ trans('validation.success_reset_password') }}");
                	
                	setTimeout(function(){
						redirect('redirect/{{ $lang }}/login');
					}, 1000);
                }
            } else {
         		notif(false,"{{ trans('validation.failed') }}");
         	}
        },
        error: function(){
        	$("form button").attr("disabled", false);

			$(".subscribtion button").removeClass('loading');
			$(".subscribtion button span").addClass('hide');

			notif(false,"{{ trans('validation.failed') }}");
		}
    })
});

</script>
@endif
