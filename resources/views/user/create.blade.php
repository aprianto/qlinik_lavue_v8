@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_user') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form id="form-add" role="form" class="sec">                        
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_name') }}" name="name">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} <span>*</span></label></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="password-group">
                            <label class="control-label">{{ trans('messages.password') }} <span>*</span></label></label>
                            <input type="password" class="form-control" placeholder="{{ trans('messages.input_password') }}" id="password" name="password">
                            <span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password')"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="password_confirmation-group">
                            <label class="control-label">{{ trans('messages.password_confirmation') }} <span>*</span></label></label>
                            <input type="password" class="form-control" placeholder="{{ trans('messages.input_password_confirmation') }}" id="password_confirmation" name="password_confirmation">
                            <span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password_confirmation')"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="role-group">
                            <label class="control-label">{{ trans('messages.role') }} <span>*</span></label>
                            <div class="border-group">
                                <select id="role" name="role[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/user" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-add select[name='phone_code']").wSelect();
$("#form-add select[name='phone_code']").val('62').change();

$("#form-add #role-group #role").select2({
    placeholder: "{{ trans('messages.select_role') }}"
});

function listRole(val) {
    resetValidation('form-add #role');
    loading_content("#form-add #role-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/role",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-add #role-group", "success");              

                $("#form-add #role-group .border-group select").html("");
                $("#form-add #role-group .border-group select").prop("disabled", false); 
               
                item = data.data;

                if(item.length>0) {
                    for (var i = 0; i < item.length; i++) {
                        if(val.length>0) {
                            var selected = "";
                            for (var j = 0; j < val.length; j++) {
                                if(item[i].id==val[j]) {
                                    selected = "selected='selected'";
                                }

                            }
                        }

                        $("#form-add #role-group #role").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                    }

                    $("#form-add #role-group #role").select2({
                        placeholder: "{{ trans('messages.select_role') }}",
                        allowClear: false
                    });                        

                } else {
                    $("#form-add #role-group .border-group select").prop("disabled", true);
                    $("#form-add #role-group .border-group select").val("").trigger("change");
                    $("#form-add #role-group .border-group select").select2({
                        placeholder: "{{ trans('messages.no_role') }}"
                    });
                }
                

            } else {
                loading_content("#form-add #role-group", "failed");

                $("#form-add #role-group #loading-content").click(function(){ listRole(val); });
            }
        },
        error: function(){
            loading_content("#form-add #role-group", "failed");

            $("#form-add #role-group #loading-content").click(function(){ listRole(val); });
        }
    })
}

listRole('');

$('#form-add input[name=name]').focus(function() { resetValidation('form-add #name'); });
$('#form-add input[name=email]').focus(function() { resetValidation('form-add #email'); });
$('#form-add input[name=phone]').focus(function() { resetValidation('form-add #phone'); });
$('#form-add input[name=password]').focus(function() { resetValidation('form-add #password'); });
$('#form-add input[name=password_confirmation]').focus(function() { resetValidation('form-add #password_confirmation'); });
$('#form-add #role-group .border-group').click(function() { resetValidation('form-add #role'); });

$("#form-add").submit(function(event) {
    resetValidation('form-add #name', 'form-add #email', 'form-add #phone', 'form-add #password', 'form-add #password_confirmation', 'form-add #role');

    event.preventDefault();
    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    var validate = false;

    var scrollValidate = true;

    var role = $('#form-add select[name="role[]"]').val();
    if(role==null) {
        validate = true;
        formValidate(scrollValidate, ['form-add #role','{{ trans("validation.min_role") }}', true]);
        formData.append("role", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectRole = new Array(); 
        $('#form-add select[name="role[]"] :selected').each(function (i, selected) {
            selectRole[i] = $(selected).val();
            formData.append("role["+i+"]", selectRole[i]);
        });
    }

    formData.append("validate", validate);

    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("email", $("#form-add input[name=email]").val());
    formData.append("password", $("#form-add input[name=password]").val());
    formData.append("password_confirmation", $("#form-add input[name=password_confirmation]").val());
    formData.append("phone_code", $("#form-add select[name='phone_code'] option:selected").val());
    formData.append("phone", parseInt($("#form-add input[name=phone]").val()));
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    formValidate(scrollValidate, ['form-add #name',data.errors.name, true], ['form-add #email',data.errors.email, true], ['form-add #phone',data.errors.phone, true], ['form-add #password',data.errors.password, true], ['form-add #password_confirmation',data.errors.password_confirmation, true]);
                } else {

                    if(validate==false) {

                        $("#form-add input[name=name]").val("");
                        $("#form-add input[name=email]").val("");
                        $("#form-add input[name=phone]").val("");
                        $("#form-add select[name='phone_code']").val('62').change();
                        $("#form-add input[name=password]").val("");
                        $("#form-add input[name=password_confirmation]").val("");

                        $("#form-add #role-group .border-group select").prop("disabled", false);  
                        $("#form-add #role-group .border-group select").val("").trigger("change");                      
                        resetValidation('form-add #name', 'form-add #email', 'form-add #phone', 'form-add #password', 'form-add #password_confirmation', 'form-add #role');
                        
                        notif(true,"{{ trans('validation.success_add_user') }}");

                        setTimeout(function(){
                            loading('#/{{ $lang }}/user');
                            redirect('#/{{ $lang }}/user');
                        }, 500);
                    }
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>