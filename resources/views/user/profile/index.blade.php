@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div>

<div class="panel-content">
	<div class="row">
		<div class="col-md-10">
			<div class="profile-sec">
				<div class="profile-banner-sec">
					<div class="bg-banner">
					</div>
					<ul>
						<li id="edit-photo">
							<label for="photo"><span class="icon-sec"><img src="{{ asset('assets/images/icons/action/upload.png') }}" /></span> <span>{{ trans('messages.upload_photo_profile') }}</span></label>
						</li>
					</ul>
					<div class="canvas-banner">

					</div>
				</div>
				<div class="user-bar">
					<div class="row">
						<div class="col-md-12">
							<div class="user-thumb">
								<div class="canvas-photo profile">
									@if($user->photo!=null)
									<div id="reset-photo" class="reset-photo">
										<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />
									</div>
									@endif
									@if($level == 'doctor')
									<?php echo thumbnail('hw', 'doctor', $storage, $user->photo, $user->name, 128, 128); ?>
									@else
									<?php echo thumbnail('hw', 'user', $storage, $user->photo, $user->name, 128, 128); ?>
									@endif
								</div>
								<div class="user-info">
									<h3>{{ $user->name }}</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="profile-all">
					<div class="row">
						<div class="col-md-12">
							<div class="widget widget-detail">
								<div class="widget-controls">
									<a href="#/{{ $lang }}/profile/setting" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.info') }}</h3>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="contact-details-info">
												<ul>
													<li>
														<div class="detail-sec">
															<div class="icon-sec">
																<img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
															</div>
															<div class="info-sec">
																<span class="title">{{ trans('messages.phone_number') }}</span>
																<span>+{{ $user->phone_code }}{{ $user->phone }}</span>
															</div>
														</div>
													</li>
													<li>
														<div class="detail-sec">
															<div class="icon-sec">
																<img src="{{ asset('assets/images/icons/info/email.png') }}" />
															</div>
															<div class="info-sec">
																<span class="title">{{ trans('messages.email') }}</span>
																<span>{{ checkNull($user->email) }}</span>
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget widget-detail" id="widget-login">
								<div class="widget-controls">
									<a id="link-edit-login"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									<a id="link-cancel-edit-login" class="hide"><img src="{{ asset('assets/images/icons/nav/delete-m.png') }}" /></a>
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.password') }}</h3>
									</div>
									<div class="cell hide" id="loading-page">
										<div class="card">
											<span class="three-quarters">Loading&#8230;</span>
										</div>
									</div>
									<div class="row" id="section-login">
										<div class="col-md-12">
											<div class="contact-details-info">
												<ul>
													<li>
														<div class="detail-sec">
															<div class="icon-sec">
																<img src="{{ asset('assets/images/icons/info/password.png') }}" />
															</div>
															<div class="info-sec">
																<span class="title">{{ trans('messages.password') }}</span>
																<span>******</span>
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<form id="edit-login" class="hide alert-notif">
										<div class="form-group" id="old_password-group">
											<label class="control-label">{{ trans('messages.old_password') }}</label>
											<input type="password" class="form-control" placeholder="{{ trans('messages.input_old_password') }}" id="old_password" name="old_password">
											<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('old_password')"></span>
											<span class="help-block"></span>
										</div>
										<div class="form-group" id="password-group">
											<label class="control-label">{{ trans('messages.new_password') }}</label>
											<input type="password" class="form-control" placeholder="{{ trans('messages.input_new_password') }}" id="password" name="password">
											<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password')"></span>
											<span class="help-block"></span>
										</div>
										<div class="form-group" id="password_confirmation-group">
											<label class="control-label">{{ trans('messages.password_confirmation') }}</label>
											<input type="password" class="form-control" placeholder="{{ trans('messages.input_password_confirmation') }}" id="password_confirmation" name="password_confirmation">
											<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password_confirmation')"></span>
											<span class="help-block"></span>
										</div>
										<div class="form-group last-item">
											<button type="submit" id="btn-edit-login">
												<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
												{{ trans('messages.save') }}
											</button>
										</div>
									</form>
								</div>
							</div>
							<div class="widget">
								<div class="widget-controls">
									@if($level != 'doctor')
									@if(isset($acl->clinic))
									@if($acl->clinic->update==true)
									<a href="#/{{ $lang }}/clinic" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									@endif
									@endif
									@endif

								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.clinic') }}</h3>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="contact-details-info">
												<ul>
													<li>
														<div class="detail-sec">
															<div class="icon-sec">
																<img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
															</div>
															<div class="info-sec">
																<span class="title">{{ trans('messages.clinic') }}</span>
																@if($level != 'doctor')
																<span>{{ $user->clinic->name }}</span>
																@else
																<span>{{ $clinic->name }}</span>
																@endif
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="form-photo">
	<input accept="image/*" type="file" name="photo[]" id="photo" class="new-img" style="visibility: hidden" />
</form>

<script type="text/javascript">
	var level = "<?php echo $level; ?>";

	$("#link-cancel-edit-login").click(function() {
		$("#edit-login").addClass("hide");
		$("#section-login").removeClass("hide");

		$('#link-edit-login').removeClass('hide');
		$('#link-cancel-edit-login').addClass('hide');
	});

	$("#link-edit-login").click(function() {
		$("#edit-login").removeClass("hide");
		$("#section-login").addClass("hide");

		$('#link-edit-login').addClass('hide');
		$('#link-cancel-edit-login').removeClass('hide');
	});

	$("input[name='old_password']").focus(function() {
		resetValidation('old_password');
	});

	$("input[name='password']").focus(function() {
		resetValidation('password');
	});

	$("input[name='password_confirmation']").focus(function() {
		resetValidation('password_confirmation');
	});

	$("#edit-login").submit(function(event) {
		resetValidation('old_password', 'password', 'password_confirmation');

		var old_password = $("input[name='old_password']").val();

		var scrollValidate = true;
		var error = false;

		if (old_password == "") {
			formValidate(scrollValidate, ['old_password', '{{ trans("validation.empty_old_password") }}', true]);
			error = true;
			if (scrollValidate == true) {
				scrollValidate = false;
			}
		} else {
			event.preventDefault();
			$("#edit-login button").attr("disabled", true);

			$("#btn-edit-login").addClass('loading');
			$("#btn-edit-login span").removeClass('hide');
			$('#widget-login').addClass('loading-wait');

			formData = new FormData();
			var passwordURL = "{{ $api_url }}/{{ $lang }}/" + (level === "doctor" ? level : "user") + "/password"
			// var passwordURL = level === "doctor" ? "{{ $api_url }}/{{ $lang }}/doctor/password" : "{{ $api_url }}/{{ $lang }}/user/password";
			formData.append("password", old_password);
			$.ajax({
				url: passwordURL,
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function(dataPassword) {

					$("#edit-login button").attr("disabled", false);

					if (!dataPassword.error) {
						if (dataPassword.password == true) {

							var password = $("input[name='password']").val();
							var password_confirmation = $("input[name='password_confirmation']").val();

							formData = new FormData();
							formData.append("validate", false);
							formData.append("_method", "PATCH");
							formData.append("password", password);
							formData.append("password_confirmation", password_confirmation);
							formData.append("level", (level === "doctor" ? level : "user"));
							formData.append("user", "{{ $id_user }}");

							if (password == "") {
								formValidate(scrollValidate, ['password', '{{ trans("validation.empty_password") }}', true]);
								error = true;
								if (scrollValidate == true) {
									scrollValidate = false;
								}
							}

							if (password_confirmation == "") {
								formValidate(scrollValidate, ['password_confirmation', '{{ trans("validation.empty_password_confirmation") }}', true]);
								error = true;
								if (scrollValidate == true) {
									scrollValidate = false;
								}
							}
							var requestPasswordURL = "{{ $api_url }}/{{ $lang }}/" + (level === "doctor" ? "doctor" : "user");
							if (error == false) {
								$.ajax({
									url: requestPasswordURL,
									type: "POST",
									data: formData,
									processData: false,
									contentType: false,
									success: function(data) {


										$('#widget-login').removeClass('loading-wait');
										$("#btn-edit-login").removeClass('loading');
										$("#btn-edit-login span").addClass('hide');

										if (!data.error) {
											if (!data.success) {

												formValidate(scrollValidate, ['password', data.errors.password, true], ['password_confirmation', data.errors.password_confirmation, true]);

											} else {
												notif(true, "{{ trans('validation.success_edit_password') }}");

												$("#edit-login").addClass("hide");
												$("#section-login").removeClass("hide");

												$('#link-edit-login').removeClass('hide');
												$('#link-cancel-edit-login').addClass('hide');

												$("input[name='old_password']").val("");
												$("input[name='password']").val("");
												$("input[name='password_confirmation']").val("");
											}
										} else {
											notif(false, "{{ trans('validation.failed') }}");
										}

									},
									error: function() {
										$('#widget-login').removeClass('loading-wait');
										$("#btn-edit-login").removeClass('loading');
										$("#btn-edit-login span").addClass('hide');

										notif(false, "{{ trans('validation.failed') }}");
									}
								});
							} else {
								$('#widget-login').removeClass('loading-wait');
								$("#btn-edit-login").removeClass('loading');
								$("#btn-edit-login span").addClass('hide');
							}
						} else {
							formValidate(scrollValidate, ['old_password', '{{ trans("validation.wrong_old_password") }}', true]);

							$('#widget-login').removeClass('loading-wait');
							$("#btn-edit-login").removeClass('loading');
							$("#btn-edit-login span").addClass('hide');
						}
					} else {
						$("#edit-login button").attr("disabled", false);

						$('#widget-login').removeClass('loading-wait');
						$("#btn-edit-login").removeClass('loading');
						$("#btn-edit-login span").addClass('hide');

						notif(false, "{{ trans('validation.failed') }}");
					}

				},
				error: function() {
					$("#edit-login button").attr("disabled", false);

					$('#widget-login').removeClass('loading-wait');
					$("#btn-edit-login").removeClass('loading');
					$("#btn-edit-login span").addClass('hide');

					notif(false, "{{ trans('validation.failed') }}");
				}
			});
		}
	});



	$('.new-img').change(function(event) {
		setTimeout(function() {
			event.preventDefault();
			$(".new-img").attr("disabled", true);

			var id = "{{ $user->id }}";

			formData = new FormData();
			formData.append("_method", "PATCH");
			formData.append("validate", false);
			formData.append("leve", "user");
			formData.append("user", "{{ $id_user }}");

			var photo = document.getElementById("photo");
			file = photo.files[0];
			formData.append("photo", file);

			$("#edit-photo").addClass('loading');
			$("#edit-photo label .icon-sec").html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>');

			$.ajax({
				url: "{{ $api_url }}/{{ $lang }}/{{ $level }}",
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function(data) {

					$(".new-img").attr("disabled", false);

					$("#edit-photo").removeClass('loading');
					$("#edit-photo label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

					if (!data.error) {
						if (!data.success) {
							notif(false, data.errors.photo);
						} else {
							notif(true, "{{ trans('validation.success_edit_photo_profile') }}");

							setTimeout(function() {
								redirect('redirect/{{ $lang }}/profile');
							}, 500);
						}
					} else {
						notif(false, "{{ trans('validation.failed') }}");
					}
				},
				error: function() {
					$(".new-img").attr("disabled", false);

					$("#edit-photo").removeClass('loading');
					$("#edit-photo label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

					notif(false, "{{ trans('validation.failed') }}");
				}
			})

		}, 100);
	});

	$("#reset-photo").click(function() {
		event.preventDefault();
		$("#reset-photo").attr("disabled", true);

		$("#reset-photo").html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>');

		$.ajax({
			url: "{{ $api_url }}/{{ $lang }}/{{ $level }}/reset-photo",
			type: "GET",
			processData: false,
			contentType: false,
			success: function(data) {

				$("#reset-photo").attr("disabled", false);

				$("#reset-photo").html('<img class="icon" src="{{ asset(`assets/images/icons/action/delete.png`) }}" />');

				if (!data.error) {
					if (!data.success) {
						notif(false, "{{ trans('validation.failed') }}");
					} else {
						setTimeout(function() {
							redirect('redirect/{{ $lang }}/profile');
						}, 500);
					}
				} else {
					notif(false, "{{ trans('validation.failed') }}");
				}
			},
			error: function() {
				$("#reset-photo").attr("disabled", false);

				$("#reset-photo").html('<img class="icon" src="{{ asset(`assets/images/icons/action/delete.png`) }}" />');

				notif(false, "{{ trans('validation.failed') }}");
			}
		})

	});
</script>