@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.edit_profile') }}
                    </h3>
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide">
                    <form role="form" class="sec">
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.name') }} <span>*</span></label>
							<input type="text" name="name" class="form-control" placeholder="{{ trans('messages.input_name') }}"  />
							<span class="help-block"></span>
						</div>
						<div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} <span>*</span></label>
							<input type="text" name="email" class="form-control" placeholder="{{ trans('messages.input_email') }}"  />
							<span class="help-block"></span>
						</div>
						<div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/profile" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                            	{{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

$("select[name='phone_code']").wSelect();
$("select[name='phone_code']").val('{{ $user->phone_code }}').change();

function edit() {
	$('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/profile",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading').addClass('hide');
            if(!data.error) {
            	$('.form-elements-sec').removeClass('hide');
                
	            $("input[name=name]").val(data.name);
	            $("input[name=email]").val(data.email);
	            $("select[name='phone_code']").val(data.phone_code).change();
                $("input[name=phone]").val(data.phone);
        	} else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ edit(); });
        	}
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ edit(); });
        }
    });
}

edit();

$("input[name=name]").click(function() {
	resetValidation('name');
});

$("input[name=email]").click(function() {
	resetValidation('email');
});

$("input[name=phone]").click(function() {
	resetValidation('phone');
});

$("form").submit(function(event) {
	event.preventDefault();
	resetValidation('name','email', 'phone');

	$("form button").attr("disabled", true);

    var id = "{{ $id_user }}";
    formData= new FormData();
    formData.append("_method", "PATCH");		    
    formData.append("validate", false);
    formData.append("name", $("input[name=name]").val());
    formData.append("email", $("input[name=email]").val());
    formData.append("phone_code", $("select[name='phone_code'] option:selected").val());
    formData.append("phone", parseInt($("input[name=phone]").val()));
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
	$(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

			if(!data.error) {
	            if(!data.success) {
	                formValidate(true, ['name',data.errors.name, true], ['email',data.errors.email, true], ['phone',data.errors.phone, true]);
	            } else {
	               
	                notif(true,"{{ trans('validation.success_edit_profile') }}");

	                setTimeout(function(){
						redirect('redirect/{{ $lang }}/profile');
					}, 1000);
	            }
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("form button").attr("disabled", false);

        	$(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>
