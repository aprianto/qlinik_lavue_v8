@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 
<div class="account-user-sec registration-sec">
	<div class="account-sec">
		@include('layouts.main-header')
		<div class="acount-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="contact-sec-center">
							<div class="row">
								<div class="col-md-6">	
									<div class="widget-title">
										<h3>{{ trans('messages.title_forgot_password') }}</h3>
										<span>{{ trans('messages.text_forgot_password') }}</span>
									</div>
									<div class="account-form alert-notif">
										<form>
											<div>
												<div class="feild">
													<label>{{ trans('messages.email') }}</label>
													<input type="text" name="email" placeholder="{{ trans('messages.input_email') }}" />
												</div>
												<div class="feild last-item">
													<button type="submit" class="btn-right button-login">
						                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                                {{ trans('messages.submit') }}
						                            </button>
												</div>
											</div>
										</form>
									</div>
									<div class="more-option">
										<span>{{ trans('messages.or') }}</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="creat-an-account">
										<h3>{{ trans('messages.have_account') }}</h3>
										<span>{{ trans('messages.info_have_account') }}</span>
										<button title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/login'">{{ trans('messages.login') }}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					@include('layouts.footer-login')
				</div>
			</div>
		</div>
	</div>
</div>


<script>
$("form").submit(function(event) {
	event.preventDefault();
    $("form button").attr("disabled", true);

    formData= new FormData();

    formData.append("email", $("input[name='email']").val());
    formData.append("validate", false);
    $(".feild button").addClass("loading");
    $(".feild button span").removeClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/forgot-password",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("form button").attr("disabled", false);
    
            $(".feild button").removeClass("loading");
            $(".feild button span").addClass("hide");

            if(!data.error) {
                if(!data.success) {                	
                	if(data.errors.email) {
                		notif(false,""+data.errors.email+"");
                	}
                } else {
                	notif(true,"{{ trans('validation.success_send_reset_password') }}");                   
                }
            } else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("form button").attr("disabled", false);

			$(".feild button").removeClass("loading");
            $(".feild button span").addClass("hide");

			notif(false,"{{ trans('validation.failed') }}");
		}
    })
});

</script>
