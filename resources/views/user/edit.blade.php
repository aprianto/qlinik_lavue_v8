@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.edit_user_role') }}
                    </h3>
                </div>
                <div class="cell" id="loading-edit">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload-edit">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide" id="section-edit">
                    <form id="form-edit" role="form" class="sec">                        
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.name') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_name') }}" name="name" disabled="disabled">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }}</label></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email" disabled="disabled">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)" disabled="disabled">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="role-group">
                            <label class="control-label">{{ trans('messages.role') }} <span>*</span></label>
                            <div class="border-group">
                                <select id="role" name="role[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/user" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-edit #role-group #role").select2({
    placeholder: "{{ trans('messages.select_role') }}"
});

function listRole(val) {
    resetValidation('form-edit #role');
    loading_content("#form-edit #role-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/role",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {  
                loading_content("#form-edit #role-group", "success");              

                $("#form-edit #role-group .border-group select").html("");
                $("#form-edit #role-group .border-group select").prop("disabled", false); 
               
                item = data.data;

                if(item.length>0) {
                    for (var i = 0; i < item.length; i++) {
                        if(val.length>0) {
                            var selected = "";
                            for (var j = 0; j < val.length; j++) {
                                if(item[i].name==val[j]) {
                                    selected = "selected='selected'";
                                }

                            }
                        }

                        $("#form-edit #role-group #role").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                    }

                    $("#form-edit #role-group #role").select2({
                        placeholder: "{{ trans('messages.select_role') }}",
                        allowClear: false
                    });                        

                } else {
                    $("#form-edit #role-group .border-group select").prop("disabled", true);
                    $("#form-edit #role-group .border-group select").val("").trigger("change");
                    $("#form-edit #role-group .border-group select").select2({
                        placeholder: "{{ trans('messages.no_role') }}"
                    });
                }
                

            } else {
                loading_content("#form-edit #role-group", "failed");

                $("#form-edit #role-group #loading-content").click(function(){ listRole(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #role-group", "failed");

            $("#form-edit #role-group #loading-content").click(function(){ listRole(val); });
        }
    })
}

//listRole('');

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#section-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading-edit').addClass('hide');
                $('#section-edit').removeClass('hide');

                $("#form-edit input[name=name]").val(data.name);
                $("#form-edit input[name=email]").val(data.email);
                $("#form-edit input[name=phone]").val('+'+data.phone_code+''+data.phone);

                listRole(data.role);        

            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }
            

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    });
}

edit("{{ $id }}");

$('#form-edit #role-group .border-group').click(function() { resetValidation('form-edit #role'); });

$("#form-edit").submit(function(event) {
    resetValidation('form-edit #role');

    event.preventDefault();
    $("#form-edit button").attr("disabled", true);

    formData= new FormData();

    var validate = false;

    var scrollValidate = true;

    var role = $('#form-edit select[name="role[]"]').val();
    if(role==null) {
        validate = true;
        formValidate(scrollValidate, ['form-edit #role','{{ trans("validation.min_role") }}', true]);
        formData.append("role", "");

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        var selectRole = new Array(); 
        $('#form-edit select[name="role[]"] :selected').each(function (i, selected) {
            selectRole[i] = $(selected).val();
            formData.append("role["+i+"]", selectRole[i]);
        });
    }

    formData.append("validate", validate);

    formData.append("_method", "PATCH");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/{{ $id }}/role",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-edit button").attr("disabled", false);
    
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                } else {

                    if(validate==false) {

                      
                        resetValidation('form-edit #role');
                        
                        notif(true,"{{ trans('validation.success_edit_user_role') }}");

                        setTimeout(function(){
                            loading('#/{{ $lang }}/user');
                            redirect('#/{{ $lang }}/user');
                        }, 500);
                    }
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
            
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>