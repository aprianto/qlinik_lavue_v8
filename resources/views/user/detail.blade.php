@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content" id="detail">    
    <div class="row" id="section-loading">
        <div class="col-md-9">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-profile">
        <div class="col-md-9">            
            <div class="admin-follow no-bg">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.user_profile') }}
                    </h3>
                </div>
                <img id="photo-user" src="{{ $storage.'/images/user/photo.png' }}" />
                <h3 id="name"></h3>
                <span id="username"></span>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-9">            
            <div class="widget widget-detail twitter-feed">
                <div class="twitter-account">
                    <h3>{{ trans("messages.info") }}</h3>                    
                </div>
                <ul class="detail-profile">
                    <div id="detail-profile">
                        <li class="profile-features">
                            <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.phone_number') }}</span>
                                    <span id="phone"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.email') }}</span>
                                    <span id="email"></span>
                                </div>
                            </div>
                        </li>    
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.role') }}</span>
                                    <span id="role"></span>
                                </div>
                            </div>
                        </li>          
                    </div>
                </ul>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-profile').addClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-profile').removeClass('hide');
                $('#section-detail').removeClass('hide');

                $("#path").html(data.name);

                var photo_user = "";
                if(data.photo==null) {
                    photo_user = "{{ $storage.'/images/user/photo.png' }}";
                } else {
                    photo_user = "<?php echo $storage.'/images/user/"+data.photo+"'; ?>";
                }

                $("#detail #photo-user").replaceWith(thumbnail('lg','user','{{ $storage }}',data.photo,data.name));

                $("#detail #name").html(data.name);
                $("#detail #username").html(data.username);
                $("#detail #email").html(data.email);
                $("#detail #phone").html("+"+data.phone_code+""+data.phone);

                var role = "";
                for (var i = 0; i < data.role.length; i++) {
                    role = role+''+data.role[i];
                    if(i!=data.role.length-1) {
                        role = role+''+", ";
                    }
                }

                $("#detail #role").html(role);                

            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

detail("{{ $id }}");

</script>
