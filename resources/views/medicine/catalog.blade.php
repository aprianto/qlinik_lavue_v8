


<div class="modal fade report modal-scroll" id="frame" role="dialog" aria-labelledby="reportTitle" aria-hidden="true">
    <div id="frame-dialog" class="modal-dialog modal-lg" role="document">        
        <div class="modal-content">
            <div class="modal-header">                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!--<embed src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqk9q4OrBxQ7bwkHlGBQcw9mmdJPpfTOUYx7hwX8atKAudXXxj_O0RjfmYwn4N56ovhsm_REcp6dyl/pubhtml" type="application/vnd.ms-excel" width="100%" height="600px" />-->
                <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vRqk9q4OrBxQ7bwkHlGBQcw9mmdJPpfTOUYx7hwX8atKAudXXxj_O0RjfmYwn4N56ovhsm_REcp6dyl/pubhtml?widget=true&amp;headers=false" height="500px" width="100%"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-secondary" id="cancel-frame" data-dismiss="modal" aria-label="Close">
                    {{ trans("messages.close") }}
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
