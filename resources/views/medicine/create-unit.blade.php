@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_unit') }}
                    </h3>
                </div>
                <div class="cell hide" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-unit">
                        <div class="form-group" id="unit-group">
                            <label class="control-label">{{ trans('messages.name_unit') }}</label>
                            <div class="border-group">
                                <select id="unit" name="unit[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>                            
                        </div>
                        <div class="form-group last-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" id="icon-add" class="button-add" data-toggle="modal" data-target="#add">+ {{ trans('messages.create_new_unit') }}</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" type="submit">
                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        {{ trans('messages.save') }}
                                    </button>
                                    <a href="#/{{ $lang }}/medicine" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                        {{ trans('messages.cancel') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_unit') }}</h5>
            </div>
            <form id="form-add">
                <div class="modal-body modal-main alert-notif">               
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.name_new_unit') }}<span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.enter_name_new_unit') }}" name="name"  autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

$("#unit").select2({
    placeholder: "{{ trans('messages.choice_unit') }}",
    alowClear: false
});

function listUnit(val) {
    loading_content("#form-unit #unit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/units",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (data) {
            if (!data.error) {
                loading_content("#form-unit #unit-group", "success");
                $("#unit-group .border-group select").html('');
                var list_unit = data.data
                for(i = 0; i < list_unit.length; i++) {
                    if ( val.length > 0 ) {
                        var selected = "";
                        for(j = 0; j < val.length; j++) {
                            if(list_unit[i].id === val[j]) {
                                selected = "selected='selected'";
                            }
                        }
                    }
                    $("#unit").append("<option value='"+list_unit[i].id+"' "+selected+">"+list_unit[i].name+"</option>")
                }
                $("#unit").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                    allowClear: false
                });
            } else {
                loading_content("#form-unit #unit-group", "failed");
                $("#form-unit #unit-group #loading-content").click(function () { listUnit(val); });
            }
        },
        error: function () {
            loading_content("#form-unit #unit-group", "failed");
            $("#form-unit #unit-group #loading-content").click(function () { listUnit(val); });
        }
    });
}

function listUnitClinic() {
    loading_content("#form-unit #unit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/units",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (data) {
            if (!data.error) {
                loading_content("#form-unit #unit-group", "success");
                var item = data.data;
                var item_id = [];
                for(i = 0; i < item.length; i++) {
                    item_id.push(item[i].id)
                }
                listUnit(item_id)
            } else {
                loading_content("#form-unit #unit-group", "failed");
                $("#form-unit #unit-group #loading-content").click(function () { listUnitClinic(); });
            }
        },
        error: function () {
            loading_content("#form-unit #unit-group", "failed");
            $("#form-unit #unit-group #loading-content").click(function () { listUnitClinic(); });
        }
    });
}

listUnitClinic();

$("input[name=name]").on('input', function(){
    resetValidation('form-add #name');
});

$("#form-add").submit(function(event){
    event.preventDefault();
    var name_unit = $("input[name=name]").val();
    if ( name_unit == "" ) {
        formValidate(true, ['form-add #name','Masukkan nama satuan', true]);
    }else {
        $("#form-add button").attr("disabled", true);
        $("#form-add .btn-primary").addClass('loading');
        $("#form-add .btn-primary span").removeClass('hide');
        formData= new FormData();
        formData.append("name", name_unit.charAt(0).toUpperCase() + name_unit.toLowerCase().slice(1));
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/units",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                if ( !data.error ) {
                    var selectUnit = []; 
                    $('#unit :selected').each(function (i, selected) {
                        selectUnit.push($(selected).val());
                    });

                    selectUnit.push(data.data.id);

                    listUnit(selectUnit);
                    $("#form-add button").attr("disabled", false);
                    $("#form-add .btn-primary").removeClass('loading');
                    $("#form-add .btn-primary span").addClass('hide');
                    $("input[name=name]").val("");
                    $("#add").modal("toggle");
                }else {
                    formValidate(true, ['form-add #name',data.errors.name, true]);
                    $("#form-add button").attr("disabled", false);
                    $("#form-add .btn-primary").removeClass('loading');
                    $("#form-add .btn-primary span").addClass('hide');
                }
            },
            error: function(){
                $("#form-add button").attr("disabled", false);
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");
                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
});

$('#add').on('show.bs.modal', function (e) {
    $("#form-add input[name=name]").val("");
    resetValidation('form-add #name');
});

$("#form-unit").submit(function(event){
    event.preventDefault();
    var unit = $("#unit").val();
    if ( unit === null ) {
        formValidate(true, ['form-unit #unit',"{{ trans('messages.choice_unit') }}", true]);
    }else {
        $("#form-unit button").attr("disabled", true);
        $("#form-unit .btn-primary").addClass('loading');
        $("#form-unit .btn-primary span").removeClass('hide');
        
        formData= new FormData();

        var selectUnit = new Array()
        
        $('#unit :selected').each(function (i, selected) {
            selectUnit[i] = $(selected).val();
            formData.append("units["+i+"]", selectUnit[i]);
        });

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/units",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    $("#form-unit button").attr("disabled", false);
                    $("#form-unit .btn-primary").removeClass('loading');
                    $("#form-unit .btn-primary span").addClass('hide');
                    notif(true,"{{ trans('validation.success_select_unit') }}");    
                }else {
                    $("#form-unit button").attr("disabled", false);
                    $("#form-unit .btn-primary").removeClass('loading');
                    $("#form-unit .btn-primary span").addClass('hide');
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-unit button").attr("disabled", false);
                $("#form-unit .btn-primary").removeClass("loading");
                $("#form-unit .btn-primary span").addClass("hide");
                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
})




</script>