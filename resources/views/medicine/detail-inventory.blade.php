@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row hide" id="tutorial">
        <div class="col-md-12">
            <div class="widget">
                <div class="welcome-bar">
                    <div id="text-tutorial">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 id="title-inventory"></h3>
                    <a href="#/{{ $lang }}/help/medicine"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a class="active">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.list_medicine') }}</span>
                        </div>
                    </a>
                    <a data-toggle= "modal" id="modal_add" data-target="#add_price" data-id="{{$id_inventory}}" data-name="">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Tambah/Ubah Harga</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-3">
                                <div class="form-group content-column" id="type-group">
                                    <div class="border-group">
                                        <select class="form-control" name="type">
                                            <option value="all">Semua tipe harga</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-md">{{ trans('messages.code') }}</th>
                                <th class="hide-md">{{ trans('messages.unit') }}</th>
                                <th class="hide-md">{{ trans('messages.price') }}</th>
                                <th class="hide-md">{{ trans('messages.category_patient') }}</th>
                                <th class="hide-md">{{ trans('messages.calculation') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/function.js') }}"></script>
<script>

    var id = "{{$id_inventory}}";
    $("#form-search select[name='type']").select2({
        placeholder: "Pilih tipe harga"
    });

    function getListInventory() {
        $("#loading-table").removeClass("hide");
        $(".table").removeClass("table-hover");
        $("#reload").addClass("hide");
        $("#table").empty();
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/medicine-price?id=" + id,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if(!data.error) {
                    $("#title-inventory").html(data.data.name);
                    $("#modal_add").attr("data-name", data.data.name);
                    $("#loading-table").addClass("hide");
                    $(".table").addClass("table-hover");
                    $(".pagination-sec").removeClass("hide");
                    var tr;
                    var list_data = data.data.price;
                    if(list_data.length > 0) {
                        for (i = 0; i < list_data.length; i++) {
                            tr = $('<tr/>');
                               tr.append("<td><a><h1>" + data.data.name + "</h1></a></td>");
                               tr.append("<td>" + data.data.code + "</td>");
                               tr.append("<td>" + list_data[i].unit.name + "</td>");
                                tr.append("<td>" + formatCurrency('{{ $lang }}','Rp',list_data[i].price.toFixed(0)) + "</td>");
                               tr.append("<td>"+ list_data[i].patient_category.name+"</td>");
                               tr.append("<td>"+ list_data[i].type+"</td>");
                               var action = "<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+list_data[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\"></a>"
                               tr.append("<td>"+
                                    action +'<div class="dropdown">' +
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">' + action + '</ul>' + '</div>' + "</td>");
                                $("#table").append(tr)
                        }
                    }else {
                        $(".pagination-sec").addClass("hide");
                        $("#table").append("<tr><td align='center' colspan='12'>Data tidak ada</td></tr>");
                    }
                }else {
                    $("#loading-table").addClass("hide");
                    $("#reload").removeClass("hide");
                    $("#reload .reload").click(function(){ getListInventory(); });
                }
            },
            error: function() {
                $("#loading-table").addClass("hide");
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ getListInventory(); });
            }
        });
    }
    getListInventory()

    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(e){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-price/"+id,
        type: "DELETE",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#confirmDelete").modal("toggle");   
            if (!data.error ) {
                if ( data.success ) {
                    getListInventory();
                    notif(true,"{{ trans('validation.success_delete_medicine') }}");
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});

</script>

@include ('medicine.create-price')

@include('_partial.confirm_delele')
