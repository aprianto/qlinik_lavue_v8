@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row hide" id="tutorial">
        <div class="col-md-12">
            <div class="widget">
                <div class="welcome-bar">
                    <div id="text-tutorial">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                    <h3>
                        {{ trans('messages.inventory') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/medicine"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);" class="active">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.list_medicine') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-incoming" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-masuk.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_incoming") }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-outgoing" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-keluar.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_outgoing") }}</span>
                        </div>
                    </a>
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-tambah.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_medicine") }}</span>
                        </div>
                    </a>
                    <a data-toggle="modal" data-target="#add_price">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-harga-tambah.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Tambah Harga</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine/unit">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-satuan-tambah.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Tambah satuan</span>
                        </div>
                    </a>
                    <div class="dropdown pull-right">
                      <a data-toggle="dropdown">
                          <div class="image">
                              <div class="canvas">
                                  <img src="{{ asset('assets/images/icons/header/more-horizontal.png') }}" />
                              </div>
                          </div>
                          <div class="text">
                              <span>Lainnya</span>
                          </div>
                      </a>
                      <ul class="dropdown-menu" style="z-index: 0;">
                        <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                      </ul>
                    </div>
                </div>
                <div class="dropdown-rs">
                    <ul class="wrapper-dropdown-rs dropdown-scroll">
                        <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                    </ul>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-3">
                                <div class="form-group content-column" id="type-group">
                                    <div class="border-group">
                                        <select class="form-control" name="type">
                                            <option value="all">Semua Inventori</option>
                                            <option value="dpho">Obat DPHO</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group search-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_medicine_name') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-md">{{ trans('messages.code') }}</th>
                                <th class="hide-md">Harga Satuan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>
$(".pull-right").click(function(){ 
    $(".wrapper-dropdown-rs").toggleClass("open-rs")
});

$("input[name='export_key']").val('medicines');
$("input[name='import_key']").val('medicines');

$("#form-search select[name='type']").select2({
    placeholder: "Silahkan Pilih"
});

if(setup=="medicine") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_medicine_add_medicine"); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('');");
}

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }
    var type = $("#form-search select[name=type] option:selected").val();
    if(type == 'dpho') {
        type = true;
    }else{
        type = '';
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine",
        type: "GET",
        data: "q="+q+"&is_dpho="+type+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");
            if (!data.error) {
                $(".table").addClass("table-hover");
                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total > 0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        tr = $('<tr/>');                        
                        tr.append("<td><a href='#/{{ $lang }}/medicine/detail/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a>"+
                                '<div class="sub show-sm">'+
                                    "<span class='general'>" + item[i].code + "</span>"+
                                '</div>'+
                                "</td>");
                        tr.append("<td class='hide-md'>" + item[i].code + (item[i].kode_dpho != null && item[i].kode_dpho != "undefined" ? " / " + item[i].kode_dpho : '') + "</td>");
                        tr.append("<td class='hide-md'>" + (item[i].price === null ? "<span class='price-list-medicine'>Belum dimasukkan</span>" : formatCurrency('{{ $lang }}','Rp',item[i].price)) + "</td>");
                        
                        let hide = item[i].has_transaction ? "hide" : "";
                        var action = "<a class='btn-table btn-blue drill_down-"+item[i].id+"' onclick=\"showDetail('"+item[i].id+"')\" data-toggle='collapse' data-target='#collapseExample' aria-expanded='false' aria-controls='collapseExample'><img src=\"{{ asset('assets/images/icons/nav/chevron-dm.png') }}\" id='img-detail' /> {{ trans('messages.show_detail') }}</a>"
                            + "<a href='#/{{ $lang }}/medicine/detail/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))' class='btn-table btn-blue'><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /></a>"
                            +"<a data-toggle='modal' data-target='#edit' data-id='"+item[i].id+"' class='btn-table btn-orange'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /></a>"
                            +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red "+hide+"'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /></a>";

                        tr.append("<td>"+
                            action +'<div class="dropdown">' +
                            '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                            '<ul class="dropdown-menu dropdown-menu-right">' + action + '</ul>' + '</div>' + "</td>");
                        $("#table").append(tr);

                        let detail = "";
                        detail += "<div></div>";
                        detail += "<table class='table-detail'><thead><tr>"
                            + "<th>Satuan</th>"
                            + "<th>Range harga</th>" 
                            + "<th>Jumlah</th>"
                            + "</thead></tr><tbody id='list-"+item[i].id+"'></tbody>"
                            + '<tbody>'
                            +'<tr id="loading-list-'+item[i].id+'" class="hide">'
                            + '<td colspan="4" align="center">'
                            + '<div class="card">'
                            + '<span class="three-quarters">Loading&#8230;</span>'
                            + '</div>'
                            + '</td>'
                            +'</tr>'
                            +'<tr id="reload-list-'+item[i].id+'" class="hide">'
                            + '<td colspan="4" align="center">'
                            + '<div class="card">'
                            + '<span class="reload fa fa-refresh"></span>'
                            + '</div>'
                            + '</td>'
                            +'</tr>'
                            + '</tbody>'

                        detail = detail + "</table>";
                        tr = $('<tr id="income-'+item[i].id+'" class="row-drill-down hide" />');
                        tr.append("<td colspan='5'>"+detail+"</td>");
                        $("#table").append(tr);

                        no++;
                    }
                } else {
                    $(".pagination-sec").addClass("hide");
                    if(page==1 && q=='') {
                        $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.empty_medicine') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                }
                pages(page, total, per_page, current_page, last_page, from, to, q);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

function showDetail(no) {
    if($("#income-"+no+"").hasClass('row-drill-down hide')) {
        $("#income-"+no+"").removeClass("hide");
        $("tr td a.drill_down-"+no+"").html("<img src=\"{{ asset('assets/images/icons/nav/chevron-um.png') }}\" id='img-detail' />{{ trans('messages.close_detail') }}");
        getMedicineId(no)
    }else {
        $("#income-"+no+"").addClass("row-drill-down hide");
        $("tr td a.drill_down-"+no+"").html("<img src=\"{{ asset('assets/images/icons/nav/chevron-dm.png') }}\" id='img-detail' />{{ trans('messages.show_detail') }}")
    }
}

function getMedicineId(id) {
    $("#loading-list-"+id+"").removeClass("hide")
    $("#reload-list-"+id+"").addClass("hide")

    $("#income-"+id+" #list-"+id+" ").html("")
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $("#loading-list-"+id+"").addClass("hide")
                let unitRatio = getUnitRatio(data.unit);
                let data_price = data.price_category;
                let detail = "<tr>"
                if(data_price.length > 0) {
                    for (i = 0; i < data_price.length; i++) {
                        let range_price = "<td>"+formatCurrency('{{ $lang }}','Rp',data_price[i].min_price) +" - "+formatCurrency('{{ $lang }}','Rp',data_price[i].max_price)+"</td>";
                        if(data_price[i].min_price === data_price[i].max_price) {
                            range_price = "<td>"+formatCurrency('{{ $lang }}','Rp',data_price[i].min_price) +"</td>"
                        }
                        detail = detail + "<tr>" + "<td>"+data_price[i].unit+"</td>" +range_price + "<td>" + (data.stock ? Math.floor(data.stock * unitRatio.ratio[data_price[i].unit]) : 0) + "</td>"+ "</tr>";
                    }
                }else {
                    detail = detail + "<tr>" + "<td>-</td>" + "<td>-</td>" + "<td>-</td>" + "</tr>";
                }
                $("#income-"+id+" #list-"+id+" ").append(detail)
            }else{
                $("#loading-list-"+id+"").addClass("hide")
                $("#reload-list-"+id+"").removeClass("hide")
                $("#reload-list-"+id+"").click(function(){getMedicineId(id)})
            }
        },
        error: function() {
            $("#loading-list-"+id+"").addClass("hide")
            $("#reload-list-"+id+"").removeClass("hide")
            $("#reload-list-"+id+"").click(function(){getMedicineId(id)})
        }
    });
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch);
                    notif(true,"{{ trans('validation.success_delete_medicine') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", $("input[name=q]").val());
        }, 2000);
    }
});

$("#form-search select[name='type']").on('change', function(e){
    search(1,"true", $("input[name=q]").val());
});

$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val());
});

search(pageRepo,"false",searchRepo);


</script>
@include('medicine.create-price')

@include('medicine.edit')

@include('_partial.confirm_delele')

@include('medicine.create')

@include('medicine.detail')

@include('docs.import')

@include('docs.import_success')

@include('docs.export')
