<div class="modal fade window" id="add_price" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_price_inventory') }}</h5>
            </div>
            <form id="form-add-price" class="">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="inventory-group">
                        <label class="control-label">{{ trans('messages.inventory') }}</label>
                        <div class="border-group">
                            <input name="inventory" placeholder="{{ trans('messages.choice_inventory') }}" class="form-control" data-id="" />
                        </div>
                        <span id="loading-inventory" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    
                    <div class="rasio-title">
                        <h5>{{ trans('messages.price') }}</h5>
                    </div>
                    <div id="section-price">
                        <div class="field_price">
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-12" id="unit_add-group">
                                        <label class="control-label">{{ trans('messages.unit') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="unit_inventory[]">
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6" id="patient_category-group">
                                        <label class="control-label">{{ trans('messages.patient_category') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="patient_category[]">
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">{{ trans('messages.price_calculation') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="type_text[]">
                                                <option value="margin">Margin</option>
                                                <option value="flat">Flat</option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="" id="margin-group">
                                <label class="control-label">{{ trans('messages.selling_price_margin') }}</label>
                                <div class="row">
                                    <div class="col-md-6" id="price-group">
                                        <input type="text" class="form-control" placeholder="Masukkan persentase" name="margin_or_price[]" onkeydown="number(event)" oninput="valuePriceAdd()">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="border-group">
                                            <select class="form-control" name="type_margin[]">
                                                <option value="persentage">Persentase (%)</option>
                                                <option value="fix rate">Fix Rate</option>
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="button">
                        <a class="btn-remove hide" onclick="$('#form-add-price #section-price .btn-remove:last').parent().parent().remove();removePrice_add();">Hapus</a>
                        <a class="btn-add" onclick="addPrice_add()">+ {{ trans('messages.add_price') }}</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

var priceItem_add = 0;
var list_unit = [];
var data_inventory = [];
var data_category = [];
var category_default = "";
var list_inventory = [];
var template_price = ''
var id_price = '';

function getTemplatePrice() {
    return '<div class="field_price form-group">'+
        '<div class="form-group">'+
            '<div class="row">'+
                '<div class="form-group col-md-12" id="unit_add-group">' +
                    '<label class="control-label">{{ trans("messages.unit")}}</label>' +
                    '<div class="border-group">' +
                        '<select class="form-control" name="unit_inventory[]"><option value=""></option></select>' +
                    '</div>' +
                    '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>' +
                    '<span class="help-block"></span>'+
                '</div>' +
                '<div class="col-md-6" id="patient_category-group">'+
                    '<label class="control-label">{{ trans("messages.patient_category") }}</label>'+
                    '<div class="border-group">'+
                        '<select class="form-control" name="patient_category[]"><option value=""></option></select>'+
                    '</div>'+
                    '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="col-md-6">'+
                    '<label class="control-label">{{ trans("messages.price_calculation") }}</label>'+
                    '<div class="border-group">'+
                        '<select class="form-control" name="type_text[]">'+
                            '<option value="margin">Margin</option>'+
                            '<option value="flat">Flat</option>'+
                        '</select>'+
                    '</div>'+
                    '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                    '<span class="help-block"></span>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="" id="margin-group">'+
            '<label class="control-label">{{ trans("messages.selling_price_margin") }}</label>'+
            '<div class="row">'+
                '<div class="col-md-6" id="price-group">'+
                    '<input type="text" class="form-control" placeholder="Masukkan persentase" name="margin_or_price[]" onkeydown="number(event)" oninput="valuePriceAdd()">'+
                    '<span class="help-block"></span>'+
                '</div>'+
                '<div class="col-md-6">'+
                    '<div class="border-group">'+
                        '<select class="form-control" name="type_margin[]">'+
                            '<option value="persentage">Persentase (%)</option>'+
                            '<option value="fix rate">Fix Rate</option>'+
                        '</select>'+
                    '</div>'+
                    '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                    '<span class="help-block"></span>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="form-group hide" id="btn-remove-price">' +
                '<a class="btn-remove" onclick="$(this).parent().parent().remove();removePrice_add();">Hapus</a>' +
           '</div>' +
    '</div>';
}

function listInventory(id) {
    id = id != undefined ? "?id=" + id : "";
    loading_content("#form-add-price #inventory-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-price" + id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function (data) {
            if ( !data.error ) {
                loading_content("#form-add-price #inventory-group", "success");
                $("#inventory").append("<option value=''></option>");
                list_inventory = data.data;
                data_inventory = data.data.price;
                $("#inventory").append("<option value='"+data.data.id_medicine+"' selected='selected'>"+data.data.name+"</option>");
                unit(data.data.id_medicine);
                $("#inventory").select2({
                    placeholder: "{{ trans('messages.choice_inventory') }}",
                    allowClear: false
                });
                return 0
            }else {
                loading_content("#form-add-price #inventory-group", "failed");
                $("#form-add-price #inventory-group #loading-content").click(function(){ listInventory(id); });
            }
        },
        error: function() {
            loading_content("#form-add-price #inventory-group", "failed");
            $("#form-add-price #inventory-group #loading-content").click(function(){ listInventory(id); });
        }
    });
}

function convertNumber (field) {
    if(event.which >= 37 && event.which <= 40) return
    $(field).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });
}


let timer = "";
$("input[name='inventory']").autocomplete({
    minLength: 3,
    source: function(request, response) {
        let temp = [];
        clearTimeout(timer);
        timer = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/medicine?has_price=false&q=" + request.term + "&per_page=10",
                beforeSend: function() {
                    $("#form-add-price #loading-inventory").removeClass("hide");
                },
                success: function(results) {
                    $("#form-add-price #loading-inventory").addClass("hide");
                    if (!results.error) {
                        for (i = 0; i < results.data.length; i++) {
                            let inventory = results.data[i];
                            temp.push({
                                id: inventory.id,
                                name: inventory.name,
                                value: inventory.name
                            });
                        }
                        response(temp);
                    }
                },
                error: function(xhr) {
                    $("#form-add-price #loading-inventory").addClass("hide");
                    response(temp);
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function(event, ui) {
        $("input[name='inventory']").val(ui.item.name);
        $("input[name='inventory']").data("id", ui.item.id);
        unit(ui.item.id)
        return false;
    }
});

function unit(id) {
    loading_content("#form-add-price #unit_add-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/" + id + "/unit",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                list_unit = data.data;
                loading_content("#form-add-price #unit_add-group", "success");
                $("#form-add-price select[name='unit_inventory[]']").append('<option value=""></option>');
                listPatientCategory_add();
                if( list_unit.length > 0 ) {
                    for (i = 0; i < list_unit.length; i++) {
                        $("#form-add-price select[name='unit_inventory[]']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>');
                    }
                    $("#form-add-price .btn-primary").attr("disabled", false);
                }else {
                    notif(false, 'Satuan inventori tidak tersedia, Silahkan tambah satuan inventori pada menu tambah inventori');
                    $("#form-add-price .btn-primary").attr("disabled", true);
                }
                $("#form-add-price select[name='unit_inventory[]']").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                    allowClear: false
                });
            }else {
                loading_content("#form-add-price #unit_add-group", "failed");
                $("#form-add-price #unit_add-group #loading-content").click(function(){ unit(id); });
            }
        },
        error : function() {
            loading_content("#form-add-price #unit_add-group", "failed");
            $("#form-add-price #unit_add-group #loading-content").click(function(){ unit(id); });
        }
    });
}

function listDataInventory () {
    if(data_inventory.length > 0) {
        for (i = 0; i < data_inventory.length; i++) {
            if(i > 0) {
                $("#form-add-price #section-price").append(getTemplatePrice(null));
            }
        }
    
        $("#form-add-price select[name^='patient_category']").html("<option value=''></option> ");
        $("#form-add-price select[name='unit_inventory[]']").html('');
        var index = [];
        $("#add_price").find(".field_price").each(function(key, value) {
            index.push(key)
            $(this).attr('id', function (index) {
                return "type-" + key;
            });

            if(key > 0) {
                $("#form-add-price #type-"+key+" #btn-remove-price").removeClass('hide')
            }

            if(data_inventory[key].type === 'margin') {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val(data_inventory[key].margin)
                $("#form-add-price #type-"+key+" select[name='type_text[]']").val('margin').trigger('change')
                $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().removeClass("hide");
            }else {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val(data_inventory[key].price);
                $("#form-add-price #type-"+key+" select[name='type_text[]']").val('flat').trigger('change')
                $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().addClass("hide");
            }
    
            if(data_inventory[key].type_margin === 'persentage') {
                $("#form-add-price #type-"+key+" select[name^='type_margin']").val('persentage').trigger('change')
            }else if(data_inventory[key].type_margin != null){
                $("#form-add-price #type-"+key+" select[name^='type_margin']").val('fix rate').trigger('change')
            }
    
            $("#form-add-price #type-"+key+" input[name^='margin_or_price']").each(function(key, value) {
                valuePriceAdd();
            });

            $("#form-add-price #type-"+key+" select[name='type_margin[]']").on('change', function(e) {
                if(this.value == "fix rate") {
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                }else {
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
                }
                $("#form-add-price #type-"+key+" input[name='margin_or_price[]']").val("");
                valuePriceAdd();
            });
    
            $("#form-add-price #type-"+key+" select[name='type_text[]']").on('change', function(e) {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val("");
                $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().removeClass("hide");
                if(this.value == 'flat'){
                    $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().addClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Harga Jual';
                }else{
                    $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().removeClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Margin Harga Jual';
                    if($("#form-add-price #type-"+key+" select[name^='type_margin'] option:selected").val() == 'persentage') {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
                    }else {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    }
                }
                valuePriceAdd();
            });
            
        });
    
        $("select[name='unit_inventory[]']").each(function(index, unit){
            for (i = 0; i < list_unit.length; i++) {
                var selected = "";
                if(data_inventory[index].unit.id === list_unit[i].id) {
                    selected = "selected = 'selected'";
                }
                $(unit).append("<option value='"+list_unit[i].id+"' "+selected+">"+list_unit[i].name+"</option>")
            }
        });
    
        $("select[name='patient_category[]']").each(function(key, value) {
            for(i = 0; i < data_category.length; i++) {
                var selected = "";
                if(data_category[i].id === data_inventory[key].patient_category.id) {
                    selected = "selected='selected'"
                }
                $(value).append('<option value='+data_category[i].id+' '+selected+'>'+data_category[i].name+'</option>')
            }
        });
    
        $("#form-add-price select[name^='type_text']").select2({
            placeholder: "{{ trans('messages.choice_price_calculation') }}",
            allowClear: false
        });
    
        $("#form-add-price select[name^='type_margin']").select2({
            placeholder: "{{ trans('messages.choice_type_margin') }}",
            allowClear: false
        });
    
        $("#form-add-price select[name^='unit_inventory']").select2({
            placeholder: "{{ trans('messages.choice_unit') }}",
            allowClear: false
        });
    
        $("#form-add-price select[name^='patient_category']").select2({
            placeholder: "{{ trans('messages.select_category') }}"
        });
    }else {
         $("#add_price").find(".field_price").each(function(key, val) {
            $(this).attr('id', function (index) {
                return "type-" + key;
            });

            $("#form-add-price #type-"+key+" select[name='type_margin[]']").on('change', function(e) {
                if(this.value == "fix rate") {
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                }else {
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
                }
                $("#form-add-price #type-"+key+" input[name='margin_or_price[]']").val("");
                valuePriceAdd();
            });
            
            $("select[name='type_text[]']").on('change', function(e) {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val("");
                if(e.target.value == 'flat'){
                    $("#form-add-price #type-"+key+" select[name='type_margin[]']").parent().addClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Harga Jual';

                }else{
                    $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().removeClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Margin Harga Jual';
                    if($("#form-add-price #type-"+key+" select[name^='type_margin'] option:selected").val() == 'persentage') {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
                    }else {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    }
                }
            });
        });
    }
}

var checked_persentage = false;
function valuePriceAdd(){
    checked_persentage = false;
    $("#form-add-price").find(".field_price").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        var type = $("#form-add-price #type-"+key+" select[name^='type_text'] option:selected").val();
        var type_margin = $("#form-add-price #type-"+key+" select[name^='type_margin'] option:selected").val();
        var value = $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val();
        var form_input = $("#form-add-price #type-"+key+" input[name^='margin_or_price']");
        if(type != 'flat'){
            if(type_margin == 'persentage'){
                if(value > 100){
                    formValidate(true, ['type-'+key+' #price',"Persentase tidak boleh lebih dari 100%", true]);
                    checked_persentage = true;
                }else{
                    formValidate(false, ['type-'+key+' #price',""]);
                    checked_persentage = false;
                    convertNumber(form_input);
                }
            }else{
                formValidate(false, ['type-'+key+' #price',""]);
                convertNumber(form_input);
            }
        }else{
            formValidate(false, ['type-'+key+' #price', '']);
            convertNumber(form_input);
        }
    });
}

function addPrice_add() { 
    $("#form-add-price #section-price").append(getTemplatePrice());
    $("#form-add-price select[name^='type_text']").select2({
        placeholder: "{{ trans('messages.choice_price_calculation') }}",
        allowClear: false
    });

    $("#form-add-price select[name^='type_margin']").select2({
        placeholder: "{{ trans('messages.choice_type_margin') }}",
        allowClear: false
    });

    var index = []
    $("#add_price").find(".field_price").each(function(key, value) {
        index.push(key)
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        if(key > 0) {
            $("#form-add-price #type-"+key+" #btn-remove-price").removeClass('hide')
        }

        $("#form-add-price #type-"+key+" select[name='type_margin[]']").on('change', function(e) {
            if(this.value == "fix rate") {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
            }else {
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
            }
            $("#form-add-price #type-"+key+" input[name='margin_or_price[]']").val("");
            valuePriceAdd();
        });
        $("#form-add-price #type-"+key+" select[name='type_text[]']").on('change', function(e) {
            $("#form-add-price #type-"+key+" input[name^='margin_or_price']").val("");
                if(e.target.value == 'flat'){
                    $("#form-add-price #type-"+key+" select[name='type_margin[]']").parent().addClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Harga Jual';

                }else{
                    $("#form-add-price #type-"+key+" select[name^='type_margin']").parent().removeClass("hide");
                    $("#form-add-price #type-"+key+" input[name^='margin_or_price']").parent().parent().parent()[0].firstElementChild.innerHTML = 'Margin Harga Jual';
                    if($("#form-add-price #type-"+key+" select[name^='type_margin'] option:selected").val() == 'persentage') {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan persentase")
                    }else {
                        $("#form-add-price #type-"+key+" input[name^='margin_or_price']").attr("placeholder", "Masukkan harga jual")
                    }
                }
            valuePriceAdd();
        });
    });

    if(index[index.length-1] != 0) {
        for(i = 0; i < list_unit.length; i++) {
            $("#form-add-price #type-"+index[index.length-1]+" select[name='unit_inventory[]']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>')
        }

        for(i = 0; i < data_category.length; i++) {
            if (data_category[i].default == 1) {
                $("#form-add-price #type-"+index[index.length-1]+" select[name^='patient_category']").append("<option value='"+data_category[i].id+"' selected>"+data_category[i].name+"</option>")
            }else {
                $("#form-add-price #type-"+index[index.length-1]+" select[name^='patient_category']").append("<option value='"+data_category[i].id+"'>"+data_category[i].name+"</option>")
            }
        }
    }

    $("#form-add-price select[name^='unit_inventory']").select2({
        placeholder: "{{ trans('messages.choice_unit') }}",
        allowClear: false
    });

    $("#form-add-price select[name^='patient_category']").select2({
        placeholder: "{{ trans('messages.select_category') }}"
    });
}

function removePrice_add() {
    $("#add_price").find(".field_price").each(function(key, value) {
        $(this).attr('id', function (index) {
            return "type-" + key;
        });

        if(key === 0) {
            $("#form-add-price #type-"+key+" #btn-remove-price").addClass('hide')
        }

        $("#form-add-price #type-"+key+" select[name='type_margin[]']").on('change', function(e) {
            valuePriceAdd();
        });
        $("#form-add-price #type-"+key+" select[name='type_text[]']").on('change', function(e) {
            if(this.value == 'flat'){
                $("#form-add-price #type-"+key+" select[name^='type_margin']").each(function(key, value) {
                    $(this).parent().parent().addClass("hide");
                });
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").each(function(key, value) {
                    $(this).attr("placeholder", "Masukkan harga jual");
                    $(this).parent().parent().parent()[0].firstElementChild.innerHTML = 'Harga Jual';
                });
            }else{
                $("#form-add-price #type-"+key+" select[name^='type_margin']").each(function(key, value) {
                    $(this).parent().parent().removeClass("hide");
                });
                $("#form-add-price #type-"+key+" input[name^='margin_or_price']").each(function(key, value) {
                    $(this).attr("placeholder", "Masukkan presentase");
                    $(this).parent().parent().parent()[0].firstElementChild.innerHTML = 'Margin Harga Jual';
                });
            }
            valuePriceAdd();
        });
    });
}

function listPatientCategory_add() {    
    loading_content("#form-add-price #patient_category-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                loading_content("#form-add-price #patient_category-group", "success");
                var item = data.data;
                data_category = item;
                    $("#form-add-price select[name^='patient_category']").html("<option value=''></option> ");
                    for (var i = 0; i < item.length; i++) {
                        if(item[i].default == 1) {
                            $("#form-add-price select[name^='patient_category']").append("<option value='"+item[i].id+"' selected>"+item[i].name+"</option>");
                        }else{
                            $("#form-add-price select[name^='patient_category']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                        }
                    }
                    listDataInventory(data_inventory)
                $("#form-add-price select[name^='patient_category']").select2({
                    placeholder: "{{ trans('messages.select_category') }}",
                    allowClear: false
                });
            } else {
                loading_content("#form-add-price #patient_category-group", "failed");
                $("#form-add-price #patient_category-group #loading-content").click(function(){ listPatientCategory_add(); });
            }
        },
        error: function(){
            loading_content("#form-add-price #patient_category-group", "failed");
            $("#form-add-price #patient_category-group #loading-content").click(function(){ listPatientCategory_add(); });
        }
    })
}

@if(Session::get('pcare'))
$("#form-add-price input[name='code_dpho']").autocomplete({
    minLength: 0,
    source: function( request, response ) {
        var r = [];
        if(request.term != ''){
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/bpjs/list-obat/"+request.term+"/0/5",
                beforeSend: function(){
                    $("#form-add-price #loading-code_dpho").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-add-price #loading-code_dpho").addClass("hide");
                    
                    var itemSearch = dataSearch.list;

                    for (var i = 0; i < itemSearch.length; i++) {
                        r.push({code:itemSearch[i].kdObat,value:itemSearch[i].nmObat,stok:itemSearch[i].sedia});
                    }

                    response(r);
                },
                error: function(){
                }
            });
        }
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.code;
        return false;
    }
});
@endif

function resetAddPrice() {
    $("#inventory").html('');
    $("#form-add-price .btn-primary").attr("disabled", false);
}


$('#add_price').on('show.bs.modal', function (e) {
    resetAddPrice();
    $("#form-add-price input[name='inventory']").val("")
    $("#form-add-price #section-price").html(getTemplatePrice()); 
    id_price = $(e.relatedTarget).attr('data-id');
    let data_name = $(e.relatedTarget).attr('data-name');

    if ( id_price != undefined && data_name != undefined) {
        listInventory(id_price)
        $("#form-add-price input[name='inventory']").val(data_name)
        $("#form-add-price input[name='inventory']").data("id", id_price)
        $("#form-add-price input[name='inventory']").attr('disabled', true)
    }

    $("#form-add-price select[name='inventory']").select2({
        placeholder: "{{ trans('messages.choice_inventory') }}"
    });

    $("#form-add-price select[name='unit_inventory[]']").select2({
        placeholder: "{{ trans('messages.choice_unit') }}"
    });

    $("#form-add-price select[name='type_text[]']").select2({
        placeholder: "{{ trans('messages.choice_price_calculation') }}"
    });
    

    $("#form-add-price select[name='type_margin[]']").select2({
        placeholder: "{{ trans('messages.choice_type_margin') }}"
    });

    $("#form-add-price select[name='patient_category[]']").select2({
        placeholder: "{{ trans('messages.select_category') }}"
    });
});

$("#form-add-price").submit(function(event) {
    resetValidation('form-add-price #code', 'form-add-price #name');

    var id_medicine = $("#form-add-price input[name=inventory]").data("id");
    var unit = [];
    var patient_category = [];
    var type_text = [];
    var type_margin = [];
    var price = [];

    var checked_unit = true;
    var checked_margin_or_price = true;
    var checked_same_name = true;

    $("#form-add-price select[name='unit_inventory[]']").each(function(key, value) {
        if($(value).val() === null || $(value).val() === "") {
            checked_unit = false
        }else {
            unit[key] = $(value).val();
        }
    });

    $("#form-add-price input[name='margin_or_price[]']").each(function(key, value) {
        if($(value).val() === "") {
            checked_margin_or_price = false;
        }else {
            price[key] = $(value).val().replace(/\D/g, "");
        }
    });

    $("#form-add-price select[name='patient_category[]']").each(function(key, value) {
        patient_category[key] = $(value).val();
    });
    
    for (i = 0; i < patient_category.length - 1; i++) {
        if (patient_category[i + 1] === patient_category[i] && unit[i + 1] === unit[i]) {
            checked_same_name = false;
        }
    }

    $("#form-add-price select[name^='type_margin']").each(function(key, value) {
        type_margin[key] = $(value).val();
    });

    $("#form-add-price select[name^='type_text']").each(function(key, value) {
        if( $(value).val() === 'flat' ) {
            type_margin[key] = ""
        }

        type_text[key] = $(value).val();
    });

    if (!validatePrice(id_medicine, checked_unit, checked_margin_or_price, checked_same_name)) return;

    formData= new FormData();

    formData.append('id_medicine', id_medicine);

    for (i = 0; i < patient_category.length; i++) {
        formData.append('medicine_price['+i+'][id_medicine]', id_medicine)
        formData.append('medicine_price['+i+'][id_patient_category]', patient_category[i])
        formData.append('medicine_price['+i+'][id_unit]', unit[i])
        formData.append('medicine_price['+i+'][price]', price[i])
        formData.append('medicine_price['+i+'][type]', type_text[i])
        formData.append('medicine_price['+i+'][type_margin]', type_margin[i])
    }
    formData.append('_method', 'PATCH')
    if(!checked_persentage) {
        $("#form-add-price button").attr("disabled", true);
        $("#form-add-price .btn-primary").addClass("loading");
        $("#form-add-price .btn-primary span").removeClass("hide");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/medicine-price",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $("#form-add-price button").attr("disabled", false);
                $("#form-add-price .btn-primary").removeClass("loading");
                $("#form-add-price .btn-primary span").addClass("hide");

                if ( !data.error ) {
                    if ( !data.success ) {
                        formValidate(true, ['form-add-price #code',data.errors.code, true], ['form-add-price #name',data.errors.name, true]);
                        if(data.message){
                            notif(false,data.message);
                        }
                    } else {
                        $("#add_price").modal("toggle");
                        if ( id_price != undefined ) {
                            getListInventory();
                        }else {
                            search(numPage, "false", keySearch);
                        }
                        notif(true,"{{ trans('validation.success_add_medicine') }}");
                        priceItem_add = 0;
                        if ( setup == "medicine" ) {
                            setup = "";
                            $('#previous-step').removeClass('hide');
                            $('#step_1').addClass('hide');
                            $('#step_2').addClass('hide');
                            $('#step_3').addClass('hide');
                            $('#step_4').addClass('hide');
                            $('#step_5').addClass('hide');
                            $('#step_1').addClass('done');
                            $('#step_6').removeClass('hide');
                            $('#step_6').removeClass('disable');
                            $('#next-step').addClass('hide');
                            $('#step-progress').addClass('hide');
                            $('header').removeClass('header-step');
                            $('#main-content').removeClass('main-step');
                            $("#tutorial .widget-controls span").removeClass("hide");
                            $("#text-tutorial").html('<?php echo trans("tutorial.tutorial_success"); ?>');
                            setup_clinic('');
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-add-price button").attr("disabled", false);
                $("#form-add-price .btn-primary").removeClass("loading");
                $("#form-add-price .btn-primary span").addClass("hide");
                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
});

function validatePrice(id_medicine, checked_unit, checked_margin_or_price, checked_same_name) {
	!id_medicine && formValidate(false,['inventory', '{{ trans("validation.empty_name_inventory") }}', true]);
    !checked_unit && notif(false,"{{ trans('validation.empty_unit') }}");
    !checked_margin_or_price && notif(false,"{{ trans('validation.empty_qty_price') }}");
    !checked_same_name && notif(false,"{{ trans('validation.error_patient_category_name') }}");
	return id_medicine && checked_unit && checked_margin_or_price && checked_same_name
}

</script>