<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_medicine') }}</h5>
            </div>
            <form id="form-add" class="">
                <div class="modal-body alert-notif">
                    @if(Session::get('pcare'))
                    <div class="form-group" id="code-group">
                        <label class="control-label">Kode DPHO</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_name') }}" name="code_dpho">
                        <span id="loading-code_dpho" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    @endif
                    
                    <div class="form-group" id="category-group">
                        <label class="control-label">{{ trans('messages.category_inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <select id="category" name="category[]" multiple="multiple"></select>
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>                            
                    </div>

                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.inventory_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_name') }}" name="name">
                        <span id="loading-name" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="code_inventory-group">
                        <label class="control-label">{{ trans('messages.medicine_code') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_code') }}" name="code">
                        <span class="help-block"></span>
                    </div>                    
                    <div class="form-group" id="company-group">
                        <label class="control-label">{{ trans('messages.medicine_company') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_company') }}" name="company">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="notes-group">
                        <label class="control-label">{{ trans('messages.record_of_medicine_interactions') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.add_record_of_medicine_interactions') }}" name="notes">
                        <span class="help-block"></span>
                    </div>
                    <div class="rasio-title">
                        <h5>{{ trans('messages.unit_ratio') }}</h5>
                    </div>
                    <div id="section-price">
                        <div class="field_price">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-6" id="unit-group">
                                        <label class="control-label">{{ trans('messages.unit') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="unit[]">
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="control-label">{{ trans('messages.qty') }}</label>
                                        <div class="border-group">
                                            <input class="form-control" placeholder="{{ trans('messages.qty') }}" type="number" name="amount[]" value="1" disabled />
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <label class="control-label">Setiap inventori minimal memiliki 1 satuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('messages.information') }}</label>
                        <div id="information">
                            <ul></ul>
                        </div>
                    </div>

                    <div class="form-group" id="button">
                        <a class="btn-add" onclick="add()">+ {{ trans('messages.add_unit') }}</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" id="cancel-btn">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

var list_unit = [];
var title = [];
var qty = [];
var info = "";


// $("#form-add select[name='category[]']").select2({
//     placeholder: "{{ trans('messages.choice_inventory_category') }}",
//     allowClear: false
// });

$("#form-add select[name^='unit']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}",
    allowClear: false
});

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
    listCategory();
    listUnitClinic();
});

function listCategory() {
    loading_content("#form-add #category-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/category-inventory",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (data) {
            if ( !data.error ) {
                loading_content("#form-add #category-group", "success");
                $("#category").html("<option value=''></option>")
                var item = data.data
                if(item.length > 0) {
                    for (i = 0; i < item.length; i++) {    
                        $("#category").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }
                }
                $("#category").select2({
                    placeholder: "{{ trans('messages.choice_inventory_category') }}",
                    allowClear: false
                });
            }else {
                loading_content("#form-add #category-group", "failed");
                $("#form-add #category-group #loading-content").click(function(){ listCategory(); });
            }
        },
        error: function() {
            loading_content("#form-add #category-group", "failed");
            $("#form-add #category-group #loading-content").click(function(){ listCategory(); });
        }
    });
}

function listUnitClinic() {    
    loading_content("#form-add #unit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/units",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                loading_content("#form-add #unit-group", "success");
                var item = data.data;
                list_unit = data.data;
                $("#form-add select[name='unit[]']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='unit[]']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }
                $("#form-add select[name='unit[]']").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                    allowClear: true
                });
            } else {
                loading_content("#form-add #unit-group", "failed");
                $("#form-add #unit-group #loading-content").click(function(){ listUnitClinic(); });
            }
        },
        error: function(){
            loading_content("#form-add #unit-group", "failed");
            $("#form-add #unit-group #loading-content").click(function(){ listUnitClinic(); });
        }
    });
}
let timer = ""
$("#form-add input[name='name']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        var r = [];
        clearTimeout(timer);
        timer = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/default-medicine?q="+request.term+"&per_page=5",
                beforeSend: function(){
                    $("#form-add #loading-name").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-add #loading-name").addClass("hide");
                    var itemSearch = dataSearch.data;
                    for (var i = 0; i < itemSearch.length; i++) {
                        r.push({id:itemSearch[i].id,code:itemSearch[i].code,value:itemSearch[i].name,company:itemSearch[i].company});
                    }
                    response(r);
                },
                error: function(){
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.value;
        $("#form-add input[name='code']").val(ui.item.code);
        $("#form-add input[name='company']").val(ui.item.company);
        return false;
    }
});

@if(Session::get('pcare'))
$("#form-add input[name='code_dpho']").autocomplete({
    minLength: 0,
    source: function( request, response ) {
        var r = [];
        if(request.term != ''){
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/bpjs/list-obat/"+request.term+"/0/5",
                beforeSend: function(){
                    $("#form-add #loading-code_dpho").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-add #loading-code_dpho").addClass("hide");
                    
                    var itemSearch = dataSearch.list;

                    for (var i = 0; i < itemSearch.length; i++) {
                        r.push({code:itemSearch[i].kdObat,value:itemSearch[i].nmObat,stok:itemSearch[i].sedia});
                    }
                    response(r);
                }
            })
        }
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.code;
        return false;
    }
});
@endif

function resetAdd() {
    resetValidation('form-add #code', 'form-add #name', 'form-add unit', 'form-add #category', 'form-add #code_inventory', 'form-add #company');

    $("#form-add input[name=code]").val("");
    $("#form-add input[name=name]").val("");
    $("#form-add input[name=company]").val("");
    $("#form-add input[name=notes]").val("");
    $("#category").html("");

    @if(Session::get('pcare'))
        $("#form-add input[name=code_dpho]").val("");
    @endif

    $("#form-add #section-price").html('<div class="field_price">'+
            '<div class="form-group">'+
                '<div class="row">'+
                    '<div class="col-xs-6" id="unit-group">'+
                        '<label class="control-label">{{ trans("messages.unit") }}</label>'+
                        '<div class="border-group">'+
                            '<select class="form-control" name="unit[]"></select>'+
                        '</div>'+
                        '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="col-xs-6">'+
                        '<label class="control-label">{{ trans("messages.qty") }}</label>'+
                        '<div class="border-group">'+
                            '<input class="form-control" name="amount[]" type="number" placeholder="{{ trans("messages.qty") }}" value="1" disabled />'+
                        '</div>'+
                        '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                '</div>'+
                '<label class="control-label">Setiap inventori minimal memiliki 1 satuan</label>' +
            '</div>'+
        '</div>');

        $("#information ul").html('')
        $("#form-add select[name^='unit']").select2({
            placeholder: "{{ trans('messages.choice_unit') }}",
            allowClear: false
        });

        $("#form-add select[name='category[]']").select2({
            placeholder: "{{ trans('messages.choice_inventory_category') }}",
            allowClear: false
        });
}

function add() {
    var template = '<div class="field_price">' + 
                        '<div class="form-list">' + 
                            '<ul>' + 
                                '<li>' + 
                                    '<div class="form-group">'+
                                        '<div class="row">'+
                                            '<div class="col-xs-6" id="unit-group">'+
                                                '<label class="control-label">{{ trans("messages.unit") }}</label>'+
                                                '<div class="border-group">'+
                                                    '<select class="form-control" name="unit[]">' + '<option value=""></option>' + '</select>'+
                                                '</div>'+
                                                '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                                                '<span class="help-block"></span>'+
                                            '</div>'+
                                            '<div class="col-xs-4">'+
                                                '<label class="control-label">{{ trans("messages.qty") }}</label>'+
                                                '<div class="border-group">'+
                                                    '<input class="form-control" name="amount[]" type="number" placeholder="{{ trans("messages.qty") }}" min="1" />'+
                                                '</div>'+
                                                '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                                                '<span class="help-block"></span>'+
                                            '</div>'+
                                            '<div class="col-xs-2">' +
                                                '<a class="btn-remove" onclick="remove($(this).parents());">' + '<img src="{{ asset("assets/images/icons/action/delete.png") }}" />' + '</a>' + 
                                            '</div>' + 
                                        '</div>' + 
                                    '</div>' + 
                                '</li>' + 
                            '</ul>' + 
                        '</div>' + 
                    '</div>'

    $("#form-add #section-price").append(template);
    var index = new Array();
    $("#add").find(".field_price").each(function(key, value) {
        index.push(key)
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        $("#form-add #type-"+key+" select[name='unit[]']").on('change', function(e) {
            setChangeNotes()
        });

        $("#form-add #type-"+key+" input[name='amount[]']").on('input', function(e){
            setChangeNotes();
        });
    });

    if(index[index.length-1] != 0) {
        for(i = 0; i < list_unit.length; i++) {
            $("#form-add #type-"+index[index.length-1]+" select[name='unit[]']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>')
        }
    }

    $("#form-add select[name='unit[]']").select2({
        placeholder: "{{ trans('messages.choice_unit') }}",
        allowClear: true
    });

}

function setChangeNotes() {
    $("#information ul").html('');

    $("#add").find(".field_price").each(function(key, value) {
        var id = $(value).attr('id').replace( /^\D+/g, '');
        var data_title = $("#type-"+id+"").find('option:selected').text();
        var data_qty = $("#type-"+id+"").find('input').val();
        
        if (data_qty === "") {
            data_qty = 0
        }else if(data_title === "") {
            data_title = '-'
        }
        title[key] = data_title;
        
        if(key != 0) {
            qty[key] = data_qty
            info = "<li id='notes-"+id+"' class='notes-list'>"+key+".  Setiap "+title[key-1]+" terdiri dari<p class='qty-list'>"+qty[key]+"</p>"+title[key]+"</li>";
            $("#information ul").append(info)
        }
    });
}

function remove(event) {
    event[6].remove()
    var id = event[6].id.replace( /^\D+/g, '');
    
    title.splice(id, 1)
    qty.splice(id, 1)

    setChangeNotes()
}

$("#cancel-btn").click(function() {
    $("#add").modal("toggle");
    resetAdd();
})

$('select[name="unit[]"]').on('change', function() {
    resetValidation('form-add', 'unit')
});

$('#form-add input[name=company]').on('input', function() {
    resetValidation('form-edit', 'company');
});

$('#form-add input[name=code]').focus(function() {
    resetValidation('form-add #code_inventory');
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$("select[name='category[]']").on('change', function(){
    resetValidation('form-add #category');
});

$("#form-add").submit(function(event){
    event.preventDefault();
    $("#form-add button").attr("disabled", true);

    var category = $("select[name^='category'] option:selected").val();
    var name_inventory = $("input[name='name']").val();
    var code_inventory = $("input[name='code']").val();
    var company = $("input[name='company']").val();
    var notes = $("input[name='notes']").val();
    var code_dpho = "";
    var data_unit = [];
    var data_qty = [];
    var unit = true;
    var qty = true;
    var unit_same = true;
    var amount_qty = true;

    $("select[name='unit[]']").each(function(key, value) {
        if($(value).val() === null || $(value).val() === "") {
            unit = false
        }else {
            data_unit[key] = $(value).val();
        }
    });

    $("input[name='amount[]']").each(function(key, value) {
        if($(value).val() === "" || $(value).val() === null) {
            qty = false
        }else if($(value).val() < 1){
            amount_qty = false
        }else {
            data_qty[key] = $(value).val();
        }
    });

    var checked_unit = data_unit.slice().sort();
    for (var i = 0; i < data_unit.length - 1; i++) {
        if (checked_unit[i + 1] == checked_unit[i]) {
            unit_same = false;
        }
    }
    
    if (!validate(category, name_inventory, code_inventory, company, unit, qty, unit_same, amount_qty)) return;
    
    @if(Session::get('pcare'))
        code_dpho = $("#form-add input[name=code_dpho]").val();
    @endif

    formData = new FormData();
    formData.append('id_clinic', '{{$id_clinic}}');
    formData.append("kode_dpho", code_dpho);

    $("select[name^='category'] :selected").each(function (i, selected) {
        formData.append('category['+i+']', $(selected).val())
    })

    formData.append('code', code_inventory);
    formData.append('name', name_inventory);
    formData.append('company', company);
    formData.append('notes', notes);

    for (i = 0; i < data_unit.length; i++) {
        formData.append('unit['+i+'][id]', data_unit[i])
        formData.append('unit['+i+'][weight]', i)
        formData.append('unit['+i+'][amount_ratio]', data_qty[i])
    }

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-add button").attr("disabled", false);
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");
            
            if( !data.error ) {
                if(!data.success) {
                    notif(false, data.errors.message);
                }
                resetAdd();
                $("#add").modal("toggle");
                search(numPage, "false", keySearch);
                notif(true,"{{ trans('validation.success_add_medicine') }}");
                if ( setup == "medicine" ) {
                    setup = "";
                    $('#previous-step').removeClass('hide');
                    $('#step_1').addClass('hide');
                    $('#step_2').addClass('hide');
                    $('#step_3').addClass('hide');
                    $('#step_4').addClass('hide');
                    $('#step_5').addClass('hide');
                    $('#step_1').addClass('done');
                    $('#step_6').removeClass('hide');
                    $('#step_6').removeClass('disable');
                    $('#next-step').addClass('hide');
                    $('#step-progress').addClass('hide');
                    $('header').removeClass('header-step');
                    $('#main-content').removeClass('main-step');
                    $("#tutorial .widget-controls span").removeClass("hide");
                    $("#text-tutorial").html('<?php echo trans("tutorial.tutorial_success"); ?>');
                    setup_clinic('');
                }  
            }else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});


function validate(category, name_inventory, code_inventory, company, unit, qty, unit_same, amount_qty) {
	!category && formValidate(false,['category', '{{ trans("validation.empty_category") }}', true]);
	!name_inventory && formValidate(false,['name', '{{ trans("validation.empty_name_inventory") }}', true]);
	!code_inventory && formValidate(false,['code_inventory', '{{ trans("validation.empty_code_inventory") }}', true]);
    !company && formValidate(false,['company', '{{ trans("validation.empty_company_inventory") }}', true]);
    !unit &&  notif(false,"{{ trans('validation.empty_unit') }}");
    !qty && notif(false, '{{ trans("validation.empty_unit_amount") }}');
    !unit_same && notif(false, '{{ trans("validation.error_unit_name") }}');
    !amount_qty && notif(false, '{{ trans("validation.min_amount_unit")}}');
    $("#form-add button").attr("disabled", false);
	return category && name_inventory && code_inventory && company && unit && qty && unit_same && amount_qty
}

</script>
