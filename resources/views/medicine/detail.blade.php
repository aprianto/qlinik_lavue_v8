<div class="modal fade window window-main window-detail" id="detail" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body hide" id="loading-detail">
                <div class="cell">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="reload-detail">
                <div class="cell">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="section-detail">
                <div class="detail-item detail-window">
                    <div class="detail">
                        <h5 id="code"></h5>
                        <h3 id="name"></h3>
                        <div class="bottom">
                            <span id="company"></span>
                        </div>
                    </div>
                    <div class="detail detail-color">
                        <h5>Harga</h5>
                        <div  id="info-item">
                        </div>
                    </div>
                    <div class="detail detail-color">
                        <div  id="info-other-item">
                        </div>
                    </div>
                    <div class="detail-button">
                        <a data-dismiss="modal" title="" class="btn-main">{{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

$('#detail').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).attr('data-id');
    detail(id);

    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
});

function detail(id) {
    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');

                $("#detail #name").html(data.name);
                $("#detail #code").html(data.code);
                $("#detail #company").html(data.company);

                $("#detail #info-item").html("");

                itemPrice = data.price_category;
                for(var i=0; i<itemPrice.length; i++) {
                    try {
                        var style_price = "";
                        if(i==itemPrice.length-1) {
                            style_price = "last-item";
                        }

                        $("#detail #info-item").append('<span class="price-item '+style_price+'">'+
                                    '<u>'+itemPrice[i].patient_category.name+'</u>'+
                                    '<ins>Rp '+itemPrice[i].price+'</ins>'+                                
                                '</span>');
                    } catch(err) {}
                }

                $("#detail #info-other-item").html('<span class="price-item last-item"><u>{{ trans("messages.stock") }}</u><ins>'+data.stock+'</ins></span>');
                
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

</script>