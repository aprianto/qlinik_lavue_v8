<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.edit_medicine') }}</h5>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id" />
                    @if(Session::get('pcare'))
                    <div class="form-group" id="code-group">
                        <label class="control-label">Kode DPHO</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_name') }}" name="code_dpho">
                        <span id="loading-code_dpho" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    @endif
                    
                    <div class="form-group" id="category_edit-group">
                        <label class="control-label">{{ trans('messages.category_inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <select id="category_edit" name="category_edit[]" multiple="multiple"></select>
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>                            
                    </div>

                    <div class="form-group" id="name_edit-group">
                        <label class="control-label">{{ trans('messages.inventory_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_name') }}" name="name_edit" disabled>
                        <span id="loading-name" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="code_inventory_edit-group">
                        <label class="control-label">{{ trans('messages.medicine_code') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_medicine_code') }}" name="code_edit" disabled>
                        <span class="help-block"></span>
                    </div>                    
                    <div class="form-group" id="company_edit-group">
                        <label class="control-label">{{ trans('messages.medicine_company') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_company') }}" name="company_edit" disabled>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="notes_edit-group">
                        <label class="control-label">{{ trans('messages.record_of_medicine_interactions') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.add_record_of_medicine_interactions') }}" name="notes_edit">
                        <span class="help-block"></span>
                    </div>
                    <div class="rasio-title">
                        <h5>{{ trans('messages.unit_ratio') }}</h5>
                    </div>
                    <div id="section-price-edit">
                        <div class="field_price-edit">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6" id="unit_edit-group">
                                        <label class="control-label">{{ trans('messages.unit') }}</label>
                                        <div class="border-group">
                                            <select class="form-control" name="unit_edit[]">
                                            </select>
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">{{ trans('messages.qty') }}</label>
                                        <div class="border-group">
                                            <input class="form-control" placeholder="{{ trans('messages.qty') }}" type="number" name="amount_edit[]" min="1" max="1" value="1" disabled />
                                        </div>
                                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <label class="control-label">Setiap inventori minimal memiliki 1 satuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{{ trans('messages.information') }}</label><br/>
                        <div id="information_edit">
                            <ul></ul>
                        </div>
                    </div>

                    <div class="form-group hide" id="button">
                        <a class="btn-add" onclick="addUnit()">+ {{ trans('messages.add_unit') }}</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit" id="save-btn-edit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" id="cancel-btn-edit">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
var list_unit = [];
var title_edit = [];
var qty_edit = [];
var info = "";
var list_category = [];
var id_inventory = ""

var template_edit = '<div class="field_price-edit">' + 
                    '<div class="form-list">' + 
                        '<ul>' + 
                            '<li>' + 
                                '<div class="form-group">'+
                                    '<div class="row">'+
                                        '<div class="col-md-6" id="unit_edit-group">'+
                                            '<label class="control-label">{{ trans("messages.unit") }}</label>'+
                                            '<div class="border-group">'+
                                                '<select class="form-control" name="unit_edit[]">' + '<option value=""></option>' + '</select>'+
                                            '</div>'+
                                            '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                                            '<span class="help-block"></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<label class="control-label">{{ trans("messages.qty") }}</label>'+
                                            '<div class="border-group">'+
                                                '<input class="form-control" name="amount_edit[]" type="number" placeholder="{{ trans("messages.qty") }}" min="1" disabled/>'+
                                            '</div>'+
                                            '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                                            '<span class="help-block"></span>'+
                                        '</div>'+
                                        '<div class="col-md-2 hide">' +
                                            '<a class="btn-remove" onclick="removeUnit($(this).parents());">' + '<img src="{{ asset("assets/images/icons/action/delete.png") }}" />' + '</a>' + 
                                        '</div>' + 
                                    '</div>' + 
                                '</div>' + 
                            '</li>' + 
                        '</ul>' + 
                    '</div>' + 
                '</div>'

$('#edit').on('show.bs.modal', function (e) {
    resetEdit()
    id_inventory = $(e.relatedTarget).attr('data-id');
    edit(id_inventory);
});

$("#form-edit select[name='category_edit[]']").select2({
    placeholder: "{{ trans('messages.choice_inventory_category') }}",
});

$("#form-edit select[name='unit_edit[]']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}",
});


function listCategoryEdit(val) {
    loading_content("#form-edit #category_edit-group", "loading");
    $("#category_edit-group .border-group select").html('');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/category-inventory",
        type: "GET",
        processData: false,
        contentType: false,
        success: function (data) {
            if ( !data.error ) {
                loading_content("#form-edit #category_edit-group", "success");

                var item = data.data;

                for (i = 0; i < item.length; i++) {
                    var selected = "";
                    for (j = 0; j < val.length; j++) {
                        if(item[i].id === val[j]) {
                            selected = "selected='selected'"
                        }
                    }
                    $("#category_edit").append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>");
                }
                $("#category_edit").select2({
                    placeholder: "{{ trans('messages.choice_inventory_category') }}",
                });
            }else {
                loading_content("#form-edit #category_edit-group", "failed");
                $("#form-edit #category_edit-group #loading-content").click(function(){ listCategoryEdit(val); });
            }
        },
        error: function() {
            loading_content("#form-edit #category_edit-group", "failed");
            $("#form-edit #category_edit-group #loading-content").click(function(){ listCategoryEdit(val); });
        }
    });
}

function listUnitClinicEdit(val) {    
    loading_content("#form-edit #unit_edit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/units",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                loading_content("#form-edit #unit_edit-group", "success");
                var item = data.data;
                list_unit = data.data;
                $("#form-edit select[name='unit_edit[]']").html("<option value=''></option> ");
                listEditUnit(item, val)
                $("#form-edit select[name^='unit_edit']").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                });
                $("#form-edit select[name^='unit_edit']").prop('disabled', true)
            } else {
                loading_content("#form-edit #unit_edit-group", "failed");
                $("#form-edit #unit_edit-group #loading-content").click(function(){ listUnitClinicEdit(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #unit_edit-group", "failed");
            $("#form-edit #unit_edit-group #loading-content").click(function(){ listUnitClinicEdit(val); });
        }
    });
}

function listEditUnit(item, val) {
    for (i = 0; i < val.length; i++) {
        if(i > 0) {
            $("#form-edit #section-price-edit").append(template_edit);
        }
        $("#edit").find(".field_price-edit").each(function(key, value) {
            $(this).attr('id', function (index) {
                return "type-" + key;
            });
        });
    }

    $("select[name='unit_edit[]']").each(function(index, unit){
        for(i = 0; i < item.length; i++) {
            var selected = "";
            if(val.length > 0 && val[index].id === item[i].id) {
                selected = "selected = 'selected'";
            }
            $(unit).append("<option value='"+item[i].id+"' "+selected+">"+item[i].name+"</option>")
        }

        $(unit).on('change', function(e) {
            setChangeNotesEdit()
        });

    });

    $("input[name='amount_edit[]']").each(function(index, amount){
        if(val.length > 0) {
            $(amount).val(val[index].amount_ratio)
        }
        $(amount).on('input', function(e) {
            setChangeNotesEdit();
        })
    });

    setChangeNotesEdit()
}

let timer_medicine_edit = "";
$("#form-edit input[name='name_edit']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        var r = [];
        clearTimeout(timer_medicine_edit);
        timer_medicine_edit = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/default-medicine?q="+request.term+"&per_page=5",
                beforeSend: function(){
                    $("#form-edit #loading-name").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-edit #loading-name").addClass("hide");
                    var itemSearch = dataSearch.data;
                    for (var i = 0; i < itemSearch.length; i++) {
                        r.push({id:itemSearch[i].id,code:itemSearch[i].code,value:itemSearch[i].name,company:itemSearch[i].company});
                    }
                    response(r);
                },
                error: function(){
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.value;
        $("#form-edit input[name='code_edit']").val(ui.item.code);
        $("#form-edit input[name='company_edit']").val(ui.item.company);
        return false;
    }
});

@if(Session::get('pcare'))
let timer_dpho = "";
$("#form-edit input[name='code_dpho']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        var r = [];
        clearTimeout(timer_dpho);
        timer_dpho = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/bpjs/list-obat/"+request.term+"/0/5",
                beforeSend: function(){
                    $("#form-edit #loading-code_dpho").removeClass("hide");
                },
                success: function(dataSearch){
                    $("#form-edit #loading-code_dpho").addClass("hide");
                    
                    var itemSearch = dataSearch.list;

                    for (var i = 0; i < itemSearch.length; i++) {
                        r.push({code:itemSearch[i].kdObat,value:itemSearch[i].nmObat,stok:itemSearch[i].sedia});
                    }
                    response(r);
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.code;
        return false;
    }
});
@endif

function resetEdit() {
    resetValidation('form-edit #code_edit', 'form-edit #name_edit', 'form-edit unit_edit', 'form-edit #category_edit', 'form-edit #code_inventory_edit', 'form-edit #company_edit');

    $("#form-edit input[name=code_edit]").val("");
    $("#form-edit input[name=name_edit]").val("");
    $("#form-edit input[name=company_edit]").val("");
    $("#form-edit input[name=notes_edit]").val("");
    $("#category_edit-group .border-group select").html('');

    @if(Session::get('pcare'))
        $("#form-edit input[name=code_dpho]").val("");
    @endif

    $("#form-edit #section-price-edit").html('<div class="field_price-edit">'+
            '<div class="form-group">'+
                '<div class="row">'+
                    '<div class="col-md-6" id="unit_edit-group">'+
                        '<label class="control-label">{{ trans("messages.unit") }}</label>'+
                        '<div class="border-group">'+
                            '<select class="form-control" name="unit_edit[]"></select>'+
                        '</div>'+
                        '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                        '<label class="control-label">{{ trans("messages.qty") }}</label>'+
                        '<div class="border-group">'+
                            '<input class="form-control" name="amount_edit[]" type="number" min="1" placeholder="{{ trans("messages.qty") }}" value="1" disabled />'+
                        '</div>'+
                        '<span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                '</div>'+
                '<label class="control-label">Setiap inventori minimal memiliki 1 satuan</label>' +
            '</div>'+
        '</div>');

        $("#information_edit ul").html('')
        
        $("input[name='amount_edit[]']").each(function(index, amount){
            $(amount).val("1");
        })
}

function addUnit() {
    $("#form-edit #section-price-edit").append(template_edit);
    var index = new Array();
    $("#edit").find(".field_price-edit").each(function(key, value) {
        index.push(key)
        $(this).attr('id', function (index) {
            return "type-" + key;
        });
        $("#form-edit #type-"+key+" select[name='unit_edit[]']").on('change', function(e) {
            setChangeNotesEdit()
        });

        $("#form-edit #type-"+key+" input[name='amount_edit[]']").on('input', function(e){
            setChangeNotesEdit();
        });
    });

    if(index[index.length-1] != 0) {
        for(i = 0; i < list_unit.length; i++) {
            $("#form-edit #type-"+index[index.length-1]+" select[name='unit_edit[]']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>')
        }
    }

    $("#form-edit select[name^='unit_edit']").select2({
        placeholder: "{{ trans('messages.choice_unit') }}",
        allowClear: true
    });
}

function setChangeNotesEdit() {
    $("#information_edit ul").html('');

    $("#edit").find(".field_price-edit").each(function(key, value) {
        var id = $(value).attr('id')
        if(id) {
            id = id.replace( /^\D+/g, '');
        }
        var data_title = $("#type-"+id+"").find('option:selected').text();
        var data_qty = $("#type-"+id+"").find('input').val();
        if (data_qty === "") {
            data_qty = 0
        }else if(data_title === "") {
            data_title = '-'
        }
        title_edit[key] = data_title;

        if ( key != 0 ) {
            qty_edit[key] = data_qty;
            info = "<li id='notes-"+id+"' class='notes-list'>"+key+".  Setiap "+title_edit[key-1]+" terdiri dari<p class='qty-list'>"+qty_edit[key]+"</p>"+title_edit[key]+"</li>";
            $("#information_edit ul").append(info);
        }
    });
}

function removeUnit(event) {
    event[6].remove();
    var id = event[6].id.replace( /^\D+/g, '');
    
    title_edit.splice(id, 1);
    qty_edit.splice(id, 1);

    setChangeNotesEdit();
}

$("#cancel-btn-edit").click(function() {
    $("#edit").modal("toggle");
    resetEdit();
})

$('select[name="unit_edit[]"]').on('change', function() {
    resetValidation('form-edit', 'unit_edit');
});

$('#form-edit input[name=company_edit]').on('input', function() {
    resetValidation('form-edit', 'company_edit');
});

$('#form-edit input[name=code_edit]').focus(function() {
    resetValidation('form-edit #code_inventory_edit');
});

$('#form-edit input[name=name_edit]').focus(function() {
    resetValidation('form-edit #name_edit');
});

$("select[name='category_edit[]']").on('change', function(){
    resetValidation('form-edit #category_edit');
});

$("#save-btn-edit").on('click', function(event){
    event.preventDefault();
    $("#form-edit button").attr("disabled", true);

    var data_category = $("select[name^='category_edit'] option:selected").val();
    var data_name_inventory = $("input[name='name_edit']").val();
    var data_code_inventory = $("input[name='code_edit']").val();
    var data_company = $("input[name='company_edit']").val();
    var data_notes = $("input[name='notes_edit']").val();
    var data_code_dpho = "";
    var data_unit_edit = [];
    var data_qty_edit = [];
    var data_edit = []
    var unit_edit = true;
    var checked_qty_edit = true;
    var unit_same_edit = true;
    var amount_qty_edit = true;

    $("select[name='unit_edit[]']").each(function(key, value) {
        if($(value).val() === null || $(value).val() === "") {
            unit_edit = false
        }else {
            data_unit_edit[key] = $(value).val();
        }
    });

    $("input[name='amount_edit[]']").each(function(key, value) {
        if($(value).val() === "" || $(value).val() === null) {
            checked_qty_edit = false;
        }else if($(value).val() < 1){
            amount_qty_edit = false;
        }else {
            data_qty_edit[key] = $(value).val();
        }
    });

    var checked_unit = data_unit_edit.slice().sort();
    for (var i = 0; i < data_unit_edit.length - 1; i++) {
        if (checked_unit[i + 1] == checked_unit[i]) {
            unit_same_edit = false;
        }
    }
    
    if (!validateEdit(data_category, data_name_inventory, data_code_inventory, data_company, unit_edit, checked_qty_edit, unit_same_edit, amount_qty_edit)) return;
    
    @if(Session::get('pcare'))
        data_code_dpho = $("#form-edit input[name=code_dpho]").val();
    @endif

    formData = new FormData();
    formData.append('_method', 'PATCH');
    formData.append('id_clinic', '{{$id_clinic}}');
    formData.append("kode_dpho", data_code_dpho);
    formData.append('code', data_code_inventory);
    formData.append('name', data_name_inventory);
    formData.append('company', data_company);
    formData.append('notes', data_notes);

    for (i = 0; i < data_unit_edit.length; i++) {
        formData.append('unit['+i+'][id]', data_unit_edit[i])
        formData.append('unit['+i+'][weight]', i)
        formData.append('unit['+i+'][amount_ratio]', data_qty_edit[i])
    }

    $("#category_edit :selected").each(function(index, categories) {
        formData.append('category['+index+']', $(categories).val())
    });

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/" + id_inventory,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-edit button").attr("disabled", false);
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");
            
            if( !data.error ) {
                if(!data.success) {
                    notif(false, data.errors.message);
                }
                resetEdit();
                $("#edit").modal("toggle");
                search(numPage, "false", keySearch);
                notif(true,"{{ trans('validation.success_edit_medicine') }}");
            }else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});

function validateEdit(data_category, data_name_inventory, data_code_inventory, data_company, unit_edit, checked_qty_edit, unit_same_edit, amount_qty_edit) {
	!data_category && formValidate(false,['category_edit', '{{ trans("validation.empty_category") }}', true]);
	!data_name_inventory && formValidate(false,['name_edit', '{{ trans("validation.empty_name_inventory") }}', true]);
	!data_code_inventory && formValidate(false,['code_inventory_edit', '{{ trans("validation.empty_code_inventory") }}', true]);
	!data_company && formValidate(false,['company_edit', '{{ trans("validation.empty_company_inventory") }}', true]);
    !unit_edit &&  notif(false,"{{ trans('validation.empty_unit') }}");
    !checked_qty_edit && notif(false, '{{ trans("validation.empty_unit_amount") }}');
    !unit_same_edit && notif(false, '{{ trans("validation.error_unit_name") }}');
    !amount_qty_edit && notif(false, '{{ trans("validation.min_amount_unit")}}');
    $("#form-edit button").attr("disabled", false);
	return data_category && data_name_inventory && data_code_inventory && data_company && unit_edit && checked_qty_edit && unit_same_edit && amount_qty_edit
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                $('#loading-edit').addClass('hide');
                $('#form-edit').removeClass('hide');

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=code_edit]").val(data.code);
                @if(Session::get('pcare'))
                $("#form-edit input[name=code_dpho]").val(data.kode_dpho);
                @endif
                $("#form-edit input[name=name_edit]").val(data.name);
                $("#form-edit input[name=company_edit]").val(data.company);
                $("#form-edit input[name=notes_edit]").val(data.notes);

                var item_category = [];
                for (i = 0; i < data.category.length; i++) {
                    item_category.push(data.category[i].id)
                }

                listCategoryEdit(item_category)
                listUnitClinicEdit(data.unit)
                
            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }
        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");
            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    });
}

</script>
