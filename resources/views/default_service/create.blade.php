<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_service') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">
                <div class="modal-body">
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.service_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.service_name') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="code-group">
                        <label class="control-label">{{ trans('messages.service_code') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.service_code') }}" name="code" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="polyclinic-group">
                        <label class="control-label">{{ trans('messages.polyclinic') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.polyclinic') }}" name="polyclinic" autocomplete="off">
                    </div>
                    <div class="form-group" id="service_category-group">
                        <label class="control-label">{{ trans('messages.service_category') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.service_category') }}" name="service_category" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

function resetAdd() {
    resetValidation('form-add #name','form-add #code');
    $("#form-add input[name=name]").val("");
    $("#form-add input[name=code]").val("");
    $("#form-add input[name=polyclinic]").val("");
    $("#form-add input[name=service_category]").val("");
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$('#form-add input[name=code]').focus(function() {
    resetValidation('form-add #code');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #name', 'form-add #code');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    formData.append("validate", false);    
    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("code", $("#form-add input[name=code]").val());
    formData.append("polyclinic", $("#form-add input[name=polyclinic]").val());
    formData.append("service_category", $("#form-add input[name=service_category]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");
    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/default-service",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.name, true], ['form-add #code',data.errors.code, true]);
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_add_service') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>