@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

@if($level=="admin")

<!--

<div class="heading-sec">
    <div class="row">
        <div class="col-md-4 column">
            <div class="heading-profile heading-home">
                <h2><?php echo trans('messages.welcome', ['name'=>$user->name]); ?></h2>
                <span>{{ trans('messages.info_dashboard') }}</span>
            </div>
        </div>
    </div>
</div>

-->

@endif



@if($level=="user" || $level=="doctor")

<!--
<div class="heading-sec">
    <div class="row">
        <div class="col-md-4 column">
            <div class="heading-profile heading-home">
                <h2><?php echo trans('messages.welcome', ['name'=>$user->name]); ?></h2>
                <span>{{ trans('messages.info_dashboard') }}</span>
            </div>
        </div>
    </div>
</div>
-->
    
<div class="panel-content hide" id="tutorial">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="welcome-bar">
                    <div id="text-tutorial"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-content hide" id="dashboard">
<div class="row">
        <div class="@if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true) col-md-6 @endif">
            <div class="mini-stats-sec">
                <div class="@if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true) row @endif">

                @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true || $role == 'doctor')
                    <div class="@if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true) col-md-6 @else col-md-4 dash-rs @endif">
                        <div class="widget widget-red" id="widget_schedule">
                            <div class="widget-controls">
                                <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="mini-stats">
                                <span class="icon-rs"><img src="{{ asset('assets/images/icons/sidebar/rawat-jalan.png') }}" /></span>
                                <p>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                            <i id="icon_schedule" class="fa fa-arrow-up up"></i> 
                                        </div>
                                        <div class="right-sec">
                                            <span class="text">{{ trans('messages.patient_outpatient_today') }}</span>
                                        </div>
                                    </div>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                        </div>
                                        <div class="right-sec">
                                            <h3>
                                                <span id="schedule">0</span> 
                                                <a href="#/{{ $lang }}/schedule/sick" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['doctor',''],['clinic',''],['status',''],['from_date_now','now'],['to_date_now','now'],['page','1']);"><img src="{{ asset('assets/images/icons/nav/chevron-rm.png') }}" /></a>
                                            </h3>
                                        </div>
                                    </div>
                                </p>                                
                            </div>
                        </div>                        
                    </div>
                    @endif

                    @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true || $role == 'doctor')
                    <div class="@if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true) col-md-6 @else col-md-4 dash-rs @endif">
                        <div class="widget widget-sky" id="widget_patient_medical">
                            <div class="widget-controls">
                                <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="mini-stats ">
                                <span class="icon-rs"><img src="{{ asset('assets/images/icons/sidebar/rekam-medis.png') }}" /></span>
                                <p>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                            <i id="icon_patient_medical" class="fa fa-arrow-up up"></i> 
                                        </div>
                                        <div class="right-sec">
                                            <span class="text">{{ trans('messages.patient_medical_today') }}</span>
                                        </div>
                                    </div>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                        </div>
                                        <div class="right-sec">
                                            <h3>
                                                <span id="patient_medical">0</span> 
                                                <a href="#/{{ $lang }}/medical" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['doctor',''],['from_date_now','now'],['to_date_now','now'],['page','1']);"><img src="{{ asset('assets/images/icons/nav/chevron-rm.png') }}" /></a>
                                            </h3>
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true || $role == 'doctor')
                    <div class="@if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true) col-md-6 @else col-md-4 dash-rs @endif">
                        <div class="widget widget-red" id="widget_schedule_unconfirmed">
                            <div class="widget-controls">
                                <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="mini-stats">
                                <span class="icon-rs"><img src="{{ asset('assets/images/icons/sidebar/rawat-jalan.png') }}" /></span>

                                <p>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                            <i id="icon_schedule_unconfirmed" class="fa fa-exchange none"></i> 
                                        </div>
                                        <div class="right-sec">
                                            <span class="text">{{ trans('messages.schedule_unconfirmed') }}</span>
                                        </div>
                                    </div>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                        </div>
                                        <div class="right-sec">
                                            <h3>
                                                <span id="schedule_unconfirmed">0</span> 
                                                <a href="#/{{ $lang }}/schedule/sick" onclick="loading($(this).attr('href'));repository(['search',''],['polyclinic',''],['doctor',''],['clinic',''],['status','1'],['from_date_now','now'],['to_date_now',''],['page','1']);"><img src="{{ asset('assets/images/icons/nav/chevron-rm.png') }}" /></a>
                                            </h3>
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true)
                    <div class="col-md-6">
                        <div class="widget widget-blue" id="widget_doctor_practice">
                            <div class="widget-controls">
                                <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="mini-stats">
                                <span class="icon-rs"><img src="{{ asset('assets/images/icons/sidebar/dokter.png') }}" /></span>
                                <p>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                            <i id="icon_doctor_practice" class="fa fa-arrow-down down"></i> 
                                        </div>
                                        <div class="right-sec">
                                            {{ trans('messages.doctor_practice_today') }}
                                        </div>
                                    </div>
                                    <div class="dashboard-sec">
                                        <div class="left-sec">
                                        </div>
                                        <div class="right-sec">
                                            <h3 id="doctor_practice">0</h3>
                                        </div>
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif

                    <!--<div class="col-md-6">
                        <div class="widget widget-pink" id="widget_doctor_leave">
                            <div class="widget-controls">
                                <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                            </div>
                            <div class="mini-stats ">
                                <span class="pink-skin"><i class="fa fa fa-user-times"></i></span>
                                <p><i class="fa  fa-arrow-up up"></i> {{ trans('messages.doctor_leave_today') }}</p>
                                <h3 id="doctor_leave">0</h3>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>

            @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true || $role == 'doctor')
            <div class="@if($role == 'doctor') col-md-8 dash-rs @endif">
                <div class="widget widget-purple" id="widget_patient" style="min-height: 461px">
                    <div class="widget-controls">
                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                    </div>
                    <div class="our-clients-sec">
                        <div class="widget-title">
                            <h3>{{ trans('messages.patient_outpatient') }}</h3>
                            <span>{{ trans('messages.info_patient_outpatient_today') }}</span>
                        </div>
                        <ul id="patient-list" class="client-list">
                            <li>
                                <img src='{{ asset("assets/images/icons/sidebar/pasien.png") }}' class="avatar-icon" />
                                <div class="client-info">
                                    <h3><a href="#" title="">{{ trans('messages.patient') }}</a></h3>
                                    <p>{{ trans('messages.schedule') }}</p>
                                </div>
                            </li>                        
                        </ul>
                        <div class="dashboard-btn-sec hide" id="link-all-patient">
                            <a href="#/{{ $lang }}/schedule/sick" class="button" onclick="loading($(this).attr('href'))">{{ trans('messages.view_all') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endif            

        </div>

        
        <div class="col-md-6">

            @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true)
            <div class="widget widget-purple" id="widget_patient_all_schedule_medical">
                <div class="widget-controls">
                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                </div>
                <div class="activity-sec">
                    <div class="widget-title">
                        <h3>{{ trans('messages.patient') }}</h3>
                        <span>{{ trans('messages.info_schedule_medical') }}</span>
                    </div>
                    <div id="graph-wrapper">
                        <div class="graph-info">
                            <a href="" class="visitors"><span class="red-skin"></span>{{ trans('messages.patient_outpatient') }}</a>
                            <a href="" class="returning"><span class="purple-skin"></span>{{ trans('messages.patient_medical') }}</a>
                        </div>
                        <div class="graph-container">
                            <canvas id="chart"></canvas>
                        </div>
                        <div class="graph-details">
                            <ul>
                                <li><span class="red-skin" id="total_type_schedule">0</span><strong id="total_today_schedule">0</strong><p>{{ trans('messages.total_week') }}: <span id="total_week_schedule">0</span></p></li>
                                <li><span class="purple-skin" id="total_type_medical">0</span><strong id="total_today_medical">0</strong><p>{{ trans('messages.total_week') }}: <span id="total_week_medical">0</span></p></li>
                            </ul>
                        </div>
                    </div>  
                </div>
            </div>
            @endif

            @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true)
            <div class="widget widget-green" id="widget_icd">
                <div class="widget-controls">
                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                </div>
                <div class="pie-chart-sec">
                    <div class="widget-title">
                        <h3>{{ trans('messages.icd') }}</h3>
                        <span>{{ trans('messages.info_most_icd_month') }}</span>
                    </div>
                    <div class="most-browsers">
                        <div class="wrapper-pie">
                            <div class="pieID pie">
                            </div>
                        </div>
                        <ul class="pieID legend" id="icd">
                          <li class="pieID-list">
                            <em>{{ trans('messages.icd') }}</em>
                            <span>1</span>
                          </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif

        </div>
        

    </div>
    <div class="row">

        @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'receptionist')==true)
        <div class="col-md-4">
            <div class="widget widget-yellow" id="widget_doctor">
                <div class="widget-controls">
                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                </div>
                <div class="our-clients-sec">
                    <div class="widget-title">
                        <h3>{{ trans('messages.doctor_practice') }}</h3>
                        <span>{{ trans('messages.info_doctor_practice_today') }}</span>
                    </div>
                    <ul id="doctor-list" class="client-list">
                        <li>
                            <img src='{{ asset("assets/images/icons/sidebar/dokter.png") }}' class="avatar-icon" />
                            <div class="client-info">
                                <h3><a href="#" title="">{{ trans('messages.doctor') }}</a></h3>
                                <p>{{ trans('messages.schedule') }}</p>
                            </div>
                        </li>                        
                    </ul>
                </div>
            </div>
        </div>
        @endif

        @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'cashier')==true || checkRole($role, 'owner')==true)
        <div class="col-md-4">
            <div class="widget widget-brown" id="widget_income">
                <div class="widget-controls">
                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                </div>
                <div class="product-stats">
                    <h3>{{ trans('messages.income_this_month') }}</h3>
                    <span>
                        <i>{{ trans('messages.total') }}</i> <br />
                        <span  id="income">{{ formatCurrency($lang,'Rp',0) }}</span>
                    </span>
                    <ul>
                        <li>
                            {{ trans('messages.from') }}
                            <span id="income_start_date"></span>
                        </li>
                        <li>
                            {{ trans('messages.until') }}
                            <span id="income_end_date"></span>
                        </li>
                    </ul>

                    @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true || checkRole($role, 'owner')==true)
                    <div class="dashboard-btn-sec">
                        <a href="#/{{ $lang }}/report/income" title="" onclick="loading($(this).attr('href'))">{{ trans('messages.view_report') }}</a>
                    </div>
                    @endif

                </div>
            </div>
        </div>
        @endif

        @if(checkRole($role, 'superuser')==true || checkRole($role, 'user')==true)
        <div class="col-md-4">
            <div class="widget widget-purple" id="widget_service">
                <div class="widget-controls">
                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                </div>
                <div class="task-managment">
                    <div class="widget-title">
                        <h3>{{ trans('messages.service') }}</h3>
                        <span>{{ trans('messages.info_most_service_today') }}</span>
                    </div>
                    <ol id ="listitems">
                        {{ trans("messages.no_result") }}
                    </ol>
                </div>

            </div>
        </div>
        @endif

    </div>
    <div class="row">

        <div class="col-md-8"></div>
        

    </div>
</div>

@endif
@extends('layouts.footer')

<script type="text/javascript">

@if($level=="user" || $level=="doctor")

if("{{Request::is('*home*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#link-dashboard").addClass('active-bar')
}else {
    $(".side-menus ul li a").removeClass('active-bar')
}

var date = new Date();
var monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
var dateNow = (date.getDate()+100).toString().slice(-2);
var yearNow = date.getFullYear();

var getDateNow = yearNow+"-"+monthNow+"-"+dateNow;
var getDateNowFormat = yearNow+"/"+monthNow+"/"+dateNow;

var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var dateFirst = (firstDay.getDate()+100).toString().slice(-2);
var dateLast = (lastDay.getDate()+100).toString().slice(-2);
var getDateFirst = yearNow+"-"+monthNow+"-"+dateFirst;
var getDateLast = yearNow+"-"+monthNow+"-"+dateLast;


@if($level == 'doctor')
    clinic_Schedule();
    clinic_ScheduleUnconfirmed();
    clinic_patientMedical();
    clinic_patient();
@else
    clinic_doctorPractice();
    //clinic_doctorLeave();
    clinic_patientAllScheduleMedical();
    clinic_Schedule();
    clinic_ScheduleUnconfirmed();
    clinic_patientMedical();
    clinic_patient()
    clinic_income();
    clinic_service();
    clinic_icd();
    clinic_doctor();
@endif



if(setup == "" || setup == null) {
    if("{{$level == 'doctor'}}") {
        $("#dashboard").removeClass("hide");
        $("#tutorial").addClass("hide");
    }
    $("#dashboard").removeClass("hide");
    $("#tutorial").addClass("hide");
} else {
    $("#dashboard").addClass("hide");
    $("#tutorial").removeClass("hide");

    if(setup == "doctor") {
        $("#text-tutorial").html('<?php echo trans("tutorial.dashboard_add_doctor", ["link"=>"#/".$lang."/doctor/create"]); ?>');
        $("#tutorial .close-content").attr("onclick","setup_clinic('schedule');");
    } else if(setup=="schedule") {
        $("#text-tutorial").html('<?php echo trans("tutorial.dashboard_schedule_doctor", ["link"=>"#/".$lang."/doctor/schedule"]); ?>');
        $("#tutorial .close-content").attr("onclick","setup_clinic('service');");
    } else if(setup=="service") {
        $("#text-tutorial").html('<?php echo trans("tutorial.dashboard_add_service", ["link"=>"#/".$lang."/service/create"]); ?>');
        $("#tutorial .close-content").attr("onclick","setup_clinic('commission');");
    } else if(setup=="commission") {
        $("#text-tutorial").html('<?php echo trans("tutorial.dashboard_add_commission", ["link"=>"#/".$lang."/commission/create"]); ?>');
        $("#tutorial .close-content").attr("onclick","setup_clinic('medicine');");
    } else if(setup=="medicine") {
        $("#text-tutorial").html('<?php echo trans("tutorial.dashboard_add_medicine", ["link"=>"#/".$lang."/medicine"]); ?>');
        $("#tutorial .close-content").attr("onclick","setup_clinic('');");

        $("#dashboard").removeClass("hide");
        $("#tutorial").addClass("hide");

    }
}

function clinic_Schedule() { 
    $("#widget_schedule .refresh-content").click(function(){ clinic_Schedule(); });
    $("#widget_schedule").addClass("loading-wait");
    $("#widget_schedule .mini-stats").removeClass("widget-transparent");
    queryParam = '';
    if ("{{$level == 'doctor'}}") {
        queryParam = "?id_clinic={{$id_clinic}}"
    }
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/schedule" + queryParam,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $("#widget_schedule").removeClass("loading-wait");

                if(getZero(data[0].schedule_before)>getZero(data[0].schedule_now)) {
                    $("#widget_schedule .mini-stats #icon_schedule").attr("class","fa fa-arrow-down down");
                } else if(getZero(data[0].schedule_before)==getZero(data[0].schedule_now)) {
                    $("#widget_schedule .mini-stats #icon_schedule").attr("class","fa fa-exchange none");
                } else {
                    $("#widget_schedule .mini-stats #icon_schedule").attr("class","fa fa-arrow-up up");
                }

                $("#schedule").html(getZero(data[0].schedule_now));
            } else {
                $("#widget_schedule .mini-stats").addClass("widget-transparent");
                $("#widget_schedule").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_schedule .mini-stats").addClass("widget-transparent");
            $("#widget_schedule").removeClass("loading-wait");
        }
    });
}


function clinic_ScheduleUnconfirmed() {
    $("#widget_schedule_unconfirmed .refresh-content").click(function(){ clinic_ScheduleUnconfirmed(); });
    $("#widget_schedule_unconfirmed").addClass("loading-wait");
    $("#widget_schedule_unconfirmed .mini-stats").removeClass("widget-transparent");
    queryParam = '';
    if ("{{$level == 'doctor'}}") {
        queryParam = "?id_clinic={{$id_clinic}}"
    }
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/schedule-unconfirmed" + queryParam,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                $("#widget_schedule_unconfirmed").removeClass("loading-wait");

                $("#schedule_unconfirmed").html(getZero(data[0].total));
            } else {
                $("#widget_schedule_unconfirmed .mini-stats").addClass("widget-transparent");
                $("#widget_schedule_unconfirmed").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_schedule_unconfirmed .mini-stats").addClass("widget-transparent");
            $("#widget_schedule_unconfirmed").removeClass("loading-wait");
        }
    });
}

function clinic_patientMedical() {
    $("#widget_patient_medical .refresh-content").click(function(){ clinic_patientMedical(); });
    $("#widget_patient_medical").addClass("loading-wait");
    $("#widget_patient_medical .mini-stats").removeClass("widget-transparent");
    queryParam = '';
    if ("{{$level == 'doctor'}}") {
        queryParam = "?id_clinic={{$id_clinic}}"
    }
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/patient-medical-records" + queryParam,
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_patient_medical").removeClass("loading-wait");

                if(getZero(data[0].patient_medical_records_before)>getZero(data[0].patient_medical_records_now)) {
                    $("#widget_patient_medical .mini-stats #icon_patient_medical").attr("class","fa fa-arrow-down down");
                } else if(getZero(data[0].patient_medical_records_before)==getZero(data[0].patient_medical_records_now)) {
                    $("#widget_patient_medical .mini-stats #icon_patient_medical").attr("class","fa fa-exchange none");
                } else {
                    $("#widget_patient_medical .mini-stats #icon_patient_medical").attr("class","fa fa-arrow-up up");
                }

                $("#patient_medical").html(getZero(data[0].patient_medical_records_now));
            } else {
                $("#widget_patient_medical .mini-stats").addClass("widget-transparent");
                $("#widget_patient_medical").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_patient_medical .mini-stats").addClass("widget-transparent");
            $("#widget_patient_medical").removeClass("loading-wait");
        }
    });
}

function clinic_doctorPractice() {
    $("#widget_doctor_practice .refresh-content").click(function(){ clinic_doctorPractice(); });
    $("#widget_doctor_practice").addClass("loading-wait");
    $("#widget_doctor_practice .mini-stats").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/doctor-practice",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_doctor_practice").removeClass("loading-wait");

                if(getZero(data[0].doctor_practice_before)>getZero(data[0].doctor_practice_now)) {
                    $("#widget_doctor_practice .mini-stats #icon_doctor_practice").attr("class","fa fa-arrow-down down");
                } else if(getZero(data[0].doctor_practice_before)==getZero(data[0].doctor_practice_now)) {
                    $("#widget_doctor_practice .mini-stats #icon_doctor_practice").attr("class","fa fa-exchange none");
                } else {
                    $("#widget_doctor_practice .mini-stats #icon_doctor_practice").attr("class","fa fa-arrow-up up");
                }

                $("#doctor_practice").html(getZero(data[0].doctor_practice_now));
            } else {
                $("#widget_doctor_practice .mini-stats").addClass("widget-transparent");
                $("#widget_doctor_practice").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_doctor_practice .mini-stats").addClass("widget-transparent");
            $("#widget_doctor_practice").removeClass("loading-wait");
        }
    });
}

function clinic_doctorLeave() {
    $("#widget_doctor_leave .refresh-content").click(function(){ clinic_doctorLeave(); });
    $("#widget_doctor_leave").addClass("loading-wait");
    $("#widget_doctor_leave .mini-stats").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/doctor-leave",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_doctor_leave").removeClass("loading-wait");

                if(getZero(data[0].doctor_leave_before)>getZero(data[0].doctor_leave_now)) {
                    $("#widget_doctor_leave .mini-stats p i").attr("class","fa fa-arrow-down down");
                } else if(getZero(data[0].doctor_leave_before)==getZero(data[0].doctor_leave_now)) {
                    $("#widget_doctor_leave .mini-stats p i").attr("class","fa fa-exchange none");
                } else {
                    $("#widget_doctor_leave .mini-stats p i").attr("class","fa fa-arrow-up up");
                }

                $("#doctor_leave").html(getZero(data[0].doctor_leave_now));
            } else {
                $("#widget_doctor_leave .mini-stats").addClass("widget-transparent");
                $("#widget_doctor_leave").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_doctor_leave .mini-stats").addClass("widget-transparent");
            $("#widget_doctor_leave").removeClass("loading-wait");
        }
    });
}

function clinic_patientAllScheduleMedical() {
    $("#widget_patient_all_schedule_medical .refresh-content").click(function(){ clinic_patientAllScheduleMedical(); });
    $("#widget_patient_all_schedule_medical").addClass("loading-wait");
    $("#widget_patient_all_schedule_medical .activity-sec").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/patient-schedule-medical-records",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_patient_all_schedule_medical").removeClass("loading-wait");

                var checkDate = [];
                var dateChart = [];
                var Schedule = [];
                var patientMedical = [];

                var totalWeekSchedule = 0;
                var totalWeekMedical = 0;

                for(var i=0; i<data.length; i++) {
                    checkDate.push(data[i].date);
                    dateChart.push(getDateMonth(data[i].date));
                    Schedule.push(getZero(data[i].patient_schedule));
                    patientMedical.push(getZero(data[i].patient_medical));

                    totalWeekSchedule = totalWeekSchedule+getZero(data[i].patient_schedule);
                    totalWeekMedical = totalWeekMedical+getZero(data[i].patient_medical);
                }

                if(checkDate.indexOf(getDateNow)==-1) {
                    checkDate.push(getDateNow);
                    dateChart.push(getDateMonth(getDateNow));
                    Schedule.push(0);
                    patientMedical.push(0);
                }

                var minDate = "";
                for(var i=0; i<checkDate.length; i++) {
                    if(i==0) {
                        minDate = checkDate[i].replace(/\-/g,'/');
                    }

                    if(Date.parse(minDate)>Date.parse(checkDate[i].replace(/\-/g,'/'))) {
                        minDate = checkDate[i].replace(/\-/g,'/');
                    }
                }


                if(dateChart.length<7) {
                    var numLoopDate = 7-(dateChart.length);
                    for(i=1;i<=numLoopDate;i++) {

                        var dateBefore = new Date(minDate);
                        dateBefore.setDate(dateBefore.getDate() - i);

                        var monthNowBefore = ((dateBefore.getMonth()+1)+100).toString().slice(-2);
                        var dateNowBefore = (dateBefore.getDate()+100).toString().slice(-2);
                        var yearNowBefore = dateBefore.getFullYear();

                        var getDateBefore = yearNowBefore+"-"+monthNowBefore+"-"+dateNowBefore;  

                        dateChart.unshift(getDateMonth(getDateBefore));
                        Schedule.unshift(0);
                        patientMedical.unshift(0); 
                    }
                }

                $("#total_week_schedule").html(totalWeekSchedule);
                $("#total_week_medical").html(totalWeekMedical);

                if(Schedule[Schedule.length-1]<Schedule[Schedule.length-2]) {
                    var total = parseInt(Schedule[Schedule.length-2])-parseInt(Schedule[Schedule.length-1]);
                    $("#total_type_schedule").html("-"+total);
                } else if(Schedule[Schedule.length-1]>Schedule[Schedule.length-2]) {
                    var total = parseInt(Schedule[Schedule.length-1])-parseInt(Schedule[Schedule.length-2]);
                    $("#total_type_schedule").html("+"+total);
                } else {
                    $("#total_type_schedule").html("0");
                }

                if(patientMedical[patientMedical.length-1]<patientMedical[patientMedical.length-2]) {
                    var total = parseInt(patientMedical[patientMedical.length-2])-parseInt(patientMedical[patientMedical.length-1]);
                    $("#total_type_medical").html("-"+total);
                } else if(patientMedical[patientMedical.length-1]>patientMedical[patientMedical.length-2]) {
                    var total = parseInt(patientMedical[patientMedical.length-1])-parseInt(patientMedical[patientMedical.length-2]);
                    $("#total_type_medical").html("+"+total);
                } else {
                    $("#total_type_medical").html("0");
                }

                $("#total_today_schedule").html(Schedule[Schedule.length-1]);
                $("#total_today_medical").html(patientMedical[patientMedical.length-1]);
                



                function updateChartData(object, dataChart) {
                    for (var i = 0; i < dataChart.length; i++) {
                        object[i].value = dataChart[i];            
                    }
                };

                var data = {
                    labels: dateChart,
                    datasets: [
                        {
                            label: "{{ trans('messages.patient_outpatient') }}",
                            fillColor: "rgba(36,226,207,0.2)",
                            strokeColor: "rgba(36,226,207,1)",
                            pointColor: "rgba(36,226,207,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(36,226,207,1)",
                            data: Schedule
                        },
                        {
                            label: "{{ trans('messages.patient_medical') }}",
                            fillColor: "rgba(26,177,229,0.2)",
                            strokeColor: "rgba(26,177,229,1)",
                            pointColor: "rgba(26,177,229,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,177,229,1)",
                            data: patientMedical
                        }
                    ]
                };

                

                var ctx = document.getElementById("chart").getContext("2d");
                var myLineChart = new Chart(ctx).Line(data, {
                    multiTooltipTemplate: "<%= value %>",
                    legendTemplate: '',
                    scaleShowLabels: false,
                  scaleShowGridLines : true,
                pointDotRadius : 6,

                    pointDotStrokeWidth : 2,

                    pointHitDetectionRadius : 20,
                    scaleGridLineColor : "rgba(0,0,0,.0)",


                    scaleShowHorizontalLines: true,

                    scaleShowVerticalLines: true,
                });
                

            } else {
                $("#widget_patient_all_schedule_medical .activity-sec").addClass("widget-transparent");
                $("#widget_patient_all_schedule_medical").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_patient_all_schedule_medical .activity-sec").addClass("widget-transparent");
            $("#widget_patient_all_schedule_medical").removeClass("loading-wait");
        }
    });
}

function clinic_income() {
    $("#widget_income .refresh-content").click(function(){ clinic_income(); });
    $("#widget_income").addClass("loading-wait");
    $("#widget_income .product-stats").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/income",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_income").removeClass("loading-wait");

                $("#income").html(formatCurrency('{{ $lang }}','Rp',getZero(data[0].income)));
                $("#income_start_date").html(getFormatDate(getDateFirst));
                $("#income_end_date").html(getFormatDate(getDateLast));

            } else {
                $("#widget_income .product-stats").addClass("widget-transparent");
                $("#widget_income").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_income .product-stats").addClass("widget-transparent");
            $("#widget_income").removeClass("loading-wait");
        }
    });
}

function clinic_service() {
    $("#widget_service .refresh-content").click(function(){ clinic_service(); });
    $("#widget_service").addClass("loading-wait");
    $("#widget_service .task-managment").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/service",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_service").removeClass("loading-wait");

                coloArray = ["red-skin","sky-skin","purple-skin","pink-skin"];

                $("#listitems").html("");
                if(data.length==0) {
                    $("#listitems").html('<div class="no-data">{{ trans("messages.no_service") }}</div>');   
                }

                for(var i=0;i<data.length;i++) {
                    var color = coloArray[Math.floor(Math.random() * coloArray.length)];
                    $("#listitems").append('<li><i class="'+color+'"></i>'+data[i].service+'</li>');
                }

                

            } else {
                $("#widget_service .task-managment").addClass("widget-transparent");
                $("#widget_service").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_service .task-managment").addClass("widget-transparent");
            $("#widget_service").removeClass("loading-wait");
        }
    });
}

function clinic_icd() {
    $("#widget_icd .refresh-content").click(function(){ clinic_icd(); });
    $("#widget_icd").addClass("loading-wait");
    $("#widget_icd .pie-chart-sec").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/icd",
        type: "GET",
        success: function(data){
            if(!data.error) {
                $("#widget_icd").removeClass("loading-wait");

                $("#icd").html("");
                if(data.length==0) {
                    $("#icd").append('<li><em>{{ trans("messages.no_icd") }}</em><span>1</span></li>');
                }

                for(var i=0;i<data.length;i++) {
                    $("#icd").append('<li><em>'+data[i].indonesian_name+'</em><span>'+data[i].total+'</span></li>');
                }

                createPie(".pieID.legend", ".pieID.pie");
                

            } else {
                $("#widget_icd .pie-chart-sec").addClass("widget-transparent");
                $("#widget_icd").removeClass("loading-wait");
            }
        },
        error: function() {
            $("#widget_icd .pie-chart-sec").addClass("widget-transparent");
            $("#widget_icd").removeClass("loading-wait");
        }
    });
}

function clinic_doctor() {
    $("#widget_doctor .refresh-content").click(function(){ clinic_doctor(); });
    $("#widget_doctor").addClass("loading-wait");
    $("#widget_doctor .our-clients-sec").removeClass("widget-transparent");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/doctor",
        type: "GET",
        success: function(data){
            $("#widget_doctor").removeClass("loading-wait");

            coloArray = ["red-skin","sky-skin","purple-skin","pink-skin"];
            $("#doctor-list").html("");



            if(data.length==0) {
               $("#doctor-list").html('<li class="no-data no-border" style="margin-top: 50px;">{{ trans("messages.no_doctor_practice") }}</li>');
            }

            for(var i=0;i<data.length;i++) {


                if(i>=5) {
                    break;
                }

                var schedule = "";
                for(var j=0;j<data[i].schedule.length;j++) {
                    schedule = schedule+""+convertTime(data[i].schedule[j].start_time)+"-"+convertTime(data[i].schedule[j].end_time);
                    if(j!=data[i].schedule.length) {
                        schedule = schedule+"<br />";
                    }
                }

                var color = coloArray[Math.floor(Math.random() * coloArray.length)];

                var char_doctor = data[i].doctor.toString().substring(0,1);



                $("#doctor-list").append('<li>'+
                    '<img src=\'{{ asset("assets/images/icons/sidebar/dokter.png") }}\' class="avatar-icon" />'+
                    '<div class="client-info">'+
                        '<h3><a href="#/{{ $lang }}/doctor/'+data[i].doctor.id+'" onclick=\'loading($(this).attr("href"));\'>'+data[i].doctor.name+'</a></h3>'+
                        '<p>'+schedule+'</p>'+
                    '</div>'+
                '</li>');
            }
        },
        error: function() {
            $("#widget_doctor .our-clients-sec").addClass("widget-transparent");
            $("#widget_doctor").removeClass("loading-wait");
        }
    });
}

function clinic_patient() {
    $("#widget_patient .refresh-content").click(function(){ clinic_patient(); });
    $("#widget_patient").addClass("loading-wait");
    $("#widget_patient .our-clients-sec").removeClass("widget-transparent");
    queryParam = '';
    if ("{{$level == 'doctor'}}") {
        queryParam = "?id_clinic={{$id_clinic}}"
    }
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/dashboard/patient-schedule" + queryParam,
        type: "GET",
        success: function(data){
            $("#widget_patient").removeClass("loading-wait");

            coloArray = ["red-skin","sky-skin","purple-skin","pink-skin"];
            $("#patient-list").html("");

            if(data.length==0) {
               $("#patient-list").html('<li class="no-data no-border" style="margin-top: 100px;">{{ trans("messages.no_patient_outpatient") }}</li>');
            }

            for(i=0;i<data.length;i++) {
                if(i>=5) {
                    break;
                }
                
                var schedule = "";
                if(data[i].appointment==null) {
                    schedule = convertTime(data[i].start_time)+" - "+convertTime(data[i].end_time)+" • {{ trans('messages.number') }}: "+data[i].number;
                } else {
                    schedule = convertTime(data[i].start_time);
                }

                var color = coloArray[Math.floor(Math.random() * coloArray.length)];

                var char_patient = data[i].patient.toString().substring(0,1);

                $("#patient-list").append('<li>'+
                    '<img src=\'{{ asset("assets/images/icons/sidebar/pasien.png") }}\' class="avatar-icon" />'+
                    '<div class="client-info">'+
                        '<h3><a href="#/{{ $lang }}/patient/'+data[i].patient.id+'" onclick=\'loading($(this).attr("href"));\'>'+data[i].patient.name+'</a></h3>'+
                        '<p>'+schedule+'</p>'+
                        '<p>'+data[i].doctor.name+'</p>'+
                        '<p>'+data[i].polyclinic.name+'</p>'+
                    '</div>'+
                '</li>');
            }

            if(data.length>5) {
                $("#link-all-patient").removeClass("hide");
            }

        },
        error: function() {
            $("#widget_patient .our-clients-sec").addClass("widget-transparent");
            $("#widget_patient").removeClass("loading-wait");
        }
    });
}

@endif



function sliceSize(dataNum, dataTotal) {
  return (dataNum / dataTotal) * 360;
}
function addSlice(sliceSize, pieElement, offset, sliceID, color) {
  $(pieElement).append("<div class='slice "+sliceID+"'><span></span></div>");
  var offset = offset - 1;
  var sizeRotation = -158 + sliceSize;
  $("."+sliceID).css({
    "transform": "rotate("+offset+"deg) translate3d(0,0,0)"
  });
  $("."+sliceID+" span").css({
    "transform"       : "rotate("+sizeRotation+"deg) translate3d(0,0,0)",
    "background-color": color
  });
}
function iterateSlices(sliceSize, pieElement, offset, dataCount, sliceCount, color) {
  var sliceID = "s"+dataCount+"-"+sliceCount;
  var maxSize = 158;
  if(sliceSize<=maxSize) {
    addSlice(sliceSize, pieElement, offset, sliceID, color);
  } else {
    addSlice(maxSize, pieElement, offset, sliceID, color);
    iterateSlices(sliceSize-maxSize, pieElement, offset+maxSize, dataCount, sliceCount+1, color);
  }
}

function createPie(dataElement, pieElement) {
  var listData = [];
  $(dataElement+" span").each(function() {
    listData.push(Number($(this).html()));
  });
  var listTotal = 0;
  for(var i=0; i<listData.length; i++) {
    listTotal += listData[i];
  }
  var offset = 0;
  var color = [
    "#24e2cf", 
    "#1ab1e5", 
    "#046e89", 
    "#db0062", 
    "#ffa332", 
  ];
  for(var i=0; i<listData.length; i++) {
    var size = sliceSize(listData[i], listTotal);
    iterateSlices(size, pieElement, offset, i, 0, color[i]);
    $(dataElement+" li:nth-child("+(i+1)+")").css("border-color", color[i]);
    offset += size;
  }
}
createPie(".pieID.legend", ".pieID.pie");

$(function(){
    $('#doctor-list').slimScroll({
        height: '228px',
        wheelStep: 10,
        size: '2px'
    });
});

$(function(){
    $('#patient-list').slimScroll({
        height: '332px',
        wheelStep: 10,
        size: '2px'
    });
});
</script>
