@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div>

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a>
                    <h3 class="right">
                        {{ trans('messages.preliminary_examination') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-add">
                        <input type="hidden" name="id" />
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.patient') }}</label>
                            <div class="border-group">
                                <input name="name" placeholder="{{ trans('messages.select_patient') }}" type="text" class="form-control" />
                            </div>
                            <span id="loading-name" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="complaint-group">
                            <label class="control-label">{{ trans('messages.complaints')}}<span style="margin-left: 2px; color: var(--error);"></span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_complaint') }}" name="complaint" id="complaint" />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" style="margin-top: 24px;">
                            <label class="control-label">{{ trans('messages.history_of_illness') }}</label>
                            <div class="form-group" id="riwayat-group">
                                <div class="border-group">
                                    <select id="riwayat" name="riwayat[]" multiple="multiple"></select>
                                </div>
                                <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                            <button type="button" id="icon-add" class="button-add" data-toggle="modal" data-target="#add-history">
                                <span class="fa fa-plus"></span>
                                {{ trans('messages.add_history_of_illness') }}
                            </button>
                        </div>
                        <div class="form-group" style="margin-top: 24px;">
                            <label class="control-label">{{ trans('messages.history_of_allergy') }}</label>
                            <div class="form-group" id="riwayat-alergi-group" style="margin-bottom: 0px;">
                                <div class="border-group">
                                    <select id="riwayat-alergi" name="riwayat-alergi[]" multiple="multiple"></select>
                                </div>
                                <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                            <button type="button" id="icon-add-allergy" class="button-add" data-toggle="modal" data-target="#add-history-allergy">
                                <span class="fa fa-plus"></span>
                                {{ trans('messages.add_history_of_allergy') }}
                            </button>
                        </div>
                        <div class="form-group" style="margin-top: 24px;">
                            <label class="control-label">{{ trans('messages.history_of_vaccination') }}</label>
                            <div class="panel-vaccine-history panel-vital">
                                <div class="form-gorup table-reponsive" id="list-vaccine-history">
                                    <table class="table table-hove mini-table">
                                        <thead>
                                            <tr id="title-vaccine-history">
                                                <th>{{trans('messages.vaccine_name')}}</th>
                                                <th>{{trans('messages.vaccine_given_date')}}</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="data-vaccine-history">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="group-label first-label">{{ trans('messages.vital_sign') }}</h3>
                        <div class="form-group" id="body_temperature-group">
                            <label class="control-label">{{ trans('messages.body_temperature') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_body_temperature') }}" name="body_temperature" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="sistole-group">
                                    <label class="control-label">Sistole (mmHg)</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_blood_pressure') }}" name="blood_sistole" onkeydown="number(event)" autocomplete="off">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="diastole-group">
                                    <label class="control-label">Diastole (mmHg)</label>
                                    <input type="text" class="form-control" placeholder="Masukkan tekanan darah" name="blood_diastole" onkeydown="number(event)" autocomplete="off">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="pulse-group">
                            <label class="control-label">{{ trans('messages.pulse') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_pulse') }}" name="pulse" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="respiratory_frequency-group">
                            <label class="control-label">{{ trans('messages.respiratory_frequency') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_respiratory_frequency') }}" name="respiratory_frequency" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <h3 class="group-label first-label">{{ trans('messages.physical_exam') }}</h3>
                        <div class="form-group" id="height-group">
                            <label class="control-label">{{ trans('messages.height') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_height') }}" name="height"  id="height" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="head-group">
                            <label class="control-label">{{ trans('messages.head_circumference') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_head_circumference') }}" name="head" id="head" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="weight-group">
                            <label class="control-label">{{ trans('messages.weight') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_weight') }}" name="weight" id="weight" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="abdomen-group">
                            <label class="control-label">{{ trans('messages.abdominal_circumference') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_abdominal_circumference') }}" name="abdomen" onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="imt-group">
                            <label class="control-label">IMT</label>
                            <input type="text" class="form-control" placeholder="-" name="imt" id="imt" disabled onkeydown="number(event)" autocomplete="off">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/medical" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default" id="form-add-modal">
        <div class="modal fade window" id="add-vaccine-history" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img src="{{ asset('assets/images/icons/settings/add-vaccine.png') }}" />
                        <h5 class="modal-title" id="title-vaccine">{{ trans('messages.add_history_of_vaccination') }}</h5>
                        <button class="close" type="button" id="close-vaccine-history">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group" id="vaccine-group">
                            <input type="hidden" name="vaccine_id" value="" />
                            <label class="control-label">{{ trans('messages.vaccine_name') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.vaccine_name') }}" name="vaccine_name" id="vaccine_name" autocomplete="off" disabled >
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="vaccine-group">
                            <label class="control-label">{{ trans('messages.given_number') }}</label>
                            <input name="vaccine_dosage_sequence" id="vaccine_dosage_sequence" class="form-control" onkeydown="number(event)" value="" autocomplete="off" placeholder="{{ trans('messages.given_number_placeholder') }}" maxLength="5" />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="vaccine-group">
                            <label class="control-label">{{ trans('messages.given_date') }}</label>
                            <div id="datepicker-vaccine-given-date" class="input-group date">
                                <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="vaccine_given_date" id="vaccine_given_date" />
                                <span class="input-group-addon">
                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                </span>
                            </div>
                            <br>
                            <span class="help-block"></span>  
                        </div>
                    </div>
                    <div class="modal-footer">                    
                        <button class="btn btn-primary" id="save-vaccine-history">
                            <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            {{ trans('messages.save') }}
                        </button>
                        <button class="btn btn-secondary" id="cancel-vaccine-history">{{ trans('messages.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade window" id="add-history" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            <i class="fa fa-plus"></i>
                            {{ trans('messages.add_history_of_illness') }}
                        </h5>
                        <button class="close" type="button" id="close-illness">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group" id="illness-group">
                            <label class="control-label">{{ trans('messages.history_of_illness') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.history_of_illness') }}" name="illness" autocomplete="off" id="illness">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="add-illness">
                            <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            {{ trans('messages.save') }}
                        </button>
                        <button class="btn btn-secondary" id="cancel-illness">{{ trans('messages.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade window" id="add-history-allergy" data-backdrop="static" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            <i class="fa fa-plus"></i>
                            {{ trans('messages.add_history_of_allergy') }}
                        </h5>
                        <button class="close" type="button" id="close-allergy">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group" id="allergy-category-group">
                            <label class="control-label">{{ trans('messages.category') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="allergy-category" id="allergy-category">
                                    <option value="" disabled selected>{{ trans('messages.select_allergy_category') }}</option>
                                </select>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="allergy-group">
                            <label class="control-label">{{ trans('messages.history_of_allergy') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.history_of_allergy') }}" name="allergy" autocomplete="off" id="allergy">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="add-allergy">
                            <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            {{ trans('messages.save') }}
                        </button>
                        <button class="btn btn-secondary" id="cancel-allergy">{{ trans('messages.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    /**
     * VACCINE FEATURE
     */ 

    // VACCINE HISTORY //
    var vaccinesCache = [];

    function addVaccineCache(data) {
        vaccinesCache.push(data);
    }

    function removeVaccineCache(data) {
        const iToDelete = [];
        vaccinesCache.forEach(function(item, index) {
            if (item.id == data.id && item.dosage_sequence == data.dosage_sequence) {
                iToDelete.push(index);
            }
        });
        iToDelete.sort(function(a, b) { return b - a });
        iToDelete.forEach(function(item) {
            vaccinesCache.splice(item, 1);
        });
    }

    function appendVaccineHistoryToFormData(formData) {
        for (let i in vaccinesCache) {
            formData.append(`vaccine[history][${i}][id]`, vaccinesCache[i].id);
            formData.append(`vaccine[history][${i}][dosage_sequence]`, vaccinesCache[i].dosage_sequence);
            formData.append(`vaccine[history][${i}][date_given]`, vaccinesCache[i].date_given);
        }
    }

    function showAddVaccineHistory(id, name) {
        $("#add-vaccine-history").modal("show");
        $("#vaccine-group input[name='vaccine_id']").val(id);
        $("#vaccine-group input[name='vaccine_name']").val(name);
    }

    // add date time picker
    $('#datepicker-vaccine-given-date').datetimepicker({
        locale: '{{ $lang }}',
        format: 'DD/MM/YYYY'
    });

    // close modal on cancel
    $("#close-vaccine-history").click(function(event){
        event.preventDefault();
        $("#cancel-vaccine-history").click();
    });

    // reset modal form input on cancel
    $("#cancel-vaccine-history").click(function(event){
        event.preventDefault();
        resetVaccineHistory();
        $("#add-vaccine-history").modal("hide");
    });

    function resetVaccineHistory() {
        $("#add-vaccine-history").attr("disabled", false);
        $("#add-vaccine-history").removeClass("loading");
        resetValidation('vaccine-group #vaccine_dosage_sequence');
        resetValidation('vaccine-group #vaccine_given_date');
        $("#vaccine-group input[name=vaccine_id]").val("");
        $("#vaccine-group input[name=vaccine_name]").val("");
        $("#vaccine-group input[name=vaccine_dosage_sequence]").val("");
        $("#vaccine-group input[name=vaccine_given_date]").val("");
    }

    function listVaccineHistory(vaccineHistories) {
        $("#form-add #data-vaccine-history").html("");
        for (let i in vaccineHistories) {
            processVaccineHistory(vaccineHistories[i], false);
        }
    }

    function processVaccineHistory(vaccineHistory, isPush) {
        let title = '<td>' + vaccineHistory.name + '</td><td id="vaccine-row-' + vaccineHistory.id + '">';
        let action = '<div class="chip chip-add" id="chip-action-' + vaccineHistory.id + '">'
                        + '<span onClick="showAddVaccineHistory(\'' + vaccineHistory.id + '\',\'' + vaccineHistory.name + '\');">'
                        +   '<svg class="chip-add-svg" focusable="false" viewBox="0 0 6 6" aria-hidden="true"><path d="M2 1 h1 v1 h1 v1 h-1 v1 h-1 v-1 h-1 v-1 h1 z"></path></svg>'
                        + '</span>'
                        + '</div>';
        
        if (isPush) {
            $('#chip-action-' + vaccineHistory.id).remove();
            $('#form-add #vaccine-row-' + vaccineHistory.id).append(
                '<div class="chip" id="' + vaccineHistory.id + '_' + vaccineHistory.dosage_sequence + '">'
                +   '<div class="chip-content">' + vaccineHistory.dosage_sequence + ' &bull; ' + getShortFormatDate(vaccineHistory.date_given) + '</div>'
                +   '<div>'
                +       '<span onClick="deleteVaccineHistory(\'' + vaccineHistory.id + '_' + vaccineHistory.dosage_sequence + '\');">' 
                +           '<svg class="chip-close-svg" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z"></path></svg>'
                +       '</span>'
                +   '</div>'
                + '</div>'
                + action);
            addVaccineCache(vaccineHistory);
            return 0;
        }

        // here is to initiate data
        let section = '';
        for (let i in vaccineHistory.history) {
            section +=   '<div class="chip">'
                        +   '<div class="chip-content">' + vaccineHistory.history[i].dosage_sequence + ' &bull; ' + getShortFormatDate(vaccineHistory.history[i].date_given) + '</div>'
                        + '</div>';
        }

        $("#form-add #list-vaccine-history").removeClass("hide");
        $("#form-add #data-vaccine-history").append('<tr class="template_vaccine_history" id="vaccine-history-' + vaccineHistory.id + '">' + title + section + action + '</td></tr>');
    }

    function deleteVaccineHistory(identifier) {
        $('#form-add #' + identifier).remove();
        const id = identifier.split('_');
        removeVaccineCache({ id: id[0], dosage_sequence: id[1] });
        console.log(vaccinesCache);
    }

    $("#save-vaccine-history").click(function(event) {
        event.preventDefault();
        let id = $("#vaccine-group input[name='vaccine_id']").val();
        let name = $("#vaccine-group input[name='vaccine_name']").val();
        let dosageSequence = $("#vaccine-group input[name='vaccine_dosage_sequence']").val();
        let givenDate = $("#vaccine-group input[name='vaccine_given_date']").val();
        
        if (dosageSequence != "" && givenDate != "") {
            $("#add-vaccine-history").attr("disabled", true);
            $("#add-vaccine-history").addClass("loading");
        } else {
            formValidate(false, ['vaccine-group #vaccine_dosage_sequence','{{ trans("validation.empty_vaccination_dosage_sequence") }}', true]);
            formValidate(false, ['vaccine-group #vaccine_given_date','{{ trans("validation.empty_vaccination_given_date") }}', true]);
            return 0;
        }

        processVaccineHistory({
            id: id,
            name: name,
            dosage_sequence: dosageSequence,
            date_given: givenDate.split("/").reverse().join("-")
        }, true);

        notif(true, "{{ trans('validation.success_add_vaccination_history') }}");
        $("#add-vaccine-history").modal("toggle");
        resetVaccineHistory();
    });

    function resetAdd() {
        resetValidation('form-add #name');

        $("#form-add select[name='name']").val("").trigger("change");
        $("#form-add input[name='blood_sistole']").val("");
        $("#form-add input[name='blood_diastole']").val("");
        $("#form-add input[name='pulse']").val("");
        $("#form-add input[name='respiratory_frequency']").val("");
        $("#form-add input[name='body_temperature']").val("");
        $("#form-add input[name='height']").val("");
        $("#form-add input[name='head']").val("");
        $("#form-add input[name='weight']").val("");
        $("#form-add input[name='complaint']").val("");
        $("#form-add input[name='abdomen']").val("");
        $("#form-add input[name='imt']").val("");
    }

    let ilnessesPatients = {};
    let allergiesPatients = {};
    let timer = "";


    $("#form-add input[name='name']").autocomplete({
        minLength: 3,
        source: function(request, response) {
            let temp = [];
            clearTimeout(timer);
            timer = setTimeout(() => {
                $.ajax({
                    type: "GET",
                    url: "{{ $api_url }}/{{ $lang }}/clinic/patient?active=true&q=" + request.term + "&per_page=10",
                    beforeSend: function() {
                        $("#form-add #loading-name").removeClass("hide");
                    },
                    success: function(results) {
                        $("#form-add #loading-name").addClass("hide");
                        if (!results.error) {
                            for (i = 0; i < results.data.length; i++) {
                                let patient = results.data[i];
                                temp.push({
                                    id: patient.id,
                                    name: patient.name,
                                    value: patient.name,
                                    illnesses: patient.illnesses_history,
                                    allergies: patient.allergies_history,
                                    vaccines: patient.vaccine_history,
                                })
                            }
                            response(temp);
                        }
                    },
                    error: function(xhr) {
                        $("#form-add #loading-name").addClass("hide");
                        response(temp);
                    }
                });
            }, 2000);
        },
        focus: function() {
            return false;
        },
        select: function(event, ui) {
            listIllness(ui.item && ui.item.illnesses);
            listAllergies(ui.item && ui.item.allergies);
            listVaccineHistory(ui.item && ui.item.vaccines);
            $("#form-add input[name='name']").val(ui.item.name);
            $("#form-add input[name='name']").attr("data-id", ui.item.id);
            return false;
        }
    });

    $('#form-add select[name=name]').on('change', function() {
        var name = this.value;

        if (name != "") {
            $.ajax({
                url: "{{ $api_url }}/{{ $lang }}/vital-sign/patient/" + this.value + "?date={{ Date('Y-m-d') }}",
                type: "GET",
                processData: false,
                contentType: false,
                success: function(data) {

                    console.log(data);
                    if (!data.error) {
                        if (data.id != undefined) {
                            $("#form-add input[name='id']").val(checkZero(data.id));
                            $("#form-add input[name='blood_sistole']").val(checkZero(data.sistole));
                            $("#form-add input[name='blood_diastole']").val(checkZero(data.diastole));
                            $("#form-add input[name='pulse']").val(checkZero(data.pulse));
                            $("#form-add input[name='respiratory_frequency']").val(checkZero(data.respiratory_frequency));
                            $("#form-add input[name='body_temperature']").val(checkZero(data.body_temperature));
                            $("#form-add input[name='height']").val(checkZero(data.height));
                            $("#form-add input[name='head']").val(checkZero(data.head));
                            $("#form-add input[name='weight']").val(checkZero(data.weight));
                            $("#form-add input[name='abdomen']").val(checkZero(data.abdomen));
                            $("#form-add input[name='imt']").val(checkZero(data.imt));
                            $("#form-add input[name='complaint']").val(data.complaint);
                        } else {
                            $("#form-add input[name='id']").val("");
                            $("#form-add input[name='blood_sistole']").val("");
                            $("#form-add input[name='blood_diastole']").val("");
                            $("#form-add input[name='pulse']").val("");
                            $("#form-add input[name='respiratory_frequency']").val("");
                            $("#form-add input[name='body_temperature']").val("");
                            $("#form-add input[name='height']").val("");
                            $("#form-add input[name='head']").val("");
                            $("#form-add input[name='weight']").val("");
                            $("#form-add input[name='abdomen']").val("");
                            $("#form-add input[name='imt']").val("");
                            $("#form-add input[name='complaint']").val("");
                        }
                    } else {
                        $("#form-add input[name='id']").val("");
                        $("#form-add input[name='blood_sistole']").val("");
                        $("#form-add input[name='blood_diastole']").val("");
                        $("#form-add input[name='pulse']").val("");
                        $("#form-add input[name='respiratory_frequency']").val("");
                        $("#form-add input[name='body_temperature']").val("");
                        $("#form-add input[name='height']").val("");
                        $("#form-add input[name='head']").val("");
                        $("#form-add input[name='weight']").val("");
                        $("#form-add input[name='abdomen']").val("");
                        $("#form-add input[name='imt']").val("");
                        $("#form-add input[name='complaint']").val("");
                    }
                },
                error: function() {
                    notif(false, "{{ trans('validation.failed') }}");
                }
            });
        } else {
            $("#form-add input[name='id']").val("");
            $("#form-add input[name='blood_sistole']").val("");
            $("#form-add input[name='blood_diastole']").val("");
            $("#form-add input[name='pulse']").val("");
            $("#form-add input[name='respiratory_frequency']").val("");
            $("#form-add input[name='body_temperature']").val("");
            $("#form-add input[name='height']").val("");
            $("#form-add input[name='head']").val("");
            $("#form-add input[name='weight']").val("");
            $("#form-add input[name='abdomen']").val("");
            $("#form-add input[name='imt']").val("");
            $("#form-add input[name='complaint']").val("");
        }
    });

    $("#form-add").submit(function(event) {
        event.preventDefault();
        $("#form-add button").attr("disabled", true);

        formData = new FormData();
        const imt = updateIMT();

        formData.append("validate", false);
        formData.append("id_patient", $("#form-add input[name='name']").data("id"));
        formData.append("blood_sistole", $("#form-add input[name='blood_sistole']").val());
        formData.append("blood_diastole", $("#form-add input[name='blood_diastole']").val());
        formData.append("pulse", $("#form-add input[name='pulse']").val());
        formData.append("respiratory_frequency", $("#form-add input[name='respiratory_frequency']").val());
        formData.append("body_temperature", $("#form-add input[name='body_temperature']").val());
        formData.append("height", $("#form-add input[name='height']").val());
        formData.append("head", $("#form-add input[name='head']").val());
        formData.append("weight", $("#form-add input[name='weight']").val());
        formData.append("abdomen", $("#form-add input[name='abdomen']").val());
        formData.append("complaint", $("#form-add input[name='complaint']").val());
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        // append vaccine history to formData
        appendVaccineHistoryToFormData(formData);

        if (imt) {
            formData.append("imt", imt);
        }
        var data_illness = new Array();
        $('#riwayat :selected').each(function(i, selected) {
            data_illness[i] = $(selected).val();
            formData.append("illnesses_history[" + i + "]", data_illness[i]);
        });
        const data_allergy = new Array();
        $('#riwayat-alergi :selected').each(function(i, selected) {
            data_allergy[i] = $(selected).val();
            formData.append("allergies_history[" + i + "]", data_allergy[i]);
        });

        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");

        var id = $("#form-add input[name='id']").val();
        var getUrl = "";
        if (id == "") {
            getUrl = "{{ $api_url }}/{{ $lang }}/vital-sign";
        } else {
            formData.append("_method", "PATCH");
            getUrl = "{{ $api_url }}/{{ $lang }}/vital-sign/" + id;
        }

        $.ajax({
            url: getUrl,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {

                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                if (!data.error) {

                    if (!data.success) {
                        formValidate(true, ['form-add #name', data.errors.id_patient, true]);
                    } else {
                        resetAdd();

                        notif(true, "{{ trans('validation.success_save_preliminary_examination') }}");

                        window.location = "#/{{ $lang }}/medical";
                        loading($(this).attr("#/{{ $lang }}/medical"));

                    }
                } else {
                    notif(false, "{{ trans('validation.failed') }}");
                }
            },
            error: function() {
                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                notif(false, "{{ trans('validation.failed') }}");
            }
        })
    });

    /*
        IMT
     */

    $("#form-add input[name='weight']").on('keyup', function() {
        updateIMT();
    });

    $("#form-add input[name='height']").on('keyup', function() {
        updateIMT();
    });

    function updateIMT() {
        let imt = 0;
        const weight = $("#form-add input[name='weight']").val();
        const height = $("#form-add input[name='height']").val();

        if (height) {
            const heightInMeter = height * 0.01;
            imt = weight / (heightInMeter * heightInMeter);
            $("#form-add input[name='imt']").val(imt.toFixed(1));
        } else {
            $("#form-add input[name='imt']").val("");
        }

        return imt;
    }

    $("#add-illness").click(function(event) {
        event.preventDefault();
        var name_illness = $("#illness-group input[name=illness]").val();
        if (name_illness != "") {
            $("#add-illness").attr("disabled", true);
            $("#add-illness").addClass("loading");
            $("#add-illness span").removeClass("hide");
            addIllness(name_illness);
        } else {
            formValidate(false, ['form-add-modal #illness', '{{ trans("validation.empty_illness") }}', true]);
        }
    });

    function addIllness(illness) {
        formData = new FormData();
        formData.append("indonesian_name", illness);
        formData.append("english_name", "");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/illness",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                var id = $('#riwayat').val() || [];
                if (!response.error) {
                    $("#add-illness").attr("disabled", false);
                    $("#add-illness").removeClass("loading");
                    $("#add-illness span").addClass("hide");
                    $("#cancel-illness").click();
                    id.push(response.data);
                    listIllness(id);
                } else {
                    $("#add-illness").attr("disabled", false);
                    $("#add-illness").removeClass("loading");
                    $("#add-illness span").addClass("hide");
                    formValidate(false, ['form-add-modal #illness', response.message, true]);
                }
            }
        });
    }

    $("#riwayat").select2({
        placeholder: "{{ trans('messages.history_of_illness') }}",
        allowClear: true
    });

    $("#close-allergy").click(function(event) {
        event.preventDefault();
        $("#cancel-allergy").click();
    });

    $("#close-illness").click(function(event) {
        event.preventDefault();
        $("#cancel-illness").click();
    });

    $("#cancel-allergy").click(function(event) {
        event.preventDefault();
        $("#add-history-allergy").modal("hide");
        resetValidation('form-add-modal #allergy', 'form-add-modal #allergy-category');
        $("#allergy-group input[name=allergy]").val("");
    });

    
    $("#cancel-illness").click(function(event) {
        event.preventDefault();
        $("#add-history").modal("hide");
        resetValidation('form-add-modal #illness', 'form-add-modal #illness');
        $("#illness-group input[name=illness]").val("");
    });

    $("#add-allergy").click(function(event) {
        event.preventDefault();

        const id_allergy_category = $("#allergy-category-group select[name=allergy-category]").val();
        const name_allergy = $("#allergy-group input[name=allergy]").val();
        if (!id_allergy_category) {
            formValidate(false, ['form-add-modal #allergy-category', '{{ trans("validation.empty_allergy_category") }}', true]);
        }
        if (!name_allergy) {
            formValidate(false, ['form-add-modal #allergy', '{{ trans("validation.empty_allergy") }}', true]);
        }

        if (id_allergy_category && name_allergy) {
            $("#add-allergy").attr("disabled", true);
            $("#add-allergy").addClass("loading");
            $("#add-allergy span").removeClass("hide");
            addAllergy(id_allergy_category, name_allergy);
        }

    });

    function addAllergy(id_allergy_category, name_allergy) {
        formData = new FormData();
        formData.append("indonesian_name", name_allergy);
        formData.append("english_name", "");
        formData.append("id_allergy_category", id_allergy_category);
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/allergies",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(response) {
                var id = $('#riwayat-alergi').val() || [];
                if (!response.error) {
                    $("#add-allergy").attr("disabled", false);
                    $("#add-allergy").removeClass("loading");
                    $("#add-allergy span").addClass("hide");
                    $("#cancel-allergy").click();
                    id.push(response.data);
                    listAllergies(id);
                } else {
                    $("#add-allergy").attr("disabled", false);
                    $("#add-allergy").removeClass("loading");
                    $("#add-allergy span").addClass("hide");
                    formValidate(false, ['form-add-modal #allergy', response.message, true]);
                }
            },
            error: function(xhr, statusText) {
                $("#add-allergy").attr("disabled", false);
                $("#add-allergy").removeClass("loading");
                $("#add-allergy span").addClass("hide");
                formValidate(false, ['form-add #allergy', statusText, true]);
            }
        });
    }

    $("#riwayat-alergi").select2({
        placeholder: "{{ trans('messages.history_of_allergy') }}",
        allowClear: true
    });

    function listIllness(val) {
        loading_content("#riwayat-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/illness",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data_illness) {
                if (!data_illness.error) {
                    var data = data_illness.data;
                    $("#riwayat").html("");
                    loading_content("#riwayat-group", "success");
                    for (var i = 0; i < data.length; i++) {
                        if (val && val.length > 0) {
                            var selected = "";
                            for (var j = 0; j < val.length; j++) {
                                if (data[i].id == val[j].id || data[i].id == val[j]) {
                                    selected = "selected='selected'";
                                }
                            }
                        }
                        $("#riwayat").append("<option value='" + data[i].id + "' " + selected + ">" + data[i].name + "</option>")
                    }
                    $("#riwayat").select2({
                        placeholder: "{{ trans ('messages.history_of_illness') }}",
                        allowClear: true
                    });
                } else {
                    loading_content("#riwayat-group", "failed");
                    $("#riwayat-group #loading-content").click(function() {
                        listIllness(val);
                    });
                }
            },
            error: function() {
                loading_content("#riwayat-group", "failed");
                $("#riwayat-group #loading-content").click(function() {
                    listIllness(val);
                });
            }
        });
    }

    function listAllergyCategories() {
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/allergy_categories",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
                if (!data.error) {
                    $("#allergy-category").html("");
                    $("#allergy-category").append(`<option value="" disabled selected>{{ trans('messages.select_allergy_category') }}</option>`);
                    data.data.forEach(value => {
                        let selected = '';
                        $("#allergy-category").append("<option value='" + value.id + "' " + selected + ">" + value.name + "</option>");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                notif(false, textStatus);
            }
        });
    }

    listAllergyCategories();

    function listAllergies(val) {
        loading_content("#riwayat-alergi-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/allergies",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
                if (!data.error) {
                    loading_content("#riwayat-alergi-group", "success");
                    $("#riwayat-alergi").html("");
                    data.data.forEach(allergy => {
                        let selected = '';
                        val && val.forEach(value => {
                            if (value.id === allergy.id || allergy.id == value) {
                                selected = "selected='selected'";
                            }
                        });
                        $("#riwayat-alergi").append("<option value='" + allergy.id + "' " + selected + ">" + allergy.name + " (" + allergy.name_category + ")" + "</option>")
                    });

                    $("#riwayat-alergi").select2({
                        placeholder: "{{ trans('messages.history_of_allergy') }}",
                        allowClear: true
                    });
                } else {
                    loading_content("#riwayat-alergi-group", "failed");
                    $("#riwayat-alergi-group #loading-content").click(function() {
                        listAllergies(val);
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                loading_content("#riwayat-alergi-group", "failed");
                $("#riwayat-alergi-group #loading-content").click(function() {
                    listAllergies(val);
                });
            }
        });
    }

    listAllergies();

</script>