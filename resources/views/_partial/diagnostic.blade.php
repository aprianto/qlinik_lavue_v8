
<div class="modal fade window" id="diagnostic" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: none;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="title-medicine">Penunjang diagnostik</h5>
            </div>
            <form id="form-diagnostic">
                <div class="modal-body alert-notif">
                    <div class="row">
                        @if(Session::get('pcare'))
                        <div class="col-md-6" id="ppk-parent">
                            <div class="form-group" id="ppk-group">
                                <label class="control-label">PPK <span>*</span></label>
                                <div class="border-group">
                                    <select class="form-control" name="diagnostic_helper[0]" id="ppk">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-6 form-group">
                            <div class="form-group" id="service_date-group">
                                <label class="control-label">Tanggal Pelayanan <span>*</span></label>
                                <div id="" class="input-group date datepicker_date">
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="diagnostic_helper[1]" />
                                    <span class="input-group-addon">
                                        <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <ul class="list__diagnostic">
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Tekanan Darah</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="blood" data-trigger="blood_pressure-group" value="tekanan darah"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="blood_pressure-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Sistole (mmHg)</label>
                                            <input type="text" data-trigger="Sistole" id="form0" data-id="blood" class="form-control" placeholder="{{ trans('messages.input_blood_pressure') }}" name="diagnostic_helper[2]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Diastole (mmHg)</label>
                                            <input type="text" id="form1" data-trigger="Diastole" data-id="blood" class="form-control" placeholder="Masukkan tekanan darah" name="diagnostic_helper[3]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Radiologi</label>
                                        <label class="float-right"><input type="checkbox" data-id="radiologi" name="type_diagnostic" data-trigger="radiologi-group" value="radiologi"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="radiologi-group">
                                    <div class="col-md-12">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Keterangan Radiologi</label>
                                            <input type="text" data-trigger="Keterangan Radiologi" id="form2" data-id="radiologi" class="form-control" placeholder="Masukkan keterangan radiologi" name="diagnostic_helper[4]" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Darah Rutin</label>
                                        <label class="float-right"><input type="checkbox" data-id="routine" name="type_diagnostic" data-trigger="routine_blood-group" value="darah rutin"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="routine_blood-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Hemoglobin (gr %)</label>
                                            <input type="text" data-trigger="Hemoglobin" id="form3" data-id="routine" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[5]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Laju Endap Darah (mm/jam)</label>
                                            <input type="text" data-trigger="Laju Endap Darah" id="form4" data-id="routine" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[6]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Leukosit (/mm3)</label>
                                            <input type="text" data-trigger="Leukosit" class="form-control" data-id="routine" id="form5" placeholder="Masukkan data" name="diagnostic_helper[7]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Eritrosit (juta/m3)</label>
                                            <input type="text" data-trigger="Eritrosit" class="form-control" data-id="routine" id="form6" placeholder="Masukkan data" name="diagnostic_helper[8]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Hematokrit</label>
                                            <input type="text" data-trigger="Hematokrit" class="form-control" data-id="routine" id="form7" placeholder="Masukkan data" name="diagnostic_helper[9]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Trombosit</label>
                                            <input type="text" data-trigger="Trombosit" class="form-control" data-id="routine" id="form8" placeholder="Masukkan data" name="diagnostic_helper[10]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Lemak Darah</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="fat" data-trigger="blood_fat-group" value="lemak darah"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="blood_fat-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">HDL</label>
                                            <input type="text" data-trigger="HDL" class="form-control" id="form9" data-id="fat" placeholder="Masukkan data" name="diagnostic_helper[11]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">LDL</label>
                                            <input type="text" data-trigger="LDL" class="form-control" id="form10" data-id="fat" placeholder="Masukkan data" name="diagnostic_helper[12]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Cholesterol Total</label>
                                            <input type="text" data-trigger="Cholesterol Total" id="form11" data-id="fat" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[13]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Trigliserid</label>
                                            <input type="text" data-trigger="Trigliserid" id="form12" data-id="fat" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[14]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Gula Darah</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="sugar" data-trigger="blood_sugar-group" value="gula darah"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="blood_sugar-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Gula Darah Sewaktu</label>
                                            <input type="text" data-trigger="Gula Darah Sewaktu" id="form13" data-id="sugar" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[15]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Gula Darah Puasa</label>
                                            <input type="text" data-trigger="Gula Darah Puasa" id="form14" data-id="sugar" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[16]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Gula Darah Post Pradial</label>
                                            <input type="text" data-trigger="Gula Darah Post Pradial" id="form15" data-id="sugar" class="form-control" placeholder="Masukkan data" name="diagnostic_helper[17]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">HbA1C</label>
                                            <input type="text" data-trigger="HbA1C" class="form-control" data-id="sugar" id="form16" placeholder="Masukkan data" name="diagnostic_helper[18]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Fungsi Hati</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="liver" data-trigger="liver-group" value="fungsi hati"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="liver-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">SGOT</label>
                                            <input type="text" data-trigger="SGOT" class="form-control" data-id="liver" id="form17" placeholder="Masukkan data" name="diagnostic_helper[19]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">SGPT</label>
                                            <input type="text" data-trigger="SGPT" class="form-control" data-id="liver" id="form18" placeholder="Masukkan data" name="diagnostic_helper[20]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Gamma GT</label>
                                            <input type="text" data-trigger="Gamma GT" class="form-control" data-id="liver" id="form19" placeholder="Masukkan data" name="diagnostic_helper[21]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">ProtKual</label>
                                            <input type="text" data-trigger="ProtKual" class="form-control" data-id="liver" id="form20" placeholder="Masukkan data" name="diagnostic_helper[22]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Fungsi Ginjal</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="kidney" data-trigger="kidney-group" value="fungsi ginjal"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="kidney-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Creatinin</label>
                                            <input type="text" data-trigger="Creatinin" class="form-control" data-id="kidney" id="form21" placeholder="Masukkan data" name="diagnostic_helper[23]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">Ureum</label>
                                            <input type="text" data-trigger="Ureum" class="form-control" data-id="kidney" id="form22" placeholder="Masukkan data" name="diagnostic_helper[24]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Asam Urat</label>
                                            <input type="text" data-trigger="Asam Urat" class="form-control" data-id="kidney" id="form23" placeholder="Masukkan data" name="diagnostic_helper[25]" onkeydown="number(event)" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <div class="sf-check">
                                        <label class="control-label">Fungsi Jantung</label>
                                        <label class="float-right"><input type="checkbox" name="type_diagnostic" data-id="heart" data-trigger="heart-group" value="fungsi jantung"><span></span></label>
                                    </div>
                                </div>
                                <div class="row hide" id="heart-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">ABI</label>
                                            <input type="text" data-trigger="ABI" class="form-control" id="form24" data-id="heart" placeholder="Masukkan data" name="diagnostic_helper[26]" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="diastole-group">
                                            <label class="control-label">EKG</label>
                                            <input type="text" data-trigger="EKG" class="form-control" id="form25" data-id="heart" placeholder="Masukkan data" name="diagnostic_helper[27]" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="sistole-group">
                                            <label class="control-label">Echo</label>
                                            <input type="text" data-trigger="Echo" class="form-control" id="form26" data-id="heart" placeholder="Masukkan data" name="diagnostic_helper[28]" autocomplete="off" onclick="downClick(this)">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Kesimpulan Dokter</label>
                        <textarea class="form-control" name="diagnostic_helper[29]" placeholder="Masukkan kesimpulan dokter"></textarea>
                        <input type="hidden" name="diagnostic_helper[30]">
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" onClick="selectedDiagnostic()">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

function downClick(event) {
    $("input[name='"+event.name+"']").keydown(function(e){
        if(e.keyCode !== 38 && e.keyCode !== 40) return
        $("input[name='"+event.name+"']").off("keydown")

        var checked = false;
        var x = $(".sf-check").find("input[name=type_diagnostic]");
        var y = $(".list__diagnostic").find("input.form-control");
        changeFieldFocus(x, y, event, e.keyCode)
        return
    });
}

function changeFieldFocus(x, y, event, keyCode) {
    for (i = 0; i < x.length; i++) {
        if($(x[i]).attr("data-id") === $(event).attr("data-id")) {
            if(!$(x[i]).is(":checked")) {
                $(x[i]).click()
                checked = true
            }else {
                checked = true
            }
            break;
        }
    }
    for (i = 0; i < y.length; i++) {
        if(event === y[i]) {
            setFieldFocus(i, keyCode, checked)
            break;
        }
    }
}

function setFieldFocus(index, keyCode, checked) {
    if (keyCode === 38 && index > 0 && checked) {
        index--
        $("#form"+index+"").focus()
        $("#form"+index+"").click()
    } else if( keyCode === 40 && index < 26 && checked) {
        index++
        $("#form"+index+"").focus()
        $("#form"+index+"").click()
    }
}

$('#form-diagnostic input[name=type_diagnostic]').change(function() {  
    var hiddenId = $(this).attr("data-trigger");
    if ($(this).is(':checked')) {
        $("#" + hiddenId).removeClass('hide');
    } else {
        $("#" + hiddenId).addClass('hide');        
    }
});

var checked_ppk = false;

@if(Session::get('pcare'))
checked_ppk = true;
function listPPK(val) {
    loading_content("#form-diagnostic #ppk-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-provider",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPpk){
            if(!dataPpk.error) {
                loading_content("#form-diagnostic #ppk-group", "success");

                var item = dataPpk.list;

                $("#form-diagnostic select[name='diagnostic_helper[0]']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    try {
                    $("#form-diagnostic select[name='diagnostic_helper[0]']").append("<option value='"+item[i].kdProvider+"'>"+item[i].nmProvider+"</option>");
                    } catch(err) {}
                }

                $("#form-diagnostic select[name='diagnostic_helper[0]']").select2({
                    placeholder: "{{ trans('messages.select_schedule') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-diagnostic select[name=schedule]").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-diagnostic #ppk-group", "failed");

                $("#form-diagnostic #ppk-group #loading-content").click(function(){ listPPK(val); });
            }
        },
        error: function(){
            loading_content("#form-diagnostic #ppk-group", "failed");

            $("#form-diagnostic #ppk-group #loading-content").click(function(){ listPPK(val); });
        }
    })
}
@endif

$('.datepicker_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'YYYY-MM-DD'
});

$('#form-diagnostic select[name="diagnostic_helper[0]"]').on('change', function(e){
    if (e.target.value) {
        resetValidation('ppk-parent #ppk')
    }
});

function selectedDiagnostic(){
    var checked = true;
    var ppk = $("#form-diagnostic select[name='diagnostic_helper[0]'] option:selected").val();
    
    if(!$("#form-diagnostic #ppk-parent").hasClass("hide")) {
        if(checked_ppk) {
            if(ppk == "") {
                formValidate(false, ['ppk-parent #ppk','{{ trans("validation.empty_ppk") }}', true]);
                checked = false;
            }else {
                checked = true;
            }
        }
    }

    if(checked) {
        $('#diagnostic').hide();
        $('.modal-backdrop').remove();
        $('body').removeClass("modal-open");
        $("#data-diagnostic").empty();
        var dates = convertDate($("#form-diagnostic input[name='diagnostic_helper[1]']").val());

        var numDiagnostic = 0;
        var keyFile=0;
        var tr;
        var td;
        var keyDate = dateRepo;

        $("#date-diagnostic").text('('+getShortFormatDate(dates)+')')
        $('#form-diagnostic input[name=type_diagnostic]:checked').map(function(_, el) {
            id = $(el).attr("data-trigger");
            tr = $('<tr/>');
            td = $('<td/>');
            tr.append("<td><a><h1 class='no-hover'>"+$(el).val()+"</h1></a></td>");
            $("#"+id+" input[name^='diagnostic_helper']").each(function(key, value) {
                try {
                    td.append($(this).attr("data-trigger") + " : " + value.value +" <br />");
                }
                catch(err) {}
            });
            tr.append(td);
            $("#data-diagnostic").append(tr);
            numDiagnostic++;
        });

        $("#data-diagnostic").append(
            "<tr><td><a><h1 class='no-hover'>Kesimpulan</h1></a></td><td>"+$("#form-diagnostic textarea[name='diagnostic_helper[29]']").val()+"</td></tr>"
        );

        if(numDiagnostic > 0) {
            $("#list-diagnostic").removeClass("hide");
            $("#add-diagnostik").text("+ Ubah data penunjang");
            notif(true,"Penunjang diagnostik berhasil diperbarui!");
        }else{
            $("#list-diagnostic").addClass("hide");
            $("#add-diagnostik").text("+ Tambah data penunjang");
        }
    }
}

@if(Session::get('pcare'))
listPPK('');
@endif
</script>