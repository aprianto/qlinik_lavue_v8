<div class="modal fade window" id="rujukan" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: none;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-klinik.png') }}" />
                <h5 class="modal-title" id="title-medicine">Cari Rujukan</h5>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <form id="form-rujukan">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="gender-group">
                        <label class="control-label">Jenis Rujukan </label>
                        <div class="sf-radio"> 
                            <label><input type="radio" value="0" name="is_specialist" data-trigger="special_conditions-group"><span></span> Kondisi Khusus</label>
                            <label><input type="radio" value="1" name="is_specialist" data-trigger="specialist_data-group"><span></span> Spesialis</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="row row-rujukan hide" id="special_conditions-group">
                        <div class="col-md-3">
                            <div class="form-group column-group" id="category_bpjs-group">
                                <label class="control-label">Kategori </label>
                                <div class="border-group">
                                    <select class="form-control" name="category_bpjs">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3 hide" id="subcategory_content">
                            <div class="form-group column-group" id="subcategory_bpjs-group">
                                <label class="control-label">Sub kategori </label>
                                <div class="border-group">
                                    <select class="form-control" name="subcategory_bpjs">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" id="note-group">
                                <label class="control-label">Catatan </label>
                                <input type="text" class="form-control" placeholder="Isi catatan" name="note">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" id="service_date-group">
                                <label class="control-label">Tanggal Rencana Berkunjung</label>
                                <div id="" class="input-group date datepicker_date">
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="date_visit_special" value="{{ date('d/m/Y') }}" />
                                    <span class="input-group-addon">
                                        <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary" style="float: none;" id="find_special_condition">
                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    Cari Faskes
                                </button>
                                <button type="button" class="btn btn-secondary" style="float: none;" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                    <div class="row row-rujukan hide" id="specialist_data-group">
                        <div class="col-md-3 column-group">
                            <div class="form-group content-group" id="specialist-group">
                                <label class="control-label">Spesialis<span>*</span></label>
                                <div class="border-group">
                                    <select class="form-control" name="specialist">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group content-group" id="subspecialist-group">
                                <label class="control-label">Subspesialis<span>*</span></label>
                                <div class="border-group">
                                    <select class="form-control" name="subspecialist" disabled>
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group content-group" id="tool-group">
                                <label class="control-label">Sarana<span>*</span></label>
                                <div class="border-group">
                                    <select class="form-control" name="tool">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" id="service_date-group">
                                <label class="control-label">Tanggal Rencana Berkunjung</label>
                                <div id="" class="input-group date datepicker_date">
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="date_visit_specialist" value="{{ date('d/m/Y') }}" />
                                    <span class="input-group-addon">
                                        <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btn btn-primary" style="float: none;" id="find_specialist">
                                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    Cari Faskes
                                </button>
                                <button type="button" class="btn btn-secondary" style="float: none;" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-area">
                        <table class="table table-hover" id="table-rujukan">
                            <thead>
                                <tr>
                                    <th>Nama Faskes</th>
                                    <th class="hide-lg">Kelas</th>
                                    <th class="hide-md">Alamat</th>
                                    <th class="hide-md">Jadwal</th>
                                    <th class="hide-md" style="width: 130px;">Kuota Rujukan</th>
                                    <th style="width: 135px;"></th>
                                </tr>
                            </thead>
                            <tbody id="table">
                                <td colspan='6' align='center'>Harap selesaikan filter pencarian untuk melihat daftar faskes</td>
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="6" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="6" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination-sec bottom-sec">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$('#form-rujukan input[name=is_specialist]').click(function() {  
    var hiddenId = $(this).attr("data-trigger");
    $(".row-rujukan").not("#" + hiddenId).addClass('hide');
    $("#" + hiddenId).removeClass('hide');
});

$("#form-rujukan select[name='subspecialist']").select2({
    placeholder: "Pilih subspesialis",
    allowClear: false,
    dropdownParent: $('#subspecialist-group')
});

$('#form-rujukan select[name=specialist]').on('change', function() {
    var code = this.value;
    listSubSpecialist(code);
});

$('#form-rujukan select[name=category_bpjs]').on('change', function() {
    var code = this.value;
    if(code == "THA" || code == "HEM"){
        $("#form-rujukan #subcategory_content").removeClass("hide");
        listSubcategoryBpjs();
    }else{
        $("#form-rujukan select[name=subcategory_bpjs]").val('').trigger("change");
        $("#form-rujukan #subcategory_content").addClass("hide");
    }
});

$('#form-rujukan #find_special_condition').click(function() {  
    var type = $("#form-rujukan select[name='category_bpjs']").val();
    var no_card = $("#form-add input[name='card_number']").val();
    var note = $("#form-rujukan input[name='note']").val();
    var date = convertDate($("#form-rujukan input[name='date_visit_special']").val());
    var value = $("#form-rujukan select[name='category_bpjs']").val();
    var subType = (value == 'THA' || value == 'HEM') ? $("#form-rujukan select[name='subcategory_bpjs']").val() : '';
    if(type == ''){
        formValidate(true, ['category_bpjs',"Kategori harus diisi", true]);
    }else if(value != ''){
        if(value == 'THA' || value == 'HEM'){
            if(subType == ''){
                formValidate(true, ['subcategory_bpjs',"Subkategori harus diisi", true]);
            }else{
                findSpecialCondition(type, no_card, date, subType);
            }
        }else{
            if(note.length < 20){
                formValidate(true, ['note',"Minimal alasan 20 karakter", true]);
            }else if(date == ''){
                formValidate(true, ['date_visit_specialist',"Tanggal harus diisi", true]);
            }else{
                findSpecialCondition(type, no_card, date, subType);
            }
        }
    }else if(note.length < 20){
        formValidate(true, ['note',"Minimal alasan 20 karakter", true]);
    }else if(date == ''){
        formValidate(true, ['date_visit_specialist',"Tanggal harus diisi", true]);
    }else{
        findSpecialCondition(type, no_card, date, subType);
    }
});

$('#form-rujukan #find_specialist').click(function() {
    var specialist = $("#form-rujukan select[name='specialist']").val();
    var subSpecialist = $("#form-rujukan select[name='subspecialist']").val();
    var nmSpecialist = $("#form-rujukan select[name='subspecialist']").find(':selected').html();
    var tool = $("#form-rujukan select[name='tool'] option:selected").val();
    var date = convertDate($("#form-rujukan input[name='date_visit_specialist']").val());
    if(specialist == ''){
        formValidate(true, ['specialist',"{{ trans('validation.empty_of_specialist') }}", true]);
    }else if(subSpecialist == ''){
        formValidate(true, ['subspecialist',"{{ trans('validation.empty_of_subspesialis') }}", true]);
    }else if(date == ''){
        formValidate(true, ['date_visit_specialist',"{{ trans('validation.empty_of_date') }}", true]);
    }else if(tool == ''){
        formValidate(true, ['tool', "{{ trans('validation.empty_of_tool') }}", true]);
    }else {
        findSpecialist(subSpecialist, tool, date, nmSpecialist);
    }
});

function listCategoryBpjs() {
    loading_content("#form-rujukan #category_bpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-referensi-khusus",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-rujukan #category_bpjs-group", "success");

                var item = data.list;

                $("#form-rujukan select[name='category_bpjs']").prop("disabled", false);
                $("#form-rujukan select[name='category_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-rujukan select[name='category_bpjs']").append("<option value='"+item[i].kdKhusus+"'>"+item[i].nmKhusus+"</option>");
                }

                $("#form-rujukan select[name='category_bpjs']").select2({
                    placeholder: "Pilih kategori",
                    allowClear: false,
                    dropdownParent: $('#category_bpjs-group')
                });
                
            } else {
                loading_content("#form-rujukan #category_bpjs-group", "failed");

                $("#form-rujukan #category_bpjs-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-rujukan #category_bpjs-group", "failed");

            $("#form-rujukan #category_bpjs-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listSubcategoryBpjs() {
    loading_content("#form-rujukan #subcategory_bpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-referensi-khusus-sub-specialist",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-rujukan #subcategory_bpjs-group", "success");

                var item = data.list;

                $("#form-rujukan select[name='subcategory_bpjs']").prop("disabled", false);
                $("#form-rujukan select[name='subcategory_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-rujukan select[name='subcategory_bpjs']").append("<option value='"+item[i].kdSubSpesialis+"'>"+item[i].nmSubSpesialis+"</option>");
                }

                $("#form-rujukan select[name='subcategory_bpjs']").select2({
                    placeholder: "Pilih sub kategori",
                    allowClear: false,
                    dropdownParent: $('#subcategory_bpjs-group')
                });
                
            } else {
                loading_content("#form-rujukan #subcategory_bpjs-group", "failed");

                $("#form-rujukan #subcategory_bpjs-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-rujukan #subcategory_bpjs-group", "failed");

            $("#form-rujukan #subcategory_bpjs-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listSarana() {
    loading_content("#form-rujukan #tool-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-sarana",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-rujukan #tool-group", "success");

                var item = data.list;

                $("#form-rujukan select[name='tool']").prop("disabled", false);
                $("#form-rujukan select[name='tool']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-rujukan select[name='tool']").append("<option value='"+item[i].kdSarana+"'>"+item[i].nmSarana+"</option>");
                }

                $("#form-rujukan select[name='tool']").select2({
                    placeholder: "Pilih sarana",
                    allowClear: false,
                    dropdownParent: $('#tool-group')
                });
                
            } else {
                loading_content("#form-rujukan #tool-group", "failed");

                $("#form-rujukan #tool-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-rujukan #tool-group", "failed");

            $("#form-rujukan #tool-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listSpecialist() {
    loading_content("#form-rujukan #specialist-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-specialist",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-rujukan #specialist-group", "success");

                var item = data.list;

                $("#form-rujukan select[name='specialist']").prop("disabled", false);
                $("#form-rujukan select[name='specialist']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-rujukan select[name='specialist']").append("<option value='"+item[i].kdSpesialis+"'>"+item[i].nmSpesialis+"</option>");
                }

                $("#form-rujukan select[name='specialist']").select2({
                    placeholder: "Pilih spesialis",
                    allowClear: false,
                    dropdownParent: $('#specialist-group')
                });
                
            } else {
                loading_content("#form-rujukan #specialist-group", "failed");

                $("#form-rujukan #specialist-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-rujukan #specialist-group", "failed");

            $("#form-rujukan #specialist-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listSubSpecialist(code) {
    loading_content("#form-rujukan #subspecialist-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-sub-specialist/"+code,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-rujukan #subspecialist-group", "success");

                var item = data.list;

                $("#form-rujukan select[name='subspecialist']").prop("disabled", false);
                $("#form-rujukan select[name='subspecialist']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-rujukan select[name='subspecialist']").append("<option value='"+item[i].kdSubSpesialis+"'>"+item[i].nmSubSpesialis+"</option>");
                }

                $("#form-rujukan select[name='subspecialist']").select2({
                    placeholder: "Pilih subspecialis",
                    allowClear: false,
                    dropdownParent: $('#subspecialist-group')
                });
                
            } else {
                loading_content("#form-rujukan #subspecialist-group", "failed");

                $("#form-rujukan #subspecialist-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-rujukan #subspecialist-group", "failed");

            $("#form-rujukan #subspecialist-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function findSpecialCondition(type, no_card, date, subType){
    $("#table").empty();
    $("#loading-table").removeClass("hide");
    $("#reload").addClass("hide");

    var url = "";
    if(subType != ''){
        url = "{{ $api_url }}/{{ $lang }}/bpjs/list-rujukan-khusus-sub-specialist/"+type+"/"+subType+"/"+no_card+"/"+date
    }else{
        url = "{{ $api_url }}/{{ $lang }}/bpjs/list-rujukan-khusus/"+type+"/"+no_card+"/"+date;
    }

    $.ajax({
        url: url,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.list.length;
                var per_page = 10;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.list;
                    var tr;
                    var no = 0;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            tr = $('<tr/>');

                            tr.append("<td><b>"+item[i].nmppk+"<br /></b>"+item[i].kdppk+"</td>");
                            
                            tr.append("<td class='hide-lg'>"+item[i].kelas+"</td>");
                            tr.append("<td class='hide-md'>"+item[i].alamatPpk+"<br /><b>"+item[i].nmkc+"</b><br />"+item[i].distance+" m<br /><a href='"+item[i].telpPpk+"'>"+item[i].telpPpk+"</a></td>");
                            tr.append("<td class='hide-md'>"+item[i].jadwal+"</td>");

                            tr.append("<td class='hide-md'>"+item[i].jmlRujuk+" / "+item[i].kapasitas+" ("+item[i].persentase+"%)</td>");

                            var action = "<a onclick=\"selectedFaskes('"+item[i].nmppk+"','"+item[i].kdppk+"','"+type+"','"+date+"',' ')\" class='btn-table btn-blue'>Pilih Faskes</a>";

                            tr.append("<td>"+action+"</td>");
                            $("#table").append(tr);

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<td colspan='6' align='center'>"+data.message+"</td>");

                    $(".pagination-sec").addClass("hide");

                }

            } else {
                tr.append("<td colspan='6' align='center'>Harap selesaikan filter pencarian untuk melihat daftar faskes</td>");
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#table-rujukan #reload").removeClass("hide");
        }
    })
}

function findSpecialist(subSpecialist, tool, date, nmSpecialist){
    $("#table").empty();
    $("#loading-table").removeClass("hide");
    $("#table-rujukan #reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-rujukan-sub-specialist/"+subSpecialist+"/"+tool+"/"+date,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.list.length;
                var per_page = 10;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.list;
                    var tr;
                    var no = 0;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            tr = $('<tr/>');

                            tr.append("<td><b>"+item[i].nmppk+"<br /></b>"+item[i].kdppk+"</td>");
                            
                            tr.append("<td class='hide-lg'>"+item[i].kelas+"</td>");
                            tr.append("<td class='hide-md'>"+item[i].alamatPpk+"<br /><b>"+item[i].nmkc+"</b><br />"+item[i].distance+" m<br /><a href='"+item[i].telpPpk+"'>"+item[i].telpPpk+"</a></td>");
                            tr.append("<td class='hide-md'>"+item[i].jadwal+"</td>");

                            tr.append("<td class='hide-md'>"+item[i].jmlRujuk+" / "+item[i].kapasitas+" ("+item[i].persentase+"%)</td>");

                            var action = "<a onclick=\"selectedFaskes('"+item[i].nmppk+"','"+item[i].kdppk+"','"+nmSpecialist+"','"+date+"',' ')\" class='btn-table btn-blue'>Pilih Faskes</a>";

                            tr.append("<td>"+action+"</td>");
                            $("#table").append(tr);

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<td colspan='6' align='center'>"+data.message+"</td>");

                    $(".pagination-sec").addClass("hide");
                        
                }

            } else {
                tr.append("<td colspan='6' align='center'>Harap selesaikan filter pencarian untuk melihat daftar faskes</td>");
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#table-rujukan #reload").removeClass("hide");
        }
    })
}

function selectedFaskes(name, code, type, date, note){
    $('#rujukan').modal('hide');
    $('.modal-backdrop').remove();
    $('body').removeClass( "modal-open" );
    $('#form-add #reference_selected-group').removeClass('hide');
    $('#form-add input[name=name_ppk]').val(name);
    $('#form-add input[name=code_reference_hospital]').val(code);
    $('#form-add input[name=name_specialist]').val(type);
    $('#form-add input[name=date_to_reference_string]').val(getFormatDate(date));
    $('#form-add input[name=date_to_reference]').val(date);
    $('#form-add input[name=note]').val(note);
}

$("#rujukan #specialist-group .border-group").click(function() {
    resetValidation('rujukan #specialist');
});

$("#rujukan #subspecialist-group .border-group").click(function() {
    resetValidation('rujukan #subspecialist');
});

$("#rujukan #tool-group .border-group").click(function() {
    resetValidation('rujukan #tool');
});

$("#rujukan #category_bpjs-group .border-group").click(function() {
    resetValidation('rujukan #category_bpjs');
});

$("#rujukan #subcategory_bpjs-group .border-group").click(function() {
    resetValidation('rujukan #subcategory_bpjs');
});

$("#rujukan input[name='note']").focus(function() {
    resetValidation('rujukan #note');
});

listCategoryBpjs()
listSpecialist()
listSarana()
</script>