<div id="alert" class="alert-sec alert hide">
	<div class="notify-alert green-skin">
		<div class="notify-content">
			<div class="row">
				<div class="col-md-12">
					<span></span>
					<a class="close-alert" onclick="$(this).parent().parent().parent().parent().parent().css({'top':'-30%'});$(this).parent().parent().parent().parent().parent().addClass('hide');">{{ trans('messages.close') }}</a>
				</div>
			</div>
		</div>
	</div>
</div>