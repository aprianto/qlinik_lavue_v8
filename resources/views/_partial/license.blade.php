<div class="modal fade pop-up" id="license_agreemen" tabindex="-1" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-file-text-o "></i> {{ trans('messages.title_agreemen') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body content-license">
                <h5> KETENTUAN PRIVASI</h5>
                <br/>
                <p class="center">www.medigo.id</p>
                <br/>
                <p>Ketentuan Privasi berikut merupakan perjanjian hukum ("<b>Perjanjian</b>") antara pengguna Produk Medigo ("<b>Pengguna</b>") dengan pemilik, pengembang dan pengelola Produk Medigo yaitu <b>PT Medigo Kesehatan Indonesia ("Medigo").</b> Perjanjian ini mengatur hubungan hukum antara Pengguna dan Medigo dalam hal penggunaan situs www.medigo.id, informasi, Produk Medigo, atau produk-produk lainnya yang dikeluarkan oleh Medigo ("<b>Produk</b>"). Medigo menetapkan Ketentuan Privasi ini untuk memberikan informasi kepada pengguna mengenai jenis informasi yang Medigo kumpulkan, mengapa Medigo mengumpulkan informasi pengguna, untuk apa Medigo menggunakan informasi pengguna, bagaimana pengguna dapat mengelola informasi pengguna, dan dalam keadaan apa, jika ada, informasi itu diungkapkan. Dengan menggunakan produk Medigo, pengguna menerima praktik yang dijelaskan dalam ketentuan privasi ini.</p>
                <br/>
                <p>Pengguna dan Medigo secara bersama-sama disebut sebagai "<b>Para Pihak</b>".</p>
                <br/>
                <p>Para pihak sepakat mengenai hal sebagaimana tercantum berikut:</p>
                <br/>
                <b>PENDAHULUAN</b>
                <br/>
                <br/>
                <div class="wrapper-text">
                    <p>1.</p><p class="content-text">Pengguna diwajibkan membaca dan memahami semua ketentuan yang tertera di dalam Perjanjian ini sebelum menggunakan produk Medigo. Dengan menggunakan Produk Medigo, Pengguna dianggap telah menerima dan menyetujui seluruh ketentuan dalam Perjanjian ini.</p>
                </div>
                <div class="wrapper-text">
                    <p>2.</p><p class="content-text">Medigo berhak untuk, dan Pengguna memberikan hak kepada Medigo untuk, sewaktu-waktu mengubah atau menghapus ketentuan-ketentuan dalam Perjanjian ini. Dalam hal terjadi pengubahan atau penghapusan ketentuan-ketentuan dalam Perjanjian, Pengguna dianggap memberikan persetujuannya terhadap Perjanjian versi terbaru saat Pengguna menggunakan Produk Medigo setelah perubahan atau penghapusan tersebut terjadi.</p>
                </div>
                <br/>
                <b>KETENTUAN PRIVASI</b>
                <br/>
                <br/>
                <div class="wrapper-text">
                    <p>1.</p><p class="content-text">Pengguna dapat memberikan informasi mengenai nama, alamat, alamat surat elektronik, nomor telepon, informasi kartu kredit dan finansial. Pengguna memberikan hak kepada Medigo untuk menyimpan serta menampilkannya dalam akun Pengguna serta dalam data Medigo. Kerahasiaan data Pengguna terjamin dan akan digunakan hanya untuk keperluan interaksi dengan dokter yang dilakukan melalui Produk Medigo.</p>
                </div>
                <div class="wrapper-text">
                    <p>2.</p><p class="content-text">Pengguna memberikan hak kepada Medigo untuk merekam atau menyimpan segala bentuk interaksi atau komunikasi antara Pengguna dengan Dokter secara otomatis. Pengguna dapat memberikan atau mengunggah deskripsi pribadi, foto, dan/atau termasuk diantaranya informasi tentang kesehatan Pengguna, rekam medis, performa fisik dan mental, informasi penyakit dan disabilitas, ras, jenis kelamin, usia dan informasi sensitif lainnya. Segala data yang disimpan oleh Medigo yang berasal dari interaksi ataupun komunikasi antara Dokter dengan Pengguna akan digunakan oleh Medigo semata-mata untuk keperluan legal dan peningkatan kualitas pelayanan medis Medigo. Medigo menjamin kerahasiaan data Pengguna dan semua data tidak akan disebarluaskan dengan sengaja oleh Medigo.</p>
                </div>
                <div class="wrapper-text">
                    <p>3.</p><p class="content-text">Informasi yang secara otomatis diambil oleh Medigo adalah sebagai berikut:</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>1.</p><p class="content-text">Informasi teknis, termasuk di dalamnya alamat internet protocol (IP address) yang digunakan oleh ponsel ataupun peralatan lainnya untuk mengakses Produk Medigo, informasi login, jenis dan versi dari sistem operasi (operating system), zona waktu, lokasi; dan</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>2.</p><p class="content-text">Informasi kunjungan Pengguna, termasuk di dalamnya produk dan jasa yang dilihat atau digunakan, waktu respon, informasi interaksi dan nomor telepon yang digunakan untuk oleh Pengguna.</p>
                </div>
                <div class="wrapper-text">
                    <p>4.</p><p class="content-text">Medigo menggunakan informasi yang diberikan oleh Pengguna untuk:</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>1.</p><p class="content-text">Memberikan produk dan layanan.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>2.</p><p class="content-text">Memberikan informasi kepada pihak lain yang terkait seperti rekanan dokter dan/atau penyedia jasa kesehatan lainnya dalam rangka penyediaan produk dan layanan kepada Pengguna.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>3.</p><p class="content-text">Proses pembayaran Pengguna untuk produk dan layanan.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>4.</p><p class="content-text">Meninjau dan meningkatkan kualitas produk dan layanan Medigo, termasuk di dalamnya pemantauan kepatuhan pada standar layanan klinis.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>5.</p><p class="content-text">Membuat pengungkapan yang diperlukan atau berhubungan dengan permintaan beralasan dari badan pembuat regulasi termasuk di dalamnya Konsil Kedokteran Indonesia atau badan lain yang diperlukan untuk keperluan hukum dan/atau regulasi.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>6.</p><p class="content-text">Sesuai dengan permintaan dan/atau persetujuan Pengguna untuk keperluan asuransi yang berkaitan dengan produk atau layanan yang diberikan.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>7</p><p class="content-text">Sesuai dengan permintaan Pengguna untuk memberikan informasi tersebut kepada fasilitas pelayanan kesehatan lain yang terkait dengan Pengguna, termasuk di dalamnya apabila Pengguna harus dirujuk ke fasilitas pelayanan kesehatan tersebut.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>8.</p><p class="content-text">Membantu dalam pendeteksian penipuan.</p>
                </div>
                <div class="wrapper-text">
                    <p>5.</p><p class="content-text">Medigo menggunakan informasi yang diberikan oleh Pengguna juga untuk:</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>1.</p><p class="content-text">Mengelola Produk Medigo untuk operasional internal, termasuk di dalamnya untuk tujuan penyelesaian masalah, analisa data, keperluan statistik dan survei.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>2.</p><p class="content-text">Mengembangkan Produk Medigo untuk memastikan bahwa konten ditampilkan dengan cara yang paling efektif bagi Pengguna.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>3.</p><p class="content-text">Mengizinkan Pengguna untuk berpartisipasi dalam fitur-fitur interaktif dari layanan Medigo, sesuai dengan keinginan Pengguna.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>4.</p><p class="content-text">Tujuan lain seperti penelitian di mana nama Pengguna untuk data tersebut akan disamarkan atau ditiadakan (anonim).</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>5.</p><p class="content-text">Sebagai bagian dari usaha untuk tetap membuat Produk Medigo aman dan terjamin kualitasnya.</p>
                </div>
                <div class="wrapper-text-tab">
                    <p>6.</p><p class="content-text">Membuat saran dan rekomendasi kepada Pengguna tentang layanan yang mungkin menarik bagi Pengguna.</p>
                </div>
                <div class="wrapper-text">
                    <p>6.</p><p class="content-text">Medigo berhak untuk membatasi atau menghentikan penggunaan Produk Medigo jika Medigo memiliki alasan untuk mencurigai bahwa Pengguna telah melanggar ketentuan dalam Perjanjian ini, Syarat dan Ketentuan Medigo, atau peraturan perundang-undangan yang berlaku.</p>
                </div>
                <div class="wrapper-text">
                    <p>7.</p><p class="content-text">Jika sebagian atau seluruh aset Medigo diakuisisi oleh pihak ketiga, data yang dimiliki oleh Medigo menjadi salah satu bagian dari aset yang akan diserahkan.</p>
                </div>
                <div class="wrapper-text">
                    <p>8.</p><p class="content-text">Medigo dapat membuka informasi pribadi jika Medigo berada dalam kewajiban untuk membuka informasi pribadi Pengguna untuk kepatuhan dan kebutuhan hukum, untuk melaksanakan kesepakatan lain, atau untuk melindungi hak dan keamanan Medigo dan Pengguna.</p>
                </div>
                <div class="wrapper-text">
                    <p>9.</p><p class="content-text">Medigo menggunakan penyedia layanan pihak ketiga untuk menyediakan layanan yang melibatkan pemrosesan data, untuk pemeriksaan referensi, saran profesional (termasuk hukum, akuntansi, konsultasi keuangan dan bisnis), teknologi, situs web, penelitian, pemrosesan data, asuransi, forensik, pemasaran, dan layanan keamanan. Medigo memberikan informasi kepada perusahaan-perusahaan tersebut hal-hal yang mereka butuhkan untuk melakukan layanan mereka. Perusahaan-perusahaan ini dilarang untuk menyalahgunakan data-data yang Medigo berikan selain dengan layanan yang berkaitan dengan medigo.id, dan medigo akan memastikan kerahasiaan data data pengguna sehingga tidak digunakan untuk kepentingan yang tidak semestinya.</p>
                </div>
                <br/>
                <b>PEMBERITAHUAN</b>
                <br/>
                <br/>
                <div class="wrapper-text">
                    <p>1.</p><p class="content-text">Segala bentuk pemberitahuan, permintaan informasi atau komunikasi kepada atau tentang Medigo akan dianggap diterima oleh Medigo jika telah dibuat secara tertulis dan dikirimkan ke alamat Medigo melalui kurir atau surat tercatat di:</p>
                    <br>
                </div>
                <div class="content-text-tab"><p>PT Medigo Teknologi Kesehatan Graha Chantia Lantai Dasar</p><p>Jalan Bangka Raya No. 6, Pela Mampang</p><p>Jakarta Selatan 12720.</p></div>
                <div class="wrapper-text">
                    <p>2.</p><p class="content-text">Segala bentuk pemberitahuan, permintaan informasi atau komunikasi kepada atau tentang Pengguna akan dianggap telah diterima oleh Pengguna jika: Medigo mengirimkan e-mail atau surat elektronik atau surat tertulis kepada Pengguna melalui alamat yang dicantumkan oleh Pengguna dalam akun Pengguna di Produk Medigo; atau Jika dianggap diperlukan oleh Medigo, Medigo memasang pengumuman baik yang bisa diakses secara umum ataupun terbatas di Produk Medigo.</p>
                </div>
                <br/>
                <b>PENUTUP</b>
                <br/>
                <br/>
                <div class="wrapper-text">
                    <p>1.</p><p class="content-text">Penggunaan dan akses ke Produk Medigo diatur oleh Syarat dan Ketentuan serta Ketentuan Privasi Medigo. Dengan mengakses atau menggunakan Produk Medigo, berarti pengguna telah memahami dan menyetujui serta terikat dan tunduk dengan segala syarat dan ketentuan yang berlaku di Situs ini.</p>
                </div>
                <div class="wrapper-text">
                    <p>2.</p><p class="content-text">Hubungan antara Medigo dan Pengguna merupakan suatu hubungan independen dan tidak memiliki hubungan keagenan, kemitraan, usaha patungan, karyawan-perusahaan atau pemilik waralaba-pewaralaba yang akan dibuat atau dibuat dengan adanya Perjanjian ini.</p>
                </div>
                <div class="wrapper-text">
                    <p>3.</p><p class="content-text">Judul di dalam Ketentuan ini dibuat hanya sebagai acuan, dan sama sekali tidak menetapkan, membatasi, menjelaskan atau menjabarkan apa yang termaktub dalam pasal atau ketentuan tersebut.</p>
                </div>
                <div class="wrapper-text">
                    <p>4.</p><p class="content-text">Medigo memiliki hak untuk menginvestigasi dan menuntut hak untuk setiap pelanggaran atas Perjanjian ini sepanjang yang dimungkinkan dan diperkenankan oleh hukum.</p>
                </div>
                <div class="wrapper-text">
                    <p>5.</p><p class="content-text">Pengguna dengan ini mengakui bahwa Medigo memiliki hak untuk memonitor akses penggunaan Produk Medigo untuk memastikan kepatuhan Para Pihak atas Perjanjian ini, atau untuk mematuhi peraturan yang berlaku atau perintah atau persyaratan dari pengadilan, lembaga administratif atau badan pemerintahan lainnya di Republik Indonesia.</p>
                </div>
                <div class="wrapper-text">
                    <p>6.</p><p class="content-text">Apabila terdapat 1 (satu) atau lebih ketentuan di dalam Perjanjian ini yang dinyatakan tidak berlaku, melawan hukum atau tidak dapat dilaksanakan berdasarkan ketentuan perundang-undangan yang berlaku di Republik Indonesia maka keadaan tersebut tidak akan mempengaruhi keberlakuan ketentuan-ketentuan lain dalam Perjanjian ini. Dalam hal terjadi keadaan tersebut, Para Pihak berjanji untuk tetap berusaha memenuhi segala ketentuan yang terdapat di dalam Perjanjian ini.</p>
                </div>
                <div class="wrapper-text">
                    <p>7.</p><p class="content-text">Segala sengketa yang berkaitan dengan Perjanjian ini, akan pertama-tama diselesaikan secara musyawarah untuk mufakat oleh Para Pihak. Dalam hal penyelesaian secara musyawarah untuk mufakat tidak dapat dilakukan, Para Pihak memutuskan untuk menyelesaikan konflik yang terjadi melalui Pengadilan Perdata.</p>
                </div>
                <div class="wrapper-text">
                    <p>8.</p><p class="content-text">Perjanjian ini diatur, ditafsirkan, serta dilaksanakan berdasarkan hukum yang berlaku di Republik Indonesia dan Para Pihak diwajibkan untuk tunduk kepada semua peraturan yang berlaku di Indonesia.</p>
                </div>
                <br/>
                <p>Pasien setuju bahwa segala informasi dan data medis pasien, termasuk informasi dan data mana yang dapat diklasifikasikan sebagai Rahasia Kedokteran dan Rekam Medis berdasarkan peraturan perundang-undangan yang berlaku dalam Republik Indonesia, akan disetor, disimpan, dan/atau diproses dalam infrastruktur online milik PT Medigo Teknologi Kesehatan.</p>
            </div>
        </div>
    </div>
</div>