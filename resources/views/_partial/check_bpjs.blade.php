<div class="modal fade window-confirm auto-center" id="checkBPJS" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Pasien</h1>
                </div>
                <form class="sec form-bpjs" role="form" style="margin-top: 25px;" id="formCheck">
                    <div class="form-group" id="type-group">
                        <label class="control-label" style="font-size: 12px;">Jenis Pasien</label>
                        <div class="sf-radio"> 
                            <label style="font-size: 14px;"><input type="radio" value="0" name="type"><span></span> BPJS Kesehatan</label>
                            <label style="font-size: 14px;"><input type="radio" value="1" name="type"><span></span> Umum</label>
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="nik-group" style="display: none;">
                        <label class="control-label" style="font-size: 12px;">Nomor kartu BPJS / NIK (KTP/KIA) <span>*</span></label>
                        <input type="number" class="form-control" style="font-size: 14px;" placeholder="Masukkan nomor kartu" name="nik">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm" style="display: none;">Cek Data</button>
                <a href="#/{{ $lang }}/patient/create" onclick="loading($(this).attr('href')); $('#checkBPJS').hide(); $('.modal-backdrop').remove(); $('body').removeClass('modal-open');" class="btn btn-primary" id="create" style="display: none;">Tambah Pasien</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm auto-center" id="checkBPJSOnly" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Pasien</h1>
                </div>
                <form class="sec form-bpjs" role="form" style="margin-top: 25px;" id="formCheckBpjs">
                    <div class="form-group" id="nik-group">
                        <label class="control-label" style="font-size: 12px;">Nomor kartu BPJS / NIK (KTP/KIA) <span>*</span></label>
                        <input type="number" class="form-control" style="font-size: 14px;" placeholder="Masukkan nomor kartu" name="nik">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm">Cek Data</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm auto-center" id="showBPJS" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Pasien</h1>
                </div>
                <ul class="detail-profile" style="margin-top: 27px;">
                    <div id="detail-profile">
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">NIK</span>
                                    <span id="patient_nik"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">No BPJS Kesehatan</span>
                                    <span id="patient_bpjs"></span>
                                    <span>Status: <span id="patient_status" style="display: initial; color: #24E2CF;"></span></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Nama</span>
                                    <span id="patient_name"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Faskes 1</span>
                                    <span id="patient_facility"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Kelas</span>
                                    <span id="patient_category"></span>
                                </div>
                            </div>
                        </li>                    
                    </div>
                </ul>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" id="create">Tambah Pasien</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm auto-center" id="showBPJSOnly" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Pasien</h1>
                </div>
                <ul class="detail-profile" style="margin-top: 27px;">
                    <div id="detail-profile">
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">NIK</span>
                                    <span id="patient_nik"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">No BPJS Kesehatan</span>
                                    <span id="patient_bpjs"></span>
                                    <span>Status: <span id="patient_status" style="display: initial; color: #24E2CF;"></span></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Nama</span>
                                    <span id="patient_name"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Faskes 1</span>
                                    <span id="patient_facility"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Kelas</span>
                                    <span id="patient_category"></span>
                                </div>
                            </div>
                        </li>                    
                    </div>
                </ul>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" id="create">Tambah Pasien</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#resetFormBpjs").on('click', function(){
    $(".form-bpjs")[0].reset();
    $('input[type="radio"]').prop('checked', false);
    $("#checkBPJS").find("#nik-group").hide();
});

$("#resetFormBpjsOnly").on('click', function(){
    $("#formCheckBpjs")[0].reset();
});

$("#showBPJS").find("#create").on('click', function(){
    var id = $("#checkBPJS input[name=nik]").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/is-member-terdaftar/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    $("#showBPJS").hide();
                    $('.modal-backdrop').remove();
                    $('body').removeClass("modal-open");
                    window.location = "#/{{ $lang }}/patient/create/"+ id;
                    loading($(this).attr("#/{{ $lang }}/patient/create/"+ id));
                }
            } else {
                notif(false,data.message);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#showBPJSOnly").find("#create").on('click', function(){
    var id = $("#checkBPJSOnly input[name=nik]").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/is-member-terdaftar/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    $("#showBPJSOnly").hide();
                    $('.modal-backdrop').remove();
                    $('body').removeClass("modal-open");
                    window.location = "#/{{ $lang }}/patient/create/"+ id;
                    loading($(this).attr("#/{{ $lang }}/patient/create/"+ id));
                }
            } else {
                notif(false,data.message);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$('#checkBPJS').find('.modal-footer #confirm').on('click', function(){
    var id = $("input[name=nik]").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(data.data){
                if(!data.error) {
                    if(data.success==true) {
                        if(data.data.aktif==false){
                            $("#showBPJS").find(".modal-footer #create").hide();
                            notif(false,data.data.ketAktif);
                        }else{
                            if(id.length != 16){
                                $("#showBPJS #patient_nik").html(data.data.noKTP);
                            }else{
                                $("#showBPJS #patient_nik").html(id);
                            }
                            $("#showBPJS #patient_bpjs").html(data.data.noKartu);
                            $("#showBPJS #patient_name").html(data.data.nama);
                            $("#showBPJS #patient_facility").html(data.data.kdProviderPst.nmProvider);
                            $("#showBPJS #patient_category").html(data.data.jnsKelas.nama);
                            $("#showBPJS #patient_status").html(data.data.ketAktif);
                            $("#showBPJS").find(".modal-footer #create").show();
                            $("#checkBPJS").find("#nik-group").hide();
                            $("#checkBPJS").modal("toggle");
                            $("#showBPJS").modal("toggle");
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            }else{
                notif(false,"Nomor tidak valid!");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$('#checkBPJSOnly').find('.modal-footer #confirm').on('click', function(){
    var id = $("#checkBPJSOnly input[name=nik]").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
            if(data.data){
                if(!data.error) {
                    if(data.success==true) {
                        if(data.data.aktif==false){
                            $("#showBPJSOnly").find(".modal-footer #create").hide();
                            notif(false,data.data.ketAktif);
                        }else{
                            if(id.length != 16){
                                $("#showBPJSOnly #patient_nik").html(data.data.noKTP);
                            }else{
                                $("#showBPJSOnly #patient_nik").html(id);
                            }
                            $("#showBPJSOnly #patient_nik").html(data.data.noKTP);
                            $("#showBPJSOnly #patient_bpjs").html(data.data.noKartu);
                            $("#showBPJSOnly #patient_name").html(data.data.nama);
                            $("#showBPJSOnly #patient_facility").html(data.data.kdProviderPst.nmProvider);
                            $("#showBPJSOnly #patient_category").html(data.data.jnsKelas.nama);
                            $("#showBPJSOnly #patient_status").html(data.data.ketAktif);
                            $("#checkBPJSOnly").modal("toggle");
                            $("#showBPJSOnly").modal("toggle");
                        }
                    }
                } else {
                    notif(false,data.message);
                }
            }else{
                notif(false,"Nomor tidak valid!");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>
