<div class="modal fade window-confirm" id="confirmCancel" role="dialog" aria-labelledby="confirmCancelLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/confirm/confirm-deactivate.png') }}" />
                    <h1>{{ trans('messages.info_canceled') }}</h1>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="confirm">
                        <span class="fa fa-circle-o-notch fa-spin fa-1x fa-fw hide"></span>
                        {{ trans('messages.yes') }}
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>    
            </div>
        </div>
    </div>
</div>
