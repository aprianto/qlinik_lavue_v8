<div class="modal fade window-confirm" id="confirmNonactive" role="dialog" aria-labelledby="confirmNonactiveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/confirm/confirm-deactivate.png') }}" />
                    <h1>{{ trans('messages.info_deactivate') }}</h1>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="confirm">{{ trans('messages.deactivate') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>    
            </div>
        </div>
    </div>
</div>
