<div class="modal fade window-confirm" id="confirmVerification" role="dialog" aria-labelledby="confirmVerificationLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="center">
                    <span>{{ trans('messages.verification') }}</span>
                    <h1>{{ trans('messages.info_verification') }}</h1>
                    <i class="fa fa-check"></i>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm">{{ trans('messages.yes') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
