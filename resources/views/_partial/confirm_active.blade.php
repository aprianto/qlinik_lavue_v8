<div class="modal fade window-confirm" id="confirmActive" role="dialog" aria-labelledby="confirmActiveLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/confirm/confirm-activate.png') }}" />
                    <h1>{{ trans('messages.info_activate') }}</h1>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm">{{ trans('messages.activate') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
