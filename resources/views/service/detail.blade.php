@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right" id="path"></h3>
                </div>
                <div class="cell" id="loading-detail">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell" id="reload-detail">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>                
                <div id="detail" class="">
                    <div class="detail-item">
                        <div class="detail">
                            <h5 id="code"></h5>
                            <h3 id="name"></h3>
                            <h5 id="note"></h5>
                            <div class="bottom">
                                <span id="service_category" class="blue"></span>
                                <span id="polyclinic"></span>
                            </div>
                        </div>
                        <div class="detail detail-color">
                            <h5>Harga</h5>
                            <div id="info-item">
                            </div>
                        </div>
                        <div class="detail detail-color">
                            <div id="info-other-item">
                            </div>
                        </div>
                        <div class="detail-button">
                            <a href="#/{{ $lang }}/service/{{ $id }}/edit" title="" class="btn-main">{{ trans("messages.edit_data") }}</a>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
detail('{{ $id }}');

function detail(id) {
    $('#loading-detail').removeClass('hide');
    $('#detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/service/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#loading-detail').addClass('hide');
                $('#detail').removeClass('hide');

                $("#path").html(data.name);
                $("#detail #name").html(data.name);
                $("#detail #code").html(data.code);
                
                var polyclinic;
                if(data.id_polyclinic==0) {
                    polyclinic = "{{ trans('messages.all_polyclinic') }}";
                } else {
                    polyclinic = data.polyclinic.name;
                }


                $("#detail #polyclinic").html(polyclinic);
                
                try {
                    $("#detail #service_category").html(data.service_category.name);
                } catch(err) {
                    $("#detail #service_category").addClass("hide");
                }
                
                $("#detail #note").html(data.note);

                itemCost = data.cost_category;
                for(var i=0; i<itemCost.length; i++) {
                    try {
                        var style_cost = "";
                        if(i==itemCost.length-1) {
                            style_cost = "last-item";
                        }

                        $("#detail #info-item").append('<span class="price-item '+style_cost+'">'+
                                    '<u>'+itemCost[i].patient_category.name+'</u>'+
                                    '<ins>'+formatCurrency('{{ $lang }}','Rp',itemCost[i].cost)+'</ins>'+                                
                                '</span>');
                    } catch(err) {}
                }

                if(nullToZero(data.admin_cost)!=0) {
                    $("#detail #info-other-item").append('<span class="price-item">'+
                                '<u>{{ trans("messages.admin_cost") }}</u>'+
                                '<ins>Rp '+nullToZero(data.admin_cost)+'</ins>'+                           
                            '</span>');
                }

                var implementer = "";
                if(data.implementer==1) {
                    implementer = "<img src=\"{{ asset('assets/images/icons/info/status-active.png') }}\" class='icon-status' />";
                } else {
                    implementer = "<img src=\"{{ asset('assets/images/icons/info/status-inactive.png') }}\" class='icon-status' />";
                }

                var style_implementer = "";
                if(nullToZero(data.admin_cost)==0) {
                    style_implementer = "last-item";
                }

                $("#detail #info-other-item").append('<span class="price-item '+style_implementer+'"><u>{{ trans("messages.select_implementer") }}</u><ins>'+implementer+'</ins></span>');

                
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

</script>