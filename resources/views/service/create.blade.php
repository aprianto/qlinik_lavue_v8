@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_service') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form id="form-add" role="form" class="sec">
                        <div class="form-group" id="typeBpjs-group">
                            <label class="control-label">Tipe Layanan <span>*</span></label>
                            <div class="sf-radio"> 
                                <label><input type="radio" value="outpatient" name="typeBpjs"><span></span> Rawat Jalan</label>
                                <label><input type="radio" value="inpatient" name="typeBpjs"><span></span> Rawat Inap</label>
                                <label><input type="radio" value="promotif_preventif" name="typeBpjs"><span></span> Promotif Preventif</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        @if(Session::get('pcare'))
                        <div class="form-group" id="serviceBpjs-group">
                            <label class="control-label">Layanan BPJS </label>
                            <div class="border-group">
                                <select class="form-control" name="code_bpjs" disabled>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        @endif
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.service_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_service_name') }}" id="name" name="name">
                            <span id="loading-name" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="code-group">
                            <label class="control-label">{{ trans('messages.service_code') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_service_code') }}" id="code" name="code">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="polyclinic-group">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select name="polyclinic" id="polyclinic" class="form-control">
                                        </select>                                
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="service_category-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">{{ trans('messages.service_category') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select id="service_category" name="service_category" class="form-control">        
                                        </select>                            
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-6">
                                    <a id="icon-add" class="link-add link-label" data-toggle="modal" data-target="#pop-up">+ {{ trans('messages.add_category') }}</a>
                                </div>
                            </div>
                        </div>
                        <label class="control-label">{{ trans("messages.cost_symbol") }} <span>*</span></label>
                        <div id="section-cost">
                            <div class="form-group" id="patient_category-group">
                                <select class="form-control"></select>
                                <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            </div>
                        </div>
                        <div class="form-group add-group">
                            <a class="btn-add" onclick="addCost()">+ {{ trans('messages.add_cost') }}</a>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.note') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_note') }}" id="note" name="note" />
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="sf-check">
                                        <label><input type="checkbox" name="type_admin_cost"><span></span> {{ trans('messages.admin_cost') }}</label>
                                    </div>
                                    <div class="form-sub-group hide" id="admin_cost_sec">
                                        <label class="control-label">{{ trans('messages.admin_cost_symbol') }}</label>
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_admin_cost') }}" id="admin_cost" name="admin_cost" onkeydown="number(event)" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="implementer"><span></span> {{ trans('messages.option_implementer') }}</label>
                            </div>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/service" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade window" id="pop-up" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-kat-layanan.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_service_category') }}</h5>
            </div>
            <form id="form-pop-up">
                <div class="modal-body modal-main alert-notif">               
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.service_category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_service_category') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="hide" id="item-cost">
    <div class="form-group">
        <input type="text" class="form-control cost" placeholder='{{ trans("messages.input_cost") }}' name="cost[]"  onkeydown="number(event)"  autocomplete="off" onclick="info_help_price();" oninput="convertNumber()">
        <img class="remove-item" src="{{ asset('assets/images/icons/nav/delete-m.png') }}" onclick="$(this).parent().remove();removeCost();" />
        <select name="patient_category[]" class="select-cost">
            
        </select>
        <span class="help-block"></span>
    </div>
</div>



<script>

var costItem = 0;
var numPatientCategory = 0;
var defaultPatientCategory = "";

function convertNumber() {
    $(document).find("#item-cost").each(function(key, value) {
        var form_input = $("input[name^='cost']");
        
        if(event.which >= 37 && event.which <= 40) return
        $(form_input).val(function(index, field) {
            return field
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
        });
    });
}

function addCost() {
    $("#form-add #section-cost").append($("#item-cost").html());

    costItem=costItem+1;

    if(costItem==1) {
        $("#form-add #section-cost .remove-item").addClass("hide");
    } else {
        $("#form-add #section-cost .remove-item").removeClass("hide");
    }

    if(costItem>=numPatientCategory) {
        $("#form-add .button-add-item").addClass("hide");
    } else {
        $("#form-add .button-add-item").removeClass("hide");
    }
}

function removeCost() {
    costItem=costItem-1;

    if(costItem==1) {
        $("#form-add #section-cost .remove-item").addClass("hide");
    } else {
        $("#form-add #section-cost .remove-item").removeClass("hide");
    }

    if(costItem>=numPatientCategory) {
        $("#form-add .button-add-item").addClass("hide");
    } else {
        $("#form-add .button-add-item").removeClass("hide");
    }
}

function listPoly(val) {
    loading_content("#form-add #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #polyclinic-group", "success");

                var item = data.data;

                $("#form-add select[name='polyclinic']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #polyclinic-group", "failed");

                $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-add #polyclinic-group", "failed");

            $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listServiceCategory(val) {
    loading_content("#form-add #service_category-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/service-category",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #service_category-group", "success");

                var item = data.data;

                $("#form-add select[name='service_category']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='service_category']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name='service_category']").select2({
                    placeholder: "{{ trans('messages.select_service_category') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='service_category']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #service_category-group", "failed");

                $("#form-add #service_category-group #loading-content").click(function(){ listServiceCategory(); });
            }
        },
        error: function(){
            loading_content("#form-add #service_category-group", "failed");

            $("#form-add #service_category-group #loading-content").click(function(){ listServiceCategory(); });
        }
    })
}

function listPatientCategory(val) {    
    loading_content("#form-add #patient_category-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #patient_category-group", "success");

                $("#section-cost").html('');

                var item = data.data;

                numPatientCategory = data.total;

                $("#item-cost select").html('');
                for (var i = 0; i < item.length; i++) {
                    $("#item-cost select").append('<option value='+item[i].id+'>'+item[i].name+'</option>');

                    if(item[i].default==1) {
                        defaultPatientCategory = item[i].id;
                    }
                }

                if(val!="") {
                    $("#item-cost select").val(val);
                }

                addCost();

            } else {
                loading_content("#form-add #patient_category-group", "failed");

                $("#form-add #patient_category-group #loading-content").click(function(){ listPatientCategory(val); });
            }
        },
        error: function(){
            loading_content("#form-add #patient_category-group", "failed");

            $("#form-add #patient_category-group #loading-content").click(function(){ listPatientCategory(val); });
        }
    })
}
$("#form-add select[name='code_bpjs']").select2({
    placeholder: "Pilih Layanan",
    allowClear: false
});

$('#form-add input[name=typeBpjs]').click(function() {  
    var value = $(this).val();
    listServiceBPJS(value);
});

function listServiceBPJS(val) {
    loading_content("#form-add #serviceBpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-layanan/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            ;


            if(!data.error) {
                loading_content("#form-add #serviceBpjs-group", "success");

                var item = data.list;

                $("#form-add select[name='code_bpjs']").attr("disabled", false);
                $("#form-add select[name='code_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='code_bpjs']").append("<option value='"+item[i].kdTindakan+"' data-name='"+item[i].nmTindakan+"'>"+item[i].kdTindakan+' - '+item[i].nmTindakan+"</option>");    
                }
                
                $("#form-add select[name='code_bpjs']").select2({
                    placeholder: "Pilih Layanan",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='code_bpjs']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #serviceBpjs-group", "failed");

                $("#form-add #serviceBpjs-group #loading-content").click(function(){ listServiceBPJS(val); });
            }
        },
        error: function(){
            loading_content("#form-add #serviceBpjs-group", "failed");

            $("#form-add #serviceBpjs-group #loading-content").click(function(){ listServiceBPJS(val); });
        }
    })
}

@if(Session::get('pcare'))
$("#form-add select[name='code_bpjs']").select2({
    placeholder: "Pilih Layanan",
    allowClear: false
});
@endif

$('#form-add input[name=typeBpjs]').click(function() {  
    var value = $(this).val();
    listServiceBPJS(value);
});

@if(Session::get('pcare'))
function listServiceBPJS(val) {
    loading_content("#form-add #serviceBpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-layanan/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #serviceBpjs-group", "success");

                var item = data.list;

                $("#form-add select[name='code_bpjs']").attr("disabled", false);
                $("#form-add select[name='code_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='code_bpjs']").append("<option value='"+item[i].kdTindakan+"' data-name='"+item[i].nmTindakan+"'>"+item[i].kdTindakan+' - '+item[i].nmTindakan+"</option>");    
                }
                
                $("#form-add select[name='code_bpjs']").select2({
                    placeholder: "Pilih Layanan",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='code_bpjs']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #serviceBpjs-group", "failed");

                $("#form-add #serviceBpjs-group #loading-content").click(function(){ listServiceBPJS(val); });
            }
        },
        error: function(){
            loading_content("#form-add #serviceBpjs-group", "failed");

            $("#form-add #serviceBpjs-group #loading-content").click(function(){ listServiceBPJS(val); });
        }
    })
}
@endif

listPoly('');
listServiceCategory('');
listPatientCategory('');

$('#pop-up').on('show.bs.modal', function (e) {
    $("#pop-up #form-pop-up input[name=name]").val("");
    resetValidation('pop-up #form-pop-up #name');
});

$("#pop-up #form-pop-up input[name=name]").focus(function() {
    resetValidation('pop-up #form-pop-up #name');
});

$("#pop-up #form-pop-up").submit(function(event) {
    event.preventDefault();
    resetValidation('popu-p #form-pop-up #name');
    $("#pop-up #form-pop-up button").attr("disabled", true);

    formData= new FormData();
    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("name", $("#pop-up #form-pop-up input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#pop-up #form-pop-up .btn-primary").addClass('loading');
    $("#pop-up #form-pop-up .btn-primary span").removeClass('hide');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/service-category",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#pop-up #form-pop-up button").attr("disabled", false);
            $("#pop-up #form-pop-up .btn-primary").removeClass('loading');
            $("#pop-up #form-pop-up .btn-primary span").addClass('hide');

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['pop-up #form-pop-up #name',data.errors.name, true]);
                } else {

                    $("#pop-up #form-pop-up input[name=name]").val("");
                    
                    notif(true,"{{ trans('validation.success_add_service_category') }}");

                    listServiceCategory($("#form-add select[name='service_category']").val());

                    $("#pop-up").modal("toggle");
                }

            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#pop-up #form-pop-up button").attr("disabled", false);

            $("#pop-up #form-pop-up .btn-primary").addClass('loading');
            $("#pop-up #form-pop-up .btn-primary span").removeClass('hide');

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#form-add #name").autocomplete({
    minLength: 0,
    source: function( request, response ) {
        var r = [];
        $.ajax({
            type: "GET",
            url: "{{ $api_url }}/{{ $lang }}/default-service?q="+request.term+"&per_page=5",
            beforeSend: function(){
                $("#form-add #loading-name").removeClass("hide");
            },
            success: function(dataSearch){
                $("#form-add #loading-name").addClass("hide");
                
                var itemSearch = dataSearch.data;

                for (var i = 0; i < itemSearch.length; i++) {
                    r.push({id:itemSearch[i].id,value:itemSearch[i].name,code:itemSearch[i].code,polyclinic:itemSearch[i].polyclinic,service_category:itemSearch[i].service_category});
                }

                response(r);
            },
            error: function(){
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {
        this.value = ui.item.value;

        $("#form-add #code").val(ui.item.code);
        return false;
    }
});


$('#form-add input[name="type_admin_cost"]').click(function() {    
    var checked = this.checked;
    if(checked==true) {
        $("#form-add #admin_cost_sec").removeClass('hide');
    } else {
        $("#form-add #admin_cost_sec").addClass('hide');
        $("#form-add input[name='admin_cost']").val('');
    }
});

$("#form-add #polyclinic-group .border-group").click(function() {
    resetValidation('form-add #polyclinic');
});

$('#form-add #service_category-group .border-group').click(function() {
    resetValidation('form-add #service_category');
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$('#form-add input[name=code]').focus(function() {
    resetValidation('form-add #code');
});

$("#form-add").submit(function(event) {
    resetValidation('form-add #polyclinic','form-add #service_category', 'form-add #name', 'form-add #code');

    var polyclinic = $("#form-add #polyclinic option:selected").val();
    var service_category = $("#form-add #service_category option:selected").val();
    var code = $("#form-add #code").val();
    var name = $("#form-add #name").val();

    if(name == ""){
        formValidate(true, ['form-add #name','{{ trans("messages.input_service_name") }}', true]);
    }else if(code == ""){
        formValidate(true, ['form-add #code','{{ trans("messages.input_service_code") }}', true]);
    }else if(polyclinic == "") {
        formValidate(true, ['form-add #polyclinic','{{ trans("validation.empty_polyclinic") }}', true]);
    }else if(service_category == ""){
        formValidate(true, ['form-add #service_category','{{ trans("messages.select_service_category") }}', true]);
    } else {
        formData= new FormData();

        var cost = [];
        var errorCost = false;
        var errorDefaultCost = true;
        
        
        $("#form-add input[name^='cost']").each(function(key, value) {
            cost[key] = this.value.replace(/\D/g, "");
            formData.append("cost_category["+key+"][1]", this.value.replace(/\D/g, ""));        
            if(this.value=="") {
                errorCost = true;
            }
        });

        var patient_category = [];
        $("#form-add select[name^='patient_category']").each(function(key, value) {
            formData.append("cost_category["+key+"][0]", this.value);
            patient_category.push(this.value);

            if(this.value==defaultPatientCategory) {
                formData.append("cost", cost[key]);
                errorDefaultCost = false;
            }
        });

        var sorted_patient_category = patient_category.slice().sort();
        for (var i = 0; i < patient_category.length - 1; i++) {
            if (sorted_patient_category[i + 1] == sorted_patient_category[i]) {
                errorCost = true;
            }
        }

        if(errorCost==true) {
            notif(false,"{{ trans('validation.invalid_cost') }}");
        }

        if(errorCost==false && errorDefaultCost==true) {
            notif(false,"{{ trans('validation.invalid_default_cost') }}");   
        }

        var errorAdminCost = false;
        var type_admin_cost = $('#form-add input[name="type_admin_cost"]').is(':checked');
        if(type_admin_cost==true) {
            if($("#form-add #admin_cost").val()=='' || $("#form-add #admin_cost").val()==0) {
                errorAdminCost = true;
            }
        }

        if(errorCost==false && errorDefaultCost==false && errorAdminCost==true) {
            notif(false,"{{ trans('validation.empty_admin_cost') }}");   
        }

        if(errorCost==false && errorDefaultCost==false && errorAdminCost==false) {
            event.preventDefault();
            $("#form-add button").attr("disabled", true);

            formData.append("validate", false);
            formData.append("level", "user");
            formData.append("user", "{{ $id_user }}");
            formData.append("id_clinic", "{{ $id_clinic }}");
            formData.append("id_polyclinic", $("#form-add #polyclinic option:selected").val());
            formData.append("id_service_category", $("#form-add #service_category option:selected").val());
            formData.append("name", $("#form-add #name").val());
            formData.append("code", $("#form-add #code").val());
            formData.append("admin_cost", $("#form-add #admin_cost").val());
            formData.append("note", nl2br($("#form-add #note").val()));
            @if(Session::get('pcare'))
            formData.append("kode_bpjs", ($("#form-add select[name=code_bpjs] option:selected").val() != undefined ? $("#form-add select[name=code_bpjs] option:selected").val() : ''));
            @else
            formData.append("kode_bpjs", '');
            @endif
            formData.append("type_kunjungan", ($("#form-add input[name=typeBpjs]:checked").val() != undefined ? $("#form-add input[name=typeBpjs]:checked").val() : ''));

            var implementer = $('#form-add input[name="implementer"]').is(':checked');
            if(implementer==true) {
                implementer = 1;
            } else {
                implementer = 0;
            }

            formData.append("implementer", implementer);

            $("#form-add .btn-primary").addClass("loading");
            $("#form-add .btn-primary span").removeClass("hide");

            $.ajax({
                url: "{{ $api_url }}/{{ $lang }}/service",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    
                    $("#form-add button").attr("disabled", false);
            
                    $("#form-add .btn-primary").removeClass("loading");
                    $("#form-add .btn-primary span").addClass("hide");

                    if(!data.error) {
                        if(!data.success) {
                            formValidate(true, ['form-add #polyclinic',data.errors.id_polyclinic, true], ['form-add #service_category',data.errors.id_service_category, true], ['form-add #name',data.errors.name, true], ['form-add #code',data.errors.code, true], ['form-add #typeBpjs',data.errors.type_layanan, true]);
                            if(data.message) {
                                notif(false,""+data.message+"");
                            }
                        } else {

                            $('#form-add input[name="type_admin_cost"]').prop('checked',false);
                            $('#form-add input[name="admin_cost"]').val("");
                            $('#form-add input[name="admin_cost"]').addClass("hide");
                            $("#form-add #polyclinic").select2("val", "");
                            $("#form-add #polyclinic").val("");
                            $("#form-add #service_category").select2("val", "");
                            $("#form-add #service_category").val("");
                            $("#form-add #name").val("");
                            $("#form-add #code").val("");
                            $("#form-add #note").val("");
                            $('#form-add input[name="implementer"]').prop('checked',false);

                            notif(true,"{{ trans('validation.success_add_service') }}");

                            costItem = 0;
                            $("#form-add #section-cost").html("");
                            addCost();

                            if(setup=="service") {
                                setup = "commission";
                                
                                $('#previous-step').removeClass('hide');
                                $('#step_1').addClass('hide');
                                $('#step_2').addClass('hide');
                                $('#step_3').addClass('hide');
                                $('#step_4').addClass('done');
                                $('#step_4').addClass('hide');
                                $('#step_5').removeClass('hide');
                                $('#step_5').removeClass('disable');
                                $('#step_6').addClass('hide');
                                $('#next-step').removeClass('hide');

                                setup_clinic('commission');

                                setTimeout(function(){
                                    loading('#/{{ $lang }}/service');
                                    redirect('#/{{ $lang }}/service');
                                }, 500);
                            } else {
                                setTimeout(function(){
                                    back();loading($(this).attr('href'))
                                }, 500);   
                            }
                    
                        }
                    } else {
                        notif(false,"{{ trans('validation.failed') }}");
                    }
                },
                error: function(){
                    $("#form-add button").attr("disabled", false);

                    $("#form-add .btn-primary").removeClass("loading");
                    $("#form-add .btn-primary span").addClass("hide");

                    notif(false,"{{ trans('validation.failed') }}");
                }
            })
        }
    }
});

// var name = $("#form-add select[name=code_bpjs]").val(); 
// $("#form-add #name").val(name).trigger("change");
@if(Session::get('pcare'))
$("#form-add select[name=code_bpjs]").on("change", function() {
    // var name = $('option:selected', this).a ttr('data-name');
    $("#form-add #name").val($('option:selected', this).attr('data-name'));
    $("#form-add #code").val($('option:selected', this).val());
});
@endif
</script>

@include('service.help')