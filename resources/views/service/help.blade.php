
<div class="modal fade window window-main" id="help-price" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <p><?php echo trans('messages.help_price'); ?></p>
                    <div class="form">
                        <div class="sf-check">
                            <label><input type="checkbox" name="hide_help"><span></span> {{ trans("messages.hide_help_message") }}</label>
                        </div>
                    </div>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
function info_help_price() {
    if(help_price==1) {
        $("#help-price").modal("show");
    }
}

$('#help-price').on('hidden.bs.modal', function (e) {
    help_price = 0;

    var hide_help = $('#help-price input[name="hide_help"]').is(':checked');
    if(hide_help==true) {
        formData= new FormData();

        formData.append("validate", "false");
        formData.append("help_price", "0");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/clinic/{{ $id_clinic }}/setting",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
              

              if(!data.error) {

                  if(!data.success) {
                  } else {                
                  }
              } else {
              }
          },
          error: function(){
          }
        })
    }
});
    
</script>