@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-10">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        Pendaftaran
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">                    
                    <form role="form" class="sec" id="form-add">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Jenis Kunjungan</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="kunjungan sakit" name="type"><span></span> Kunjungan Sakit</label>
                                        <label><input type="radio" value="kunjungan sehat" name="type"><span></span> Kunjungan Sehat</label>
                                    </div>
                                </div>
                                <div class="row" style="display: none;" id="sickVisit">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Jenis Perawatan <span>*</span></label>
                                            <select class="form-control" name="type_kunjungan_sakit">
                                                <option value="">Pilih Jenis Perawatan</option>
                                                <option value="outpatient">Rawat Jalan</option>
                                                <option value="inpatient">Rawat Inap</option>
                                                <option value="promotif preventif">Promotif Preventif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="display: none;" id="healthyVisit">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Jenis Perawatan <span>*</span></label>
                                            <select class="form-control" name="type_kunjungan_sehat">
                                                <option value="">Pilih Jenis Perawatan</option>
                                                <option value="outpatient">Rawat Jalan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="kegiatan-group">
                                            <label class="control-label">{{trans('messages.activities')}} <span>*</span></label>
                                            <select class="form-control" name="kegiatan">
                                                <option value="">{{trans('messages.select_type_activity')}}</option>
                                                <option value="homevisit">Home Visit</option>
                                                <option value="konselling">Konselling</option>
                                                <option value="imunisasi">Imunisasi</option>
                                            </select>
                                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="group-label first-label">{{ trans('messages.patient') }}</h3>
                                <div class="form-group content-column" id="name-group">
                                    <label class="control-label">{{ trans('messages.patient') }} <span>*</span></label>
                                    <div class="border-group">
                                        <input name="name" placeholder= "{{ trans('messages.select_patient') }}" type="text" class="form-control" data-id="" />
                                    </div>
                                    <span id="loading-name" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.mrn') }}</label>
                                    <input type="text" class="form-control" placeholder="<?php echo trans('messages.mrn'); ?>" name="mrn" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.age') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.age') }}" name="age" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.gender') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="0" name="gender" disabled="disabled"><span></span> {{ trans('messages.male') }}</label>
                                        <label><input type="radio" value="1" name="gender" disabled="disabled"><span></span> {{ trans('messages.female') }}</label>
                                    </div>
                                </div>
                                <div class="form-group" id="smoke-group">
                                    <label class="control-label">{{ trans('messages.smoking') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="1" name="smoke" disabled="disabled"><span></span> {{ trans('messages.smoking') }}</label>
                                        <label><input type="radio" value="0" name="smoke" disabled="disabled"><span></span> {{ trans('messages.no_smoking') }}</label>
                                    </div>
                                </div>
                                <div class="form-group no-arrow">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">{{ trans('messages.blood_type') }}</label>
                                            <select class="form-control" name="blood_type" disabled="disabled">
                                                <option value="">{{ trans('messages.blood_type') }}</option>
                                                <option value="A">{{ trans('messages.A') }}</option>
                                                <option value="B">{{ trans('messages.B') }}</option>
                                                <option value="AB">{{ trans('messages.AB') }}</option>
                                                <option value="O">{{ trans('messages.O') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.address') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.address') }}" name="address" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.email') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.email') }}" name="email" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.phone_number') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.phone_number') }}" name="phone" disabled="disabled">
                                </div>
                                <div class="form-group no-arrow">
                                    <label class="control-label">{{ trans('messages.patient_category') }}</label>
                                    <select class="form-control" name="patient_category" disabled="disabled">
                                        <option value=""></option>
                                    </select>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.financing') }}</label>
                                    <select class="form-control" name="financing">
                                        <option value="">{{ trans('messages.financing') }}</option>
                                        <option value="personal">{{ trans('messages.personal') }}</option>
                                        <option value="agency">{{ trans('messages.agency') }}</option>
                                        <option value="insurance">{{ trans('messages.insurance') }}</option>
                                    </select>
                                </div>
                                <div class="form-group content-column hide" id="insurance-group">
                                    <label class="control-label">{{ trans('messages.insurance') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="insurance">
                                            <option value="">{{ trans('messages.select_insurance') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="row hide" id="no_bpjs-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="card_number-group">
                                            <label class="control-label">Nomor Kartu <span>*</span></label>
                                            <input type="text" class="form-control" placeholder="Masukkan Nomor Kartu" name="card_number">
                                            <span class="help-block"></span>
                                            <small class="text-success hide">Kartu dapat digunakan</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <a id="verification" class="link-add link-label">{{trans('messages.verification')}}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="group-label first-label">{{ trans('messages.doctor') }}</h3>
                                <div class="form-group content-column" id="polyclinic-group">
                                    <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic" disabled>
                                            <option value="">{{ trans('messages.select_polyclinic') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column" id="doctor-group">
                                    <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="doctor" disabled="disabled">
                                            <option value="">{{ trans('messages.no_doctor') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column hide" id="schedule-group">
                                    <label class="control-label">{{ trans('messages.practice_hour') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="schedule" disabled="disabled">
                                            <option value="">{{ trans('messages.no_schedule') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column date-column condition-group hide" id="appointment-group">
                                    <div class="sf-check">
                                        <label><input type="checkbox" name="set_appointment" checked=false><span></span> {{ trans('messages.set_appointment') }}</label>
                                    </div>
                                    <div id="timepicker_time" class="input-group date">
                                        <input  class="form-control" data-format="HH:mm" type="text" placeholder="{{ trans('messages.input_time') }}" name="appointment" value="" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-hour.png') }}" />
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column date-column date-disabled hide" id="date-group">
                                    <label class="control-label">{{ trans('messages.date') }} <span>*</span></label>
                                    <div id="date-section">
                                        <div id="datepicker_date" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="date" disabled="disabled" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                                <div class="form-group content-column date-column hide" id="date_inap-group">
                                    <label class="control-label">{{ trans('messages.date') }} <span>*</span></label>
                                    <div id="date-section-inap">
                                        <div id="datepicker_date-inap" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="date_inap" disabled="disabled" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group last-item">
                                    <button class="btn btn-primary">
                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        {{ ucwords(trans('messages.save')) }}
                                    </button>
                                    <a href="#/{{ $lang }}/schedule" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                        {{ trans('messages.cancel') }}
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

let registration_type = "";
let activity = "";
let polyclinic = "";

$("#datepicker_date input[name='date']").keypress(function(event) {event.preventDefault();});
$("#datepicker_date-inap input[name='date_inap']").keypress(function(event) {event.preventDefault();});

$("#form-add select[name='blood_type']").select2({
    placeholder: "{{ trans('messages.blood_type') }}",
    allowClear: false
});

$("#form-add select[name='patient_category']").select2({
    placeholder: "{{ trans('messages.patient_category') }}",
    allowClear: false
});

$("#form-add select[name='financing']").select2({
    placeholder: "{{ trans('messages.financing') }}",
    allowClear: false
});

$("#form-add select[name='insurance']").select2({
    placeholder: "{{ trans('messages.insurance') }}",
    allowClear: false
});

$("#form-add select[name=polyclinic]").select2({
    placeholder: "{{ trans('messages.select_polyclinic') }}",
    allowClear: false
});

$("#form-add select[name=doctor]").select2({
    placeholder: "{{ trans('messages.no_doctor') }}",
    allowClear: false
});

$("#form-add select[name=schedule]").select2({
    placeholder: "{{ trans('messages.no_schedule') }}",
    allowClear: false
});

$("#form-add select[name=type_kunjungan_sakit]").select2({
    placeholder: "Tidak ada jenis perawatan",
    allowClear: false
});

$("#form-add select[name=type_kunjungan_sehat]").select2({
    placeholder: "Tidak ada jenis perawatan",
    allowClear: false
});

$("#form-add select[name=kegiatan]").select2({
    placeholder: "Tidak ada kegiatan",
    allowClear: false
});

$("#form-add input[name='type']:radio").change(function(e) {
    registration_type = e.target.value;
    activity = "";
    
    if(e.target.value == 'kunjungan sakit'){
        $('#form-add').find("#sickVisit").show();
        $('#form-add').find("#healthyVisit").hide();
        $('#form-add').find("#schedule-group").addClass("hide").removeClass("show");
        $('#form-add').find("#date-group").addClass("hide").removeClass("show");
        $('#form-add').find("#date_inap-group").addClass("show").removeClass("hide");
        $("#form-add select[name='type_kunjungan_sakit']").val("").trigger("change");
    }else if(e.target.value == 'kunjungan sehat'){
        $('#form-add').find("#sickVisit").hide();
        $('#form-add').find("#healthyVisit").show();
        $('#form-add').find("#schedule-group").addClass("show").removeClass("hide");
        $('#form-add').find("#date-group").addClass("show").removeClass("hide");
        $('#form-add').find("#date_inap-group").addClass("hide").removeClass("show");
        $("#form-add select[name='type_kunjungan_sehat']").val("").trigger("change");
        $("#form-add select[name='kegiatan']").val("").trigger("change");
    }
    listPoly();
});

$('#form-add select[name=type_kunjungan_sakit]').on('change', function(e) {
    if(this.value == 'inpatient'){
        $('#form-add').find("#schedule-group").addClass("hide").removeClass("show");
        $('#form-add').find("#date-group").addClass("hide").removeClass("show");
        $('#form-add').find("#date_inap-group").addClass("show").removeClass("hide");
        $('#form-add').find("#datepicker_date-inap").datetimepicker({
            locale: '{{ $lang }}',
            format: 'DD/MM/YYYY'
        });
    }else{
        $('#form-add').find("#schedule-group").addClass("show").removeClass("hide");
        $('#form-add').find("#date-group").addClass("show").removeClass("hide");
        $('#form-add').find("#date_inap-group").addClass("hide").removeClass("show");
        $("#form-add input[name='date_inap']").val("");
        $("#form-add input[name='date_inap']").prop("disabled", true);
    }
});

let timer = ""
$('#timepicker_time').datetimepicker({format: 'HH:mm'});
$("#form-add input[name='name']").autocomplete({
    minLength: 3,
    source: function( request, response ) {
        $("#form-add #loading-name").removeClass("hide");
        clearTimeout(timer);
        let temp = [];
        timer = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/clinic/patient?active=true&q="+request.term+"&per_page=10",
                success: function(results){
                    $("#form-add #loading-name").addClass("hide");
                    for(i = 0; i < results.data.length; i++) {
                        let patient = results.data[i].name;
                        temp.push({id:results.data[i].id, name:results.data[i].name, value: patient})
                    }
                    response(temp);
                },
                error: function() {
                    $("#form-add #loading-name").addClass("hide");
                    response(temp);
                }
            });
            
        }, 2500);
    },
    focus: function() {
        return false;
    },
    select: function( event, ui) {
        $("#form-add input[name='name']").val(ui.item.name);
        $("#form-add input[name='name']").data("id", ui.item.id);
        $("#form-add select[name='polyclinic']").val("").trigger("change");
        formValidate(true, ['form-add #card_number', '', true]);
        getPatient(ui.item.id);
        return false;
    }
});

function listInsurance(val) {
    loading_content("#form-add #insurance-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsurance){
            


            if(!dataInsurance.error) {
                loading_content("#form-add #insurance-group", "success");

                var item = dataInsurance.data;

                $("#form-add select[name='insurance']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='insurance']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name='insurance']").select2({
                    placeholder: "{{ trans('messages.select_insurance') }}",
                    allowClear: false
                });

                
            } else {
                loading_content("#form-add #insurance-group", "failed");

                $("#form-add #insurance-group #loading-content").click(function(){ listInsurance(val); });
            }
        },
        error: function(){
            loading_content("#form-add #insurance-group", "failed");

            $("#form-add #insurance-group #loading-content").click(function(){ listInsurance(val); });
        }
    })
}

listInsurance('');

$('#form-add select[name=financing]').on('change', function() {
    var financing = this.value;

    resetValidation('form-add #insurance');

    if(financing=='insurance') {
        $("#form-add select[name='insurance']").val("").trigger("change");
        $("#form-add #insurance-group").removeClass("hide");
        $("#form-add #no_bpjs-group").addClass("hide");
        @if(Session::get('pcare'))
        $("#form-add button").attr("disabled", true);
        @endif
    } else {
        $("#form-add #insurance-group").addClass("hide");
        $("#form-add #no_bpjs-group").addClass("hide");
        $("#form-add button").attr("disabled", false);
    }
});

$('#form-add select[name=insurance]').on('change', function() {
    var insurance = this.selectedOptions[0].label;
    
    if(insurance.toLowerCase().includes("bpjs")){
        $("#form-add #no_bpjs-group").removeClass("hide");
        @if(Session::get('pcare'))
        $("#form-add button").attr("disabled", true);
        @endif
    }else{
        $("#form-add #no_bpjs-group").addClass("hide");
        $("#form-add button").attr("disabled", false);
        $("#form-add input[name=card_number]").val("");
    }
});

$("#form-add input[name=card_number]").on('keyup', function() {
    formValidate(true, ['form-add #card_number', '', true]);
});


$('#form-add #verification').on('click', function() {
    var id = $("#form-add input[name='card_number']").val();
    if(!id) {
        notif(false, "Nomor Kartu tidak boleh kosong");
        return;
    }
    
    @if(Session::get('pcare'))
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(data.data){
                if(!data.error) {
                    if(data.success==true) {
                        if(data.data.aktif==false){
                            formValidate(true, ['form-add #card_number', 'Nomor tidak valid', true]);
                            $("#form-add small.text-success").addClass("hide");
                            $("#form-add button").attr("disabled", true);
                        }else{
                            if(data.data.tunggakan > 0){
                                formValidate(true, ['form-add #card_number', 'Anda memliki tunggakan sebesar Rp.'+data.data.tunggakan, true]);
                                $("#form-add small.text-success").addClass("hide");
                                $("#form-add button").attr("disabled", true);
                            }else{
                                formValidate(true, ['form-add #card_number', '', true]);
                                $("#form-add small.text-success").removeClass("hide");
                                $("#form-add button").attr("disabled", false);
                            }
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            } else if (!data.error && !data.success && data.message) {
                notif(false, data.message);
            } else {
                formValidate(true, ['form-add #card_number', 'Nomor tidak valid', true]);
                $("#form-add small.text-success").addClass("hide");
                $("#form-add button").attr("disabled", true);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    @else
    notif(false, 'Belum terhubung dengan PCare');
    @endif
});


function listPoly(val) {
    var is_bpjs = false;
    var type = "";
    
    if($("#form-add input[name='type']:checked").val() == "kunjungan sehat"){
        type = "kunjungan_sehat"
    }else if($("#form-add input[name='type']:checked").val() == "kunjungan sakit"){
        type = "kunjungan_sakit"
    }

    if($("#form-add input[name='card_number']").val() != ""){
        @if(Session::get('pcare'))
        is_bpjs = true;
        @else
        is_bpjs = false;
        @endif
    }else{
        is_bpjs = false;
    }

    loading_content("#form-add #polyclinic-group", "loading");
    loading_content("#form-add #kegiatan-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic?is_bpjs="+is_bpjs+"&type="+type,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            


            if(!dataPoly.error) {
                loading_content("#form-add #polyclinic-group", "success");
                loading_content("#form-add #kegiatan-group", "success");

                var item = dataPoly.data;

                $("#form-add select[name='polyclinic']").html("<option value=''></option> ");
                $("#form-add select[name='kegiatan']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='kegiatan']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    $("#form-add select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                if(registration_type === 'kunjungan sehat') {
                    $("#form-add select[name='kegiatan']").val(activity);
                    $("#form-add select[name='polyclinic']").val(polyclinic);
                }


                $("#form-add select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                $("#form-add select[name='kegiatan']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });
                
            } else {
                loading_content("#form-add #polyclinic-group", "failed");
                loading_content("#form-add #kegiatan-group", "failed");

                $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
                $("#form-add #kegiatan-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-add #polyclinic-group", "failed");
            loading_content("#form-add #kegiatan-group", "failed");

            $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            $("#form-add #kegiatan-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function resetSchedule() {
    $("#form-add #appointment-group").addClass("hide");
    $("#form-add input[name='set_appointment']").prop("checked",false);
    $("#form-add input[name='appointment']").prop("disabled",true);
    $("#form-add input[name='appointment']").val("");
    $("#form-add input[name='date']").prop("disabled", true);
    $("#form-add input[name='date']").val("");
    $("#form-add input[name='date_inap']").prop("disabled", true);
    $("#form-add input[name='date_inap']").val("");
}

function listDoctor(val) {

    resetSchedule();

    // don't request when val (id_polyclinic) is empty
    if(!val) {
        return;
    }


    loading_content("#form-add #doctor-group", "loading");
    
    var is_bpjs = false;

    if($("#form-add input[name='card_number']").val() != ""){
        @if(Session::get('pcare'))
        is_bpjs = true;
        @else
        is_bpjs = false;
        @endif
    }else{
        is_bpjs = false;
    }
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+val+"&active=true&is_bpjs="+is_bpjs,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #doctor-group", "success");

                $("#form-add select[name=schedule]").html("");
                $("#form-add select[name=schedule]").prop("disabled", true);
                $("#form-add select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                $("#form-add select[name=schedule]").select2({
                    placeholder: "{{ trans('messages.no_schedule') }}",
                    allowClear: false
                });

                var item = data.data;

                $("#form-add select[name=doctor]").html("");

                if(item.length==0 || val=="") {
                    $("#form-add select[name=doctor]").prop("disabled", true);
                    $("#form-add select[name=doctor]").append("<option value=''>{{ trans('messages.no_doctor') }}</option>");
                    $("#form-add select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.no_doctor') }}",
                        allowClear: false
                    });
                } else {
                    $("#form-add select[name=doctor]").prop("disabled", false);

                    $("#form-add select[name=doctor]").append("<option value=''>{{ trans('messages.select_doctor') }}</option>");

                    for (var i = 0; i < item.length; i++) {

                        $("#form-add select[name=doctor]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-add select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}",
                        allowClear: false
                    });
                }
            } else {
                loading_content("#form-add #doctor-group", "failed");

                $("#form-add #doctor-group #loading-content").click(function(){ listDoctor(val); });
            }
             
        },
        error: function(){
            loading_content("#form-add #doctor-group", "failed");

            $("#form-add #doctor-group #loading-content").click(function(){ listDoctor(val); });
        }
    });
}

$('#form-add select[name=polyclinic]').on('change', function() {
    polyclinic = this.value;
    
    listDoctor(polyclinic);
});

$('#form-add select[name=kegiatan]').on('change', function() {
    activity = this.value;
    $("#form-add select[name='polyclinic']").val(this.value).trigger("change");    
});

var doctorEndDate = "31-12-9999";
var doctorStartLeaveDate = "";
var doctorStartEndLeaveDate = "";

function listSchedule(val) {
    resetSchedule();

    doctorEndDate = "9999-12-31";
    doctorStartLeaveDate = "";
    doctorStartEndLeaveDate = "";

    if(val!="") {
        loading_content("#form-add #schedule-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+val+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                

                if(!data.error) {
                    loading_content("#form-add #schedule-group", "success");
                    if(data.schedule!=null) {
                        item = data.schedule.daily;

                        //doctorEndDate = data.schedule.end_date_active;
                        doctorStartLeaveDate = data.schedule.start_date_leave;
                        doctorEndLeaveDate = data.schedule.end_date_leave;

                        $("#form-add select[name=schedule]").html("");

                        if(item.length==0) {
                            $("#form-add select[name=schedule]").prop("disabled", true);
                            $("#form-add select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                            $("#form-add select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.no_schedule') }}"
                            });
                        } else {
                            $("#form-add select[name=schedule]").prop("disabled", false);
                            $("#form-add select[name=schedule]").append("<option value=''></option>");

                            var checkSchedule = false;
                            for (var i = 0; i < item.length; i++) {
                                $("#form-add select[name=schedule]").append("<option data-day='"+item[i].day+"' data-appointment='"+item[i].appointment+"' value='"+item[i].id+"'>"+getDay(item[i].day)+" ("+convertTime(item[i].start_time)+" - "+convertTime(item[i].end_time)+")</option>");
                            }

                            $("#form-add select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.select_practice_hour') }}"
                            });
                            
                        }

                    } else {
                        $("#form-add select[name=schedule]").html("");
                        $("#form-add select[name=schedule]").prop("disabled", true);
                        $("#form-add select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                        $("#form-add select[name=schedule]").select2({
                            placeholder: "{{ trans('messages.no_schedule') }}"
                        });
                    }
                } else {
                    loading_content("#form-add #schedule-group", "failed");

                    $("#form-add #schedule-group #loading-content").click(function(){ listSchedule(val); });
                }
                
            },
            error: function(){
                loading_content("#form-add #schedule-group", "failed");

                $("#form-add #schedule-group #loading-content").click(function(){ listSchedule(val); });
            }
        });
    } else {
        $("#form-add select[name=schedule]").html("");
        $("#form-add select[name=schedule]").prop("disabled", true);
        $("#form-add select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
        $("#form-add select[name=schedule]").select2({
            placeholder: "{{ trans('messages.no_schedule') }}"
        });
    }
}

$('#form-add select[name=doctor]').on('change', function() {
    var doctor = this.value;

    listSchedule(doctor);
    $("#form-add input[name='date_inap']").prop("disabled", false);
    $("#datepicker_date-inap").datetimepicker({
        locale: '{{ $lang }}',
        format: 'DD/MM/YYYY'
    });
});

function listDate(val, appointment) {
    const set_appointment = $("#form-add input[name='set_appointment']").is(':checked');

    if(val!="") {

        if(appointment==1) {
            
            @if(Session::get('pcare'))
                $("#form-add #appointment-group").removeClass("hide");
                $("#form-add input[name='set_appointment']").prop("checked",set_appointment);
                $("#form-add input[name='appointment']").prop("disabled",false);
                $("#form-add input[name='appointment']").val("");
            @else
                if(type_patient == false){
                    $("#form-add #appointment-group").removeClass("hide");
                    $("#form-add input[name='set_appointment']").prop("checked",set_appointment);
                    $("#form-add input[name='appointment']").prop("disabled",false);
                    $("#form-add input[name='appointment']").val("");
                }
            @endif
        } else {
            $("#form-add #appointment-group").addClass("hide");
            $("#form-add input[name='set_appointment']").prop("checked",false);
            $("#form-add input[name='appointment']").prop("disabled",true);
            $("#form-add input[name='appointment']").val("");
        }

        if(val==7) {
            val=0;
        }

        var dateDay = [];
        for(i=0;i<7;i++) {
            if(i!=val) {
                dateDay.push(i);
            }
        }

        $("#form-add #date-section").html('<div id="datepicker_date" class="input-group date">'+
                                            '<input class="form-control" type="text" placeholder=\'{{ trans("messages.select_date") }}\' name="date" disabled="disabled" />'+
                                            '<span class="input-group-addon">'+
                                                '<img src=\'{{ asset("assets/images/icons/action/select-date.png") }}\' />'+
                                            '</span>'+
                                        '</div>');

        $("#form-add input[name='date']").prop("disabled", false);

        var date = new Date();
        date.setDate(date.getDate());

        var min_date = new Date();
        min_date.setDate(min_date.getDate() - 1);

        var disabled_dates = [];

        if(doctorStartLeaveDate!="0000-00-00" && doctorEndLeaveDate!="0000-00-00") {

            var start_min_date = new Date();
            start_min_date.setDate(start_min_date.getDate()); 

            var end_min_date = new Date();
            end_min_date.setDate(end_min_date.getDate() - 1);  

            for (var d = new Date(doctorStartLeaveDate.replace(/\-/g,'/')); d <= new Date(doctorEndLeaveDate.replace(/\-/g,'/')); d.setDate(d.getDate()+1)) {

                monthLoop = ((d.getMonth()+1)+100).toString().slice(-2);
                dayLoop = (d.getDate()+100).toString().slice(-2);
                dateLoop = d.getFullYear()+'/'+monthLoop+'/'+dayLoop;
                disabled_dates.push(dateLoop);

                /*monthDate = ((date.getMonth()+1)+100).toString().slice(-2);
                dayDate = (date.getDate()+100).toString().slice(-2);
                dateNow = date.getFullYear()+'/'+monthDate+'/'+dayDate;

                if(Date.parse(dateNow)==Date.parse(dateLoop)) {
                    date.setDate(date.getDate());
                }*/

                if(Date.parse(start_min_date)>=Date.parse(new Date(doctorStartLeaveDate.replace(/\-/g,'/'))) && Date.parse(end_min_date)<=Date.parse(new Date(doctorEndLeaveDate.replace(/\-/g,'/')))) {
                    if(Date.parse(min_date)<=Date.parse(new Date(dateLoop))) {
                        //min_date.setDate(new Date(dateLoop).getDate()+1);
                        min_date.setDate(new Date(dateLoop).getDate());
                    }
                }

            }
        }

        disabled_dates.push(min_date);

        $('#datepicker_date').datetimepicker({
            locale: '{{ $lang }}',
            minDate: min_date,
            maxDate: doctorEndDate.replace(/\-/g,'/'),
            format: 'DD/MM/YYYY',
            daysOfWeekDisabled: dateDay,
            disabledDates: disabled_dates
        });


    } else {
        $("#form-add input[name='date']").prop("disabled", true);
        $("#form-add input[name='date']").val("");
    }
}

$('#form-add select[name=schedule]').on('change', function() {
    var schedule = $(this).find(':selected').data('day');
    var appointment = $(this).find(':selected').data('appointment');
    
    listDate(schedule, appointment);
});

var type_patient = false;

function getPatient(val) {
    if(val!="") {
        loading_content("#form-add #name-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/patient/"+val+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(dataPatient){

                if(!dataPatient.error) {
                    loading_content("#form-add #name-group", "success");
                    
                    if(dataPatient.financing == 'insurance' && dataPatient.card_number != null){
                        type_patient = true;
                    }else{
                        type_patient = false;
                    }

                    $("#form-add input[name='mrn']").val(dataPatient.mrn);
                    $("#form-add input[name='age']").val(getAge(dataPatient.birth_date));
                    $("#form-add input[name='gender'][value=" + dataPatient.gender + "]").prop('checked', true);
                    $("#form-add input[name='smoke'][value=" + dataPatient.smoke + "]").prop('checked', true);
                    $("#form-add select[name='blood_type']").val(dataPatient.blood_type).trigger("change");
                    $("#form-add input[name='address']").val(nullToEmpty(dataPatient.address)+' '+nullToEmpty(dataPatient.village.name)+' '+nullToEmpty(dataPatient.district.name)+' '+nullToEmpty(dataPatient.regency.name)+' '+nullToEmpty(dataPatient.province.name)+' '+nullToEmpty(dataPatient.country.name));
                    $("#form-add input[name='email']").val(dataPatient.email);
                    $("#form-add input[name='phone']").val("+"+dataPatient.phone_code+""+dataPatient.phone);
                    $("#form-add select[name='financing']").val(dataPatient.financing).trigger("change");
                    $("#form-add select[name='insurance']").val(dataPatient.id_insurance).trigger("change");
                    $("#form-add input[name='card_number']").val(dataPatient.card_number);

                    $("#form-add select[name='patient_category']").append("<option value=''></option>");

                    try {
                        $("#form-add select[name='patient_category']").append("<option value='"+dataPatient.id_patient_category+"' selected='selected'>"+dataPatient.patient_category.name+"</option>");
                    } catch(err) {}

                    $("#form-add select[name=patient_category]").select2({
                        placeholder: "{{ trans('messages.patient_category') }}"
                    });

                    listPoly('');

                } else {
                    loading_content("#form-add #name-group", "failed");

                    $("#form-add #name-group #loading-content").click(function(){ getPatient(val); });
                }
            },
            error: function() {
                loading_content("#form-add #name-group", "failed");

                $("#form-add #name-group #loading-content").click(function(){ getPatient(val); });
            }
        });
    } else {
        $("#form-add input[name='mrn']").val("");
        $("#form-add input[name='age']").val("");
        $('#form-add input[name="gender"]:checked').each(function(){
            $(this).prop('checked', false); 
        });
        $('#form-add input[name="smoke"]:checked').each(function(){
            $(this).prop('checked', false); 
        });
        $("#form-add select[name='blood_type']").val("").trigger("change");
        $("#form-add input[name='address']").val("");
        $("#form-add input[name='phone']").val("");
        $("#form-add select[name='patient_category']").val("").trigger("change");
        $("#form-add select[name='financing']").val("").trigger("change");
    }
}

$('#form-add input[name="set_appointment"]').click(function() {    
    var checked = this.checked;
    if(checked==true) {
        $("#form-add input[name='appointment']").prop("disabled",false);
        $("#form-add input[name='appointment']").val("");
    } else {
        $("#form-add input[name='appointment']").prop("disabled",true);
        $("#form-add input[name='appointment']").val("");
    }
});

$('#form-add input[name=type]').click(function() {
    resetValidation('form-add #type');
});

$("#form-add #name-group .border-group").click(function() {
    resetValidation('form-add #name');
});

$("#form-add #insurance-group .border-group").click(function() {
    resetValidation('form-add #insurance');
});

$("#form-add #polyclinic-group .border-group").click(function() {
    resetValidation('form-add #polyclinic');
});

$("#form-add #doctor-group .border-group").click(function() {
    resetValidation('form-add #doctor');
});

$("#form-add #schedule-group .border-group").click(function() {
    resetValidation('form-add #schedule');
});

$("#form-add #date-group").click(function() {
    resetValidation('form-add #date');
});

$('#form-add input[name="set_appointment"]').click(function() {
    resetValidation('form-add #appointment');
});

$('#form-add input[name="appointment"]').focus(function() {
    resetValidation('form-add #appointment');
});

$("#form-add").submit(function(event) {
    resetValidation('form-add #name', 'form-add #insurance', 'form-add #polyclinic', 'form-add #doctor', 'form-add #schedule', 'form-add #date', 'form-add #type');

    event.preventDefault();
    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    var type = $("#form-add input[name='type']:checked").val();
    if(type == 'kunjungan sakit'){
        var type_kunjungan = $("#form-add select[name='type_kunjungan_sakit'] option:selected").val();
        activity = "";

        formData.append("type_kunjungan", type_kunjungan);

        if(type_kunjungan == 'inpatient'){
            var date = $("#form-add input[name='date_inap']").val();
            if(date!="") {
                date = convertDate(date);
            }
        }else{
            var date = $("#form-add input[name='date']").val();
            if(date!="") {
                date = convertDate(date);
            }
        }
    }else if(type == 'kunjungan sehat'){
        formData.append("type_kunjungan", $("#form-add select[name='type_kunjungan_sehat'] option:selected").val());
        formData.append("kegiatan", $("#form-add select[name='kegiatan'] option:selected").val(activity));

        var date = $("#form-add input[name='date']").val();
        if(date!="") {
            date = convertDate(date);
        }
    }

    var appointment = $("#form-add input[name='appointment']").val();

    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("type", type);
    formData.append("id_patient", $("#form-add input[name='name']").data("id"));
    formData.append("id_polyclinic", $("#form-add select[name='polyclinic'] option:selected").val());
    formData.append("id_doctor", $("#form-add select[name='doctor'] option:selected").val());
    formData.append("id_doctor_daily_schedule", $("#form-add select[name='schedule'] option:selected").val());
    formData.append("appointment", appointment);


    var financing = $("#form-add select[name='financing'] option:selected").val();
    formData.append("financing", financing);

    if(financing=="insurance") {
        formData.append("insurance", $("#form-add select[name='insurance'] option:selected").val());
        formData.append("card_number", $("#form-add input[name='card_number']").val());
    }

    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    formData.append("date", date);

    var errorAppointment = false;
    if($("#form-add select[name='schedule']").val()!="" && $("#form-add input[name='set_appointment']").is(':checked') && $("#form-add input[name='appointment']").val()=="") {
        formData.append("validate", true);

        formValidate(true, ['form-add #appointment','{{ trans("validation.empty_appointment") }}', true]);

        errorAppointment = true;
    }


    var errorTimeNow = false;

    if(date!='' && appointment!='' && $("#form-add input[name='set_appointment']").is(':checked')) {

        newDate = new Date();
        monthNow = ((newDate.getMonth()+1)+100).toString().slice(-2);
        dayNow = (newDate.getDate()+100).toString().slice(-2);
        yearNow = newDate.getFullYear();
        hoursNow = (newDate.getHours()+100).toString().slice(-2);
        minutesNow = (newDate.getMinutes()+100).toString().slice(-2);
        secondsNow = (newDate.getSeconds()+100).toString().slice(-2);
        dateTimeNow = yearNow+'/'+monthNow+'/'+dayNow+' '+hoursNow+':'+minutesNow+':'+secondsNow+'';

        splitDateForm = date.split("-");
        dateForm = splitDateForm[0]+'/'+splitDateForm[1]+'/'+splitDateForm[2]; 
        dateTimeForm = dateForm+' '+appointment+':00';

        if(Date.parse(dateTimeNow)>Date.parse(dateTimeForm)) {
            
            notif(false,'{{ trans("validation.invalid_time_now_appointment") }}');

            $("#form-add button").attr("disabled", false);

            errorTimeNow = true;
        }
    }

    if(errorTimeNow==false) {

        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/schedule",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                
                $("#form-add button").attr("disabled", false);
        
                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                if(!data.error) {
                    if(!data.success) {

                        if(data.message) {
                            notif(false,""+data.message+"");
                        }

                        if($("#form-add input[name='name'] option:selected").val()=='') {
                            formValidate(true, ['form-add #name','{{ trans("validation.empty_patient") }}', true], ['form-add #insurance',data.errors.insurance, true], ['form-add #polyclinic',data.errors.id_polyclinic, true], ['form-add #doctor',data.errors.id_doctor, true], ['form-add #schedule',data.errors.id_doctor_daily_schedule, true], ['form-add #date',data.errors.date, true], ['form-add #type',data.errors.type, true]);
                        } else {
                            formValidate(true, ['form-add #name','', true], ['form-add #insurance',data.errors.insurance, true], ['form-add #polyclinic',data.errors.id_polyclinic, true], ['form-add #doctor',data.errors.id_doctor, true], ['form-add #schedule',data.errors.id_doctor_daily_schedule, true], ['form-add #date',data.errors.date, true], ['form-add #type',data.errors.type, true]);
                        }
                        

                    } else {
                        if(errorAppointment==false) {

                            notif(true,'Pendaftaran berhasi ditambahkan!');

                            if(help_schedule==1) {
                                if(appointment=="") {
                                    $('#help-schedule input[name="id"]').val(data.id);
                                    info_help_schedule(
                                        data.doctor.name, 
                                        data.polyclinic.name, 
                                        data.clinic.name, 
                                        data.date, 
                                        data.start_time, 
                                        data.end_time, 
                                        data.number, 
                                        data.estimate
                                    );
                                } else {
                                    $('#help-schedule_appointment input[name="id"]').val(data.id);
                                    info_help_schedule_appointment(
                                        data.doctor.name, 
                                        data.patient.name, 
                                        data.polyclinic.name, 
                                        data.clinic.name, 
                                        data.date, 
                                        data.appointment
                                    );
                                }
                            } else {
                                setTimeout(function(){
                                    loading('#/{{ $lang }}/schedule/'+data.id);
                                    redirect('#/{{ $lang }}/schedule/'+data.id);                
                                },500);
                            }
                        }
                
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        })
    }
});

</script>

@include('schedule.help')