
<div class="modal fade window window-main" id="help-inpatient" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <span><?php echo trans('messages.help_inpatient'); ?></span>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade window window-main" id="help-schedule" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <input type="hidden" name="id" />
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <span id="help-schedule-text"><?php echo trans('messages.help_schedule'); ?></span>
                    <div class="form">
                        <div class="sf-check">
                            <label><input type="checkbox" name="hide_help"><span></span> {{ trans("messages.hide_help_message") }}</label>
                        </div>
                    </div>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>



<div class="modal fade window window-main" id="help-schedule-appointment" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <input type="hidden" name="id" />
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <span id="help-schedule-appointment-text"><?php echo trans('messages.help_schedule_appointment'); ?></span>
                    <div class="form">
                        <div class="sf-check">
                            <label><input type="checkbox" name="hide_help"><span></span> {{ trans("messages.hide_help_message") }}</label>
                        </div>
                    </div>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade window window-main" id="help-confirm-schedule-appointment" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <span id="help-confirm-schedule-appointment-text"><?php echo trans('messages.help_confirm_schedule_appointment'); ?></span>
                    <div class="form">
                        <div class="sf-check">
                            <label><input type="checkbox" name="hide_help"><span></span> {{ trans("messages.hide_help_message") }}</label>
                        </div>
                    </div>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div class="modal fade window window-main" id="help-cancel-schedule-appointment" role="dialog" aria-labelledby="detailTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="help">
                    <h1 class="icon"><i class="fa fa-info-circle"></i></h1>
                    <span id="help-cancel-schedule-appointment-text"><?php echo trans('messages.help_cancel_schedule_appointment'); ?></span>
                    <div class="form">
                        <div class="sf-check">
                            <label><input type="checkbox" name="hide_help"><span></span> {{ trans("messages.hide_help_message") }}</label>
                        </div>
                    </div>
                    <div class="help-button">
                        <a data-dismiss="modal" title="" class="btn-main"><i class="fa fa-close"></i> {{ trans("messages.close") }}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

function getDayHelp(num) {
    var day = "";

    if(num==1) {
        day = "{{ trans('messages.monday') }}";
    } else if(num==2) {
        day = "{{ trans('messages.tuesday') }}";
    } else if(num==3) {
        day = "{{ trans('messages.wednesday') }}";
    } else if(num==4) {
        day = "{{ trans('messages.thursday') }}";
    } else if(num==5) {
        day = "{{ trans('messages.friday') }}";
    } else if(num==6) {
        day = "{{ trans('messages.saturday') }}";
    } else if(num==0 || num==7) {
        day = "{{ trans('messages.sunday') }}";
    }

    return day;
}
    
function info_help_inpatient() {
    $("#help-inpatient").modal("show");
}

function info_help_schedule(doctor, polyclinic, clinic, date, start_time, end_time, no, estimate) {
    if(help_schedule==1) {
        $("#help-schedule").modal("show");

        var info = $("#help-schedule-text").html().replace('"Nama Tenaga Medis"', doctor);
        info = info.replace('"Doctor Name"', doctor);

        info = info.replace('"Nama Poli"', polyclinic);
        info = info.replace('"Poly Name"', polyclinic);

        info = info.replace('"Nama Klinik"', clinic);
        info = info.replace('"Clinic Name"', clinic);
        
        if(start_time!=null&&end_time!=null){
            info = info.replace('"Tanggal dan Waktu"', ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(start_time)+'-'+convertTime(end_time)+')');
            info = info.replace('"Date and Time"', ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(start_time)+'-'+convertTime(end_time)+')');
        }else{
            info = info.replace('"Tanggal dan Waktu"', ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date));
            info = info.replace('"Date and Time"', ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date));
        }

        info = info.replace('"Nomer Urut"', no);
        info = info.replace('"Serial Number"', no);

        if(estimate!=null){
            info = info.replace('"Waktu estimasi"', ''+convertTime(estimate)+'');
            info = info.replace('"Estimated time"', ''+convertTime(estimate)+'');
        }

        $("#help-schedule-text").html(info);

    }
}

$('#help-schedule').on('hidden.bs.modal', function (e) {
    var hide_help = $('#help-schedule input[name="hide_help"]').is(':checked');
    if(hide_help==true) {
        help_schedule = 0;

        formData= new FormData();

        formData.append("validate", "false");
        formData.append("_method", "PATCH");
        formData.append("help_schedule", "0");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
              

              if(!data.error) {

                  if(!data.success) {
                  } else {              
                  }
              } else {
              }
          },
          error: function(){
          }
        })
    }

    var id = $('#help-schedule input[name="id"]').val();
    setTimeout(function(){
        loading('#/{{ $lang }}/schedule/'+id);
        redirect('#/{{ $lang }}/schedule/'+id);                
    },500);
});

function info_help_schedule_appointment(doctor, patient, polyclinic, clinic, date, appointment) {
    if(help_schedule==1) {
        $("#help-schedule-appointment").modal("show");

        var info = $("#help-schedule-appointment-text").html().replace('"Nama Tenaga Medis"', doctor);
        info = info.replace('"Doctor Name"', doctor);

        info = info.replace('"Nama Pasien"', patient);
        info = info.replace('"patient Name"', patient);

        info = info.replace(/"Nama Poli"/g, polyclinic);
        info = info.replace(/"Poly Name"/g, polyclinic);

        info = info.replace(/"Nama Klinik"/g, clinic);
        info = info.replace(/"Clinic Name"/g, clinic);

        info = info.replace(/"Tanggal dan Waktu"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');
        info = info.replace(/"Date and Time"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');

        $("#help-schedule-appointment-text").html(info);
    }
}

$('#help-schedule-appointment').on('hidden.bs.modal', function (e) {
    var hide_help = $('#help-schedule-appointment input[name="hide_help"]').is(':checked');
    if(hide_help==true) {
        help_schedule = 0;

        formData= new FormData();

        formData.append("validate", "false");
        formData.append("_method", "PATCH");
        formData.append("help_schedule", "0");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
              

              if(!data.error) {

                  if(!data.success) {
                  } else {              
                  }
              } else {
              }
          },
          error: function(){
          }
        })
    }

    var id = $('#help-schedule-appointment input[name="id"]').val();
    setTimeout(function(){
        loading('#/{{ $lang }}/schedule/'+id);
        redirect('#/{{ $lang }}/schedule/'+id);                
    },500);
});

function info_help_confirm_schedule_appointment(doctor, patient, polyclinic, clinic, date, appointment) {
    if(help_action_schedule==1) {
        $("#help-confirm-schedule-appointment").modal("show");

        var info = $("#help-confirm-schedule-appointment-text").html().replace(/"Nama Tenaga Medis"/g, doctor);
        info = info.replace(/"Doctor Name"/g, doctor);

        info = info.replace('"Nama Pasien"', patient);
        info = info.replace('"patient Name"', patient);

        info = info.replace(/"Nama Poli"/g, polyclinic);
        info = info.replace(/"Poly Name"/g, polyclinic);

        info = info.replace(/"Nama Klinik"/g, clinic);
        info = info.replace(/"Clinic Name"/g, clinic);

        info = info.replace(/"Tanggal dan Waktu"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');
        info = info.replace(/"Date and Time"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');

        $("#help-confirm-schedule-appointment-text").html(info);

    }
}

$('#help-confirm-schedule-appointment').on('hidden.bs.modal', function (e) {
    var hide_help = $('#help-confirm-schedule-appointment input[name="hide_help"]').is(':checked');
    if(hide_help==true) {
        help_action_schedule = 0;

        formData= new FormData();

        formData.append("validate", "false");
        formData.append("_method", "PATCH");
        formData.append("help_action_schedule", "0");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
              

              if(!data.error) {

                  if(!data.success) {
                  } else {              
                  }
              } else {
              }
          },
          error: function(){
          }
        })
    }
});

function info_help_cancel_schedule_appointment(doctor, patient, polyclinic, clinic, date, appointment) {
    if(help_action_schedule==1) {
        $("#help-cancel-schedule-appointment").modal("show");

        var info = $("#help-cancel-schedule-appointment-text").html().replace(/"Nama Tenaga Medis"/g, doctor);
        info = info.replace(/"Doctor Name"/g, doctor);

        info = info.replace('"Nama Pasien"', patient);
        info = info.replace('"patient Name"', patient);

        info = info.replace(/"Nama Poli"/g, polyclinic);
        info = info.replace(/"Poly Name"/g, polyclinic);

        info = info.replace(/"Nama Klinik"/g, clinic);
        info = info.replace(/"Clinic Name"/g, clinic);

        info = info.replace(/"Tanggal dan Waktu"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');
        info = info.replace(/"Date and Time"/g, ''+getDayHelp((new Date(date)).getDay())+', '+getShortDate(date)+' ('+convertTime(appointment)+')');

        $("#help-cancel-schedule-appointment-text").html(info);
    }
}

$('#help-cancel-schedule-appointment').on('hidden.bs.modal', function (e) {
    var hide_help = $('#help-cancel-schedule-appointment input[name="hide_help"]').is(':checked');
    if(hide_help==true) {
        help_action_schedule = 0;

        formData= new FormData();

        formData.append("validate", "false");
        formData.append("_method", "PATCH");
        formData.append("help_action_schedule", "0");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $.ajax({
          url: "{{ $api_url }}/{{ $lang }}/clinic/setting",
          type: "POST",
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
              

              if(!data.error) {

                  if(!data.success) {
                  } else {              
                  }
              } else {
              }
          },
          error: function(){
          }
        })
    }
});
    
</script>