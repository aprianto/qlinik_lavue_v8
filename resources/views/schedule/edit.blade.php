@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-10">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.update_registration') }}
                    </h3>
                </div>
                <div class="cell hide" id="loading-edit">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload-edit">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif" id="section-edit">                    
                    <form role="form" class="sec" id="form-edit">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.type_of_visit') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="kunjungan sakit" name="type"><span></span> {{ trans('messages.sick_visit') }}</label>
                                        <label><input type="radio" value="kunjungan sehat" name="type"><span></span> {{ trans('messages.healthy_visit') }}</label>
                                    </div>
                                </div>
                                <div class="row" style="display: none;" id="sickVisit">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">{{ trans('messages.type_of_care') }} <span>*</span></label>
                                            <select class="form-control" name="type_kunjungan_sakit">
                                                <option value="">{{ trans('messages.select_type_of_care') }}</option>
                                                <option value="outpatient">{{ trans('messages.outpatient') }}</option>
                                                <option value="inpatient">{{ trans('messages.inpatient') }}</option>
                                                <option value="promotif preventif">Promotif Preventif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="display: none;" id="healthyVisit">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">{{ trans('messages.type_of_care') }} <span>*</span></label>
                                            <select class="form-control" name="type_kunjungan_sehat">
                                                <option value="">{{ trans('messages.select_type_of_care') }}</option>
                                                <option value="outpatient">{{ trans('messages.outpatient') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">{{ trans('messages.activities') }} <span>*</span></label>
                                            <select class="form-control" name="kegiatan">
                                                <option value="">{{ trans('messages.Select_type_activity') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="group-label first-label">{{ trans('messages.patient') }}</h3>
                                <input type="hidden" name="id" />       
                                <div class="form-group content-column" id="name-group">
                                    <label class="control-label">{{ trans('messages.patient') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="name">
                                            <option value="">{{ trans('messages.select_patient') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.mrn') }}</label>
                                    <input type="text" class="form-control" placeholder="<?php echo trans('messages.mrn'); ?>" name="mrn" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.age') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.age') }}" name="age" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.gender') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="0" name="gender" disabled="disabled"><span></span> {{ trans('messages.male') }}</label>
                                        <label><input type="radio" value="1" name="gender" disabled="disabled"><span></span> {{ trans('messages.female') }}</label>
                                    </div>
                                </div>
                                <div class="form-group" id="smoke-group">
                                    <label class="control-label">{{ trans('messages.smoking') }}</label>
                                    <div class="sf-radio"> 
                                        <label><input type="radio" value="1" name="smoke" disabled="disabled"><span></span> {{ trans('messages.smoking') }}</label>
                                        <label><input type="radio" value="0" name="smoke" disabled="disabled"><span></span> {{ trans('messages.no_smoking') }}</label>
                                    </div>
                                </div>
                                <div class="form-group no-arrow">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">{{ trans('messages.blood_type') }}</label>
                                            <select class="form-control" name="blood_type" disabled="disabled">
                                                <option value="">{{ trans('messages.blood_type') }}</option>
                                                <option value="A">{{ trans('messages.A') }}</option>
                                                <option value="B">{{ trans('messages.B') }}</option>
                                                <option value="AB">{{ trans('messages.AB') }}</option>
                                                <option value="O">{{ trans('messages.O') }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.address') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.address') }}" name="address" />
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.email') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.email') }}" name="email" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.phone_number') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.phone_number') }}" name="phone" disabled="disabled">
                                </div>
                                <div class="form-group no-arrow">
                                    <label class="control-label">{{ trans('messages.patient_category') }}</label>
                                    <select class="form-control" name="patient_category" disabled="disabled">
                                        <option value=""></option>
                                    </select>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.financing') }}</label>
                                    <select class="form-control" name="financing">
                                        <option value="">{{ trans('messages.financing') }}</option>
                                        <option value="personal">{{ trans('messages.personal') }}</option>
                                        <option value="agency">{{ trans('messages.agency') }}</option>
                                        <option value="insurance">{{ trans('messages.insurance') }}</option>
                                    </select>
                                </div>
                                <div class="form-group content-column hide" id="insurance-group">
                                    <label class="control-label">{{ trans('messages.insurance') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="insurance">
                                            <option value="">{{ trans('messages.select_insurance') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="row hide" id="no_bpjs-group">
                                    <div class="col-md-6">
                                        <div class="form-group" id="card_number-group">
                                            <label class="control-label">{{ trans('messages.card_number') }} <span>*</span></label>
                                            <input type="text" class="form-control" placeholder="{{ trans('messages.enter_card_number') }}" name="card_number">
                                            <span class="help-block"></span>
                                            <small class="text-success hide">{{ trans('messages.card_can_be_used') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <a id="verification" class="link-add link-label"> {{ trans('messages.verification') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="group-label first-label">{{ trans('messages.doctor') }}</h3>
                                <div class="form-group content-column" id="polyclinic-group">
                                    <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value="">{{ trans('messages.select_polyclinic') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column" id="doctor-group">
                                    <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="doctor" disabled="disabled">
                                            <option value="">{{ trans('messages.no_doctor') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column" id="schedule-group">
                                    <label class="control-label">{{ trans('messages.practice_hour') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="schedule" disabled="disabled">
                                            <option value="">{{ trans('messages.no_schedule') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column date-column condition-group hide" id="appointment-group">
                                    <div class="sf-check">
                                        <label><input type="checkbox" name="set_appointment"><span></span> {{ trans('messages.set_appointment') }}</label>
                                    </div>
                                    <div id="timepicker_time" class="input-group date">
                                        <input  class="form-control" data-format="HH:mm" type="text" placeholder="{{ trans('messages.input_time') }}" name="appointment" value="" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-hour.png') }}" />
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group content-column date-column date-disabled" id="date-group">
                                    <label class="control-label">{{ trans('messages.date') }} <span>*</span></label>
                                    <div id="date-section">
                                        <div id="datepicker_date" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="date" disabled="disabled" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                                <div class="form-group hide" id="date_inap-group">
                                    <label class="control-label">{{ trans('messages.date') }} <span>*</span></label>
                                    <div id="date_inap_section">
                                        <div id="datepicker_date_inap" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="date_inap" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group last-item">
                                    <button class="btn btn-primary">
                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        {{ ucwords(trans('messages.save')) }}
                                    </button>
                                    <a href="#/{{ $lang }}/schedule/sick" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                        {{ trans('messages.cancel') }}
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

let prevData;

$("#datepicker_date input[name='date']").keypress(function(event) {event.preventDefault();});
$("#datepicker_date_inap input[name='date_inap']").keypress(function(event) {event.preventDefault();});

$("#form-edit select[name='blood_type']").select2({
    placeholder: "{{ trans('messages.blood_type') }}",
    allowClear: false
});

$("#form-edit select[name='patient_category']").select2({
    placeholder: "{{ trans('messages.patient_category') }}",
    allowClear: false
});

$("#form-edit select[name='financing']").select2({
    placeholder: "{{ trans('messages.financing') }}",
    allowClear: false
});

$("#form-edit select[name='insurance']").select2({
    placeholder: "{{ trans('messages.insurance') }}",
    allowClear: false
});

$("#form-edit select[name=type_kunjungan_sakit]").select2({
    placeholder: "{{ trans('validation.empty_type_of_care') }}",
    allowClear: false
});

$("#form-edit select[name=type_kunjungan_sehat]").select2({
    placeholder: "{{ trans('validation.empty_type_of_care') }}",
    allowClear: false
});

$("#form-edit select[name=kegiatan]").select2({
    placeholder: "{{ trans('validation.empty_of_activity') }}",
    allowClear: false
});

$("#datepicker_date_inap").datetimepicker({
    locale: "{{ $lang }}",
    format: "DD/MM/YYYY"
});

$('#timepicker_time').datetimepicker({format: 'HH:mm'});

var get_id_polyclinic = "";
var get_id_doctor = "";
var get_id_doctor_daily_schedule = "";
var get_date = "";
var get_appointment = "";

function resetData() {
    get_id_polyclinic = "";
    get_id_doctor = "";
    get_id_doctor_daily_schedule = "";
    get_date = "";
    get_appointment = "";
}

var doctorEndDate = "31-12-9999";
var doctorStartLeaveDate = "";
var doctorStartEndLeaveDate = "";

function listInsurance(val) {
    loading_content("#form-edit #insurance-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsurance){
            if( !dataInsurance.error ) {
                loading_content("#form-edit #insurance-group", "success");
                let item = dataInsurance.data;
                $("#form-edit select[name='insurance']").html("<option value=''></option> ");
                
                for (i = 0; i < item.length; i++) {
                    $("#form-edit select[name='insurance']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-edit select[name='insurance']").select2({
                    placeholder: "{{ trans('messages.select_insurance') }}",
                    allowClear: false
                });

                if( val != "" ) {
                    $("#form-edit select[name='insurance']").val(val).trigger("change");
                }
            } else {
                loading_content("#form-edit #insurance-group", "failed");
                $("#form-edit #insurance-group #loading-content").click(function(){ listInsurance(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #insurance-group", "failed");
            $("#form-edit #insurance-group #loading-content").click(function(){ listInsurance(val); });
        }
    });
}

$("#form-edit input[name='type']:radio").change(function(e) {

    if(prevData && prevData.type === e.target.value) {
        placeData(prevData);
    } 
    else {
        if(e.target.value == 'kunjungan sakit'){
            $('#form-edit').find("#sickVisit").show();
            $('#form-edit').find("#healthyVisit").hide();
            $("#form-edit select[name='type_kunjungan_sakit']").val("").trigger("change");
        }else if(e.target.value == 'kunjungan sehat'){
            $('#form-edit').find("#sickVisit").hide();
            $('#form-edit').find("#healthyVisit").show();
            $('#form-edit').find("#schedule-group").removeClass("hide");
            $('#form-edit').find("#date-group").removeClass("hide");
            $('#form-edit').find("#date_inap-group").addClass("hide");
            $('#form-edit').find("#date_inap-group").val("");
        }
        listPoly();
        resetField();
    }

    
});

$("#form-edit select[name=type_kunjungan_sakit]").on('change', function() {
    if(this.value === 'inpatient') {
        $('#form-edit').find("#schedule-group").addClass("hide");
        $("#form-edit").find("#date-group").addClass("hide");
        $("#form-edit").find("#date_inap-group").removeClass("hide");
    }else {
        $('#form-edit').find("#schedule-group").removeClass("hide")
        $("#form-edit").find("#date-group").removeClass("hide");
        $("#form-edit").find("#date_inap-group").addClass("hide");
        $("#form-edit").find("input[name='date_inap']").val("");
    }
    resetField();
});

$('#form-edit select[name=financing]').on('change', function() {
    let financing = this.value;
    resetValidation('form-edit #insurance');

    if( financing == 'insurance' ) {
        $("#form-edit select[name='insurance']").val("").trigger("change");
        $("#form-edit #insurance-group").removeClass("hide");
        $("#form-edit #no_bpjs-group").addClass("hide");
        $("#form-edit button").attr("disabled", true);
    } else {
        $("#form-edit #insurance-group").addClass("hide");
        $("#form-edit #no_bpjs-group").addClass("hide");
        $("#form-edit button").attr("disabled", false);
    }
});

$('#form-edit select[name=insurance]').on('change', function() {
    let insurance = this.selectedOptions[0].label;
    
    if( insurance.toLowerCase().includes("bpjs") ){
        $("#form-edit #no_bpjs-group").removeClass("hide");
        $("#form-edit button").attr("disabled", true);
    }else{
        $("#form-edit #no_bpjs-group").addClass("hide");
        $("#form-edit button").attr("disabled", false);
        $("#form-edit input[name=card_number]").val("");
    }
});

$("#form-edit input[name=card_number]").on('keyup', function() {
    formValidate(true, ['form-edit #card_number', '', true]);
});

function resetField() {
    $("#form-edit select[name='polyclinic']").val("").trigger("change");
    $("#form-edit select[name='doctor']").val("").trigger("change");
    $("#form-edit select[name='schedule']").val("").trigger("change");
    $("#form-edit input[name='date']").val("");
    $("#form-edit input[name='date_inap']").val("");
    $("#form-edit select[name='kegiatan']").val("").trigger("change");
    $("#form-edit input[name='appointment']").val("");
    $("#form-edit #appointment-group").addClass("hide");
    $("#form-edit select[name='type_kunjungan_sehat']").val("").trigger("change");
}

$('#form-edit #verification').on('click', function() {
    let id = $("#form-edit input[name='card_number']").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(data.data){
                if(!data.error && data.success) {
                    if(!data.data.aktif){
                        formValidate(true, ['form-edit #card_number', '{{ trans("validation.invalid_number") }}', true]);
                        $("#form-edit small.text-success").addClass("hide");
                        $("#form-edit button").attr("disabled", true);
                    }else{
                        if(data.data.tunggakan > 0){
                            formValidate(true, ['form-edit #card_number', '{{ trans("validation.notif_arrears") }} Rp.'+data.data.tunggakan, true]);
                            $("#form-edit small.text-success").addClass("hide");
                            $("#form-edit button").attr("disabled", true);
                        }else{
                            formValidate(true, ['form-edit #card_number', '', true]);
                            $("#form-edit small.text-success").removeClass("hide");
                            $("#form-edit button").attr("disabled", false);
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            }else{
                formValidate(true, ['form-edit #card_number', '{{ trans("validation.invalid_number") }}', true]);
                $("#form-edit small.text-success").addClass("hide");
                $("#form-edit button").attr("disabled", true);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});

function listPoly(val) {
    let is_bpjs = false;
    let type = "";
    
    if($("#form-edit input[name='type']:checked").val() == "kunjungan sehat"){
        type = "kunjungan_sehat"
    }else if($("#form-edit input[name='type']:checked").val() == "kunjungan sakit"){
        type = "kunjungan_sakit"
    }

    if($("#form-add input[name='card_number']").val() != ""){
        @if(Session::get('pcare'))
            is_bpjs = true;
        @endif
    }

    loading_content("#form-edit #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic?is_bpjs="+is_bpjs+"&type="+type,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            if( !dataPoly.error ) {
                loading_content("#form-edit #polyclinic-group", "success");
                let item = dataPoly.data;
                $("#form-edit select[name='polyclinic']").html("<option value=''></option>");
                $("#form-edit select[name='kegiatan']").html("<option value=''></option>");
                
                for (i = 0; i < item.length; i++) {
                    $("#form-edit select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    $("#form-edit select[name='kegiatan']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-edit select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val != "") {
                    $("#form-edit select[name='polyclinic']").val(val).trigger("change");
                }                
            } else {
                loading_content("#form-edit #polyclinic-group", "failed");
                $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #polyclinic-group", "failed");
            $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    });
}

function resetSchedule() {
    $("#form-edit #appointment-group").addClass("hide");
    $("#form-edit input[name='set_appointment']").prop("checked",false);
    $("#form-edit input[name='appointment']").prop("disabled",true);
    $("#form-edit input[name='appointment']").val("");
    $("#form-edit input[name='date']").prop("disabled", true);
    $("#form-edit input[name='date']").val("");
}

function listDoctor(polyclinic, val) {
    resetSchedule();
    loading_content("#form-edit #doctor-group", "loading");
    let is_bpjs = false;

    if( $("#form-add input[name='card_number']").val() != "" ){
        @if(Session::get('pcare'))
            is_bpjs = true;
        @endif
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+polyclinic+"&active=true&is_bpjs="+is_bpjs,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if( !data.error ) {
                loading_content("#form-edit #doctor-group", "success");
                $("#form-edit select[name=schedule]").prop("disabled", true);
                $("#form-edit select[name=schedule]").html("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                
                $("#form-edit select[name=schedule]").select2({
                    placeholder: "{{ trans('messages.no_schedule') }}",
                    allowClear: false
                });

                let item = data.data;
                if( item.length > 0 ) {
                    $("#form-edit select[name=doctor]").html("<option value=''>{{ trans('messages.select_doctor') }}</option>");
                    for (i = 0; i < item.length; i++) {
                        $("#form-edit select[name=doctor]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-edit select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}",
                        allowClear: false
                    });

                    if( val != "" ) {
                        $("#form-edit select[name='doctor']").val(val).trigger("change");
                    }
                } else {
                    $("#form-edit select[name=doctor]").html("<option value=''>{{ trans('messages.no_doctor') }}</option>");
                    
                    $("#form-edit select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.no_doctor') }}",
                        allowClear: false
                    });
                }
            } else {
                loading_content("#form-edit #doctor-group", "failed");
                $("#form-edit #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
            }
        },
        error: function(){
            loading_content("#form-edit #doctor-group", "failed");
            $("#form-edit #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
        }
    });
}

function listSchedule(doctor, val) {
    resetSchedule();

    doctorEndDate = "9999-12-31";
    doctorStartLeaveDate = "";
    doctorStartEndLeaveDate = "";

    if(doctor!="") {
        loading_content("#form-edit #schedule-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+doctor+"",
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                if( !data.error ) {
                    loading_content("#form-edit #schedule-group", "success");
                    if(data.schedule != null) {
                        item = data.schedule.daily;
                        doctorStartLeaveDate = data.schedule.start_date_leave;
                        doctorEndLeaveDate = data.schedule.end_date_leave;

                        if(item.length == 0) {
                            $("#form-edit select[name=schedule]").prop("disabled", true);
                            $("#form-edit select[name=schedule]").html("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                            $("#form-edit select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.no_schedule') }}"
                            });
                        } else {
                            $("#form-edit select[name=schedule]").prop("disabled", false);
                            $("#form-edit select[name=schedule]").html("<option value=''></option>");

                            for (i = 0; i < item.length; i++) {
                                $("#form-edit select[name=schedule]").append("<option data-day='"+item[i].day+"' data-appointment='"+item[i].appointment+"' value='"+item[i].id+"'>"+getDay(item[i].day)+" ("+convertTime(item[i].start_time)+" - "+convertTime(item[i].end_time)+")</option>");
                            }

                            $("#form-edit select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.select_practice_hour') }}"
                            });

                            if(val != "") {
                                $("#form-edit select[name='schedule']").val(val).trigger("change");
                            }
                        }
                    } else {
                        $("#form-edit select[name=schedule]").html("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                        $("#form-edit select[name=schedule]").select2({
                            placeholder: "{{ trans('messages.no_schedule') }}"
                        });
                    }
                } else {
                    loading_content("#form-edit #schedule-group", "failed");
                    $("#form-edit #schedule-group #loading-content").click(function(){ listSchedule(doctor, val); });
                }
            },
            error: function(){
                loading_content("#form-edit #schedule-group", "failed");
                $("#form-edit #schedule-group #loading-content").click(function(){ listSchedule(doctor, val); });
            }
        });
    } else {
        $("#form-edit select[name=schedule]").html("<option value=''>{{ trans('messages.no_schedule') }}</option>");
        $("#form-edit select[name=schedule]").select2({
            placeholder: "{{ trans('messages.no_schedule') }}"
        });
    }
}

function listDate(day, val, appointment) {
    if(day != "") {
        if(appointment == 1) {
            $("#form-edit #appointment-group").removeClass("hide");
            if(val != "") {
                if(get_appointment == null || get_appointment == "") {
                    $("#form-edit input[name='set_appointment']").prop("checked",false);
                    $("#form-edit input[name='appointment']").prop("disabled",true);
                    $("#form-edit input[name='appointment']").val("");
                } else {
                    $("#form-edit input[name='set_appointment']").prop("checked",true);
                    $("#form-edit input[name='appointment']").prop("disabled",false);
                    $("#form-edit input[name='appointment']").val(convertTime(get_appointment));
                }
            } else {
                $("#form-edit input[name='set_appointment']").prop("checked",false);
                $("#form-edit input[name='appointment']").prop("disabled",false);
                $("#form-edit input[name='appointment']").val("");
            }
        } else {
            $("#form-edit #appointment-group").addClass("hide");
            $("#form-edit input[name='set_appointment']").prop("checked",false);
            $("#form-edit input[name='appointment']").prop("disabled",true);
            $("#form-edit input[name='appointment']").val("");
        }

        if( day == 7 ) {
            day = 0;
        }

        let dateDay = [];
        for( i = 0; i < 7; i++) {
            if(i != day) {
                dateDay.push(i);
            }
        }

        $("#form-edit #date-section").html('<div id="datepicker_date" class="input-group date">'+
                                    '<input class="form-control" type="text" placeholder=\'{{ trans("messages.select_date") }}\' name="date" disabled="disabled" />'+
                                    '<span class="input-group-addon">'+
                                        '<img src=\'{{ asset("assets/images/icons/action/select-date.png") }}\' />'+
                                    '</span>'+
                                '</div>');

        $("#form-edit input[name='date']").prop("disabled", false);

        let date = new Date();
        date.setDate(date.getDate());

        let min_date = new Date();
        min_date.setDate(min_date.getDate() - 1); 

        let disabled_dates = [];

        if(doctorStartLeaveDate != "0000-00-00" && doctorEndLeaveDate != "0000-00-00") {

            let start_min_date = new Date();
            start_min_date.setDate(start_min_date.getDate()); 

            let end_min_date = new Date();
            end_min_date.setDate(end_min_date.getDate() - 1); 

            for (let d = new Date(doctorStartLeaveDate.replace(/\-/g,'/')); d <= new Date(doctorEndLeaveDate.replace(/\-/g,'/')); d.setDate(d.getDate()+1)) {

                monthLoop = ((d.getMonth()+1)+100).toString().slice(-2);
                dayLoop = (d.getDate()+100).toString().slice(-2);
                dateLoop = d.getFullYear()+'/'+monthLoop+'/'+dayLoop;
                disabled_dates.push(dateLoop);

                /*monthDate = ((date.getMonth()+1)+100).toString().slice(-2);
                dayDate = (date.getDate()+100).toString().slice(-2);
                dateNow = date.getFullYear()+'/'+monthDate+'/'+dayDate;

                if(Date.parse(dateNow)==Date.parse(dateLoop)) {
                    date.setDate(date.getDate());
                }*/

                if(Date.parse(start_min_date) >= Date.parse(new Date(doctorStartLeaveDate.replace(/\-/g,'/'))) && Date.parse(end_min_date) <= Date.parse(new Date(doctorEndLeaveDate.replace(/\-/g,'/')))) {
                    if(Date.parse(min_date) <= Date.parse(new Date(dateLoop))) {
                        min_date.setDate(new Date(dateLoop).getDate());
                    }
                }
            }
        }

        disabled_dates.push(min_date);

        $('#datepicker_date').datetimepicker({
            locale: '{{ $lang }}',
            minDate: min_date,
            maxDate: doctorEndDate.replace(/\-/g,'/'),
            format: 'DD/MM/YYYY',
            daysOfWeekDisabled: dateDay,
            disabledDates: disabled_dates
        });

        if(val != "") {
            $("#form-edit input[name='date']").val(unconvertDate(val));
        }
        resetData();
    }
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#section-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if( !data.error ) {
                prevData = data;
                placeData(data);
            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }
        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");
            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    });
}

function placeData(data) {
    $("#form-edit input[name='id']").val(data.id);
    $("#form-edit input[name='type'][value='" + data.type + "']").prop('checked', true);
    resetValidation('form-edit #name', 'form-edit #polyclinic', 'form-edit #doctor', 'form-edit #schedule', 'form-edit #type');
    $("#form-edit select[name=name]").prop("disabled", true);
    $("#form-edit select[name=name]").html("");
    $("#form-edit select[name=name]").append("<option value='"+data.patient.id+"'>"+data.patient.name+"</option>");

    $("#form-edit select[name=name]").select2({
        placeholder: "{{ trans('messages.select_patient') }}",
    });
    
    if(data.type == 'kunjungan sakit'){
        $('#form-edit').find("#sickVisit").show();
        $('#form-edit').find("#healthyVisit").hide();
        $("#form-edit select[name='type_kunjungan_sakit']").val(data.type_kunjungan).trigger("change");
    }else if(data.type == 'kunjungan sehat'){
        $('#form-edit').find("#sickVisit").hide();
        $('#form-edit').find("#healthyVisit").show();
        $("#form-edit select[name='type_kunjungan_sehat']").val(data.type_kunjungan).trigger("change");
        $("#form-edit select[name='kegiatan']").val(data.kegiatan).trigger("change");
    }

    if(data.type_kunjungan === 'inpatient') {
        $("#form-edit input[name='date_inap']").val(unconvertDate(data.date))
    }

    $("#form-edit input[name='mrn']").val(data.patient.mrn);
    $("#form-edit input[name='age']").val(getAge(data.patient.birth_date));
    $("#form-edit input[name='gender'][value=" + data.patient.gender + "]").prop('checked', true);
    $("#form-edit input[name='smoke'][value=" + data.patient.smoke + "]").prop('checked', true);
    $("#form-edit select[name='blood_type']").val(data.patient.blood_type).trigger("change");
    $("#form-edit input[name='address']").val(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
    $("#form-edit input[name='email']").val(data.patient.email);
    $("#form-edit input[name='phone']").val('+'+data.patient.phone_code+''+data.patient.phone);
    
    try {
        $("#form-edit select[name=patient_category]").append("<option value='"+data.patient.id_patient_category+"' selected='selected'>"+data.patient.patient_category.name+"</option>");
    } catch(err) {}

    $("#form-edit select[name=patient_category]").select2({
        placeholder: "{{ trans('messages.patient_category') }}"
    });

    $("#form-edit select[name='financing']").val(data.financing).trigger("change");
    $("#form-edit input[name='card_number']").val(data.card_number);
    $('#form-edit select[name=polyclinic]').val(data.id_polyclinic).trigger("change");

    get_id_polyclinic = data.id_polyclinic;
    get_id_doctor = data.id_doctor;
    get_id_doctor_daily_schedule = data.id_doctor_daily_schedule;
    get_date = data.date;
    get_appointment = data.appointment;

    listPoly(data.id_polyclinic);

    if(data.insurance != null) {
        listInsurance(data.insurance.id);
    } else {
        listInsurance('');
    }

    $('#loading-edit').addClass('hide');
    $("#section-edit").removeClass("hide");
}

edit("{{ $id }}");

$('#form-edit select[name=polyclinic]').on('change', function() {
    if(this.value != "") {
        listDoctor(this.value, get_id_doctor);
        $("#form-edit select[name='kegiatan']").val(this.value).trigger("change")
    }
});

$('#form-edit select[name=doctor]').on('change', function() {
    if(this.value != "") {
        if(this.value == get_id_doctor) {
            listSchedule(this.value, get_id_doctor_daily_schedule); 
        } else {
            resetData();
            listSchedule(this.value, '');
        }
    }
});

$('#form-edit select[name=schedule]').change(function() {
    if(this.value != "") {
        let dateSchedule = $(this).find(':selected').data('day');
        let appointment = $(this).find(':selected').data('appointment');
        
        if($("#form-edit select[name='type_kunjungan_sakit'] option:selected").val() !== 'inpatient') {
            if(this.value == get_id_doctor_daily_schedule) { 
                listDate(dateSchedule, get_date, appointment);
            } else {
                resetData();
                listDate(dateSchedule, '', appointment);
            }
        }
    }
});

$('#form-edit input[name="set_appointment"]').click(function() {    
    let checked = this.checked;
    if(checked == true) {
        $("#form-edit input[name='appointment']").prop("disabled",false);
        $("#form-edit input[name='appointment']").val("");
    } else {
        $("#form-edit input[name='appointment']").prop("disabled",true);
        $("#form-edit input[name='appointment']").val("");
    }
});

$('#form-edit input[name=type]').click(function() {
    resetValidation('form-edit #type');
});

$("#form-edit #insurance-group .border-group").click(function() {
    resetValidation('form-edit #insurance');
});

$("#form-edit #polyclinic-group .border-group").click(function() {
    resetValidation('form-edit #polyclinic');
});

$("#form-edit #doctor-group .border-group").click(function() {
    resetValidation('form-edit #doctor');
});

$("#form-edit #schedule-group .border-group").click(function() {
    resetValidation('form-edit #schedule');
});

$("#form-edit #date-group").click(function() {
    resetValidation('form-edit #date');
});

$('#form-edit input[name="set_appointment"]').click(function() {
    resetValidation('form-edit #appointment');
});

$('#form-edit input[name="appointment"]').focus(function() {
    resetValidation('form-edit #appointment');
});

$("#form-edit").submit(function(event) {
    resetValidation('form-edit #name', 'form-edit #insurance', 'form-edit #polyclinic', 'form-edit #doctor', 'form-edit #schedule', 'form-edit #type');
    
    event.preventDefault();
    $("#form-edit button").attr("disabled", true);

    let id = $("#form-edit input[name=id]").val();
    let type = $("#form-edit input[name='type']:checked").val();
    let date_inap = $("#form-edit input[name='date_inap']").val();
    let date_ = $("#form-edit input[name='date']").val();
    let financing = $("#form-edit select[name='financing'] option:selected").val();
    let appointment = $("#form-edit input[name='appointment']").val();
    let type_kunjungan = $("#form-edit select[name='type_kunjungan_sakit'] option:selected").val();
    let date = "";
    let errorAppointment = true;
    let errorTimeNow = true;

    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);

    if(type == 'kunjungan sakit'){
        formData.append("type_kunjungan", type_kunjungan);
        if(type_kunjungan == 'inpatient' && date_inap != ""){
            date = convertDate(date_inap);
        }else{
            if(date_ != "") {
                date = convertDate(date_);
            }
        }
    }else if(type == 'kunjungan sehat'){
        formData.append("type_kunjungan", $("#form-edit select[name='type_kunjungan_sehat'] option:selected").val());
        formData.append("kegiatan", $("#form-edit select[name='kegiatan'] option:selected").val());
        if(date_ != "") {
            date = convertDate(date_);
        }
    }

    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("type", type);
    formData.append("id_patient", $("#form-edit select[name='name'] option:selected").val());
    formData.append("id_polyclinic", $("#form-edit select[name='polyclinic'] option:selected").val());
    formData.append("id_doctor", $("#form-edit select[name='doctor'] option:selected").val());
    formData.append("id_doctor_daily_schedule", $("#form-edit select[name='schedule'] option:selected").val());
    formData.append("appointment", $("#form-edit input[name='appointment']").val());
    formData.append("financing", financing);

    if(financing == "insurance") {
        formData.append("insurance", $("#form-edit select[name='insurance'] option:selected").val());
        formData.append("card_number", $("#form-edit input[name='card_number']").val());
    }

    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    formData.append("date", date);

    if($("#form-edit select[name='schedule']").val() != "" && $("#form-edit input[name='set_appointment']").is(':checked') && $("#form-edit input[name='appointment']").val() == "") {
        formData.append("validate", true);
        formValidate(true, ['form-edit #appointment','{{ trans("validation.empty_appointment") }}', true]);
        errorAppointment = false;
    }

    if(date != '' && appointment != '' && $("#form-edit input[name='set_appointment']").is(':checked')) {

        newDate = new Date();
        monthNow = ((newDate.getMonth()+1)+100).toString().slice(-2);
        dayNow = (newDate.getDate()+100).toString().slice(-2);
        yearNow = newDate.getFullYear();
        hoursNow = (newDate.getHours()+100).toString().slice(-2);
        minutesNow = (newDate.getMinutes()+100).toString().slice(-2);
        secondsNow = (newDate.getSeconds()+100).toString().slice(-2);
        dateTimeNow = yearNow+'/'+monthNow+'/'+dayNow+' '+hoursNow+':'+minutesNow+':'+secondsNow+'';

        splitDateForm = date.split("-");
        dateForm = splitDateForm[0]+'/'+splitDateForm[1]+'/'+splitDateForm[2]; 
        dateTimeForm = dateForm+' '+appointment+':00';

        if(Date.parse(dateTimeNow) > Date.parse(dateTimeForm)) {
            notif(false,'{{ trans("validation.invalid_time_now_appointment") }}');
            $("#form-edit button").attr("disabled", false);
            errorTimeNow = false;
        }
    }

    if(errorTimeNow) {

        $("#form-edit .btn-primary").addClass("loading");
        $("#form-edit .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/schedule/"+id,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $("#form-edit button").attr("disabled", false);
                $("#form-edit .btn-primary").removeClass("loading");
                $("#form-edit .btn-primary span").addClass("hide");
                if(!data.error) {
                    if(!data.success) {
                        if(data.errors.schedule) {
                            notif(false,""+data.errors.schedule+"");
                        } else {
                            if(data.message) {
                                notif(false,""+data.message+"");
                            }
                        }
                        formValidate(true, ['form-edit #name',data.errors.id_patient, true], ['form-edit #insurance',data.errors.insurance, true], ['form-edit #polyclinic',data.errors.id_polyclinic, true], ['form-edit #doctor',data.errors.id_doctor, true], ['form-edit #schedule',data.errors.id_doctor_daily_schedule, true], ['form-edit #date',data.errors.date, true], ['form-edit #type',data.errors.type, true]);
                    } else {                    
                        if(errorAppointment) {
                            notif(true,"{{ trans('validation.success_update_schedule') }}");  
                            setTimeout(function(){
                                loading('#/{{ $lang }}/schedule/{{ $id }}');
                                redirect('#/{{ $lang }}/schedule/{{ $id }}');                    
                            },500);
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-edit button").attr("disabled", false);
                $("#form-edit .btn-primary").removeClass("loading");
                $("#form-edit .btn-primary span").addClass("hide");
                notif(false,"{{ trans('validation.failed') }}");
            }
        });
    }
});
</script>

@include('schedule.help')