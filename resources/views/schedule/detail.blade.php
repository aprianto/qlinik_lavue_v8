@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content" id="detail">    
    <div class="row" id="section-loading">
        <div class="col-md-9">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-button">
        <div class="col-md-9">
            <div class="widget no-color">
                <div class="btn-sec">
                    <ul>
                        @if($level != 'doctor')
                        <li id="edit_schedule" class="hide" style="float: left">                         
                            <a href="#/{{ $lang }}/schedule/{{ $id }}/edit" onclick="loading($(this).attr('href'))" >
                                <img src="{{ asset('assets/images/icons/action/schedule-blue.png') }}" />
                                <span>{{ trans('messages.change_schedule') }}</span>
                            </a>
                        </li>
                        @endif
                        @if(isset($acl->medical_records))
                        @if($acl->medical_records->read==true)
                        <li id="link_medical_records" class="hide" style="float: right">                         
                            <a onclick="loading($(this).attr('href'))">
                                <img src="{{ asset('assets/images/icons/action/med-rec-blue.png') }}" />
                                <span>{{ trans('messages.medical_records') }}</span>
                            </a>
                        </li>
                        @endif
                        @endif
                        @if($level != 'doctor')
                        <li style="float: left">                         
                            <a onclick="reportSchedule('{{ $id }}')">
                                <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                                <span>{{ trans('messages.print_registration_number') }}</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-9">            
            <div class="admin-follow no-bg">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        Detail Pendaftaran
                    </h3>
                </div>
                <div class="search-sec">
                    <div id="section-profile" class="detail-profile-form" style="display: flex; flex-direction: row; align-items: center;">
                        <div class="column" id="patient-photo"><img src="{{ $storage.'/images/patient/photo.png' }}" width="80px" height="80px" style="min-width:80px;max-width:80px;width:80px;min-height:80px;max-height:80px;height:80px;border: 1px solid #ededed;"></div>
                        <div class="column text-left">
                            <h3 id="name" style="font-size: 20px; line-height: 1.4; margin-bottom: 4px; margin-top: 0;"></h3>
                            <p id="mrn"></p>
                        </div>
                        <div class="column" style="margin-left: auto;">
                            <div class="btn-profile-sec">
                                <a id="btn-detail-profile">{{ trans("messages.show_detail") }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card hide" id="loading-profile">
                        <span class="three-quarters">Loading…</span>
                    </div>
                    <div class="card hide" id="reload-profile">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div id="detail-profile" class="hide widget widget-detail">
                    <div class="twitter-account">
                        <h3>{{ trans("messages.info") }}</h3>                    
                    </div>
                    <ul class="detail-profile">
                        <div id="detail-profile">
                            <li class="profile-features">
                                <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.phone_number') }}</span>
                                        <span id="phone"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.email') }}</span>
                                        <span id="email"></span>
                                    </div>
                                </div>
                            </li>                        
                            <li class="profile-features">  
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.birth_date') }}</span>
                                        <span id="birth_date"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">  
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.gender') }}</span>
                                        <span id="gender"></span>
                                    </div>
                                </div>
                            </li>     
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.no_identity') }}</span>
                                        <span id="no_identity"></span>
                                    </div>
                                </div>
                            </li>            
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.blood_type') }}</span>
                                        <span id="blood_type"></span>
                                    </div>
                                </div>
                            </li>                        
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.smoking') }}?</span>
                                        <span id="smoke"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <h4>{{ trans("messages.address") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.address') }}</span>
                                        <span id="address"></span>
                                    </div>
                                </div>
                            </li>
                            <li class="profile-features">
                                <h4>{{ trans("messages.patient_data") }}</h4>
                            </li>
                            <li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.patient_category') }}</span>
                                        <span id="patient_category"></span>
                                    </div>
                                </div>
                            </li>
                            <!--<li class="profile-features">
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/payment-plan.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.financing') }}</span>
                                        <span id="financing"></span>
                                    </div>
                                </div>
                            </li>-->
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-content panel-detail hide" id="section-schedule">
    <div class="row">
        <div class="col-md-9">
        <div class="widget widget-detail" id="schedule">
                <div class="timeline-color activity-feed">
                    <div class="widget-title">
                        <h3>Detail Pendaftaran</h3>
                    </div>
                    <ul class="activity">
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" class="margin" />
                                </div>
                                <div class="info-sec">
                                    <span>Jenis Kunjungan</span>
                                    <h3><a id="type_clinic"></a></h3>
                                    <span id="kegiatan"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/payment-plan.png') }}" class="margin" />
                                </div>
                                <div class="info-sec">
                                    <h3>Rencana Pembayaran</h3>
                                    <span id="financing-schedule"></span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="widget widget-detail" id="schedule">
                <div class="timeline-color activity-feed">
                    <div class="widget-title">
                        <h3>Jadwal Pendaftaran</h3>
                    </div>
                    <ul class="activity">
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" class="margin" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans("messages.clinic") }}</span>
                                    <h3><a id="clinic"></a></h3>
                                    <span id="address"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/doctor-name.png') }}" class="margin" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans("messages.doctor") }}</span>
                                    <h3><a id="doctor"></a></h3>
                                    <span id="polyclinic"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/schedule.png') }}" />
                                </div>
                                <div class="info-sec">                                    
                                    <h3 id="date"></h3>
                                    <span id="time"></span>
                                    <span id="section-number">{{ trans("messages.serial_number") }}: <i id="number"></i></span>
                                    <span id="status-confirmation" class="hide">{{ trans("messages.status") }}: {{ trans("messages.confirmed") }}</span>
                                    <span id="status-canceled" class="hide">{{ trans("messages.status") }}: {{ trans("messages.cancel") }}</span>
                                    <span id="status-pending" class="hide">{{ trans("messages.status") }}: {{ trans("messages.pending") }}</span>
                                    <span id="status-done" class="hide">{{ trans("messages.status") }}: {{ trans("messages.done") }}</span>
                                    </span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--<div class="btn-sec">
                    <ul>
                        <li id="confirm_schedule" class="hide">
                            <a data-toggle='modal' data-target='#confirmAccept' data-message="{{ trans('messages.info_confirmation') }}" data-id='{{ $id }}' class="hover"><i class="fa fa-check"></i> {{ trans("messages.confirmation") }}</a>
                        </li>
                        <li id="cancel_schedule" class="hide">                         
                            <a data-toggle='modal' data-target='#confirmCancel' data-message="{{ trans('messages.info_canceled') }}" data-id='{{ $id }}' class="disable hover"><i class="fa fa-times"></i> {{ trans("messages.canceled") }}</a>
                        </li>
                        <li>
                            <a class="hover" onclick="reportSchedule('{{ $id }}')"><i class="fa fa-print"></i> {{ trans("messages.print") }}</a>
                        </li>
                        <li id="edit_schedule" class="hide">                         
                            <a href="#/{{ $lang }}/schedule/{{ $id }}/edit" onclick="loading($(this).attr('href'))" class="disable hover"><i class="fa fa-clock-o"></i> {{ trans("messages.edit_schedule") }}</a>
                        </li>
                        <li>
                            <a id="link_medical_records" class="hover hide" onclick="loading($(this).attr('href'))"><i class="fa fa-medkit"></i> {{ trans("messages.medical_records") }}</a>
                        </li>
                    </ul>
                </div>-->
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script type="text/javascript">

$("#btn-detail-profile").click(function(){ 
    var section = $("#detail-profile").css('display');
    if(section=="block") {
        $("#btn-detail-profile").html("{{ trans('messages.show_detail') }}");
        $("#detail-profile").addClass("hide");
    } else {
        $("#btn-detail-profile").html("{{ trans('messages.hide_detail') }}");
        $("#detail-profile").removeClass("hide");
    }
});

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-button').addClass('hide');
    $('#section-detail').addClass('hide');
    $('#section-schedule').addClass('hide');
    $("#reload-detail").addClass("hide");

    $("#section-button #edit_schedule").addClass("hide");
    $("#schedule #confirm_schedule").addClass("hide");
    $("#schedule #cancel_schedule").addClass("hide");
    $("#schedule #status-confirmation").addClass("hide");
    $("#schedule #status-canceled").addClass("hide");
    $("#schedule #status-pending").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-button').removeClass('hide');
                $('#section-detail').removeClass('hide');
                $('#section-schedule').removeClass('hide');

                var no_identity = "-";
                if(data.patient.id_number!="" && data.patient.id_number!=null) {
                    no_identity = data.patient.id_number+" ("+data.patient.id_type+")";
                }

                $("#detail #photo-patient").replaceWith(thumbnail('lg','patient','{{ $storage }}',data.patient.photo,data.patient.name));
                $("#path").html(data.patient.name);
                $("#detail #mrn").html(data.patient.mrn);
                $("#detail #name").html(data.patient.name);
                $("#detail #birth_date").html(getFormatDate(data.patient.birth_date)+" ("+getAge(data.patient.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#detail #gender").html(getGender(data.patient.gender));

                var smoke = "";
                if(data.patient.smoke==1) {
                    smoke = '{{ trans("messages.smoking") }}';
                } else {
                    smoke = '{{ trans("messages.no_smoking") }}';
                }

                $("#detail #smoke").html(smoke);
                $("#detail #blood_type").html(checkNull(data.patient.blood_type));
                $("#detail #no_identity").html(no_identity);
                $("#detail #address").html(nullToEmpty(data.patient.address)+' '+nullToEmpty(data.patient.village)+' '+nullToEmpty(data.patient.district)+' '+nullToEmpty(data.patient.regency)+' '+nullToEmpty(data.patient.province)+' '+nullToEmpty(data.patient.country));
                $("#detail #email").html(checkNull(data.patient.email));
                $("#detail #phone").html("+"+data.patient.phone_code+""+data.patient.phone);

                try {
                    $("#detail #patient_category").html(data.patient.patient_category.name);
                } catch(err) {}

                $("#detail #financing").html(getFinancing(data.patient.financing));


                $("#schedule #logo").replaceWith(thumbnail('md','clinic','{{ $storage }}',data.clinic.logo,data.clinic.name));
                $("#schedule #clinic").html(data.clinic.name);
                $("#schedule #clinic").attr("href","#/{{ $lang }}/clinic/"+data.id_clinic);
                $("#schedule #address").html(nullToEmpty(data.clinic.address)+' '+nullToEmpty(data.clinic.village)+' '+nullToEmpty(data.clinic.district)+' '+nullToEmpty(data.clinic.regency)+' '+nullToEmpty(data.clinic.province)+' '+nullToEmpty(data.clinic.country));

                $("#schedule #photo-doctor").replaceWith(thumbnail('md','doctor','{{ $storage }}',data.doctor.photo,data.doctor.name));
                
                $("#schedule #doctor").attr("href","#/{{ $lang }}/doctor/"+data.id_doctor);
                $("#schedule #doctor").attr("onclick","loading($(this).attr('href'))");
                $("#schedule #doctor").html(data.doctor.name);
                $("#schedule #polyclinic").html(data.polyclinic.name);
                $("#schedule #kegiatan").html(data.kegiatan);

                type_kunjungan = "";
                if(data.type_kunjungan!=null) {
                    if(data.type_kunjungan == "outpatient"){
                        type_kunjungan = "Rawat Jalan";
                    }else if(data.type_kunjungan == "inpatient"){
                        type_kunjungan = "Rawat Inap";
                    }else if(data.type_kunjungan == "promotif preventif"){
                        type_kunjungan = "Promotif Preventif";
                    }
                }

                $("#schedule #type_clinic").html((data.type != null ? data.type.charAt(0).toUpperCase() + data.type.slice(1) + " - " : "") + (type_kunjungan));

                var date_schedule = new Date(data.date);
                var day_schedule = date_schedule.getDay();

                $("#schedule #date").html(getDay(day_schedule)+', '+getFormatDate(data.date));

                var time = "";
                if(data.appointment==null) {
                    if(data.start_time != null && data.end_time != null){
                        time = convertTime(data.start_time)+' - '+convertTime(data.end_time);
                    }
                } else {
                    if(data.start_time != null && data.end_time != null){
                        time = convertTime(data.start_time);
                    }
                }

                $("#schedule #time").html(time);

                var number = "";
                if(data.number==null) {
                    $("#schedule #section-number").addClass("hide");
                    $("#schedule #number").html("");
                } else {
                    $("#schedule #section-number").removeClass("hide");
                    $("#schedule #number").html(data.number);
                }

                date = new Date();
                monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
                dayNow = (date.getDate()+100).toString().slice(-2);
                yearNow = date.getFullYear();
                dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

                if(Date.parse(data.date.replace(/\-/g,'/'))>=Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                    
                    if(data.status.code!=-1 && data.status.code!=5) {
                        $("#section-button #edit_schedule").removeClass("hide");
                    }

                    if(data.appointment!='' || data.appointment!=null) {
                        if(data.status.code==1) {
                            $("#schedule #confirm_schedule").removeClass("hide");
                            $("#schedule #cancel_schedule").removeClass("hide");
                        }
                    }

                    if(data.status.code==-1) {
                        $("#schedule #status-canceled").removeClass("hide");
                    } else if(data.status.code==1) {
                        $("#schedule #status-pending").removeClass("hide");
                    } else if(data.status.code==2) {
                        if(data.appointment!='' && data.appointment!=null) {
                            $("#schedule #status-confirmation").removeClass("hide");
                        }
                    } else if(data.status.code==5) {
                        $("#schedule #status-done").removeClass("hide");
                    }
                } else {
                    if(data.status.code==5) {
                        $("#schedule #status-done").removeClass("hide");
                    }
                }

                if(data.status.code==5) {
                    $("#section-button #link_medical_records").removeClass("hide");
                    $("#section-button #link_medical_records a").attr("href","#/{{ $lang }}/medical/"+data.id_medical_records);
                } else {
                    if(data.status.code==2) {
                        //if(Date.parse(data.date.replace(/\-/g,'/'))==Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                        if(Date.parse(data.date.replace(/\-/g,'/'))<=Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                            $("#section-button #link_medical_records").removeClass("hide");
                            $("#section-button #link_medical_records a").attr("href","#/{{ $lang }}/medical/create/schedule/"+data.id);
                        }
                    }
                }

                var financing = getFinancing(data.financing);

                if(data.insurance!=null) {
                    financing = financing+" ("+data.insurance.name+")";
                    if(data.insurance.name.toLowerCase().includes("bpjs")){
                        $("#section-button #edit_schedule").addClass("hide");
                    }
                }

                $("#schedule #financing-schedule").html(financing);

                if(data.payment==true) {
                    $("#schedule #paid").html('<i class="fa fa-check-circle payment-success"></i>');
                } else {
                    $("#schedule #paid").html('<i class="fa fa-minus-circle payment-failed"></i>');
                }

            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

detail("{{ $id }}");


$('#confirmAccept').find('.modal-footer #confirm').on('click', function(){
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-button').addClass('hide');
    $('#section-detail').addClass('hide');
    $('#section-schedule').addClass('hide');
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"/status/2",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    
                    detail("{{ $id }}");
                    notif(true,"{{ trans('validation.success_confirmation') }}");

                    if(help_action_schedule==1) {
                        info_help_confirm_schedule_appointment(
                            data.doctor.name, 
                            data.patient.name, 
                            data.polyclinic.name, 
                            data.clinic.name, 
                            data.date, 
                            data.appointment
                        );
                    }
                        
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");

                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-button').removeClass('hide');
                $('#section-detail').removeClass('hide');
                $('#section-schedule').removeClass('hide');
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");

            $('#section-loading').addClass('hide');
            $('#loading-detail').addClass('hide');
            $('#section-button').removeClass('hide');
            $('#section-detail').removeClass('hide');
            $('#section-schedule').removeClass('hide');
        }
    })
    $("#confirmAccept").modal("toggle");
});

$('#confirmCancel').find('.modal-footer #confirm').on('click', function(){
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-button').addClass('hide');
    $('#section-detail').addClass('hide');
    $('#section-schedule').addClass('hide');
    
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"/status/-1",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    
                    detail("{{ $id }}");
                    notif(true,"{{ trans('validation.success_cancel') }}"); 

                    if(help_action_schedule==1) {
                        info_help_cancel_schedule_appointment(
                            data.doctor.name, 
                            data.patient.name, 
                            data.polyclinic.name, 
                            data.clinic.name, 
                            data.date, 
                            data.appointment
                        );
                    }
                     
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");

                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-button').removeClass('hide');
                $('#section-detail').removeClass('hide');
                $('#section-schedule').removeClass('hide');
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");

            $('#section-loading').addClass('hide');
            $('#loading-detail').addClass('hide');
            $('#section-button').removeClass('hide');
            $('#section-detail').removeClass('hide');
            $('#section-schedule').removeClass('hide');
        }
    })
    $("#confirmCancel").modal("toggle");
});

</script>

@include('schedule.help')

@include('schedule.print')

@include('_partial.confirm_accept')

@include('_partial.confirm_cancel')