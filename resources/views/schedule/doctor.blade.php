<div class="modal fade window" id="doctor" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" />
                <h5 class="modal-title" id="title-service">{{ trans('messages.add_doctor') }}</h5>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <form id="form-doctor">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id">
                    <input type="hidden" name="type_kunjungan">
                    <div class="form-group content-column" id="polyclinic-group">
                        <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="polyclinic" disabled="disabled">
                                <option value="">{{ trans('messages.select_polyclinic') }}</option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group content-column" id="doctor-group">
                        <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="doctor" disabled="disabled">
                                <option value="">{{ trans('messages.no_doctor') }}</option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group content-column date-column date-disabled" id="date-group" style="margin-bottom: 35px;">
                        <label class="control-label">{{ trans('messages.date') }} <span>*</span></label>
                        <div id="date-section">
                            <div id="datepicker_date" class="input-group date">
                                <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="date" disabled="disabled" />
                                <span class="input-group-addon">
                                    <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                </span>
                            </div>
                        </div>
                        <span class="help-block"></span>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                    </div>
                    <div class="form-group content-column" id="schedule-group">
                        <label class="control-label">{{ trans('messages.practice_hour') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="schedule" disabled="disabled">
                                <option value="">{{ trans('messages.no_schedule') }}</option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function listPolyPopup(val) {
    loading_content("#form-doctor #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            


            if(!dataPoly.error) {
                loading_content("#form-doctor #polyclinic-group", "success");

                var item = dataPoly.data;
                
                $("#form-doctor select[name=polyclinic]").prop("disabled", true);

                $("#form-doctor select[name='polyclinic']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-doctor select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-doctor select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-doctor select[name=polyclinic]").val(val).trigger("change");
                    listDoctorPopup(val);
                }
                
            } else {
                loading_content("#form-doctor #polyclinic-group", "failed");

                $("#form-doctor #polyclinic-group #loading-content").click(function(){ listPolyPopup(val); });
            }
        },
        error: function(){
            loading_content("#form-doctor #polyclinic-group", "failed");

            $("#form-doctor #polyclinic-group #loading-content").click(function(){ listPolyPopup(val); });
        }
    })
}

$('#form-doctor select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;
    
    listDoctorPopup(polyclinic);
});

function listDoctorPopup(val) {
    resetSchedule();

    loading_content("#form-doctor #doctor-group", "loading");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+val+"&active=true&is_bpjs=true",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-doctor #doctor-group", "success");

                $("#form-doctor select[name=schedule]").html("");
                $("#form-doctor select[name=schedule]").prop("disabled", true);
                $("#form-doctor select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                $("#form-doctor select[name=schedule]").select2({
                    placeholder: "{{ trans('messages.no_schedule') }}",
                    allowClear: false
                });

                var item = data.data;

                $("#form-doctor select[name=doctor]").html("");

                if(item.length==0 || val=="") {
                    $("#form-doctor select[name=doctor]").prop("disabled", true);
                    $("#form-doctor select[name=doctor]").append("<option value=''>{{ trans('messages.no_doctor') }}</option>");
                    $("#form-doctor select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.no_doctor') }}",
                        allowClear: false
                    });
                } else {
                    $("#form-doctor select[name=doctor]").prop("disabled", false);

                    $("#form-doctor select[name=doctor]").append("<option value=''>{{ trans('messages.select_doctor') }}</option>");

                    for (var i = 0; i < item.length; i++) {

                        $("#form-doctor select[name=doctor]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-doctor select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}",
                        allowClear: false
                    });
                }
            } else {
                loading_content("#form-doctor #doctor-group", "failed");

                $("#form-doctor #doctor-group #loading-content").click(function(){ listDoctorPopup(val); });
            }
             
        },
        error: function(){
            loading_content("#form-doctor #doctor-group", "failed");

            $("#form-doctor #doctor-group #loading-content").click(function(){ listDoctorPopup(val); });
        }
    });
    $('#doctor').modal('show');
}

function resetSchedule() {
    $("#form-doctor #appointment-group").addClass("hide");
    $("#form-doctor input[name='set_appointment']").prop("checked",false);
    $("#form-doctor input[name='appointment']").prop("disabled",true);
    $("#form-doctor input[name='appointment']").val("");
}

$('#form-doctor select[name=doctor]').on('change', function() {
    var doctor = this.value;

    listSchedule(doctor);
});

var doctorEndDate = "31-12-9999";
var doctorStartLeaveDate = "";
var doctorStartEndLeaveDate = "";

function listSchedule(val) {
    resetSchedule();

    doctorEndDate = "9999-12-31";
    doctorStartLeaveDate = "";
    doctorStartEndLeaveDate = "";

    var date = convertDate($("#form-doctor input[name='date']").val());
    var d = new Date(date);
    var n = d.getDay();
    n == 0 ? n = 7 : n = n;

    if(val!="") {
        loading_content("#form-doctor #schedule-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+val+"?day="+n,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                

                if(!data.error) {
                    loading_content("#form-doctor #schedule-group", "success");
                    if(data.schedule!=null) {
                        item = data.schedule.daily;

                        //doctorEndDate = data.schedule.end_date_active;
                        doctorStartLeaveDate = data.schedule.start_date_leave;
                        doctorEndLeaveDate = data.schedule.end_date_leave;

                        $("#form-doctor select[name=schedule]").html("");

                        if(item.length==0) {
                            $("#form-doctor select[name=schedule]").prop("disabled", true);
                            $("#form-doctor select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                            $("#form-doctor select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.no_schedule') }}"
                            });
                        } else {
                            $("#form-doctor select[name=schedule]").prop("disabled", false);
                            $("#form-doctor select[name=schedule]").append("<option value=''></option>");

                            var checkSchedule = false;
                            for (var i = 0; i < item.length; i++) {
                                $("#form-doctor select[name=schedule]").append("<option data-day='"+item[i].day+"' data-appointment='"+item[i].appointment+"' value='"+item[i].id+"'>"+getDay(item[i].day)+" ("+convertTime(item[i].start_time)+" - "+convertTime(item[i].end_time)+")</option>");
                            }

                            $("#form-doctor select[name=schedule]").select2({
                                placeholder: "{{ trans('messages.select_practice_hour') }}"
                            });
                            
                        }

                    } else {
                        $("#form-doctor select[name=schedule]").html("");
                        $("#form-doctor select[name=schedule]").prop("disabled", true);
                        $("#form-doctor select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
                        $("#form-doctor select[name=schedule]").select2({
                            placeholder: "{{ trans('messages.no_schedule') }}"
                        });
                    }
                } else {
                    loading_content("#form-doctor #schedule-group", "failed");

                    $("#form-doctor #schedule-group #loading-content").click(function(){ listSchedule(val); });
                }
                
            },
            error: function(){
                loading_content("#form-doctor #schedule-group", "failed");

                $("#form-doctor #schedule-group #loading-content").click(function(){ listSchedule(val); });
            }
        });
    } else {
        $("#form-doctor select[name=schedule]").html("");
        $("#form-doctor select[name=schedule]").prop("disabled", true);
        $("#form-doctor select[name=schedule]").append("<option value=''>{{ trans('messages.no_schedule') }}</option>");
        $("#form-doctor select[name=schedule]").select2({
            placeholder: "{{ trans('messages.no_schedule') }}"
        });
    }
}

$("#form-doctor").submit(function(event) {
    resetValidation('form-doctor #doctor', 'form-doctor #schedule');

    event.preventDefault();
    $("#form-doctor button").attr("disabled", true);

    formData= new FormData();

    var id = $("#form-doctor input[name='id']").val();
    var type_kunjungan = $("#form-doctor input[name='type_kunjungan']").val();

    if(type_kunjungan == 'inpatient'){
        formData.append("id_doctor", $("#form-doctor select[name='doctor'] option:selected").val());
    }else{
        formData.append("id_doctor", $("#form-doctor select[name='doctor'] option:selected").val());
        formData.append("id_doctor_daily_schedule", $("#form-doctor select[name='schedule'] option:selected").val());
    }

    $("#form-doctor .btn-primary").addClass("loading");
    $("#form-doctor .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/schedule/update-doctor/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-doctor button").attr("disabled", false);
    
            $("#form-doctor .btn-primary").removeClass("loading");
            $("#form-doctor .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {

                    if(data.errors) {
                        notif(false,""+data.errors.schedule+"");
                    }else{
                        if(data.message) {
                            notif(false,""+data.message+"");
                        }
                    }

                    if($("#form-doctor select[name='doctor'] option:selected").val()=='') {
                        formValidate(true, ['form-doctor #doctor','{{ trans("validation.empty_patient") }}', true], ['form-doctor #schedule',data.errors.schedule, true]);
                    } else {
                        formValidate(true, ['form-doctor #doctor','', true], ['form-doctor #schedule',data.errors.schedule, true]);
                    }
                    

                } else {
                    notif(true,'Pendaftaran berhasi ditambahkan!');
                    $('#doctor').modal('hide');
                    $('.modal-backdrop').remove();
                    $('body').removeClass( "modal-open" );

                    setTimeout(function(){
                        loading('#/{{ $lang }}/schedule/'+data.id);
                        redirect('#/{{ $lang }}/schedule/'+data.id);                
                    },500);
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-doctor button").attr("disabled", false);

            $("#form-doctor .btn-primary").removeClass("loading");
            $("#form-doctor .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>