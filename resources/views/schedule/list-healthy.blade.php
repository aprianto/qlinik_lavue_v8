@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 
<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/rawat-jalan.png') }}" />
                    <h3>
                        Pendaftaran
                    </h3>
                    <a href="#/{{ $lang }}/help/schedule"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/schedule/sick" onclick="loading($(this).attr('href'));">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-klinik.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Kunjungan Sakit</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/schedule/healthy" onclick="loading($(this).attr('href'));" class="active">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/tanda-vital.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Kunjungan Sehat</span>
                        </div>
                    </a>
                    @if($level != 'doctor')
                    <a href="#/{{ $lang }}/schedule/create" onclick="loading($(this).attr('href'));">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Buat Pendaftaran</span>
                        </div>
                    </a>
                    @endif
                    @if(Session::get('pcare'))
                    <a onclick="syncSchedule()">
                        <div class="image">
                            <div class="canvas">
                                <i class="fa fa-refresh" id="sync"></i>
                            </div>
                        </div>
                        <div class="text">
                            <span>Sync Pendaftaran</span>
                        </div>
                    </a>
                    @endif
                    <div class="info-sec">
                        <h1 id="total">0</h1>
                        <h3>{{ trans("messages.total") }}</h3>
                    </div>
                </div>
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="polyclinic-group">
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-3 form-change ">
                                <div class="form-group content-column" id="doctor-group">
                                    <div class="border-group">
                                        <select class="form-control" name="doctor">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span> 
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="status-group">
                                    <div class="border-group">
                                        <select class="form-control" name="status">
                                            <option value=""></option>
                                            <option value="all">{{ trans("messages.all_status") }}</option>
                                            <option value="1">{{ trans("messages.pending") }}</option>                          
                                            <option value="-1">{{ trans("messages.cancel") }}</option>
                                            <option value="11">{{ trans("messages.not_served") }}</option>
                                            <option value="5">{{ trans("messages.done") }}</option>                         
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group content-column" id="doctor-group">
                                    <div class="border-group">
                                        <select class="form-control" name="type_kunjungan">
                                            <option value=""></option>
                                            <option value="outpatient">Rawat Jalan</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span> 
                                </div>
                            </div>
                        </div>
                        <div class="row search">
                            <div class="col-md-3 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Cari nama pasien, nomor rekam medis, nomor urut" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.patient') }}</th>
                                <th class="hide-lg">{{ trans('messages.doctor') }}</th>
                                <th class="hide-md" width="140px">{{ trans('messages.schedule') }}</th>
                                <th class="hide-md" width="120px">No. Urut / BPJS</th>
                                <th class="hide-md" width="140px">Perawatan</th>
                                <th class="hide-lg">{{ trans('messages.status') }}</th>
                                <th class="hide" id="action"></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>


<script type="text/javascript">
/*
var pusher = new Pusher('{{ $pusher_key }}');

var id_pusher = "";
id_pusher = "{{ $id_clinic }}";

var channelVerification = pusher.subscribe('schedule');
channelVerification.bind('schedule-{{ $level }}-'+id_pusher, function(data) {
    if(data.status.code==true) {
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+'').addClass('row-green');
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' td:first-child').html("<i class='fa fa-check-circle status-success status-text'></i>");
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #link-medical_records').attr("href","#/{{ $lang }}/medical/"+data.id_medical_records);
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #link-edit').remove();

        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #status').html("<span class='status-done'>{{ trans('messages.done') }}</span>");
    }
});*/

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();
dateNowSecond = date.getFullYear()+'-'+monthNow+'-'+dayNow;

$("#form-search select[name='status']").select2({
    placeholder: "{{ trans('messages.select_status') }}",
    allowClear: false
});

$("#form-search select[name='type_kunjungan']").select2({
    placeholder: "Pilih Jenis Perawatan",
    allowClear: false
});


$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day});
    });
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day});
    });
});

$("#form-search input[name='q']").val(searchRepo);
$("#form-search select[name='status']").val(statusRepo).trigger("change");
$("#form-search input[name='from_date']").val(unconvertDate(from_date_nowRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_date_nowRepo));

@if( $level != 'doctor' )

function listPoly(val) {
    loading_content("#form-search #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            


            if(!dataPoly.error) {
                loading_content("#form-search #polyclinic-group", "success");

                var item = dataPoly.data;

                $("#form-search select[name='polyclinic']").html("<option value=''></option><option value='all'>{{ trans('messages.all_polyclinic') }}</option>");
                for (var i = 0; i < item.length; i++) {
                    $("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-search select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-search select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-search #polyclinic-group", "failed");

                $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-search #polyclinic-group", "failed");

            $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listDoctor(polyclinic, val) {
    loading_content("#form-search #doctor-group", "loading");

    var search = "";
    if(polyclinic!="") {
        search = "?id_polyclinic="+polyclinic;
    }

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor"+search,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataDoctor){
            

            if(!dataDoctor.error) {
                loading_content("#form-search #doctor-group", "success");

                var item = dataDoctor.data;

                $("#form-search select[name=doctor]").html("");

                if(item.length==0) {
                    $("#form-search select[name=doctor]").prop("disabled", true);
                    $("#form-search select[name=doctor]").append("<option value=''>{{ trans('messages.no_doctor') }}</option>");
                    $("#form-search select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.no_doctor') }}",
                        allowClear: false
                    });
                } else {
                    $("#form-search select[name=doctor]").prop("disabled", false);

                    $("#form-search select[name=doctor]").append("<option value=''></option><option value='all'>{{ trans('messages.all_doctor') }}</option>");

                    for (var i = 0; i < item.length; i++) {
                        $("#form-search select[name=doctor]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }

                    $("#form-search select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}",
                        allowClear: false
                    });

                    if(val!="") {
                        $("#form-search select[name='doctor']").val(val).trigger("change");
                    }
                }
            } else {
                loading_content("#form-search #doctor-group", "failed");

                $("#form-search #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
            }
             
        },
        error: function(){
            loading_content("#form-search #doctor-group", "failed");

            $("#form-search #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
        }
    });
}

listPoly(polyclinicRepo);
listDoctor(polyclinicRepo, doctorRepo);
@else
    $("#form-search select[name='polyclinic']").html("<option>{{$name_polyclinic}}</option>");
    $("#form-search select[name=polyclinic]").prop("disabled", true);
    $("#form-search select[name='doctor']").html("<option>{{$name_doctor}}</option>");
    $("#form-search select[name=doctor]").prop("disabled", true);
@endif

$('#form-search select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;
    if(polyclinic == "all") {
        polyclinic = "";
    }
    filterSearch();
    listDoctor(polyclinic, doctorRepo);
});

$("#form-search select[name='doctor']").on('change', function(){
    filterSearch()
});

$("#form-search select[name='status']").on('change', function(){
    filterSearch()
});

$("#form-search select[name='type_kunjungan']").on('change', function(){
    filterSearch()
});

let timer = ""; 
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer)
    if (e.target.value.length > 2 || e.target.value.length == 0){
        timer = setTimeout(() => {
            filterSearch();
        }, 2000);
    }
});

$("#form-search").submit(function(event) {
    filterSearch();
});

function filterSearch(arg) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val()
    var doctor = $("#form-search select[name=doctor] option:selected").val();
    var status = $("#form-search select[name=status] option:selected").val();
    var type_kunjungan = $("#form-search select[name=type_kunjungan] option:selected").val();

    if (arg) {
        if(arg.from_day) {
            from_date = arg.from_day
        }else if(arg.to_day) {
            to_date = arg.to_day
        }
    }

    if(polyclinic == "all") {
        polyclinic = "";
    }else if(doctor == "all") {
        doctor = "";
    }else if(status == "all") {
        status = "";
    }

    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, status, type_kunjungan, convertDate(from_date), convertDate(to_date));
}


var keySearch = searchRepo;
var keyPoly = polyclinicRepo;
var keyDoctor = doctorRepo;
var keyFromDate = from_date_nowRepo;
var keyToDate = to_date_nowRepo;
var keyStatus = statusRepo;
var keyType_kunjungan = "";
var numPage = pageRepo;


function search(page, submit, q, polyclinic, doctor, status, type_kunjungan, from_date, to_date) {
    if(dateNow==$("#form-search input[name='from_date']").val() && dateNow==$("#form-search input[name='to_date']").val()) {
        $("#list-title").html("{{ trans('messages.schedule') }} {{ strtolower(trans('messages.today')) }}");    
    } else {
        $("#list-title").html("{{ trans('messages.schedule') }}");
    }

    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if(doctor=="" && submit=="false") {
        var doctor = keyDoctor;
    }

    if(status=="" && submit=="false") {
        var status = keyStatus;
    }

    if(type_kunjungan=="" && submit=="false") {
        var type_kunjungan = keyType_kunjungan;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyPoly = polyclinic;
    keyDoctor = doctor;
    keyStatus = status;
    keyType_kunjungan = type_kunjungan;
    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;
    per_page = 10;
    order = "asc";
    type = "kunjungan_sehat";
        
    repository(['search',q],['order',order],['polyclinic',polyclinic],['doctor',doctor],['status',status],['type_kunjungan',type_kunjungan],['from_date_now',from_date],['to_date_now',to_date],['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule",
        type: "GET",
        data: "q="+q+"&order="+order+"&id_polyclinic="+polyclinic+"&id_doctor="+doctor+"&status="+status+"&type_kunjungan="+type_kunjungan+"&from_date="+from_date+"&to_date="+to_date+"&page="+page+"&medic=false&per_page="+per_page+"&type="+type,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                $("#total").html(total);

                if(total>0) {
                    $(".pagination-sec").removeClass("hide");
                    $("#action").removeClass("hide");
                    let item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(i==0) {
                                tr = $('<tr class="table-header" />');
                                if(item[i].date==convertDate(dateNow)) {
                                    tr.append("<td colspan='2'>{{ trans('messages.today') }}</td>");
                                } else {
                                    tr.append("<td colspan='2'>"+getFormatDate(item[i].date)+"</td>");
                                }
                                tr.append("<td colspan='5' class='hide-md header-right'>{{ trans('messages.total_patient') }}: "+item[i].group_patient+"<br class='show-md' /><span class='hide-lg'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>{{ trans('messages.total_doctor') }}: "+item[i].group_doctor+"</td>");
                                //tr.append("<td colspan='5' class='hide-lg'>{{ trans('messages.total_doctor') }}: "+item[i].group_doctor+"</td>");
                                $("#table").append(tr);
                            }

                            var row = "";
                            var status_row = "";
                            
                            /*if(item[i].status.code==5) {
                                row = "row-green";
                                status_row = "<i class='fa fa-check-circle status-success status-text'></i>";
                            }*/

                            var id_schedule = "";
                            if(item[i].status.code==2) {
                                id_schedule = ' id="schedule_'+item[i].id_clinic+'_'+item[i].id_patient+'_'+item[i].id_doctor+''+item[i].id_polyclinic+''+item[i].date+'"';
                            }

                            var date_schedule = new Date(item[i].date);
                            var day_schedule = date_schedule.getDay();

                            var time = "";
                            if(item[i].appointment!=null) {
                                if(item[i].start_time != null){
                                    time = "<span class='text'>"+convertTime(item[i].start_time)+"</span>";
                                }
                            } else {
                                if(item[i].start_time != null){
                                    time = "<span class='text'>"+convertTime(item[i].start_time)+" - "+convertTime(item[i].end_time)+"</span>";
                                }
                            }

                            number = "<span>-</span>";
                            if(item[i].number!=null) {
                                number = "<span>" + item[i].number + "</span>";
                            }

                            number_bpjs = "";
                            if(item[i].number_bpjs!=null) {
                                number_bpjs = "<span> / " + item[i].number_bpjs + "</span>";
                            }

                            number = number + number_bpjs;

                            type_kunjungan = "";
                            if(item[i].type_kunjungan!=null) {
                                if(item[i].type_kunjungan == "outpatient"){
                                    type_kunjungan = "<span>Rawat Jalan</span>";
                                }else if(item[i].type_kunjungan == "inpatient"){
                                    type_kunjungan = "<span>Rawat Inap</span>";
                                }else if(item[i].type_kunjungan == "promotif preventif"){
                                    type_kunjungan = "<span>Promotif Preventif</span>";
                                }
                            }

                            var status = "";
                            var paid = "";
                            
                            var action = "";

                            /*if(item[i].payment==true) {
                                paid = '<i class="fa fa-check-circle payment-success"></i>';
                            } else {
                                paid = '<i class="fa fa-minus-circle payment-failed"></i>';
                            }*/

                            if(Date.parse(item[i].date.replace(/\-/g,'/'))>=Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                                if(item[i].appointment!=null && item[i].appointment!='') {
                                    if(item[i].status.code == 1) {
                                        action = action+"<a data-toggle='modal' data-target='#confirmAccept' data-message=\"{{ trans('messages.info_confirmation') }}\" data-id='"+item[i].id+"' class='btn-table btn-green'><img src=\"{{ asset('assets/images/icons/action/activate.png') }}\" /> {{ trans('messages.confirmation') }}</a>"+
                                        "<a data-toggle='modal' data-target='#confirmCancel' data-message=\"{{ trans('messages.info_canceled') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/deactivate.png') }}\" /> {{ trans('messages.canceled') }}</a>";
                                    }
                                    if(item[i].status.code == 1 || item[i].status.code == 2) {
                                        status = "<span class='status-not-served'>{{ trans('messages.not_served') }}</span>";
                                    } else {
                                        status = confirmation(item[i].status.code);
                                    }
                                } else {
                                    if(item[i].status.code==5) {
                                        status = confirmation(item[i].status.code);
                                    } else if(item[i].status.code == 1 || item[i].status.code == 2) {
                                        status = "<span class='status-not-served'>{{ trans('messages.not_served') }}</span>";
                                    }
                                }                                

                                @if(isset($acl->medical_records))
                                    @if($acl->medical_records->read==true)
                                        if(item[i].status.code==2) {
                                            if(Date.parse(item[i].date.replace(/\-/g,'/'))==Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                                                action = action+"<a id='link-medical_records' href='#/{{ $lang }}/medical/create/schedule/"+item[i].id+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/med-rec.png') }}\" /></a>";
                                            }
                                        }
                                    @endif
                                @endif

                                @if(isset($acl->medical_records))
                                    @if($acl->medical_records->read==true)
                                        if(item[i].status.code==5) {
                                            action = action+"<a id='link-medical_records' href='#/{{ $lang }}/medical/"+item[i].id_medical_records+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/med-rec.png') }}\" /></a>";
                                        }
                                    @endif
                                @endif

                                if(item[i].status.code==1) {
                                    action = action+"<a data-toggle='modal' data-target='#confirmAccept' data-message=\"{{ trans('messages.info_confirmation') }}\" data-id='"+item[i].id+"' class='btn-table btn-green'><img src=\"{{ asset('assets/images/icons/action/activate.png') }}\" /> {{ trans('messages.confirmation') }}</a>"+
                                    "<a data-toggle='modal' data-target='#confirmCancel' data-message=\"{{ trans('messages.info_canceled') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/deactivate.png') }}\" /> {{ trans('messages.canceled') }}</a>";
                                }

                                action = action+"<a href='#/{{ $lang }}/schedule/"+item[i].id+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /></a>";

                                if(item[i].status.code!=-1 && item[i].status.code!=5 && item[i].id_doctor == '' && item[i].id_doctor_daily_schedule == '') {
                                    action = action+"<a id='link-edit' href='#/{{ $lang }}/schedule/"+item[i].id+"/edit' class='btn-table btn-orange' onclick='loading($(this).attr(href))'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /></a>";
                                }
                            } else {
                                if(item[i].status.code==5) {
                                    status = "<span class='status-done'>{{ trans('messages.done') }}</span>";

                                    @if(isset($acl->medical_records))
                                        @if($acl->medical_records->read==true)
                                            action = action+"<a id='link-medical_records' href='#/{{ $lang }}/medical/"+item[i].id_medical_records+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/med-rec.png') }}\" /></a>";
                                        @endif
                                    @endif
                                } else if(item[i].status.code == 1 || item[i].status.code == 2) {
                                    status = "<span class='status-not-served'>{{ trans('messages.not_served') }}</span>";
                                }

                                @if(isset($acl->medical_records))
                                    @if($acl->medical_records->read==true)
                                        if(item[i].status.code==2) {
                                            if(Date.parse(item[i].date.replace(/\-/g,'/'))<=Date.parse(yearNow+'/'+monthNow+'/'+dayNow)) {
                                                action = action+"<a id='link-medical_records' href='#/{{ $lang }}/medical/create/schedule/"+item[i].id+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/med-rec.png') }}\" /></a>";
                                            }
                                        }
                                    @endif
                                @endif

                                action = action+"<a href='#/{{ $lang }}/schedule/"+item[i].id+"' class='btn-table btn-blue'  onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /></a>";
                            }

                            var medical_records;
                            if(item[i].id_medical_records!=null){
                                medical_records = 'hide';
                            }else{
                                medical_records = '';
                            }

                            action = action+"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red "+medical_records+"'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /></a>";

                            tr = $('<tr class="'+row+'" '+id_schedule+' />');
                            //tr.append("<td width='20px' class='hide-lg'>"+status_row+"</td>");

                            //tr.append("<td class='photo'>"+thumbnail('sm','patient','{{ $storage }}',item[i].patient.photo,item[i].patient.name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/patient/"+item[i].patient.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].patient.name + "</h1></a><span class='type'>" + item[i].patient.mrn + "</span>"+
                                '<div class="sub show-md">'+
                                    (item[i].doctor!=null ? "<span class='info'><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></span>" : "")+
                                    "<span class='general show-sm'>"+getDay(day_schedule)+", " + getFormatDate(item[i].date) +" ("+time+")</span>"+
                                    "<span class='general show-sm'>"+number+"</span>"+
                                    "<span class='general show-sm'>"+type_kunjungan+"</span>"+
                                    '<span class="info margin right">' + status + '</span>'+
                                    //'<span class="info margin right">{{ trans("messages.paid") }}: ' + paid + '</span>'+
                                '</div>'+
                                "</td>");

                            //tr.append("<td class='photo hide-lg'>"+thumbnail('sm','doctor','{{ $storage }}',item[i].doctor.photo,item[i].doctor.name)+"</td>");
                            if(item[i].doctor!=null){
                                tr.append("<td class='hide-lg'><a href='#/{{ $lang }}/doctor/"+item[i].doctor.id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].doctor.name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></td>");
                            }else{
                                tr.append("<td class='hide-lg'><a onclick='loadDoctor(\""+item[i].id_polyclinic+"\",\""+item[i].date+"\",\""+item[i].type_kunjungan+"\",\""+item[i].id+"\")' class='btn-table btn-blue'><img src='{{ asset('assets/images/icons/info/doctor-name.png') }}' />&nbsp; Pilih tenaga Medis</a></td>");
                            }

                            tr.append("<td class='hide-md'><h2>"+getDay(day_schedule)+"</h2><br /><span class='text'>" + getFormatDate(item[i].date) +"</span><br /><span class='text'>"+time+"</span></td>");
                            tr.append("<td class='hide-md'>"+number+"</td>");
                            tr.append("<td class='hide-md'>"+type_kunjungan+"</td>");
                            tr.append("<td id='status' class='hide-lg'>"+status+"</td>");
                            //tr.append("<td id='paid' class='hide-lg' align='center'>"+paid+"</td>");

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            if(i<to) {
                                if(item[i].date!=item[i+1].date) {
                                    tr = $('<tr class="table-header" />');
                                    if(item[i+1].date==convertDate(dateNow)) {
                                        tr.append("<td colspan='2'>{{ trans('messages.today') }}</td>");
                                    } else {
                                        tr.append("<td colspan='2'>"+getFormatDate(item[i+1].date)+"</td>");
                                    }
                                    tr.append("<td colspan='5' class='hide-md header-right'>{{ trans('messages.total_patient') }}: "+item[i+1].group_patient+"<br class='show-md' /><span class='hide-lg'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span> {{ trans('messages.total_doctor') }}: "+item[i+1].group_doctor+"</td>");
                                    //tr.append("<td colspan='2' class='hide-md'>{{ trans('messages.total_patient') }}: "+item[i+1].group_patient+"</td>");
                                    //tr.append("<td colspan='4' class='hide-lg'>{{ trans('messages.total_doctor') }}: "+item[i+1].group_doctor+"</td>");
                                    $("#table").append(tr);
                                }   
                            }

                            no++;
                        } catch(err) {
                        }
                    }
                    } else {
                    $(".pagination-sec").addClass("hide");

                    if(dateNow==$("#form-search input[name='from_date']").val() && dateNow==$("#form-search input[name='to_date']").val()) {
                        tr = $('<tr class="table-header" />');
                        tr.append("<td colspan='6'>{{ trans('messages.today') }}</td>");
                        $("#table").append(tr);
                        //$("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }} {{ strtolower(trans('messages.today')) }}</td></tr>"); 
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");    
                    } else {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");
                    }

                }

                pages(page, total, per_page, current_page, last_page, from, to, q, polyclinic, doctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, doctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, doctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate); });
        }
    })
}

function loadDoctor(polyclinic, date, type_kunjungan, id){
    listPolyPopup(polyclinic);
    $('#form-doctor input[name=date]').val(unconvertDate(date));
    $('#form-doctor input[name=type_kunjungan]').val(type_kunjungan);
    $('#form-doctor input[name=id]').val(id);
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyPoly, keyDoctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate);
                    notif(true,"{{ trans('validation.success_delete_schedule') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$('#confirmAccept').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"/status/2",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyPoly, keyDoctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate);
                    notif(true,"{{ trans('validation.success_confirmation') }}");

                    if(help_action_schedule==1) {
                        info_help_confirm_schedule_appointment(
                            data.doctor.name, 
                            data.patient.name, 
                            data.polyclinic.name, 
                            data.clinic.name, 
                            data.date, 
                            data.appointment
                        );
                    }
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmAccept").modal("toggle");
});

$('#confirmCancel').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/schedule/"+id+"/status/-1",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyPoly, keyDoctor, keyStatus, keyType_kunjungan, keyFromDate, keyToDate);
                    notif(true,"{{ trans('validation.success_cancel') }}");

                    if(help_action_schedule==1) {
                        info_help_cancel_schedule_appointment(
                            data.doctor.name, 
                            data.patient.name, 
                            data.polyclinic.name, 
                            data.clinic.name, 
                            data.date, 
                            data.appointment
                        );
                    }  
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmCancel").modal("toggle");
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        var doctor = $("#form-search select[name=doctor] option:selected").val();
        if(doctor=="all") {
            doctor = "";
        }

        var status = $("#form-search select[name=status] option:selected").val();
        if(status=="all") {
            status = "";
        }

        var type_kunjungan = $("#form-search select[name=type_kunjungan] option:selected").val();

        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, status, type_kunjungan, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        var doctor = $("#form-search select[name=doctor] option:selected").val();
        if(doctor=="all") {
            doctor = "";
        }

        var status = $("#form-search select[name=status] option:selected").val();
        if(status=="all") {
            status = "";
        }

        var type_kunjungan = $("#form-search select[name=type_kunjungan] option:selected").val();

        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, doctor, status, type_kunjungan, convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false",searchRepo,polyclinicRepo,doctorRepo,'', '',from_date_nowRepo, to_date_nowRepo);

function syncSchedule(){
    $('#sync').addClass('fa-spin');
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/syncronize-pendaftaran?from_date="+dateNowSecond+"&to_date="+dateNowSecond,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
            $('#sync').removeClass('fa-spin');

            if(!data.error) {
                if(data.success==true) {
                    notif(true,"Data berhasil disinkronkan");
                    search(pageRepo,"false",searchRepo,polyclinicRepo,doctorRepo,'', '',from_date_nowRepo, to_date_nowRepo);
                } else {
                    if(data.message){
                        notif(false,data.message);
                    }
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
            $('#sync').removeClass('fa-spin');
        }
    })
}
</script>

@include('schedule.help')

@include('_partial.confirm_delele')

@include('_partial.confirm_accept')

@include('_partial.confirm_cancel')

@include('schedule.doctor')