@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_patient') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    @if(isset($bpjs))
                    <form role="form" class="sec" id="form-add">
                        <h3 class="group-label first-label">{{ trans('messages.personal_data') }}</h3>
                        <input type="hidden" name="id_patient" value="" />                        
                        <div class="form-group hide" id="profile-patient">
                            <div class="detail-profile-form">
                                <div class="column" id="patient-photo">
                                    <img src='{{ $storage."/images/patient/photo.png" }}'>
                                </div>
                                <div class="column">
                                    <h1 id="patient-name"></h1>
                                    <p id="patient-email"></p>
                                    <p id="patient-phone"></p>
                                </div>
                                <a class="btn-white" onclick="resetPatient();">+ {{ trans("messages.add_other_patient") }}</a>
                            </div>
                        </div>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.full_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_full_name') }}" name="name" value="{{ $bpjs->data->nama }}">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} </label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email">
                            <span class="help-block"></span>
                            <span id="loading-email" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)" value="{{ $bpjs->data->noHP }}">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                            <span id="loading-phone" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group column-group" id="birth_date-group">
                                    <label class="control-label">{{ trans('messages.birth_date') }} <span>*</span></label>
                                    <div id="datepicker_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="birth_date" value="{{ $bpjs->data->tglLahir }}" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.age') }}</label>
                                    <input type="text" class="form-control input-text" placeholder="-" name="age" value="{{ date_diff(date_create($bpjs->data->tglLahir), date_create('today'))->y }}" disabled="disabled">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="gender-group">
                            <label class="control-label">{{ trans('messages.gender') }} <span>*</span></label>
                            <div class="sf-radio"> 
                                @if($bpjs->data->sex == 'P')
                                <label><input type="radio" value="0" name="gender"><span></span> {{ trans('messages.male') }}</label>
                                <label><input type="radio" value="1" name="gender" checked><span></span> {{ trans('messages.female') }}</label>
                                @else
                                <label><input type="radio" value="0" name="gender" checked><span></span> {{ trans('messages.male') }}</label>
                                <label><input type="radio" value="1" name="gender"><span></span> {{ trans('messages.female') }}</label>
                                @endif
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="id_type-group">
                                    <label class="control-label">{{ trans('messages.identity_card_type') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="id_type">
                                            <option value="">{{ trans('messages.select_card_type') }}</option>
                                            <option value="KTP" selected>{{ trans('messages.KTP') }}</option>
                                            <option value="KIA">KIA</option>
                                            <!-- <option value="SIM">{{ trans('messages.SIM') }}</option> -->
                                            <option value="KK">{{ trans('messages.KK') }}</option>                          
                                            <!-- <option value="Passport">{{ trans('messages.passport') }}</option> -->
                                            <option value="other">{{ trans('messages.other') }}</option>
                                        </select>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="id_number-group">
                                    <label class="control-label">{{ trans('messages.identity_number') }} <span>*</span></label>
                                    @if(strlen($id) != 16)
                                    <input type="text" class="form-control" placeholder="<?php echo trans('messages.input_identity_number'); ?>" value="{{ $bpjs->data->noKTP }}" name="id_number">
                                    @else
                                    <input type="text" class="form-control" placeholder="<?php echo trans('messages.input_identity_number'); ?>" value="{{ $id }}" name="id_number">
                                    @endif
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.blood_type') }}</label>
                                    <select class="form-control" name="blood_type">
                                        <option value="">{{ trans('messages.select_blood_type') }}</option>
                                        @if($bpjs->data->golDarah == 'A')                                        
                                        <option value="A" selected>{{ trans('messages.A') }}</option>
                                        <option value="B">{{ trans('messages.B') }}</option>
                                        <option value="AB">{{ trans('messages.AB') }}</option>
                                        <option value="O">{{ trans('messages.O') }}</option>
                                        @endif
                                        @if($bpjs->data->golDarah == 'B')
                                        <option value="A">{{ trans('messages.A') }}</option>
                                        <option value="B" selected>{{ trans('messages.B') }}</option>
                                        <option value="AB">{{ trans('messages.AB') }}</option>
                                        <option value="O">{{ trans('messages.O') }}</option>
                                        @endif
                                        @if($bpjs->data->golDarah == 'AB')
                                        <option value="A">{{ trans('messages.A') }}</option>
                                        <option value="B">{{ trans('messages.B') }}</option>
                                        <option value="AB" selected>{{ trans('messages.AB') }}</option>
                                        <option value="O">{{ trans('messages.O') }}</option>
                                        @endif
                                        @if($bpjs->data->golDarah == '0')
                                        <option value="A">{{ trans('messages.A') }}</option>
                                        <option value="B">{{ trans('messages.B') }}</option>
                                        <option value="AB">{{ trans('messages.AB') }}</option>
                                        <option value="O" selected>{{ trans('messages.O') }}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="smoke-group">
                            <label class="control-label">{{ trans('messages.smoking') }}</label>
                            <div class="sf-radio"> 
                                <label><input type="radio" value="1" name="smoke"><span></span> {{ trans('messages.smoking') }}</label>
                                <label><input type="radio" value="0" name="smoke"><span></span> {{ trans('messages.no_smoking') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>                                                
                        <h3 class="group-label">{{ trans('messages.address') }}</h3>
                        <div class="form-group content-column" id="country-group">
                            <label class="control-label">{{ trans('messages.country') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="country">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="province-group">
                            <label class="control-label">{{ trans('messages.province') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="province" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="regency-group">
                            <label class="control-label">{{ trans('messages.regency') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="regency" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                                
                        <div class="form-group content-column" id="district-group">
                            <label class="control-label">{{ trans('messages.district') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="district" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="village-group">
                            <label class="control-label">{{ trans('messages.village') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="village" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div> 
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.address') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_address') }}" name="address" />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">{{ trans('messages.postal_code') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_postal_code') }}" name="postal_code">
                                </div>                    
                            </div>
                        </div>
                        <h3 class="group-label">{{ trans('messages.patient_data') }}</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="patient_category-group">
                                    <label class="control-label">{{ trans('messages.patient_category') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="patient_category">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a id="icon-add" class="link-add link-label" data-toggle="modal" data-target="#pop-up">+ {{ trans('messages.add_category') }}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="financing-group">
                                    <label class="control-label">{{ trans('messages.financing') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="financing">
                                            <option value="">{{ trans('messages.select_financing') }}</option>
                                            <option value="personal">{{ trans('messages.personal') }}</option>
                                            <option value="agency">{{ trans('messages.agency') }}</option>
                                            <option value="insurance" selected>{{ trans('messages.insurance') }}</option>
                                        </select>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group content-column" id="insurance-group">
                                    <label class="control-label">{{ trans('messages.insurance') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="insurance">
                                            <option value="">{{ trans('messages.select_insurance') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="card_number-group">
                                    <label class="control-label">Nomor Kartu <span>*</span></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nomor Kartu" name="card_number" value="{{ $bpjs->data->noKartu }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ ucwords(trans('messages.save')) }}
                            </button>
                            <a href="#/{{ $lang }}/patient" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                    @else
                    <form role="form" class="sec" id="form-add">
                        <h3 class="group-label first-label">{{ trans('messages.personal_data') }}</h3>
                        <input type="hidden" name="id_patient" value="" />                        
                        <div class="form-group hide" id="profile-patient">
                            <div class="detail-profile-form">
                                <div class="column" id="patient-photo">
                                    <img src='{{ $storage."/images/patient/photo.png" }}'>
                                </div>
                                <div class="column">
                                    <h1 id="patient-name"></h1>
                                    <p id="patient-email"></p>
                                    <p id="patient-phone"></p>
                                </div>
                                <a class="btn-white" onclick="resetPatient();">+ {{ trans("messages.add_other_patient") }}</a>
                            </div>
                        </div>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.full_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_full_name') }}" name="name">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} </label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email">
                            <span class="help-block"></span>
                            <span id="loading-email" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                            <span id="loading-phone" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group column-group" id="birth_date-group">
                                    <label class="control-label">{{ trans('messages.birth_date') }} <span>*</span></label>
                                    <div id="datepicker_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="birth_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.age') }}</label>
                                    <input type="text" class="form-control input-text" placeholder="-" name="age" disabled="disabled">
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="gender-group">
                            <label class="control-label">{{ trans('messages.gender') }} <span>*</span></label>
                            <div class="sf-radio"> 
                                <label><input type="radio" value="0" name="gender"><span></span> {{ trans('messages.male') }}</label>
                                <label><input type="radio" value="1" name="gender"><span></span> {{ trans('messages.female') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="id_type-group">
                                    <label class="control-label">{{ trans('messages.identity_card_type') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="id_type">
                                            <option value="">{{ trans('messages.select_card_type') }}</option>
                                            <option value="KTP">{{ trans('messages.KTP') }}</option>
                                            <option value="SIM">{{ trans('messages.SIM') }}</option>
                                            <option value="KK">{{ trans('messages.KK') }}</option>                          
                                            <option value="Passport">{{ trans('messages.passport') }}</option>
                                            <option value="other">{{ trans('messages.other') }}</option>
                                        </select>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="id_number-group">
                                    <label class="control-label">{{ trans('messages.identity_number') }} <span>*</span></label>
                                    <input type="text" class="form-control" placeholder="<?php echo trans('messages.input_identity_number'); ?>" name="id_number">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{ trans('messages.blood_type') }}</label>
                                    <select class="form-control" name="blood_type">
                                        <option value="">{{ trans('messages.select_blood_type') }}</option>
                                        <option value="A">{{ trans('messages.A') }}</option>
                                        <option value="B">{{ trans('messages.B') }}</option>
                                        <option value="AB">{{ trans('messages.AB') }}</option>
                                        <option value="O">{{ trans('messages.O') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="smoke-group">
                            <label class="control-label">{{ trans('messages.smoking') }}</label>
                            <div class="sf-radio"> 
                                <label><input type="radio" value="1" name="smoke"><span></span> {{ trans('messages.smoking') }}</label>
                                <label><input type="radio" value="0" name="smoke"><span></span> {{ trans('messages.no_smoking') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>                                                
                        <h3 class="group-label">{{ trans('messages.address') }}</h3>
                        <div class="form-group content-column" id="country-group">
                            <label class="control-label">{{ trans('messages.country') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="country">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="province-group">
                            <label class="control-label">{{ trans('messages.province') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="province" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="regency-group">
                            <label class="control-label">{{ trans('messages.regency') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="regency" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                                
                        <div class="form-group content-column" id="district-group">
                            <label class="control-label">{{ trans('messages.district') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="district" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="village-group">
                            <label class="control-label">{{ trans('messages.village') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="village" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div> 
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.address') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_address') }}" name="address" />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">{{ trans('messages.postal_code') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_postal_code') }}" name="postal_code">
                                </div>                    
                            </div>
                        </div>
                        <h3 class="group-label">{{ trans('messages.patient_data') }}</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="patient_category-group">
                                    <label class="control-label">{{ trans('messages.patient_category') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="patient_category">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a id="icon-add" class="link-add link-label" data-toggle="modal" data-target="#pop-up">+ {{ trans('messages.add_category') }}</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="financing-group">
                                    <label class="control-label">{{ trans('messages.financing') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="financing">
                                            <option value="">{{ trans('messages.select_financing') }}</option>
                                            <option value="personal">{{ trans('messages.personal') }}</option>
                                            <option value="agency">{{ trans('messages.agency') }}</option>
                                            <option value="insurance">{{ trans('messages.insurance') }}</option>
                                        </select>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group content-column hide" id="insurance-group">
                                    <label class="control-label">{{ trans('messages.insurance') }} <span>*</span></label>
                                    <div class="border-group">
                                        <select class="form-control" name="insurance">
                                            <option value="">{{ trans('messages.select_insurance') }}</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row hide" id="no_bpjs-group">
                            <div class="col-md-6">
                                <div class="form-group" id="card_number-group">
                                    <label class="control-label">Nomor Kartu <span>*</span></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nomor Kartu" name="card_number">
                                    <span class="help-block"></span>
                                    <small class="text-success hide">Kartu dapat digunakan</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a id="verification" class="link-add link-label"> Verifikasi</a>
                            </div>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ ucwords(trans('messages.save')) }}
                            </button>
                            <a href="#/{{ $lang }}/patient" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window" id="pop-up" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_patient_category') }}</h5>
            </div>
            <form id="form-pop-up">
                <div class="modal-body modal-main alert-notif">               
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.patient_category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_patient_category') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

$("#form-add select[name='blood_type']").select2({
    placeholder: "{{ trans('messages.select_blood_type') }}",
    allowClear: false
});

$("#form-add select[name='id_type']").select2({
    placeholder: "{{ trans('messages.select_card_type') }}",
    allowClear: false
});

$("#form-add select[name='district']").select2({
    placeholder: "{{ trans('messages.select_district') }}",
    allowClear: false
});

$("#form-add select[name='village']").select2({
    placeholder: "{{ trans('messages.select_village') }}",
    allowClear: false
});

$("#form-add select[name='regency']").select2({
    placeholder: "{{ trans('messages.select_regency') }}",
    allowClear: false
});

$("#form-add select[name='province']").select2({
    placeholder: "{{ trans('messages.select_province') }}",
    allowClear: false
});

$("#form-add select[name='country']").select2({
    placeholder: "{{ trans('messages.select_country') }}",
    allowClear: false
});

$("#form-add select[name='financing']").select2({
    placeholder: "{{ trans('messages.select_financing') }}",
    allowClear: false
});

$("#form-add select[name='insurance']").select2({
    placeholder: "Tidak ada data asuransi",
    allowClear: false
});

$("#form-add select[name='phone_code']").wSelect();
$("#form-add select[name='phone_code']").val('62').change();

$('#datepicker_date').datetimepicker({
    locale: "{{ $lang }}",
    viewMode: 'years',
    format: 'DD/MM/YYYY',
});

function listInsurance(val) {
    loading_content("#form-add #insurance-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsurance){

            if(!dataInsurance.error) {
                loading_content("#form-add #insurance-group", "success");

                var item = dataInsurance.data;

                $("#form-add select[name='insurance']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    if(item[i].name.toLowerCase().includes("bpjs")){
                        @if(isset($bpjs))
                        $("#form-add select[name='insurance']").append("<option value='"+item[i].id+"' selected>"+item[i].name+"</option>");
                        @endif
                    }else{
                        $("#form-add select[name='insurance']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }
                }

                $("#form-add select[name='insurance']").select2({
                    placeholder: "{{ trans('messages.select_insurance') }}",
                    allowClear: false
                });

                
            } else {
                loading_content("#form-add #insurance-group", "failed");

                $("#form-add #insurance-group #loading-content").click(function(){ listInsurance(val); });
            }
        },
        error: function(){
            loading_content("#form-add #insurance-group", "failed");

            $("#form-add #insurance-group #loading-content").click(function(){ listInsurance(val); });
        }
    })
}

listInsurance('');

$('#form-add select[name=financing]').on('change', function() {
    var financing = this.value;

    resetValidation('form-add #insurance');

    if(financing=='insurance') {
        $("#form-add select[name='insurance']").val("").trigger("change");
        $("#form-add #insurance-group").removeClass("hide");
        $("#form-add #no_bpjs-group").addClass("hide");
        $("#form-add button").attr("disabled", true);
    } else {
        $("#form-add #insurance-group").addClass("hide");
        $("#form-add #no_bpjs-group").addClass("hide");
        $("#form-add button").attr("disabled", false);
    }
});

$('#form-add select[name=insurance]').on('change', function() {
    var insurance = this.selectedOptions[0].label;
    
    if(insurance.toLowerCase().includes("bpjs")){
        $("#form-add #no_bpjs-group").removeClass("hide");
        $("#form-add button").attr("disabled", true);
    }else{
        $("#form-add #no_bpjs-group").addClass("hide");
        $("#form-add button").attr("disabled", false);
        $("#form-add input[name=card_number]").val("");
    }
});

$("#form-add input[name=card_number]").on('keyup', function() {
    formValidate(true, ['form-add #card_number', '', true]);
    $("#form-add small.text-success").addClass("hide");
});

$('#form-add #verification').on('click', function() {
    var id = $("#form-add input[name='card_number']").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(data.data){
                if(!data.error) {
                    if(data.success==true) {
                        if(data.data.aktif==false){
                            formValidate(true, ['form-add #card_number', 'Nomor tidak valid', true]);
                            $("#form-add small.text-success").addClass("hide");
                            $("#form-add button").attr("disabled", true);
                        }else{
                            if(data.data.tunggakan > 0){
                                formValidate(true, ['form-add #card_number', 'Anda memliki tunggakan sebesar Rp.'+data.data.tunggakan, true]);
                                $("#form-add small.text-success").addClass("hide");
                                $("#form-add button").attr("disabled", true);
                            }else{
                                formValidate(true, ['form-add #card_number', '', true]);
                                $("#form-add small.text-success").removeClass("hide");
                                $("#form-add button").attr("disabled", false);
                            }
                        }
                    }
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            }else{
                formValidate(true, ['form-add #card_number', 'Nomor tidak valid', true]);
                $("#form-add small.text-success").addClass("hide");
                $("#form-add button").attr("disabled", true);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

var get_country = "";
var get_province = "";
var get_regency = "";
var get_district = "";
var get_village = "";
var get_postal_code = "";

function resetData() {
    get_country = "";
    get_province = "";
    get_regency = "";
    get_district = "";
    get_village = "";
    get_postal_code = "";
}

function listPatientCategory(val) {
    loading_content("#form-add #patient_category-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                loading_content("#form-add #patient_category-group", "success");

                var item = data.data;

                $("#form-add select[name=patient_category]").html("<option value=''><option>");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name=patient_category]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-add select[name='patient_category']").select2({
                    placeholder: "{{ trans('messages.select_category') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='patient_category']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #patient_category-group", "failed");

                $("#form-add #patient_category-group #loading-content").click(function(){ listPatientCategory(val); });
            }
        },
        error: function(){
            loading_content("#form-add #patient_category-group", "failed");

            $("#form-add #patient_category-group #loading-content").click(function(){ listPatientCategory(val); });
        }
    });
}

function listCountry(val) {
    loading_content("#form-add #country-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/country",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #country-group", "success");

                var item = data.data;

                $("#form-add select[name='country']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='country']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].name+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='country']").select2({
                    placeholder: "{{ trans('messages.select_country') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='country']").val(val).trigger("change");
                } else {
                    $("#form-add select[name='country']").val("Indonesia").trigger("change");
                }

                
            } else {
                loading_content("#form-add #country-group", "failed");

                $("#form-add #country-group #loading-content").click(function(){ listCountry(val); });
            }
        },
        error: function(){
            loading_content("#form-add #country-group", "failed");

            $("#form-add #country-group #loading-content").click(function(){ listCountry(val); });
        }
    })
}

function listProvince(val, id_country) {
    loading_content("#form-add #province-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/province",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #province-group", "success");

                var item = data.data;

                $("#form-add select[name='province']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='province']").append("<option value='"+checkLocationCase(toTitleCase(item[i].name))+"' data-id='"+item[i].id+"'>"+checkLocationCase(toTitleCase(item[i].name))+"</option>");
                }

                $("#form-add select[name='province']").select2({
                    placeholder: "{{ trans('messages.select_province') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='province']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #province-group", "failed");

                $("#form-add #province-group #loading-content").click(function(){ listProvince(val, id_country); });
            }
        },
        error: function(){
            loading_content("#form-add #province-group", "failed");

            $("#form-add #province-group #loading-content").click(function(){ listProvince(val, id_country); });
        }
    })
}

function listRegency(val, id_province) {
    loading_content("#form-add #regency-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/regency?id_province="+id_province,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #regency-group", "success");

                var item = data.data;

                $("#form-add select[name='regency']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='regency']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='regency']").select2({
                    placeholder: "{{ trans('messages.select_regency') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='regency']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-add #regency-group", "failed");

                $("#form-add #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
            }
        },
        error: function(){
            loading_content("#form-add #regency-group", "failed");

            $("#form-add #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
        }
    })
}

function listDistrict(val, id_regency) {
    loading_content("#form-add #district-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/district?id_regency="+id_regency,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #district-group", "success");

                var item = data.data;

                $("#form-add select[name='district']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='district']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='district']").select2({
                    placeholder: "{{ trans('messages.select_district') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='district']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-add #district-group", "failed");

                $("#form-add #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
            }
        },
        error: function(){
            loading_content("#form-add #district-group", "failed");

            $("#form-add #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
        }
    })
}

function listVillage(val, id_district) {
    loading_content("#form-add #village-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/village?id_district="+id_district,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #village-group", "success");

                var item = data.data;

                $("#form-add select[name='village']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='village']").append("<option value='"+toTitleCase(item[i].name)+"' data-postal_code='"+item[i].postal_code+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='village']").select2({
                    placeholder: "{{ trans('messages.select_village') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='village']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-add #village-group", "failed");

                $("#form-add #village-group #loading-content").click(function(){ listVillage(val, id_district); });
            }
        },
        error: function(){
            loading_content("#form-add #village-group", "failed");

            $("#form-add #village-group #loading-content").click(function(){ listVillage(val, id_district); });
        }
    })
}

function resetProvince() {
    $("#form-add select[name=province]").html("");
    $("#form-add select[name=province]").prop("disabled", true);
    $("#form-add select[name=province]").append("<option value=''>{{ trans('messages.select_province') }}</option>");
    $("#form-add select[name=province]").select2({
        placeholder: "{{ trans('messages.select_province') }}",
        allowClear: false
    });
}

function resetRegency() {
    $("#form-add select[name=regency]").html("");
    $("#form-add select[name=regency]").prop("disabled", true);
    $("#form-add select[name=regency]").append("<option value=''>{{ trans('messages.select_regency') }}</option>");
    $("#form-add select[name=regency]").select2({
        placeholder: "{{ trans('messages.select_regency') }}",
        allowClear: false
    });
}

function resetDistrict() {
    $("#form-add select[name=district]").html("");
    $("#form-add select[name=district]").prop("disabled", true);
    $("#form-add select[name=district]").append("<option value=''>{{ trans('messages.select_district') }}</option>");
    $("#form-add select[name=district]").select2({
        placeholder: "{{ trans('messages.select_district') }}",
        allowClear: false
    });
}

function resetVillage() {
    $("#form-add select[name=village]").html("");
    $("#form-add select[name=village]").prop("disabled", true);
    $("#form-add select[name=village]").append("<option value=''>{{ trans('messages.select_village') }}</option>");
    $("#form-add select[name=village]").select2({
        placeholder: "{{ trans('messages.select_village') }}",
        allowClear: false
    });
}

function resetZipCode() {
    $("#form-add input[name=postal_code]").val("");
}

$('#form-add select[name=country]').on('change', function() {
    var country = $(this).find(':selected').data('id');

    if(this.value=="" || this.value.toLowerCase()!='indonesia') {
        resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(country==get_country) {
            listProvince(get_province, country);
        } else {
            resetProvince()
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listProvince('', country);
        }        
    }
});

$('#form-add select[name=province]').on('change', function() {
    var province = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_province) {
            listRegency(get_regency, province);
        } else {
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listRegency('', province);
        }
    }
    
});

$('#form-add select[name=regency]').on('change', function() {
    var regency = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_regency) {
            listDistrict(get_district, regency);
        } else {
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listDistrict('', regency);
        }
    }
    
});

$('#form-add select[name=district]').on('change', function() {
    var district = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_district) {
            listVillage(get_village, district);
        } else {
            resetVillage()
            resetZipCode()
            resetData();
            listVillage('', district);
        }
    }
    
});

$('#form-add select[name=village]').on('change', function() {
    var postal_code = $(this).find(':selected').data('postal_code');

    if(this.value=="") {
        resetData();
    } else {
        if(this.value==get_village) {
            $("#form-add input[name=postal_code]").val(get_postal_code);
        } else {
            resetData();
            $("#form-add input[name=postal_code]").val(postal_code);
        }
    }
});

listPatientCategory('');
listCountry('');
//listProvince();
//listRegency();
//listDistrict();

$('#pop-up').on('show.bs.modal', function (e) {
    $("#pop-up #form-pop-up input[name=name]").val("");
    resetValidation('pop-up #form-pop-up #name');
});

$("#pop-up #form-pop-up input[name=name]").focus(function() {
    resetValidation('pop-up #form-pop-up #name');
});

$("#pop-up #form-pop-up").submit(function(event) {
    event.preventDefault();
    resetValidation('popu-p #form-pop-up #name');
    $("#pop-up #form-pop-up button").attr("disabled", true);

    formData= new FormData();
    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("name", $("#pop-up #form-pop-up input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#pop-up #form-pop-up .btn-primary").addClass('loading');
    $("#pop-up #form-pop-up .btn-primary span").removeClass('hide');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#pop-up #form-pop-up button").attr("disabled", false);
            $("#pop-up #form-pop-up .btn-primary").removeClass('loading');
            $("#pop-up #form-pop-up .btn-primary span").addClass('hide');

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['pop-up #form-pop-up #name',data.errors.name, true]);
                } else {

                    $("#pop-up #form-pop-up input[name=name]").val("");
                    
                    notif(true,"{{ trans('validation.success_add_patient_category') }}");

                    listPatientCategory($("#form-add select[name='patient_category']").val());

                    $("#pop-up").modal("toggle");
                }

            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#pop-up #form-pop-up button").attr("disabled", false);

            $("#pop-up #form-pop-up .btn-primary").addClass('loading');
            $("#pop-up #form-pop-up .btn-primary span").removeClass('hide');

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

function getPatient(id, email, phone_code, phone, name, birth_date, gender, smoke, blood_type, id_type, id_number, address, country, province, regency, district, village, postal_code, patient_photo) {
    resetValidation('form-add #name', 'form-add #birth_date', 'form-add #gender', 'form-add #email', 'form-add #phone', 'form-add #patient_category', 'form-add #financing');

    $("#form-add input[name='id_patient']").val(id);
    $("#form-add #profile-patient").removeClass("hide");
    $("#form-add #profile-patient #patient-photo").html(thumbnail('md','patient','{{ $storage }}',patient_photo, name));
    $("#form-add #profile-patient #patient-name").html(name);
    $("#form-add #profile-patient #patient-email").html(email);
    $("#form-add #profile-patient #patient-phone").html("+"+phone_code+""+phone);

    $("#form-add input[name='email']").val(email);
    $("#form-add select[name='phone_code']").val(phone_code).change();
    $("#form-add input[name='phone']").val(phone);
    $("#form-add input[name='name']").val(name);
    $("#form-add input[name='birth_date']").val(unconvertDate(birth_date));
    $("#form-add input[name='age']").val(getAge(birth_date.replace(/\-/g,'/')));
    $("#form-add input[name='gender'][value=" + gender + "]").prop('checked', true);
    $("#form-add input[name='smoke'][value=" + smoke + "]").prop('checked', true);
    $("#form-add select[name='blood_type']").val(blood_type).trigger("change");
    $("#form-add select[name='id_type']").val(id_type).trigger("change");
    $("#form-add input[name='id_number']").val(id_number);
    $("#form-add input[name='address']").val(address);
    $("#form-add input[name='postal_code']").val(postal_code);
    /*$("#form-add select[name='regency']").val(regency).trigger("change");
    $("#form-add select[name='district']").val(district).trigger("change");
    $("#form-add select[name='village']").val(village).trigger("change");
    $("#form-add select[name='province']").val(province).trigger("change");
    $("#form-add select[name='country']").val(country).trigger("change");*/

    get_country = country;
    get_province = province;
    get_regency = regency;
    get_district = district;
    get_village = village;
    get_postal_code = postal_code;

    listCountry(country);
}

function resetPatient() {
    resetData()
    resetValidation('form-add #name', 'form-add #birth_date', 'form-add #gender', 'form-add #email', 'form-add #phone', 'form-add #patient_category', 'form-add #financing');

    $("#form-add input[name='id_patient']").val("");
    $("#form-add #profile-patient").addClass("hide");
    $("#form-add #profile-patient #patient-photo").attr("src", "{{ $storage.'/images/patient/photo.png' }}");
    $("#form-add #profile-patient #patient-name").html("");
    $("#form-add #profile-patient #patient-email").html("");
    $("#form-add #profile-patient #patient-phone").html("");

    $("#form-add input[name='email']").val("");
    $("#form-add select[name='phone_code']").val("62").change();
    $("#form-add input[name='phone']").val("");
    $("#form-add input[name='name']").val("");
    $("#form-add input[name='birth_date']").val("");
    $("#form-add input[name='age']").val("");
    $('#form-add input[name="gender"]:checked').each(function(){
        $(this).prop('checked', false); 
    });
    $('#form-add input[name="smoke"]:checked').each(function(){
        $(this).prop('checked', false); 
    });
    $("#form-add select[name='blood_type']").val("").trigger("change");
    $("#form-add select[name='id_type']").val("").trigger("change");
    $("#form-add input[name='id_number']").val("");
    $("#form-add input[name='address']").val("");
    $("#form-add input[name='postal_code']").val("");
    $("#form-add select[name='regency']").val("").trigger("change");
    $("#form-add select[name='district']").val("").trigger("change");
    $("#form-add select[name='village']").val("").trigger("change");
    $("#form-add select[name='province']").val("").trigger("change");
    $("#form-add select[name='country']").val("Indonesia").trigger("change");
}

// $("#form-add input[name='email']").autocomplete({
//     minLength: 5,
//     source: function( request, response ) {
//         var r = [];

//         formData= new FormData();
//         formData.append("email", request.term);

//         $.ajax({
//             url: "{{ $api_url }}/{{ $lang }}/patient/search",
//             type: "POST",
//             data: formData,
//             processData: false,
//             contentType: false,
//             beforeSend: function(){
//                 $("#form-add #loading-email").removeClass("hide");
//             },
//             success: function(data){
//                 $("#form-add #loading-email").addClass("hide");

//                 if(!data.error) {
//                     r.push({value:data.id,
//                         id:data.id,
//                         email:data.email,
//                         phone_code:data.phone_code,
//                         phone:data.phone,
//                         name:data.name,
//                         birth_date:data.birth_date,
//                         gender:data.gender,
//                         smoke:data.smoke,
//                         blood_type:data.blood_type,
//                         id_type:data.id_type,
//                         id_number:data.id_number,
//                         address:data.address,
//                         country:data.country.name,
//                         province:data.province.name,
//                         regency:data.regency.name,
//                         district:data.district.name,
//                         village:data.village.name,
//                         postal_code:data.postal_code,
//                         photo:data.photo
//                     });

//                     response(r);         
//                 } else {
//                     response(r);
//                 }
                
//             },
//             error: function(){
//                 response(r);
//             }
//         });
//     },
//     focus: function() {
//         return false;
//     },
//     select: function( event, ui ) {        
//         getPatient(ui.item.id, ui.item.email, ui.item.phone_code, ui.item.phone, ui.item.name, ui.item.birth_date, ui.item.gender, ui.item.smoke, ui.item.blood_type, ui.item.id_type, ui.item.id_number, ui.item.address, ui.item.country, ui.item.province, ui.item.regency, ui.item.district, ui.item.village, ui.item.postal_code, ui.item.photo);
//         return false;
//     },
// }).data("ui-autocomplete")._renderItem = function (ul, item) {
//     var inner_html = '<div class="autocomplete">'+
//         thumbnail('md','patient','{{ $storage }}',item.photo,item.name)+
//         '<div class="text">'+
//             '<h1>'+item.name+'</h1>'+
//             '<p>'+nullToEmpty(item.email)+'</p>'+
//             '<p>+'+item.phone_code+''+item.phone+'</p>'+
//         '</div>'+
//     '</div>';
//     return $("<li></li>")
//         .data("item.autocomplete", item)
//         .append(inner_html)
//         .appendTo(ul);
// };

// $("#form-add input[name='phone']").autocomplete({
//     minLength: 5,
//     source: function( request, response ) {
//         var r = [];

//         formData= new FormData();
//         formData.append("phone", parseInt(request.term));

//         $.ajax({
//             url: "{{ $api_url }}/{{ $lang }}/patient/search",
//             type: "POST",
//             data: formData,
//             processData: false,
//             contentType: false,
//             beforeSend: function(){
//                 $("#form-add #loading-phone").removeClass("hide");
//             },
//             success: function(data){
//                 $("#form-add #loading-phone").addClass("hide");

//                 if(!data.error) {
//                     r.push({value:data.id,
//                         id:data.id,
//                         email:data.email,
//                         phone_code:data.phone_code,
//                         phone:data.phone,
//                         name:data.name,
//                         birth_date:data.birth_date,
//                         gender:data.gender,
//                         smoke:data.smoke,
//                         blood_type:data.blood_type,
//                         id_type:data.id_type,
//                         id_number:data.id_number,
//                         address:data.address,
//                         country:data.country.name,
//                         province:data.province.name,
//                         regency:data.regency.name,
//                         district:data.district.name,
//                         village:data.village.name,
//                         postal_code:data.postal_code,
//                         photo:data.photo
//                     });

//                     response(r);         
//                 } else {
//                     response(r);
//                 }
                
//             },
//             error: function(){
//                 response(r);
//             }
//         });
//     },
//     focus: function() {
//         return false;
//     },
//     select: function( event, ui ) {        
//         getPatient(ui.item.id, ui.item.email, ui.item.phone_code, ui.item.phone, ui.item.name, ui.item.birth_date, ui.item.gender, ui.item.smoke, ui.item.blood_type, ui.item.id_type, ui.item.id_number, ui.item.address, ui.item.country, ui.item.province, ui.item.regency, ui.item.district, ui.item.village, ui.item.postal_code, ui.item.photo);
//         return false;
//     },
// }).data("ui-autocomplete")._renderItem = function (ul, item) {
//     var inner_html = '<div class="autocomplete">'+
//         thumbnail('md','patient','{{ $storage }}',item.photo,item.name)+
//         '<div class="text">'+
//             '<h1>'+item.name+'</h1>'+
//             '<p>'+nullToEmpty(item.email)+'</p>'+
//             '<p>+'+item.phone_code+''+item.phone+'</p>'+
//         '</div>'+
//     '</div>';
//     return $("<li></li>")
//         .data("item.autocomplete", item)
//         .append(inner_html)
//         .appendTo(ul);
// };

$('#datepicker_date').on('dp.change', function(e) {
    age(e.date, '#form-add input[name="age"]');
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$('#form-add input[name=gender]').click(function() {
    resetValidation('form-add #gender');
});

$('#form-add input[name="birth_date"]').focus(function() {
    resetValidation('form-add #birth_date');
});

$('#form-add input[name=email]').focus(function() {
    resetValidation('form-add #email');
});

$('#form-add input[name=phone]').focus(function() {
    resetValidation('form-add #phone');
});

$("#form-add #patient_category-group .border-group").click(function() {
    resetValidation('form-add #patient_category');
});

$("#form-add #financing-group .border-group").click(function() {
    resetValidation('form-add #financing');
});

$("#form-add").submit(function(event) {
    resetValidation('form-add #name', 'form-add #birth_date', 'form-add #gender', 'form-add #email', 'form-add #phone', 'form-add #patient_category', 'form-add #financing');

    event.preventDefault();
    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    var gender = $("#form-add input[name='gender']:checked").val();
    if(gender==undefined) {
        gender="";
    }

    var smoke = $("#form-add input[name='smoke']:checked").val();
    if(smoke==undefined) {
        smoke="";
    }

    formData.append("validate", false);
    formData.append("id_patient", $("input[name=id_patient]").val());
    formData.append("email", $("#form-add input[name='email']").val());
    formData.append("phone_code", $("#form-add select[name='phone_code'] option:selected").val());
    formData.append("phone", parseInt($("#form-add input[name='phone']").val()));
    formData.append("name", $("#form-add input[name='name']").val());
    formData.append("birth_date", convertDate($("#form-add input[name='birth_date']").val()));
    formData.append("smoke", smoke);
    formData.append("gender", gender);
    formData.append("blood_type", $("#form-add select[name='blood_type'] option:selected").val());
    formData.append("id_type", $("#form-add select[name='id_type'] option:selected").val());
    formData.append("id_number", $("#form-add input[name='id_number']").val());
    formData.append("address", $("#form-add input[name='address']").val());
    formData.append("postal_code", $("#form-add input[name='postal_code']").val());
    formData.append("district", $("#form-add select[name='district'] option:selected").val());
    formData.append("village", $("#form-add select[name='village'] option:selected").val());  
    formData.append("regency", $("#form-add select[name='regency'] option:selected").val());
    formData.append("province", $("#form-add select[name='province'] option:selected").val());
    formData.append("country", $("#form-add select[name='country'] option:selected").val());  
    formData.append("id_patient_category", $("#form-add select[name='patient_category'] option:selected").val());
    var financing = $("#form-add select[name='financing'] option:selected").val();
    formData.append("financing", financing);
    if(financing=="insurance") {
        formData.append("id_insurance", $("#form-add select[name='insurance'] option:selected").val());
        formData.append("card_number", $("#form-add input[name='card_number']").val());
        formData.append("status_insurance", null);
    }else{
        formData.append("id_insurance", "");
        formData.append("card_number", "");
        formData.append("status_insurance", "");
    }
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.name, true], ['form-add #id_number',data.errors.id_number, true], ['form-add #id_type',data.errors.id_number, true], ['form-add #birth_date',data.errors.birth_date, true], ['form-add #gender',data.errors.gender, true], ['form-add #email',data.errors.email, true], ['form-add #phone',data.errors.phone, true], ['form-add #patient_category',data.errors.id_patient_category, true], ['form-add #financing',data.errors.financing, true]);

                    if(data.errors.id_patient) {
                        notif(false,""+data.errors.id_patient+"");
                    } else {
                        if(data.message) {
                            notif(false,""+data.message+"");
                        }
                    }
                } else {
                
                    $("#form-add input[name='id_patient']").val("");
                    $("#form-add input[name='name']").val("");
                    $("#form-add input[name='birth_date']").val("");
                    $("#form-add input[name='age']").val("");
                    $('#form-add input[name="gender"]:checked').each(function(){
                        $(this).prop('checked', false); 
                    });
                    $('#form-add input[name="smoke"]:checked').each(function(){
                        $(this).prop('checked', false); 
                    });
                    $("#form-add select[name='blood_type']").val("").trigger("change");
                    $("#form-add select[name='id_type']").val("").trigger("change");
                    $("#form-add input[name='id_number']").val("");
                    $("#form-add input[name='address']").val("");
                    $("#form-add input[name='postal_code']").val("");
                    $("#form-add select[name='regency']").val("").trigger("change");
                    $("#form-add select[name='district']").val("").trigger("change");
                    $("#form-add select[name='village']").val("").trigger("change");
                    $("#form-add select[name='province']").val("").trigger("change");
                    $("#form-add select[name='country']").val("").trigger("change");
                    $("#form-add input[name='email']").val("");
                    $("#form-add select[name='phone_code']").val('62').change();
                    $("#form-add input[name='phone']").val("");
                    $("#form-add select[name='patient_category']").val("").trigger("change");
                    $("#form-add select[name='financing']").val("").trigger("change");

                    notif(true,"{{ trans('validation.success_add_patient') }}");

                    setTimeout(function(){
                        loading('#/{{ $lang }}/patient/'+data.id+"");
                        redirect('#/{{ $lang }}/patient/'+data.id+"");                    
                    },500);
                }
        
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});


</script>
