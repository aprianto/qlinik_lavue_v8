<script type="text/javascript">

function exportData(key) {
  $("#document-export").html('');
  $("#document-export").addClass("hide");
  $("#loading-document-export").removeClass("hide");
  $("#reload-document-export").addClass("hide");
  $.ajax({        
    url: "{{ $docs_url }}/{{ $lang }}/docs/clinic/{{ $id_clinic }}/export/"+key,
    type: "GET",
    processData: false,
    contentType: false,
    success: function(data){
      $("#loading-document-export").addClass("hide");
      $("#document-export").removeClass("hide");
      $("#document-export").html(data);
    },
    error: function(){
      $("#loading-document-export").addClass("hide");
      $("#reload-document-export").removeClass("hide");

      $("#reload-document-export .reload").click(function(){ exportData(); });
    }
  })
}

function exportPatient(key) {
  $('#report-export-dialog').attr('class','modal-dialog small-report');
  $("input[name='key_report_export']").val(key);
  $("#report-export").modal("show");
  exportData(key);
}

</script>