@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

@if($level=="user" || $level == 'doctor')
<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" />
                    <h3>
                        {{ trans('messages.patient') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/patient"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                @if($level != 'doctor')
                <div class="thumbnail-sec">
                    @if(Session::get('pcare'))
                    <a data-toggle="modal" id="resetFormBpjs" data-target="#checkBPJS">
                    @else
                    <a href="#/{{ $lang }}/patient/create" onclick="loading($(this).attr('href'));">
                    @endif
                    
                    <div class="image">
                        <div class="canvas">
                            <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                        </div>
                    </div>
                    <div class="text">
                        <span>{{ trans('messages.add_patient') }}</span>
                    </div>
                    </a>
                    <a href="#/{{ $lang }}/schedule/sick" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/sidebar/rawat-jalan.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Pendaftaran</span>
                        </div>
                    </a>
                    @if(Session::get('pcare'))
                    <a data-toggle="modal" id="resetFormBpjsOnly" data-target="#checkBPJSOnly">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/sidebar/cek-status.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Cek Status</span>
                        </div>
                    </a>
                    @endif
                    <div class="menu-row-rs">
                        <div class=" pull-right" id="other-dropdown">
                            <a href="" data-toggle="dropdown" id="other-toggle">
                                <div class="image">
                                    <div class="canvas">
                                        <img src="{{ asset('assets/images/icons/header/more-horizontal.png') }}" />
                                    </div>
                                </div>
                                <div class="text">
                                    <span>Lainnya</span>
                                </div>
                            </a>
                            <ul class=" dropdown dropdown-menu dropdown-scroll">
                                <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                                <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <div class="dropdown-rs">
                    <ul class="wrapper-dropdown-rs dropdown-scroll">
                        <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                    </ul>
                </div>           
                @endif
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">  
                            <div class="col-md-4 form-change">
                                <div class="form-group search-group">
                                    <div id="datepicker_birth_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_birth_date') }}" name="birth_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group search-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_name_mrn') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-lg">{{ trans('messages.category') }}</th>
                                <th class="hide-lg">{{ trans('messages.birth_date') }}</th>
                                <th class="hide-md">{{ trans('messages.status') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('assets/js/function.js') }}"></script>
<script type="text/javascript">

if("{{Request::is('*patient*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#patient-bar").addClass('active-bar')
}else {
    $("side-menus ul li a").removeClass('active-bar')
}

$(".pull-right").click(function(){ 
    $(".wrapper-dropdown-rs").toggleClass("open-rs")
});

$("input[name='export_key']").val('patients');
$("input[name='import_key']").val('patients');

$('#datepicker_birth_date').datetimepicker({
    locale: "{{ $lang }}",
    viewMode: 'years',
    format: 'DD/MM/YYYY',
});

$("#datepicker_birth_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        search(1,"true", $("#form-search input[name=q]").val(), convertDate(day));
    });
});

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='birth_date']").val(unconvertDate(birth_dateRepo));

var keySearch = searchRepo;
var keyBirthDate = birth_dateRepo;
var numPage = pageRepo;

function search(page, submit, q, birth_date) {
    formData= new FormData();
    if (q == "" && submit == "false") {
        var q = keySearch;
    }
    if (birth_date == "" && submit == "false") {
        var birth_date = keyBirthDate;
    }
    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();
    
    keySearch = q;
    keyBirthDate = birth_date;
    numPage = page;
    repository(['search',q],['birth_date',birth_date],['page',page]);
    queryParam  = '';
    
    if("{{$level == 'doctor'}}") {
        queryParam = "&id_clinic={{$id_clinic}}&id_polyclinic={{$id_polyclinic}}";
    }
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient",
        type: "GET",
        data: "q="+q+"&birth_date="+birth_date+"&page="+page+"&per_page=10" + queryParam,
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");
            if(!data.error) {
                $(".table").addClass("table-hover");
                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;
                if(total > 0) {
                    $(".pagination-sec").removeClass("hide");
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            /*var patient_photo = "";
                            if(item[i].photo=="" || item[i].photo==null) {
                                patient_photo = "{{ $storage.'/images/patient/photo.png' }}";
                            } else {
                                patient_photo = "<?php echo $storage.'/images/patient/"+item[i].photo+"'; ?>";
                            }*/

                            patient_photo = "<?php echo url('image/patient/30/30/"+item[i].photo+"'); ?>";

                            var patient_category = "";
                            try {
                                patient_category = item[i].patient_category.name;
                            } catch(err) {}
                            tr = $('<tr/>');
                            tr.append("<td>"+
                                "<a href='#/{{ $lang }}/patient/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a><span class='type'>" + item[i].mrn + "</span>"+
                                '<div class="sub show-md">'+
                                    '<span class="general">{{ trans("messages.category") }}: '+patient_category+'</span>'+
                                    '<span class="general">{{ trans("messages.DOB") }}: '+getFormatDate(item[i].birth_date)+'</span>'+
                                    '<span class="general margin show-sm">' + active(item[i].active) + '</span>'+
                                '</div>'+
                                "</td>");
                            
                            try {
                                tr.append("<td class='hide-lg'><span>"+item[i].patient_category.name+"</span></td>");
                            } catch(err) {
                                tr.append("<td class='hide-lg'></td>");
                            }

                            tr.append("<td class='hide-lg'><span class='info'>" + getFormatDate(item[i].birth_date) + "</span></td>");
                            tr.append("<td class='hide-md'>" + active(item[i].active) + "</td>");

                            var action = "";

                            @if(isset($acl->medical_records))
                            @if($acl->medical_records->read==true)
                            action = action+""
                                +"<a href='#/{{ $lang }}/medical/patient/"+item[i].id+"' onclick=\"loading($(this).attr('href'));repository(['polyclinic',''],['from_date',''],['to_date',''],['page','1']);\" class='btn-table btn-blue'><img src=\"{{ asset('assets/images/icons/action/med-rec.png') }}\" /> {{ trans('messages.medical_records') }}</a>"
                            @endif
                            @endif
                            
                            @if($level != 'doctor')
                                action = action+""
                                    +"<a href='#/{{ $lang }}/patient/"+item[i].id+"/edit' onclick=\"loading($(this).attr('href'));\" class='btn-table btn-orange'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /> {{ trans('messages.edit') }}</a>"
                                    +"<a onclick=\"reportPatient('"+item[i].id+"')\" class='btn-table btn-purple'><img src=\"{{ asset('assets/images/icons/action/print.png') }}\" /> {{ trans('messages.print') }}</a>";
                                if(item[i].active==1) {
                                    action = action+"<a data-toggle='modal' data-target='#confirmNonactive' data-message=\"{{ trans('messages.info_non_active') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/deactivate.png') }}\" /> {{ trans('messages.deactivate') }}</a>";
                                } else {
                                    action = action+"<a data-toggle='modal' data-target='#confirmActive' data-message=\"{{ trans('messages.info_active') }}\" data-id='"+item[i].id+"' class='btn-table btn-green'><img src=\"{{ asset('assets/images/icons/action/activate.png') }}\" /> {{ trans('messages.activate') }}</a>";
                                }
                            @endif
                            tr.append("<td>"+
                                action+                                                                                                                                                                                                                         
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");
                    if (page == 1 && q =='' && birth_date == '') {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.empty_patient') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                }
                pages(page, total, per_page, current_page, last_page, from, to, q, birth_date);
            } else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ search(page,"false", q, birth_date); });    
            }
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ search(page,"false", q, birth_date); });
        }
    });
}

$('#confirmActive').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient/"+id+"/status/1",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if (!data.error) {
                if (data.success ==true) {
                    search(numPage,"false", keySearch, keyBirthDate);
                    notif(true,"{{ trans('validation.success_active_patient') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmActive").modal("toggle");
});

$('#confirmNonactive').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient/"+id+"/status/0",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyBirthDate);
                    notif(true,"{{ trans('validation.success_non_active_patient') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmNonactive").modal("toggle");
});

$("#form-search").submit(function(event) {
    search(1,"true", $("#form-search input[name=q]").val(), convertDate($("input[name='birth_date']").val()));
});

$("#form-search input[name='birth_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("#form-search input[name=q]").val(), convertDate($("input[name='birth_date']").val()));
    }
});

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer)
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", $("#form-search input[name=q]").val(), convertDate($("input[name='birth_date']").val()))
        }, 2000);
    }
});

$("#formCheck input[name='type']:radio").change(function(e) {
    if(e.target.value == 0){
        $("#nik-group").show();
        $('#checkBPJS .modal-footer #confirm').show();
        $('#checkBPJS .modal-footer #create').hide();
    }else{
        $("#nik-group").hide();
        $('#checkBPJS .modal-footer #confirm').hide();
        $('#checkBPJS .modal-footer #create').show();
    }
});

search(pageRepo,"false",searchRepo,birth_dateRepo);


</script>

@include('_partial.confirm_active')

@include('_partial.confirm_nonactive')

@include('_partial.check_bpjs')

@include('patient.print')

@include('docs.import')

@include('docs.import_success')

@include('docs.export')

@else
<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-users"></i>
                    <h3>
                        {{ trans('messages.patient') }}
                    </h3>
                </div>         
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search"> 
                            <div class="col-md-11">
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_patient') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="2">{{ trans('messages.name') }}</th>
                                    <th>{{ trans('messages.email') }}</th>
                                    <th>{{ trans('messages.phone') }}</th>
                                    <th>{{ trans('messages.address') }}</th>
                                </tr>
                            </thead>
                            <tbody id="table">
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="5" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="5" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            tr = $('<tr/>');
                            tr.append("<td width='20px' class='photo'>"+thumbnail('sm','patient','{{ $storage }}',item[i].photo,item[i].name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/patient/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a></td>");
                            tr.append("<td><span class='type'>"+nullToEmpty(item[i].email)+"</span></td>");
                            tr.append("<td><span class='text'>+"+item[i].phone_code+""+item[i].phone+"</span></td>");
                            tr.append("<td>"+nullToEmpty(item[i].address)+" "+nullToEmpty(item[i].village.name)+" "+nullToEmpty(item[i].district.name)+" "+nullToEmpty(item[i].regency.name)+" "+nullToEmpty(item[i].province.name)+" "+nullToEmpty(item[i].country.name)+"</td>");
                            
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q); });
        }
    })
}

$("#form-search").submit(function(event) {
    search(1,"true", $("#form-search input[name=q]").val());
});

search(pageRepo,"false",searchRepo);

</script>

@endif

