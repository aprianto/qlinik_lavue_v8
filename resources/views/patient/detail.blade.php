@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content" id="detail">    
    <div class="row" id="section-loading">
        <div class="col-md-9">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-button">
        <div class="col-md-9">
            <div class="widget no-color">
                @if($level=="user" || $level == "doctor")
                <div class="btn-sec">
                    <ul>
                    @if($level=="user")                  
                        <li id="link-schedule">                         
                            <a href="#/{{ $lang }}/schedule/create/patient/{{ $id }}" onclick="loading($(this).attr('href'));">
                                <img src="{{ asset('assets/images/icons/action/create-blue.png') }}" />
                                <span>Daftar Pendaftaran</span>
                            </a>
                        </li>
                        @endif
                        <li>                         
                            <a href="#/{{ $lang }}/medical/patient/{{ $id }}" onclick="loading($(this).attr('href'));">
                                <img src="{{ asset('assets/images/icons/action/med-rec-blue.png') }}" />
                                <span>{{ trans('messages.history_medical_records') }}</span>
                            </a>
                        </li>
                        @if($level=="user")
                        <li>                         
                            <a onclick="reportPatient('{{ $id }}')">
                                <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                                <span>{{ trans('messages.print_patient_card') }}</span>
                            </a>
                        </li>
                        @endif
                        <!--<li id="link-medical_records"> 
                            <a class="hover" href="#/{{ $lang }}/medical/patient/{{ $id }}" onclick="loading($(this).attr('href'));repository(['polyclinic',''],['from_date',''],['to_date',''],['page','1']);"><i class="fa fa-stethoscope"></i><span>{{ trans('messages.medical_records') }}</span></a>
                        </li>-->
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row hide" id="section-profile">
        <div class="col-md-9">            
            <div class="admin-follow no-bg">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.patient_profile') }}
                    </h3>
                </div>
                <img id="photo-patient" src="{{ $storage.'/images/patient/photo.png' }}" />
                <h3 id="name"></h3>
                <span id="mrn"></span>
                @if($level=="user")
                <div class="btn-profile-sec">
                    <a href="#/{{ $lang }}/patient/{{ $id }}/edit" onclick="loading($(this).attr('href'))" id="link-edit">{{ trans("messages.edit_patient_data") }}</a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-9">            
            <div class="widget widget-detail twitter-feed">
                <div class="twitter-account">
                    <h3>{{ trans("messages.info") }}</h3>                    
                </div>
                <ul class="detail-profile">
                    <div id="detail-profile">
                        <li class="profile-features">
                            <h4 class="first-item">{{ trans("messages.personal_data") }}</h4>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.phone_number') }}</span>
                                    <span id="phone"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.email') }}</span>
                                    <span id="email"></span>
                                </div>
                            </div>
                        </li>                        
                        <li class="profile-features">  
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/birth-date.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.birth_date') }}</span>
                                    <span id="birth_date"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">  
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/gender.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.gender') }}</span>
                                    <span id="gender"></span>
                                </div>
                            </div>
                        </li>     
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.no_identity') }}</span>
                                    <span id="no_identity"></span>
                                </div>
                            </div>
                        </li>            
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/blood-type.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.blood_type') }}</span>
                                    <span id="blood_type"></span>
                                </div>
                            </div>
                        </li>                        
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/smoking.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.smoking') }}?</span>
                                    <span id="smoke"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <h4>{{ trans("messages.address") }}</h4>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.address') }}</span>
                                    <span id="address"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <h4>{{ trans("messages.patient_data") }}</h4>
                        </li>                        
                        @if($level=="user" || $level == 'doctor')
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.patient_category') }}</span>
                                    <span id="patient_category"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/payment-plan.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.financing') }}</span>
                                    <span id="financing"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/status.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">{{ trans('messages.patient_status') }}</span>
                                    <span id="active"></span>
                                </div>
                            </div>
                        </li>
                        <!--<li class="profile-features">
                            <div class="col-md-6"><i class="icon ion-arrow-swap"></i> {{ trans("messages.BPJS") }} : </div>
                            <div class="col-md-6"><span id="bpjs"></span></div>
                            <input type="hidden" name="bpjs_number" />
                        </li>-->
                        @endif
                        <!--<li class="profile-button">
                        </li>-->                    
                    </div>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window" id="connect-bpjs" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="ion-arrow-swap"></i> {{ trans("messages.connect_to_bpjs") }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-bpjs" class="">
                <div class="modal-body" id="search-sec">
                    <div class="form-group" id="number-group">
                        <input type="text" class="form-control group" placeholder='No' name="number"  onkeydown="number(event)"  autocomplete="off">
                        <select name="number_type" class="select-group">
                            <option value="bpjs_number">{{ trans("messages.bpjs_number") }}</option>
                            <option value="nik">{{ trans("messages.NIK") }}</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-body hide" id="detail-sec">
                    <input type="hidden" name="bpjs_number" />
                    <table border="0" class="table-info">
                        <tr>
                            <td>{{ trans("messages.bpjs_number") }}</td>
                            <td> : </td>
                            <td id="bpjs_number"></td>
                        </tr>
                        <tr>
                            <td>{{ trans("messages.name") }}</td>
                            <td> : </td>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <td>{{ trans("messages.NIK") }}</td>
                            <td> : </td>
                            <td id="nik"></td>
                        </tr>
                        <tr>
                            <td>{{ trans("messages.status") }}</td>
                            <td> : </td>
                            <td id="status"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" id="btn-search-sec">                    
                    <button class="btn btn-primary" id="btn-search">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.search') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
                <div class="modal-footer hide" id="btn-connect-sec">                    
                    <button class="btn btn-primary" id="btn-connect">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans("messages.connect") }}
                    </button>
                    <button type="button" class="btn btn-secondary" id="btn-back">{{ trans("messages.back") }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade window" id="detail-bpjs" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-info"></i> {{ trans("messages.bpjs_detail") }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body hide" id="loading-bpjs">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-bpjs">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <div class="modal-body" id="bpjs-sec">
                <table border="0" class="table-info">
                    <tr>
                        <td>{{ trans("messages.bpjs_number") }}</td>
                        <td> : </td>
                        <td id="bpjs_number"></td>
                    </tr>
                    <tr>
                        <td>{{ trans("messages.name") }}</td>
                        <td> : </td>
                        <td id="name"></td>
                    </tr>
                    <tr>
                        <td>{{ trans("messages.NIK") }}</td>
                        <td> : </td>
                        <td id="nik"></td>
                    </tr>
                    <tr>
                        <td>{{ trans("messages.status") }}</td>
                        <td> : </td>
                        <td id="status"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">                    
                <button class="btn btn-primary" type="submit" id="btn-disconnect">
                    <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                    {{ trans("messages.disconnect") }}
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-button').addClass('hide');
    $('#section-profile').addClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        @if($level=="user")
        url: "{{ $api_url }}/{{ $lang }}/clinic/patient/"+id+"",
        @else
        url: "{{ $api_url }}/{{ $lang }}/patient/"+id+"",
        @endif
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-button').removeClass('hide');
                $('#section-profile').removeClass('hide');
                $('#section-detail').removeClass('hide');

                var no_identity = "-";
                if(data.id_number!="" && data.id_number!=null) {
                    no_identity = data.id_number+" ("+data.id_type+")";
                }

                var photo_patient = "";
                if(data.photo==null) {
                    photo_patient = "{{ $storage.'/images/patient/photo.png' }}";
                } else {
                    photo_patient = "<?php echo $storage.'/images/patient/"+data.photo+"'; ?>";
                }

                $("#detail #photo-patient").replaceWith(thumbnail('lg','patient','{{ $storage }}',data.photo,data.name));

                $("#path").html(data.name);
                $("#detail #mrn").html(data.mrn);
                $("#detail #name").html(data.name);
                $("#detail #birth_date").html(getFormatDate(data.birth_date)+" ("+getAge(data.birth_date.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }})");
                $("#detail #gender").html(getGender(data.gender));

                var smoke = "";
                if(data.smoke==1) {
                    //smoke = '<i class="fa fa-check-circle success"></i>';
                    smoke = '{{ trans("messages.smoking") }}';
                } else if(data.smoke==null || data.smoke==""){
                    smoke = "-";
                } else {
                    //smoke = '<i class="fa fa-minus-circle failed"></i>';
                    smoke = '{{ trans("messages.no_smoking") }}';
                }

                $("#detail #smoke").html(smoke);
                $("#detail #blood_type").html(checkNull(data.blood_type));
                $("#detail #no_identity").html(no_identity);


                $("#detail #address").html((nullToEmpty(data.address) == "" ? nullToEmpty(data.address) : nullToEmpty(data.address) + ', ')+(nullToEmpty(data.village.name) == "" ? nullToEmpty(data.village.name) : nullToEmpty(data.village.name) + ', ')+(nullToEmpty(data.district.name) == "" ? nullToEmpty(data.district.name) : nullToEmpty(data.district.name) + ', ')+(nullToEmpty(data.regency.name) == "" ? nullToEmpty(data.regency.name) : nullToEmpty(data.regency.name) + ', ')+(nullToEmpty(data.province.name) == "" ? nullToEmpty(data.province.name) : nullToEmpty(data.province.name)+', ')+nullToEmpty(data.country.name));
                $("#detail #email").html(checkNull(data.email));
                $("#detail #phone").html("+"+data.phone_code+""+data.phone);

                //$("#link-medical_records a").attr("href","#/{{ $lang }}/medical/q/name="+data.name+"&birth_date="+data.birth_date+"&from_date=null&to_date=null");
                
                @if($level=="user" || $level == 'doctor')
                try {
                    $("#detail #patient_category").html(data.patient_category.name);
                } catch(err) {}

                var insurance = "";
                if(data.insurance != null){
                    insurance = ' - ' + data.insurance.name;
                }

                var card_number = "";
                if(data.card_number != null){
                    card_number = ' - ' + data.card_number;
                }

                $("#detail #financing").html(getFinancing(data.financing) + insurance + card_number);

                var active = "";
                if(data.active==1) {                    
                    //active = '<i class="fa fa-check-circle success"></i>';
                    active = '{{ trans("messages.active") }}';
                } else {
                    //active = '<i class="fa fa-minus-circle failed"></i>';
                    active = '{{ trans("messages.inactive") }}';

                    $("#link-schedule").addClass("hide");
                }
                $("#detail #active").html(active);
                @endif

                var bpjs = "";
                if(data.bpjs!=null) {                    
                    bpjs = data.bpjs.bpjs_number;
                    $("#link-bpjs-connect").addClass("hide");
                    $("#link-bpjs-connected").removeClass("hide");
                    $("#detail input[name='bpjs_number']").val(data.bpjs.bpjs_number);
                } else {
                    bpjs = '<i class="fa fa-minus-circle failed"></i>';
                    $("#link-bpjs-connect").removeClass("hide");
                    $("#link-bpjs-connected").addClass("hide");
                }

                $("#detail #bpjs").html(bpjs);

            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

detail("{{ $id }}");

$('#connect-bpjs input[name="number"]').focus(function() {
    resetValidation('connect-bpjs #number');
});

$("#connect-bpjs #btn-search").click(function(event){  
    event.preventDefault();
    resetValidation('connect-bpjs #number');

    var type = $("#connect-bpjs select[name='number_type'] option:selected").val();

    var number = $("#connect-bpjs input[name='number']").val();

    if(number=="") {
        if(type=="nik") {
            formValidate(true, ['connect-bpjs #number',"{{ trans('validation.empty_nik') }}", true]);
        } else {
            formValidate(true, ['connect-bpjs #number',"{{ trans('validation.empty_bpjs_number') }}", true]);
        }
    } else {
        $("#connect-bpjs #btn-search").attr("disabled", true);
        $("#connect-bpjs #btn-search").addClass("loading");
        $("#connect-bpjs #btn-search span").removeClass("hide");

        $.ajax({
            url: "{{ url('/') }}/patient/bpjs/"+type+"/"+number,
            type: "GET",
            success: function(data){
                

                $("#connect-bpjs #btn-search").attr("disabled", false);
                $("#connect-bpjs #btn-search").removeClass("loading");
                $("#connect-bpjs #btn-search span").addClass("hide");

                if(data.metadata.message!="OK") {
                    if(type=="nik") {
                        formValidate(true, ['connect-bpjs #number',"{{ trans('validation.invalid_nik') }}", true]);
                    } else {
                        formValidate(true, ['connect-bpjs #number',"{{ trans('validation.invalid_bpjs_number') }}", true]);
                    }
                } else {
                    $("#connect-bpjs #search-sec").addClass("hide");
                    $("#connect-bpjs #detail-sec").removeClass("hide");
                    $("#connect-bpjs #btn-search-sec").addClass("hide");
                    $("#connect-bpjs #btn-connect-sec").removeClass("hide");

                    $("#connect-bpjs #detail-sec input[name='bpjs_number']").val(data.response.peserta.noKartu);
                    $("#connect-bpjs #detail-sec #bpjs_number").html(data.response.peserta.noKartu);
                    $("#connect-bpjs #detail-sec #name").html(data.response.peserta.nama);
                    $("#connect-bpjs #detail-sec #nik").html(data.response.peserta.nik);

                    var status = data.response.peserta.statusPeserta.keterangan;
                    if(status=="AKTIF") {
                        status = 1;
                    } else {
                        status = 0;
                    }

                    $("#connect-bpjs #detail-sec #status").html(active(status));


                }
            },
            error: function(){
                $("#connect-bpjs #btn-search").attr("disabled", false);
                $("#connect-bpjs #btn-search").removeClass("loading");
                $("#connect-bpjs #btn-search span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        })
    }
});

$("#connect-bpjs #btn-connect").click(function(event){  
    event.preventDefault();
    $("#connect-bpjs #btn-connect").attr("disabled", true);
    $("#connect-bpjs #btn-connect").addClass("loading");
    $("#connect-bpjs #btn-connect span").removeClass("hide");

    var bpjs_number = $("#connect-bpjs #detail-sec input[name='bpjs_number']").val();

    formData= new FormData();
    formData.append("validate", false);
    formData.append("bpjs_number", bpjs_number);
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient/bpjs/connect/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#connect-bpjs #btn-connect").attr("disabled", false);
            $("#connect-bpjs #btn-connect").removeClass("loading");
            $("#connect-bpjs #btn-connect span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    notif(false,""+data.errors.bpjs_number+"");
                } else {
                    $("#connect-bpjs #search-sec").removeClass("hide");
                    $("#connect-bpjs #detail-sec").addClass("hide");
                    $("#connect-bpjs #btn-search-sec").removeClass("hide");
                    $("#connect-bpjs #btn-connect-sec").addClass("hide");
                    $("#connect-bpjs input[name='number']").val("");

                    $("#connect-bpjs").modal("toggle");

                    notif(true,"{{ trans('validation.success_connect_bpjs') }}");

                    $("#link-bpjs-connect").addClass("hide");
                    $("#link-bpjs-connected").removeClass("hide");

                    $("#detail #bpjs").html(bpjs_number);
                    $("#detail input[name='bpjs_number']").val(bpjs_number);
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#connect-bpjs #btn-connect").attr("disabled", false);
            $("#connect-bpjs #btn-connect").removeClass("loading");
            $("#connect-bpjs #btn-connect span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});


$("#connect-bpjs #btn-back").click(function(){  
    $("#connect-bpjs #search-sec").removeClass("hide");
    $("#connect-bpjs #detail-sec").addClass("hide");
    $("#connect-bpjs #btn-search-sec").removeClass("hide");
    $("#connect-bpjs #btn-connect-sec").addClass("hide");
});

function getBPJS(number) {
    $('#detail-bpjs #loading-bpjs').removeClass('hide');
    $("#detail-bpjs #reload-bpjs").addClass("hide");
    $('#detail-bpjs #bpjs-sec').addClass('hide');

    $.ajax({
        url: "{{ url('/') }}/patient/bpjs/bpjs_number/"+number,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#detail-bpjs #loading-bpjs').addClass('hide');

            if(data.metadata.message=="OK") {
                $('#detail-bpjs #bpjs-sec').removeClass('hide');

                $("#detail-bpjs #bpjs-sec #bpjs_number").html(data.response.peserta.noKartu);
                $("#detail-bpjs #bpjs-sec #name").html(data.response.peserta.nama);
                $("#detail-bpjs #bpjs-sec #nik").html(data.response.peserta.nik);

                var status = data.response.peserta.statusPeserta.keterangan;
                if(status=="AKTIF") {
                    status = 1;
                } else {
                    status = 0;
                }

                $("#detail-bpjs #bpjs-sec #status").html(active(status));
            } else {
                $("#detail-bpjs #reload-bpjs").removeClass("hide");
                $("#detail-bpjs #reload-bpjs .reload").click(function(){ getBPJS(number); });
            }

        },
        error: function(){
            $('#detail-bpjs #loading-bpjs').addClass('hide');
            $("#detail-bpjs #reload-bpjs").removeClass("hide");

            $("#detail-bpjs #reload-bpjs .reload").click(function(){ getBPJS(number); });
        }
    })
}

$('#detail-bpjs').on('show.bs.modal', function (e) {
    var number = $("#detail input[name='bpjs_number']").val();

    getBPJS(number);
});

$("#detail-bpjs #btn-disconnect").click(function(event){  
    event.preventDefault();
    $("#detail-bpjs #btn-disconnect").attr("disabled", true);
    $("#detail-bpjs #btn-disconnect").addClass("loading");
    $("#detail-bpjs #btn-disconnect span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient/bpjs/disconnect/{{ $id }}",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#detail-bpjs #btn-disconnect").attr("disabled", false);
            $("#detail-bpjs #btn-disconnect").removeClass("loading");
            $("#detail-bpjs #btn-disconnect span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    notif(false,"{{ trans('validation.failed') }}");
                } else {
                    $("#detail-bpjs").modal("toggle");

                    notif(true,"{{ trans('validation.success_disconnect_bpjs') }}");

                    $("#link-bpjs-connect").removeClass("hide");
                    $("#link-bpjs-connected").addClass("hide");
                    $("#detail #bpjs").html('<i class="fa fa-minus-circle failed"></i>');
                    $("#detail input[name='bpjs_number']").val("");

                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#detail-bpjs #btn-disconnect").attr("disabled", false);
            $("#detail-bpjs #btn-disconnect").removeClass("loading");
            $("#detail-bpjs #btn-disconnect span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>

@include('patient.print')