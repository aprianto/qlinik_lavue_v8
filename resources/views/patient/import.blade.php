<script type="text/javascript">

function importData() {
  $("#document-import").html('');
  $("#document-import").addClass("hide");
  $("#loading-document-import").removeClass("hide");
  $("#reload-document-import").addClass("hide");
  $.ajax({        
    url: "{{ $docs_url }}/{{ $lang }}/docs/clinic/{{ $id_clinic }}/patient-import",
    type: "GET",
    processData: false,
    contentType: false,
    success: function(data){
      $("#loading-document-import").addClass("hide");
      $("#document-import").removeClass("hide");
      $("#document-import").html(data);
    },
    error: function(){
      $("#loading-document-import").addClass("hide");
      $("#reload-document-import").removeClass("hide");

      $("#reload-document-import .reload").click(function(){ importData(); });
    }
  })
}

function importPatient() {
  $('#report-import-dialog').attr('class','modal-dialog small-report');
  $("#report-import").modal("show");
  importData();
}

</script>