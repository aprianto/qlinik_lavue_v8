@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 


<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/pendapatan.png') }}" />
                    <h3>
                        {{ trans('messages.income_report') }}
                    </h3>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="total-sec">
                    <div class="row">
                        <div class="col-md-4">
                            <h3>{{ trans('messages.total_income') }}</h3>
                            <h1 id="total_income">{{ formatCurrency($lang,'Rp', 0) }}</h1>
                        </div>
                        <div class="col-md-2">
                            <h3>{{ trans('messages.total_patient') }}</h3>
                            <h1 id="total_patient">0</h1>
                        </div>
                        <div class="col-md-2">
                            <h3>{{ trans('messages.total_registration') }}</h3>
                            <h1 id="total_registration">0</h1>
                        </div>
                        <div class="col-md-4">
                            <div class="thumb">
                                <a id="btn-print" onclick="reportIncome()">
                                    <div class="image">
                                        <div class="canvas">
                                            <img src="{{ asset('assets/images/icons/link/print.png') }}" />
                                        </div>
                                    </div>
                                    <div class="text">
                                        <span>{{ trans('messages.print') }}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-area">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.date') }}</th>
                                <th>{{ trans('messages.amount') }}</th>
                                <th>{{ trans('messages.registration') }}</th>
                                <th class="hide-md">{{ trans('messages.income') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if("{{Request::is('*report/income*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#report-income-bar").addClass('active-bar')
}else {
    $('.side-menus ul li a.active-bar').removeClass('active-bar')
}

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});
$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day});
    });
});
$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day});
    });
});

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val()
    var to_date = $("#form-search input[name=to_date]").val()
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", convertDate(from_date), convertDate(to_date));
}

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

dateMonth = new Date();
dateMonth.setMonth(dateMonth.getMonth() - 1);
monthFrom = ((dateMonth.getMonth()+1)+100).toString().slice(-2);
dayFrom = (dateMonth.getDate()+100).toString().slice(-2);
yearFrom = dateMonth.getFullYear();
dateFrom = dayFrom+'/'+monthFrom+'/'+yearFrom;

if(from_dateRepo=="now") {
    from_dateRepo = convertDate(dateFrom);
}

if(to_dateRepo=="now") {
    to_dateRepo = convertDate(dateNow);
}

$("#form-search input[name='from_date']").val(unconvertDate(from_date_monthRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_date_monthRepo));

var numPage = pageRepo;
var keyFromDate = from_date_monthRepo;
var keyToDate = to_date_monthRepo;

var id_clinic = '{{ $id_clinic }}'

function search(page, submit, from_date, to_date) {
    formData= new FormData();

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }


    $("#loading-table").removeClass("hide");
    $("#reload").addClass("hide");
    $("#table").empty();


    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;

    repository(['from_date_month',from_date],['to_date_month',to_date],['page',page]);

    $("#total_income").html(formatCurrency('{{ $lang }}','Rp', 0))
    $("#total_patient").html(0)
    $("#total_registration").html(0)
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/report/clinic/income",
        type: "GET",
        data: "from_date="+from_date+"&to_date="+to_date+"",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $("#total_income").html(formatCurrency('{{ $lang }}','Rp',data.total_income))
                $("#total_patient").html(data.total_patient)
                $("#total_registration").html(data.total_invoice)

                var item = data.data;

                var total = item.length;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var tr;
                    var no = 1;
                    var total = 0;
                    for (var i = 0; i < item.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td>" + getFormatDate(item[i].date) +
                                '<div class="sub show-sm">'+
                                    "<span class='general'>{{ trans('messages.patient') }}: " + item[i].total_patient + "</span>"+
                                    "<span class='general'>{{ trans('messages.registration') }}: " + item[i].total_invoice + "</span>"+
                                    "<span class='general'>{{ trans('messages.total') }}: <b>" + formatCurrency('{{ $lang }}','Rp',item[i].income) + "</b></span>"+
                                '</div>'+
                                "</td>");
                        tr.append("<td class='hide-md'>" + item[i].total_patient + "</td>");
                        tr.append("<td class='hide-md'>" + item[i].total_invoice + "</td>");
                        tr.append("<td class='hide-md'><b>" + formatCurrency('{{ $lang }}','Rp',item[i].income) + "</b></td>");
                        

                        var action = "<a class='btn-table btn-blue drill_down-"+no+"' onclick=\"showDetail('"+no+"')\" data-toggle='collapse' data-target='#collapseExample' aria-expanded='false' aria-controls='collapseExample'><span class='fa fa-chevron-down'></span> {{ trans('messages.show_detail') }}</a>"
                            +"<a onclick=\"reportDetailIncome('"+item[i].id+"')\" class='btn-table btn-purple'><img src=\"{{ asset('assets/images/icons/action/print.png') }}\" /> {{ trans('messages.print') }}</a>";

                        tr.append("<td>"+
                            action+
                            '<div class="dropdown">'+
                                '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                '<ul class="dropdown-menu dropdown-menu-right">'+
                                    action+
                                '</ul>'+
                            '</div>'+
                            "</td>");
                        $("#table").append(tr);

                        var detail = "";

                        detail += "<div class='row sub'><div class='col-md-6'>";

                        detail += "<h3 class='table-detail-title'>{{ trans('messages.doctor_income') }}</h3>";

                        detail += "<table class='table-detail'><thead><tr>"+
                            "<th>{{ trans('messages.doctor') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.polyclinic') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.nominal') }}</th>"+
                            "</thead></tr><tbody>";
                            for(var j=0; j< item[i].commission.length && item[i].commission[j].commission; j++) {
                                const commission_data = item[i].commission[j];
                                var commission = commission_data.commission || '' ;
                                var polyclinic = commission_data.polyclinic_name || '';
                                var doctor = commission_data.doctor_name || '';
                                detail=detail+"<tr>"+
                                "<td>"+doctor+""+
                                    '<div class="sub show-sm">'+
                                        "<span class='general sub-general'>"+polyclinic+"</span>"+
                                        "<span class='general sub-general'><b>"+formatCurrency('{{ $lang }}','Rp',commission)+"</b></span>"+
                                    '</div>'+
                                "</td>"+
                                "<td class='hide-md'>"+polyclinic+"</td>"+
                                "<td class='hide-md'><b>"+formatCurrency('{{ $lang }}','Rp',commission)+"</b></td>"+
                                "</tr>";
                            }
                        detail=detail+"</tbody></table>";
                        
                        detail += "</div><div class='col-md-6'>";

                        detail += "<h3 class='table-detail-title'>{{ trans('messages.polyclinic_income') }}</h3>";

                        detail += "<table class='table-detail'><thead><tr>"+
                            "<th>{{ trans('messages.polyclinic') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.nominal') }}</th>"+
                            "</thead></tr><tbody>";

                        for(var j=0; j< item[i].polyclinic.length; j++) {
                            detail=detail+"<tr>"+
                            "<td>"+item[i].polyclinic[j].polyclinic_name+""+
                                '<div class="sub show-sm">'+
                                    "<span class='general sub-general'><b>"+formatCurrency('{{ $lang }}','Rp',item[i].polyclinic[j].income)+"</b></span>"+
                                '</div>'+
                            "</td>"+
                            "<td class='hide-md'><b>"+formatCurrency('{{ $lang }}','Rp',item[i].polyclinic[j].income)+"</b></td>"+
                            "</tr>";
                        }
                        detail=detail+"</tbody></table>";

                        detail += "</div></div'>";


                        detail += "<div class='row sub'><div class='col-md-6'>";

                        detail += "<h3 class='table-detail-title'>{{ trans('messages.service_income') }}</h3>";

                        detail += "<table class='table-detail'><thead><tr>"+
                            "<th>{{ trans('messages.service') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.quantity') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.nominal') }}</th>"+
                            "</thead></tr><tbody>";

                        for(var j=0; j< item[i].service.length; j++) {
                            detail=detail+"<tr>"+
                            "<td>"+item[i].service[j].service_name+""+
                                '<div class="sub show-sm">'+
                                    "<span class='general sub-general'>{{ trans('messages.quantity') }}: <b>"+item[i].service[j].quantity+"</b></span>"+
                                    "<span class='general sub-general'>{{ trans('messages.income') }}: <b>"+formatCurrency('{{ $lang }}','Rp',item[i].service[j].total)+"</b></span>"+
                                '</div>'+
                            "</td>"+
                            "<td class='hide-md'><b>"+item[i].service[j].quantity+"</b></td>"+
                            "<td class='hide-md'><b>"+formatCurrency('{{ $lang }}','Rp',item[i].service[j].total)+"</b></td>"+
                            "</tr>";
                        }
                        detail=detail+"</tbody></table>";

                        detail += "</div><div class='col-md-6'>";

                        detail += "<h3 class='table-detail-title'>{{ trans('messages.medicine_income') }}</h3>";

                        detail += "<table class='table-detail'><thead><tr>"+
                            "<th>{{ trans('messages.medicine') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.amount') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.total') }}</th>"+
                            "</thead></tr><tbody>";

                        if(item[i].medicine) {
                            item[i].medicine.forEach(function (medicine) {
                                detail=detail+"<tr>"+
                                "<td>"+medicine.medicine_name+""+
                                    '<div class="sub show-sm">'+
                                        "<span class='general sub-general'>{{ trans('messages.quantity') }}: <b>"+medicine.quantity+"</b></span>"+
                                        "<span class='general sub-general'>{{ trans('messages.income') }}: <b>"+formatCurrency('{{ $lang }}','Rp',medicine.total)+"</b></span>"+
                                    '</div>'+
                                "</td>"+
                                "<td class='hide-md'><b>"+medicine.quantity+"</b></td>"+
                                "<td class='hide-md'><b>"+formatCurrency('{{ $lang }}','Rp',medicine.total)+"</b></td>"+
                                "</tr>";
                            });
                        }

                        detail=detail+"</tbody></table>";

                        detail += "</div></div'>";



                        detail += "<div class='row sub'><div class='col-md-12'>";

                        detail += "<h3 class='table-detail-title'>{{ trans('messages.transaction_income') }}</h3>";

                        detail += "<table class='table-detail'><thead><tr>"+
                            "<th>{{ trans('messages.invoice_no') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.patient') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.nominal') }}</th>"+
                            "<th class='hide-md'>{{ trans('messages.payment_type') }}</th>"+
                            "</thead></tr><tbody>";

                        // Append jenis pembayaran
                        const translate_invoice = {
                            insurance: "{{ trans('messages.insurance') }}",
                            cash: "{{ trans('messages.cash') }}",
                            credit_card: "{{ trans('messages.credit_card') }}",
                        }
                        const key_payment_types = Object.keys(translate_invoice);

                        for(var j=0; j< item[i].invoice.length; j++) {

                            const invoice = item[i].invoice[j];
                            let payment_type = '-';
                            const total = invoice.total + (invoice.insurance || 0);
                            
                            key_payment_types.forEach(key => {
                                if (invoice[key]) {
                                    if(payment_type === '-') {
                                        payment_type = `${translate_invoice[key]} ${formatCurrency('{{ $lang }}','Rp',invoice[key])}`;
                                    } else  {
                                        payment_type = `${payment_type}<br />${translate_invoice[key]} ${formatCurrency('{{ $lang }}','Rp',invoice[key])}`;
                                    }
                                }
                            });

                            payment_type = `<b>${payment_type}</b>`;

                            detail=detail+"<tr>"+
                            "<td><a href='#/{{ $lang }}/invoice/"+invoice.id+"' onclick=\"loading($(this).attr('href'));\">"+invoice.number+"</a>"+
                                '<div class="sub show-sm">'+
                                    "<span class='general sub-general'>"+invoice.patient_name+"</span>"+
                                    "<span class='general sub-general'>"+formatCurrency('{{ $lang }}','Rp',total)+"</span>"+
                                    "<span class='general sub-general'>"+payment_type+"</span>"+
                                '</div>'+
                            "</td>"+
                            "<td class='hide-md'>"+invoice.patient_name+"</td>"+
                            "<td class='hide-md'>"+formatCurrency('{{ $lang }}','Rp',total)+"</td>"+
                            "<td class='hide-md'>"+payment_type+"</td>"+
                            "</tr>";
                        }
                        detail=detail+"</tbody></table>";

                        detail += "</div></div'>";



                        tr = $('<tr id="income-'+no+'" class="row-drill-down hide" />');
                        tr.append("<td colspan='5'>"+detail+"</td>");

                        $("#table").append(tr);

                        total = parseInt(total)+parseInt(item[i].income);
                        no++;
                    }

                    $("#total").html(formatCurrency('{{ $lang }}','Rp',total));
                } else {
                    $(".pagination-sec").addClass("hide");

                    $("#total").html(formatCurrency('{{ $lang }}','Rp',0));
                    $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                }
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(numPage,"false", keyFromDate, keyToDate); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(numPage,"false", keyFromDate, keyToDate); });


        }
    })
}

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});


search(pageRepo,"false", from_date_monthRepo, to_date_monthRepo);

// FUNCTION PRINT ALL INCOME
function report(id_clinic, from_date, to_date) {
    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/income/clinic/"+id_clinic+"?from_date="+from_date+"&to_date="+to_date,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);
            
        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");

            $("#reload-document .reload").click(function(){ report("{{ $id_clinic }}", keyFromDate, keyToDate); });
        }
    })
}

function showDetail(no) {
    if($("#income-"+no+"").hasClass('row-drill-down hide')) {
        $("#income-"+no+"").removeClass("hide");
    }else {
        $("#income-"+no+"").addClass("row-drill-down hide");
    }
}

// FUNCTION PRINT INCOME DETAIL
function reportDetail(id_clinic, id, from_date, to_date) {

    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/income/clinic/"+id_clinic+"/detail/"+id+"?from_date="+from_date+"&to_date="+to_date,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);
            
        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");

            $("#reload-document .reload").click(function(){ reportDetail("{{ $id_clinic }}", id, keyFromDate, keyToDate); });
        }
    });
}

function reportIncome() {
    $("#desktop-report").addClass("hide");
    $("#mobile-report").addClass("hide");
    
    $('#report-dialog').attr('class','modal-dialog modal-lg');
    $("#report").modal("show");
    report("{{ $id_clinic }}", keyFromDate, keyToDate);
}

function reportDetailIncome(id) {
    $("#desktop-report").addClass("hide");
    $("#mobile-report").addClass("hide");
    
    $('#report-dialog').attr('class','modal-dialog modal-lg');
    $("#report").modal("show");
    reportDetail("{{ $id_clinic }}", id, keyFromDate, keyToDate);
}


</script>
