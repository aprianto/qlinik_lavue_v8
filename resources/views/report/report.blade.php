


<div class="modal fade report modal-scroll" id="report" role="dialog" aria-labelledby="reportTitle" aria-hidden="true">
    <div id="report-dialog" class="modal-dialog modal-lg" role="document">        
        <div class="modal-content">
            <div class="modal-header">                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="id_report" />
            <div class="modal-body" id="loading-document">
                <div class="cell">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="reload-document">
                <div class="cell">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="document">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-secondary" id="cancel-print" data-dismiss="modal" aria-label="Close">
                    {{ trans("messages.cancel") }}
                </button>
                <button type="button" class="btn-primary" id="print">
                    {{ trans("messages.print") }}
                </button>
                <button type="button" class="btn-view hide" id="mobile-report">
                    <img src="{{ asset('assets/images/icons/view/view-mobile.png') }}" />
                </button>
                <button type="button" class="btn-view hide" id="desktop-report">
                    <img src="{{ asset('assets/images/icons/view/view-desktop.png') }}" />
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#print").click(function() {
    var print = $("#document").html();
    var divToPrint=$('#document').html();
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write(divToPrint+'<script>window.print()<\/script>');
    setTimeout(function(){newWin.close();}, 10);
});

</script>
