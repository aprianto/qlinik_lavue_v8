@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 


<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                    <h3>
                        {{ trans('messages.medicine_report') }}
                    </h3>
                </div>
                <div class="thumbnail-sec hide">
                    <a id="btn-print" onclick="reportMedicine()">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/action/print-blue.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.print') }}</span>
                        </div>
                    </a>
                    <div class="info-sec">
                        <h1 id="total">{{ formatCurrency($lang,'Rp',0) }}</h1>
                        <h3>{{ trans("messages.total") }}</h3>
                    </div>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="hide-md">{{ trans('messages.number') }}</th>
                                <th>{{ trans('messages.inventory_name') }}</th>
                                <th class="hide-md">{{ trans('messages.quantity') }}</th>
                                <th class="hide-md">{{ trans('messages.total') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="6  " align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if("{{Request::is('*report/medicine*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#report-medicine-bar").addClass('active-bar')
}else {
    $('.side-menus ul li a.active-bar').removeClass('active-bar')
}

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});
$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day});
    });
});
$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day});
    });
});

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val()
    var to_date = $("#form-search input[name=to_date]").val()
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", convertDate(from_date), convertDate(to_date));
}

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

dateMonth = new Date();
dateMonth.setMonth(dateMonth.getMonth() - 1);
monthFrom = ((dateMonth.getMonth()+1)+100).toString().slice(-2);
dayFrom = (dateMonth.getDate()+100).toString().slice(-2);
yearFrom = dateMonth.getFullYear();
dateFrom = dayFrom+'/'+monthFrom+'/'+yearFrom;

$("#form-search input[name='from_date']").val(unconvertDate(from_date_monthRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_date_monthRepo));

var numPage = pageRepo;
var keyFromDate = from_date_monthRepo;
var keyToDate = to_date_monthRepo;
var data_medicine;
function search(page, submit, from_date, to_date) {
    formData= new FormData();

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $("#reload").addClass("hide");
    $("#table").empty();


    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;

    repository(['from_date_month',from_date],['to_date_month',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/report/clinic/medicine",
        type: "GET",
        data: "from_date="+from_date+"&to_date="+to_date+"",
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");

            if ( !data.error ) {
                let total = data.length;
                if ( total > 0 ) {
                    $(".pagination-sec").removeClass("hide");
                    let tr;
                    let no = 1;
                    let total = 0;
                    data_medicine = data;
                    for (let i = 0; i < data.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td class='hide-md'>" + no + "</td>");
                        tr.append("<td>" + data[i].medicine_name + 
                                '<div class="sub show-sm">'+
                                    "<span class='general'>{{ trans('messages.quantity') }}: " + data[i].quantity + "</span>"+
                                    "<span class='general'>{{ trans('messages.total') }}: " + formatCurrency('{{ $lang }}','Rp',data[i].total) + "</span>"+
                                '</div>'+
                                "</td>");
                        tr.append("<td class='hide-md'>" + data[i].quantity + "</td>");
                        tr.append("<td class='hide-md'>" + formatCurrency('{{ $lang }}','Rp',data[i].total) + "</td>");

                        let action = "<a class='btn-table btn-blue drill_down-"+no+"' onclick=\"showDetail('"+no+"')\" data-toggle='collapse' data-target='#collapseExample' aria-expanded='false' aria-controls='collapseExample'><img src=\"{{ asset('assets/images/icons/nav/chevron-dm.png') }}\" id='img-detail' /> {{ trans('messages.show_detail') }}</a>"
                            +"<a onclick=\"reportDetailInventory('"+data[i].id_medicine+"')\" class='btn-table btn-purple'><img src=\"{{ asset('assets/images/icons/action/print.png') }}\" /> {{ trans('messages.print') }}</a>";

                        tr.append("<td>"+
                            action+
                            '<div class="dropdown">'+
                                '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                '<ul class="dropdown-menu dropdown-menu-right">'+
                                    action+
                                '</ul>'+
                            '</div>'+
                            "</td>");

                        $("#table").append(tr);

                        let detail = "";
                        detail += "<div></div>";
                        detail += "<table class='table-detail'><thead><tr>"
                            + "<th>No</th>"
                            + "<th>{{ trans('messages.doctor_title') }}</th>"
                            + "<th>{{ trans('messages.qty') }}</th>"
                            + "<th>{{ trans('messages.nominal') }}</th>"
                            + "</thead></tr><tbody>";
                        let number = 1;
                        for (j = 0; j < data[i].detail.length; j++) {
                            detail = detail + "<tr>" + "<td>"+number+"</td>" + "<td>"+(data[i].detail[j].name != null ? data[i].detail[j].name : '-')+"</td>" + "<td>"+data[i].detail[j].quantity+"</td>" + "<td>"+formatCurrency('{{ $lang }}','Rp',data[i].detail[j].total)+"</td>" + "</tr>";
                            number++;
                        } 
                        
                        detail = detail + "</tbody></table>";
                        tr = $('<tr id="report-'+no+'" class="row-drill-down hide" />');
                        tr.append("<td colspan='5'>"+detail+"</td>");
                        $("#table").append(tr);

                        total = parseInt(total)+parseInt(data[i].total);
                        no++;
                    }

                    $("#total").html(formatCurrency('{{ $lang }}','Rp',total));
                } else {
                    $(".pagination-sec").addClass("hide");
                    $("#total").html(formatCurrency('{{ $lang }}','Rp',0));
                    $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                }
            } else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ search(page,"false", from_date, to_date); });
            }
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ search(page,"false", from_date, to_date); });
        }
    });
}

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false", from_date_monthRepo, to_date_monthRepo);

function report(id_clinic, from_date, to_date) {
    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medicine/clinic/"+id_clinic+"?from_date="+from_date+"&to_date="+to_date,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);
        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");
            $("#reload-document .reload").click(function(){ report("{{ $id_clinic }}", keyFromDate, keyToDate); });
        }
    })
}

function reportDetail(id, from_date, to_date) {
    $("#document").html('');
    $("#document").addClass("hide");
    $("#loading-document").removeClass("hide");
    $("#reload-document").addClass("hide");
    $.ajax({        
        url: "{{ $docs_url }}/{{ $lang }}/docs/medicine/clinic/detail/"+id+"?from_date="+from_date+"&to_date="+to_date,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            $("#loading-document").addClass("hide");
            $("#document").removeClass("hide");
            $("#document").html(data);
        },
        error: function(){
            $("#loading-document").addClass("hide");
            $("#reload-document").removeClass("hide");
            $("#reload-document .reload").click(function(){ reportDetail("{{ $id_clinic }}",id_clinic, id, keyFromDate, keyToDate); });
        }
    })
}

function reportMedicine() {
    $("#desktop-report").addClass("hide");
    $("#mobile-report").addClass("hide");
    $('#report-dialog').attr('class','modal-dialog modal-lg');
    $("#report").modal("show");
    report("{{ $id_clinic }}", keyFromDate, keyToDate);
}

function showDetail(no) {
    if($("#report-"+no+"").hasClass('row-drill-down hide')) {
        $("#report-"+no+"").removeClass("hide");
        $("tr td a.drill_down-"+no+"").html("<img src=\"{{ asset('assets/images/icons/nav/chevron-um.png') }}\" id='img-detail' />{{ trans('messages.close_detail') }}");
    }else {
        $("#report-"+no+"").addClass("row-drill-down hide");
        $("tr td a.drill_down-"+no+"").html("<img src=\"{{ asset('assets/images/icons/nav/chevron-dm.png') }}\" id='img-detail' />{{ trans('messages.show_detail') }}")
    }
}

function reportDetailInventory(id) {
    $("#desktop-report").addClass("hide");
    $("#mobile-report").addClass("hide");
    $('#report-dialog').attr('class','modal-dialog modal-lg');
    $("#report").modal("show");
    reportDetail(id, keyFromDate, keyToDate);
}

</script>