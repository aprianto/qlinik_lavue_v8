@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 


<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/penyakit.png') }}" />
                    <h3>
                        {{ trans('messages.illness_report') }}
                    </h3>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="file-sec">
                                    <a id="btn-print" class="pull-right" onclick="exportData(from_date_monthRepo, to_date_monthRepo)">
                                        <div class="image">
                                            <div class="canvas">
                                                <img src="{{ asset('assets/images/icons/action/export.png') }}" />
                                            </div>
                                        </div>
                                        <div class="text">
                                            <span>{{ trans('messages.export_data') }}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table" style="display: block; overflow-x: auto; white-space: nowrap;">
                        <thead>
                            <tr>
                                <th class="hide-md">{{ trans('messages.number') }}</th>
                                <th>{{ trans('messages.icd_10_code') }}</th>
                                <th class="hide-md">{{ trans('messages.icd_name') }}</th>
                                <th class="hide-md">{{ trans('messages.0_7_d') }}</th>
                                <th class="hide-md">{{ trans('messages.8_28_d') }}</th>
                                <th class="hide-md">{{ trans('messages.29_d_1_y') }}</th>
                                <th class="hide-md">{{ trans('messages.1_4_y') }}</th>
                                <th class="hide-md">{{ trans('messages.5_9_y') }}</th>
                                <th class="hide-md">{{ trans('messages.10_14_y') }}</th>
                                <th class="hide-md">{{ trans('messages.15_19_y') }}</th>
                                <th class="hide-md">{{ trans('messages.20_44_y') }}</th>
                                <th class="hide-md">{{ trans('messages.45_54_y') }}</th>
                                <th class="hide-md">{{ trans('messages.55_59_y') }}</th>
                                <th class="hide-md">{{ trans('messages.60_69_y') }}</th>
                                <th class="hide-md">{{ trans('messages.70_y') }}</th>
                                <th class="hide-md">{{ trans('messages.man') }}</th>
                                <th class="hide-md">{{ trans('messages.woman') }}</th>
                                <th class="hide-md">{{ trans('messages.new_case') }}</th>
                                <th class="hide-md">{{ trans('messages.old_case') }}</th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="6" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if("{{Request::is('*report/illness*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#report-illness-bar").addClass('active-bar')
}else {
    $('.side-menus ul li a.active-bar').removeClass('active-bar')
}

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});
$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day});
    });
});
$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day});
    });
});

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val()
    var to_date = $("#form-search input[name=to_date]").val()
    if (arg) {
        if(arg.from_day) {
            from_date = arg.from_day
        }else if(arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", convertDate(from_date), convertDate(to_date));
}

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

dateMonth = new Date();
dateMonth.setMonth(dateMonth.getMonth() - 1);
monthFrom = ((dateMonth.getMonth()+1)+100).toString().slice(-2);
dayFrom = (dateMonth.getDate()+100).toString().slice(-2);
yearFrom = dateMonth.getFullYear();
dateFrom = dayFrom+'/'+monthFrom+'/'+yearFrom;

$("#form-search input[name='from_date']").val(unconvertDate(from_date_monthRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_date_monthRepo));

var numPage = pageRepo;
var keyFromDate = from_date_monthRepo;
var keyToDate = to_date_monthRepo;

function search(page, submit, from_date, to_date) {
    formData= new FormData();

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $("#reload").addClass("hide");
    $("#table").empty();

    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;

    repository(['from_date_month',from_date],['to_date_month',to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medical-records/icd-census",
        type: "GET",
        data: "startDate="+from_date+"&endDate="+to_date+"",
        processData: false,
        contentType: false,

        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");

            if(!data.error) {
                let dataTable = data.data
                var total = dataTable.length;

                if(total>0) {
                    $(".pagination-sec").removeClass("hide");
                    
                    var tr;
                    var no = 1;
                    var total = 0;
                    for (var i = 0; i < dataTable.length; i++) {
                        tr = $('<tr />');
                        tr.append("<td class='hide-md'>" + no + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i].code + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i].name + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['0_7_d'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['8_28_d'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['29_d_1_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['1_4_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['5_9_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['10_14_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['15_19_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['20_44_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['45_54_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['55_59_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['60_69_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['70_y'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['L'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i]['P'] + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i].new_case + "</td>");
                        tr.append("<td class='hide-md'>" + dataTable[i].old_case + "</td>");

                        $("#table").append(tr);
                        no++;
                    }

                } else {
                    $(".pagination-sec").addClass("hide");
                    $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");
                }
            } else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ search(page,"false", from_date, to_date); });
            }
            
        },

        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ search(page,"false", from_date, to_date); });
        }
    })
}

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false", from_date_monthRepo, to_date_monthRepo);

function exportData(from_date, to_date) {
    $.ajax({
        url: "{{ $docs_url }}/{{ $lang }}/download/clinic/{{ $id_clinic }}/export/medical_record_icd_census",
        type: "GET",
        data: { type: "monthly", startDate: from_date, endDate: to_date },
        contentType: false,

        success: function(fileUrl){
            var link = document.createElement('a');
            link.href = fileUrl;
            link.click();
        },

        error: function(){
            notif(false, 'Terjadi Kesalahan Saat Melakukan Export.');
        }
    });
}

</script>
