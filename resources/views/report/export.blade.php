<div class="modal fade report modal-scroll" id="report-export" role="dialog" aria-labelledby="reportTitle" aria-hidden="true">
    <div id="report-export-dialog" class="modal-dialog modal-lg" role="document">        
        <div class="modal-content">
            <div class="modal-header">                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="key_report_export" />
            <div class="modal-body" id="loading-document-export">
                <div class="cell">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="reload-document-export">
                <div class="cell">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
            </div>
            <div class="modal-body hide" id="document-export">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-secondary" id="cancel-print-export" data-dismiss="modal" aria-label="Close">
                    {{ trans("messages.cancel") }}
                </button>
                <button type="button" class="btn-primary" id="print-export">
                    EXPORT
                </button>
                <button type="button" class="btn-view hide" id="mobile-report-export">
                    <img src="{{ asset('assets/images/icons/view/view-mobile.png') }}" />
                </button>
                <button type="button" class="btn-view hide" id="desktop-report-export">
                    <img src="{{ asset('assets/images/icons/view/view-desktop.png') }}" />
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#print-export").click(function() {
  var key = $("input[name='key_report_export']").val();
  $.ajax({        
    url: "{{ $docs_url }}/{{ $lang }}/download/clinic/{{ $id_clinic }}/export/"+key,
    type: "GET",
    processData: false,
    contentType: false,
    success: function(data){
      var obj = JSON.parse(data);
      var link = document.createElement('a');
      link.href = obj.resource;
      link.click();
    },
    error: function(){
      alert('ERROR');
    }
  });
});
</script>
