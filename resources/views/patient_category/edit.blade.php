<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.edit_patient_category') }}</h5>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="hide">
                <div class="modal-body alert-notif">
                    <input type="hidden" name="id" />
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.patient_category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_patient_category') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">

function resetEdit() {
    resetValidation('form-edit #name');

    $("#form-edit input[name=id]").val("");
    $("#form-edit input[name=name]").val("");
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading-edit').addClass('hide');

            if(!data.error) {
                $('#form-edit').removeClass('hide');

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=name]").val(data.name);
            } else {
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    })
}

$('#edit').on('show.bs.modal', function (e) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    resetEdit();

    var id = $(e.relatedTarget).attr('data-id');
    edit(id);
});


$('#form-edit input[name=name]').focus(function() {
    resetValidation('form-edit #name');
});

$("#form-edit").submit(function(event) {
    event.preventDefault();
    resetValidation('form-edit #name');

    $("#form-edit button").attr("disabled", true);

    var id = $("#form-edit input[name=id]").val();
    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("name", $("#form-edit input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-edit button").attr("disabled", false);

            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-edit #name',data.errors.name, true]);
                } else {
                    resetEdit();

                    $("#edit").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_edit_patient_category') }}");  
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
            
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>