<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_patient_category') }}</h5>
            </div>
            <form id="form-add" class="">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.patient_category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_patient_category') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

function resetAdd() {
    resetValidation('form-add #name');
    $("#form-add input[name=name]").val("");
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #name');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    formData.append("validate", false);
    formData.append("id_clinic", "{{ $id_clinic }}");
    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.name, true]);
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_add_patient_category') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>