@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/settings/menu-kat-pasien.png') }}" />
                    <h3>
                        {{ trans('messages.patient_category') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_patient_category") }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-12">
                                <div class="form-group search-group">
                                    <div class="input-group date search">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_patient_category') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="3" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="3" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    
    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td>" + item[i].name + "</td>");

                        var action = "<a data-toggle='modal' data-target='#edit' data-id='"+item[i].id+"' class='btn-table btn-orange'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /> {{ trans('messages.edit') }}</a>"
                                +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /> {{ trans('messages.delete') }}</a>";

                        if(item[i].default==1) {
                            tr.append("<td></td");
                        } else {
                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                        }

                        $("#table").append(tr);
                        no++;
                    }
                } else {
                    
                    $(".pagination-sec").addClass("hide");
                        
                    if(page==1 && q=='') {
                        $("#table").append("<tr><td align='center' colspan='2'>{{ trans('messages.empty_patient_category') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='3'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);

            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-category/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch);
                    notif(true,"{{ trans('validation.success_delete_patient_category') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val());
});

search(pageRepo,"false",searchRepo);

</script>

@include('patient_category.create')

@include('patient_category.edit')

@include('_partial.confirm_delele')