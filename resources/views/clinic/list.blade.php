@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div>

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-hospital-o"></i>
                    <h3>
                        {{ trans('messages.clinic') }}
                    </h3>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <div class="input-group date search">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_clinic') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2" class="sort" id="sort_name_asc" onclick="sort('name','desc')">
                                        {{ trans('messages.clinic') }}&nbsp;
                                        <i class="hide fa fa-caret-down" id="arrow_sort_name_asc"></i>
                                    </th>
                                    <th colspan="2" class="sort hide" id="sort_name_desc" onclick="sort('name','asc')">
                                        {{ trans('messages.clinic') }}&nbsp;
                                        <i class="fa fa-caret-up"></i>
                                    </th>
                                    <th>{{ trans('messages.responsible_name') }}</th>
                                    <th>{{ trans('messages.address') }}</th>


                                    <th class="sort hide" id="sort_last_login_asc" onclick="sort('last_login','desc')">
                                        {{ trans('messages.last_login') }}&nbsp;
                                        <i class="hide fa fa-caret-down" id="arrow_sort_last_login_asc"></i>
                                    </th>
                                    <th class="sort" id="sort_last_login_desc" onclick="sort('last_login','asc')">
                                        {{ trans('messages.last_login') }}&nbsp;
                                        <i class="fa fa-caret-up"></i>
                                    </th>
                                    <th>{{ trans('messages.status') }}</th>
                                    <th>{{ trans('messages.end_date') }}</th>
                                    <th>{{ trans('messages.created_at') }}</th>
                                    <th>{{ trans('messages.verification') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="table">
                                
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="10" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="10" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="order_by" value="last_login" />
<input type="hidden" name="order_type" value="desc" />

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;
var keyOrderBy = 'last_login';
var keyOrderType = 'desc';

function sort(by, type) {
    $("input[name='order_by']").val(by);
    $("input[name='order_type']").val(type);
    keyOrderBy = by;
    keyOrderType = type;

    if(by=="name") {
        $("#arrow_sort_name_asc").removeClass("hide");

        if(type=="asc") {
            $("#sort_name_asc").removeClass("hide");
            $("#sort_name_desc").addClass("hide");           
        } else {
            $("#sort_name_asc").addClass("hide");
            $("#sort_name_desc").removeClass("hide"); 
        }

        $("#arrow_sort_last_login_asc").addClass("hide");
        $("#sort_last_login_asc").removeClass("hide");
        $("#sort_last_login_desc").addClass("hide");
    } else {
        $("#arrow_sort_last_login_asc").removeClass("hide");

        if(type=="asc") {
            $("#sort_last_login_asc").removeClass("hide");
            $("#sort_last_login_desc").addClass("hide");           
        } else {
            $("#sort_last_login_asc").addClass("hide");
            $("#sort_last_login_desc").removeClass("hide"); 
        }

        $("#arrow_sort_name_asc").addClass("hide");
        $("#sort_name_asc").removeClass("hide");
        $("#sort_name_desc").addClass("hide");
    }

    search(numPage,"false",keySearch, by, type);
}

function search(page, submit, q, order_by, order_type) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;
    keyOrderBy = order_by;
    keyOrderType = order_type;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic",
        type: "GET",
        data: "q="+q+"&order_by="+order_by+"&order_type="+order_type+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            var action = "";
                            if(item[i].verification==0) {
                                action = action+"<a data-toggle='modal' data-target='#confirmVerification' data-message=\"{{ trans('messages.info_verification') }}\" data-id='"+item[i].id+"' class='btn-table btn-green'><span class='fa fa-check'></span> {{ trans('messages.verification') }}</a>";
                            }

                            action = action+"<a onclick='pay(\""+item[i].id+"\",\""+item[i].name+"\")' class='btn-table btn-orange'><span class='fa fa-dollar'></span> {{ trans('messages.pay') }}</a>";

                            tr = $('<tr/>');

                            tr.append("<td class='photo'>"+thumbnail('sm','clinic','{{ $storage }}',item[i].logo,item[i].name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/clinic/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a><span class='type'>" + nullToEmpty(item[i].email) + "</span><br /><span class='text'>" + nullToEmpty(item[i].phone) + "</span></td>");
                            tr.append("<td>" + item[i].user.name + "<br /><span class='type'>" + item[i].user.email + "</span><br /><span class='text'>+" + item[i].user.phone_code + "" + item[i].user.phone + "</span></td></td>");
                            tr.append("<td>" + nullToEmpty(item[i].address) + " " + nullToEmpty(item[i].village.name) + " " + nullToEmpty(item[i].district.name) + " " + nullToEmpty(item[i].regency.name) + " " + nullToEmpty(item[i].province.name) + " " + nullToEmpty(item[i].country.name) + "</td>");
                            tr.append("<td>" + getFormatDateTime(item[i].user.last_login) + "</td>");
                            tr.append("<td>" + statusClinic(item[i].status.code) + "</td>");
                            tr.append("<td>" + getFormatDate(item[i].end_active) + "</td>");                            
                            tr.append("<td>" + getFormatDateTime(item[i].created_at.date) + "</td>");
                            tr.append("<td>" + verification(item[i].verification) + "</td>");
                            tr.append("<td>"+action+"</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='10'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, order_by, order_type);

            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q, order_by, order_type); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q, order_by, order_type); });
        }
    })
}

$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val(), $("input[name=order_by]").val(), $("input[name=order_type]").val());
});

search(pageRepo,"false",searchRepo, keyOrderBy, keyOrderType);

$('#confirmVerification').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/"+id+"/verification/1",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyOrderBy, keyOrderType);

                    notif(true,"{{ trans('validation.success_verification') }}");
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmVerification").modal("toggle");
});

function pay(id, name) {
    $('#add').modal('show');

    var option = new Option(name, id, true, true);
    $("#form-add select[name='clinic']").append(option).trigger('change');
}

</script>


@include('_partial.confirm_verification')

@include('payment.create', ['list' => 'clinic'])


