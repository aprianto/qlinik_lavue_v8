@if(!$clinic->error)

@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="panel-content">
	<div class="row">
		<div class="col-md-10">
			<div class="profile-sec">
				<div class="profile-banner-sec">
					@if($clinic->banner==null)
					<div class="bg-banner">
					</div>	
					@endif
					@if($level=="user" && $preview!=true)

					@if(isset($acl->clinic))
                    @if($acl->clinic->update==true)
					<ul>
						<li id="edit-banner">							
							<label for="banner"><span class="icon-sec"><img src="{{ asset('assets/images/icons/action/upload.png') }}" /></span> <span>{{ trans('messages.upload_photo_banner') }}</span></label>
						</li>
						<li id="edit-logo">
							<label for="logo"><span class="icon-sec"><img src="{{ asset('assets/images/icons/action/upload.png') }}" /></span> <span>{{ trans('messages.upload_clinic_logo') }}</span></label>
						</li>
						<!--<li><a href="#/{{ $lang }}/clinic/setting" title="" onclick="loading($(this).attr('href'))"><i class="fa fa-pencil"></i> {{ trans('messages.edit_clinic') }}</a></li>-->
					</ul>
					@endif
					@endif

					@endif
					<div class="canvas-banner">
						@if($clinic->banner!=null && $level=="user" && $preview!=true)

						@if(isset($acl->clinic))
	                    @if($acl->clinic->update==true)
						<a id="reset-banner" class="reset-banner">
							<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />
						</a>
						@endif
						@endif

						

						@endif
						@if($preview==true)
						<a class="back" onclick="back();loading($(this).attr('href'))">
							<img class="icon" src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
						</a>
						@endif
						@if($clinic->banner!=null)
						<img src="{{ $storage.'/images/clinic/banner/'.$clinic->banner }}" alt="" />
						@endif
					</div>
				</div>
				<div class="user-bar">
					<div class="row">
						<div class="col-md-9">
							<div class="user-thumb">
								<div class="canvas-logo">
									@if($clinic->logo!=null && $level=="user" && $preview!=true)

									@if(isset($acl->clinic))
				                    @if($acl->clinic->update==true)
									<div id="reset-logo" class="reset-logo">
										<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />
									</div>
									@endif
									@endif

									@endif
									<?php echo thumbnail('hw','clinic',$storage,$clinic->logo,$clinic->name, 128, 128); ?>
								</div>
								<div class="user-info">
									<h3>{{ $clinic->name }}</h3>
									<span><img src="{{ asset('assets/images/icons/info/location.png') }}" /> {{ $clinic->regency->name }}</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<ul>
								<li>
									<b id="total_patient">&nbsp;</b>
									<span>{{ trans('messages.patient') }}</span>
								</li>
								<li>
									<b id="total_doctor">&nbsp;</b>
									<span>{{ trans('messages.doctor') }}</span>
								</li>				
							</ul>
						</div>
					</div>
				</div>
				<div class="profile-all">
					<div class="row">
						<div class="col-md-12">
							<div class="widget widget-detail">
								<div class="widget-controls">
									@if($level=="user" && $preview!=true)

									@if(isset($acl->clinic))
				                    @if($acl->clinic->update==true)
									<a href="#/{{ $lang }}/clinic/setting" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									@endif
									@endif

									@endif
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.info') }}</h3>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="contact-details-info">
												<ul>
						                            <li>
						                                <div class="detail-sec">
						                                    <div class="icon-sec">
						                                        <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
						                                    </div>
						                                    <div class="info-sec">
						                                        <span class="title">{{ trans('messages.phone_number') }}</span>
						                                        <span>{{ checkNull($clinic->phone) }}</span>
						                                    </div>
						                                </div>
						                            </li>
						                           	<li>
						                                <div class="detail-sec">
						                                    <div class="icon-sec">
						                                        <img src="{{ asset('assets/images/icons/info/email.png') }}" />
						                                    </div>
						                                    <div class="info-sec">
						                                        <span class="title">{{ trans('messages.email') }}</span>
						                                        <span>{{ checkNull($clinic->email) }}</span>
						                                    </div>
						                                </div>
						                            </li>
						                        </ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget widget-detail" id="widget-polyclinic">
								<div class="widget-controls">
									@if($level=="user" && $preview!=true)

									@if(isset($acl->clinic))
				                    @if($acl->clinic->update==true)
									<a id="link-edit-polyclinic"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									<a id="link-cancel-edit-polyclinic" class="hide"><img src="{{ asset('assets/images/icons/nav/delete-m.png') }}" /></a>
									@endif
									@endif

									@endif
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.list_polyclinic') }}</h3>
									</div>
									<div class="cell hide" id="loading-page">
				                        <div class="card">
				                            <span class="three-quarters">Loading&#8230;</span>
				                        </div>
				                    </div>
				                    <div class="cell hide" id="reload">
				                        <div class="card">
				                            <span class="reload fa fa-refresh"></span>
				                        </div>
				                    </div>
									<div class="row" id="section-polyclinic">
										<div class="col-md-12">
											<div id="list-polyclinic">
											</div>									
										</div>
									</div>
									<form id="edit-polyclinic" class="hide alert-notif">
										<div class="form-group" id="polyclinic-group">
											<div class="row">
					                            <div class="col-md-12">
						                            <div class="border-group">
						                            	<select id="polyclinic" name="polyclinic[]" multiple="multiple"></select>
						                            </div>                      
						                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                            <span class="help-block"></span>
						                        </div>
						                    </div>
						                </div>
						                <div class="form-group last-item">  
							                <div class="row">
							                	<div class="col-md-6">
							                		<button type="button" id="icon-add" class="button-add @if($level != 'admin') hide @endif" data-toggle="modal" data-target="#add">+ {{ trans('messages.add_polyclinic') }}</button>
							                	</div>
	                            				<div class="col-md-6">
													<button type="submit" id="btn-edit-polyclinic">
														<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
														{{ trans('messages.save') }}
													</button>
												</div>
											</div>											
										</div>
									</form>
								</div>
							</div>
							<div class="widget widget-detail" id="widget-insurance">
								<div class="widget-controls">
									@if($level=="user" && $preview!=true)

									@if(isset($acl->clinic))
				                    @if($acl->clinic->update==true)
									<a id="link-edit-insurance"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									<a id="link-cancel-edit-insurance" class="hide"><img src="{{ asset('assets/images/icons/nav/delete-m.png') }}" /></a>
									@endif
									@endif

									@endif
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.list_insurance') }}</h3>
									</div>
									<div class="cell hide" id="loading-page">
				                        <div class="card">
				                            <span class="three-quarters">Loading&#8230;</span>
				                        </div>
				                    </div>
				                    <div class="cell hide" id="reload">
				                        <div class="card">
				                            <span class="reload fa fa-refresh"></span>
				                        </div>
				                    </div>
									<div class="row" id="section-insurance">
										<div class="col-md-12">
											<div id="list-insurance">
											</div>									
										</div>
									</div>
									<form id="edit-insurance" class="hide alert-notif">
										<div class="form-group" id="insurance-group"> 
											<div class="row">
					                            <div class="col-md-12">          
						                            <div class="border-group">
						                            	<select id="insurance" name="insurance[]" multiple="multiple"></select>
						                            </div>
						                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                            <span class="help-block"></span>
						                        </div>
						                    </div>
				                        </div>
				                        <div class="form-group last-item">  
							                <div class="row">
							                	<div class="col-md-12">
							                		<button type="submit" id="btn-edit-insurance">
														<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
														{{ trans('messages.save') }}
													</button>
							                	</div>
							                </div>
							            </div>
									</form>
								</div>
							</div>
							<div class="widget widget-detail" id="widget-location">
								<div class="widget-controls" id="link-edit-location">
									@if($level=="user" && $preview!=true)

									@if(isset($acl->clinic))
				                    @if($acl->clinic->update==true)
									<a href="#/{{ $lang }}/clinic/location" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
									@endif
									@endif

									@endif
								</div>
								<div class="save-draft">
									<div class="widget-title">
										<h3>{{ trans('messages.location') }}</h3>
									</div>
									<div class="cell hide" id="loading-location">
				                        <div class="card">
				                            <span class="three-quarters">Loading&#8230;</span>
				                        </div>
				                    </div>
				                    <div class="cell hide" id="reload-location">
				                        <div class="card">
				                            <span class="reload fa fa-refresh"></span>
				                        </div>
				                    </div>
									<div class="row" id="section-location">
										<div class="col-md-12">
											<div class="contact-details-info">
												<ul>
						                            <li>
						                                <div class="detail-sec">
						                                    <div class="icon-sec">
						                                        <img src="{{ asset('assets/images/icons/info/location.png') }}" />
						                                    </div>
						                                    <div class="info-sec">
						                                        <span class="title">{{ trans('messages.address') }}</span>
						                                        <span>{{ $clinic->address }} {{ $clinic->village->name }} {{ $clinic->district->name }} {{ $clinic->regency->name }} {{ $clinic->province->name }} {{ $clinic->country->name }}</span>
						                                    </div>
						                                </div>
						                            </li>
						                            <li>
						                            	<div class="detail-sec">
						                                    <div class="icon-sec">
						                                    </div>
						                                    <div class="info-sec">
						                                        <div id="map" class="hide"></div>
						                                    </div>
						                                </div>
						                            </li>
						                        </ul>
												<button id="btn-show-map" onclick="show_map();">{{ trans('messages.show_location_map') }}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="form-logo">
	<input accept="image/*" type="file" name="logo[]" id="logo" class="new-img" style="visibility: hidden" />
</form>
<form id="form-banner">
	<input accept="image/*" type="file" name="banner[]" id="banner" class="new-banner" style="visibility: hidden" />
</form>

<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            	<img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_new_polyclinic') }}</h5>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <form id="form-add" class="alert-notif">
                <div class="modal-body modal-main">               
                    <div class="form-group" id="name-group">
                    	<label class="control-label">{{ trans('messages.polyclinic_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_polyclinic_name') }}" name="name"  autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="script">
</div>

<script type="text/javascript">

function show_map() {
	$("#script").html('<script src="https://maps.googleapis.com/maps/api/js?key={{ $map_key }}&callback=initMap" async defer><\/script>');
	$("#map").removeClass("hide");
	$("#btn-show-map").addClass("hide");
}

$("#polyclinic").select2({
    placeholder: "{{ trans('messages.select_polyclinic') }}"
});

function initMap() {
	$('#widget-location .widget-title').removeClass('hide');
	$('#widget-location').addClass('loading-wait');
	$('#section-location').addClass('hide');
	$("#reload-location").addClass("hide");

	var url = 'profile';
	@if($level=="admin")
	url = '{{ $clinic->id }}';
	@endif


	$.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/"+url,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
            	$('#widget-location').removeClass('loading-wait');
	            $('#widget-location .widget-title').removeClass('hide');
	    		$('#section-location').removeClass('hide');
	    		$('#link-edit-location').removeClass('hide');

				var marker;
				var geocoder;
				if(data.address=="") {
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 12,
						center: {lat: {{ default_location('latitude') }}, lng: {{ default_location('longitude') }}}
					});
				} else {
					if(data.latitude==0 || data.longitude==0 || data.latitude==null || data.longitude==null) { 
						var address = data.address+' '+data.village.name+' '+data.district.name+' '+data.regency.name+' '+data.province.name+' '+data.country.name;

					    geocoder = new google.maps.Geocoder();
					    geocoder.geocode({
					        'address': address
					    }, function(results, status) {
					        if (status == google.maps.GeocoderStatus.OK) {
					            var myOptions = {
					                zoom: 15,
					                center: results[0].geometry.location,
					                mapTypeId: google.maps.MapTypeId.ROADMAP
					            }
					            map = new google.maps.Map(document.getElementById("map"), myOptions);

					            marker = new google.maps.Marker({
					                map: map,
					                animation: google.maps.Animation.DROP,
					                position: results[0].geometry.location
					              });
					        }
					    }); 
					} else {
						var map = new google.maps.Map(document.getElementById('map'), {
							zoom: 15,
							center: {lat: data.latitude, lng: data.longitude}
						});

						marker = new google.maps.Marker({
							map: map,
							animation: google.maps.Animation.DROP,
							position: {lat: data.latitude, lng: data.longitude}
						});
					}
				}
			} else {
				$('#widget-location').removeClass('loading-wait');
	            $('#widget-location .widget-title').addClass('hide');
	    		$('#section-location').addClass('hide');
	    		$('#link-edit-location').addClass('hide');
	            $("#reload-location").removeClass("hide");

	            $("#reload-location .reload").click(function(){ initMap(); });
			}
			
        },
        error: function(){
            $('#widget-location').removeClass('loading-wait');
            $('#widget-location .widget-title').addClass('hide');
    		$('#section-location').addClass('hide');
    		$('#link-edit-location').addClass('hide');
            $("#reload-location").removeClass("hide");

            $("#reload-location .reload").click(function(){ initMap(); });
        }
    })
}

@if($level=="admin")
var url_total_doctor =  'clinic/{{ $clinic->id }}/total-doctor'; 
var url_total_patient =  'clinic/{{ $clinic->id }}/total-patient'; 
@else
var url_total_doctor =  'clinic/doctor'; 
var url_total_patient =  'clinic/patient'; 
@endif

$.ajax({
    url: "{{ $api_url }}/{{ $lang }}/"+url_total_doctor,
    type: "GET",
    processData: false,
    contentType: false,
    success: function(data){
        

        if(data.error==false) {
        	$("#total_doctor").html(data.total);
        } else {
			$("#total_doctor").html("-");
        }
    },
    error: function(){
        $("#total_doctor").html("-");
    }
});

$.ajax({
    url: "{{ $api_url }}/{{ $lang }}/"+url_total_patient,
    type: "GET",
    processData: false,
    contentType: false,
    success: function(data){
        

        if(data.error==false) {
        	$("#total_patient").html(data.total);
        } else {
        	$("#total_patient").html("-");
        }
        
    },
    error: function(){
        $("#total_patient").html("-");
    }
});

function profilePoly() {
	$('#widget-polyclinic .widget-title').removeClass('hide');
	$('#widget-polyclinic').addClass('loading-wait');
	$('#edit-polyclinic').addClass('hide');
	$("#reload").addClass("hide");

	var url = '';
	@if($level=="admin")
	url = 'clinic/{{ $clinic->id }}/polyclinic';
	@else
	url = 'clinic/polyclinic';
	@endif

	$.ajax({
        url: "{{ $api_url }}/{{ $lang }}/"+url,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#widget-polyclinic').removeClass('loading-wait');
            
			if(data.error==false) {         
	    		$('#section-polyclinic').removeClass('hide');
	    		$('#link-edit-polyclinic').removeClass('hide');
				$('#link-cancel-edit-polyclinic').addClass('hide');

	            $('#list-polyclinic').html('');

	            var item = data.data;

	            for (var i = 0; i < item.length; i++) {
	                $('#list-polyclinic').append('<div class="list-polyclinic">'+item[i].name+'</div>');
	            }
        	} else {
	            $('#widget-polyclinic .widget-title').addClass('hide');
	    		$('#section-polyclinic').addClass('hide');
	    		$('#link-edit-polyclinic').addClass('hide');
				$('#link-cancel-edit-polyclinic').addClass('hide');
	            $("#reload").removeClass("hide");

	            $("#reload .reload").click(function(){ profilePoly(); });
        	}
        },
        error: function(){
            $('#widget-polyclinic').removeClass('loading-wait');
            $('#widget-polyclinic .widget-title').addClass('hide');
    		$('#section-polyclinic').addClass('hide');
    		$('#link-edit-polyclinic').addClass('hide');
			$('#link-cancel-edit-polyclinic').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ profilePoly(); });
        }
    })
}

profilePoly();

$("#link-cancel-edit-polyclinic").click(function() {
	profilePoly();
});


function listPoly(val) {
    loading_content("#polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){

            if(!dataPoly.error) {  
                loading_content("#polyclinic-group", "success");              

                $("#polyclinic-group .border-group select").html('');

                itemPoly = dataPoly.data;

                for (var i = 0; i < itemPoly.length; i++) {
                    if(val.length>0) {
                        var selected = "";
                        for (var j = 0; j < val.length; j++) {
                            if(itemPoly[i].id==val[j]) {
                                selected = "selected='selected'";
                            }

                        }
                    }

                    $("#polyclinic").append("<option value='"+itemPoly[i].id+"' "+selected+">"+itemPoly[i].name+"</option>")
                }

                $("#polyclinic").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: true
                });
                

            } else {
                loading_content("#polyclinic-group", "failed");

                $("#polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#polyclinic-group", "failed");

            $("#polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function editPoly() {
	resetValidation('edit-polyclinic #polyclinic');
    $('#widget-polyclinic .widget-title').removeClass('hide');
	$('#widget-polyclinic').addClass('loading-wait');
	$('#section-polyclinic').addClass('hide');
	$("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPolyClinic){

            if(!dataPolyClinic.error) {
                $('#widget-polyclinic').removeClass('loading-wait');
		        $('#link-edit-polyclinic').addClass('hide');
		        $('#edit-polyclinic').removeClass('hide');
				$('#link-cancel-edit-polyclinic').removeClass('hide');
            
                var itemPolyClinic = dataPolyClinic.data;

                var arrayPoly = [];
                for (var i = 0; i < itemPolyClinic.length; i++) {
                    arrayPoly.push(itemPolyClinic[i].id);
                }

                listPoly(arrayPoly);

            } else {
                $('#widget-polyclinic .widget-title').addClass('hide');
	            $('#widget-polyclinic').removeClass('loading-wait');
	    		$('#edit-polyclinic').addClass('hide');
	    		$('#link-edit-polyclinic').addClass('hide');
	    		$('#link-cancel-edit-polyclinic').removeClass('hide');
	    		$("#reload").removeClass("hide");

	            $("#reload .reload").click(function(){ editPoly(); });
            }
        },
        error: function(){
            $('#widget-polyclinic .widget-title').addClass('hide');
            $('#widget-polyclinic').removeClass('loading-wait');
    		$('#edit-polyclinic').addClass('hide');
    		$('#link-edit-polyclinic').addClass('hide');
    		$('#link-cancel-edit-polyclinic').removeClass('hide');
    		$("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ editPoly(); });
        }
    })
}

$("#link-edit-polyclinic").click(function() {
	editPoly();
});




$('#add').on('show.bs.modal', function (e) {
    $("#form-add input[name=name]").val("");

    resetValidation('form-add #name');
    resetValidation('edit-polyclinic #polyclinic');
});

$("#form-add input[name=name]").focus(function() {
	resetValidation('form-add #name');
});

$("#form-add").submit(function(event) {
	event.preventDefault();
	resetValidation('form-add #name');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();
    formData.append("validate", false);
    formData.append("indonesian_name", $("#form-add input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass('loading');
	$("#form-add .btn-primary span").removeClass('hide');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/polyclinic",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass('loading');
	        $("#form-add .btn-primary span").addClass('hide');

	        if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.indonesian_name, true]);
                } else {

                    $("#form-add input[name=name]").val("");
                    
                    notif(true,"{{ trans('validation.success_add_polyclinic') }}");

                    var selectPoly = []; 
                    $('#polyclinic :selected').each(function (i, selected) {
                        selectPoly.push($(selected).val());
                    });
                    selectPoly.push(data.id);

                    listPoly(selectPoly);

                    $("#add").modal("toggle");
            
                }
            } else {
            	notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
        	$("#form-add button").attr("disabled", false);

        	$("#form-add .btn-primary").addClass('loading');
			$("#form-add .btn-primary span").removeClass('hide');

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});


$("#polyclinic-group .border-group").click(function() {
	resetValidation('edit-polyclinic #polyclinic');
});

$("#edit-polyclinic").submit(function(event) {
	resetValidation('edit-polyclinic #polyclinic');

	var polyclinic = $("#polyclinic").val();
    if(polyclinic==null) {
        formValidate(true, ['polyclinic','{{ trans("validation.min_polyclinic") }}', true]);
    } else {
    	event.preventDefault();
		$("#edit-polyclinic button").attr("disabled", true);

		formData= new FormData();
		formData.append("_method", "PATCH");	
        formData.append("validate", false);
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        var selectPoly = new Array(); 

        $('#polyclinic :selected').each(function (i, selected) {
            selectPoly[i] = $(selected).val();
            formData.append("polyclinic["+i+"]", selectPoly[i]);
        });

        $("#btn-edit-polyclinic").addClass('loading');
        $("#btn-edit-polyclinic span").removeClass('hide');

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                

                $("#edit-polyclinic button").attr("disabled", false);
                $("#btn-edit-polyclinic").removeClass('loading');
                $("#btn-edit-polyclinic span").addClass('hide');

                if(!data.error) {
                    profilePoly();
                    notif(true,"{{ trans('validation.success_select_polyclinic') }}");    
            	} else {
            		notif(false,"{{ trans('validation.failed') }}");
            	}
            },
	        error: function(){
	        	$("#edit-polyclinic button").attr("disabled", false);

	        	$("#btn-edit-polyclinic").removeClass('loading');
                $("#btn-edit-polyclinic span").addClass('hide');

	            notif(false,"{{ trans('validation.failed') }}");
	        }
        })
    }
});

function profileInsurance() {
	$('#widget-insurance .widget-title').removeClass('hide');
	$('#widget-insurance').addClass('loading-wait');
	$('#edit-insurance').addClass('hide');
	$("#reload").addClass("hide");

	var url = '';
	@if($level=="admin")
	url = 'clinic/{{ $clinic->id }}/insurance';
	@else
	url = 'clinic/insurance';
	@endif

	$.ajax({
        url: "{{ $api_url }}/{{ $lang }}/"+url,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#widget-insurance').removeClass('loading-wait');
            
			if(data.error==false) {         
	    		$('#section-insurance').removeClass('hide');
	    		$('#link-edit-insurance').removeClass('hide');
				$('#link-cancel-edit-insurance').addClass('hide');

	            $('#list-insurance').html('');

	            var item = data.data;

	            if(item.length>0) {
		            for (var i = 0; i < item.length; i++) {
		                $('#list-insurance').append('<div class="list-polyclinic">'+item[i].name+'</div>');
		            }
		        } else {
		        	$('#list-insurance').append('<span>{{ trans('messages.no_insurance') }}</span>');
		        }
        	} else {
	            $('#widget-insurance .widget-title').addClass('hide');
	    		$('#section-insurance').addClass('hide');
	    		$('#link-edit-insurance').addClass('hide');
				$('#link-cancel-edit-insurance').addClass('hide');
	            $("#reload").removeClass("hide");

	            $("#reload .reload").click(function(){ profileInsurance(); });
        	}
        },
        error: function(){
            $('#widget-insurance').removeClass('loading-wait');
            $('#widget-insurance .widget-title').addClass('hide');
    		$('#section-insurance').addClass('hide');
    		$('#link-edit-insurance').addClass('hide');
			$('#link-cancel-edit-insurance').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ profileInsurance(); });
        }
    })
}

profileInsurance();

$("#link-cancel-edit-insurance").click(function() {
	profileInsurance();
});


function listInsurance(val) {
    loading_content("#insurance-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsurance){

            if(!dataInsurance.error) {  
                loading_content("#insurance-group", "success");              

                $("#insurance-group .border-group select").html('');

                itemInsurance = dataInsurance.data;

                for (var i = 0; i < itemInsurance.length; i++) {
                    if(val.length>0) {
                        var selected = "";
                        for (var j = 0; j < val.length; j++) {
                            if(itemInsurance[i].id==val[j]) {
                                selected = "selected='selected'";
                            }

                        }
                    }

                    $("#insurance").append("<option value='"+itemInsurance[i].id+"' "+selected+">"+itemInsurance[i].name+"</option>")
                }

                $("#insurance").select2({
                    placeholder: "{{ trans('messages.select_insurance') }}",
                    allowClear: true
                });
                

            } else {
                loading_content("#insurance-group", "failed");

                $("#insurance-group #loading-content").click(function(){ listInsurance(val); });
            }
        },
        error: function(){
            loading_content("#insurance-group", "failed");

            $("#insurance-group #loading-content").click(function(){ listInsurance(val); });
        }
    })
}

function editInsurance() {
	resetValidation('edit-insurance #insurance');
    $('#widget-insurance .widget-title').removeClass('hide');
	$('#widget-insurance').addClass('loading-wait');
	$('#section-insurance').addClass('hide');
	$("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsuranceClinic){

            if(!dataInsuranceClinic.error) {
                $('#widget-insurance').removeClass('loading-wait');
		        $('#link-edit-insurance').addClass('hide');
		        $('#edit-insurance').removeClass('hide');
				$('#link-cancel-edit-insurance').removeClass('hide');
            
                var itemInsuranceClinic = dataInsuranceClinic.data;

                var arrayInsurance = [];
                for (var i = 0; i < itemInsuranceClinic.length; i++) {
                    arrayInsurance.push(itemInsuranceClinic[i].id);
                }

                listInsurance(arrayInsurance);

            } else {
                $('#widget-insurance .widget-title').addClass('hide');
	            $('#widget-insurance').removeClass('loading-wait');
	    		$('#edit-insurance').addClass('hide');
	    		$('#link-edit-insurance').addClass('hide');
	    		$('#link-cancel-edit-insurance').removeClass('hide');
	    		$("#reload").removeClass("hide");

	            $("#reload .reload").click(function(){ editInsurance(); });
            }
        },
        error: function(){
            $('#widget-insurance .widget-title').addClass('hide');
            $('#widget-insurance').removeClass('loading-wait');
    		$('#edit-insurance').addClass('hide');
    		$('#link-edit-insurance').addClass('hide');
    		$('#link-cancel-edit-insurance').removeClass('hide');
    		$("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ editInsurance(); });
        }
    })
}

$("#link-edit-insurance").click(function() {
	editInsurance();
});

$("#insurance-group .border-group").click(function() {
	resetValidation('edit-insurance #insurance');
});

$("#edit-insurance").submit(function(event) {
	resetValidation('edit-insurance #insurance');

	var insurance = $("#insurance").val();

	event.preventDefault();
	$("#edit-insurance button").attr("disabled", true);

	formData= new FormData();
	formData.append("_method", "PATCH");	
    formData.append("validate", false);
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");    

    if(insurance==null) {
    	formData.append("insurance", "");
    } else {
    	var selectInsurance = new Array(); 

	    $('#insurance :selected').each(function (i, selected) {
	        selectInsurance[i] = $(selected).val();
	        formData.append("insurance["+i+"]", selectInsurance[i]);
	    });
    }

    $("#btn-edit-insurance").addClass('loading');
    $("#btn-edit-insurance span").removeClass('hide');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/insurance",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#edit-insurance button").attr("disabled", false);
            $("#btn-edit-insurance").removeClass('loading');
            $("#btn-edit-insurance span").addClass('hide');

            if(!data.error) {
                profileInsurance();
                notif(true,"{{ trans('validation.success_select_insurance') }}");    
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("#edit-insurance button").attr("disabled", false);

        	$("#btn-edit-insurance").removeClass('loading');
            $("#btn-edit-insurance span").addClass('hide');

            notif(false,"{{ trans('validation.failed') }}");
        }
    })

});

$('.new-img').change(function (event) {
	setTimeout(function(){	
		event.preventDefault();
        $(".new-img").attr("disabled", true);

	 	var id = "{{ $clinic->id }}";
	    
	    formData= new FormData();
	    formData.append("_method", "PATCH");		    
	    formData.append("validate", false);
	    formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

	    var logo = document.getElementById("logo");
	    file = logo.files[0];
	    formData.append("logo", file);

	    $("#edit-logo").addClass('loading');
	    //$("#edit-logo label i").removeClass('fa fa-picture-o');
	    //$("#edit-logo label i").addClass('fa fa-circle-o-notch fa-spin fa-3x fa-fw');    
	    $("#edit-logo label .icon-sec").html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>');   

	    $.ajax({
	        url: "{{ $api_url }}/{{ $lang }}/clinic",
	        type: "POST",
	        data: formData,
	        processData: false,
	        contentType: false,
	        success: function(data){
	            
	            $(".new-img").attr("disabled", false);

	            $("#edit-logo").removeClass('loading');
	            $("#edit-logo label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

	            if(!data.error) {
		            if(!data.success) {
		            	notif(false, data.errors.logo);
		            } else {
		                notif(true,"{{ trans('validation.success_edit_logo_clinic') }}");

		                setTimeout(function(){
							redirect('redirect/{{ $lang }}/clinic');
						}, 500);  
		            }
	        	} else {
	        		notif(false,"{{ trans('validation.failed') }}");
	        	}
	        },
	        error: function(){
	        	$(".new-img").attr("disabled", false);

	        	$("#edit-logo").removeClass('loading');
	            $("#edit-logo label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

	            notif(false,"{{ trans('validation.failed') }}");
	        }
	    })

	},100);
});

$('.new-banner').change(function (event) {
	setTimeout(function(){	
		event.preventDefault();
		$(".new-banner").attr("disabled", true);

	 	var id = "{{ $clinic->id }}";
	    
	    formData= new FormData();
	    formData.append("_method", "PATCH");		    
	    formData.append("validate", false);
	    formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

	    var banner = document.getElementById("banner");
	    file = banner.files[0];
	    formData.append("banner", file);

	    $("#edit-banner").addClass('loading');
	    //$("#edit-banner label i").removeClass('fa fa-picture-o');
	    //$("#edit-banner label i").addClass('fa fa-circle-o-notch fa-spin fa-3x fa-fw');
	    $("#edit-banner label .icon-sec").html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>');    

	    $.ajax({
	        url: "{{ $api_url }}/{{ $lang }}/clinic",
	        type: "POST",
	        data: formData,
	        processData: false,
	        contentType: false,
	        success: function(data){
	            
	            $(".new-banner").attr("disabled", false);

	            $("#edit-banner").removeClass('loading');
	            $("#edit-banner label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

	            if(!data.error) {
		            if(!data.success) {
		            	notif(false, data.errors.banner);
		            } else {
		                notif(true,"{{ trans('validation.success_edit_banner_clinic') }}");

		                setTimeout(function(){
							redirect('redirect/{{ $lang }}/clinic');
						}, 500);  
		            }
	        	} else {
	        		notif(false,"{{ trans('validation.failed') }}");
	        	}
	        },
	        error: function(){
	        	$(".new-banner").attr("disabled", false);

	        	$("#edit-banner").removeClass('loading');
	            $("#edit-banner label .icon-sec").html('<img src=\'{{ asset("assets/images/icons/action/upload.png") }}\' />');

	            notif(false,"{{ trans('validation.failed') }}");
	        }
	    })

	},100);
});


$("#reset-logo").click(function(){
	event.preventDefault();
	$("#reset-logo").attr("disabled", true);

    //$("#reset-logo").attr("class","reset-logo fa fa-circle-o-notch fa-spin fa-3x fa-fw");
    $("#reset-logo").html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/reset-logo",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
			$("#reset-logo").attr("disabled", false);

            $("#reset-logo").html('<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />');

            if(!data.error) {
	            if(!data.success) {
	            	notif(false,"{{ trans('validation.failed') }}");
	            } else {
	                setTimeout(function(){
						redirect('redirect/{{ $lang }}/clinic');
					}, 500); 
	            }
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
			$("#reset-logo").attr("disabled", false);

        	$("#reset-logo").html('<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />');

            notif(false,"{{ trans('validation.failed') }}");
        }
    }) 
	
});

$("#reset-banner").click(function(){
	event.preventDefault();
	$("#reset-banner").attr("disabled", true);

	//$("#reset-banner span").attr("class","fa fa-circle-o-notch fa-spin fa-3x fa-fw");
	$("#reset-banner").html('<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span>');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/reset-banner",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#reset-banner").attr("disabled", false);

            $("#reset-banner").html('<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />');

            if(!data.error) {
	            if(!data.success) {
	            	notif(false,"{{ trans('validation.failed') }}");
	            } else {

	                setTimeout(function(){
						redirect('redirect/{{ $lang }}/clinic');
					}, 500); 
	            }
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("#reset-banner").attr("disabled", false);
        	
        	$("#reset-banner").html('<img class="icon" src="{{ asset('assets/images/icons/action/delete.png') }}" />');

            notif(false,"{{ trans('validation.failed') }}");
        }
    }) 
});

</script>

@endif