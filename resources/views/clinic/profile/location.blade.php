@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.clinic_location') }}
                    </h3>
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide">
                    <form role="form" class="sec">
                        <input type="hidden" name="latitude" />
                        <input type="hidden" name="longitude" />                               
                        <div class="form-group" id="country-group">
                            <label class="control-label">{{ trans('messages.country') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="country">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="province-group">
                            <label class="control-label">{{ trans('messages.province') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="province" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="regency-group">
                            <label class="control-label">{{ trans('messages.regency') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="regency" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                                
                        <div class="form-group" id="district-group">
                            <label class="control-label">{{ trans('messages.district') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="district" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                            
                        <div class="form-group" id="village-group">
                            <label class="control-label">{{ trans('messages.village') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="village" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="address-group">
                            <label class="control-label">{{ trans('messages.address') }} </label>
                            <input type="text" name="address" class="form-control" placeholder="{{ trans('messages.input_address') }}" />
                            <span class="help-block"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="postal_code-group">
                                    <label class="control-label">{{ trans('messages.postal_code') }} </label>
                                    <input type="text" name="postal_code" class="form-control" placeholder="{{ trans('messages.input_postal_code') }}" />
                                    <span class="help-block"></span>
                                </div>  
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" id="searchMap" id="icon-add" class="button-add">{{ trans('messages.search_map') }}
                            </button>
                            <div id="map"></div>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/clinic" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key={{ $map_key }}&callback=edit"
          async defer></script>


<script type="text/javascript">

$("select[name='country']").select2({
    placeholder: "{{ trans('messages.select_country') }}",
    allowClear: false
});

$("select[name='province']").select2({
    placeholder: "{{ trans('messages.select_province') }}",
    allowClear: false
});

$("select[name='regency']").select2({
    placeholder: "{{ trans('messages.select_regency') }}",
    allowClear: false
});

$("select[name='district']").select2({
    placeholder: "{{ trans('messages.select_district') }}",
    allowClear: false
});

$("select[name='village']").select2({
    placeholder: "{{ trans('messages.select_village') }}",
    allowClear: false
});

var get_country = "";
var get_province = "";
var get_regency = "";
var get_district = "";
var get_village = "";
var get_postal_code = "";

function resetData() {
    get_country = "";
    get_province = "";
    get_regency = "";
    get_district = "";
    get_village = "";
    get_postal_code = "";
}

function listCountry(val) {
    loading_content("#country-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/country",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#country-group", "success");

                var item = data.data;

                $("select[name='country']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='country']").append("<option value='"+toTitleCase(item[i].name)+"'  data-id='"+item[i].name+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("select[name='country']").select2({
                    placeholder: "{{ trans('messages.select_country') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='country']").val(val).trigger("change");
                }

            } else {
                loading_content("#country-group", "failed");

                $("#country-group #loading-content").click(function(){ listCountry(val); });
            }
        },
        error: function(){
            loading_content("#country-group", "failed");

            $("#country-group #loading-content").click(function(){ listCountry(val); });
        }
    })
}


function listProvince(val, id_country) {
    loading_content("#province-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/province",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#province-group", "success");

                var item = data.data;

                $("select[name='province']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='province']").append("<option value='"+checkLocationCase(toTitleCase(item[i].name))+"'  data-id='"+item[i].id+"'>"+checkLocationCase(toTitleCase(item[i].name))+"</option>");
                }

                $("select[name='province']").select2({
                    placeholder: "{{ trans('messages.select_province') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='province']").val(val).trigger("change");
                }

            } else {
                loading_content("#province-group", "failed");

                $("#province-group #loading-content").click(function(){ listProvince(val, id_country); });
            }
        },
        error: function(){
            loading_content("#province-group", "failed");

            $("#province-group #loading-content").click(function(){ listProvince(val, id_country); });
        }
    })
}


function listRegency(val, id_province) {
    loading_content("#regency-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/regency?id_province="+id_province,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#regency-group", "success");

                var item = data.data;

                $("select[name='regency']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='regency']").append("<option value='"+toTitleCase(item[i].name)+"'  data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("select[name='regency']").select2({
                    placeholder: "{{ trans('messages.select_regency') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='regency']").val(val).trigger("change");
                }

            } else {
                loading_content("#regency-group", "failed");

                $("#regency-group #loading-content").click(function(){ listRegency(val, id_province); });
            }
        },
        error: function(){
            loading_content("#regency-group", "failed");

            $("#regency-group #loading-content").click(function(){ listRegency(val, id_province); });
        }
    })
}

function listDistrict(val, id_regency) {
    loading_content("#district-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/district?id_regency="+id_regency,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#district-group", "success");

                var item = data.data;

                $("select[name='district']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='district']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("select[name='district']").select2({
                    placeholder: "{{ trans('messages.select_district') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='district']").val(val).trigger("change");
                }

            } else {
                loading_content("#district-group", "failed");

                $("#district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
            }
        },
        error: function(){
            loading_content("#district-group", "failed");

            $("#district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
        }
    })
}

function listVillage(val, id_district) {
    loading_content("#village-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/village?id_district="+id_district,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#village-group", "success");

                var item = data.data;

                $("select[name='village']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='village']").append("<option value='"+toTitleCase(item[i].name)+"' data-postal_code='"+item[i].postal_code+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("select[name='village']").select2({
                    placeholder: "{{ trans('messages.select_village') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='village']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#village-group", "failed");

                $("#village-group #loading-content").click(function(){ listVillage(val, id_district); });
            }
        },
        error: function(){
            loading_content("#village-group", "failed");

            $("#village-group #loading-content").click(function(){ listVillage(val, id_district); });
        }
    })
}

function edit() {
    $('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/profile",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading').addClass('hide');
            if(!data.error) {
                $('.form-elements-sec').removeClass('hide');
                $("input[name=postal_code]").val(data.postal_code);

                get_country = data.country.name;
                get_province = data.province.name;
                get_regency = data.regency.name;
                get_district = data.district.name;
                get_village = data.village.name;
                get_postal_code = data.postal_code;
                
                listCountry(data.country.name);
                //listProvince(data.province);
                //listRegency(data.regency);
                //listDistrict(data.district);

                $("input[name=address]").val(data.address);

                var marker;
                var geocoder;

                if(data.address=="") {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 12,
                        center: {lat: {{ default_location('latitude') }}, lng: {{ default_location('longitude') }}}
                    });


                } else {
                    if(data.latitude==0 || data.longitude==0 || data.latitude==null || data.longitude==null) {

                        var address = data.address+' '+data.village.name+' '+data.district.name+' '+data.regency.name+' '+data.province.name+' '+data.country.name;

                        geocoder = new google.maps.Geocoder();
                        geocoder.geocode({
                            'address': address
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var myOptions = {
                                    zoom: 15,
                                    center: results[0].geometry.location,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                }
                                map = new google.maps.Map(document.getElementById("map"), myOptions);

                                marker = new google.maps.Marker({
                                    map: map,
                                    draggable: true,
                                    animation: google.maps.Animation.DROP,
                                    position: results[0].geometry.location
                                  });

                                $("input[name='latitude']").val(results[0].geometry.location.lat());
                                $("input[name='longitude']").val(results[0].geometry.location.lng());

                                google.maps.event.addListener(marker, 'dragend', function(event) 
                                {
                                    $("input[name='latitude']").val(event.latLng.lat());
                                    $("input[name='longitude']").val(event.latLng.lng());
                                });
                            }
                        }); 

                    } else {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 15,
                            center: {lat: data.latitude, lng: data.longitude}
                        });

                        marker = new google.maps.Marker({
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: {lat: data.latitude, lng: data.longitude}
                        });


                        google.maps.event.addListener(marker, 'dragend', function(event) 
                        {
                            $("input[name='latitude']").val(event.latLng.lat());
                            $("input[name='longitude']").val(event.latLng.lng());
                        });
                    }
                }

            } else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ edit(); });
            }
                    
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ edit(); });
        }
    });
}

function resetProvince() {
    $("select[name=province]").html("");
    $("select[name=province]").prop("disabled", true);
    $("select[name=province]").append("<option value=''>{{ trans('messages.select_province') }}</option>");
    $("select[name=province]").select2({
        placeholder: "{{ trans('messages.select_province') }}",
        allowClear: false
    });
}

function resetRegency() {
    $("select[name=regency]").html("");
    $("select[name=regency]").prop("disabled", true);
    $("select[name=regency]").append("<option value=''>{{ trans('messages.select_regency') }}</option>");
    $("select[name=regency]").select2({
        placeholder: "{{ trans('messages.select_regency') }}",
        allowClear: false
    });
}

function resetDistrict() {
    $("select[name=district]").html("");
    $("select[name=district]").prop("disabled", true);
    $("select[name=district]").append("<option value=''>{{ trans('messages.select_district') }}</option>");
    $("select[name=district]").select2({
        placeholder: "{{ trans('messages.select_district') }}",
        allowClear: false
    });
}

function resetVillage() {
    $("select[name=village]").html("");
    $("select[name=village]").prop("disabled", true);
    $("select[name=village]").append("<option value=''>{{ trans('messages.select_village') }}</option>");
    $("select[name=village]").select2({
        placeholder: "{{ trans('messages.select_village') }}",
        allowClear: false
    });
}

function resetZipCode() {
    $("input[name=postal_code]").val("");
}

$('select[name=country]').on('change', function() {
    var country = $(this).find(':selected').data('id');

    if(this.value=="" || this.value.toLowerCase()!='indonesia') {
        resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(country==get_country) {
            listProvince(get_province, country);
        } else {
            resetProvince()
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listProvince('', country);
        }        
    }
});

$('select[name=province]').on('change', function() {
    var province = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_province) {
            listRegency(get_regency, province);
        } else {
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listRegency('', province);
        }
    }
    
});

$('select[name=regency]').on('change', function() {
    var regency = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_regency) {
            listDistrict(get_district, regency);
        } else {
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listDistrict('', regency);
        }
    }
    
});

$('select[name=district]').on('change', function() {
    var district = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_district) {
            listVillage(get_village, district);
        } else {
            resetVillage()
            resetZipCode()
            resetData();
            listVillage('', district);
        }
    }
    
});

$('select[name=village]').on('change', function() {
    var postal_code = $(this).find(':selected').data('postal_code');

    if(this.value=="") {
        resetData();
    } else {
        if(this.value==get_village) {
            $("input[name=postal_code]").val(get_postal_code);
        } else {
            resetData();
            $("input[name=postal_code]").val(postal_code);
        }
    }
});

function searchMap() {
    var address = $("input[name='address']").val()+" "+$("select[name=village] option:selected").val()+" "+$("select[name=regency] option:selected").val()+" "+$("select[name=province] option:selected").val()+" "+$("select[name=country] option:selected").val();

    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': address
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var myOptions = {
                zoom: 15,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: results[0].geometry.location
              });

            $("input[name='latitude']").val(results[0].geometry.location.lat());
            $("input[name='longitude']").val(results[0].geometry.location.lng());

            google.maps.event.addListener(marker, 'dragend', function(event) 
            {
                $("input[name='latitude']").val(event.latLng.lat());
                $("input[name='longitude']").val(event.latLng.lng());
            });
        }
    });

}

$("#searchMap").click(function() {
    searchMap();
});


$("input[name=address]").focus(function() {
    resetValidation('address');
});

$("#province-group .border-group").click(function() {
    resetValidation('province');
});

$("#country-group .border-group").click(function() {
    resetValidation('country');
});

$("#regency-group .border-group").click(function() {
    resetValidation('regency');
});

$("#district-group .border-group").click(function() {
    resetValidation('district');
});

$("#village-group .border-group").click(function() {
    resetValidation('village');
});

$("input[name=postal_code]").focus(function() {
    resetValidation('postal_code');
});

$("form").submit(function(event) {
    event.preventDefault();
    resetValidation('address','country', 'province', 'regency', 'district', 'village', 'postal_code');

    $("form button").attr("disabled", true);

    var id = "{{ $id_clinic }}";
    formData= new FormData();
    formData.append("_method", "PATCH");            
    formData.append("validate", false);
    formData.append("address", $("input[name=address]").val());
    formData.append("postal_code", $("input[name=postal_code]").val());
    formData.append("district", $("select[name=district] option:selected").val());
    formData.append("village", $("select[name='village'] option:selected").val());
    formData.append("regency", $("select[name=regency] option:selected").val());
    formData.append("province", $("select[name=province] option:selected").val());
    formData.append("country", $("select[name=country] option:selected").val());
    formData.append("latitude", $("input[name=latitude]").val());
    formData.append("longitude", $("input[name=longitude]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
    $(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['address',data.errors.address, true], ['country',data.errors.country, true], ['province',data.errors.province, true], ['regency',data.errors.regency, true], ['district',data.errors.district, true], ['village',data.errors.village, true] ['postal_code',data.errors.postal_code, true]);
                } else {
                    notif(true,"{{ trans('validation.success_edit_clinic_location') }}");

                    setTimeout(function(){
                        redirect('redirect/{{ $lang }}/clinic');
                    }, 1000);
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>


<!--
    <div id="map2" style="height:500px;width:500px"></div>
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map2'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDb1go7UC5lIl-TCf3Gubz3E6ium2S1pZs&callback=initMap"
    async defer></script>-->
