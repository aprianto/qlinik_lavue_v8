@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.select_insurance') }}
                    </h3>
                </div>
                <div class="cell hide" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec">
                    <form role="form" class="sec" id="form-search">
                        <div class="form-group hide" id="insurance-group">
                            <div class="border-group">
                                <select id="insurance" name="insurance[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group column-group search">
                            <div class="input-group date">
                                <input type="text" class="form-control" placeholder="Cari asuransi..." name="q" autocomplete="off" />
                                <button id="btn-search" class="btn-icon">
                                    <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                </button>
                            </div>
                        </div>
                        <div class="table-area">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama Asuransi / Penjamin</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="table">
                                </tbody>
                                <tbody id="loading-table" class="hide">
                                    <tr>
                                        <td colspan="5" align="center">
                                            <div class="card">
                                                <span class="three-quarters">Loading&#8230;</span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody id="reload" class="hide">
                                    <tr>
                                        <td colspan="5" align="center">
                                            <div class="card">
                                                <span class="reload fa fa-refresh"></span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group hide">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/clinic" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('clinic.modal.bpjs')

<script type="text/javascript">

$("#insurance").select2({
    placeholder: "{{ trans('messages.select_insurance') }}"
});

// function listInsurance(val) {
//     loading_content("#insurance-group", "loading");
//     $.ajax({
//         url: "{{ $api_url }}/{{ $lang }}/insurance",
//         type: "GET",
//         processData: false,
//         contentType: false,
//         success: function(dataInsurance){
//             

//             if(!dataInsurance.error) {  
//                 loading_content("#insurance-group", "success");              

//                 $("#insurance-group .border-group select").html('');

//                 itemInsurance = dataInsurance.data;

//                 for (var i = 0; i < itemInsurance.length; i++) {
//                     if(val.length>0) {
//                         var selected = "";
//                         for (var j = 0; j < val.length; j++) {
//                             if(itemInsurance[i].id==val[j]) {
//                                 selected = "selected='selected'";
//                             }

//                         }
//                     }

//                     $("#insurance").append("<option value='"+itemInsurance[i].id+"' "+selected+">"+itemInsurance[i].name+"</option>")
//                 }

//                 $("#insurance").select2({
//                     placeholder: "{{ trans('messages.select_insurance') }}",
//                     allowClear: true
//                 });
                

//             } else {
//                 loading_content("#insurance-group", "failed");

//                 $("#insurance-group #loading-content").click(function(){ listInsurance(val); });
//             }
//         },
//         error: function(){
//             loading_content("#insurance-group", "failed");

//             $("#insurance-group #loading-content").click(function(){ listInsurance(val); });
//         }
//     })
// }


function getInsurance() {
    loading_content("#insurance-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsuranceClinic){

            if(!dataInsuranceClinic.error) {
                loading_content("#insurance-group", "success");
                $("#table").empty();
            
                var itemInsuranceClinic = dataInsuranceClinic.data;

                var arrayInsurance = [];
                var tr;
                for (var i = 0; i < itemInsuranceClinic.length; i++) {
                    arrayInsurance.push(itemInsuranceClinic[i].id);
                    tr = $('<tr />');
                    if(itemInsuranceClinic[i].name.toLowerCase().includes("bpjs")){
                        tr.append("<td>"+itemInsuranceClinic[i].name+"</td>");
                        @if(!Session::get('pcare'))
                        tr.append("<td>"+
                            "<div class='material-switch pull-right'>"+
                                "<input id='someSwitchOptionDanger' value='selected' name='someSwitchOption001' type='checkbox'/>"+
                                "<label for='someSwitchOptionDanger' class='label-danger'></label>"+
                            "</div>"+
                            "<button id='connect' class='link-add link-label pull-right' style='margin-top: 0px; padding: 0px 16px; line-height: 21px;'> Hubungkan</button>"+
                        "</td>");
                        @else
                        tr.append("<td>"+
                            "<div class='material-switch pull-right'>"+
                                "<input id='someSwitchOptionDanger' value='selected' name='someSwitchOption001' type='checkbox' checked/>"+
                                "<label for='someSwitchOptionDanger' class='label-danger'></label>"+
                            "</div>"+
                            "<button id='connect' class='link-add link-label pull-right' style='margin-top: 0px; padding: 0px 16px; line-height: 21px;'> Terhubung</button>"+
                        "</td>");
                        @endif
                    }else{
                        tr.append("<td colspan='2'>"+itemInsuranceClinic[i].name+"</td>");
                    }
                    $("#table").append(tr);
                }

                // listInsurance(arrayInsurance);

                $('input[name=someSwitchOption001]').change(function() {  
                    var value = $(this).is(':checked');
                    if(value){
                        $("#pcare").modal('show');
                        $('#connect').html('Terhubung');
                    }else{
                        deletePcareInsurance();
                    }
                });

            } else {
                loading_content("#insurance-group", "failed");

                $("#insurance-group #loading-content").click(function(){ getInsurance(); });
            }    
        },
        error: function(){
            loading_content("#insurance-group", "failed");

            $("#insurance-group #loading-content").click(function(){ getInsurance(); });
        }
    })
}

getInsurance();

$("#insurance-group .border-group").click(function() {
    resetValidation('insurance');
});

$("#form-insurance").submit(function(event) {
    resetValidation('insurance');
    var insurance = $("#insurance").val();
    
    formData= new FormData();
    formData.append("validate", false);
    formData.append("_method", "PATCH");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    if(insurance==null) {
        formData.append("insurance", "");
    } else {
        var selectInsurance = new Array(); 

        $('#insurance :selected').each(function (i, selected) {
            selectInsurance[i] = $(selected).val();
            formData.append("insurance["+i+"]", selectInsurance[i]);
        });
    }

    $("#form-insurance .btn-primary").addClass("loading");
    $("#form-insurance .btn-primary span").removeClass("hide");
    
    var queryParam = ("{{ $level == 'doctor' }}") ? "?id_clinic={{ $id_clinic }}" : "";

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/insurance" + queryParam,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-insurance .btn-primary").removeClass("loading");
            $("#form-insurance .btn-primary span").addClass("hide");

            if(!data.error) {
                notif(true,"{{ trans('validation.success_select_insurance') }}");    
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-insurance .btn-primary").removeClass("loading");
            $("#form-insurance .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>