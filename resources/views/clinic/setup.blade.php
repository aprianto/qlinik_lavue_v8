@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 


<div class="account-user-sec no-color">
	<div class="account-sec">
		@include('layouts.main-header')
		<div class="acount-sec">
			<div class="container">
				<div>
					<div class="container-form-setup">
						<div class="widget">
							<div class="step-form-sec">
								<div class="widget-title">
									<h3>{{ trans('messages.setup_register') }}</h3>
									<span>{{ trans('messages.info_setup_clinic') }}</span>
								</div>
						        <div class="stepsForm">
						            <form id="form-create">
						                <div class="sf-steps">
						                    <div class="sf-steps-content">
						                    	<div class="sf-active" id="tab-clinic">
						                        	<span>1</span>{{ trans('messages.first_step_clinic') }}
                                                </div>
						                        <div id="tab-polyclinic">
						                        	<span>2</span> {{ trans('messages.second_step_clinic') }}
						                        </div>
						                    </div>
						                </div>                
						                <div class="sf-steps-form sf-radius row">					                    
						                    <ul class="sf-content" id="section-clinic">
						                         <li>
						                         	<div class="row">
							                            <div class="col-md-12 field-input-setup">
							                            	<div class="form-group" id="name-group">
                                                                <label class="control-label">{{ trans('messages.clinic_name') }}</label>
								                                <input type="text" name="name" placeholder="{{ trans('messages.input_clinic_name')}}" class="form-control">
								                                <span class="help-block"></span>
							                                </div>
							                            </div>               
						                            </div>
						                         </li>
						                         <li>
						                         	<div class="row">
							                            <div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="email-group">
                                                                <label class="control-label">{{ trans('messages.email') }}</label>
								                                <input type="text" name="email" placeholder="{{ trans('messages.input_email') }}" class="form-control">
								                                <span class="help-block"></span>
							                                </div>
							                            </div>
							                            <div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="phone-group">
                                                                <label class="control-label">{{ trans('messages.phone') }}</label>
								                                <input type="text" name="phone" placeholder="{{ trans('messages.input_phone_number') }}" onkeydown="number(event)" class="form-control">
								                                <span class="help-block"></span>
							                                </div>
							                            </div> 
						                            </div>
						                         </li>
						                         <li>
						                         	<div class="row">
							                            <div class="col-md-12 field-input-setup">
							                            	<div class="form-group" id="address-group">
                                                                <label class="control-label">{{ trans('messages.address') }}</label>
								                                <textarea name="address" class="form-control" placeholder="{{ trans('messages.input_address') }}"></textarea>
								                                <span class="help-block"></span>
								                            </div>
								                        </div>
						                            </div>
						                         </li>
						                          <li>
						                         	<div class="row">
						                         		<div class="col-md-6 field-input-setup">
							                                <div class="form-group" id="country-group">
                                                                <label class="control-label">Negara</label>
									                            <div class="border-group">
									                                <select name="country"></select>
									                            </div>
									                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
									                            <span class="help-block"></span>
									                        </div>
							                            </div>
							                            <div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="province-group">
                                                                <label class="control-label">Profinsi</label>
									                            <div class="border-group">
									                                <select name="province" disabled="disabled">
									                                	<option value=''></option>
									                                </select>
									                            </div>
									                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
									                            <span class="help-block"></span>
									                        </div>
							                            </div>
							                    	</div>
							                    </li>
						                        <li>
						                        	<div class="row">
							                            <div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="regency-group">
                                                            <label class="control-label">Kota/Kabupten</label>
									                            <div class="border-group">
									                                <select name="regency" disabled="disabled">
									                                	<option value=''></option>
									                                </select>
									                            </div>
									                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
									                            <span class="help-block"></span>
									                        </div>
							                            </div>
							                            <div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="district-group">
                                                            <label class="control-label">Kecamatan</label>
									                            <div class="border-group">
									                                <select name="district" disabled="disabled">
									                                	<option value=''></option>
									                                </select>
									                            </div>
									                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
									                            <span class="help-block"></span>
									                        </div>
							                            </div>
						                            </div>
						                         </li>
						                         <li>
						                        	<div class="row">
						                        		<div class="col-md-6 field-input-setup">
							                            	<div class="form-group" id="village-group">
                                                            <label class="control-label">Desa/Kelurahan</label>
									                            <div class="border-group">
									                                <select name="village" disabled="disabled">
									                                	<option value=''></option>
									                                </select>
									                            </div>
									                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
									                            <span class="help-block"></span>
									                        </div>
							                            </div>
							                            <div class="col-md-6 field-input-setup">
                                                        <label class="control-label">{{ trans('messages.postal_code') }}</label>
							                            	<div class="form-group" id="postal_code-group">
								                                <input type="text" name="postal_code" placeholder="Masukkan kode pos" class="form-control">
								                                <span class="help-block"></span>
							                                </div>
							                            </div>
						                            </div>
						                         </li>						                         
						                    </ul>

						                    <ul class="sf-content hide" id="section-polyclinic">
						                         <li>
						                         	<div class="row">
						                            	<div class="col-md-12">
						                            		<div class="cell" id="loading-page">
										                        <div class="card">
										                            <span class="three-quarters">Loading&#8230;</span>
										                        </div>
										                    </div>
										                    <div class="cell hide" id="reload">
										                        <div class="card">
										                            <span class="reload fa fa-refresh"></span>
										                        </div>
										                    </div>
							                            	<div class="form-group hide" id="polyclinic-group">            
									                            <div class="border-group"></div>
									                            <span class="help-block"></span>
									                            <button type="button" id="icon-add" class="button-add hide" data-toggle="modal" data-target="#add">
										                        	<span class="fa fa-plus"></span> 
										                        	{{ trans('messages.add_other_polyclinic') }}
										                        </button>
									                        </div>
									                    </div>
									                </div>
						                         </li>						                         
						                    </ul>
						                </div>
						                
						                <div class="sf-steps-navigation sf-align-right" id="button-clinic">
						                    <button type="submit" id="btn-next" class="sf-button button-login">
						                    	<span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                    	{{ trans('messages.next') }}
						                    </button>
						                </div>
						                <div class="sf-steps-navigation sf-align-right hide" id="button-polyclinic">
                                            <button id="btn-prev" type="button" class="sf-button-cancel">{{ trans('messages.back') }}</button>
                                            <button type="submit" id="btn-create" class="sf-button">
                                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                                {{ trans('messages.last_step_clinic') }}
                                            </button>
						                </div>
						            </form>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.footer')
	</div> 
</div>

<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <i class="fa fa-plus"></i>
                    {{ trans('messages.add_polyclinic') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add">
                <div class="modal-body modal-main">               
                    <div class="form-group" id="name-group">
                        <input type="text" class="form-control" placeholder="{{ trans('messages.polyclinic_name') }}" name="name"  autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

$("select[name='country']").select2({
    placeholder: "{{ trans('messages.select_country') }}",
    allowClear: true
});

$("select[name='province']").select2({
    placeholder: "{{ trans('messages.select_province') }}",
    allowClear: true
});

$("select[name='regency']").select2({
    placeholder: "{{ trans('messages.select_regency') }}",
    allowClear: true
});

$("select[name='district']").select2({
    placeholder: "{{ trans('messages.select_district') }}",
    allowClear: true
});

$("select[name='village']").select2({
    placeholder: "{{ trans('messages.select_village') }}",
    allowClear: true
});

$("#btn-prev").click(function(event) {
	$("#tab-clinic").addClass("sf-active");
    $("#tab-polyclinic").removeClass("sf-active");

    $("#section-clinic").removeClass("hide");
    $("#section-polyclinic").addClass("hide");

	$("#button-clinic").removeClass("hide");
    $("#button-polyclinic").addClass("hide");

    resetValidation('form-create #polyclinic');
});







function listPoly(val) {
	$("#loading-page").removeClass("hide");
	$("#polyclinic-group").addClass("hide");
	$("#reload").addClass("hide");
	$.ajax({
	    url: "{{ $api_url }}/{{ $lang }}/polyclinic",
	    type: "GET",
	    processData: false,
	    contentType: false,
	    success: function(dataPoly){

	        $("#loading-page").addClass("hide");
	    	$("#polyclinic-group").removeClass("hide");

	    	if(!dataPoly.error) {

	    		var selectPoly = new Array(); 

	            $('#polyclinic :selected').each(function (i, selected) {
	                selectPoly[i] = $(selected).val();
	            });

		        $("#polyclinic-group .border-group").html("");

		        $("#polyclinic-group .border-group").append('<select id="polyclinic" name="polyclinic[]" multiple="multiple" class="poly-select">');

		        var itemPoly = dataPoly.data;

		        for (var i = 0; i < itemPoly.length; i++) {
                	var selected = '';

                	for (var j = 0; j < selectPoly.length; j++) {
                		if(itemPoly[i].id==selectPoly[j]) {
                			selected = "selected='selected'";
                		}
                	}

                	if(val!="") {
	                	if(itemPoly[i].id==val) {
	                		selected = "selected='selected'";
	                	}
                	}

                    $("#polyclinic").append("<option value='"+itemPoly[i].id+"' "+selected+">"+itemPoly[i].name+"</option>");
                }

		        $("#polyclinic-group .border-group").append('</select>');

		        $("#polyclinic").select2({
		            placeholder: "{{ trans('messages.select_polyclinic') }}",
		            allowClear: true
		        });

	    	} else {
	    		$("#reload").removeClass("hide");
		    	$("#reload .reload").click(function(){ listPoly(val); });
	    	}	   
	    },
		error: function(){
			$("#loading-page").addClass("hide");
	    	$("#polyclinic-group").removeClass("hide");
	    	$("#reload").removeClass("hide");

	    	$("#reload .reload").click(function(){ listPoly(val); });
		}
	})
}



$('#add').on('show.bs.modal', function (e) {
    $("#form-add input[name=name]").val("");

    resetValidation('form-add #name');
	resetValidation('form-create #polyclinic');
});

$("#form-add input[name=name]").focus(function() {
		resetValidation('form-add #name');
});

$("#form-add").submit(function(event) {
	event.preventDefault();
	resetValidation('form-add #name');
    $("#form-add button").attr("disabled", true);

	formData= new FormData();

    formData.append("validate", false);
    formData.append("indonesian_name", $("#form-add input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass('loading');
	$("#form-add .btn-primary span").removeClass('hide');

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/polyclinic",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass('loading');
			$("#form-add .btn-primary span").addClass('hide');

			if(!data.error) {
                if(!data.success) {                    
                	formValidate(true, ['form-add #name',data.errors.indonesian_name, true]);
                } else {

                    $("#form-add input[name=name]").val("");
                    
                    notif(true,"{{ trans('validation.success_add_polyclinic') }}");

                    $("#loading-page").removeClass("hide");
					$("#polyclinic-group").addClass("hide");

					listPoly(data.id);

            		$("#add").modal("toggle");
                }
            } else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
		error: function(){
			$("#form-add button").attr("disabled", false);

			$("#form-add .btn-primary").removeClass('loading');
			$("#form-add .btn-primary span").addClass('hide');

			notif(false,"{{ trans('validation.failed') }}");
		}
    })	
});



$("#form-create input[name=name]").focus(function() {
	resetValidation('form-create #name');
});

$("#form-create input[name=email]").focus(function() {
	resetValidation('form-create #email');
});

$("#form-create input[name=phone]").focus(function() {
	resetValidation('form-create #phone');
});

$("#form-create textarea[name=address]").focus(function() {
	resetValidation('form-create #address');
});

$("#form-create #country-group .border-group").click(function() {
	resetValidation('form-create #country');
});

$("#form-create #province-group .border-group").click(function() {
	resetValidation('form-create #province');
});

$("#form-create #regency-group .border-group").click(function() {
	resetValidation('form-create #regency');
});

$("#form-create #district-group .border-group").click(function() {
	resetValidation('form-create #district');
});

$("#form-create #village-group .border-group").click(function() {
	resetValidation('form-create #village');
});

$("#form-create input[name=postal_code]").focus(function() {
	resetValidation('form-create #postal_code');
});

$("#polyclinic-group .border-group").click(function() {
	resetValidation('form-create #polyclinic');
});

function listCountry() {
    loading_content("#form-create #country-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/country",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-create #country-group", "success");

                var item = data.data;

                $("#form-create select[name='country']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-create select[name='country']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].name+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-create select[name='country']").select2({
                    placeholder: "-- {{ trans('messages.select_country') }} --",
                    allowClear: true
                });

                $("#form-create select[name='country']").val("Indonesia").trigger("change");

                
            } else {
                loading_content("#form-create #country-group", "failed");

                $("#form-create #country-group #loading-content").click(function(){ listCountry(); });
            }
        },
        error: function(){
            loading_content("#form-create #country-group", "failed");

            $("#form-create #country-group #loading-content").click(function(){ listCountry(); });
        }
    })
}

function listProvince(id_country) {
    loading_content("#form-create #province-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/province",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-create #province-group", "success");

                var item = data.data;

                $("#form-create select[name='province']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-create select[name='province']").append("<option value='"+checkLocationCase(toTitleCase(item[i].name))+"' data-id='"+item[i].id+"'>"+checkLocationCase(toTitleCase(item[i].name))+"</option>");
                }

                $("#form-create select[name='province']").select2({
                    placeholder: "-- {{ trans('messages.select_province') }} --",
                    allowClear: true
                });

                
            } else {
                loading_content("#form-create #province-group", "failed");

                $("#form-create #province-group #loading-content").click(function(){ listProvince(id_country); });
            }
        },
        error: function(){
            loading_content("#form-create #province-group", "failed");

            $("#form-create #province-group #loading-content").click(function(){ listProvince(id_country); });
        }
    })
}

function listRegency(id_province) {
    loading_content("#form-create #regency-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/regency?id_province="+id_province,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-create #regency-group", "success");

                var item = data.data;

                $("#form-create select[name='regency']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-create select[name='regency']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-create select[name='regency']").select2({
                    placeholder: "-- {{ trans('messages.select_regency') }} --",
                    allowClear: true
                });

                
            } else {
                loading_content("#form-create #regency-group", "failed");

                $("#form-create #regency-group #loading-content").click(function(){ listRegency(id_province); });
            }
        },
        error: function(){
            loading_content("#form-create #regency-group", "failed");

            $("#form-create #regency-group #loading-content").click(function(){ listRegency(id_province); });
        }
    })
}

function listDistrict(id_regency) {
    loading_content("#form-create #district-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/district?id_regency="+id_regency,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-create #district-group", "success");

                var item = data.data;

                $("#form-create select[name='district']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-create select[name='district']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-create select[name='district']").select2({
                    placeholder: "-- {{ trans('messages.select_district') }} --",
                    allowClear: true
                });
                
            } else {
                loading_content("#form-create #district-group", "failed");

                $("#form-create #district-group #loading-content").click(function(){ listDistrict(id_regency); });
            }
        },
        error: function(){
            loading_content("#form-create #district-group", "failed");

            $("#form-create #district-group #loading-content").click(function(){ listDistrict(id_regency); });
        }
    })
}

function listVillage(id_district) {
    loading_content("#form-create #village-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/village?id_district="+id_district,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-create #village-group", "success");

                var item = data.data;

                $("#form-create select[name='village']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-create select[name='village']").append("<option value='"+toTitleCase(item[i].name)+"' data-postal_code='"+item[i].postal_code+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-create select[name='village']").select2({
                    placeholder: "-- {{ trans('messages.select_village') }} --",
                    allowClear: true
                });
                
            } else {
                loading_content("#form-create #village-group", "failed");

                $("#form-create #village-group #loading-content").click(function(){ listVillage(id_district); });
            }
        },
        error: function(){
            loading_content("#form-create #village-group", "failed");

            $("#form-create #village-group #loading-content").click(function(){ listVillage(id_district); });
        }
    })
}

function resetProvince() {
    $("#form-create select[name=province]").html("");
    $("#form-create select[name=province]").prop("disabled", true);
    $("#form-create select[name=province]").append("<option value=''>-- {{ trans('messages.select_province') }} --</option>");
    $("#form-create select[name=province]").select2({
        placeholder: "-- {{ trans('messages.select_province') }} --",
        allowClear: true
    });
}

function resetRegency() {
    $("#form-create select[name=regency]").html("");
    $("#form-create select[name=regency]").prop("disabled", true);
    $("#form-create select[name=regency]").append("<option value=''>-- {{ trans('messages.select_regency') }} --</option>");
    $("#form-create select[name=regency]").select2({
        placeholder: "-- {{ trans('messages.select_regency') }} --",
        allowClear: true
    });
}

function resetDistrict() {
    $("#form-create select[name=district]").html("");
    $("#form-create select[name=district]").prop("disabled", true);
    $("#form-create select[name=district]").append("<option value=''>-- {{ trans('messages.select_district') }} --</option>");
    $("#form-create select[name=district]").select2({
        placeholder: "-- {{ trans('messages.select_district') }} --",
        allowClear: true
    });
}

function resetVillage() {
    $("#form-create select[name=village]").html("");
    $("#form-create select[name=village]").prop("disabled", true);
    $("#form-create select[name=village]").append("<option value=''>-- {{ trans('messages.select_village') }} --</option>");
    $("#form-create select[name=village]").select2({
        placeholder: "-- {{ trans('messages.select_village') }} --",
        allowClear: true
    });
}

function resetZipCode() {
    $("#form-create input[name=postal_code]").val("");
}

$('#form-create select[name=country]').on('change', function() {
    var country = $(this).find(':selected').data('id');

    if(this.value=="" || this.value.toLowerCase()!='indonesia') {
        resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
    } else {
    	resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        listProvince(country);
    }
});

$('#form-create select[name=province]').on('change', function() {
    var province = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
    } else {
    	resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        listRegency(province);
    }
});

$('#form-create select[name=regency]').on('change', function() {
    var regency = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetDistrict()
        resetVillage()
        resetZipCode()
    } else {
    	resetDistrict()
        resetVillage()
        resetZipCode()
        listDistrict(regency);
    }
});

$('#form-create select[name=district]').on('change', function() {
    var district = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetVillage()
        resetZipCode()
    } else {
        resetVillage()
        resetZipCode()
        listVillage(district);
    }
});

$('#form-create select[name=village]').on('change', function() {
    var postal_code = $(this).find(':selected').data('postal_code');

    if(this.value=="") {
        resetZipCode()
    } else {
        $("#form-create input[name=postal_code]").val(postal_code);
    }
});

listCountry();
//listProvince();
//listRegency();
//listDistrict();

let erorr = false
function input() {
    if(! validationScript( $("#form-create input[name=name]").val(), $("#form-create input[name=email]").val(), 
    $("#form-create textarea[name=address]").val(), $("#form-create input[name=postal_code]").val())) {
        error = false
        formData.append("name", $("#form-create input[name=name]").val());
        formData.append("email", $("#form-create input[name=email]").val());
        formData.append("phone", $("#form-create input[name=phone]").val());
        formData.append("address", $("#form-create textarea[name=address]").val());
        formData.append("country", $("#form-create select[name=country] option:selected").val());
        formData.append("province", $("#form-create select[name=province] option:selected").val());
        formData.append("regency", $("#form-create select[name=regency] option:selected").val());
        formData.append("district", $("#form-create select[name=district] option:selected").val());
        formData.append("village", $("#form-create select[name=village] option:selected").val());
        formData.append("postal_code", $("#form-create input[name=postal_code]").val());
        formData.append("id_user", "{{ $id_user }}");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");
    }else {
        error = true
    }
}

$("#form-create").submit(function(event) {
	if($("#button-polyclinic").css("display")=="none") {
		event.preventDefault();
		resetValidation('form-create #name', 'form-create #email', 'form-create #phone', 'form-create #address', 'form-create #country', 'form-create #province', 'form-create #regency', 'form-create #district', 'form-create #village', 'form-create #postal_code');
		
        
		formData= new FormData();
        formData.append("validate", true);
        input();
        
        if(!error) {
            $("#form-create button").attr("disabled", true);
            $("#btn-next").addClass("loading");
            $("#btn-next span").removeClass("hide");
            $.ajax({
                url: "{{ $api_url }}/{{ $lang }}/clinic",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    
                    $("#form-create button").attr("disabled", false);
            
                    $("#btn-next").removeClass("loading");
                    $("#btn-next span").addClass("hide");
    
                    if(!data.error) {
                        if(!data.success) {
                            formValidate(true, ['form-create #name',data.errors.name, false], ['form-create #email',data.errors.email, false], ['form-create #phone',data.errors.phone, false], ['form-create #address',data.errors.address, false], ['form-create #country',data.errors.country, false], ['form-create #province',data.errors.province, false], ['form-create #regency',data.errors.regency, false], ['form-create #district',data.errors.district, false], ['form-create #village',data.errors.village, false], ['form-create #postal_code',data.errors.postal_code, false]);
                        } else {
                            $("#tab-clinic").removeClass("sf-active");
                            $("#tab-polyclinic").addClass("sf-active");
    
                            $("#section-clinic").addClass("hide");
                            $("#section-polyclinic").removeClass("hide");
    
                            $("#button-clinic").addClass("hide");
                            $("#button-polyclinic").removeClass("hide");
    
    
                            if($("#polyclinic-group .border-group").html()=="") {
                                listPoly('');
                            }
                        }
                    } else {
                        notif(false,"{{ trans('validation.failed') }}");
                    }
                },
                error: function(){
                    $("#form-create button").attr("disabled", false);
    
                    $("#btn-next").removeClass("loading");
                    $("#btn-next span").addClass("hide");
    
                    notif(false,"{{ trans('validation.failed') }}");
                }
            })
        }
	} else {
		resetValidation('form-create #polyclinic');
    	var polyclinic = $("#polyclinic").val();
        if(polyclinic==null) {
            formValidate(true, ['form-create #polyclinic','{{ trans("validation.min_polyclinic") }}', true]);
        } else {
        	event.preventDefault();
            
            formData= new FormData();
            formData.append("validate", false);
            input();
            
            var selectPoly = new Array(); 
            
            $('#polyclinic :selected').each(function (i, selected) {
                selectPoly[i] = $(selected).val();
                formData.append("polyclinic["+i+"]", selectPoly[i]);
            });
            
            if(!error) {
                $("#form-create button").attr("disabled", true);
                $("#btn-create").addClass("loading");
                $("#btn-create span").removeClass("hide");
                $.ajax({
                    url: "{{ $api_url }}/{{ $lang }}/clinic",
                    type: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        
    
                        $("#form-create button").attr("disabled", false);
                
                        $("#btn-create").removeClass("loading");
                        $("#btn-create span").addClass("hide");
    
                        if(!data.error) {
    
                            if(!data.success) {
                                notif(false,"{{ trans('validation.failed') }}");
                            } else {
                                notif(true,"{{ trans('validation.success_add_clinic') }}");
    
                                setTimeout(function(){
                                    redirect('');
                                }, 1000);
                            }
                            
                        } else {
                            notif(false,"{{ trans('validation.failed') }}");
                        }
                    },
                    error: function(){
                        $("#form-create button").attr("disabled", false);
    
                        $("#btn-create").removeClass("loading");
                        $("#btn-create span").addClass("hide");
    
                        notif(false,"{{ trans('validation.failed') }}");
                    }
                })
            }
        }
    }
    
});

</script>