@include('layouts.lib')

<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{{ trans('messages.app_name') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{ $storage }}/images/icons/favicon.png" />
    <link rel="shortcut icon" href="{{ $storage }}/images/icons/favicon.png" />

	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('assets/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('assets/css/icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

</head>
<body>

<script src="{{ asset('assets/js/jquery-2.1.3.js') }}"></script>

<div class="maintainance">
	<div class="account-top-bar account-top-bar-maintenance">
		<div class="container">
			<div class="logo">
		        <a href="#/" title="" class="text" onclick="loading($(this).attr('href'))">
	        		<img src="{{ $storage }}/images/icons/logo.png" />
	        	<!--<span>Lix</span>Medic.-->
	        	</a>
		    </div>
		    <ul class="account-header-link">
		    	<li>
			    	<div class="quick-links">
						<ul>
							<li>
								<a title="" class="">
									@if($lang=="id")
									<img src="{{ asset('assets/images/flags/id.png') }}" class="icon-flag" /><span class="text">ID</span> 
									@else
									<img src="{{ asset('assets/images/flags/en.png') }}" class="icon-flag" /><span class="text">EN</span>
									@endif
									<i class="fa fa-chevron-down"></i>
								</a>
								<div class="dialouge" style="display: none;">
									<a class="menu" href="{{ url('/') }}/language/id"><img src="{{ asset('assets/images/flags/id.png') }}" class="icon-link flag" /> Indonesia</a>
									<a class="menu" href="{{ url('/') }}/language/en"><img src="{{ asset('assets/images/flags/en.png') }}" class="icon-link flag" /> English</a>
								</div>
							</li>
						</ul>
		            </div>
	            </li>
		    	<li><a href="{{ url('/') }}/logout">{{ trans('messages.logout') }}</a></li>
		    </ul>
		</div>
	</div>

	<div class="container">
		<div class="maintainance-mode">
			<div class="logo">
		        <a href="#/" title="" class="text" onclick="loading($(this).attr('href'))"><img src="{{ $storage }}/images/icons/logo.png" /></a>
		    </div>
		    <h2>{{ trans("messages.account_suspended") }}</h2>
		    <span>{{ trans("messages.info_account_suspended") }}</span>
			<div class="fancy-social">
				<span>{{ $clinic->name }}</span>
				<img src="<?php if($clinic->logo!=null) { echo $storage.'/images/clinic/logo/'.$clinic->logo; } else { echo $storage.'/images/clinic/logo/logo.png'; } ?>" alt="" />
			</div>
			<div class="btn-sec">
				<a href="{{ url('/') }}/logout" class="button">{{ trans("messages.logout") }}</a>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	
$('.quick-links > ul > li > a').on("click", function(){
	$(this).parent().siblings().find('.dialouge').fadeOut();
	$(this).next('.dialouge').fadeIn();
	return false;
});

$("html").on("click", function(){
	$(".dialouge").fadeOut();
});

</script>

</body>
</html>