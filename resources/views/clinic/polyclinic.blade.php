@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.select_polyclinic') }}
                    </h3>
                </div>
                <div class="cell hide" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-polyclinic">
                        <div class="form-group" id="polyclinic-group">
                            <div class="border-group">
                                <select id="polyclinic" name="polyclinic[]" multiple="multiple"></select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>                            
                        </div>
                        <div class="form-group last-item">
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="button" id="icon-add" class="button-add hide" data-toggle="modal" data-target="#add">+ {{ trans('messages.add_polyclinic') }}</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-primary">
                                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                        {{ trans('messages.save') }}
                                    </button>
                                    <a href="#/{{ $lang }}/clinic" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                        {{ trans('messages.cancel') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-poli.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_new_polyclinic') }}</h5>
            </div>
            <form id="form-add">
                <div class="modal-body modal-main alert-notif">               
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.polyclinic_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_polyclinic_name') }}" name="name"  autocomplete="off">
                        <span class="help-block"></span>
                    </div>                
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

$("#polyclinic").select2({
    placeholder: "{{ trans('messages.select_polyclinic') }}"
});

function listPoly(val) {
    loading_content("#polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){

            if(!dataPoly.error) {  
                loading_content("#polyclinic-group", "success");              

                $("#polyclinic-group .border-group select").html('');

                itemPoly = dataPoly.data;

                for (var i = 0; i < itemPoly.length; i++) {
                    if(val.length>0) {
                        var selected = "";
                        for (var j = 0; j < val.length; j++) {
                            if(itemPoly[i].id==val[j]) {
                                selected = "selected='selected'";
                            }

                        }
                    }

                    $("#polyclinic").append("<option value='"+itemPoly[i].id+"' "+selected+">"+itemPoly[i].name+"</option>")
                }

                $("#polyclinic").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: true
                });
                

            } else {
                loading_content("#polyclinic-group", "failed");

                $("#polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#polyclinic-group", "failed");

            $("#polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}


function getPoly() {
    loading_content("#polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPolyClinic){

            if(!dataPolyClinic.error) {
                loading_content("#polyclinic-group", "success");
            
                var itemPolyClinic = dataPolyClinic.data;

                var arrayPoly = [];
                for (var i = 0; i < itemPolyClinic.length; i++) {
                    arrayPoly.push(itemPolyClinic[i].id);
                }

                listPoly(arrayPoly);

            } else {
                loading_content("#polyclinic-group", "failed");

                $("#polyclinic-group #loading-content").click(function(){ getPoly(); });
            }    
        },
        error: function(){
            loading_content("#polyclinic-group", "failed");

            $("#polyclinic-group #loading-content").click(function(){ getPoly(); });
        }
    })
}

getPoly();


$("#form-add input[name=name]").focus(function() {
    resetValidation('form-add #name');
});

$('#add').on('show.bs.modal', function (e) {
    $("#form-add input[name=name]").val("");

    resetValidation('form-add #name');
    resetValidation('polyclinic');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #name');
    
    $("#form-add button").attr("disabled", true);

    formData= new FormData();
    formData.append("validate", false);
    formData.append("indonesian_name", $("#form-add input[name=name]").val());
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $("#form-add .btn-primary").addClass('loading');
    $("#form-add .btn-primary span").removeClass('hide');
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/polyclinic",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass('loading');
            $("#form-add .btn-primary span").addClass('hide');

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.indonesian_name, true]);
                } else {

                    $("#form-add input[name=name]").val("");
                    
                    notif(true,"{{ trans('validation.success_add_polyclinic') }}");

                    var selectPoly = []; 
                    $('#polyclinic :selected').each(function (i, selected) {
                        selectPoly.push($(selected).val());
                    });
                    selectPoly.push(data.id);

                    listPoly(selectPoly);

                    $("#add").modal("toggle");         
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            
            $("#form-add .btn-primary").addClass('loading');
            $("#form-add .btn-primary span").removeClass('hide');

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#polyclinic-group .border-group").click(function() {
    resetValidation('polyclinic');
});

$("#form-polyclinic").submit(function(event) {
    resetValidation('polyclinic');
    var polyclinic = $("#polyclinic").val();
    if(polyclinic==null) {
        formValidate(true, ['polyclinic','{{ trans("validation.min_polyclinic") }}', true]);
    } else {
        formData= new FormData();
        formData.append("validate", false);
        formData.append("_method", "PATCH");
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        var selectPoly = new Array(); 

        $('#polyclinic :selected').each(function (i, selected) {
            selectPoly[i] = $(selected).val();
            formData.append("polyclinic["+i+"]", selectPoly[i]);
        });

        $("#form-polyclinic .btn-primary").addClass("loading");
        $("#form-polyclinic .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                

                $("#form-polyclinic .btn-primary").removeClass("loading");
                $("#form-polyclinic .btn-primary span").addClass("hide");

                if(!data.error) {
                    notif(true,"{{ trans('validation.success_select_polyclinic') }}");    
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("#form-polyclinic .btn-primary").removeClass("loading");
                $("#form-polyclinic .btn-primary span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        })
    }
});

</script>