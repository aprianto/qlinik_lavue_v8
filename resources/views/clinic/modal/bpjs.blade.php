<div class="modal fade window" id="pcare" role="dialog" aria-labelledby="addTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat-masuk.png') }}" />
                <h5 class="modal-title" id="title-pcare">Hubungkan Aplikasi PCARE</h5>
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </div>
            <form id="form-pcare">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="username-group">
                        <label class="control-label">Username Pcare <span>*</span></label>
                        <input type="text" name="username" class="form-control" placeholder="Masukkan username aplikasi pcare" />
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="password-group">
                        <label class="control-label">Password Pcare <span>*</span></label>
                        <input type="password" name="password" class="form-control" placeholder="Masukkan password" />
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="consumer-group">
                        <label class="control-label">Consumer ID (Cons ID) <span>*</span></label>
                        <input type="text" name="consumer" class="form-control" placeholder="Masukkan consumer id" />
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="secret-group">
                        <label class="control-label">Secret Key <span>*</span></label>
                        <input type="text" name="secret" class="form-control" placeholder="Masukkan secret key" />
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group" id="code_app-group">
                                <label class="control-label">Kode Aplikasi (App) <span>*</span></label>
                                <input type="text" name="code_app" class="form-control" placeholder="Masukkan kode app" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" id="code_ppk-group">
                                <label class="control-label">Kode PPK (Provider) <span>*</span></label>
                                <input type="text" name="code_ppk" class="form-control" placeholder="Masukkan kode ppk" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" onclick="clickCance()" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

function deletePcareInsurance() {
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/{{ $id_user }}/cliniccredential/delete",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataInsuranceClinic){

            if(!dataInsuranceClinic.error) {
                $.ajax({
                    url: '{{ url("/") }}/session?session=pcare&value=false',
                    type: "GET",
                    processData: false,
                    contentType: false,
                    success: function(dataInsuranceClinic){
                        $('#someSwitchOptionDanger').attr('checked',false);
                        $('#connect').html('Hubungkan');
                        location.reload();
                    },
                    error: function(){}
                })
                notif(true, dataInsuranceClinic.data);
            } else {
                $('#someSwitchOptionDanger').attr('checked',true);
                $('#connect').html('Terhubung');
            }
        },
        error: function(){
        }
    })
}

function clickCance(){
    $('#someSwitchOptionDanger').attr('checked',false);
    $('#connect').html('Hubungkan');
}

$("#form-pcare").submit(function(event) {
    resetValidation('insurance');
    var insurance = $("#insurance").val();
    
    formData= new FormData();
    formData.append("username", $("#form-pcare input[name='username']").val());
    formData.append("password", $("#form-pcare input[name='password']").val());
    formData.append("cons_id", $("#form-pcare input[name='consumer']").val());
    formData.append("secret_key", $("#form-pcare input[name='secret']").val());
    formData.append("app_code", $("#form-pcare input[name='code_app']").val());
    formData.append("ppk_code", $("#form-pcare input[name='code_ppk']").val());

    $("#form-pcare .btn-primary").addClass("loading");
    $("#form-pcare .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/user/{{ $id_user }}/cliniccredential",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-pcare .btn-primary").removeClass("loading");
            $("#form-pcare .btn-primary span").addClass("hide");
            if(!data.error) {
                if(!data.success){
                    notif(false, data.message);    
                    $('#someSwitchOptionDanger').attr('checked',false);
                    $('#connect').html('Hubungkan');
                    formValidate(true, ['form-pcare #username',data.errors.username, true], ['form-pcare #password',data.errors.password, true], ['form-pcare #consumer',data.errors.cons_id, true], ['form-pcare #secret',data.errors.secret_key, true], ['form-pcare #code_app',data.errors.app_code, true], ['form-pcare #code_ppk',data.errors.ppk_code, true]);
                }else{
                    $.ajax({
                        url: '{{ url("/") }}/session?session=pcare&value=true',
                        type: "GET",
                        processData: false,
                        contentType: false,
                        success: function(dataInsuranceClinic){
                            $('#someSwitchOptionDanger').attr('checked',true);
                            $('#connect').html('Terhubung');
                            location.reload();
                            // $('#pcare').modal('hide');
                            // $('.modal-backdrop').remove();
                            notif(true,"{{ trans('validation.success_select_insurance') }}");    
                        },
                        error: function(){
                            $('#someSwitchOptionDanger').attr('checked',false);
                            $('#connect').html('Hubungkan');
                        }
                    })
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(error){
            $("#form-pcare .btn-primary").removeClass("loading");
            $("#form-pcare .btn-primary span").addClass("hide");
            
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>