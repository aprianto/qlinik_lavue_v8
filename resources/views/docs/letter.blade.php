<html>
<head>
<style type="text/css">
 
html, body {
    margin: 0;
    padding: 0;
    -webkit-print-color-adjust: exact;
}

h4 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    /* font-size: 13px; */
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    padding: 12px 24px 0;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 50%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: 'Open Sans', sans-serif;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #000;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: 'Open Sans', sans-serif;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report  {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report  h1 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report  h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 11px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    
}

#print-report .body-report  p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    
}

#print-report .body-report .table {
    display: table;
    width: 100%;
}

#print-report .body-report .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 50%;
}

#print-report .body-report .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 50%;
}


#print-report .body-report section {
    padding: 29px 29px 19px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border: 2px solid #000;
}
#print-report .body-report section:last-child{
    padding: 19px 29px;
    border-top: none;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #000;
}

#print-report .body-report table tr td h2 {
    margin: 24px 0 0 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 11px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    
}

#print-report .body-report table tr td h2.first-item {
    margin: 0 0;
}

#print-report .body-report table tr td b {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #000;
}

#print-report .body-report table tr td span {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 11px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    
}

#print-report .body-report table tr td a {
    text-decoration: none;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #1ab1e5;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #000;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 11px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.box{
    border: 1px solid;
    width: 25px;
    height: 20px;
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
}
.page_break { page-break-before: always; }

</style>
</head>
<body>
<!-- @include('function.function') -->
<div  id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <div class="powered">
                            <img class="footer-logo" src="https://bpjs-kesehatan.go.id/bpjs//themes/webbpjs/img/logo/logo-bpjs.png" />
                        </div>
                    </div>
                    <div class="right">
                        <table>
                            <tr>
                                <td><h4 style="font-size: 13px;"><b>Kedeputian Wilayah</b></h4></td>
                                <td>&nbsp;</td>
                                <td><h4 style="font-size: 13px;">{{ empty($data->data->ppk->kc->kdKR->nmKR) ? '' : $data->data->ppk->kc->kdKR->nmKR }}</h4></td>
                            </tr>
                            <tr>
                                <td><h4 style="font-size: 13px;"><b>Kantor Cabang</b></h4></td>
                                <td>&nbsp;</td>
                                <td><h4 style="font-size: 13px;">{{ empty($data->data->ppk->kc->nmKC) ? '' : $data->data->ppk->kc->nmKC }}</h4></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <h4 style="text-align: center; margin: 0px 0px 15px;"><b>Surat Rujukan FKTP</b></h4>
            <section>
                <div class="content" style="border: 2px solid;margin: -10px -10px 10px;padding: 10px;">
                    <div class="table">
                        <div class="row">
                            <div class="left" style="width: 100%;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 20%;">No. Rujukan</td>
                                        <td style="width: 45%;">&nbsp;:&nbsp; {{ empty($data->data->noRujukan) ? '' : $data->data->noRujukan }}</td>
                                        <td rowspan="3" style="vertical-align: middle; width: 35%;">
                                            <img src="data:image/png;base64,{{ empty(DNS1D::getBarcodePNG($data->data->noRujukan, 'C39E' )) ? '#' : DNS1D::getBarcodePNG($data->data->noRujukan, 'C39E' ) }}" style="max-width: 230px; height: 50px;" alt="barcode" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;"><b>Kantor Cabang</b></td>
                                        <td style="width: 45%;">&nbsp;:&nbsp; {{ empty($data->data->ppk->nmPPK) ? '' : $data->data->ppk->nmPPK }}({{ empty($data->data->ppk->kdPPK) ? '' : $data->data->ppk->kdPPK }})</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%;"><b>Kabupaten / Kota</b></td>
                                        <td style="width: 45%;">&nbsp;:&nbsp; {{ empty($data->data->ppk->kc->nmKC) ? '' : $data->data->ppk->kc->nmKC }}({{ empty($data->data->ppk->kc->kdKC) ? '' : $data->data->ppk->kc->kdKC  }})</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content" style="margin-bottom: 10px;">
                    <div class="table">
                        <div class="row">
                            <div class="left">
                                <table>
                                    <tr>
                                        <td>Kepada Yth. TS Dokter</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Di</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>{{ empty($data->data->ppkRujuk->nmPPK) ? '' : $data->data->ppkRujuk->nmPPK }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <p style="margin-bottom: 10px;">Mohon pemeriksaan dan penangan lebih lanjut pasien :</p>
                <div class="content">
                    <div class="table">
                        <div class="row">
                            <div class="left">
                                <table>
                                    <tr>
                                        <td>Nama</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>{{ empty($data->data->nmPst) ? '' : $data->data->nmPst }}</td>
                                    </tr>
                                    <tr>
                                        <td>No. Kartu BPJS</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>{{ empty($data->data->nokaPst) ? '' : $data->data->nokaPst }}</td>
                                    </tr>
                                    <tr>
                                        <td>Diagnosa</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>
                                            {{ empty($data->data->diag1->nmDiag) ? '' : $data->data->diag1->nmDiag }} ({{ empty($data->data->diag1->kdDiag) ? '' : $data->data->diag1->kdDiag }})<br />
                                            @if($data->data->diag2 != null || $data->data->diag2 != 'null')
                                                {{ empty($data->data->diag2->nmDiag) ? '' : $data->data->diag2->nmDiag }} ({{ empty($data->data->diag2->kdDiag) ? '' : $data->data->diag2->kdDiag }})<br />
                                            @endif
                                            @if($data->data->diag3 != null || $data->data->diag3 != 'null')
                                                {{ empty($data->data->diag3->nmDiag) ? '' : $data->data->diag3->nmDiag }} ({{ empty($data->data->diag3->kdDiag) ? '' : $data->data->diag3->kdDiag }})<br />
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telah diberikan</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>ada</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="right">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>Tanggal lahir</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>{{ empty($data->data->tglLahir) ? '' : $data->data->tglLahir }}, ({{ date_diff(date_create($data->data->tglLahir), date_create('today'))->y }} Tahun)</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Status</td>
                                        <td style="vertical-align: middle;">&nbsp;:&nbsp;</td>
                                        <td style="vertical-align: middle;"><div class="box" style="padding: 2px 10px;">{{ empty($data->data->pisa) ? '' : $data->data->pisa }}</div> Utama/Tanggunan</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle;">Jenis Kelamin</td>
                                        <td style="vertical-align: middle;">&nbsp;:&nbsp;</td>
                                        <td style="vertical-align: middle;"><div class="box" style="padding: 2px 10px;">{{ empty($data->data->sex) ? '' : $data->data->sex }}</div> (L / P)</td>
                                    </tr>
                                    <tr>
                                        <td>Catatan</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>{{ empty($data->data->catatan) ? '' : $data->data->catatan }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div><br />
                </div>
                <div class="content">
                    <p style="margin-bottom: 15px; color: red;"># Alasan Rujuk Diagnosa Non-Spesialistik : {{ empty($data->data->tacc->nmTacc) ? '-' : $data->data->tacc->nmTacc }} {{ empty($data->data->tacc->alasanTacc) ? '' : $data->data->tacc->alasanTacc }}</p>
                    <p>Atas bantuannya, diucapkan terima kasih</p><br />
                    <div class="table">
                        <div class="row">
                            <div class="left" style="width: auto;">
                                <p style="margin-bottom: 10px;">Tgl. Rencana Berkunjung : {{ empty($data->data->tglKunjungan) ? '' : $data->data->tglKunjungan }}</p>
                                <p style="margin-bottom: 10px;">Jadwal Praktek : {{ empty($data->data->jadwal) ? '' : $data->data->jadwal }}</p>
                                <p style="margin-bottom: 10px;">Surat rujukan berlaku 1[satu] kali kunjungan, berlaku sampai dengan : {{ date('d F Y', strtotime("+90 days")) }}</p>
                            </div>
                            <div class="right" style="margin-left: 10px; width: 200px; text-align: center;">
                                <p>Salam sejawat, {{ date('d F Y') }}</p><br /><br />
                                <p>{{ empty($data->data->dokter->nmDokter) ? '' : $data->data->dokter->nmDokter }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <h5 style="font-size: 14px; text-align: center;"><b><span style="border-bottom: 1px solid;">SURAT RUJUKAN BALIK</span></b></h5>
                <div class="content">
                    <p>Teman sejawat Yth.<br />Mohon kontrol selanjutnya penderita :</p>
                    <div style="padding: 0 20px;">
                        <div class="table">
                            <div class="row">
                                <div class="left" style="width: auto;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 25%;">Nama</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td>{{ $data->data->nmPst }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%;">Diagnosa</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td>................................................................................................................................</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%;">Terapi</td>
                                            <td>&nbsp;:&nbsp;</td>
                                            <td>................................................................................................................................</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>Tindak lanjut yang dianjurkan</p><br />
                    <div class="table">
                        <div class="row">
                            <div class="left">
                                <div>
                                    <div class="box"></div>
                                    <p style="display: inline-block; width: auto; margin: 0px; padding: 0px;">Pengobatan dengan obat-obatan :</p>
                                </div>
                                <p style="margin: -10px 0px 10px 40px; padding: 0px;">........................................................................</p>
                                <div>
                                    <div class="box"></div>
                                    <p style="display: inline-block; width: auto; margin: 0px; padding: 0px;">Kontrol kembali ke RS tanggal : ....................</p>
                                </div>
                                <div>
                                    <div class="box"></div>
                                    <p style="display: inline-block; width: auto; margin: 0px; padding: 0px;">Lain-lain : .......................................................</p>
                                </div>
                            </div>
                            <div class="right">
                                <div>
                                    <div class="box"></div>
                                    <p style="display: inline-block; width: auto; margin: 0px; padding: 0px;">Perlu rawat inap</p>
                                </div>                                
                                <div>
                                    <div class="box"></div>
                                    <p style="display: inline-block; width: auto; margin: 0px; padding: 0px;">Konsultasi selesai</p>
                                </div>
                                <p style="margin: -10px 0px 10px 40px; padding: 0px;">...................................tgl.................................</p>
                            </div>
                        </div>
                    </div>
                    <div class="table">
                        <div class="row">
                            <div class="left" style="width: auto;"></div>
                            <div class="right" style="width: 200px; text-align: center;">
                                <p>Dokter RS</p><br /><br />
                                <p>(................................................)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

</body>
</html>