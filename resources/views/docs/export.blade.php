<div class="modal fade window" id="export" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img src="{{ asset('assets/images/icons/action/export.png') }}" />
        <h5 class="modal-title" id="exampleModalLongTitle">
          <span id="title-export">Export Data</span>
        </h5>
      </div>
      <input type="text" name="export_key" id="export_key" val="" hidden>
      <form id="form-export" class="">
        <div class="modal-body alert-notif">
          <div class="form-group" id="format-export">
            <label class="control-label">Format</label>
            <input type="text" class="form-control" value="Excel (xlsx)" disabled>
            <span id="loading-code_dpho" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
            <span class="help-block"></span>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit">
            <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
            export
          </button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">

$("#form-export").submit(function() {
    var key = document.getElementById("export_key").value;
    $.ajax({
        url: "{{ $docs_url }}/{{ $lang }}/download/clinic/{{ $id_clinic }}/export/"+key,
        type: "GET",
        contentType: false,
        success: function(fileUrl){
            var link = document.createElement('a');
            link.href = fileUrl;
            link.click();
        },
        error: function(){
            notif(false, 'Terjadi Kesalahan Saat Melakukan Export.');
        }
    });
})

if (document.getElementById("export_key").value == 'patients') {
    document.getElementById("title-export").innerHTML = "Export Data Pasien";
} else if (document.getElementById("export_key").value == 'medicines') {
    document.getElementById("title-export").innerHTML = "{{ trans('messages.export_inventory_data') }}";
}
</script>