<html>
<head>
<style type="text/css">

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0; 
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    min-width: 800px;
}

#print-report .body-report.body-report-large, #print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: 'Open Sans', sans-serif;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: 'Open Sans', sans-serif;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 40%;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 60%;
}

#print-report .body-report .top .right .row-column {
    float: right;
    display: table-row;
}

#print-report .body-report .top .right .column {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    //min-width: 100px;
    padding-right: 24px;
}

#print-report .body-report .top .right .column.last {
    padding-right: 0px;
}

#print-report .body-report section {
    padding: 0px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section.last-section {
    border-bottom: none;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content .item {
    width: 100%;
    padding: 15px 0;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
    padding-right: 24px;
}

#print-report .body-report table tr td.last {
    padding-right: 0px;
}

#print-report .body-report table tr td h3 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.service_report") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <h5>{{ trans("messages.date") }}</h5>
                            <h1>{{ getFormatDate($from_date) }} - {{ getFormatDate($to_date) }}</h1>
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
            </div>
            <section>
                <div class="content">
                    @if(count($service)>0)
                    <div class="item">
                        <table>
                            <tr>
                                <td><h3>{{ trans('messages.number') }}</h3></td>
                                <td><h3>{{ trans('messages.service_name') }}</h3></td>
                                <td><h3>{{ trans('messages.quantity') }}</h3></td>
                                <td><h3>{{ trans('messages.total') }}</h3></td>
                            </tr>
                            <?php foreach ($service as $key => $value) { $no=$key+1; ?>
                            <tr>
                                <td>{{ $no }}</td>
                                <td>{{ $value->service_name }}</td>
                                <td>{{ $value->quantity }}</td>
                                <td>{{ formatCurrency($lang,'Rp', $value->total) }}</td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                    @endif
                </div>
            </section>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$clinic->logo,$clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $clinic->name }}</h3>
                            <h6>{{ $clinic->address }} {{ $clinic->village->name }} {{ $clinic->district->name }} {{ $clinic->regency->name }} {{ $clinic->province->name }} {{ $clinic->country->name }}</h6>
                            <h6>{{ $clinic->phone }} • {{ $clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>