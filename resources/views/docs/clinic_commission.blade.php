<html>
<head>
<style type="text/css">

@font-face {
    font-family: Inter_Regular;
    src: url("../../../public/assets/fonts/Inter/Inter-Regular.woff").format("woff");
}

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    
    @page:first {
        margin-top: 0;
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
    h5, h1, h3, p, td, h6  {
        font-family: 'Open Sans', sans-serif !important;
    }
}
 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    min-width: 800px;
}

#print-report .body-report.body-report-large, #print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: Inter_Regular;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: Inter_Regular;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 40%;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 60%;
}

#print-report .body-report .top .right .row-column {
    float: right;
    display: table-row;
}

#print-report .body-report .top .right .column {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    //min-width: 100px;
    padding-right: 24px;
}

#print-report .body-report .top .right .column.last {
    padding-right: 0px;
}

#print-report .body-report section {
    padding: 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section.last-section {
    border-bottom: none;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content .item {
    width: 100%;
    padding: 15px 0;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444 !important;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
    padding-right: 24px;
}

#print-report .body-report table tr td.last {
    padding-right: 0px;
}

#print-report .body-report table tr td h3 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.item table thead  {
    background: #F5F8F9;
}

.item table thead tr th {
    padding: 16px 16px;
}

.item table tbody tr td.list-content {
    padding: 20px !important;
}

.item table thead tr th h3 {
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 18px;
    color: #757575;
    margin: 0;
}
.content h5 {
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    color: #444444;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.commission_report") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <h5>{{ trans("messages.doctor") }}</h5>
                            <h1>{{ $commission_detail->doctor_name }}</h1>
                        </div>
                        <div class="right">
                            <h5>{{ trans("messages.date") }}</h5>
                            <h1>{{ getFormatDate($from_date) }} - {{ getFormatDate($to_date) }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <section>
                <div class="content">
                    <h5>{{ trans('messages.service_income') }} - {{ $commission_detail-> polyclinic_name}}</h5>
                    <div class="item">
                        <table>
                            <tr>
                                <td><h3>{{ trans('messages.total_income') }}</h3></td>
                                <td><h3>{{ trans('messages.patient') }}</h3></td>
                                <td><h3>{{ trans('messages.total') }} {{ trans('messages.registration') }}</h3></td>
                            </tr>
                            <tr>
                                <td>{{ formatCurrency($lang,'Rp', $commission_detail->commission) }}</td>
                                <td>{{ $commission_detail->total_patient }}</td>
                                <td>{{ $commission_detail->total_invoice }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="item">
                        <table>
                            <thead>
                                <tr>
                                    <th><h3>No</h3></th>
                                    <th><h3>{{ trans('messages.service_name') }}</h3></th>
                                    <th><h3>{{ trans('messages.qty') }}</h3></th>
                                    <th><h3>{{ trans('messages.total_income') }}</h3></th>
                                    <th><h3>{{ trans('messages.commission_per_item') }}</h3></th>
                                    <th><h3>{{ trans('messages.total_commission') }}</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($commission_detail->service as $key => $service) {$no = $key+1; ?>
                                <tr>
                                    <td class="list-content">{{ $no }}</td>
                                    <td class="list-content">{{ $service->service_name }}</td>
                                    <td class="list-content">{{ $service->quantity }}</td>
                                    <td class="list-content">{{ $service->total }}</td>
                                    <td class="list-content">{{ $service->commission }}</td>
                                    <td class="list-content">{{ $service->total_commission }}</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$clinic->logo,$clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $clinic->name }}</h3>
                            <h6>{{ $clinic->address }} {{ $clinic->village->name }} {{ $clinic->district->name }} {{ $clinic->regency->name }} {{ $clinic->province->name }} {{ $clinic->country->name }}</h6>
                            <h6>{{ $clinic->phone }} • {{ $clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>