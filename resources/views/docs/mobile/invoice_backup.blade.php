<html>
<head>
<style type="text/css">

@page {
    size: A5;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0; 
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .header-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #24e2cf;
    height: 30px;
    z-index: 20;
}

#print-report .header-report .before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    background-color: #24e2cf;
    width: 50%;
    height: 30px;
}

#print-report .header-report .after {
    content: "";
    position: absolute;
    top: 0px;
    height: 100%;
    right: 50%;
    background-color: #ffffff;
    width: 30px;
    height: 60px;
    -webkit-transform: skew(-30deg);
    -moz-transform: skew(-30deg);
    -o-transform: skew(-30deg);
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    margin-top: 35px;
    margin-bottom: 80px;
    z-index: 50;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report .table-report {
    display: table;
    width: 100%;
}

#print-report .body-report .table-report .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .table-report .row .left {
    display: table-cell;
    padding: 10px 10px;
    vertical-align: top;
    width: 10px;
}

#print-report .body-report .table-report .row .left .logo-report {
    position: relative;
    float: none;
    margin: 0 0;
    border-right: 1px solid #b3babe;
    padding: 5px 20px 5px 20px;
    text-align: right;
}

#print-report .body-report .table-report .row .left .logo-report img {
    max-height: 100px;
    max-width: 100px;
}

#print-report .body-report .table-report .row .left .logo-report .photo-image {
    margin: 0 auto;
    border-radius: 50px;
    -webkit-border-radius: 50px;
    -moz-border-radius: 50px;
    -ms-border-radius: 50px;
    -o-border-radius: 50px;
    background-color: #85c636;
    color: #ffffff;
    font-weight: normal;
    vertical-align: middle !important;
    text-align: center;
    font-size: 75px;
    padding-top: 0px !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
}

#print-report .body-report .table-report .row .right {
    display: table-cell;
    padding: 10px 10px;
    vertical-align: top;
}

#print-report .body-report .table-report .row .right table {
    width: 80%;
}

#print-report .body-report table tr td {
    padding: 3px 0;
    font-size: 14px;
    vertical-align: middle;
    text-align: left;
}

#print-report .body-report table tr td h1 {
    margin: 0 0;
    color: #1ab1e5;
    font-size: 30px;
    vertical-align: top;
    text-align: left;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report table tr td h2 {
    margin: 0 0;
    color: #1ab1e5;
    font-size: 20px;
    vertical-align: top;
    text-align: left;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report table tr td h3 {
    margin: 0 0;
    color: #1ab1e5;
    font-size: 16px;
    vertical-align: top;
    text-align: left;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report table tr td h4 {
    margin: 0 0;
    color: #000000;
    font-size: 16px;
    vertical-align: top;
    text-align: left;
    font-weight: bold;
    margin: 5px 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report table tr td.text-left {
    text-align: left;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
}

#print-report .body-report table tr td.text-center {
    text-align: center;
}

#print-report .body-report table tr td.text-number {
    width: 10px;
}

#print-report .body-report table tr td.text-symbol {
    width: 20px;
    padding: 5px 10px;
}

#print-report .body-report table tr td.text-price {
    width: 130px;
    padding: 5px 10px;
}

#print-report .body-report table tr td.price-true {
    color: #000000;
    font-size: 14px;
}

#print-report .body-report table tr td.price-false {
    color: #000000;
    font-size: 14px;
}

#print-report .body-report table tr td.text-header {
    font-size: 14px;
}

#print-report .body-report table tr td.text-discount {
    color: #ff6e6e;
}

#print-report .body-report table tr td.th {
    background-color: #24e2cf;
    color: #ffffff !important;
    padding: 5px 15px;
}

#print-report .body-report table tr td.td {
    background-color: #f1f1f1;
    padding: 5px 10px;
}


#print-report .body-report section {
    padding: 20px 5px 20px 5px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content {
    padding: 10px 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .success {
    color: #87cc62;
    font-size: 20px;
    margin: 0 0;
    padding: 0 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report .failed {
    color: #ff6e6e;
    font-size: 20px;
    margin: 0 0;
    padding: 0 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .footer-link {
    position: absolute;
    bottom: 25px;
    left: 0;
    width: 100%;
    text-align: left;
    font-size: 12px;
    color: #000000;
    padding: 5px 10px;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .footer-link a {
    text-decoration: none;
    color: #00a1e5;
}

#print-report .footer-report {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: right;
    font-size: 13px;
    color: #000000;
    padding: 5px 10px;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-top: 1px solid #b3babe;
}

#print-report .footer-report .logo-report {
    text-align: left;
    color: #000000;
    font-size: 13px;
    margin: 0 0;
    padding: 0 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    font-weight: bold;
}

#print-report .footer-report .logo-report span {
    color: #1ab1e5;
}

#print-report .footer-report .footer-logo {
    max-height: 12px;
    margin-left: 3px;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="header-report"><div class="before"></div><div class="after"></div></div>
    <div class="body-report">
        <div class="table-report">
            <div class="row">
                <div class="left">
                    <div class="logo-report">
                        <?php echo thumbnail('lg','clinic',$storage,$invoice->clinic->logo,$invoice->clinic->name); ?>
                    </div>
                </div>
                <div class="right">
                    <table border="0">
                        <tr>
                            <td><h2>{{ $invoice->clinic->name }}</h2></td>
                        </tr>
                        <tr>
                            <td>{{ $invoice->clinic->address }} {{ $invoice->clinic->village }} {{ $invoice->clinic->district }} {{ $invoice->clinic->regency }} {{ $invoice->clinic->province }} {{ $invoice->clinic->country }}</td>
                        </tr>
                        <tr>
                            <td>{{ $invoice->clinic->phone }}</td>
                        </tr>
                        <tr>
                            <td>{{ $invoice->clinic->email }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <section>
            <div class="content">
                <table width="100%">
                    <tr>
                        <td><h2>{{ $invoice->patient->name }}</h2></td>
                    </tr>
                    <tr>
                        <td>{{ $invoice->patient->mrn }}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><h3>{{ $invoice->number }}</h3></td>
                    </tr>     
                    <tr>
                        <td>{{ getFormatDate($invoice->date) }}</td>
                    </tr>
                    <tr>
                        <td>
                            <?php if($invoice->status->code==1) {
                                echo '<span class="success">'.trans("messages.keel").'</span>';
                            } else {
                                echo '<span class="failed">'.trans("messages.not_keel").'</span>';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    @if($invoice->note!="")
                    <tr>
                        <td><h2>{{ trans('messages.note') }}</h2></td>                
                    </tr>
                    <tr>
                        <td><?php echo checkNull($invoice->note); ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    @endif
                    <tr>
                        <td><h2>{{ trans('messages.payment') }}</h2></td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <?php if($invoice->service!=null) { $total_sevice=0; ?>
                                <tr>
                                    <td colspan="6"><h4>{{ trans('messages.service') }}</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="th">{{ trans('messages.service_name') }}</td>
                                    <td class="text-center th">{{ trans('messages.quantity') }}</td>
                                    <td class="th"></td>
                                    <td colspan="2" class="text-center th">{{ trans('messages.total') }}</td>
                                </tr>
                                <?php foreach ($invoice->service as $key => $value) { $no = $key+1; $total_sevice+=$value->total ?>                
                                <tr>
                                    <td class="text-number td"><b>{{ $no }}. </b></td>
                                    <td class="td"><b>{{ $value->service->name }}</b></td>
                                    <td class="text-center td">{{ $value->quantity }}</td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right text-price price-true td">{{ formatCurrency($lang,'', $value->cost*$value->quantity) }}</td>
                                </tr>
                                <?php if($value->admin_cost!=0) { ?>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td">{{ trans('messages.admin_cost') }}</td>
                                    <td class="text-center td">{{ $value->quantity }}</td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right price-true td">{{ formatCurrency($lang,'', $value->quantity*$value->admin_cost) }}</td>
                                </tr>
                                <?php } ?>
                                <?php if($value->doctor_cost!=0) { ?>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td">- {{ trans('messages.doctor_cost') }}</td>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right price-true td">{{ formatCurrency($lang,'', $value->doctor_cost) }}</td>
                                </tr>
                                <?php } ?>
                                <?php

                                if($value->discount!=0) {

                                $discount = $value->discount;
                                $discount_type = $value->discount_type;

                                $text_dicount = "";
                                if($discount_type=="%"){
                                    $discount = ($discount/100)*((($value->cost+$value->admin_cost)*$value->quantity)+$value->doctor_cost);
                                    $text_dicount = "(".$value->discount."%)";
                                }

                                ?>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td text-discount">{{ trans('messages.discount') }} {{ $text_dicount }}</td>
                                    <td class="td"></td>
                                    <td class="text-right price-false td">-</td>
                                    <td class="text-symbol price-false td">Rp</td>
                                    <td class="text-right price-false td">{{ formatCurrency($lang,'', $discount) }}</td>
                                </tr>
                                <?php } ?>
                                <?php if($value->discount!=0 || $value->admin_cost!=0) { ?>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right price-true td">{{ formatCurrency($lang,'', $value->total) }}</td>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                                <tr>
                                    <td class="td text-right" colspan="3"><b>{{ trans('messages.total') }}</b></td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right price-true td">{{ formatCurrency($lang,'', $total_sevice) }}</td>
                                </tr>
                                <?php } ?>
                                <?php if($invoice->medicine!=null) { $total_medicine=0; ?>
                                <tr>
                                    <td colspan="6"><br /></td>
                                </tr>
                                <tr>
                                    <td colspan="6"><h4>{{ trans('messages.medicine') }}</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="th">{{ trans('messages.medicine_name') }}</td>
                                    <td class="text-center th">{{ trans('messages.quantity') }}</td>
                                    <td class="th"></td>
                                    <td colspan="2" class="text-center th">{{ trans('messages.total') }}</td>
                                </tr>
                                <?php foreach ($invoice->medicine as $key => $value) { $no = $key+1; $total_medicine+=$value->total; ?>
                                <tr>
                                    <td class="text-number td"><b>{{ $no }}. </b></td>
                                    <td class="td"><b>{{ $value->medicine->name }}</b></td>
                                    <td class="text-center td">{{ $value->quantity }}</td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right text-price price-true td">{{ formatCurrency($lang,'', $value->total) }}</td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td class="td text-right" colspan="3"><b>{{ trans('messages.total') }}</b></td>
                                    <td class="td"></td>
                                    <td class="text-symbol price-true td">Rp </td>
                                    <td class="text-right price-true td">{{ formatCurrency($lang,'', $total_medicine) }}</td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="6"><br /></td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.subtotal') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $invoice->subtotal) }}</td>
                                </tr>
                                <?php
                                if($invoice->discount!=0) {

                                $discount = $invoice->discount;
                                $discount_type = $invoice->discount_type;

                                $text_dicount = "";
                                if($discount_type=="%"){
                                    $discount = ($discount/100)*($invoice->subtotal);
                                    $text_dicount = "(".$invoice->discount."%)";
                                } ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header text-discount">{{ trans('messages.discount') }} {{ $text_dicount }}</td>
                                    <td class="text-right price-false">-</td>
                                    <td class="text-symbol price-false">Rp </td>
                                    <td class="text-right price-false text-price">{{ formatCurrency($lang,'', $discount) }}</td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.total_payment') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang, "", $invoice->total) }}</td>
                                </tr>
                                <tr>
                                    <td colspan="6"><br /></td>
                                </tr>
                                
                                <tr>
                                    <td colspan="3" class="text-right text-header"><b>{{ trans('messages.amount_paid') }}</b></td>
                                    <td colspan="2"></td>
                                </tr>
                                <?php
                                if($invoice->insurance!=null) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.insurance') }} ({{ $invoice->insurance->name }})</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $invoice->insurance->amount) }}</td>
                                </tr>
                                <?php } ?>
                                <?php
                                if($invoice->prepayment!=0) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.prepayment') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $invoice->prepayment) }}</td>
                                </tr>
                                <?php } ?>
                                <?php
                                if($invoice->cash!=0) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.cash') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $invoice->cash) }}</td>
                                </tr>
                                <?php } ?>
                                <?php
                                if($invoice->credit_card!=0) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.credit_card') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $invoice->credit_card) }}</td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3" class="text-right"></td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>

                                    <?php
                                    $total = 0;
                                    $insurance = 0;

                                    if($invoice->insurance!=null) {
                                        $total = $invoice->prepayment+$invoice->cash+$invoice->credit_card+$invoice->insurance->amount;

                                        $insurance = $invoice->insurance->amount;
                                    } else {
                                        $total = $invoice->prepayment+$invoice->cash+$invoice->credit_card;
                                    }

                                    ?>

                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $total) }}</td>
                                </tr>
                                <?php

                                $change = ($invoice->prepayment+$invoice->cash+$invoice->credit_card+$insurance)-$invoice->total;

                                if($change>0) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.change') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $change) }}</td>
                                </tr>
                                <?php } ?>

                                <?php

                                $remaining_payment = $invoice->total-($invoice->prepayment+$invoice->cash+$invoice->credit_card+$insurance);

                                if($remaining_payment>0) { ?>
                                <tr>
                                    <td colspan="3" class="text-right text-header">{{ trans('messages.remaining_payment') }}</td>
                                    <td></td>
                                    <td class="text-symbol price-true">Rp </td>
                                    <td class="text-right price-true text-price">{{ formatCurrency($lang,'', $remaining_payment) }}</td>
                                </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </section>
    </div>
    <div class="footer-link">
        {{ trans("messages.info_link_invoice") }}: <a href="{{ $docs_url }}/{{ $lang }}/download/invoice/{{ $invoice->number }}">{{ $docs_url }}/{{ $lang }}/download/invoice/{{ $invoice->number }}</a>
    </div>
    <div class="footer-report">
        {{ trans("messages.powered_by") }} 
        <img class="footer-logo" src="{{ $storage }}/images/icons/logo.png" />
    </div>
</div>

</body>
</html>