<html>
<head>
<style type="text/css">

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    
    @page:first {
        margin-top: 0;
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    width: 100%;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: 'Open Sans', sans-serif;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: 'Open Sans', sans-serif;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top h1.status {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #24e2cf;
    text-transform: capitalize;
}

#print-report .body-report .top h1.status.success {
    color: #24e2cf;
}

#print-report .body-report .top h1.status.failed {
    color: #db0062;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .column {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
}

#print-report .body-report .top .column .item {
    margin-bottom: 24px;
}

#print-report .body-report .top .column .item.last {
    margin-bottom: 0px;
}

#print-report .body-report section {
    padding: 0px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content .item {
    width: 100%;
    padding: 15px 0;
    border-bottom: 1px solid #f0f0f0;
}

#print-report .body-report section .content .summary {
    width: 100%;
    padding: 14px 0 30px 0;
}

#print-report .body-report section .content .summary .new-line {
    padding: 15px 0;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
    padding-right: 24px;
}

#print-report .body-report table tr td.padding-right {
    padding-right: 24px;
}

#print-report .body-report table tr td.last {
    padding-right: 0px;
}

#print-report .body-report table tr td h1 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report table tr td h3 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report table tr td b {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td span {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.67;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td.new-line {
    padding: 4px 0;
}

#print-report .body-report table tr td.sub {
    padding-top: 5px;
    vertical-align: middle;
    text-align: left;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report section .content .summary table tr td {
    padding: 10px 0;
}

#print-report .body-report section .content .summary table tr td.summary-text {
    padding-right: 20px;
    vertical-align: middle;
    text-align: right;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.14;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .link-sec {
    padding: 16px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report .link-sec p {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .link-sec a {
    text-decoration: none;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #1ab1e5;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}
.link-invoice {
    overflow: hidden;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="container-report">
        <div class="header-report">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.invoice") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="column">
                            <div class="item">
                                <h5>{{ trans("messages.patient") }}</h5>
                                <h1>{{ $invoice->patient->name }}</h1>
                                <p>{{ $invoice->patient->mrn }}</p>
                            </div>
                            <div class="item">
                                <h5>{{ trans("messages.invoice_no") }}</h5>
                                <h1>{{ $invoice->number }}</h1>
                            </div>
                            <div class="item">
                                <h5>{{ trans("messages.date") }}</h5>
                                <h1>{{ getFormatDate($invoice->date) }}</h1>
                            </div>
                            <div class="item last">
                                <h5>{{ trans("messages.status") }}</h5>
                                <?php if($invoice->status->code==1) {
                                    echo '<h1 class="status success">'.trans("messages.keel").'</h1>';
                                } else {
                                    echo '<h1 class="status failed">'.trans("messages.not_keel").'</h1>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section>
                <div class="content">
                    <?php if($invoice->service!=null) { $total_sevice=0; ?>
                    <div class="item">
                        <table>
                            <tr>
                                <td colspan="4"><h1>{{ trans('messages.service') }}</h1></td>
                            </tr>
                            <tr>
                                <td class="padding-right"><h3>{{ trans('messages.service_name') }}</h3></td>
                                <td><h3>{{ trans('messages.quantity') }}</h3></td>
                                <td colspan="2"><h3>{{ trans('messages.total') }}</h3></td>
                            </tr>
                            <?php foreach ($invoice->service as $key => $value) { $no = $key+1; $total_sevice+=$value->total ?>
                            <tr>
                                <td class="padding-right"><b>{{ $value->service->name }}</b></td>
                                <td style="width: 72px">{{ $value->quantity }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->cost*$value->quantity) }}</td>
                            </tr>
                            <?php if($value->admin_cost!=0) { ?>
                            <tr>
                                <td class="padding-right"><span>{{ trans('messages.admin_cost') }}</span></td>
                                <td style="width: 72px"></td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->quantity*$value->admin_cost) }}</td>
                            </tr>
                            <?php } ?>
                            <?php if($value->doctor_cost!=0) { ?>
                            <tr>
                                <td class="padding-right"><span>{{ trans('messages.doctor_cost') }}</span></td>
                                <td style="width: 72px"></td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->doctor_cost) }}</td>
                            </tr>
                            <?php } ?>
                            <?php

                            if($value->discount!=0) {

                            $discount = $value->discount;
                            $discount_type = $value->discount_type;

                            $text_dicount = "";
                            if($discount_type=="%"){
                                $discount = ($discount/100)*((($value->cost+$value->admin_cost)*$value->quantity)+$value->doctor_cost);
                                $text_dicount = "(".$value->discount."%)";
                            }

                            ?>
                            <tr>
                                <td class="padding-right"><span>{{ trans('messages.discount') }} {{ $text_dicount }}</span></td>
                                <td style="width: 72px"></td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">-{{ formatCurrency($lang,'', $discount) }}</td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td style="width: 72px" class="sub">{{ trans('messages.total') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->total) }}</td>
                            </tr>
                            <tr><td colspan="4" class="new-line"></td></tr>
                            <?php } ?>
                        </table>
                    </div>
                    <?php } ?>
                    <?php if($invoice->medicine!=null) { $total_medicine=0; ?>
                    <div class="item">
                        <table>
                            <tr colspan="4">
                                <td><h1>{{ trans('messages.medicine') }}</h1></td>
                            </tr>
                            <tr>
                                <td class="padding-right"><h3>{{ trans('messages.medicine_name') }}</h3></td>
                                <td><h3>{{ trans('messages.quantity') }}</h3></td>
                                <td colspan="2"><h3>{{ trans('messages.total') }}</h3></td>
                            </tr>
                            <?php foreach ($invoice->medicine as $key => $value) { $no = $key+1; $total_medicine+=$value->total; ?>
                            <tr>
                                <td class="padding-right"><b>{{ $value->medicine->name }}</b></td>
                                <td style="width: 72px">{{ $value->quantity }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->total) }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="width: 72px" class="sub">{{ trans('messages.total') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $value->total) }}</td>
                            </tr>
                            <tr><td colspan="4" class="new-line"></td></tr>
                            <?php } ?>                            
                        </table>
                    </div>
                    <?php } ?>
                    <div class="summary">
                        <table>
                            <tr>
                                <td class="summary-text">{{ trans('messages.subtotal') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $invoice->subtotal) }}</td>
                            </tr>
                            <?php
                                if($invoice->discount!=0) {

                                $discount = $invoice->discount;
                                $discount_type = $invoice->discount_type;

                                $text_dicount = "";
                                if($discount_type=="%"){
                                    $discount = ($discount/100)*($invoice->subtotal);
                                    $text_dicount = "(".$invoice->discount."%)";
                                } ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.discount') }} {{ $text_dicount }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">- {{ formatCurrency($lang,'', $discount) }}</td>
                            </tr>
                            <?php } ?>
                            <?php
                            if($invoice->insurance!=null) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.insurance') }} ({{ $invoice->insurance->name }})</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $invoice->insurance->amount) }}</td>
                            </tr>
                            <?php } ?>
                            <tr><td colspan="3" class="new-line"></td></tr>                            
                            <tr>
                                <td class="summary-text">{{ trans('messages.total_payment') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang, "", $invoice->total) }}</td>
                            </tr>
                            <?php
                            if($invoice->prepayment!=0) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.prepayment') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $invoice->prepayment) }}</td>
                            </tr>
                            <?php } ?>
                            <?php
                            if($invoice->cash!=0) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.cash') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $invoice->cash) }}</td>
                            </tr>
                            <?php } ?>
                            <?php
                            if($invoice->credit_card!=0) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.credit_card') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $invoice->credit_card) }}</td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.amount_paid') }}</td>
                                <td style="width: 24px">Rp </td>

                                <?php
                                $total = 0;
                                $insurance = 0;

                                if($invoice->insurance!=null) {
                                    $total = $invoice->prepayment+$invoice->cash+$invoice->credit_card+$invoice->insurance->amount;

                                    $insurance = $invoice->insurance->amount;
                                } else {
                                    $total = $invoice->prepayment+$invoice->cash+$invoice->credit_card;
                                }

                                ?>

                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $total) }}</td>
                            </tr>
                            <?php
                            
                            $change = ($invoice->prepayment+$invoice->cash+$invoice->credit_card+$insurance)-$invoice->total;

                            if($change>0) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.change') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $change) }}</td>
                            </tr>
                            <?php } ?>

                            <?php

                            $remaining_payment = $invoice->total-($invoice->prepayment+$invoice->cash+$invoice->credit_card+$insurance);

                            if($remaining_payment>0) { ?>
                            <tr>
                                <td class="summary-text">{{ trans('messages.remaining_payment') }}</td>
                                <td style="width: 24px">Rp </td>
                                <td style="width: 109px" class="text-right last">{{ formatCurrency($lang,'', $remaining_payment) }}</td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </section>
            <div class="link-sec">
                <p>{{ trans("messages.info_link_invoice") }}: </p>
                <div class="link-invoice">
                    <a href="{{ $docs_url }}/{{ $lang }}/download/invoice/{{ $invoice->number }}">{{ $docs_url }}/{{ $lang }}/download/invoice/{{ $invoice->number }}</a>
                </div>
            </div>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$invoice->clinic->logo,$invoice->clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $invoice->clinic->name }}</h3>
                            <h6>{{ $invoice->clinic->address }} {{ $invoice->clinic->village }} {{ $invoice->clinic->district }} {{ $invoice->clinic->regency }} {{ $invoice->clinic->province }} {{ $invoice->clinic->country }}</h6>
                            <h6>{{ $invoice->clinic->phone }} • {{ $invoice->clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>