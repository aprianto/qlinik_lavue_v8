<html>
    <head>
        <style type="text/css">

        @media print {
            @page {
                margin: 0 10px;
                size: 100mm 50mm;
            }

            h1, h2, h3, h4, h5, h6, span, p {
                font-family: 'Open Sans', sans-serif !important;
            }

            body {
                -webkit-print-color-adjust: exact;
            }

            .container-wrapper {
                display: flex;
                padding: 14px 0;
                border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            }

            .container-wrapper .thumbnail-img {
                align-self: center;
                margin-right: 20px;
            }

            .container-wrapper .thumbnail-img img {
                min-width: 45px;
                max-width: 45px;
                width: 45px;
                min-height: 45px;
                border-radius: 50%;
            }

            .container-wrapper .thumbnail-title {
                align-self: center;
            }

            .container-wrapper .thumbnail-title h3 {
                font-style: normal;
                font-weight: 600;
                font-size: 7px;
                line-height: 10px;
                color: #444444;
                margin: 0;
            }

            .container-wrapper .thumbnail-title h6 {
                font-style: normal;
                font-weight: normal;
                font-size: 7px;
                line-height: 10px;
                margin: 0;
                color: #222222;
            }

            .body-content {
                margin: 10px 0;
            }

            .body-content .section {
                display: flex;
                justify-content: space-between;
                margin-bottom: 10px;
            }

            .body-content .section .section-content h4 {
                font-style: normal;
                font-weight: 600;
                font-size: 8px;
                line-height: 10px;
                color: #222222;
                margin: 0;
            }

            .body-content .section .section-content h5 {
                font-style: normal;
                font-weight: normal;
                font-size: 8px;
                line-height: 10px;
                color: #222222;
                margin: 0;
            }

            .body-content .section .section-content h5 span {
                font-style: normal;
                font-weight: 600;
                font-size: 8px;
                line-height: 10px;
                color: #222222;
            }

            .body-content .section .section-content.last {
                align-self: flex-end;
            }

            .hide {
                display: none;
            }
        }

        </style>
    </head>
    <body>
        @include('function.function')
        <div  id="print-recipe">
            <div>
                <div class="container-wrapper">
                    <div class="thumbnail-img">
                        <?php echo thumbnail('hw','clinic',$storage,$medical_records->clinic->logo,$medical_records->clinic->name, 48, 48); ?>
                    </div>
                    <div class="thumbnail-title">
                        <h3>{{ $medical_records->clinic->name }}</h3>
                        <h6>{{ $medical_records->clinic->address }} {{ $medical_records->clinic->village }} {{ $medical_records->clinic->district }} {{ $medical_records->clinic->regency }} {{ $medical_records->clinic->province }} {{ $medical_records->clinic->country }}</h6>
                        <h6>{{ $medical_records->clinic->phone }} • {{ $medical_records->clinic->email }}</h6>
                    </div>
                </div>
                <div class="body-content">
                    <div class="section">
                        <div class="section-content">
                            <h4>{{ $medical_records->patient->name }}</h4>
                        </div>
                        <div class="section-content">
                            <h5><span>{{ trans('messages.date') }} : </span>{{ getFormatDate($medical_records->date)}}</h5>
                            <h5><span>{{ trans('messages.number') }} : </span>@if( $medical_records->eticket_code !== null ) {{ $medical_records->eticket_code }} @endif</h5>
                        </div>
                    </div>
                    <div class="section">
                        <div class="section-content">
                            @if( $medicine_recipe->concoction != null )
                                <h4>{{ $medicine_recipe->concoction->name }} ({{ trans('messages.concoction') }})</h4>
                                <h5>{{ $medicine_recipe->concoction->notes }}</h5>
                                <h5>{{ $medicine_recipe->concoction->total_package }} {{ $medicine_recipe->concoction->instruction }}</h5>

                            @else
                                <?php foreach ($medicine_recipe->medicines as $key => $value) { ?>
                                    <h4>{{ $value->name }}</h4>
                                    <h5>{{ $medicine_recipe->signa->dose }} x {{ $medicine_recipe->signa->per_day }} {{ $value->unit_name }} {{ $medicine_recipe->signa->time_frame }} {{ trans('messages.during') }} {{ $medicine_recipe->signa->total_day }} {{ trans('messages.day') }}</h5>
                                    <h5>{{ $value->quantity }} {{ $value->unit_name }}</h5>
                                <?php } ?>
                            @endif
                        </div>
                        <div class="section-content last hide">
                            <h5><span>{{ trans('messages.initials_exp_date') }}: </span>-</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>