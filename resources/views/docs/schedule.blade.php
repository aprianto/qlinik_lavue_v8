<html>
<head>
<style type="text/css">

@page {
    size: A5;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0; 
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: 'Open Sans', sans-serif;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: 'Open Sans', sans-serif;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 24px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h1.highlight {
    margin: 4px 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 64px;
    font-weight: 300;
    font-style: normal;
    font-stretch: normal;
    line-height: 1;
    letter-spacing: normal;
    color: #1ab1e5;
}

#print-report .body-report .top h3 {
    margin: 20px 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 150px;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="container-report">
        <div class="header-report">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.proof_registration") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <p>{{ trans("messages.serial_number") }}</p>
                            <h1 class="highlight">{{ $schedule->number }}</h1>
                        </div>
                        <div class="right">
                            <h1>{{ $schedule->patient->name }}</h1>
                            <h5>{{ $schedule->patient->mrn }}</h5>
                            <p>{{ trans("messages.blood_type") }}: {{ checkNull($schedule->patient->blood_type) }}</p>
                            <h3>{{ $schedule->doctor->name }}</h3>
                            <p>{{ $schedule->polyclinic->name }}</p>

                            <?php

                            $day = date('N', strtotime($schedule->date));

                            ?>
                            @if($schedule->appointment==null)
                            <p>{{ getDay($day) }}, {{ getFormatDate($schedule->date) }} ({{ convertTime($schedule->start_time) }} - {{ convertTime($schedule->end_time) }})</p>
                            @else
                            <p>{{ getDay($day) }}, {{ getFormatDate($schedule->date) }} ({{ convertTime($schedule->start_time) }})</p>
                            <p>{{ trans("messages.status") }}: <?php echo confirmation_text($schedule->status->code); ?></p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$schedule->clinic->logo,$schedule->clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $schedule->clinic->name }}</h3>
                            <h6>{{ $schedule->clinic->address }} {{ $schedule->clinic->village }} {{ $schedule->clinic->district }} {{ $schedule->clinic->regency }} {{ $schedule->clinic->province }} {{ $schedule->clinic->country }}</h6>
                            <h6>{{ $schedule->clinic->phone }} • {{ $schedule->clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>