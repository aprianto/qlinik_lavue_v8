<div class="modal fade window" id="import-success" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img src="{{ asset('assets/images/icons/action/import.png') }}" />
        <h5 class="modal-title" id="exampleModalLongTitle">
          <span id="title-import">Import Data</span>
        </h5>
      </div>
      <div class="modal-body alert-notif">
        <div class="qq-uploader-selector qq-uploader">
          <span style="color: #046E89;">
            <img src="{{ asset('assets/images/icons/action/activate.png') }}" width="16" height="16" /> <span id="import-succeed">0</span> data berhasil disimpan
          </span>
          <br>
          <span style="color: #DB0062;">
            <img src="{{ asset('assets/images/icons/action/deactivate.png') }}" width="16" height="16" /> <span id="import-failed">0</span> data tidak dapat dibaca atau tidak valid
          </span>
          <br>
          <span id="import-failed-file"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" onClick="closeModal();">
          <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
          tutup
        </button>
      </div>
    </div>
  </div>
</div>

<link href="{{ asset('assets/all.fine-uploader/fine-uploader-new.css') }}" rel="stylesheet">

<script>
  function closeModal() {
    $('#import-success').modal('hide');
    location.reload();
  }
</script>
