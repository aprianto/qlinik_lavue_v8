<html>
<head>
<style type="text/css">

@font-face {
  font-family: Inter_Regular;
  src: url("../../../public/assets/fonts/Inter/Inter-Regular.woff").format("woff");
}

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page {
        margin: 0 0;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    
    @page:first {
        margin-top: 0;
    }

    body { 
        margin: 0;
        -webkit-print-color-adjust: exact;
        color-adjust: exact;

    }

    h1, h2, h3, h4, h5, h6, span, p {
        font-family: 'Open Sans', sans-serif !important;
    }

    .btn-action {
        display: none;
    }
}
 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    min-width: 800px;
}

#print-report .body-report.body-report-large, #print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    align-items: center;
    color: #046E89;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: Inter_Regular;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

section .content .item table tr td h5 {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: #444444;
    margin: 0;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 40%;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 60%;
}

#print-report .body-report section {
    padding: 0px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section.last-section {
    border-bottom: none;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content .item {
    width: 100%;
    padding: 20px 0;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
    padding-right: 24px;
}

#print-report .body-report table tr td.last {
    padding-right: 0px;
}

#print-report .body-report table tr td h3 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.copyright {
    position: absolute;
    top: 100px;
    left: 30%;
    color: #db0062;
    font-size: 100px;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    font-weight: bold;
    filter:alpha(opacity=5);
    opacity:0.05;
    z-index: 100;
    -webkit-transform: rotate(15deg);
    -moz-transform: rotate(15deg);
    -ms-transform: rotate(15deg);
    -o-transform: rotate(15deg);
    transform: rotate(15deg);
}

section .content .item table tr td span {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #757575;
}
section .content .item table tr td h4 {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #444444;
    margin: 0;
}

section .content .item h1 {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    display: flex;
    align-items: center;
    color: #046E89;
    margin: 0 0 16px 0;
}

.item-list {
    display: flex;
}

.wrapper-list {
    display: flex;
    justify-content: space-between;
}
.wrapper-list .content-wrapper {
    width : 80%;
}

.wrapper-list .bottom-list {
    align-self: center;
}

.wrapper-list .bottom-list .btn-action {
    margin : 0;
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    letter-spacing: 0.5px;
    color: #1AB1E5;
    text-transform: uppercase;
    cursor: pointer;
    border: none;
    background: transparent;
    cursor: pointer;
}
.fa {
    margin-right: 5px;
}
.hide {
    display: none;
}
.list-bullet {
    margin: 2px 0;
    padding-left: 16px !important;
    list-style: unset !important;
}
.list-bullet li {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 16px;
    align-items: center;
    color: #444444;
    margin-bottom: 8px;
}

</style>
</head>
<body>
@include('function.function')
@include('medical_records.print_medicine')
<div  id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.recipe_title") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <section>
                <div class="content">
                    <div class="item">
                        <table>
                            <tr>
                                <td width="450px"><h3>{{ trans('messages.patient') }}<h3></td>
                                <td><h3>{{ trans('messages.doctor_title') }}</h3></td>
                            </tr>
                            <tr>
                                <td><h5>{{ $medical_records->patient->name}}</h5></td>
                                <td><h5>{{ $medical_records->doctor->name }}</h5></td>
                            </tr>
                            <tr>
                                <td><span>{{ $medical_records->patient->mrn }}</span></td>
                                <td><span>{{ $medical_records->polyclinic->name}}</span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </section>
            <section>
                <div class="content">
                    <div class="item">
                        <table>
                            <tr>
                                <td width="450px"><h3>{{ trans('messages.date') }}</h3></td>
                                <td><h3>{{ trans('messages.return_home_status') }}</h3></td>
                            </tr>
                            <tr>
                                <td><h4>{{ getFormatDate($medical_records->date) }}</h4></td>
                                <td><h4>{{ $medical_records->status }}</h4></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </section>
            <section>
                <div class="content">
                    <div class="item">
                        <h1>{{ trans('messages.inventory') }}</h1>
                        <?php if(count($medical_records->prescriptions) == 0) { echo trans("messages.no_medicine"); } else { ?>
                            <?php foreach($medical_records->prescriptions as $key => $value) {$no=$key+1;?>
                                <div class="wrapper-list">
                                    <div class="content-wrapper">
                                        <table>

                                            @if($value->concoction != null)
                                            <tr>
                                                <td width="20px"><h4>{{ $no}}.</h4></td>
                                                <td colspan="3"><h4>{{ $value->concoction->name }} ( {{ trans('messages.concoction') }})</h4></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td colspan="3"><ul class="list-bullet">
                                                    <?php foreach($value->medicines as $index => $medicine) { ?>
                                                        <li>{{ $medicine->name }} ( {{ $medicine->quantity }} {{ $medicine->unit_name }}) </li>   
                                                <?php } ?>
                                                </ul></td>
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td  width="60px"><h3>{{ trans("messages.quantity") }}</h3></td>
                                                <td><h3> : </h3></td>
                                                <td><h3>{{ $value->concoction->total_package }} {{ $value->concoction->instruction}}</h3></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td  width="60px"><h3>{{ trans("messages.dose") }}</h3></td>
                                                <td><h3> : </h3></td>
                                                <td><h3>{{ $value->concoction->notes }}</h3></td>
                                            </tr>
                                            @else 
                                                <?php foreach($value->medicines as $index => $medicine) { ?>
                                                    <tr>
                                                        <td width="20px"><h4>{{ $no }}.</h4></td>
                                                        <td colspan="3"><h4>{{ $medicine->name }}</h4></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td><h3>{{ trans("messages.quantity") }}</h3></td>
                                                        <td><h3> : </h3></td>
                                                        <td><h3> {{ $medicine->quantity }} {{ $medicine->unit_name}}</h3></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td width="60px"><h3>{{ trans("messages.dose") }}</h3></td>
                                                        <td width="10px"><h3> : </h3></td>
                                                        <td><h3>{{ $value->signa->dose }} x {{ $value->signa->per_day }} {{ $medicine->unit_name }} {{ $value->signa->time_frame}} {{ trans('messages.during') }} {{ $value->signa->total_day }} {{ trans('messages.day') }}</h3></td>
                                                    </tr>
                                                <?php } ?>
                                            @endif
                                        </table>
                                    </div>
                                    <div class="bottom-list">
                                        <button onclick="reportRecipe('{{$medical_records->id}}', '{{$value->signa->id}}')" class="btn-action type-{{ $value->signa->id }}"><i class="fa fa-circle-o-notch fa-spin hide"></i>{{ trans('messages.print_label') }}</button>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$medical_records->clinic->logo,$medical_records->clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $medical_records->clinic->name }}</h3>
                            <h6>{{ $medical_records->clinic->address }} {{ $medical_records->clinic->village }} {{ $medical_records->clinic->district }} {{ $medical_records->clinic->regency }} {{ $medical_records->clinic->province }} {{ $medical_records->clinic->country }}</h6>
                            <h6>{{ $medical_records->clinic->phone }} • {{ $medical_records->clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($medical_records->copyright==true) { ?>
        <h1 class="copyright">COPY</h1>
    <?php } ?>
</div>
</body>
</html>