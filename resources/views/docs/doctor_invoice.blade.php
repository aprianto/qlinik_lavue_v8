<html>
<head>
<style type="text/css">

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0; 
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
    }
}

 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .header-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    background-color: #24e2cf;
    height: 30px;
    z-index: 20;
}

#print-report .header-report .before {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    background-color: #24e2cf;
    width: 50%;
    height: 30px;
}

#print-report .header-report .after {
    content: "";
    position: absolute;
    top: 0px;
    height: 100%;
    right: 50%;
    background-color: #ffffff;
    width: 30px;
    height: 60px;
    -webkit-transform: skew(-30deg);
    -moz-transform: skew(-30deg);
    -o-transform: skew(-30deg);
}

#print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    margin-top: 35px;
    margin-bottom: 30px;
    z-index: 50;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report.body-report-large {
    min-width: 800px;
}

#print-report .body-report .table-report {
    display: table;
    width: 100%;
}

#print-report .body-report .table-report .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .table-report .row .left {
    display: table-cell;
    padding: 10px 10px;
    vertical-align: top;
    width: 10px;
}

#print-report .body-report .table-report .row .left .logo-report {
    position: relative;
    float: none;
    margin: 0 0;
    border-right: 1px solid #b3babe;
    padding: 5px 20px 5px 20px;
    text-align: right;
}

#print-report .body-report .table-report .row .left .logo-report img {
    max-height: 100px;
    max-width: 100px;
}

#print-report .body-report .table-report .row .left .logo-report .photo-image {
    margin: 0 auto;
    border-radius: 50px;
    -webkit-border-radius: 50px;
    -moz-border-radius: 50px;
    -ms-border-radius: 50px;
    -o-border-radius: 50px;
    background-color: #85c636;
    color: #ffffff;
    font-weight: normal;
    vertical-align: middle !important;
    text-align: center;
    font-size: 75px;
    padding-top: 0px !important;
    padding-left: 0px !important;
    padding-right: 0px !important;
    padding-bottom: 0px !important;
}

#print-report .body-report .table-report .row .right {
    display: table-cell;
    padding: 10px 10px;
    vertical-align: top;
}

#print-report .body-report .table-report .row .right table {
    width: 80%;
}

#print-report .body-report .table-report .row .right table tr td {
    padding: 3px 0;
    font-size: 14px;
    vertical-align: middle;
    text-align: left;
}

#print-report .body-report .table-report .row .right table tr td h2 {
    margin: 0 0;
    color: #1ab1e5;
    font-size: 20px;
    vertical-align: top;
    text-align: left;
    font-weight: normal;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
}

#print-report .body-report section .content table {
    border-collapse: collapse;
    width: 100%;
}

#print-report .body-report section .content table tr th {
    background-color: #24e2cf;
    color: #ffffff;
    padding: 10px 15px;
    font-weight: normal;
    border: 1px solid #24e2cf;
}

#print-report .body-report section .content table tr th.text-left {
    text-align: left;
}

#print-report .body-report section .content table tr th.text-right {
    text-align: right;
}

#print-report .body-report section .content table tr th.text-center {
    text-align: center;
}

#print-report .body-report section .content table tr th.text-number {
    width: 10px;
}

#print-report .body-report section .content table tr td {
    background-color: #fafafa;
    padding: 10px 10px;
    border: 1px solid #dadada;
}

#print-report .body-report section .content table tr td.text-left {
    text-align: left;
}

#print-report .body-report section .content table tr td.text-right {
    text-align: right;
}

#print-report .body-report section .content table tr td.text-center {
    text-align: center;
}

#print-report .body-report section .content table tr td.text-number {
    width: 10px;
}

#print-report .body-report section {
    padding: 20px 30px 20px 20px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content {
    padding: 10px 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .footer-report {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    text-align: right;
    font-size: 13px;
    color: #000000;
    padding: 5px 10px;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-top: 1px solid #b3babe;
}

#print-report .footer-report .logo-report {
    text-align: left;
    color: #000000;
    font-size: 13px;
    margin: 0 0;
    padding: 0 0;
    font-family: Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;
    font-weight: bold;
}

#print-report .footer-report .logo-report span {
    color: #1ab1e5;
}

#print-report .footer-report .footer-logo {
    max-height: 12px;
    margin-left: 3px;
}

</style>
</head>
<body>
@include('function.function')
<div  id="print-report">
    <div class="header-report header-report-large"><div class="before"></div><div class="after"></div></div>
    <div class="body-report body-report-large">
        <div class="table-report">
            <div class="row">
                <div class="left">
                    <div class="logo-report">
                        <?php echo thumbnail('lg','doctor',$storage,$doctor->photo,$doctor->name); ?>
                    </div>
                </div>
                <div class="right">
                    <table border="0">
                        <tr>
                            <td><h2>{{ $doctor->name }}</h2></td>
                        </tr>
                        <tr>
                            <td>+{{ $doctor->phone_code }}{{ $doctor->phone }}</td>
                        </tr>
                        <tr>
                            <td>{{ $doctor->email }}</td>
                        </tr>
                    </table>
                </div>
                <div class="right">
                    <table border="0">
                        <tr>
                            <td><h2>{{ getFormatDate($from_date) }} - {{ getFormatDate($to_date) }}</h2></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <section>
            <div class="content">
                @if(count($invoice)>0)
                <table>
                    <tr>
                        <th class="text-number text-center">{{ trans("messages.number") }}</th>
                        <th class="text-center">{{ trans("messages.date") }}</th>
                        <th class="text-center">{{ trans("messages.total") }}</th>
                        <th class="text-center">{{ trans("messages.commission") }}</th>
                    </tr>
                    <?php foreach ($invoice as $key => $value) { $no=$key+1; ?>
                    <tr valign="top">
                        <td class="text-center">{{ $no }}</td>
                        <td class="text-left">{{ getFormatDate($value->date) }}</td>
                        <td class="text-right">{{ formatCurrency($lang,'Rp', $value->total) }}</td>
                        <td class="text-right">{{ $value->total_commission==null ? '-' : formatCurrency($lang,'Rp', $value->total_commission) }}</td>
                    </tr>
                    <?php } ?>
                </table>
                @endif
            </div>
        </section>
    </div>
    <div class="footer-report">
        {{ trans("messages.powered_by") }} 
        <img class="footer-logo" src="{{ $storage }}/images/icons/logo.png" />
    </div>
</div>

</body>
</html>