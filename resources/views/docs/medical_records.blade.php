<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style type="text/css">

@font-face {
  font-family: Inter_Regular;
  src: url("../../../public/assets/fonts/Inter/Inter-Regular.woff").format("woff");
}

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin-right: 0;
        margin-left: 0;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    
    @page:first {
        margin-top: 0;
    }

    body { 
        margin: 0;
        -webkit-print-color-adjust: exact;
        color-adjust: exact;

    }

    td, th, h2, h3, span, p, a, table, h1, h5, b, li, h6 {
        font-family: 'Open Sans', sans-serif !important;
    }

    .wrapper-list-content p {
        color: #777777;
        line-height: 26px;
        font-size: 13px;
        letter-spacing: 0.3px;
    }

    .wrapper-list-content span {
        font-style: normal;
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        color: #444444;
    }

    .page {
        page-break-after: always;
        page-break-inside: avoid;
    }
    .page-invoid {
        page-break-inside: avoid;
        page-break-after: always;
    }
    .page-auto {
        page-break-inside: auto;
        page-break-after: always;
    }
    .break-space {
        margin-top: 15px;
    }
}
 
body {
    margin: 0;
    padding: 0;
    -webkit-print-color-adjust: exact;
    color-adjust: exact;
}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    min-width: 800px;
}

#print-report .body-report.body-report-large, #print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
font-family: Inter_Regular;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: Inter_Regular;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 16px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 50%;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 50%;
}


#print-report .body-report section {
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal !important;
    font-stretch: normal !important;
    line-height: 20px !important;
    color: #444444 !important;
}

#print-report .body-report table tr td h2, #print-report .body-report table tr th h2, .list-content h2 {
    margin: 0 0 10px;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report table tr td h2.first-item {
    margin: 0 0;
}

#print-report .body-report table tr td b {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td span {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report table tr td a {
    text-decoration: none;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #1ab1e5;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.content h3 {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    display: flex;
    align-items: center;
    color: #046E89;
    margin: 0 0 20px;
}

.content .list-content .wrapper-list-content {
    margin-bottom: 16px;
}
.wrapper-list-content ul {
    list-style: none;
    padding: 0;
}
.wrapper-list-content ul li {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #222222;
    font-family: Inter_Regular;
}
.wrapper-list-content td.font-bold {
    font-style: normal !important;
    font-weight: 600 !important;
    font-size: 14px !important;
    color: #444444 !important;
    vertical-align: baseline;
}

section.section-plan {
    padding-bottom: 100px !important;
}
.hide {
    display: none;
}
.wrapper-list-content .list-bullet {
    margin: 2px 0;
    padding-left: 16px !important;
    list-style: unset !important;
}
.wrapper-list-content .list-bullet li {
    font-family: Inter_Regular;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 16px;
    align-items: center;
    color: #444444;
    margin-bottom: 8px;
}


</style>
</head>
<body>
@include('function.function')
<div id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.medical_records") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <h5>{{ trans("messages.patient") }}</h5>
                            <h1>{{ $medical_records->patient->name }}</h1>
                            <p>{{ $medical_records->patient->mrn }}</p>
                        </div>
                        <div class="right">
                            <h5>{{ trans("messages.doctor_title") }}</h5>
                            <h1>{{ $medical_records->doctor->name }}</h1>
                            <p>{{ $medical_records->polyclinic->name }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <h5>{{ trans('messages.date') }}</h5>
                            <p>{{ getFormatDate($medical_records->date) }} • {{ getFormatBetweenTime($medical_records->start_time, $medical_records->end_time) }}</p>
                        </div>
                        <div class="right">
                            <h5>{{ trans('messages.return_home_status') }}<h5>
                            <p>{{ $medical_records->status }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <section>
                <div class="content">
                    <h3>{{ trans('messages.subjective') }}</h3>
                    <div class="list-content">
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.anamnese') }}</h2>
                            <span>{{ $medical_records->anamnese }}</span>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.complaints') }}</h2>
                            <span>{{ !empty($medical_records->complaint) ? $medical_records->complaint : '-'  }}</span>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.history_of_illness') }}</h2>
                            <span>
                                @forelse($medical_records->patient->illnesses_history as $illness)
                                    @if($medical_records->patient->illnesses_history[(count($medical_records->patient->illnesses_history) - 1)]->name == $illness->name)
                                        {{$illness->name}}
                                    @else
                                        {{$illness->name.','}}
                                    @endif
                                @empty
                                    {{'-'}}
                                @endforelse
                            </span>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.history_of_allergy') }}</h2>
                            <span>
                                @forelse($medical_records->patient->allergies_history as $allergy)
                                    @if($medical_records->patient->allergies_history[(count($medical_records->patient->allergies_history) - 1)]->name == $allergy->name)
                                        {{$allergy->name.' ('.$allergy->name_category.')'}}
                                    @else
                                        {{$allergy->name.' ('.$allergy->name_category.'),'}}
                                    @endif
                                @empty
                                    {{'-'}}
                                @endforelse
                            </span>
                        </div>

                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.history_of_vaccination') }}</h2>
                            <table>
                                <thead>
                                    <tr>
                                        <th><h2>{{ trans('messages.vaccine_name') }}</h2></th>
                                        <th><h2>{{ trans('messages.vaccine_given_date') }}</h2></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($medical_records->patient->vaccine_history as $vaccineHistory)
                                    <tr>
                                        <td>{{ $vaccineHistory->name }}</td>
                                        <td>
                                        @foreach ($vaccineHistory->history as $history)
                                        {{ $history->dosage_sequence }} &bull; {{ date('d M Y', strtotime($history->date_given)) }} &nbsp;
                                        @endforeach
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </section>
            <section> 
                <div class="content page-auto">
                    <h3>{{ trans('messages.objective') }}</h3>
                    <div class="list-content">
                        <div class="wrapper-list-content">
                            @if ($medical_records->vital_sign != null)
                            <table>
                                <thead>
                                    <tr>
                                        <th><h2>{{ trans('messages.vital_sign') }}</h2></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ trans('messages.body_temperature') }} : {{$medical_records->vital_sign->body_temperature}}</td>
                                        <td>{{ trans('messages.height') }} : {{$medical_records->vital_sign->height}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('messages.sistole') }} : {{$medical_records->vital_sign->blood_sistole}}</td>
                                        <td>{{ trans('messages.weight') }} : {{$medical_records->vital_sign->weight}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('messages.head_circumference') }} : {{$medical_records->vital_sign->head ? $medical_records->vital_sign->head : '-'}}</td>
                                        <td>{{ trans('messages.diastole') }} : {{$medical_records->vital_sign->blood_diastole}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('messages.abdominal_circumference') }} : {{$medical_records->vital_sign->abdomen ? $medical_records->vital_sign->abdomen : '-'}}</td>
                                        <td>{{ trans('messages.pulse') }} : {{$medical_records->vital_sign->pulse}}</td>
                                    </tr>
                                    <tr>
                                        <td>IMT : {{$medical_records->vital_sign->imt ? round($medical_records->vital_sign->imt, 1).' ('.$medical_records->vital_sign->imt_status.')' : '-'}}</td>
                                        <td>{{ trans('messages.respiratory_frequency') }} : {{$medical_records->vital_sign->respiratory_frequency}}</td>
                                    </tr>
                                    <tr>
                                        <td><h2>Kategori IMT menurut Kemenkes RI</h2></td>
                                    </tr>
                                </tbody>
                            </table>
                            @else 
                                <h2>{{ trans('messages.vital_sign') }}</h2>
                                <h2> - </h2>
                            @endif
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.helper_diagnosis') }}</h2>
                            <table>
                                <thead>
                                    <tr>
                                        <th><h2>{{getFormatDate ($medical_records->diagnostic_helper->date)}}</h2></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.radiologi') }}</td>
                                        <td>
                                            <ul>
                                                <li>{{ trans('messages.information') }} : @if($medical_records->diagnostic_helper->radiology_information != null) {{ $medical_records->diagnostic_helper->radiology_information }} @else - @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.blood_pressure') }}</td>
                                        <td>
                                            <ul>
                                                <li>{{ trans('messages.sistole') }} : @if($medical_records->diagnostic_helper->blood_sistole != null) {{ $medical_records->diagnostic_helper->blood_sistole }} @else 0 @endif</li>
                                                <li>{{ trans('messages.diastole') }} : @if($medical_records->diagnostic_helper->blood_diastole != null) {{ $medical_records->diagnostic_helper->blood_diastole }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.routine_blood') }}</td>
                                        <td>
                                            <ul>
                                                <li>Hemoglobin (gr %) : @if($medical_records->diagnostic_helper->hemoglobin != null) {{ $medical_records->diagnostic_helper->hemoglobin }} @else 0 @endif</li>
                                                <li>Leukosit (/mm3) : @if($medical_records->diagnostic_helper->leukocytes != null) {{ $medical_records->diagnostic_helper->leukocytes }} @else 0 @endif</li>
                                                <li>Hematokrit : @if($medical_records->diagnostic_helper->hematocrit != null) {{ $medical_records->diagnostic_helper->hematocrit }} @else 0 @endif</li>
                                                <li>Laju Endap Darah (mm/jam) : @if($medical_records->diagnostic_helper->sedimentation_blood != null) {{ $medical_records->diagnostic_helper->sedimentation_blood }} @else 0 @endif</li>
                                                <li>Eritrosit (juta/m3) : @if($medical_records->diagnostic_helper->erythrocytes != null) {{ $medical_records->diagnostic_helper->erythrocytes }} @else 0 @endif</li>
                                                <li>Trombosit : @if($medical_records->diagnostic_helper->platelets != null) {{ $medical_records->diagnostic_helper->platelets }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.blood_fat') }}</td>
                                        <td>
                                            <ul>
                                                <li>HDL : @if($medical_records->diagnostic_helper->hdl != null) {{ $medical_records->diagnostic_helper->hdl }} @else 0 @endif</li>
                                                <li>LDL : @if($medical_records->diagnostic_helper->ldl != null) {{ $medical_records->diagnostic_helper->ldl }} @else 0 @endif</li>
                                                <li>Cholesterol Total : @if($medical_records->diagnostic_helper->total_cholesterol != null) {{ $medical_records->diagnostic_helper->total_cholesterol }} @else 0 @endif</li>
                                                <li>Trigliserid : @if($medical_records->diagnostic_helper->triglyceride != null) {{ $medical_records->diagnostic_helper->triglyceride }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr class="break-space">
                                        <td class="font-bold">{{ trans('messages.blood_sugar') }}</td>
                                        <td>
                                            <ul>
                                                <li>Gula Darah Sewaktu : @if($medical_records->diagnostic_helper->current_blood_sugar != null) {{ $medical_records->diagnostic_helper->current_blood_sugar }} @else 0 @endif</li>
                                                <li>Gula Darah Post Pradial : @if($medical_records->diagnostic_helper->post_paradial_blood_sugar != null) {{ $medical_records->diagnostic_helper->post_paradial_blood_sugar }} @else 0 @endif</li>
                                                <li>Gula Darah Puasa : @if($medical_records->diagnostic_helper->fasting_blood_sugar != null) {{ $medical_records->diagnostic_helper->fasting_blood_sugar }} @else 0 @endif</li>
                                                <li>HbA1C : @if($medical_records->diagnostic_helper->hba1c != null) {{ $medical_records->diagnostic_helper->hba1c }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.liver_function') }}</td>
                                        <td>
                                            <ul>
                                                <li>SGOT : @if($medical_records->diagnostic_helper->sgot != null) {{ $medical_records->diagnostic_helper->sgot }} @else 0 @endif</li>
                                                <li>Gamma GT : @if($medical_records->diagnostic_helper->gamma_gt != null) {{ $medical_records->diagnostic_helper->gamma_gt }} @else 0 @endif</li>
                                                <li>SGPT : @if($medical_records->diagnostic_helper->sgpt != null) {{ $medical_records->diagnostic_helper->sgpt }} @else 0 @endif</li>
                                                <li>ProtKual : @if($medical_records->diagnostic_helper->protkual != null) {{ $medical_records->diagnostic_helper->protkual }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.kidney_function') }}</td>
                                        <td>
                                            <ul>
                                                <li>Creatinin : @if($medical_records->diagnostic_helper->creatinin != null) {{ $medical_records->diagnostic_helper->creatinin }} @else 0 @endif</li>
                                                <li>Ureum : @if($medical_records->diagnostic_helper->ureum != null) {{ $medical_records->diagnostic_helper->ureum }} @else 0 @endif</li>
                                                <li>Asam Urat : @if($medical_records->diagnostic_helper->uric_acid != null) {{ $medical_records->diagnostic_helper->uric_acid }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold">{{ trans('messages.heart_function') }}</td>
                                        <td>
                                            <ul>
                                                <li>ABI : @if($medical_records->diagnostic_helper->abi != null) {{ $medical_records->diagnostic_helper->abi }} @else 0 @endif</li>
                                                <li>EKG : @if($medical_records->diagnostic_helper->ekg != null) {{ $medical_records->diagnostic_helper->ekg }} @else 0 @endif</li>
                                                <li>Echo : @if($medical_records->diagnostic_helper->echo != null) {{ $medical_records->diagnostic_helper->echo }} @else 0 @endif</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{trans('messages.physical_examination')}}</h2>
                            @if (isset($medical_records->physical_examinations) && count($medical_records->physical_examinations))
                            <table>
                                <thead>
                                    <th></th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    @foreach ($medical_records->physical_examinations as $physical_examination)
                                    <tr>
                                        <td class="font-bold">{{trans('messages.'.$physical_examination->type)}}</td>
                                        @if (isset($physical_examination->value))
                                        <td>{{$physical_examination->value}}<br/></td>
                                        @else
                                        <td>{{trans('messages.within_normal_limit')}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <h2>{{ trans('messages.physical_examination') }}</h2>
                            <h2> - </h2>
                            @endif
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.consciousness') }}</p>
                            <span>{{ $medical_records->consciousness }}</span>
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.additional_notes') }}</p>
                            <span>@if ($medical_records->additional_notes != null) {{ $medical_records->additional_notes }} @else - @endif</span>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class=" content page">
                    <h3>{{ trans('messages.assessment') }}</h3>
                    <div class="list-content">
                        <div class="wrapper-list-content">
                            <table>
                                <thead>
                                    <tr>
                                        <th><h2>{{ trans('messages.icd') }}</h2></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(count($medical_records->icd) == 0) { echo "-"; } else { ?>
                                        <?php
                                            foreach($medical_records->icd as $key => $value) {
                                        ?>
                                        <tr>
                                            <td><span>{{ $value->code }}</span></td>
                                            <td><span>{{ $value->name }}</span></td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.diagnosis') }}</h2>
                            <span>@if ($medical_records->diagnosis != null) {{ $medical_records->diagnosis }} @else - @endif</span>
                        </div>
                        <div class="wrapper-list-content">
                            <h2>{{ trans('messages.prognosis') }}</h2>
                            <span>@if ($medical_records->prognosis === 'sanam') {{ 'Sanam (Sembuh)' }} @elseif ($medical_records->prognosis === 'bonam') {{ 'Bonam (baik)' }} @elseif ($medical_records->prognosis === 'malam') {{ 'Malam (buruk/jelek)' }} @elseif ($medical_records->prognosis === 'dubia-ad-sanam-bolam') {{ 'Dubia ad sanam/bolam (tidak tentu/ragu-ragu, cenderung sembuh/baik)' }} @elseif ($medical_records->prognosis === 'dubia-ad-malam') {{ 'Dubia ad malam (tidak tentu/ragu-ragu, cenderung buruk/jelek)' }} @else - @endif</span>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="content page">
                    <h3>{{ trans('messages.planning') }}</h3>
                    <div class="list-content">
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.service') }}</p>
                            <?php if(count($medical_records->service) == 0) { echo trans("messages.no_service"); } else { ?>
                                <table>
                                    <?php
                                    foreach($medical_records->service as $key => $value) {
                                        $no=$key+1;
                                    ?>
                                    <tr>
                                        <td width="20px"><b>{{ $no }}.</b></td>
                                        <td colspan="3"><b>{{ $value->service->name }}</b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td width="60px"><span>{{ trans("messages.quantity") }}</span></td>
                                        <td width="10px"><span> : </span></td>
                                        <td><span>{{ $value->quantity }}</span></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            <?php } ?>
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.medicine') }}</p>
                            <?php if(count($medical_records->prescriptions) == 0) { echo trans("messages.no_medicine"); } else { ?>
                                <table>
                                    <?php
                                    foreach($medical_records->prescriptions as $key => $value) {
                                        $no=$key+1;
                                    ?>
                                    @if($value->concoction != null)
                                        <tr>
                                            <td width="20px"><b>{{ $no }}.</b></td>
                                            <td colspan="3"><b>{{ $value->concoction->name }} ({{ trans('messages.concoction') }})</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="3"><ul class="list-bullet">
                                                <?php foreach($value->medicines as $index => $medicine) { ?>
                                                    <li>{{ $medicine->name }} ({{ $medicine->quantity }} {{ $medicine->unit_name }}) </li>   
                                            <?php } ?>
                                            </ul></td>
                                        </tr>
                                        <tr class="hide">
                                            <td></td>
                                            <td><span>{{ trans("messages.quantity") }}</span></td>
                                            <td><span> : </span></td>
                                            <td><span> {{ $value->concoction->total_package }} {{ $value->concoction->instruction}}</span></td>
                                        </tr>
                                        
                                        <tr>
                                            <td></td>
                                            <td width="60px"><span>{{ trans("messages.dose") }}</span></td>
                                            <td width="10px"><span> : </span></td>
                                            <td><span>{{ $value->concoction->notes }}</span></td>
                                        </tr>
                                    @else
                                        <?php foreach($value->medicines as $index => $medicine) { ?>
                                            <tr>
                                                <td width="20px"><b>{{ $no }}.</b></td>
                                                <td colspan="3"><b>{{ $medicine->name }}</b></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><span>{{ trans("messages.quantity") }}</span></td>
                                                <td><span> : </span></td>
                                                <td><span> {{ $medicine->quantity }} {{ $medicine->unit_name}}</span></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td width="60px"><span>{{ trans("messages.dose") }}</span></td>
                                                <td width="10px"><span> : </span></td>
                                                <td><span>{{ $value->signa->dose }} x {{ $value->signa->per_day }} {{ $medicine->unit_name }} {{ $value->signa->time_frame}} {{ trans('messages.during') }} {{ $value->signa->total_day }} {{ trans('messages.day') }}</span></td>
                                            </tr>
                                        <?php } ?>
                                    @endif
                                    <?php } ?>
                                </table>
                            <?php } ?>
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.vaccine') }}</p>
                            <?php if(count($medical_records->vaccines) == 0) { echo trans("messages.no_vaccine"); } else { ?>
                                <table>
                                    <?php
                                    foreach($medical_records->vaccines as $key => $value) {
                                        $no = $key + 1;
                                    ?>
                                        <tr>
                                            <td width="20px"><b>{{ $no }}.</b></td>
                                            <td colspan="3"><b>{{ $value->name }}</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><span>{{ trans("messages.quantity") }}</span></td>
                                            <td><span> : </span></td>
                                            <td><span> {{ $value->quantity }} {{ $value->unit_name }}</span></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><span>{{ trans("messages.lot_no") }}</span></td>
                                            <td><span> : </span></td>
                                            <td><span> {{ $value->batch_no}} </span></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            <?php } ?>
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.drug_therapy') }}</p>
                            <span>@if ($medical_records->drug_therapy != null) @foreach (explode('<br />', nl2br($medical_records->drug_therapy)) as $line) {{$line}} <br />  @endforeach @else - @endif</span>
                        </div>
                        <div class="wrapper-list-content">
                            <p>{{ trans('messages.non_drug_therapy') }}</p>
                            <span>@if ($medical_records->non_drug_therapy != null) @foreach (explode('<br />', nl2br($medical_records->non_drug_therapy)) as $line) {{$line}} <br />  @endforeach @else - @endif</span>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bottom">
                <div class="table page-invoid">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$medical_records->clinic->logo,$medical_records->clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $medical_records->clinic->name }}</h3>
                            <h6>{{ $medical_records->clinic->address }} {{ $medical_records->clinic->village }} {{ $medical_records->clinic->district }} {{ $medical_records->clinic->regency }} {{ $medical_records->clinic->province }} {{ $medical_records->clinic->country }}</h6>
                            <h6>{{ $medical_records->clinic->phone }} • {{ $medical_records->clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>