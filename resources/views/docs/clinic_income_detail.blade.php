<html>
<head>
<style type="text/css">

@font-face {
    font-family: Inter_Regular;
    src: url("../../../public/assets/fonts/Inter/Inter-Regular.woff").format("woff");
}

@page {
    size: A4;
    margin: 0;
}

@media print {
    @page { 
        margin: 0 0;
        margin-bottom: 30px;
        margin-top: 30px;
    }
    
    @page:first {
        margin-top: 0;
    }

    body { 
        margin: 0 0; 
        -webkit-print-color-adjust: exact;
        color-adjust: exact;
    }
    td, th, h2, h3, span, p, a, table, h1, h5, b, li, h6 {
        font-family: 'Open Sans', sans-serif !important;
    }

    .table-detail thead th.hide-md {
        background: #f5f8f9;
        color: #757575;
        padding: 16px 16px;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        letter-spacing: normal;
        border: none !important;
        border-bottom: 1px solid #f0f0f0;
        text-align: left;
    }
    td, th, h2, h3, span, p, a, table, h1, h5, b, li, h6 {
        font-family: 'Open Sans', sans-serif !important;
    }
    .table-detail thead th.hide-md {
        background: #f5f8f9;
        color: #757575;
        padding: 16px 16px;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        letter-spacing: normal;
        border: none !important;
        border-bottom: 1px solid #f0f0f0
    }
    h3.table-detail-title {
        margin: 0 0 16px 0;
        font-size: 14px;
        font-weight: 600;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        letter-spacing: normal;
        color: #000;
        text-transform: capitalize
    }
    .table-detail tbody td {
        padding: 16px 16px !important;
        font-size: 12px !important;
        font-weight: normal !important;
        font-style: normal !important;
        font-stretch: normal !important;
        line-height: 1.43 !important;
        letter-spacing: normal !important;
    }
    
}

    h3.table-detail-title {
        margin: 0 0 16px 0;
        font-size: 14px;
        font-weight: 600;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: 1.43;
        letter-spacing: normal;
        color: #000;
        text-transform: capitalize
    }
    .table-detail tbody td {
        padding: 16px 16px !important;
        font-size: 12px !important;
        font-weight: normal !important;
        font-style: normal !important;
        font-stretch: normal !important;
        line-height: 1.43 !important;
        letter-spacing: normal !important;
    }   
}
 
body {
    margin: 0 0;
    padding: 0 0;
    -webkit-print-color-adjust: exact;
    color-adjust: exact;

}

#print-report {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    min-height: 100%;
}

#print-report .container-report {
    border: 1px solid #d8f4f2;
    min-width: 800px;
}

#print-report .body-report.body-report-large, #print-report .header-report.header-report-large {
    min-width: 800px;
}

#print-report .header-report {
    position: relative;
    width: 100%;
    background-color: #d8f4f2;
    padding: 12px 24px;
    z-index: 20;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .table {
    display: table;
    width: 100%;
}

#print-report .header-report .row {
    display: table-row;
    width: 100%;
}

#print-report .header-report .row .left {
    display: table-cell;
    vertical-align: middle;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report .row .right {
    text-align: left;
    display: table-cell;
    vertical-align: top;
    width: 95px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .header-report h1.title-report {
    margin: 0 0;
    padding: 0 0;
    vertical-align: middle;
    font-family: Inter_Regular;
    font-size: 20px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.4;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
    text-transform: capitalize;
}

#print-report .header-report .powered {
    margin: 0 0;
    padding: 0 0;
}

#print-report .header-report .powered h6 {    
    margin: 0 0;
    padding: 0 0;
    vertical-align: top;
    font-family: Inter_Regular;
    font-size: 10px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.2;
    letter-spacing: normal;
    color: #046e89;
    text-align: left;
}

#print-report .header-report .powered img {
    max-height: 24px; 
}

#print-report .body-report {
    position: relative;
    float: none;
    width: 100%;
    z-index: 50;
}

#print-report .body-report .top {
    position: relative;
    float: none;
    width: 100%;
    border-bottom: 1px solid #d8f4f2;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .top h1 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.5;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report .top h5 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .top p {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .top .table {
    display: table;
    width: 100%;
}

#print-report .body-report .top .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .top .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 40%;
}

#print-report .body-report .top .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 60%;
}

#print-report .body-report .top .right .row-column {
    float: right;
    display: table-row;
}

#print-report .body-report .top .right .column {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    //min-width: 100px;
    padding-right: 24px;
}

#print-report .body-report .top .right .column.last {
    padding-right: 0px;
}

#print-report .body-report section {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-bottom: 1px solid #d8f4f2;
}

#print-report .body-report section.last-section {
    border-bottom: none;
}

#print-report .body-report section .content {
    padding: 0 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report section .content .item {
    width: 100%;
}

#print-report .body-report table {
    padding: 0 0;
    margin: 0 0;
    width: 100%;
}

#print-report .body-report table tr td {
    padding: 2px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #444444;
}

#print-report .body-report table tr td.text-right {
    text-align: right;
    padding-right: 24px;
}

#print-report .body-report table tr td.last {
    padding-right: 0px;
}

#print-report .body-report table tr td h3 {
    margin: 0 0 4px 0;
    vertical-align: top;
    text-align: left;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
    text-transform: capitalize;
}

#print-report .body-report .bottom {
    position: relative;
    float: none;
    width: 100%;
    padding: 24px 24px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom .table {
    display: table;
    width: 100%;
}

#print-report .body-report .bottom .row {
    display: table-row;
    width: 100%;
}

#print-report .body-report .bottom .left {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    width: 64px;
}

#print-report .body-report .bottom .right {
    display: table-cell;
    vertical-align: top;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

#print-report .body-report .bottom h3 {
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 14px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    color: #046e89;
}

#print-report .body-report .bottom h6 {
    width: 100%;
    margin: 0 0 4px 0;
    font-family: Inter_Regular;
    font-size: 12px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #757575;
}

#print-report .body-report .bottom .left img {
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
}

.table-detail-doctor tr th:nth-child(1), 
.table-detail-doctor tr th:nth-child(2), 
.table-detail-doctor tr th:nth-child(3), 
.table-detail-service tr th:nth-child(1), 
.table-detail-service tr th:nth-child(2), 
.table-detail-service tr th:nth-child(3),
.table-detail-medicine tr th:nth-child(1),
.table-detail-medicine tr th:nth-child(2),
.table-detail-medicine tr th:nth-child(3) {
    width: 35%;
}

.table-detail-poly tr th:nth-child(1) {
    width: 70%;
}


</style>
</head>
<body>
@include('function.function')

<div  id="print-report">
    <div class="container-report">
        <div class="header-report header-report-large">
            <div class="table">
                <div class="row">
                    <div class="left">
                        <h1 class="title-report">{{ trans("messages.income_report") }}</h1>
                    </div>
                    <div class="right">
                        <div class="powered">
                            <h6>{{ trans("messages.powered_by") }}</h6>
                            <img class="footer-logo" src="{{ $storage }}/images/icons/powered.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-report body-report-large">
            <div class="top">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <h5>{{ trans("messages.date") }}</h5>
                            <h1>{{ getFormatDate($from_date) }} - {{ getFormatDate($to_date) }}</h1>
                        </div>
                        <div class="right"></div>
                    </div>
                </div>
            </div>
            <section>
                <div class="content">
                    @if(count($income_detail)>0)
                    
                    <div class="item">
                        <div class="top">
                            <table>
                                <tr>
                                    <td><h3>{{ trans('messages.total_income') }}</h3></td>
                                    <td><h3>{{ trans('messages.doctor_poly_practice') }}</h3></td>
                                    <td><h3>{{ trans('messages.patient') }}</h3></td>
                                    <td><h3>{{ trans('messages.total_registration') }}</h3></td>
                                </tr>
                                <tr>
                                    <td>{{ formatCurrency($lang,'Rp', $income_detail->income) }}</td>
                                    <td>{{ $income_detail->total_doctor }} / {{$income_detail->total_polyclinic}}</td>
                                    <td>{{ $income_detail->total_patient }}</td>
                                    <td>{{ $income_detail->total_patient }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="top">
                            <h3 class='table-detail-title'>{{ trans('messages.doctor_income') }}</h3>
                            <table class="table-detail">
                                <thead class="table-detail-doctor">
                                <tr>
                                    <th class="hide-md">Dokter</th>
                                    <th class="hide-md">{{ trans('messages.polyclinic') }}</th>
                                    <th class="hide-md">{{ trans('messages.nominal') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($income_detail->commission as $key => $value) { $no=$key+1; ?>
                                    @if($value->doctor_name!=null || $value->polyclinic_name != null || $value->commission != null)
                                        <tr>
                                            <td><h3>{{ $value->doctor_name }}</h3></td>
                                            <td><h3>{{ $value->polyclinic_name }}</h3></td>
                                            <td><h3>{{ $value->commission}}</h3></td>
                                        </tr>
                                    @endif
                                <?php } ?>
                                <tbody>
                            </table>
                        </div>
                        <div class="top">
                            <div>
                                <h3 class='table-detail-title'>{{ trans('messages.polyclinic_income') }}</h3>
                            </div>
                            <table class="table-detail">
                                <thead class="table-detail-poly">
                                    <tr>
                                        <th class="hide-md">{{ trans('messages.polyclinic') }}</th>
                                        <th class="hide-md">{{ trans('messages.nominal') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($income_detail->polyclinic as $key => $polyclinic) { $no=$key+1; ?>
                                        <tr>
                                            <td><h3>{{ $polyclinic->polyclinic_name }}</h3></td>
                                            <td><h3>{{ formatCurrency($lang,'Rp', $polyclinic->income) }}</h3></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="top">
                            <div>
                                <h3 class='table-detail-title'>{{ trans('messages.service_income')}}</h3>
                            </div>
                            <table class="table-detail">
                                <thead class="table-detail-service">
                                    <tr>
                                        <th class="hide-md">{{ trans('messages.service') }}</th>
                                        <th class="hide-md">{{ trans('messages.qty') }}</th>
                                        <th class="hide-md">{{ trans('messages.nominal') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($income_detail->service as $key => $service) { $no=$key+1; ?>
                                        <tr>
                                            <td><h3>{{ $service->service_name }}</h3></td>
                                            <td><h3>{{ $service->quantity }}</h3></td>
                                            <td><h3>{{ formatCurrency($lang,'Rp', $service->total) }}</h3></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="top">
                            <div>
                                <h3 class='table-detail-title'>{{ trans('messages.medicine_income')}}</h3>
                            </div>
                            <table class="table-detail">
                                <thead class="table-detail-medicine">
                                    <tr>
                                        <th class="hide-md">{{ trans('messages.medicine') }}</th>
                                        <th class="hide-md">{{ trans('messages.qty') }}</th>
                                        <th class="hide-md">{{ trans('messages.nominal') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($income_detail->medicine > 0)
                                        <?php foreach ($income_detail->medicine as $key => $medicine) { $no=$key+1; ?>
                                            <tr>
                                                <td><h3>{{ $medicine->medicine_name }}</h3></td>
                                                <td><h3>{{ $medicine->quantity }}</h3></td>
                                                <td><h3>{{ formatCurrency($lang,'Rp', $medicine->total) }}</h3></td>
                                            </tr>
                                        <?php } ?>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="top">
                            <div>
                                <h3 class='table-detail-title'>{{ trans('messages.transaction_income')}}</h3>
                            </div>
                            <table class="table-detail">
                                <thead>
                                    <tr>
                                        <th class="hide-md">{{ trans('messages.invoice_no') }}</th>
                                        <th class="hide-md">{{ trans('messages.patient') }}</th>
                                        <th class="hide-md">{{ trans('messages.cash') }}</th>
                                        <th class="hide-md">{{ trans('messages.debit_credit') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($income_detail->invoice as $key => $invoice) { $no=$key+1; ?>
                                        <tr>
                                            <td><h3>{{ $invoice->number }}</h3></td>
                                            <td><h3>{{ $invoice->patient_name }}</h3></td>
                                            <td><h3>{{ formatCurrency ($lang, 'Rp', $invoice->cash) }}</h3></td>
                                            <td><h3>{{ formatCurrency ($lang, 'Rp', $invoice->credit_card) }}</h3></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </section>
            <div class="bottom">
                <div class="table">
                    <div class="row">
                        <div class="left">
                            <?php echo thumbnail('hw','clinic',$storage,$clinic->logo,$clinic->name, 48, 48); ?>
                        </div>
                        <div class="right">
                            <h3>{{ $clinic->name }}</h3>
                            <h6>{{ $clinic->address }} {{ $clinic->village->name }} {{ $clinic->district->name }} {{ $clinic->regency->name }} {{ $clinic->province->name }} {{ $clinic->country->name }}</h6>
                            <h6>{{ $clinic->phone }} • {{ $clinic->email }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
