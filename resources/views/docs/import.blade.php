
<div class="modal fade window" id="import" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/action/import.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">
                  <span id="title-import">{{ trans('messages.import_inventory_data') }}</span>
                </h5>
            </div>
            <input type="text" name="import_key" id="import_key" hidden>
              <div class="modal-body alert-notif">
                  <div class="form-group">
                    <div id="fine-uploader-manual-trigger"></div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button class="btn btn-primary" id="upload_file">
                      <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                      import
                  </button>
                  <button type="button" class="btn btn-secondary" id="cancel_btn" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
              </div>
        </div>
    </div>
</div>

<link href="{{ asset('assets/all.fine-uploader/fine-uploader-new.css') }}" rel="stylesheet">
<script src="{{ asset('assets/all.fine-uploader/all.fine-uploader.js') }}"></script>

<script type="text/template" id="qq-template-manual-trigger">
  <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
    
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
        <span class="qq-upload-drop-area-text-selector"></span>
    </div>

    <div class="qq-upload-button-selector center">
          <p style="color: #00ABC7; margin-bottom: 2px; text-align:center;">PILIH FILE</p>
    </div>

    <div style="text-align: center;">
      <span>Harap menggunakan template file <a href="" onClick="getImportTemplate()">berikut</a>. Maksimum ukuran file 10 MB</span>
    </div>

    <span class="qq-drop-processing-selector qq-drop-processing">
        <span>Processing dropped files...</span>
        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    
    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals" style="box-shadow:none; margin-top: 40px;">
        <li style="background:#fff;">
            <div class="qq-progress-bar-container-selector">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
            </div>
            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
            <div style="display:flex; align-items:center;">
              <span class="qq-upload-file-selector qq-upload-file" style="color:#186E86;"></span>
              <span class="qq-upload-size-selector qq-upload-size"></span>
              <img src="{{ asset('assets/images/Shape.png') }}" class="qq-upload-cancel-selector" />
            </div>
            
            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
        </li>
    </ul>
    
    <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Close</button>
        </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">No</button>
            <button type="button" class="qq-ok-button-selector">Yes</button>
        </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">
        <div class="qq-dialog-buttons">
            <button type="button" class="qq-cancel-button-selector">Cancel</button>
            <button type="button" class="qq-ok-button-selector">Ok</button>
        </div>
    </dialog>
</div>
</script>

<script>
  var manualUploader = new qq.FineUploader({
    element: document.getElementById('fine-uploader-manual-trigger'),
    template: 'qq-template-manual-trigger',
    request: {
      endpoint: '{{ $api_url }}/{{ $lang }}/import-spreadsheet',
      params: {
        validate: false,
        level: 'user',
        user: "{{ $id_user }}",
        key: document.getElementById("import_key").value
      },
      customHeaders: {
        "Authorization": "Bearer {{ $api_token }}"
      }
    },
    callbacks: {
      onComplete: function(id, name, responseJSON, xhr) {
        document.getElementById("import-succeed").innerHTML = responseJSON.succeed;
        document.getElementById("import-failed").innerHTML = responseJSON.failed;
        if (responseJSON.failed !== 0) {
          document.getElementById("import-failed-file").innerHTML= "unduh file bermasalah <a href='{{ $storage }}/"+responseJSON.resource+"'>disini</a>";
        }
        $('#import').modal('hide');
        $('#import-success').modal('show');
      },
      onError: function(id, name, errorReason, xhrOrXdr) {
        notif(false, 'Terjadi Kesalahan Saat Upload.');
      }
    },
    thumbnails: {
      placeholders: {
        waitingPath: '{{ asset("assets/all.fine-uploader/placeholders/waiting-generic.png") }}',
        notAvailablePath: '{{ asset("assets/all.fine-uploader/placeholders/not_available-generic.png") }}'
      }
    },
    validation: {
      allowedExtensions: ['xlsx', 'xls', 'csv', 'odt', 'txt'],
      itemLimit: 1,
      sizeLimit: 10000000
    },
    autoUpload: false,
    debug: true
  });

  key = document.getElementById("import_key").value;

  qq(document.getElementById("upload_file")).attach("click", function() {
    manualUploader.uploadStoredFiles();
  });

  $('#cancel_btn').click(function() {
    manualUploader.cancelAll()
  });
  
  if (key == 'patients') {
    document.getElementById("title-import").innerHTML = "Import Data Pasien";
  } else if (key == 'medicines') {
    document.getElementById("title-import").innerHTML = "{{ trans('messages.import_inventory_data') }}";
  } else if (key == 'medicines_incoming') {
    document.getElementById("title-import").innerHTML = "Import Data Obat Masuk";
  }

  function getImportTemplate() {
    $.ajax({
      url: '{{ $api_url }}/{{ $lang }}/get-template?key='+key,
      type: "GET",
      contentType: false,
      success: function(fileUrl){
        var obj = JSON.parse(fileUrl);
        var link = document.createElement('a');
        link.href = "{{ $storage }}/" + obj.resource;
        link.click();
      },
      error: function(){
        notif(false, 'Template Import Tidak Ditemukan.');
      }
    });
  }
</script>
