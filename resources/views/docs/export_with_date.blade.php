<div class="modal fade window" id="export" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/action/export.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.export_inventory_data') }}</h5>
            </div>
            <input type="text" name="export_key" id="export_key" hidden>
            <form id="form-export" class="">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="format-export">
                        <label class="control-label">Format</label>
                        <input type="text" class="form-control" value="Excel (xlsx)" disabled>
                        <span id="loading-code_dpho" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="select-time">
                        <label class="control-label">Waktu</label>
                        <div class="border-group">
                            <select class="form-control" name="time" id="time" placeholder="Pilih waktu" onChange="disableDateInputs()">
                                <option value=""></option>
                                <option value="today">Hari ini</option>
                                <option value="week">Minggu ini</option>
                                <option value="range">Rentang waktu</option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group column-group" id="start-date-group">
                              <label class="control-label">Tanggal</label>
                              <div id="datepicker_start_date" class="input-group date">
                                <input class="form-control" type="text" placeholder="Tanggal minimum" name="start_date" id="start_date" disabled="true" />
                                <span class="input-group-addon">
                                  <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                </span>
                              </div>
                              <br>
                              <span class="help-block"></span>  
                            </div>
                            <div class="form-group column-group" id="end-date-group">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        export
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-export").submit(function() {
  var key = document.getElementById("export_key").value;
  var e = document.getElementById("time");
  var type = e.options[e.selectedIndex].value;
  var startDate = document.getElementById("start_date").value;
  var endDate = document.getElementById("end_date") ? document.getElementById("end_date").value : startDate;
  $.ajax({
    url: "{{ $docs_url }}/{{ $lang }}/download/clinic/{{ $id_clinic }}/export/"+key,
    type: "GET",
    data: { type: type, startDate: startDate, endDate: endDate },
    contentType: false,
    success: function(fileUrl){
      var link = document.createElement('a');
      link.href = fileUrl;
      link.click();
    },
    error: function(){
      notif(false, 'Terjadi Kesalahan Saat Melakukan Export.');
    }
  });
})

$('#form-export #datepicker_start_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$("#form-export select[name='time']").select2({
    placeholder: "Pilih waktu",
    dropdownParent: $('#select-time')
});

if (document.getElementById("export_key").value == 'payments') {
  document.getElementById("exampleModalLongTitle").innerHTML = "Export Data Pembayaran";
} else if (document.getElementById("export_key").value == 'medicines') {
  document.getElementById("exampleModalLongTitle").innerHTML = "{{ trans('messages.export_inventory_data') }}";
}

function disableDateInputs() {
  setMaxDateInputField();
  e = document.getElementById("time");
  if(document.getElementById("end_date")) document.getElementById("end_date").value = "";
  document.getElementById("start_date").value = "";

  if (e.options[e.selectedIndex].value == 'today') {
    document.getElementById("start_date").value = getDateToday();
    document.getElementById("end-date-group").innerHTML = "";
  } else if (e.options[e.selectedIndex].value == 'week') {
    dayRange = getRangeDate();
    document.getElementById("start_date").value = dayRange[0];
    document.getElementById("end_date").value = dayRange[1];
  }

  document.getElementById("start_date").disabled = (e.options[e.selectedIndex].value !== 'range');
  if(document.getElementById("end_date")) document.getElementById("end_date").disabled = (e.options[e.selectedIndex].value !== 'range');
}

function setMaxDateInputField() {
  document.getElementById("end-date-group").innerHTML = 
    "<div id='datepicker_end_date' class='input-group date'>"+
      "<input class='form-control' type='text' placeholder='Tanggal maksimum' name='end_date' id='end_date' disabled='true' />"+
      "<span class='input-group-addon'>"+
        "<img src='{{ asset('assets/images/icons/action/select-date.png') }}' />"+
      "</span>"+
    "</div>"+
    "<br>"+
    "<span class='help-block'></span>";

  $('#form-export #datepicker_end_date').datetimepicker({
      locale: '{{ $lang }}',
      format: 'DD/MM/YYYY'
  });
}

function getDateToday() {
  date = new Date();
  return `${String(date.getDate()).padStart(2, '0')}/${String(date.getMonth() + 1).padStart(2, '0')}/${date.getFullYear()}`;
}

function getRangeDate() {
  curr = new Date; // get current date
  first = curr.getDate() - curr.getDay() + 1; // First day is the day of the month - the day of the week
  last = first + 6; // last day is the first day + 6

  firstday = new Date(curr.setDate(first));
  lastday = new Date(curr.setDate(last));

  firstday = `${String(firstday.getDate()).padStart(2, '0')}/${String(firstday.getMonth() + 1).padStart(2, '0')}/${firstday.getFullYear()}`
  lastday = `${String(lastday.getDate()).padStart(2, '0')}/${String(lastday.getMonth() + 1).padStart(2, '0')}/${lastday.getFullYear()}`

  return [firstday, lastday]
}

</script>