<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i>
                    {{ trans('messages.add_polyclinic') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body hide" id="loading-add">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <form id="form-add" class="">
                <div class="modal-body">
                    <div class="form-group" id="poliBpjs-group">
                        <label class="control-label">Kode Poliklinik BPJS </label>
                        <div class="border-group">
                            <select class="form-control" name="code_bpjs">
                            </select>
                        </div>
                        <span id="loading-content"
                            class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="indonesian_name-group">
                        <label class="control-label">{{ trans('messages.indonesian_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.indonesian_name') }}"
                            name="indonesian_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="english_name-group">
                        <label class="control-label">{{ trans('messages.english_name') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.english_name') }}"
                            name="english_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

    function resetAdd() {
        resetValidation('form-add #code_bpjs', 'form-add #indonesian_name', 'form-add #english_name');
        $("#form-add select[name=code_bpjs]").val("").trigger("change");
        $("#form-add input[name=indonesian_name]").val("");
        $("#form-add input[name=english_name]").val("");
    }

    $('#add').on('show.bs.modal', function (e) {
        resetAdd();
    });

    $('#form-add select[name=code_bpjs]').focus(function () {
        resetValidation('form-add #code_bpjs');
    });

    $('#form-add input[name=indonesian_name]').focus(function () {
        resetValidation('form-add #indonesian_name');
    });

    $('#form-add input[name=english_name]').focus(function () {
        resetValidation('form-add #english_name');
    });

    $("#form-add").submit(function (event) {
        event.preventDefault();
        resetValidation('form-add #code_bpjs', 'form-add #indonesian_name', 'form-add #english_name');

        $("#form-add button").attr("disabled", true);

        formData = new FormData();

        formData.append("validate", false);
        formData.append("kode_bpjs", $("#form-add select[name=code_bpjs] option:selected").val());
        formData.append("indonesian_name", $("#form-add input[name=indonesian_name]").val());
        formData.append("english_name", $("#form-add input[name=english_name]").val());
        formData.append("level", "admin");
        formData.append("user", "{{ $id_user }}");
        $("#form-add .btn-primary").addClass("loading");
        $("#form-add .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/polyclinic",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                if (!data.error) {
                    if (!data.success) {
                        if (data.message) {
                            notif(false, data.message);
                        } else {
                            formValidate(true, ['form-add #indonesian_name', data.errors.indonesian_name, true], ['form-add #english_name', data.errors.english_name, true]);
                        }
                    } else {

                        resetAdd();

                        $("#add").modal("toggle");

                        search(numPage, "false", keySearch);

                        notif(true, "{{ trans('validation.success_add_polyclinic') }}");

                    }
                } else {
                    notif(false, data.message);
                }
            },
            error: function () {
                $("#form-add button").attr("disabled", false);

                $("#form-add .btn-primary").removeClass("loading");
                $("#form-add .btn-primary span").addClass("hide");

                notif(false, "{{ trans('validation.failed') }}");
            }
        })
    });

    function listPoliBPJS() {
        loading_content("#form-add #poliBpjs-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/bpjs/list-poli",
            type: "GET",
            processData: false,
            contentType: false,
            success: function (data) {



                if (!data.error) {
                    loading_content("#form-add #poliBpjs-group", "success");

                    var item = data.list;

                    $("select[name='code_bpjs']").html("<option value=''></option> ");
                    for (var i = 0; i < item.length; i++) {
                        $("select[name='code_bpjs']").append("<option value='" + item[i].kdPoli + "' data-name='" + item[i].nmPoli + "'>" + item[i].kdPoli + " - " + item[i].nmPoli + "</option>");
                    }

                    $("select[name='code_bpjs']").select2({
                        placeholder: "Pilih Poliklinik",
                        allowClear: false
                    });

                } else {
                    loading_content("#form-add #poliBpjs-group", "failed");

                    $("#form-add #poliBpjs-group #loading-content").click(function () { listPoliBPJS(); });
                }
            },
            error: function () {
                loading_content("#form-add #poliBpjs-group", "failed");

                $("#form-add #poliBpjs-group #loading-content").click(function () { listPoliBPJS(); });
            }
        })
    }

    $('#form-add select[name=code_bpjs]').on('change', function () {
        var name = $(this).find(':selected').data('name');
        $("input[name=name]").val(name);
    });

    listPoliBPJS();
</script>