<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-pencil"></i>
                    {{ trans('messages.edit_polyclinic') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="hide">
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="form-group" id="poliBpjs-group">
                        <label class="control-label">Kode Poliklinik BPJS </label>
                        <div class="border-group">
                            <select class="form-control" name="code_bpjs">
                            </select>
                        </div>
                        <span id="loading-content"
                            class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="indonesian_name-group">
                        <label class="control-label">{{ trans('messages.indonesian_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.indonesian_name') }}"
                            name="indonesian_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="english_name-group">
                        <label class="control-label">{{ trans('messages.english_name') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.english_name') }}"
                            name="english_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">

    function resetEdit() {
        resetValidation('form-edit #code_bpjs', 'form-edit #indonesian_name', 'form-edit #english_name');

        $("#form-edit input[name=id]").val("");
        $("#form-edit select[name=code_bpjs]").val("").trigger("change");
        $("#form-edit input[name=indonesian_name]").val("");
        $("#form-edit input[name=english_name]").val("");
    }

    function edit(id) {
        $('#loading-edit').removeClass('hide');
        $('#form-edit').addClass('hide');
        $("#reload-edit").addClass("hide");

        formData = new FormData();

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/polyclinic/" + id + "",
            type: "GET",
            processData: false,
            contentType: false,
            success: function (data) {


                $('#loading-edit').addClass('hide');

                if (!data.error) {
                    if (!data.success) {
                        if (data.message) {
                            notif(false, data.message);
                        } else {
                            formValidate(true, ['form-add #indonesian_name', data.errors.indonesian_name, true], ['form-add #english_name', data.errors.english_name, true]);
                        }
                    } else {
                        $('#form-edit').removeClass('hide');

                        $("#form-edit input[name=id]").val(data.id);
                        $("#form-edit select[name=code_bpjs]").val(data.kode_bpjs).trigger("change");
                        $("#form-edit input[name=indonesian_name]").val(data.indonesian_name);
                        $("#form-edit input[name=english_name]").val(data.english_name);
                    }
                } else {
                    $("#reload-edit").removeClass("hide");
                    $("#reload-edit .reload").click(function () { edit(id); });
                }

            },
            error: function () {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");

                $("#reload-edit .reload").click(function () { edit(id); });
            }
        })
    }

    $('#edit').on('show.bs.modal', function (e) {
        $('#loading-edit').removeClass('hide');
        $('#form-edit').addClass('hide');
        resetEdit();

        var id = $(e.relatedTarget).attr('data-id');
        edit(id);
    });

    $('#form-edit select[name=code_bpjs]').focus(function () {
        resetValidation('form-edit #code_bpjs');
    });

    $('#form-edit input[name=indonesian_name]').focus(function () {
        resetValidation('form-edit #indonesian_name');
    });

    $('#form-edit input[name=english_name]').focus(function () {
        resetValidation('form-edit #english_name');
    });

    $("#form-edit").submit(function (event) {
        event.preventDefault();
        resetValidation('form-edit #code_bpjs', 'form-edit #indonesian_name', 'form-edit #english_name');

        $("#form-edit button").attr("disabled", true);

        var id = $("#form-edit input[name=id]").val();
        formData = new FormData();
        formData.append("_method", "PATCH");
        formData.append("validate", false);
        formData.append("kode_bpjs", $("#form-edit select[name=code_bpjs] option:selected").val());
        formData.append("indonesian_name", $("#form-edit input[name=indonesian_name]").val());
        formData.append("english_name", $("#form-edit input[name=english_name]").val());
        formData.append("level", "admin");
        formData.append("user", "{{ $id_user }}");

        $("#form-edit .btn-primary").addClass("loading");
        $("#form-edit .btn-primary span").removeClass("hide");

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/polyclinic/" + id,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {

                $("#form-edit button").attr("disabled", false);

                $("#form-edit .btn-primary").removeClass("loading");
                $("#form-edit .btn-primary span").addClass("hide");

                if (!data.error) {
                    if (!data.success) {
                        if (data.message) {
                            notif(false, data.message);
                        } else {
                            formValidate(true, ['form-edit #indonesian_name', data.errors.indonesian_name, true], ['form-edit #english_name', data.errors.english_name, true]);
                        }
                    } else {
                        resetEdit();

                        $("#edit").modal("toggle");

                        search(numPage, "false", keySearch);

                        notif(true, "{{ trans('validation.success_edit_polyclinic') }}");
                    }
                } else {
                    notif(false, "{{ trans('validation.failed') }}");
                }
            },
            error: function () {
                $("#form-edit button").attr("disabled", false);

                $("#form-edit .btn-primary").removeClass("loading");
                $("#form-edit .btn-primary span").addClass("hide");

                notif(false, "{{ trans('validation.failed') }}");
            }
        })
    });

    function listPoliBPJS() {
        loading_content("#form-edit #poliBpjs-group", "loading");
        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/bpjs/list-poli",
            type: "GET",
            processData: false,
            contentType: false,
            success: function (data) {



                if (!data.error) {
                    loading_content("#form-edit #poliBpjs-group", "success");

                    var item = data.list;

                    $("select[name='code_bpjs']").html("<option value=''></option> ");
                    for (var i = 0; i < item.length; i++) {
                        $("select[name='code_bpjs']").append("<option value='" + item[i].kdPoli + "' data-name='" + item[i].nmPoli + "'>" + item[i].kdPoli + " - " + item[i].nmPoli + "</option>");
                    }

                    $("select[name='code_bpjs']").select2({
                        placeholder: "Pilih Poliklinik",
                        allowClear: false
                    });

                } else {
                    loading_content("#form-edit #poliBpjs-group", "failed");

                    $("#form-edit #poliBpjs-group #loading-content").click(function () { listPoliBPJS(); });
                }
            },
            error: function () {
                loading_content("#form-edit #poliBpjs-group", "failed");

                $("#form-edit #poliBpjs-group #loading-content").click(function () { listPoliBPJS(); });
            }
        })
    }

    $('#form-edit select[name=code_bpjs]').on('change', function () {
        var name = $(this).find(':selected').data('name');
        $("input[name=name]").val(name);
    });

    listPoliBPJS();
</script>