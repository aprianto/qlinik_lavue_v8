@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 



<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/kelompok.png') }}" />
                    <h3>
                        Kegiatan Kelompok
                    </h3>
                    <a href="#"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/group/activities/create" onclick="loading($(this).attr('href'));">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Tambah Kegiatan</span>
                        </div>
                    </a>
                    <div class="info-sec">
                        <h1 id="total">0</h1>
                        <h3>{{ trans("messages.total") }}</h3>
                    </div>
                </div>
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">
                            <div class="col-md-3 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 form-change">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Cari nama kegiatan" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kegiatan</th>
                                <th class="hide-lg">Jadwal</th>
                                <th class="hide-md">Club Prolanis</th>
                                <th class="hide-md">Materi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>


<script type="text/javascript">
/*
var pusher = new Pusher('{{ $pusher_key }}');

var id_pusher = "";
id_pusher = "{{ $id_clinic }}";

var channelVerification = pusher.subscribe('schedule');
channelVerification.bind('schedule-{{ $level }}-'+id_pusher, function(data) {
    if(data.status.code==true) {
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+'').addClass('row-green');
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' td:first-child').html("<i class='fa fa-check-circle status-success status-text'></i>");
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #link-medical_records').attr("href","#/{{ $lang }}/medical/"+data.id_medical_records);
        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #link-edit').remove();

        $('#schedule_'+data.id_clinic+'_'+data.id_patient+'_'+data.id_doctor+''+data.id_polyclinic+''+data.date+' #status').html("<span class='status-done'>{{ trans('messages.done') }}</span>");
    }
});*/

if("{{Request::is('*group*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#group-bar").addClass('active-bar')
}else {
    $(".side-menus ul li a").removeClass('active-bar')
}

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day})
    });
});

$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day})
        
    });
});



date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='from_date']").val(unconvertDate(from_date_nowRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_date_nowRepo));

var keySearch = searchRepo;
var keyFromDate = from_date_nowRepo;
var keyToDate = to_date_nowRepo;
var numPage = pageRepo;


function search(page, submit, q, from_date, to_date) {
    if(dateNow==$("#form-search input[name='from_date']").val() && dateNow==$("#form-search input[name='to_date']").val()) {
        $("#list-title").html("{{ trans('messages.schedule') }} {{ strtolower(trans('messages.today')) }}");    
    } else {
        $("#list-title").html("{{ trans('messages.schedule') }}");
    }


    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyFromDate = from_date;
    keyToDate = to_date;
    numPage = page;
    order = "asc";

    repository(['search',q],['order',order],['from_date_now',from_date],['to_date_now',to_date],['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/group",
        type: "GET",
        data: "q="+q+"&order="+order+"&from_date="+from_date+"&to_date="+to_date+"&page="+page,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                $("#total").html(total);

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            if(i==0) {
                                tr = $('<tr class="table-header" />');
                                if(item[i].date==convertDate(dateNow)) {
                                    tr.append("<td colspan='5'>{{ trans('messages.today') }}</td>");
                                } else {
                                    tr.append("<td colspan='5'>"+getFormatDate(item[i].date)+"</td>");
                                }
                                //tr.append("<td colspan='5' class='hide-lg'>{{ trans('messages.total_doctor') }}: "+item[i].group_doctor+"</td>");
                                $("#table").append(tr);
                            }

                            var row = "";
                            var status_row = "";

                            var action = "";

                            action = action+"<a href='#/{{ $lang }}/group/activities/"+item[i].id+"' class='btn-table btn-blue' onclick=\"loading($(this).attr('href'));\"><img src=\"{{ asset('assets/images/icons/action/detail.png') }}\" /></a>";

                            action = action+"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /></a>";

                            tr = $('<tr class="'+row+'" />');
                            
                            tr.append("<td><a href='#/{{ $lang }}/group/activities/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].activity + "</h1></a></td>");

                            tr.append("<td class='hide-lg'><span class='text'>" + getFormatDate(item[i].date) +"</span></td>");
                            tr.append("<td class='hide-md'>"+item[i].club_name+"</td>");
                            tr.append("<td class='hide-md'>"+item[i].material+"</td>");

                            //tr.append("<td id='paid' class='hide-lg' align='center'>"+paid+"</td>");

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            if(i<to) {
                                if(item[i].date!=item[i+1].date) {
                                    tr = $('<tr class="table-header" />');
                                    if(item[i+1].date==convertDate(dateNow)) {
                                        tr.append("<td colspan='5'>{{ trans('messages.today') }}</td>");
                                    } else {
                                        tr.append("<td colspan='5'>"+getFormatDate(item[i+1].date)+"</td>");
                                    }
                                    $("#table").append(tr);
                                }   
                            }

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");

                    if(dateNow==$("#form-search input[name='from_date']").val() && dateNow==$("#form-search input[name='to_date']").val()) {
                        tr = $('<tr class="table-header" />');
                        tr.append("<td colspan='5'>{{ trans('messages.today') }}</td>");
                        $("#table").append(tr);
                        //$("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }} {{ strtolower(trans('messages.today')) }}</td></tr>"); 
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");    
                    } else {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                    
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, keyFromDate, keyToDate);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q, keyFromDate, keyToDate); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q, keyFromDate, keyToDate); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/group/destroy/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(1);
                    notif(true,"{{ trans('validation.success_delete_medicine') }}");    
                }else{
                    notif(false,data.message);
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val();
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }
    }
    
    search(1,"true", $("#form-search input[name=q]").val(), convertDate(from_date), convertDate(to_date));
}

$("#form-search").submit(function(event) {
    search(1,"true", $("#form-search input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
});

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            filterSearch();
        }, 2000);
    }
})

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("#form-search input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("#form-search input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()));
    }
});

search(pageRepo,"false",searchRepo,from_date_nowRepo, to_date_nowRepo);

</script>
@include('_partial.confirm_delele')