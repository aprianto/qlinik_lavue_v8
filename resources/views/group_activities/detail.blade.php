@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row" id="section-loading">
        <div class="col-md-6">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-8">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        Daftar Peserta
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a data-toggle="modal" id="resetBPJS" data-target="#checkBPJSOnly">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>Tambah Peserta</span>
                        </div>
                    </a>
                    <div class="info-sec">
                        <h1 id="total">0</h1>
                        <h3>{{ trans("messages.total") }}</h3>
                    </div>
                </div>
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">
                            <div class="col-md-12">
                                <div class="form-group search-group label-group">
                                    <label class="control-label">&nbsp;</label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="Cari nama peserta" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Kegiatan</th>
                                <th class="hide-lg">Tgl. Lahir</th>
                                <th class="hide-md">Jenis Peserta</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="widget widget-detail">
                <div class="user-sec">
                    <div class="widget-title">
                        <h3>Info Kegiatan</h3>
                    </div>
                    <ul id="section-clinic" class="client-list">
                        <li class="first-sec">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/consult-date.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.date') }}</span>
                                    <h3 id="date"></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>{{ trans('messages.clinic') }}</span>
                                    <h3><a title="" id="clinic"></a></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/doctor-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>Kegiatan</span>
                                    <h3 id="activity"></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/doctor-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span>Club Prolanis</span>
                                    <h3 id="club"></h3>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script type="text/javascript">

$(function() {
    $(".line").peity("line", {
      fill: ["#f2f7f9"],
      height: ["27px"],
      width: ["100"],
      stroke: ["#7dc9f0"]
    })

});


var copyright = false;
var id_edu = '';

function detail(id) {
    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $('#section-detail').addClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/group/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');
                $('#section-detail').removeClass('hide');

                $("#section-detail #date").html(getFormatDate(data.data.date));

                $("#section-detail #clinic").attr("href","#/{{ $lang }}/clinic/"+data.data.id_clinic);
                $("#section-detail #clinic").attr("onclick","loading($(this).attr('href'))");
                $("#section-detail #clinic").html(data.data.name_clinic);
                
                $("#section-detail #activity").html(data.data.activity + '<br />' + data.data.material);
                $("#section-detail #club").html(data.data.club_name);
                id_edu = data.data.id_edu;

                search(1, '');
                
            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-group/destroy/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(1);
                    notif(true,"{{ trans('validation.success_delete_medicine') }}");    
                }else{
                    notif(false,data.message);
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$("#form-search").submit(function(event) {
    search(1, $("#form-search input[name=q]").val());
});

function search(page, q) {

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    repository(['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/group/{{ $id }}/patients",
        type: "GET",
        data: "name="+q+"&page="+page,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                $("#total").html(total);

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            var row = "";
                            var action = "";
                            var gender = "";
                            if(item[i].patient_gender == "P"){
                                gender = "Perempuan";
                            }else{
                                gender = "Laki-laki";
                            }

                            action = action+"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /></a>";

                            tr = $('<tr class="'+row+'" />');
                            
                            tr.append("<td><a href='#/{{ $lang }}/group/activities/"+item[i].id_patient+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].patient_name + "</h1></a><span class='text'>"+gender+"</span><br /><span class='text'>"+getAge(item[i].patient_birthdate.replace(/\-/g,'/'))+" {{ trans('messages.year_age') }}</span></td>");

                            tr.append("<td class='hide-lg'><span class='text'>" + getFormatDate(item[i].patient_birthdate) +"</span></td>");
                            tr.append("<td class='hide-md'><a><h1>Peserta</h1></a><span class='text'>"+item[i].participant_type+"</span></td>");
                            //tr.append("<td id='paid' class='hide-lg' align='center'>"+paid+"</td>");

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");
                    $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                }

                pages(page, total, per_page, current_page, last_page, from, to);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, q); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, q); });
        }
    })
}

detail("{{ $id }}");

</script>

@include('group_activities.bpjs')
@include('_partial.confirm_delele')