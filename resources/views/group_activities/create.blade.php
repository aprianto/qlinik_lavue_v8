@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        Tambah Kegiatan Kelompok
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-add">
                        <div class="form-group column-group" id="date-group">
                            <label class="control-label">Tanggal <span>*</span></label>
                            <div id="datepicker_date" class="input-group date">
                                <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="date" />
                                <span class="input-group-addon">
                                    <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                </span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group column-group" id="type_group-group">
                            <label class="control-label">Jenis Kelompok <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="type_group">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="type_club-group">
                            <label class="control-label">Club Prolanis</label>
                            <div class="border-group">
                                <select class="form-control" name="type_club" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="type_activities-group">
                            <label class="control-label">Jenis Kegiatan <span>*</span></label>
                            <div class="sf-radio" id="type_activities"> 
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="material-group">
                            <label class="control-label">Materi <span>*</span></label>
                            <input type="text" class="form-control" placeholder="Masukkan materi kegiatan" name="material">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="speaker-group">
                            <label class="control-label">Pembicara / Pemateri <span>*</span></label>
                            <input type="text" class="form-control" placeholder="Masukkan nama pembicara, pemateri, atau narasumber" name="speaker">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="location-group">
                            <label class="control-label">Lokasi <span>*</span></label>
                            <input type="text" class="form-control" placeholder="Masukkan lokasi kegiatan" name="location">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="price-group">
                            <label class="control-label">Biaya (Rp) </label>
                            <input type="text" class="form-control" placeholder="Masukkan biaya kegiatan" name="price"" onkeydown="number(event)">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Keterangan</label>
                            <textarea type="text" class="form-control" placeholder="Masukkan keterangan kegiatan" name="info" />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ ucwords(trans('messages.save')) }}
                            </button>
                            <a href="#/{{ $lang }}/group/activities" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$('#datepicker_date').datetimepicker({
    locale: "{{ $lang }}",
    viewMode: 'years',
    format: 'DD/MM/YYYY',
});

$("#form-add select[name='type_club']").select2({
    placeholder: "Pilih jenis club",
    allowClear: false
});

function listTypeGroup(val) {
    loading_content("#form-add #type_group-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-jenis-kelompok",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #type_group-group", "success");

                var item = data.list;

                $("#form-add select[name='type_group']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='type_group']").append("<option value='"+item[i].kdProgram+"'>"+item[i].nmProgram+"</option>");
                }

                $("#form-add select[name='type_group']").select2({
                    placeholder: "Pilih jenis kelompok",
                    allowClear: false
                });

                
            } else {
                loading_content("#form-add #type_group-group", "failed");

                $("#form-add #type_group-group #loading-content").click(function(){ listTypeGroup(val); });
            }
        },
        error: function(){
            loading_content("#form-add #type_group-group", "failed");

            $("#form-add #type_group-group #loading-content").click(function(){ listTypeGroup(val); });
        }
    })
}

listTypeGroup('');

$('#form-add select[name=type_group]').on('change', function() {
    var value = this.value;

    listTypeClub(value);
});

function listTypeClub(val) {
    loading_content("#form-add #type_club-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-kelompok-club/"+val,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #type_club-group", "success");

                var item = data.list;

                $("#form-add select[name='type_club']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='type_club']").append("<option value='"+item[i].clubId+"'>"+item[i].nama+" ("+item[i].jnsKelompok.nmProgram+")</option>");
                }

                $("#form-add select[name='type_club']").select2({
                    placeholder: "Pilih jenis club",
                    allowClear: false
                });

                
            } else {
                loading_content("#form-add #type_club-group", "failed");

                $("#form-add #type_club-group #loading-content").click(function(){ listTypeClub(val); });
            }
        },
        error: function(){
            loading_content("#form-add #type_club-group", "failed");

            $("#form-add #type_club-group #loading-content").click(function(){ listTypeClub(val); });
        }
    })
}

function listTypeActivities(val) {
    loading_content("#form-add #type_activities-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-kegiatan-kelompok",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            


            if(!data.error) {
                loading_content("#form-add #type_activities-group", "success");

                var item = data.list;

                $("#form-add #type_activities").html("");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add #type_activities").append("<label><input type='radio' data-text='"+item[i].nmProgram+"' value='"+item[i].kdProgram+"' name='type_activities'><span></span> "+item[i].nmProgram+"</label>");
                }

            } else {
                loading_content("#form-add #type_activities-group", "failed");

                $("#form-add #type_activities-group #loading-content").click(function(){ listTypeActivities(val); });
            }
        },
        error: function(){
            loading_content("#form-add #type_activities-group", "failed");

            $("#form-add #type_activities-group #loading-content").click(function(){ listTypeActivities(val); });
        }
    })
}

listTypeActivities('');

$("#form-add").submit(function(event) {
    resetValidation('form-add #date', 'form-add #type_group', 'form-add #type_club', 'form-add #type_activities', 'form-add #material', 'form-add #speaker', 'form-add #location', 'form-add #price');

    event.preventDefault();
    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    var type_activities = $("#form-add input[name='type_activities']:checked").val();
    if(type_activities==undefined) {
        type_activities="";
    }

    formData.append("id_clinic", '');
    formData.append("date", convertDate($("#form-add input[name='date']").val()));
    formData.append("code_bpjs_group", $("#form-add select[name='type_group'] option:selected").val());
    formData.append("id_club_bpjs", $("#form-add select[name='type_club'] option:selected").val());
    formData.append("code_bpjs_activity", type_activities);
    formData.append("name", $("#form-add select[name='type_group'] option:selected").html());
    formData.append("club_name", $("#form-add select[name='type_club'] option:selected").html());
    formData.append("activity", $("#form-add input[name='type_activities']:checked").attr('data-text'));
    formData.append("material", $("#form-add input[name='material']").val());
    formData.append("speaker", $("#form-add input[name='speaker']").val());
    formData.append("location", $("#form-add input[name='location']").val());
    formData.append("description", $("#form-add textarea[name='info']").val());
    formData.append("cost", parseInt(($("#form-add input[name='price']").val() == '' ? 0 : $("#form-add input[name='price']").val())));

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/group",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            
            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    if(data.errors){
                        formValidate(true, ['form-add #date',data.errors.date, true], ['form-add #type_group',data.errors.code_bpjs_group, true], ['form-add #type_activities',data.errors.code_bpjs_activity, true], ['form-add #material',data.errors.material, true], ['form-add #speaker',data.errors.speaker, true], ['form-add #location',data.errors.location, true]);
                    }

                    if(data.message) {
                        notif(false,""+data.message+"");
                    }
                } else {
                
                    $("#form-add input[name='date']").val("");
                    $("#form-add select[name='type_group']").val("").trigger("change");
                    $("#form-add select[name='type_club']").val("").trigger("change");
                    $('#form-add input[name="type_activities"]:checked').each(function(){
                        $(this).prop('checked', false); 
                    });
                    $("#form-add input[name='material']").val("");
                    $("#form-add input[name='speaker']").val("");
                    $("#form-add input[name='location']").val("");
                    $("#form-add input[name='price']").val("");
                    $("#form-add textarea[name='info']").val("");

                    notif(true,"Kegiatan kelompok berhasil ditambahkan.");

                    setTimeout(function(){
                        loading('#/{{ $lang }}/group/activities/'+data.data.id+"");
                        redirect('#/{{ $lang }}/group/activities/'+data.data.id+"");                    
                    },500);
                }
        
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>