<div class="modal fade window-confirm auto-center" id="checkBPJSOnly" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Peserta</h1>
                </div>
                <form class="sec form-bpjs" role="form" style="margin-top: 25px;" id="formCheckBpjs">
                    <div class="form-group" id="nik-group">
                        <label class="control-label" style="font-size: 12px;">Nomor kartu BPJS / NIK (KTP/KIA) <span>*</span></label>
                        <input type="number" class="form-control" style="font-size: 14px;" placeholder="Masukkan nomor kartu" name="nik">
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-primary" id="confirm">Cek Data</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade window-confirm auto-center" id="showBPJSOnly" role="dialog" aria-labelledby="confirmAcceptLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0 0!important;">
                <div class="center">
                    <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                    <h1>Tambah Peserta</h1>
                </div>
                <ul class="detail-profile" style="margin-top: 27px;">
                    <div id="detail-profile">
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">NIK</span>
                                    <span id="patient_nik"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/id-no.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">No BPJS Kesehatan</span>
                                    <span id="patient_bpjs"></span>
                                    <span>Status: <span id="patient_status" style="display: initial; color: #24E2CF;"></span></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Nama</span>
                                    <span id="patient_name"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/clinic-name.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Faskes 1</span>
                                    <span id="patient_facility"></span>
                                </div>
                            </div>
                        </li>
                        <li class="profile-features">
                            <div class="detail-sec">
                                <div class="icon-sec">
                                    <img src="{{ asset('assets/images/icons/info/patient-cat.png') }}" />
                                </div>
                                <div class="info-sec">
                                    <span class="title">Kelas</span>
                                    <span id="patient_category"></span>
                                </div>
                            </div>
                        </li>                    
                    </div>
                </ul>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" id="create">Tambah Peserta</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#resetBPJS").on('click', function(){
    $("#formCheckBpjs")[0].reset();
});

$('#checkBPJSOnly').find('.modal-footer #confirm').on('click', function(){
    var id = $("#checkBPJSOnly input[name=nik]").val();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/find-peserta-with/"+id,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
            if(!data.error) {
                if(data.success==true) {
                    if(data.data.aktif==false){
                        $("#showBPJSOnly").find(".modal-footer #create").addClass('hide');
                        notif(false,data.data.ketAktif);
                    }else{
                        $("#showBPJSOnly").find(".modal-footer #create").removeClass('hide');
                        if(id.length != 16){
                            $("#showBPJSOnly #patient_nik").html(data.data.noKTP);
                        }else{
                            $("#showBPJSOnly #patient_nik").html(id);
                        }
                        $("#showBPJSOnly #patient_bpjs").html(data.data.noKartu);
                        $("#showBPJSOnly #patient_name").html(data.data.nama);
                        $("#showBPJSOnly #patient_facility").html(data.data.kdProviderPst.nmProvider);
                        $("#showBPJSOnly #patient_category").html(data.data.jnsKelas.nama);
                        $("#showBPJSOnly #patient_status").html(data.data.ketAktif);
                        $("#checkBPJSOnly").modal("toggle");
                        $("#showBPJSOnly").modal("toggle");
                    }
                }else{
                    notif(false,data.message);
                }
            } else {
                notif(false,data.message);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

$("#showBPJSOnly").find("#create").on('click', function(){
    var id = $("#checkBPJSOnly input[name=nik]").val();
    formData= new FormData();
    
    formData.append("id_group", "{{ $id }}");
    formData.append("id_edu", id_edu);
    formData.append("card_number", id);
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/patient-group",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    $("#showBPJSOnly").modal('hide');
                    $('.modal-backdrop').remove();
                    $('body').removeClass("modal-open");
                    search(1, $("#form-search input[name=q]").val());
                    notif(true,"Peserta berhasil ditambahkan.");
                }else{
                    notif(false,data.message);
                }
            } else {
                notif(false,data.message);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>
