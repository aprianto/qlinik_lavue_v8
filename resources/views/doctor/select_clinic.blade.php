<div class="modal fade" id="selectClinic" role="dialog" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document" style="top: 27%;">
        <div class="modal-content">
            <div class="modal-body p-5">
                <div class="modal-body-header-login">
                    <img src="{{asset('assets/images/Group.png')}}" />
                    <h4>{{trans ('messages.choose_location_practice')}}</h5>
                </div>
                <div id="form-search">
                    <div>
                        <div class="form-group content-column column-group" id="polyclinic-group">
                        <span class="location-practice">{{trans ('messages.location_practice')}}</span>    
                        <div class="border-group">
                                <select class="form-control" name="polyclinic" id="select" >
                                    <option value=""></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: 0px;">
                <a class=" button-select-clinic-cancel" data-dismiss="modal">{{ trans('messages.cancel') }}</a>
                <button type="button" class=" button-select-clinic-in" onclick="login()" id="confirm">{{ trans('messages.login') }}</button>
            </div>
        </div>
    </div>
</div>
<script>

function login () {
    var id_clinic = $('#select').find(":selected").val();
    var id_polyclinic = $('#select').find(':selected').attr('id');
    var name_polyclinic = $('#select').find(':selected').attr('name')

    if(id_clinic != ''){
        queryParam = '?id_clinic=' + id_clinic + '&id_polyclinic=' + id_polyclinic + '&name_polyclinic=' + name_polyclinic;
        redirect("{{$lang}}/catcher" + queryParam);

    }else {
        $('.select2-container--default .select2-selection--single ').css('border-bottom', '1px solid red');
        $('.select2-container--default .select2-selection--single .select2-selection__placeholder').css('color', 'red');
    }
}
</script>