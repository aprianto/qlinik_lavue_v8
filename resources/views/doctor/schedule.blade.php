@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.doctor_schedule') }}
                    </h3>
                </div>
                <div class="cell hide" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec">
                        <input type="hidden" name="id" value="">
                        <div class="form-group" id="polyclinic-group">
                            <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="polyclinic">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="doctor-group">
                            <label class="control-label">{{ trans('messages.doctor') }} <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="doctor" disabled="disabled">
                                    <option value="">{{ trans('messages.no_doctor') }}</option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>                     
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label header-label">{{ trans('messages.active_period') }} <span>*</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group date-validate" id="start_date-group">
                                        <label class="control-label">{{ trans('messages.from') }} <span>*</span></label>
                                        <div id="datepicker_start_date" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="start_date" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                        <span class="help-block"></span>  
                                    </div>                         
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group date-validate" id="end_date-group">
                                        <label class="control-label">{{ trans('messages.until') }}</label>
                                        <div id="datepicker_end_date" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="end_date" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>                                
                                </div>
                            </div>
                        </div>

                        <h3 class="group-label">{{ trans('messages.days_and_hours_practice') }}</h3>

                        <?php 
                        $day = array('monday','tuesday','wednesday','thursday','friday','saturday','sunday');

                        foreach ($day as $key => $value) { 
                            $number_day = $key+1; 
                            ?>
                            <div class="form-group item-group">
                                <div class="row row-padding-left">
                                    <div class="col-md-4">
                                       <div class="sf-check">
                                            <label><input type="checkbox" value="{{ $number_day }}" name="day[{{ $key }}]"><span></span> <?php echo ucwords(trans('messages.'.$value)); ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 hide" id="section-day[{{ $key }}]">
                                        <div id="item-day[{{ $key }}]"></div>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10">
                                                <a class="btn-add-item" onclick="addItemTime('{{ $key }}','','','','','','')">
                                                    + {{ trans("messages.add_another_hour") }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>




                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="group-label">{{ trans('messages.leave') }}</h3>
                                </div>
                            </div>
                            <div class="row">                      
                                <div class="col-md-6">
                                    <div class="form-group date-validate" id="start_date_leave-group">
                                        <label class="control-label">{{ trans('messages.from') }}</label>
                                        <div id="datepicker_start_date_leave" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="start_date_leave" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group date-validate" id="end_date_leave-group">
                                        <label class="control-label">{{ trans('messages.until') }}</label>
                                        <div id="datepicker_end_date_leave" class="input-group date">
                                            <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="end_date_leave" />
                                            <span class="input-group-addon">
                                                <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                            </span>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/doctor" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

$('#datepicker_start_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$('#datepicker_end_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$('#datepicker_start_date_leave').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$('#datepicker_end_date_leave').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$("select[name=doctor]").select2({
    placeholder: "{{ trans('messages.no_doctor') }}",
    allowClear: false
});


var get_id_polyclinic = "";
var get_id_doctor = "";

var itemTime = [];

itemTime[0] = 0;
itemTime[1] = 0;
itemTime[2] = 0;
itemTime[3] = 0;
itemTime[4] = 0;
itemTime[5] = 0;
itemTime[6] = 0;


date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

function removeItemTime(section) {
    $("div[id='"+section+"']").remove();
}

function addItemTime(day, id, start_time, end_time, quota, appointment, estimate){

    if(quota==0) {
        quota = "";
    }

    var check_appointment = "";
    if(appointment==1 || appointment=="true") {
        check_appointment = "checked";
    }

    if(estimate=="") {
        estimate = 30;
    }

    $("div[id='item-day["+day+"]']").append('<div id="item-time['+day+']['+itemTime[day]+']" class="sub-item-group">'
        +'<input type="hidden" name="id_time['+day+']['+itemTime[day]+']" value="'+id+'"><div class="row">'
        +'<div class="col-md-1"></div>'
        +'<img src=\'{{ asset("assets/images/icons/misc/delete.png") }}\' class="remove-icon" onclick="removeItemTime(\'item-time['+day+']['+itemTime[day]+']\')" />'
        //+'<span class="fa fa-times remove" onclick="removeItemTime(\'item-time['+day+']['+itemTime[day]+']\')"></span>'
        +'<div class="col-md-5">'
            +'<div class="form-group date-validate column-group" id="start_time_'+day+'_'+itemTime[day]+'-group">'
                +'<label class="control-label">{{ trans("messages.start_hour") }} <span>*</span></label>'
                +'<div id="timepicker_start_time['+day+']['+itemTime[day]+']" class="input-group date">'
                    +'<input  class="form-control" data-format="HH:mm" type="text" placeholder=\'{{ trans("messages.input_hour") }}\' name="start_time['+day+']['+itemTime[day]+']" value="'+start_time+'" />'
                    +'<span class="input-group-addon">'
                        +'<img src=\'{{ asset("assets/images/icons/action/select-hour.png") }}\' />'
                    +'</span>'
                +'</div>'
                +'<span class="help-block"></span>'
            +'</div>'
        +'</div>'
        +'<div class="col-md-5">'
            +'<div class="form-group date-validate column-group" id="end_time_'+day+'_'+itemTime[day]+'-group">'
                +'<label class="control-label">{{ trans("messages.end_hour") }} <span>*</span></label>'
                +'<div id="timepicker_end_time['+day+']['+itemTime[day]+']" class="input-group date">'
                    +'<input  class="form-control" data-format="HH:mm" type="text" placeholder=\'{{ trans("messages.input_hour") }}\' id="end_time['+day+']['+itemTime[day]+']" name="end_time['+day+']['+itemTime[day]+']" value="'+end_time+'" />'
                    +'<span class="input-group-addon">'
                        +'<img src=\'{{ asset("assets/images/icons/action/select-hour.png") }}\' />'
                    +'</span>'
                +'</div>'
                +'<span class="help-block"></span>'
            +'</div>'                                  
        +'</div>'
    +'</div>'
    +'<div class="row">'
        +'<div class="col-md-1"></div>'
        +'<div class="col-md-5">'
            +'<div class="form-group no-notif" id="quota_'+day+'_'+itemTime[day]+'-group">'
                +'<label class="control-label">{{ trans("messages.quota") }}</label>'
                +'<input type="text" class="form-control" placeholder=\'{{ trans("messages.input_number") }}\' name="quota['+day+']['+itemTime[day]+']" onkeydown="number(event)" value="'+quota+'" maxlength="3" />'
                +'<span class="help-block"></span>'
            +'</div>'
        +'</div>'
        +'<div class="col-md-5">'
            +'<div class="form-group no-notif" id="estimate_'+day+'_'+itemTime[day]+'-group">'
                +'<label class="control-label">{{ trans("messages.estimate_schedule") }} ({{ trans("messages.minutes") }})</label>'
                +'<input type="text" class="form-control" placeholder=\'{{ trans("messages.input_number") }}\'  name="estimate['+day+']['+itemTime[day]+']" onkeydown="number(event)" value="'+estimate+'" maxlength="3" />'
                +'<span class="help-block"></span>'
            +'</div>'
        +'</div>'
    +'</div>'
    +'<div class="row">'
        +'<div class="col-md-1"></div>'
        +'<div class="col-md-10">'
            +'<div class="form-group column-form last-item">'
                +'<div class="sf-check">'
                    +'<label><input type="checkbox" name="appointment['+day+']['+itemTime[day]+']" '+check_appointment+'><span></span> {{ trans("messages.set_appointment") }}</label>'
                +'</div>'
            +'</div>'
        +'</div>'
    +'</div></div>');


    $("div[id='item-day["+day+"]']").append("<script>"
        +"$('div[id=\"timepicker_start_time["+day+"]["+itemTime[day]+"]\"]').datetimepicker({format: 'HH:mm'});"
        +"$('div[id=\"timepicker_end_time["+day+"]["+itemTime[day]+"]\"]').datetimepicker({format: 'HH:mm'});"
        +"$('input[name=\"start_time["+day+"]["+itemTime[day]+"]\"]').focus(function(event) { resetValidation('start_time_"+day+"_"+itemTime[day]+"');});"
        +"$('input[name=\"end_time["+day+"]["+itemTime[day]+"]\"]').focus(function(event) { resetValidation('end_time_"+day+"_"+itemTime[day]+"');});"
        +"$('input[name=\"quota["+day+"]["+itemTime[day]+"]\"]').focus(function(event) { resetValidation('quota_"+day+"_"+itemTime[day]+"');});"
        +"<\/script>");

    itemTime[day]++;
}

function resetSchedule() {
    $("input[name=id]").val("");
    $("input[name=start_date]").val(dateNow);
    $("input[name=end_date]").val("");
    $("input[name=start_date_leave]").val("");
    $("input[name=end_date_leave]").val("");

    @foreach($day as $key => $value)
        $('input[name="day[{{ $key }}]"]').prop('checked',false);
        $("div[id='item-day[{{ $key }}]']").html('');
        $("div[id='section-day[{{ $key }}]']").addClass("hide");
        itemTime['{{ $key }}'] = 0;
    @endforeach
}

function listPoly(val) {
    loading_content(".form-elements-sec #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content(".form-elements-sec #polyclinic-group", "success");

                $("select[name='polyclinic']").html("<option value=''></option> ");
                $("select[name='polyclinic']").append("<option value='all'>{{ trans('messages.all_polyclinic') }}</option> ");

                var item = data.data;
                for (var i = 0; i < item.length; i++) {
                    $("select[name=polyclinic]").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("select[name=polyclinic]").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("select[name='polyclinic']").val(val).trigger("change");
                }

            } else {
                loading_content(".form-elements-sec #polyclinic-group", "failed");

                $(".form-elements-sec #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content(".form-elements-sec #polyclinic-group", "failed");

            $(".form-elements-sec #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    });
}

function listDoctor(polyclinic, val) {
    loading_content(".form-elements-sec #doctor-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor?id_polyclinic="+polyclinic+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataDoctorPoly){

            if(!dataDoctorPoly.error) {
                loading_content(".form-elements-sec #doctor-group", "success");

                var itemDoctorPoly = dataDoctorPoly.data;

                if(itemDoctorPoly.length>0) {
                    $("select[name=doctor]").html("");
                    $("select[name=doctor]").prop("disabled", false);
                    $("select[name=doctor]").append("<option value=''></option>");

                    for (var i = 0; i < itemDoctorPoly.length; i++) {
                        $("select[name=doctor]").append("<option value='"+itemDoctorPoly[i].id+"'>"+itemDoctorPoly[i].name+"</option>");
                    }

                    $("select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.select_doctor') }}",
                        allowClear: false
                    });

                    if(val!="") {
                        $("select[name='doctor']").val(val).trigger("change");
                    }
                } else {
                    $("select[name=doctor]").html("");
                    $("select[name=doctor]").prop("disabled", true);
                    $("select[name=doctor]").append("<option value=''></option>");
                    $("select[name=doctor]").select2({
                        placeholder: "{{ trans('messages.no_doctor') }}",
                        allowClear: false
                    });

                    resetSchedule();
                }
            } else {
                loading_content(".form-elements-sec #doctor-group", "failed");

                $(".form-elements-sec #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
            }
        },
        error: function(){
            loading_content(".form-elements-sec #doctor-group", "failed");

            $(".form-elements-sec #doctor-group #loading-content").click(function(){ listDoctor(polyclinic, val); });
        }
    });
}

function getDoctor(val, get) {
    loading_content(".form-elements-sec #doctor-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+val+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataDoctor){

            if(!dataDoctor.error) {
                loading_content(".form-elements-sec #doctor-group", "success");

                get_id_polyclinic = dataDoctor.id_polyclinic;
                get_id_doctor = dataDoctor.id;

                if(get==true) {
                    listPoly(dataDoctor.id_polyclinic);
                    listDoctor(dataDoctor.id_polyclinic, dataDoctor.id);
                }

                $("input[name=id]").val("");
                $("input[name=start_date]").val(dateNow);
                $("input[name=end_date]").val("");
                $("input[name=start_date_leave]").val("");
                $("input[name=end_date_leave]").val("");

                @foreach($day as $key => $value)
                    $('input[name="day[{{ $key }}]"]').prop('checked',false);
                    $("div[id='item-day[{{ $key }}]']").html('');
                    $("div[id='section-day[{{ $key }}]']").addClass("hide");
                    itemTime['{{ $key }}'] = 0;
                @endforeach

                if(dataDoctor.schedule.id!=null) {
                    $("input[name=id]").val(dataDoctor.schedule.id);
                    $("input[name=start_date]").val(unconvertDate(dataDoctor.schedule.start_date_active));

                    if(dataDoctor.schedule.end_date_active!="0000-00-00" && dataDoctor.schedule.end_date_active!="9999-12-31") {
                        $("input[name=end_date]").val(unconvertDate(dataDoctor.schedule.end_date_active));
                    }
                    

                    if(dataDoctor.schedule.start_date_leave!="0000-00-00" && dataDoctor.schedule.start_date_leave!="9999-12-31") {
                        $("input[name=start_date_leave]").val(unconvertDate(dataDoctor.schedule.start_date_leave));
                    }

                    if(dataDoctor.schedule.end_date_leave!="0000-00-00" && dataDoctor.schedule.end_date_leave!="9999-12-31") {
                       $("input[name=end_date_leave]").val(unconvertDate(dataDoctor.schedule.end_date_leave));
                    }

                    itemSchedule = dataDoctor.schedule.daily;

                    for (var i = 0; i < itemSchedule.length; i++) {
                        day = itemSchedule[i].day-1;
                        $('input[name="day['+day+']"]').prop('checked',true);
                        $("div[id='section-day["+day+"]']").removeClass("hide");
                        addItemTime(''+day+'',''+itemSchedule[i].id+'',''+itemSchedule[i].start_time+'',''+itemSchedule[i].end_time+'',''+nullToEmpty(itemSchedule[i].quota)+'', ''+itemSchedule[i].appointment+'',''+nullToZero(itemSchedule[i].estimate)+'');
                    }
                } else {
                    $("input[name=id]").val("");
                }
                
            } else {
                loading_content(".form-elements-sec #doctor-group", "failed");

                $(".form-elements-sec #doctor-group #loading-content").click(function(){ getDoctor(val, get); });
            }
        },
        error: function(){
            loading_content(".form-elements-sec #doctor-group", "failed");

            $(".form-elements-sec #doctor-group #loading-content").click(function(){ getDoctor(val, get); });
        }
    });
}

$('select[name=polyclinic]').on('change', function() {
    var polyclinic = this.value;
    if(polyclinic=="all") {
        polyclinic = "";
    } else if(polyclinic=="") {
        polyclinic = "null";
    }
    
    if(polyclinic==get_id_polyclinic || polyclinic=="") {
        listDoctor(polyclinic, get_id_doctor);
    } else {
        listDoctor(polyclinic, '');
    }
});

$('select[name=doctor]').on('change', function() {
    var doctor = this.value;

    if(doctor!="") {
        getDoctor(doctor, '');
    } else {
        resetSchedule();
    }
});

@if($id_doctor=="")
listPoly('');
$("input[name=start_date]").val(dateNow);
@else
getDoctor('{{ $id_doctor }}', true);
@endif

$("#polyclinic-group .border-group").click(function() {
    resetValidation('polyclinic');
});

$("#doctor-group .border-group").click(function() {
    resetValidation('doctor');
});

$('input[name=start_date]').focus(function(event) {
    resetValidation('start_date');
});

$('input[name=end_date]').focus(function(event) {
    resetValidation('end_date');
});

$('input[name=start_date_leave]').focus(function(event) {
    resetValidation('start_date_leave');
});

$('input[name=end_date_leave]').focus(function(event) {
    resetValidation('end_date_leave');
});

@foreach($day as $key => $value)
    $('input[name="day[{{ $key }}]"]').click(function() {
        var checked = this.checked;
        if(checked==true) {
            $("div[id='section-day[{{ $key }}]']").removeClass("hide");

            if($("div[id='item-day[{{ $key }}]']").html()=="") {
                addItemTime('{{ $key }}','','','','','','');
            }
        } else {
            $("div[id='section-day[{{ $key }}]']").addClass("hide");        
        }
    });
@endforeach

$("form").submit(function(event) {
    resetValidation('polyclinic','doctor','start_date','end_date','start_date_leave','end_date_active');

    var error = false;
    var scrollValidate = true;

    var polyclinic = $('select[name=polyclinic] option:selected').val();
    if(polyclinic=='') {
        formValidate(scrollValidate, ['polyclinic','{{ trans("validation.empty_polyclinic") }}', false]);
        error = true;
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        resetValidation('polyclinic');
    }
    
    var doctor = $('select[name=doctor] option:selected').val();
    if(doctor=='') {
        formValidate(scrollValidate, ['doctor','{{ trans("validation.empty_doctor") }}', false]);
        error = true;
        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        resetValidation('doctor');
    }

    var start_date = $('input[name=start_date]').val();
    if(start_date=='') {
        formValidate(scrollValidate, ['start_date','{{ trans("validation.empty_start_date") }}', false]);
        error = true;

        if(scrollValidate==true) {
            scrollValidate = false;
        }
    } else {
        resetValidation('start_date');
    }

    var end_date = $('input[name=end_date]').val();

    if(start_date!='' && end_date!='') {
        

        var split_start_date = start_date.split("/");
        check_start_date = Date.parse(split_start_date[2]+'/'+split_start_date[1]+'/'+split_start_date[0]);

        var split_end_date = end_date.split("/");
        check_end_date = Date.parse(split_end_date[2]+'/'+split_end_date[1]+'/'+split_end_date[0]);

        if(check_start_date>=check_end_date) {
            formValidate(scrollValidate, ['end_date','{{ trans("validation.invalid_start_end_date") }}', false]);
            error = true;
            if(scrollValidate==true) {
                scrollValidate = false;
            }
        } else {
            resetValidation('end_date');
        }
    }

    var start_date_leave = $('input[name=start_date_leave]').val();
    var end_date_leave = $('input[name=end_date_leave]').val();

    if(end_date_leave!='') {
        if(start_date_leave=='') {
            formValidate(scrollValidate, ['start_date_leave','{{ trans("validation.empty_start_date") }}', false]);
            error = true;
            if(scrollValidate==true) {
                scrollValidate = false;
            }
        } else {
            resetValidation('start_date_leave');
        }
    }

    if(start_date_leave!='') {
        if(end_date_leave=='') {
            formValidate(scrollValidate, ['end_date_leave','{{ trans("validation.empty_end_date") }}', false]);
            error = true;
            if(scrollValidate==true) {
                scrollValidate = false;
            }
        } else {
            resetValidation('end_date_leave');
        }
    }

    if(start_date_leave!='' && end_date_leave!='') {
        var split_start_date_leave = start_date_leave.split("/");
        check_start_date_leave = Date.parse(split_start_date_leave[2]+'/'+split_start_date_leave[1]+'/'+split_start_date_leave[0]);

        var split_end_date_leave = end_date_leave.split("/");
        check_end_date_leave = Date.parse(split_end_date_leave[2]+'/'+split_end_date_leave[1]+'/'+split_end_date_leave[0]);

        if(check_start_date_leave>=check_end_date_leave) {
            formValidate(scrollValidate, ['end_date_leave','{{ trans("validation.invalid_start_end_date") }}', false]);
            error = true;
            if(scrollValidate==true) {
                scrollValidate = false;
            }
        } else {
            resetValidation('end_date_leave');
        }
    }

    var scheduleTime = [[]];

    numScheduleTime = 0;

    formData= new FormData();

    var id_doctor_schedule = $('input[name=id]').val();

    @foreach($day as $key => $value)
        var checked = $('input[name="day[{{ $key }}]"]').prop('checked');
        if(checked==true) {
            
            for(i=0;i<itemTime['{{ $key }}'];i++) {
                
                var start_time = $('input[name="start_time[{{ $key }}]['+i+']"]').val();
                if(start_time=='') {
                    formValidate(scrollValidate, ['start_time_{{ $key }}_'+i,'{{ trans("validation.empty_start_time") }}', false]);
                    error = true;
                    if(scrollValidate==true) {
                        scrollValidate = false;
                    }
                } else {
                    resetValidation('start_time_{{ $key }}_'+i);
                }

                var end_time = $('input[name="end_time[{{ $key }}]['+i+']"]').val();
                if(end_time=='') {
                    formValidate(scrollValidate, ['end_time_{{ $key }}_'+i,'{{ trans("validation.empty_end_time") }}', false]);
                    error = true;
                    if(scrollValidate==true) {
                        scrollValidate = false;
                    }
                } else {
                    resetValidation('end_time_{{ $key }}_'+i);
                }

                if(start_time!='' && end_time!='') {
                    check_start_time = Date.parse('2000/01/01 '+start_time);
                    check_end_time = Date.parse('2000/01/01 '+end_time);
                    
                    if(check_start_time>=check_end_time) {
                        formValidate(scrollValidate, ['end_time_{{ $key }}_'+i,'{{ trans("validation.invalid_start_end_time") }}', false]);
                        error = true;  
                        if(scrollValidate==true) {
                            scrollValidate = false;
                        }  
                    } else {
                        resetValidation('end_time_{{ $key }}_'+i);
                    }
                }

                var quota = $('input[name="quota[{{ $key }}]['+i+']"]').val();

                var appointment = $('input[name="appointment[{{ $key }}]['+i+']"]').is(':checked');
                if(appointment==true) {
                    appointment = 1;
                } else {
                    appointment = 0;
                }

                var estimate = $('input[name="estimate[{{ $key }}]['+i+']"]').val();

                
                try {
                    if(start_time==undefined || end_time==undefined || quota==undefined || appointment==undefined || estimate==undefined) {
                        continue;
                    }

                    if(id_doctor_schedule!="") {
                        formData.append("schedule["+numScheduleTime+"][id]", $('input[name="id_time[{{ $key }}]['+i+']"]').val());
                        formData.append("schedule["+numScheduleTime+"][day]", parseInt('{{ $key }}')+1);
                        formData.append("schedule["+numScheduleTime+"][start_time]", start_time);
                        formData.append("schedule["+numScheduleTime+"][end_time]", end_time);
                        formData.append("schedule["+numScheduleTime+"][appointment]", appointment);
                        formData.append("schedule["+numScheduleTime+"][estimate]", estimate);
                        formData.append("schedule["+numScheduleTime+"][quota]", quota);
                        formData.append("schedule["+numScheduleTime+"][room]", "");
                        formData.append("schedule["+numScheduleTime+"][online_bookable]", 1);
                    } else {
                        formData.append("schedule["+numScheduleTime+"][day]", parseInt('{{ $key }}')+1);
                        formData.append("schedule["+numScheduleTime+"][start_time]", start_time);
                        formData.append("schedule["+numScheduleTime+"][end_time]", end_time);
                        formData.append("schedule["+numScheduleTime+"][appointment]", appointment);
                        formData.append("schedule["+numScheduleTime+"][estimate]", estimate);
                        formData.append("schedule["+numScheduleTime+"][quota]", quota);
                        formData.append("schedule["+numScheduleTime+"][room]", "");
                        formData.append("schedule["+numScheduleTime+"][online_bookable]", 1);
                    }


                    numScheduleTime++;

                    scheduleTime[numScheduleTime] = [];
                    
                }
                catch(err) {
                }

            }

        }
    @endforeach

    if(error==false) {
        if(numScheduleTime==0) {
            notif(false,"{{ trans('validation.min_time_schedule') }}");
            error = true;
        }
    }

    if(error==false) {
        event.preventDefault();
        $("form button").attr("disabled", true);

        formData.append("validate", false);
        formData.append("id_clinic", "{{ $id_clinic }}");
        formData.append("start_date_active", convertDate(start_date));
        formData.append("end_date_active", convertDate(end_date));
        formData.append("start_date_leave", convertDate(start_date_leave));
        formData.append("end_date_leave", convertDate(end_date_leave));
        formData.append("level", "user");
        formData.append("user", "{{ $id_user }}");

        $(".btn-primary").addClass("loading");
        $(".btn-primary span").removeClass("hide");

        var action = "";
        if(id_doctor_schedule!="") {
            formData.append("_method", "PATCH");
        }

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/clinic/doctor-schedule/"+ doctor +""+action,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){

                $("form button").attr("disabled", false);

                $(".btn-primary").removeClass("loading");
                $(".btn-primary span").addClass("hide");

                if(!data.error) {

                    if(!data.success) {
                        if(data.errors.id_doctor) {
                            validateElementForm('doctor', 'success', 'error', data.errors.id_doctor, true);

                            if(scrollValidate==true) {
                                $('html, body').animate({
                                    scrollTop: $("#doctor-group").offset().top - 70
                                }, 0); 
                            }
                        } else {
                            validateElementForm('doctor', 'error', 'success', '', false);
                        }
                        
                    } else {

                        $("select[name='polyclinic']").val("").trigger("change");

                        notif(true,'<?php echo trans("validation.success_save_schedule_doctor") ?>', false);

                        if(setup=="schedule") {
                            setup = "service";

                            $('#previous-step').removeClass('hide');
                            $('#step_1').addClass('hide');
                            $('#step_2').addClass('hide');
                            $('#step_3').addClass('done');
                            $('#step_3').addClass('hide');
                            $('#step_4').removeClass('hide');
                            $('#step_4').removeClass('disable');
                            $('#step_5').addClass('hide');
                            $('#step_6').addClass('hide');
                            $('#next-step').removeClass('hide');

                            setup_clinic('service');
                        }

                        setTimeout(function(){
                            loading('#/{{ $lang }}/doctor/'+doctor);
                            redirect('#/{{ $lang }}/doctor/'+doctor);
                        }, 500);
                        
                    } 
                } else {
                    notif(false,"{{ trans('validation.failed') }}");
                }
            },
            error: function(){
                $("form button").attr("disabled", false);

                $(".btn-primary").removeClass("loading");
                $(".btn-primary span").addClass("hide");

                notif(false,"{{ trans('validation.failed') }}");
            }
        })
    }

});

</script>

