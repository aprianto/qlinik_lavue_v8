@include('layouts.lib')


<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.edit_doctor') }}
                    </h3>
                </div>
                <div class="cell" id="loading-edit">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload-edit">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec alert-notif hide" id="section-edit">
                    <form role="form" class="sec" id="form-edit">
                        <input type="hidden" name="id" />
                        <input type="hidden" name="id_doctor" />
                        @if(Session::get('pcare'))
                        <div class="form-group" id="doctorBpjs-group">
                            <label class="control-label">Kode Tenaga Medis BPJS </label>
                            <div class="border-group">
                                <select class="form-control" name="code_bpjs">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        @endif
                        <div class="form-group" id="polyclinic-group">
                            <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="polyclinic">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <h3 class="group-label">{{ trans('messages.personal_data') }}</h3>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.doctor_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_doctor_name') }}" name="name">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="role-group">
                            <label class="control-label">Role <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="role">
                                    <option value=""></option>
                                    <option value="Dokter Umum">Dokter Umum</option>
                                    <option value="Dokter Gigi">Dokter Gigi</option>
                                    <option value="Perawat">Perawat</option>
                                    <option value="Spesialis">Spesialis</option>
                                    <option value="Subspesialis">Subspesialis</option>
                                    <option value="Terapis">Terapis</option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                        </div>        
                        <h3 class="group-label">{{ trans('messages.address') }}</h3>
                        <div class="form-group" id="country-group">
                            <label class="control-label">{{ trans('messages.country') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="country">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="province-group">
                            <label class="control-label">{{ trans('messages.province') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="province" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="regency-group">
                            <label class="control-label">{{ trans('messages.regency') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="regency" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                                
                        <div class="form-group" id="district-group">
                            <label class="control-label">{{ trans('messages.district') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="district" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="village-group">
                            <label class="control-label">{{ trans('messages.village') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="village" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.address') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_address') }}" name="address">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">{{ trans('messages.postal_code') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_postal_code') }}" name="postal_code">
                                </div>
                            </div>
                        </div>
                        <h3 class="group-label">{{ trans('messages.others') }}</h3> 
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.note') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_note') }}" name="note">
                            <span class="info-block">*{{ trans('messages.optional') }}</span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/doctor" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#form-edit select[name='phone_code']").wSelect();
$("#form-edit select[name='phone_code']").val('62').change();

$("#form-edit select[name='district']").select2({
    placeholder: "{{ trans('messages.select_district') }}",
    allowClear: false
});

$("#form-edit select[name='village']").select2({
    placeholder: "{{ trans('messages.select_village') }}",
    allowClear: false
});

$("#form-edit select[name='regency']").select2({
    placeholder: "{{ trans('messages.select_regency') }}",
    allowClear: false
});

$("#form-edit select[name='province']").select2({
    placeholder: "{{ trans('messages.select_province') }}",
    allowClear: false
});

$("#form-edit select[name='country']").select2({
    placeholder: "{{ trans('messages.select_country') }}",
    allowClear: false
});

$("#form-edit select[name='role']").select2({
    placeholder: "Silahkan pilih role",
    allowClear: false
});

var get_country = "";
var get_province = "";
var get_regency = "";
var get_district = "";
var get_village = "";
var get_postal_code = "";

function resetData() {
    get_country = "";
    get_province = "";
    get_regency = "";
    get_district = "";
    get_village = "";
    get_postal_code = "";
}

@if(Session::get('pcare'))
function listDoctorBPJS(val) {
    loading_content("#form-edit #doctorBpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-doctor",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataDoctorBpjs){

            if(!dataDoctorBpjs.error) {
                loading_content("#form-edit #doctorBpjs-group", "success");

                var item = dataDoctorBpjs.list;

                $("#form-edit select[name='code_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='code_bpjs']").append("<option value='"+item[i].kdDokter+"' data-name='"+item[i].nmDokter+"'>"+item[i].kdDokter+' - '+item[i].nmDokter+"</option>");
                }

                $("#form-edit select[name='code_bpjs']").select2({
                    placeholder: "Pilih Tenaga Medis",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='code_bpjs']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-edit #doctorBpjs-group", "failed");

                $("#form-edit #doctorBpjs-group #loading-content").click(function(){ listDoctorBPJS(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #doctorBpjs-group", "failed");

            $("#form-edit #doctorBpjs-group #loading-content").click(function(){ listDoctorBPJS(val); });
        }
    })
}
@endif

function listPoly(val) {
    loading_content("#form-edit #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){

            if(!dataPoly.error) {
                loading_content("#form-edit #polyclinic-group", "success");

                var item = dataPoly.data;

                $("#form-edit select[name='polyclinic']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("#form-edit select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-edit #polyclinic-group", "failed");

                $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #polyclinic-group", "failed");

            $("#form-edit #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

function listCountry(val) {
    loading_content("#form-edit #country-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/country",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-edit #country-group", "success");

                var item = data.data;

                $("#form-edit select[name='country']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='country']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].name+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-edit select[name='country']").select2({
                    placeholder: "{{ trans('messages.select_country') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='country']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-edit #country-group", "failed");

                $("#form-edit #country-group #loading-content").click(function(){ listCountry(val); });
            }
        },
        error: function(){
            loading_content("#form-edit #country-group", "failed");

            $("#form-edit #country-group #loading-content").click(function(){ listCountry(val); });
        }
    })
}

function listProvince(val, id_country) {
    loading_content("#form-edit #province-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/province",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-edit #province-group", "success");

                var item = data.data;

                $("#form-edit select[name='province']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='province']").append("<option value='"+checkLocationCase(toTitleCase(item[i].name))+"' data-id='"+item[i].id+"'>"+checkLocationCase(toTitleCase(item[i].name))+"</option>");
                }

                $("#form-edit select[name='province']").select2({
                    placeholder: "{{ trans('messages.select_province') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='province']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-edit #province-group", "failed");

                $("#form-edit #province-group #loading-content").click(function(){ listProvince(val, id_country); });
            }
        },
        error: function(){
            loading_content("#form-edit #province-group", "failed");

            $("#form-edit #province-group #loading-content").click(function(){ listProvince(val, id_country); });
        }
    })
}

function listRegency(val, id_province) {
    loading_content("#form-edit #regency-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/regency?id_province="+id_province,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-edit #regency-group", "success");

                var item = data.data;

                $("#form-edit select[name='regency']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='regency']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-edit select[name='regency']").select2({
                    placeholder: "{{ trans('messages.select_regency') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='regency']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-edit #regency-group", "failed");

                $("#form-edit #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
            }
        },
        error: function(){
            loading_content("#form-edit #regency-group", "failed");

            $("#form-edit #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
        }
    })
}

function listDistrict(val, id_regency) {
    loading_content("#form-edit #district-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/district?id_regency="+id_regency,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-edit #district-group", "success");

                var item = data.data;

                $("#form-edit select[name='district']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='district']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-edit select[name='district']").select2({
                    placeholder: "{{ trans('messages.select_district') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='district']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-edit #district-group", "failed");

                $("#form-edit #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
            }
        },
        error: function(){
            loading_content("#form-edit #district-group", "failed");

            $("#form-edit #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
        }
    })
}

function listVillage(val, id_district) {
    loading_content("#form-edit #village-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/village?id_district="+id_district,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-edit #village-group", "success");

                var item = data.data;

                $("#form-edit select[name='village']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-edit select[name='village']").append("<option value='"+toTitleCase(item[i].name)+"' data-postal_code='"+item[i].postal_code+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-edit select[name='village']").select2({
                    placeholder: "{{ trans('messages.select_village') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-edit select[name='village']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-edit #village-group", "failed");

                $("#form-edit #village-group #loading-content").click(function(){ listVillage(val, id_district); });
            }
        },
        error: function(){
            loading_content("#form-edit #village-group", "failed");

            $("#form-edit #village-group #loading-content").click(function(){ listVillage(val, id_district); });
        }
    })
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#section-edit').addClass('hide');
    $("#reload-edit").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                $('#loading-edit').addClass('hide');
                $('#section-edit').removeClass('hide');

                get_country = data.country.name;
                get_province = data.province.name;
                get_regency = data.regency.name;
                get_district = data.district.name;
                get_village = data.village.name;
                get_postal_code = data.postal_code;

                @if(Session::get('pcare'))
                if(data.kode_bpjs == null){
                    listDoctorBPJS('');
                }else{
                    listDoctorBPJS(data.kode_bpjs);
                }
                @endif
                listPoly(data.polyclinic.id);
                listCountry(data.country.name);
                //listProvince(data.province);
                //listRegency(data.regency);
                //listDistrict(data.district);

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=id_doctor]").val(data.id);
                $("#form-edit input[name=email]").val(data.email);
                $("#form-edit select[name='phone_code']").val(data.phone_code).change();
                $("#form-edit select[name='role']").val(data.role).change();
                $("#form-edit input[name=phone]").val(data.phone);
                $("#form-edit input[name=name]").val(data.name);
                $("#form-edit input[name=address]").val(data.address);
                $("#form-edit input[name='postal_code']").val(data.postal_code);
                $("#form-edit input[name=note]").val(br2nl(data.note));
            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");

                $("#reload-edit .reload").click(function(){ edit(id); });
            }

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    })
}

edit("{{ $id }}");

function resetProvince() {
    $("#form-edit select[name=province]").html("");
    $("#form-edit select[name=province]").prop("disabled", true);
    $("#form-edit select[name=province]").append("<option value=''>{{ trans('messages.select_province') }}</option>");
    $("#form-edit select[name=province]").select2({
        placeholder: "{{ trans('messages.select_province') }}",
        allowClear: false
    });
}

function resetRegency() {
    $("#form-edit select[name=regency]").html("");
    $("#form-edit select[name=regency]").prop("disabled", true);
    $("#form-edit select[name=regency]").append("<option value=''>{{ trans('messages.select_regency') }}</option>");
    $("#form-edit select[name=regency]").select2({
        placeholder: "{{ trans('messages.select_regency') }}",
        allowClear: false
    });
}

function resetDistrict() {
    $("#form-edit select[name=district]").html("");
    $("#form-edit select[name=district]").prop("disabled", true);
    $("#form-edit select[name=district]").append("<option value=''>{{ trans('messages.select_district') }}</option>");
    $("#form-edit select[name=district]").select2({
        placeholder: "{{ trans('messages.select_district') }}",
        allowClear: false
    });
}

function resetVillage() {
    $("#form-edit select[name=village]").html("");
    $("#form-edit select[name=village]").prop("disabled", true);
    $("#form-edit select[name=village]").append("<option value=''>{{ trans('messages.select_village') }}</option>");
    $("#form-edit select[name=village]").select2({
        placeholder: "{{ trans('messages.select_village') }}",
        allowClear: false
    });
}

function resetZipCode() {
    $("#form-edit input[name=postal_code]").val("");
}

$('#form-edit select[name=country]').on('change', function() {
    var country = $(this).find(':selected').data('id');

    if(this.value=="" || this.value.toLowerCase()!='indonesia') {
        resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(country==get_country) {
            listProvince(get_province, country);
        } else {
            resetProvince()
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listProvince('', country);
        }        
    }
});

$('#form-edit select[name=province]').on('change', function() {
    var province = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_province) {
            listRegency(get_regency, province);
        } else {
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listRegency('', province);
        }
    }
    
});

$('#form-edit select[name=regency]').on('change', function() {
    var regency = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_regency) {
            listDistrict(get_district, regency);
        } else {
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listDistrict('', regency);
        }
    }
    
});

$('#form-edit select[name=district]').on('change', function() {
    var district = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_district) {
            listVillage(get_village, district);
        } else {
            resetVillage()
            resetZipCode()
            resetData();
            listVillage('', district);
        }
    }
    
});

$('#form-edit select[name=village]').on('change', function() {
    var postal_code = $(this).find(':selected').data('postal_code');

    if(this.value=="") {
        resetData();
    } else {
        if(this.value==get_village) {
            $("#form-edit input[name=postal_code]").val(get_postal_code);
        } else {
            resetData();
            $("#form-edit input[name=postal_code]").val(postal_code);
        }
    }
});

$("#form-edit #doctorBpjs-group .border-group").click(function() {
    resetValidation('polyclinic');
});

$("#form-edit #polyclinic-group .border-group").click(function() {
    resetValidation('polyclinic');
});

$('#form-edit input[name=name]').focus(function() {
    resetValidation('name');
});

$('#form-edit input[name=phone]').focus(function() {
    resetValidation('phone');
});

$('#form-edit input[name=email]').focus(function() {
    resetValidation('email');
});

$("#form-edit").submit(function(event) {
    event.preventDefault();
    resetValidation('form-edit #polyclinic', 'form-edit #name', 'form-edit #phone', 'form-edit #email');

    $("#form-edit button").attr("disabled", true);

    var id = $("input[name=id]").val();
    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);

    id_doctor = $("#form-edit input[name=id_doctor]").val();

    formData.append("id_polyclinic", $("#form-edit select[name=polyclinic] option:selected").val());
    formData.append("email", $("#form-edit input[name=email]").val());
    formData.append("phone_code", $("#form-edit select[name=phone_code] option:selected").val());
    formData.append("phone", parseInt($("#form-edit input[name=phone]").val()));    
    formData.append("name", $("#form-edit input[name=name]").val());
    formData.append("address", $("#form-edit input[name=address]").val());
    formData.append("country", $("#form-edit select[name=country] option:selected").val());
    formData.append("province", $("#form-edit select[name=province] option:selected").val());
    formData.append("regency", $("#form-edit select[name=regency] option:selected").val());
    formData.append("district", $("#form-edit select[name=district] option:selected").val());
    formData.append("village", $("#form-edit select[name='village'] option:selected").val());
    formData.append("role", $("#form-edit select[name=role] option:selected").val());
    @if(Session::get('pcare'))
    formData.append("kode_bpjs", $("#form-edit select[name='code_bpjs'] option:selected").val());
    @else
    formData.append("kode_bpjs", '');
    @endif
    formData.append("postal_code", $("#form-edit input[name=postal_code]").val());
    formData.append("note", nl2br($("#form-edit input[name=note]").val()));
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
    $(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/{{ $id }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){

            $("#form-edit button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    formValidate(true, ['form-edit #polyclinic',data.errors.id_polyclinic, true], ['form-edit #name',data.errors.name, true], ['form-edit #phone',data.errors.phone, true], ['form-edit #email',data.errors.email, true], ['form-edit #role',data.errors.role, true]);
                } else {

                    notif(true,"{{ trans('validation.success_edit_doctor') }}");  

                    setTimeout(function(){
                        loading('#/{{ $lang }}/doctor/'+id_doctor);
                        redirect('#/{{ $lang }}/doctor/'+id_doctor);
                    }, 500);
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>