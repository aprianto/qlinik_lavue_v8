@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/dokter.png') }}" />
                    <h3>
                        {{ trans('messages.doctor') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/doctor"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    @if($level != 'doctor')
                    <a href="#/{{ $lang }}/doctor/create" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.add_doctor') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/doctor/schedule" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/jadwal-dokter.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.doctor_schedule') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/commission" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/komisi-dokter.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.commission_doctor') }}</span>
                        </div>
                    </a>
                    @endif
                    <a href="#/{{ $lang }}/doctor" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/tampilan-list.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.list_view') }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">               
                            <div class="col-md-4 form-change">
                                <div class="form-group content-column" id="polyclinic-group">
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>               
                            <div class="col-md-4 form-change">
                                <div class="form-group">
                                    <div id="datepicker_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_date') }}" name="date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_doctor_name') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div>
                <div class="table-area table-calendar">
                    <div class="table-responsive">
                        <table class="table table-hover table-border">
                            <thead>
                                <tr id="section_date">                                    
                                </tr>
                                <!--<tr id="section_day">
                                </tr>-->
                            </thead>
                            <tbody id="table">
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="8" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="8" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>


<script>

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

$('#datepicker_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$("#datepicker_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if (polyclinic == "all") {
            polyclinic = "";
        }
        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate(day))
        calendar(convertDate(day));
    });
});

function listPoly(val) {
    loading_content("#form-search #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){

            if(!dataPoly.error) {
                loading_content("#form-search #polyclinic-group", "success");

                var item = dataPoly.data;
                if("{{$level != 'doctor'}}") {
                    $("#form-search select[name='polyclinic']").html("<option value=''></option><option value='all'>{{ trans('messages.all_polyclinic') }}</option>");
                    for (var i = 0; i < item.length; i++) {
                        $("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }
    
                    $("#form-search select[name='polyclinic']").select2({
                        placeholder: "{{ trans('messages.select_polyclinic') }}",
                        allowClear: false
                    });
                }else {
                    $("#form-search select[name='polyclinic']").html("<option selected value='{{$id_clinic}}'>{{$clinic->name}}</option>");
                }

                if(val!="") {
                    $("#form-search select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-search #polyclinic-group", "failed");

                $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-search #polyclinic-group", "failed");

            $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

listPoly(polyclinicRepo);

function calendar(date) {
    if(date=='') {
        date = new Date();
    }
    
    $('#section_date').html('<th valign="middle" class="middle">{{ trans("messages.name") }}</th>');
    //$('#section_day').html('');

    for (var d = new Date(date); d < addDays(new Date(date), 7); d.setDate(d.getDate() + 1)) {

        var getMonths = ((d.getMonth()+1)+100).toString().slice(-2);
        var getDays = (d.getDate()+100).toString().slice(-2);
        var getYears = d.getFullYear();
        var getDates = getDays+'/'+getMonths+'/'+getYears;

        var color = "";
        if(dateNow==getDates) {
            color = "color-now";
        }

        $('#section_date').append('<th class="color '+color+'">'+d.getDate()+' '+getMonth(d.getMonth()+1)+' '+d.getFullYear()+'<br /><span>'+getDay(d.getDay())+'</span></th>');
        //$('#section_day').append('<th class="color '+color+'">''</th>');
    }
}

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='date']").val(unconvertDate(dateRepo));

var keySearch = searchRepo;
var keyPoly = polyclinicRepo;
var keyDate = dateRepo;
var numPage = pageRepo;

function search(page, submit, q, polyclinic, date) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if(date=="" && submit=="false") {
        var date = keyDate;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyPoly = polyclinic;
    keyDate = date;
    numPage = page;

    repository(['search',q],['polyclinic',polyclinic],['date',date],['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor",
        type: "GET",
        data: "q="+q+"&id_polyclinic="+polyclinic+"&date="+date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");
            
            if(!data.error) {
                $(".table").addClass("table-hover");

                $(".pagination-sec").removeClass("hide");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    if(date=='') {
                        date = new Date();
                        month = date.getMonth()+1;
                        date = date.getFullYear()+'-'+month+'-'+date.getDate(); 
                    }

                    var item = data.data;
                    var tr;
                    var no = from;                    

                    for (var i = 0; i < item.length; i++) {
                        try {
                            tr = $('<tr/>');
                            tr.append("<td><a href='#/{{ $lang }}/doctor/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span></td>");

                            

                            for (var d = new Date(date.replace(/\-/g,'/')); d < addDays(new Date(date.replace(/\-/g,'/')), 7); d.setDate(d.getDate() + 1)) {
                                timeMonth = d.getMonth()+1;
                                timeDate = d.getFullYear()+'-'+timeMonth+'-'+d.getDate();


                                var getMonths = ((d.getMonth()+1)+100).toString().slice(-2);
                                var getDays = (d.getDate()+100).toString().slice(-2);
                                var getYears = d.getFullYear();
                                var getDates = getDays+'/'+getMonths+'/'+getYears;

                                var color = "";
                                if(dateNow==getDates) {
                                    color = "color-now";
                                }

                                if(item[i].schedule!=null) {

                                    if(Date.parse(timeDate.replace(/\-/g,'/'))>=Date.parse(item[i].schedule.start_date_leave.replace(/\-/g,'/')) && Date.parse(timeDate.replace(/\-/g,'/'))<=Date.parse(item[i].schedule.end_date_leave.replace(/\-/g,'/'))) {
                                        tr.append("<td class='"+color+" failed'><span class='text'>{{ trans('messages.leave') }}</span></td>");
                                    } else {

                                        day = d.getDay();
                                        if(day==0) { day=7; }

                                        var dataTime = "";

                                        itemTime = item[i].schedule.daily;

                                        var numTime = 0;
                                        for (var j = 0; j < itemTime.length; j++) {
                                            if(itemTime[j].day==day) {
                                                numTime++;
                                                dataTime += '<span class="text">'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+'</span><br />';
                                            }
                                        }

                                        var color_success = "";
                                        if(numTime==0) {
                                            dataTime = "-";
                                        } else {
                                            color_success = "success";
                                        }
                                                
                                        tr.append("<td class='"+color+" "+color_success+"'>"+dataTime+"</td>"); 

                                    }
                                } else {
                                    tr.append("<td class='"+color+"'>-</td>"); 
                                }

                                
                            }
                            
                             $("#table").append(tr);

                            no++;
                        } catch(err) {}
                    }
                } else {
                    $(".pagination-sec").addClass("hide");
                    
                    $("#table").append("<tr><td align='center' colspan='8'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, polyclinic, keyDate);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, keyDate); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, keyDate); });
        }
    })
}

$("#form-search input[name='date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }

        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()));

        calendar(convertDate($('#form-search input[name=date]').val()));
    }
});

$("#form-search select[name='polyclinic']").on('change' , function(e) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    
    if(polyclinic=="all") {
        polyclinic = "";
    }

    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()))
    calendar(convertDate($('#form-search input[name=date]').val()));
});

$("#form-search").submit(function(event) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    if(polyclinic=="all") {
        polyclinic = "";
    }

    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()));

    calendar(convertDate($('#form-search input[name=date]').val()));
});

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    let polyclinic = $("#form-search select[name=polyclinic] option:selected").val() == "all" ? "" : $("#form-search select[name=polyclinic] option:selected").val();

    if (e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", e.target.value, polyclinic, convertDate($("#form-search input[name=date]").val()));
            calendar(convertDate($('#form-search input[name=date]').val()));
        }, 2000);
    }
});

search(pageRepo, "false", searchRepo, polyclinicRepo, dateRepo);
calendar(dateRepo);

</script>

