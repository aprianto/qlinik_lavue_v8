@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div>


<!--<div class="panel-content">
    <div class="row  hide" id="tutorial">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-controls text-color">
                    <span class="close-content"><i class="fa fa-times"></i></span>
                </div>
                <div class="welcome-bar">
                    <i class="fa fa-bullhorn orange-skin"></i>
                    <div id="text-tutorial">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->


<div class="panel-content">
    <div class="row hide" id="section-loading">
        <div class="col-md-9">
            <div class="widget no-color" id="loading-detail">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="widget no-color" id="reload-detail">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-profile">
        <div class="col-md-9">            
            <div class="admin-follow no-bg">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.doctor_profile') }}
                    </h3>
                </div>
                <img id="photo-doctor" src="{{ $storage.'/images/doctor/photo.png' }}" />
                <h3 id="name"></h3>
                <span id="polyclinic"></span>
            </div>
        </div>
    </div>
    <div class="row hide" id="section-detail">
        <div class="col-md-9">
            <div class="widget widget-detail">
                <div class="widget-controls">
                    @if($level=="user")
                    <a id="link-edit-profile" href="#/{{ $lang }}/doctor/{{ $id }}/edit" onclick="loading($(this).attr('href'))"><img src="{{ asset('assets/images/icons/info/edit.png') }}" /></a>
                    @endif
                </div>
                <div class="twitter-feed">
                    <div class="twitter-account">
                        <h3>{{ trans("messages.info") }}</h3>
                    </div>
                    <div class="contact-details-info">
                        <ul>
                            <li>
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/phone-number.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.phone_number') }}</span>
                                        <span id="phone"></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/email.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.email') }}</span>
                                        <span id="email"></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/location.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.address') }}</span>
                                        <span id="address"></span>
                                    </div>
                                </div>                                
                            </li>
                            <li>
                                <div class="detail-sec">
                                    <div class="icon-sec">
                                        <img src="{{ asset('assets/images/icons/info/notes.png') }}" />
                                    </div>
                                    <div class="info-sec">
                                        <span class="title">{{ trans('messages.note') }}</span>
                                        <span id="note"></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($level=="user")
<div class="panel-content panel-detail hide" id="section-schedule">
    <div class="row">
        <div class="col-md-9">
            <div class="widget widget-detail">
                <div class="timeline-color activity-feed">
                    <div class="widget-title">
                        <h3>{{ trans("messages.practice_schedule") }}</h3>
                    </div>
                    <ul class="activity" id="schedule">                        
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                </div>
                                <div class="info-sec">
                                    <span class="item-sec">{{ trans('messages.active_period') }}</span>
                                    <h3 id="active_period"></h3>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="detail-sec">
                                <div class="icon-sec">
                                </div>
                                <div class="info-sec">
                                    <span class="item-sec">{{ trans('messages.leave') }}</span>
                                    <h3 id="leave"></h3>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="btn-sec">
                        <a href="#/{{ $lang }}/doctor/schedule/id/{{ $id }}" class="btn-transparent hide" id="add-schedule" onclick="loading($(this).attr('href'))">{{ trans("messages.add_schedule") }}</a>
                        <a href="#/{{ $lang }}/doctor/schedule/id/{{ $id }}" class="btn-transparent hide" id="edit-schedule" onclick="loading($(this).attr('href'))">{{ trans("messages.edit_schedule") }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<script type="text/javascript">

detail('{{ $id }}');

function detail(id) {
    $('#section-profile').addClass('hide');
    $('#section-detail').addClass('hide');
    $('#section-schedule').addClass('hide');

    $('#section-loading').removeClass('hide');
    $('#loading-detail').removeClass('hide');
    $("#reload-detail").addClass("hide");
    
    formData= new FormData();
    $.ajax({
        @if($level=="user")
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+id+"",
        @else
        url: "{{ $api_url }}/{{ $lang }}/doctor/"+id+"",
        @endif
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                $('#section-profile').removeClass('hide');
                $('#section-detail').removeClass('hide');

                $('#section-loading').addClass('hide');
                $('#loading-detail').addClass('hide');

                $("#section-profile #photo-doctor").replaceWith(thumbnail('lg','doctor','{{ $storage }}',data.photo,data.name));

                $("#path").html(data.name);
                $("#section-profile #name").html(data.name);
                $("#section-detail #phone").html("+"+data.phone_code+""+data.phone);
                $("#section-detail #email").html(data.email);
                $("#section-detail #address").html(nullToEmpty(data.address)+' '+nullToEmpty(data.village.name)+' '+nullToEmpty(data.district.name)+' '+nullToEmpty(data.regency.name)+' '+nullToEmpty(data.province.name)+' '+nullToEmpty(data.country.name));
                $("#section-detail #note").html(checkNull(data.note));

                @if($level=="user")
                if(data.clinic==null) {
                var polyclinic = "";
                for(i=0; i<data.polyclinic.length; i++) {
                    polyclinic = polyclinic+data.polyclinic[i].name;

                    if(i!=data.polyclinic.length-1) {
                        polyclinic = polyclinic+", ";
                    }
                }
                $("#section-profile #polyclinic").html(polyclinic);
                $('#link-edit-profile').addClass('hide');
                } else {                    
                    $('#section-schedule').removeClass('hide');
                    $("#section-profile #polyclinic").html(data.polyclinic.name);

                    if(data.schedule!=null) {
                        if(setup=="schedule") {
                            $("#tutorial").removeClass("hide");
                            $("#text-tutorial").html('<?php echo trans("tutorial.detail_doctor_schedule_doctor", ["link"=>"#/".$lang."/doctor/schedule/id/".$id.""]); ?>');
                            $("#tutorial .close-content").attr("onclick","setup_clinic('service');");
                        } else if(setup=="service") {
                            $("#tutorial").removeClass("hide");
                            $("#text-tutorial").html('<?php echo trans("tutorial.detail_doctor_add_service", ["link"=>"#/".$lang."/service/create", "back"=>"#/".$lang."/doctor"]); ?>');
                            $("#tutorial .close-content").attr("onclick","setup_clinic('commission');");
                        }

                        itemTime = data.schedule.daily;

                        var dataTime = "";

                        if(itemTime!=null) {
                            $("#schedule").removeClass("hide");
                            $("#edit-schedule").removeClass("hide");
                            $("#add-schedule").addClass("hide");

                            var schedule = "";

                            for (var j = 0; j < itemTime.length; j++) {
                                var appointment = "";
                                if(itemTime[j].appointment==1) {
                                    appointment = ' • {{ trans("messages.set_appointment") }}</i>';
                                } else {
                                    appointment = '';
                                }

                                var estimate = "";
                                if(nullToZero(itemTime[j].estimate)!=0) {
                                    estimate = '<span>{{ trans("messages.estimate_schedule") }}: '+itemTime[j].estimate+' {{ trans("messages.minutes") }}</span>';
                                }
                                
                                if(j==0) {
                                    schedule = schedule+'<li>'+
                                        '<div class="detail-sec">'+
                                            '<div class="icon-sec">'+
                                                '<img src=\'{{ asset("assets/images/icons/info/schedule.png") }}\' />'+
                                            '</div>'+
                                            '<div class="info-sec">'+
                                                '<h3>'+getDay(itemTime[j].day)+'</h3>'+
                                                '<span>'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+' '+appointment+'</span>'+
                                                ''+estimate+'';
                                } else if(itemTime[j].day!=itemTime[j-1].day) {
                                    
                                    schedule = schedule+'</div>'+
                                            '</div>'+
                                        '</li><li>'+
                                            '<div class="detail-sec">'+
                                            '<div class="icon-sec">'+
                                            '</div>'+
                                            '<div class="info-sec">'+
                                                '<h3 class="subitem-sec">'+getDay(itemTime[j].day)+'</h3>'+
                                                '<span>'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+' '+appointment+'</span>'+
                                                ''+estimate+'';
                                    }


                                if(j!==0) {
                                    if(itemTime[j].day==itemTime[j-1].day) {
                                        schedule = schedule+'<span>'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+' '+appointment+'</span>'+
                                            ''+estimate+'';

                                        if(j==itemTime.length-1) {
                                            schedule = schedule+'</div>'+
                                                    '</div>'+
                                                '</li>';
                                        }
                                    }
                                }
                            }

                            $("#schedule").prepend(schedule);

                            var active_period = "-";
                            if(data.schedule!=null) {
                                if(data.schedule.end_date_active!="0000-00-00" && data.schedule.end_date_active!="9999-12-31") {
                                    active_period = getFormatDate(data.schedule.start_date_active)+" - "+getFormatDate(data.schedule.end_date_active);
                                } else {
                                    active_period = getFormatDate(data.schedule.start_date_active);
                                }
                            }

                            var leave = "-";
                            if(data.schedule!=null) {
                                if(data.schedule.start_date_leave!="0000-00-00") {
                                    leave = getFormatDate(data.schedule.start_date_leave)+" - "+getFormatDate(data.schedule.end_date_leave);
                                }
                            }

                            $("#section-schedule #active_period").html(active_period);
                            $("#section-schedule #leave").html(leave);
                        } else {
                            $("#schedule").addClass("hide");
                            $("#edit-schedule").addClass("hide");
                            $("#add-schedule").removeClass("hide");
                        }

                    } else {
                        $("#schedule").addClass("hide");
                        $("#edit-schedule").addClass("hide");
                        $("#add-schedule").removeClass("hide");
                    }
                }
                @else
                var polyclinic = "";
                for(i=0; i<data.polyclinic.length; i++) {
                    polyclinic = polyclinic+data.polyclinic[i].name;

                    if(i!=data.polyclinic.length-1) {
                        polyclinic = polyclinic+", ";
                    }
                }
                $("#section-profile #polyclinic").html(polyclinic);
                $('#link-edit-profile').addClass('hide');
                @endif

            } else {
                $('#loading-detail').addClass('hide');
                $("#reload-detail").removeClass("hide");

                $("#reload-detail .reload").click(function(){ detail(id); });
            }

        },
        error: function(){
            $('#loading-detail').addClass('hide');
            $("#reload-detail").removeClass("hide");

            $("#reload-detail .reload").click(function(){ detail(id); });
        }
    })
}

</script>