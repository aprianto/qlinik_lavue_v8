@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 


<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <a title="" onclick="back();loading($(this).attr('href'))">
                        <img src="{{ asset('assets/images/icons/nav/chevron-lm.png') }}" />
                    </a> 
                    <h3 class="right">
                        {{ trans('messages.add_doctor') }}
                    </h3>
                </div>
                <div class="form-elements-sec alert-notif">
                    <form role="form" class="sec" id="form-add">
                        <input type="hidden" name="id_doctor" value="" />
                        @if(Session::get('pcare'))
                        <div class="form-group" id="doctorBpjs-group">
                            <label class="control-label">Kode Tenaga Medis BPJS </label>
                            <div class="border-group">
                                <select class="form-control" name="code_bpjs">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        @endif
                        <div class="form-group" id="polyclinic-group">
                            <label class="control-label">{{ trans('messages.polyclinic') }} <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="polyclinic">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <h3 class="group-label">{{ trans('messages.personal_data') }}</h3>
                        <div class="form-group hide" id="profile-doctor">
                            <div class="detail-profile-form">
                                <div class="column" id="doctor-photo">
                                    <img src='{{ $storage."/images/doctor/photo.png" }}'>
                                </div>
                                <div class="column">
                                    <h1 id="doctor-name"></h1>
                                    <p id="doctor-email"></p>
                                    <p id="doctor-phone"></p>
                                </div>
                                <a class="btn-white" onclick="resetDoctor();">+ {{ trans("messages.add_other_doctor") }}</a>
                            </div>
                        </div>
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.doctor_name') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_doctor_name') }}" name="name">
                            <span class="help-block"></span>
                        </div> 
                        <div class="form-group" id="role-group">
                            <label class="control-label">Role <span>*</span></label>
                            <div class="border-group">
                                <select class="form-control" name="role">
                                    <option value=""></option>
                                    <option value="Dokter Umum">{{ trans('messages.doctor_general') }}</option>
                                    <option value="Dokter Gigi">{{ trans('messages.dentist') }}</option>
                                    <option value="Perawat">{{ trans('messages.nurse') }}</option>
                                    <option value="Spesialis">{{ trans('messages.specialist') }}</option>
                                    <option value="Subspesialis">{{ trans('messages.subspecialist') }}</option>
                                    <option value="Terapis">{{ trans('messages.therapist') }}</option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} <span>*</span></label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_email') }}" name="email">
                            <span class="help-block"></span>
                            <span id="loading-email" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>
                        <div class="form-group" id="phone-group">
                            <label class="control-label">{{ trans('messages.phone_number') }} <span>*</span></label>
                            <input type="text" class="form-control phone" placeholder="{{ trans('messages.input_phone_number') }}" name="phone" onkeydown="number(event)">
                            
                            <?php 
                                $list_phone = phone_code();
                            ?>

                            <select id="phone_code" name="phone_code" class="select-phone-code">
                                @foreach($list_phone as $phone)
                                    <option value="{{ $phone[1] }}" data-icon="assets/images/flags/{{ $phone[0] }}.png">+{{ $phone[1] }}</option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>
                            <span id="loading-phone" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        </div>               
                        <h3 class="group-label">{{ trans('messages.address') }}</h3>
                        <div class="form-group" id="country-group">
                            <label class="control-label">{{ trans('messages.country') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="country">
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="province-group">
                            <label class="control-label">{{ trans('messages.province') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="province" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="regency-group">
                            <label class="control-label">{{ trans('messages.regency') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="regency" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>                                
                        <div class="form-group" id="district-group">
                            <label class="control-label">{{ trans('messages.district') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="district" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group content-column" id="village-group">
                            <label class="control-label">{{ trans('messages.village') }}</label>
                            <div class="border-group">
                                <select class="form-control" name="village" disabled="disabled">
                                    <option value=''></option>
                                </select>
                            </div>
                            <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                            <span class="help-block"></span>
                        </div> 
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.address') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_address') }}" name="address">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">{{ trans('messages.postal_code') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ trans('messages.input_postal_code') }}" name="postal_code">
                                </div>
                            </div>
                        </div>                        
                        <h3 class="group-label">{{ trans('messages.others') }}</h3> 
                        <div class="form-group">
                            <label class="control-label">{{ trans('messages.note') }}</label>
                            <input type="text" class="form-control" placeholder="{{ trans('messages.input_note') }}" name="note">
                            <span class="info-block">*{{ trans('messages.optional') }}</span>
                        </div>
                        <div class="form-group last-item">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                            <a href="#/{{ $lang }}/doctor" class="btn btn-secondary" onclick="loading($(this).attr('href'))">
                                {{ trans('messages.cancel') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script>

$("select[name='phone_code']").wSelect();
$("select[name='phone_code']").val('62').change();

$("#form-add select[name='role']").select2({
    placeholder: "Silahkan pilih role",
    allowClear: false
});

$("#form-add select[name='district']").select2({
    placeholder: "{{ trans('messages.select_district') }}",
    allowClear: false
});

$("#form-add select[name='regency']").select2({
    placeholder: "{{ trans('messages.select_regency') }}",
    allowClear: false
});

$("#form-add select[name='village']").select2({
    placeholder: "{{ trans('messages.select_village') }}",
    allowClear: false
});

$("#form-add select[name='province']").select2({
    placeholder: "{{ trans('messages.select_province') }}",
    allowClear: false
});

$("#form-add select[name='country']").select2({
    placeholder: "{{ trans('messages.select_country') }}",
    allowClear: false
});

var get_country = "";
var get_province = "";
var get_regency = "";
var get_district = "";
var get_village = "";
var get_postal_code = "";

function resetData() {
    get_country = "";
    get_province = "";
    get_regency = "";
    get_district = "";
    get_village = "";
    get_postal_code = "";
}

@if(Session::get('pcare'))
function listDoctorBPJS() {
    loading_content("#form-add #doctorBpjs-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/list-doctor",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #doctorBpjs-group", "success");

                var item = data.list;

                $("select[name='code_bpjs']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='code_bpjs']").append("<option value='"+item[i].kdDokter+"' data-name='"+item[i].nmDokter+"'>"+item[i].kdDokter+' - '+item[i].nmDokter+"</option>");
                }

                $("select[name='code_bpjs']").select2({
                    placeholder: "Pilih Tenaga Medis",
                    allowClear: false
                });

            } else {
                loading_content("#form-add #doctorBpjs-group", "failed");

                $("#form-add #doctorBpjs-group #loading-content").click(function(){ listDoctorBPJS(); });
            }
        },
        error: function(){
            loading_content("#form-add #doctorBpjs-group", "failed");

            $("#form-add #doctorBpjs-group #loading-content").click(function(){ listDoctorBPJS(); });
        }
    })
}
@endif

function listPoly() {
    loading_content("#form-add #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #polyclinic-group", "success");

                var item = data.data;

                $("select[name='polyclinic']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                }

                $("select[name='polyclinic']").select2({
                    placeholder: "{{ trans('messages.select_polyclinic') }}",
                    allowClear: false
                });

            } else {
                loading_content("#form-add #polyclinic-group", "failed");

                $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(); });
            }
        },
        error: function(){
            loading_content("#form-add #polyclinic-group", "failed");

            $("#form-add #polyclinic-group #loading-content").click(function(){ listPoly(); });
        }
    })
}

function listCountry(val) {
    loading_content("#form-add #country-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/country",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #country-group", "success");

                var item = data.data;

                $("#form-add select[name='country']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='country']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].name+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='country']").select2({
                    placeholder: "{{ trans('messages.select_country') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='country']").val(val).trigger("change");
                } else {
                    $("#form-add select[name='country']").val("Indonesia").trigger("change");
                }

                
            } else {
                loading_content("#form-add #country-group", "failed");

                $("#form-add #country-group #loading-content").click(function(){ listCountry(); });
            }
        },
        error: function(){
            loading_content("#form-add #country-group", "failed");

            $("#form-add #country-group #loading-content").click(function(){ listCountry(); });
        }
    })
}

function listProvince(val, id_country) {
    loading_content("#form-add #province-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/province",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #province-group", "success");

                var item = data.data;

                $("#form-add select[name='province']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='province']").append("<option value='"+checkLocationCase(toTitleCase(item[i].name))+"' data-id='"+item[i].id+"'>"+checkLocationCase(toTitleCase(item[i].name))+"</option>");
                }

                $("#form-add select[name='province']").select2({
                    placeholder: "{{ trans('messages.select_province') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='province']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #province-group", "failed");

                $("#form-add #province-group #loading-content").click(function(){ listProvince(val, id_country); });
            }
        },
        error: function(){
            loading_content("#form-add #province-group", "failed");

            $("#form-add #province-group #loading-content").click(function(){ listProvince(val, id_country); });
        }
    })
}

function listRegency(val, id_province) {
    loading_content("#form-add #regency-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/regency?id_province="+id_province,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #regency-group", "success");

                var item = data.data;

                $("#form-add select[name='regency']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='regency']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='regency']").select2({
                    placeholder: "{{ trans('messages.select_regency') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='regency']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-add #regency-group", "failed");

                $("#form-add #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
            }
        },
        error: function(){
            loading_content("#form-add #regency-group", "failed");

            $("#form-add #regency-group #loading-content").click(function(){ listRegency(val, id_province); });
        }
    })
}

function listDistrict(val, id_regency) {
    loading_content("#form-add #district-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/district?id_regency="+id_regency,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #district-group", "success");

                var item = data.data;

                $("#form-add select[name='district']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='district']").append("<option value='"+toTitleCase(item[i].name)+"' data-id='"+item[i].id+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='district']").select2({
                    placeholder: "{{ trans('messages.select_district') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='district']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-add #district-group", "failed");

                $("#form-add #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
            }
        },
        error: function(){
            loading_content("#form-add #district-group", "failed");

            $("#form-add #district-group #loading-content").click(function(){ listDistrict(val, id_regency); });
        }
    })
}

function listVillage(val, id_district) {
    loading_content("#form-add #village-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/village?id_district="+id_district,
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                loading_content("#form-add #village-group", "success");

                var item = data.data;

                $("#form-add select[name='village']").html("<option value=''></option> ");
                for (var i = 0; i < item.length; i++) {
                    $("#form-add select[name='village']").append("<option value='"+toTitleCase(item[i].name)+"' data-postal_code='"+item[i].postal_code+"'>"+toTitleCase(item[i].name)+"</option>");
                }

                $("#form-add select[name='village']").select2({
                    placeholder: "{{ trans('messages.select_village') }}",
                    allowClear: false
                });

                if(val!="") {
                    $("#form-add select[name='village']").val(val).trigger("change");
                }
                
            } else {
                loading_content("#form-add #village-group", "failed");

                $("#form-add #village-group #loading-content").click(function(){ listVillage(val, id_district); });
            }
        },
        error: function(){
            loading_content("#form-add #village-group", "failed");

            $("#form-add #village-group #loading-content").click(function(){ listVillage(val, id_district); });
        }
    })
}

function resetProvince() {
    $("#form-add select[name=province]").html("");
    $("#form-add select[name=province]").prop("disabled", true);
    $("#form-add select[name=province]").append("<option value=''>{{ trans('messages.select_province') }}</option>");
    $("#form-add select[name=province]").select2({
        placeholder: "{{ trans('messages.select_province') }}",
        allowClear: false
    });
}

function resetRegency() {
    $("#form-add select[name=regency]").html("");
    $("#form-add select[name=regency]").prop("disabled", true);
    $("#form-add select[name=regency]").append("<option value=''>{{ trans('messages.select_regency') }}</option>");
    $("#form-add select[name=regency]").select2({
        placeholder: "{{ trans('messages.select_regency') }}",
        allowClear: false
    });
}

function resetDistrict() {
    $("#form-add select[name=district]").html("");
    $("#form-add select[name=district]").prop("disabled", true);
    $("#form-add select[name=district]").append("<option value=''>{{ trans('messages.select_district') }}</option>");
    $("#form-add select[name=district]").select2({
        placeholder: "{{ trans('messages.select_district') }}",
        allowClear: false
    });
}

function resetVillage() {
    $("#form-add select[name=village]").html("");
    $("#form-add select[name=village]").prop("disabled", true);
    $("#form-add select[name=village]").append("<option value=''>{{ trans('messages.select_village') }}</option>");
    $("#form-add select[name=village]").select2({
        placeholder: "{{ trans('messages.select_village') }}",
        allowClear: false
    });
}

function resetZipCode() {
    $("#form-add input[name=postal_code]").val("");
}

@if(Session::get('pcare'))
$('#form-add select[name=code_bpjs]').on('change', function() {
    var name = $(this).find(':selected').data('name');
    $("input[name=name]").val(name);
});
@endif

$('#form-add select[name=country]').on('change', function() {
    var country = $(this).find(':selected').data('id');

    if(this.value=="" || this.value.toLowerCase()!='indonesia') {
        resetProvince()
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(country==get_country) {
            listProvince(get_province, country);
        } else {
            resetProvince()
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listProvince('', country);
        }        
    }
});

$('#form-add select[name=province]').on('change', function() {
    var province = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetRegency()
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_province) {
            listRegency(get_regency, province);
        } else {
            resetRegency()
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listRegency('', province);
        }
    }
    
});

$('#form-add select[name=regency]').on('change', function() {
    var regency = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetDistrict()
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_regency) {
            listDistrict(get_district, regency);
        } else {
            resetDistrict()
            resetVillage()
            resetZipCode()
            resetData();
            listDistrict('', regency);
        }
    }
    
});

$('#form-add select[name=district]').on('change', function() {
    var district = $(this).find(':selected').data('id');

    if(this.value=="") {
        resetVillage()
        resetZipCode()
        resetData();
    } else {
        if(this.value==get_district) {
            listVillage(get_village, district);
        } else {
            resetVillage()
            resetZipCode()
            resetData();
            listVillage('', district);
        }
    }
    
});

$('#form-add select[name=village]').on('change', function() {
    var postal_code = $(this).find(':selected').data('postal_code');

    if(this.value=="") {
        resetData();
    } else {
        if(this.value==get_village) {
            $("#form-add input[name=postal_code]").val(get_postal_code);
        } else {
            resetData();
            $("#form-add input[name=postal_code]").val(postal_code);
        }
    }
});

@if(Session::get('pcare'))
listDoctorBPJS();
@endif
listPoly();
listCountry('');
//listProvince();
//listRegency();
//listDistrict();

function getDoctor(id, email, phone_code, phone, name, address, country, province, regency, district, village, postal_code, note, doctor_photo) {
    resetValidation('name', 'phone', 'email');

    $("#form-add input[name='id_doctor']").val(id);
    $("#form-add #profile-doctor").removeClass("hide");
    $("#form-add #profile-doctor #doctor-photo").html(thumbnail('md','doctor','{{ $storage }}',doctor_photo, name));
    $("#form-add #profile-doctor #doctor-name").html(name);
    $("#form-add #profile-doctor #doctor-email").html(email);
    $("#form-add #profile-doctor #doctor-phone").html("+"+phone_code+""+phone);

    /*$("#form-add input[name='email']").prop("disabled", true);
    $("#form-add input[name='phone']").prop("disabled", true);
    $("#form-add input[name='name']").prop("disabled", true);
    $("#form-add input[name='address']").prop("disabled", true);
    $("#form-add select[name='country']").prop("disabled", true).trigger("change");
    $("#form-add select[name='province']").prop("disabled", true).trigger("change");
    $("#form-add select[name='regency']").prop("disabled", true).trigger("change");
    $("#form-add input[name='postal_code']").prop("disabled", true).trigger("change");
    $("#form-add input[name='note']").prop("disabled", true);*/



    $("#form-add input[name='email']").val(email);
    $("#form-add select[name='phone_code']").val(phone_code).change();
    $("#form-add input[name='phone']").val(phone);    
    $("#form-add input[name='name']").val(name);
    $("#form-add input[name='address']").val(address);
    /*$("#form-add select[name='country']").val(country).trigger("change");
    $("#form-add select[name='province']").val(province).trigger("change");
    $("#form-add select[name='regency']").val(regency).trigger("change");
    $("#form-add select[name='district']").val(district).trigger("change");
    $("#form-add select[name='village']").val(village).trigger("change");*/
    $("#form-add input[name='postal_code']").val(postal_code);
    $("#form-add input[name='note']").val(br2nl(note));

    get_country = country;
    get_province = province;
    get_regency = regency;
    get_district = district;
    get_village = village;
    get_postal_code = postal_code;

    listCountry(country);

}

function resetDoctor() {
    resetData()
    resetValidation('name', 'phone', 'email');

    $("#form-add input[name='id_doctor']").val("");
    $("#form-add #profile-doctor").addClass("hide");
    $("#form-add #profile-doctor #doctor-photo").attr("src", "{{ $storage.'/images/doctor/photo.png' }}");
    $("#form-add #profile-doctor #doctor-name").html("");
    $("#form-add #profile-doctor #doctor-email").html("");
    $("#form-add #profile-doctor #doctor-phone").html("");

    $("#form-add input[name='email']").prop("disabled", false);    
    $("#form-add input[name='phone']").prop("disabled", false);
    $("#form-add input[name='name']").prop("disabled", false);
    $("#form-add input[name='address']").prop("disabled", false);
    $("#form-add select[name='country']").prop("disabled", false).trigger("change");
    $("#form-add select[name='province']").prop("disabled", false).trigger("change");
    $("#form-add select[name='regency']").prop("disabled", false).trigger("change");
    $("#form-add input[name='postal_code']").prop("disabled", false).trigger("change");
    $("#form-add input[name='note']").prop("disabled", false);

    $("#form-add input[name='email']").val("");
    $("#form-add select[name='phone_code']").val("62").change();
    $("#form-add input[name='phone']").val("");    
    $("#form-add input[name='name']").val("");
    $("#form-add input[name='address']").val("");
    $("#form-add select[name='country']").val("Indonesia").trigger("change");
    $("#form-add select[name='province']").val("").trigger("change");
    $("#form-add select[name='regency']").val("").trigger("change");
    $("#form-add select[name='district']").val("").trigger("change");
    $("#form-add select[name='village']").val("").trigger("change");
    $("#form-add input[name='postal_code']").val("").trigger("change");
    $("#form-add input[name='note']").val("");
}

$("#form-add input[name='email']").autocomplete({
    minLength: 5,
    source: function( request, response ) {
        var r = [];

        formData= new FormData();
        formData.append("email", request.term);

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/doctor/search",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $("#form-add #loading-email").removeClass("hide");
            },
            success: function(data){
                $("#form-add #loading-email").addClass("hide");

                if(!data.error) {
                    r.push({value:data.id,
                        id:data.id,
                        email:data.email,
                        phone_code:data.phone_code,
                        phone:data.phone,
                        name:data.name,
                        address:data.address,
                        country:data.country.name,
                        province:data.province.name,
                        regency:data.regency.name,
                        district:data.district.name,
                        village:data.village.name,
                        postal_code:data.postal_code,
                        note:data.note,
                        photo:data.photo
                    });

                    response(r);         
                } else {
                    response(r);
                }
                
            },
            error: function(){
                response(r);
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {        
        getDoctor(ui.item.id, ui.item.email, ui.item.phone_code, ui.item.phone, ui.item.name, ui.item.address, ui.item.country, ui.item.province, ui.item.regency, ui.item.district, ui.item.village, ui.item.postal_code, ui.item.note, ui.item.photo);
        return false;
    },
}).data("ui-autocomplete")._renderItem = function (ul, item) {
    var inner_html = '<div class="autocomplete">'+
        thumbnail('md','doctor','{{ $storage }}',item.photo,item.name)+
        '<div class="text">'+
            '<h1>'+item.name+'</h1>'+
            '<p>'+item.email+'</p>'+
            '<p>+'+item.phone_code+''+item.phone+'</p>'+
        '</div>'+
    '</div>';
    return $("<li></li>")
        .data("item.autocomplete", item)
        .append(inner_html)
        .appendTo(ul);
};

$("#form-add input[name='phone']").autocomplete({
    minLength: 8,
    source: function( request, response ) {
        var r = [];

        formData= new FormData();
        formData.append("phone", parseInt(request.term));

        $.ajax({
            url: "{{ $api_url }}/{{ $lang }}/doctor/search",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $("#form-add #loading-phone").removeClass("hide");
            },
            success: function(data){
                $("#form-add #loading-phone").addClass("hide");

                if(!data.error) {
                    r.push({value:data.id,
                        id:data.id,
                        email:data.email,
                        phone_code:data.phone_code,
                        phone:data.phone,
                        name:data.name,
                        address:data.address,
                        country:data.country.name,
                        province:data.province.name,
                        regency:data.regency.name,
                        district:data.district.name,
                        village:data.village.name,
                        postal_code:data.postal_code,
                        note:data.note,
                        photo:data.photo
                    });

                    response(r);         
                } else {
                    response(r);
                }
                
            },
            error: function(){
                response(r);
            }
        });
    },
    focus: function() {
        return false;
    },
    select: function( event, ui ) {        
        getDoctor(ui.item.id, ui.item.email, ui.item.phone_code, ui.item.phone, ui.item.name, ui.item.address, ui.item.country, ui.item.province, ui.item.regency, ui.item.district, ui.item.village, ui.item.postal_code, ui.item.note, ui.item.photo);
        return false;
    },
}).data("ui-autocomplete")._renderItem = function (ul, item) {
    var inner_html = '<div class="autocomplete">'+
        thumbnail('md','doctor','{{ $storage }}',item.photo,item.name)+
        '<div class="text">'+
            '<h1>'+item.name+'</h1>'+
            '<p>'+item.email+'</p>'+
            '<p>+'+item.phone_code+''+item.phone+'</p>'+
        '</div>'+
    '</div>';
    return $("<li></li>")
        .data("item.autocomplete", item)
        .append(inner_html)
        .appendTo(ul);
};


$("#polyclinic-group .border-group").click(function() {
    resetValidation('polyclinic');
});

$('input[name=email]').focus(function() {
    resetValidation('email');
});

$('input[name=phone]').focus(function() {
    resetValidation('phone');
});

$('input[name=name]').focus(function() {
    resetValidation('name');
});

$("form").submit(function(event) {
    event.preventDefault();
    resetValidation('polyclinic', 'name', 'phone', 'email');

    $("form button").attr("disabled", true);

    formData= new FormData();

    formData.append("validate", false);
    formData.append("id_doctor", $("input[name=id_doctor]").val());
    formData.append("id_polyclinic", $("select[name=polyclinic] option:selected").val());
    formData.append("email", $("input[name=email]").val());
    formData.append("phone_code", $("select[name='phone_code'] option:selected").val());
    formData.append("phone", parseInt($("input[name=phone]").val()));
    formData.append("name", $("input[name=name]").val());
    @if(Session::get('pcare'))
    formData.append("kode_bpjs", $("select[name=code_bpjs] option:selected").val());
    @else
    formData.append("kode_bpjs", '');
    @endif
    formData.append("address", $("input[name=address]").val());
    formData.append("country", $("select[name=country] option:selected").val());
    formData.append("province", $("select[name=province] option:selected").val());
    formData.append("regency", $("select[name=regency] option:selected").val());
    formData.append("district", $("select[name=district] option:selected").val());
    formData.append("village", $("select[name=village] option:selected").val());
    formData.append("role", $("select[name=role] option:selected").val());
    formData.append("postal_code", $("input[name=postal_code]").val());
    formData.append("note", nl2br($("input[name=note]").val()));
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
    $(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){

            $("form button").attr("disabled", false);
    
            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            if(!data.error) {

                if(!data.success) {
                    formValidate(true, ['polyclinic',data.errors.id_polyclinic, true], ['name',data.errors.name, true], ['phone',data.errors.phone, true], ['email',data.errors.email, true], ['form-add #role',data.errors.role, true]);

                    if(data.errors.id_doctor) {
                        notif(false,""+data.errors.id_doctor+"");
                    }

                    if(data.errors.message) {
                        notif(false,""+data.errors.message+"");
                    }
                } else {

                    $("select[name='polyclinic']").select2("val", "");
                    $("select[name='polyclinic']").val("");
                    $("input[name=name]").val("");
                    $("input[name=phone]").val("");
                    $("input[name=email]").val("");
                    $("input[name=address]").val("");
                    $("input[name=telephone]").val("");
                    $("input[name=cost]").val("");
                    $("input[name=note]").val("");
                    $("select[name='phone_code']").val('62').change();

                    notif(true,'<?php echo trans("validation.success_add_doctor"); ?>', false);

                    if(setup=="doctor") {
                        setup = "schedule";

                        $('#previous-step').removeClass('hide');
                        $('#step_1').addClass('hide');
                        $('#step_2').addClass('done');
                        $('#step_2').addClass('hide');
                        $('#step_3').removeClass('hide');
                        $('#step_3').removeClass('disable');
                        $('#step_4').addClass('hide');
                        $('#step_5').addClass('hide');
                        $('#step_6').addClass('hide');
                        $('#next-step').removeClass('hide');

                        setup_clinic('schedule');
                    }

                    setTimeout(function(){
                        loading('#/{{ $lang }}/doctor/'+data.id);
                        redirect('#/{{ $lang }}/doctor/'+data.id);
                    }, 500);

                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>