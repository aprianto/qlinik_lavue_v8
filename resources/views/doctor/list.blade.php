@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

@if($level=="user" || $level=="doctor")
<div class="panel-content">
    <div class="row hide" id="tutorial">
        <div class="col-md-12">
            <div class="widget">
                <div class="welcome-bar">
                    <div id="text-tutorial">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/dokter.png') }}" />
                    <h3>
                        {{ trans('messages.doctor') }}
                    </h3>
                    <a href="#/{{ $lang }}/help/doctor"><img src="{{ asset('assets/images/icons/misc/info.png') }}" class="info" /></a>
                </div>
                <div class="thumbnail-sec">
                    @if($level != 'doctor')
                    <a href="#/{{ $lang }}/doctor/create" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.add_doctor') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/doctor/schedule" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/jadwal-dokter.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.doctor_schedule') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/commission" onclick="loading($(this).attr('href'));repository(['search',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/komisi-dokter.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.commission_doctor') }}</span>
                        </div>
                    </a>
                    @endif
                    <a href="#/{{ $lang }}/doctor/calendar" onclick="loading($(this).attr('href'))">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/tampilan-kalender.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.calendar_view') }}</span>
                        </div>
                    </a>
                </div>                   
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search">               
                            <div class="col-md-4 form-change">
                                <div class="form-group content-column column-group" id="polyclinic-group">
                                    <div class="border-group">
                                        <select class="form-control" name="polyclinic">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>               
                            <div class="col-md-4 form-change">
                                <div class="form-group column-group">
                                    <div id="datepicker_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="date" />
                                        <span class="input-group-addon">
                                            <!--<span class="glyphicon glyphicon-calendar"></span>-->
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control input-icon" placeholder="{{ trans('messages.search_doctor_name') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-1"></div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>-->
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <!--<th class="hide-lg"></th>-->
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-md">{{ trans('messages.practice_schedule') }}</th>
                                <th class="hide-lg">{{ trans('messages.status_today') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="4" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                     <div class="pagination-sec bottom-sec">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('layouts.footer')

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

if("{{Request::is('*doctor*')}}") {
    $('.side-menus ul li a.active-bar').each(function(){
        $(this).removeClass('active-bar');
    });
    $("#link-doctor").addClass('active-bar')
}else {
    $(".side-menus ul li a").removeClass('active-bar')
}

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dNow = date.getDay();
dateNow = dayNow+'/'+monthNow+'/'+date.getFullYear();

$('#datepicker_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$("#datepicker_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic == "all") {
            polyclinic = "";
        }
        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate(day))
    });
});
    
if(setup=="doctor") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_doctor_add_doctor", ["link"=>"#/".$lang."/doctor/create"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('schedule');");
} else if(setup=="schedule") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_doctor_schedule_doctor", ["link"=>"#/".$lang."/doctor/schedule"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('service');");
} else if(setup=="service") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_doctor_add_service", ["link"=>"#/".$lang."/service/create"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('commission');");
} else if(setup=="commission") {
    $("#tutorial").removeClass("hide");
    $("#text-tutorial").html('<?php echo trans("tutorial.list_doctor_add_commission", ["link"=>"#/".$lang."/commission/create"]); ?>');
    $("#tutorial .close-content").attr("onclick","setup_clinic('medicine');");
}

function listPoly(val) {
    loading_content("#form-search #polyclinic-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/polyclinic",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(dataPoly){
            if(!dataPoly.error) {
                loading_content("#form-search #polyclinic-group", "success");

                var item = dataPoly.data;
                if("{{$level != 'doctor'}}") {
                    $("#form-search select[name='polyclinic']").html("<option value=''></option><option value='all'>{{ trans('messages.all_polyclinic') }}</option>");
                    for (var i = 0; i < item.length; i++) {
                        $("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"'>"+item[i].name+"</option>");
                    }
    
                    $("#form-search select[name='polyclinic']").select2({
                        placeholder: "{{ trans('messages.select_polyclinic') }}",
                        allowClear: false
                    });
                }else {
                    $("#form-search select[name='polyclinic']").html("<option selected value='{{$id_clinic}}'>{{$clinic->name}}</option>");
                }

                if(val!="") {
                    $("#form-search select[name='polyclinic']").val(val).trigger("change");
                }

                
            } else {
                loading_content("#form-search #polyclinic-group", "failed");

                $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
            }
        },
        error: function(){
            loading_content("#form-search #polyclinic-group", "failed");

            $("#form-search #polyclinic-group #loading-content").click(function(){ listPoly(val); });
        }
    })
}

listPoly(polyclinicRepo);

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='date']").val(unconvertDate(dateRepo));

var keySearch = searchRepo;
var keyPoly = polyclinicRepo;
var keyDate = dateRepo;
var numPage = pageRepo;

function search(page, submit, q, polyclinic, date) {
    
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(polyclinic=="" && submit=="false") {
        var polyclinic = keyPoly;
    }

    if(date=="" && submit=="false") {
        var date = keyDate;
    }


    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyPoly = polyclinic;
    keyDate = date;
    numPage = page;

    repository(['search',q],['polyclinic',polyclinic],['date',date],['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor",
        type: "GET",
        data: "q="+q+"&id_polyclinic="+polyclinic+"&date="+date+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");
            
            if(!data.error) {
                $(".table").addClass("table-hover");

                $(".pagination-sec").removeClass("hide");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    if(date=='') {
                        date = new Date();
                        month = ((date.getMonth()+1)+100).toString().slice(-2);
                        day = (date.getDate()+100).toString().slice(-2);
                        date = date.getFullYear()+'-'+month+'-'+day;    
                    }

                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            var status = "-";
                            var type = "";

                            if(item[i].schedule!=null) {
                                var get_end_date_active = "9999-12-31";

                                if(item[i].schedule.end_date_active!="0000-00-00") {
                                    get_end_date_active = item[i].schedule.end_date_active;
                                }

                                var get_end_date_leave = "9999-12-31";

                                if(item[i].schedule.end_date_leave!="0000-00-00") {
                                    get_end_date_leave = item[i].schedule.end_date_leave;
                                }

                                if(Date.parse(date.replace(/\-/g,'/'))>=Date.parse(item[i].schedule.start_date_leave.replace(/\-/g,'/')) && Date.parse(date.replace(/\-/g,'/'))<=Date.parse(get_end_date_leave.replace(/\-/g,'/'))) {
                                    status = "<span class='status-nonactive'>{{ trans('messages.leave') }}</span>";
                                    type = "false";
                                } else if(Date.parse(date.replace(/\-/g,'/'))>=Date.parse(item[i].schedule.start_date_active.replace(/\-/g,'/')) && Date.parse(date.replace(/\-/g,'/'))<=Date.parse(get_end_date_active.replace(/\-/g,'/'))) {

                                    if(dNow==0) { dNow=7; }

                                    itemTime = item[i].schedule.daily;

                                    var numTime = 0;
                                    for (var j = 0; j < itemTime.length; j++) {
                                        if(itemTime[j].day==dNow) {
                                            numTime++;
                                        }
                                    }

                                    if(numTime==0) {
                                        status = "<span class='status-nonactive'>{{ trans('messages.not_available') }}</span>";
                                        type = "";
                                    } else {
                                        status = "<span class='status-active'>{{ trans('messages.available') }}</span>";
                                        type = "true";
                                    }

                                }
                            }

                            var row = "";
                            var status_row = "";
                            
                            /*if(type=="true") {
                                row = "row-green";
                                status_row = "status-success";
                            } else if(type=="false") {
                                row = "row-red";
                                status_row = "status-failed";
                            } else {
                                status_row = "status-failed";
                            }*/

                            tr = $('<tr class="'+row+'" />');
                            //tr.append("<td width='20px' class='hide-lg'><i class='fa fa-circle "+status_row+"'></i></td>");
                            //tr.append("<td class='photo'>"+thumbnail('sm','doctor','{{ $storage }}',item[i].photo,item[i].name)+"</td>");

                            var dataTime = "";
                            var dataTimeHidden = "";

                            if(item[i].schedule!=null) {

                                itemTime = item[i].schedule.daily;

                                if(itemTime!=null) {

                                    for (var j = 0; j < itemTime.length; j++) {
                                        
                                        if(j==0) {
                                            dataTime += '<h2>'+getDay(itemTime[j].day)+'</h2><br />';
                                            dataTimeHidden += '<br /><span class="general"><b>'+getDay(itemTime[j].day)+'</b></span><br />';
                                        } else if(itemTime[j].day!=itemTime[j-1].day) {
                                            dataTime += '<br /><br /><h2>'+getDay(itemTime[j].day)+'</h2><br />';
                                            dataTimeHidden += '<br /><span class="general"><b>'+getDay(itemTime[j].day)+'</b></span><br />';
                                        }

                                        if(j!=0 && itemTime[j].day==itemTime[j-1].day) {
                                            dataTime +='<br />';
                                        }

                                        dataTime += '<span class="text">'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+'</span>';
                                        dataTimeHidden += '<span class="text">'+convertTime(itemTime[j].start_time)+' - '+convertTime(itemTime[j].end_time)+'</span><br />';
                                    }
                                }
                            }

                            tr.append("<td><a href='#/{{ $lang }}/doctor/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a><span class='type'>" + item[i].polyclinic.name + "</span>"+
                                '<div class="sub show-md">'+
                                    '<span class="show-sm">'+
                                    dataTimeHidden+
                                    '</span>'+
                                    //'<span class="text margin right">' + status + '</span>'+
                                    '<span class="text"><br />' + status + '</span>'+
                                '</div>'+
                                "</td>");


                            tr.append("<td class='hide-md'>"+dataTime+"</td>");
                            tr.append("<td class='hide-lg'>"+status+"</td>");

                            var action = "<a href='#/{{ $lang }}/doctor/schedule/id/"+item[i].id+"' class='btn-table btn-blue' onclick='loading($(this).attr(\"href\"))'><img src=\"{{ asset('assets/images/icons/action/schedule.png') }}\" /> {{ trans('messages.schedule') }}</a>"
                                /*+"<a href='#/{{ $lang }}/doctor/"+item[i].id+"' class='btn-table btn-blue' onclick='loading($(this).attr(\"href\"))'><span class='fa fa-search'></span> {{ trans('messages.detail') }}</a>"*/
                                +"<a href='#/{{ $lang }}/doctor/"+item[i].id+"/edit' class='btn-table btn-orange' onclick='loading($(this).attr(\"href\"))'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /> {{ trans('messages.edit') }}</a>"
                                +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /> {{ trans('messages.delete') }}</a>";

                            tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    
                    $(".pagination-sec").addClass("hide");

                    if(page==1 && q=='' && polyclinic=='' && date=='') {
                        $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.empty_doctor') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q, polyclinic, keyDate);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, keyDate); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q, polyclinic, keyDate); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/clinic/doctor/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){

            if(!data.error) {
                if(data.success==true) {
                    search(numPage,"false", keySearch, keyPoly, keyDate);
                    notif(true,"{{ trans('validation.success_delete_doctor') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$("#form-search input[name='date']").keydown(function (e) {
    if (e.keyCode == 13) {
        var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
        if(polyclinic=="all") {
            polyclinic = "";
        }
        search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()));
    }
});

$("#form-search select[name='polyclinic']").on('change' , function(e) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    if (polyclinic == "all") {
        polyclinic = "";
    }
    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()))
});


$("#form-search").submit(function(event) {
    var polyclinic = $("#form-search select[name=polyclinic] option:selected").val();
    if(polyclinic=="all") {
        polyclinic = "";
    }
    search(1,"true", $("#form-search input[name=q]").val(), polyclinic, convertDate($("#form-search input[name=date]").val()));
});

search(pageRepo, "false", searchRepo, polyclinicRepo, dateRepo);

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer)
    let polyclinic = $("#form-search select[name=polyclinic] option:selected").val() == "all" ? "" : $("#form-search select[name=polyclinic] option:selected").val();

    if (e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", e.target.value, polyclinic, convertDate($("#form-search input[name=date]").val()));
        }, 2000);
    }
});

</script>

@include('_partial.confirm_delele')

@else

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-user-md"></i>
                    <h3>
                        {{ trans('messages.doctor') }}
                    </h3>
                </div>  
                <div class="search-sec">
                    <form  id="form-search">
                        <div class="row search"> 
                            <div class="col-md-11">
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_doctor') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2">{{ trans('messages.name') }}</th>
                                    <th>{{ trans('messages.polyclinic') }}</th>
                                    <th>{{ trans('messages.address') }}</th>
                                    <th>{{ trans('messages.last_login') }}</th>
                                </tr>
                            </thead>
                            <tbody id="table">
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="5" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="5" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                     <div class="pagination-sec bottom-sec">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/doctor",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");
            
            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            var polyclinic = "";
                            for(var j=0; j < item[i].polyclinic.length; j++) {
                                polyclinic = polyclinic + "<span class='category'>"+item[i].polyclinic[j].name+"</span>";
                            }

                            tr = $('<tr />');
                            tr.append("<td class='photo'>"+thumbnail('sm','doctor','{{ $storage }}',item[i].photo,item[i].name)+"</td>");
                            tr.append("<td><a href='#/{{ $lang }}/doctor/"+item[i].id+"' onclick='loading($(this).attr(\"href\"))'><h1>" + item[i].name + "</h1></a><span class='type'>"+item[i].email+"</span><br /><span class='text'>+"+item[i].phone_code+""+item[i].phone+"</span></td>");
                            tr.append("<td>"+polyclinic+"</td>");
                            tr.append("<td>"+nullToEmpty(item[i].address)+" "+nullToEmpty(item[i].village.name)+" "+nullToEmpty(item[i].district.name)+" "+nullToEmpty(item[i].regency.name)+" "+nullToEmpty(item[i].province.name)+" "+nullToEmpty(item[i].country.name)+"</td>");
                            tr.append("<td>" + getFormatDateTime(item[i].last_login) + "</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page,"false", q); });
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page,"false", q); });
        }
    })
}

$("#form-search").submit(function(event) {
    search(1,"true", $("#form-search input[name=q]").val());
});

search(pageRepo, "false", searchRepo);

</script>

@endif
