@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div>
<div class="account-user-sec">
	<div class="account-sec">
		@include('layouts.main-header')
		<div class="acount-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="contact-sec-center">
							<div class="row">
								<div class="col-md-12">	
									<div class="widget-title">
										<h3>{{ trans('messages.title_login') }}</h3>
										<span>{{ trans('messages.as_doctor') }}</span>
									</div>
									<div class="account-form alert-notif">
										<form>
											<div>
												<div class="feild" id="email-group">
													<label>{{ trans('messages.email') }}</label>
													<input type="text" id="email" name="email" placeholder="{{ trans('messages.input_email') }}" />
													<span class="help-block"></span>
												</div>
												<div class="feild" id="password-group">
													<label>{{ trans('messages.password') }}</label>
													<input type="password" id="password" name="password" placeholder="{{ trans('messages.input_password') }}" />
													<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password')"></span>
													<span class="help-block"></span>
												</div>
												<div class="feild last-item">
													<button type="submit" class="btn-right button-login">
						                                 <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                                {{ trans('messages.login') }}
						                            </button>
												</div>
											</div>
										</form>
									</div>
								</div>
                                
                            </div>
                        </div>
                        <div class="forgot-password-docter text-center">
                            <h4>{{ trans('messages.forgot_password_question') }}</h4>
                            <button class="button-transaparent" title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/doctor/forgot-password'">{{ trans('messages.reset_password') }}</button>
                        </div>
					</div>
					@include('layouts.footer-login')
				</div>
			</div>
        </div>
        @include('doctor.select_clinic')
	</div>
</div>


<script>


$("input[name=email]").bind("focus", function () {
    resetValidation('email');
});

$("input[name=password]").bind("focus", function () {
    resetValidation('password');
});

$("#link-login").html("<a title='' href='#/{{ $lang }}/login'>{{ trans('messages.clinic_application') }}</a>");
$("#link-login-rs").html("<a title='' href='#/{{ $lang }}/login'>{{ trans('messages.clinic_application') }}</a>");

$('input[name=phone]').focus(function() {
    resetValidation('email');
});

$('input[name=password]').focus(function() {
    resetValidation('password');
});

$("form").submit(function(event) {

	resetValidation('email', 'password');
	
    var email = $("#email").val();
    var password = $("#password").val();

    var error = false;

    if(email=="") {
    	error = true;
    	validateElementForm('email', 'success', 'error', "{{ trans('validation.empty_email') }}", true);
    } else {
    	validateElementForm('email', 'error', 'success', '', false);
 	}

    if(password=="") {
    	error = true;
    	validateElementForm('password', 'success', 'error', "{{ trans('validation.empty_password') }}", true);
    } else {
    	validateElementForm('password', 'error', 'success', '', false);
 	}

 	if(error==false) {
 		event.preventDefault(); 		

    	$(".feild button").attr("disabled", true);

		$(".feild button").addClass("loading");
        $(".feild button span").removeClass("hide");
 		formData= new FormData();
        formData.append("email", email);
        formData.append("password", password);
        $.ajax({
            url: "{{ $lang }}/doctor/login",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $(".feild button").attr("disabled", false);
        
                $(".feild button").removeClass("loading");
                $(".feild button span").addClass("hide");

                if(!data.error) {
	                if(data.success==true) {
	                	notif(true,"{{ trans('validation.success_login') }}");	

						setTimeout(function(){
							var item = data.clinics;
							for (var i = 0; i < item.length; i++) {
								$("#form-search select[name='polyclinic']").append("<option value='"+item[i].id+"' name='"+item[i].indonesian_name+"' id='"+item[i].id_polyclinic+"'>"+item[i].name+"</option>");
							}

							$("#form-search select[name='polyclinic']").select2({
								placeholder: "{{ trans('messages.select_polyclinic') }}",
								allowClear: false
							});
							$(".select2-container--default").css("height", "0px !important")
							$("#selectClinic").modal('show')

						}, 1000);
	                } else {
	                	notif(false,"{{ trans('validation.failed_login') }}");
	                }
                } else {
                	notif(false,"{{ trans('validation.failed') }}", true);
                }
            },
			error: function(){
                $(".feild button").attr("disabled", false);
				
				$(".feild button").removeClass("loading");
                $(".feild button span").addClass("hide");

				notif(false,"{{ trans('validation.failed') }}");
			}
        })

    }
});

</script>



