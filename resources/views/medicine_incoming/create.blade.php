<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.add_medicine_incoming') }}</h5>
            </div>
            <form id="form-add" class="">
                <div class="modal-body alert-notif">
                    <div class="form-group" id="medicine-group">
                        <label class="control-label">{{ trans('messages.inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <input placeholder="{{ trans('messages.choice_inventory') }}" name="medicine" class="form-control" data-id="" />
                        </div>
                        <span id="loading-medicine" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="batch_no-group">
                        <label class="control-label">{{ trans('messages.batch_no') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_batch_no') }}" name="batch_no">
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group column-group" id="exp_date-group">
                                <label class="control-label">{{ trans('messages.exp_date') }} <span>*</span></label>
                                <div id="datepicker_exp_date" class="input-group date">
                                    <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="exp_date" />
                                    <span class="input-group-addon">
                                        <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                    </span>
                                </div>
                                <span class="help-block"></span>  
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="unit-group">
                        <label class="control-label">{{ trans('messages.unit') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="unit">
                                <option value=""></option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="quantity-group">
                        <label class="control-label">{{ trans('messages.quantity') }} <span>*</span></label>
                        <input  type="text" name='quantity' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off" />
                        <span class="help-block"></span>
                        <div id="notes"></div>
                    </div>
                    <div class="form-group" id="price-group">
                        <label class="control-label">{{ trans('messages.purchase_price') }} <span>*</span></label>
                        <input  type="text" name='price' class="form-control" placeholder="{{ trans('messages.input_price') }}" onkeydown="number(event)" autocomplete="off" oninput="convertNumber(this)" />
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="company-group">
                        <label class="control-label">{{ trans('messages.distributor') }} <span>*</span></label>
                        <input type="text" name="company" class="form-control" placeholder="{{ trans('messages.input_company') }}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

var data_unit = [];

$("#form-add select[name='distributor']").select2({
    placeholder: "Masukkan distributor"
});

$("#form-add select[name='unit']").select2({
    placeholder: "{{ trans('messages.choice_unit') }}"
});

$('#form-add #datepicker_exp_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

let timer = "";
$("#form-add input[name='medicine']").autocomplete({
    minLength: 3,
    source: function(request, response) {
        let temp = [];
        clearTimeout(timer);
        timer = setTimeout(() => {
            $.ajax({
                type: "GET",
                url: "{{ $api_url }}/{{ $lang }}/medicine",
                data: "q="+request.term+"",
                beforeSend: function() {
                    $("#form-add #loading-medicine").removeClass("hide");
                },
                success: function(results) {
                    $("#form-add #loading-medicine").addClass("hide");

                    if (!results.error) {
                        for (i = 0; i < results.data.length; i++) {
                            let medicine = results.data[i];
                            temp.push({
                                id: medicine.id,
                                name: medicine.name,
                                value: medicine.name
                            })
                        }
                        response(temp);
                    }
                },
                error: function(xhr) {
                    $("#form-add #loading-medicine").addClass("hide");
                    response(temp);
                }
            });
        }, 2000);
    },
    focus: function() {
        return false;
    },
    select: function(event, ui) {
        listUnitInventory(ui.item.id)
        $("#form-add input[name='medicine']").val(ui.item.name)
        $("#form-add input[name='medicine']").data("id", ui.item.id)
        $("#form-add #notes").html("")
        return false;
    }
});

var obj_unit = "";
var last_el = false;

$("#form-add select[name='unit']").on('change', function() {
    $("#form-add input[name='quantity']").val("");
    $("#form-add input[name='price']").val("");
    if($(this).val() != "") {
        last_el = false;
        let i = data_unit.find(el => el.id === $(this).val()).weight
        if (i !== data_unit.length - 1) {
            obj_unit = data_unit[i+1]
        } else {
            obj_unit = data_unit[i]
            last_el = true;
        }
        
        setNotesPrice(0, obj_unit.name)
    }
});

$("#form-add input[name='quantity']").on('input', function(e) {
    if(obj_unit != "") {
        if (last_el) {
            setNotesPrice(e.target.value, obj_unit.name)
        }else {
            let calculation = e.target.value * obj_unit.amount_ratio;
            setNotesPrice(calculation, obj_unit.name)
        }
    }
});

function setNotesPrice (amount, unit) {
    $("#form-add #notes").html("<p>"+amount+" "+unit+"</p>");
}

function listUnitInventory(id) {
    loading_content("#form-add #unit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/" + id + "/unit",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                list_unit = data.data;
                loading_content("#form-add #unit-group", "success");
                $("#form-add #unit-group select").html("<option value=''></option>");
                if( list_unit.length > 0 ) {
                    for (i = 0; i < list_unit.length; i++) {
                        data_unit[i] = list_unit[i]
                        $("#form-add select[name='unit']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>');
                    }
                }else {
                    notif(false, "{{ trans('validation.no_unit') }}")
                }
                $("#form-add select[name='unit']").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                });
            }else {
                loading_content("#form-add #unit-group", "failed");
                $("#form-add #unit-group #loading-content").click(function(){ listUnitInventory(id); });
            }
        },
        error : function() {
            loading_content("#form-add #unit-group", "failed");
            $("#form-add #unit-group #loading-content").click(function(){ listUnitInventory(id); });
        }
    });
}

function resetAdd() {
    obj_unit = "";
    last_el = false;
    resetValidation('form-add #medicine', 'form-add #batch_no', 'form-add #exp_date', 'form-add #quantity' , 'form-add #price', 'form-add #company');

    $("#form-add input[name='medicine']").val("").trigger("change");
    $("#form-add input[name=batch_no]").val("");
    $("#form-add input[name='exp_date']").val("");
    $("#form-add input[name=quantity]").val("");
    $("#form-add input[name=price]").val("");
    $("#form-add input[name=company]").val("");
    $("#form-add select[name='unit']").val("").trigger("change");
    $("#form-add #notes").html("");
    $("#form-add #unit-group select").html("<option value=''></option>");
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$("#form-add #medicine-group .border-group").click(function() {
    resetValidation('form-add #medicine');
});

$('#form-add input[name=batch_no]').focus(function() {
    resetValidation('form-add #batch_no');
});

$("#form-add #exp_date-group").click(function() {
    resetValidation('form-add #exp_date');
});

$('#form-add input[name=quantity]').focus(function() {
    resetValidation('form-add #quantity');
});

$('#form-add input[name=price]').focus(function() {
    resetValidation('form-add #price');
});

$('#form-add input[name=company]').focus(function() {
    resetValidation('form-add #company');
});

$('#form-add select[name=unit]').change(function() {
    resetValidation('form-add #unit');
});

function convertNumber(field) {
    $(field).val(function(index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    });
}

$("#form-add").submit(function(event) {
    event.preventDefault();

    resetValidation('form-add #medicine', 'form-add #batch_no', 'form-add #exp_date', 'form-add #quantity', 'form-add #price', 'form-add #company');
    
    let unit = $("#form-add select[name='unit'] option:selected").val();
    let id_medicine = $("#form-add input[name='medicine']").data("id");
    let distributor = $("#form-add input[name=company]").val();
    let batch_no = $("#form-add input[name=batch_no]").val();
    let quantity = $("#form-add input[name=quantity]").val();
    let price = $("#form-add input[name=price]").val().replace(/\D/g, "");
    let exp_date = $("#form-add input[name='exp_date']").val();

    if(!validateCreate(unit, id_medicine, distributor, batch_no, quantity, price, exp_date)) return;

    formData= new FormData();
    formData.append("validate", false);
    formData.append("id_medicine", id_medicine);
    formData.append("batch_no", batch_no);
    formData.append("exp_date", convertDate(exp_date));
    formData.append("quantity", quantity);
    formData.append("distributor", distributor);
    formData.append("price", price);
    formData.append("id_unit", unit);
        
    $("#form-add button").attr("disabled", true);
    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-incoming",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-add button").attr("disabled", false);
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");
            if(!data.error) {
                if(!data.success) {
                    notif(false, ""+data.message+"")
                }else {
                    resetAdd();
                    $("#add").modal("toggle");
                    search(numPage, "false", keySearch, keyFromDate, keyToDate, keyExpFromDate, keyExpToDate);
                    notif(true,"{{ trans('validation.success_add_medicine_incoming') }}");
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    });
});

function validateCreate(unit, id_medicine, distributor, batch_no, quantity, price, exp_date) {
    !unit && formValidate(true, ["form-add #unit", "{{ trans('messages.choice_unit') }}", true]);
    !id_medicine && formValidate(true, ["form-add #medicine", "{{ trans('messages.choice_inventory') }}", true]);
    !distributor && formValidate(true, ["form-add #company", "{{ trans('validation.empty_company_inventory') }}", true]);
    !batch_no && formValidate(true, ["form-add #batch_no", "{{ trans('validation.empty_no_batch') }}", true]);
    !quantity && formValidate(true, ["form-add #quantity", "{{ trans('validation.empty_unit_amount') }}", true]);
    !price && formValidate(true, ["form-add #price", "{{ trans('validation.empty_qty_price') }}", true]);
    !exp_date && formValidate(true, ["form-add #exp_date", "{{ trans('validation.empty_exp_date') }}", true]);
    
    return unit && id_medicine && distributor && batch_no && quantity && price && exp_date
}


</script>