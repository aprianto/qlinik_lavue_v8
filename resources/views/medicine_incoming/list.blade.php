@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                    <h3>
                        {{ trans('messages.medicine_incoming') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a href="#/{{ $lang }}/medicine" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans('messages.list_medicine') }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-incoming" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['exp_from_date',''],['exp_to_date',''],['page','1']);" class="active">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-masuk.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_incoming") }}</span>
                        </div>
                    </a>
                    <a href="#/{{ $lang }}/medicine-outgoing" onclick="loading($(this).attr('href'));repository(['search',''],['from_date',''],['to_date',''],['page','1']);">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/settings/menu-obat-keluar.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.medicine_outgoing") }}</span>
                        </div>
                    </a>
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <img src="{{ asset('assets/images/icons/link/add.png') }}" />
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_medicine_incoming") }}</span>
                        </div>
                    </a>
                    <div class="dropdown pull-right">
                      <a href="" data-toggle="dropdown">
                          <div class="image">
                              <div class="canvas">
                                  <img src="{{ asset('assets/images/icons/header/more-horizontal.png') }}" />
                              </div>
                          </div>
                          <div class="text">
                              <span>Lainnya</span>
                          </div>
                      </a>
                      <ul class="dropdown-menu" style="z-index: 0;">
                        <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                      </ul>
                    </div>
                </div>
                <div class="dropdown-rs">
                    <ul class="wrapper-dropdown-rs dropdown-scroll">
                        <li><a data-toggle="modal" data-target="#import"><img src="{{ asset('assets/images/icons/action/import.png') }}" />Import Data</a></li>
                        <li><a data-toggle="modal" data-target="#export"><img src="{{ asset('assets/images/icons/action/export.png') }}" />Export Data</a></li>
                    </ul>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-3">
                                <div class="form-group content-column" id="type-group">
                                    <div class="border-group">
                                        <select class="form-control" name="type">
                                            <option value="all">Semua Obat</option>
                                            <option value="dpho">Obat DPHO</option>
                                        </select>
                                    </div>
                                    <span id="loading-content" class="loading-content fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group column-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_medicine_incoming') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row search">
                            <div class="col-md-3">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.from_date') }}</label>
                                    <div id="datepicker_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.to_date') }}</label>
                                    <div id="datepicker_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.exp_date_from') }}</label>
                                    <div id="datepicker_exp_from_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="exp_from_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group column-group">
                                    <label class="control-label">{{ trans('messages.exp_date_to') }}</label>
                                    <div id="datepicker_exp_to_date" class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.select_date') }}" name="exp_to_date" />
                                        <span class="input-group-addon">
                                            <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>
                                <th class="hide-md">{{ trans('messages.date_of_entry') }}</th>
                                <th class="hide-md">{{ trans('messages.quantity') }}</th>
                                <th class="hide-lg">{{ trans('messages.unit') }}</th>
                                <th class="hide-lg">{{ trans('messages.purchase_price') }}</th>
                                <th class="hide-lg">{{ trans('messages.exp_date') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="12" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>
$(".pull-right").click(function(){ 
    $(".wrapper-dropdown-rs").toggleClass("open-rs")
});

$("input[name='export_key']").val('medicines_incoming');
$("input[name='import_key']").val('medicines_incoming');

$("#form-search select[name='type']").select2({
    placeholder: "Silahkan Pilih"
});

$('#datepicker_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_exp_from_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$('#datepicker_exp_to_date').datetimepicker({
    locale: "{{ $lang }}",
    format: 'DD/MM/YYYY'
});

$("#datepicker_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_day: day})
    });
});

$("#datepicker_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_day: day})
        
    });
});

$("#datepicker_exp_from_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({from_exp_day: day})
    });
});

$("#datepicker_exp_to_date").on("dp.show", function(e) {
    $('div tbody').on('click', 'tr > .day', function() {
        var day = $(this).data('day');
        filterSearch({to_exp_day: day})
        
    });
});

$("#form-search input[name='q']").val(searchRepo);
$("#form-search input[name='from_date']").val(unconvertDate(from_dateRepo));
$("#form-search input[name='to_date']").val(unconvertDate(to_dateRepo));
$("#form-search input[name='exp_from_date']").val(unconvertDate(exp_from_dateRepo));
$("#form-search input[name='exp_to_date']").val(unconvertDate(exp_to_dateRepo));

var keySearch = searchRepo;
var keyFromDate = from_dateRepo;
var keyToDate = to_dateRepo;
var keyExpFromDate = exp_from_dateRepo;
var keyExpToDate = exp_to_dateRepo;
var numPage = pageRepo;

function search(page, submit, q, from_date, to_date, exp_from_date, exp_to_date) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    if(from_date=="" && submit=="false") {
        var from_date = keyFromDate;
    }

    if(to_date=="" && submit=="false") {
        var to_date = keyToDate;
    }

    if(exp_from_date=="" && submit=="false") {
        var exp_from_date = keyExpFromDate;
    }

    if(exp_to_date=="" && submit=="false") {
        var exp_to_date = keyExpToDate;
    }

    var type = $("#form-search select[name=type] option:selected").val();
    if(type == 'dpho') {
        type = true;
    }else{
        type = '';
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    keyFromDate = from_date;
    keyToDate = to_date;
    keyExpFromDate = exp_from_date;
    keyExpToDate = exp_to_date;
    numPage = page;

    repository(['search',q],['from_date',from_date],['to_date',to_date],['exp_from_date',exp_from_date],['exp_to_date',exp_to_date],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-incoming",
        type: "GET",
        data: "q="+q+"&is_dpho="+type+"&page="+page+"&per_page=10&from_date="+from_date+"&to_date="+to_date+"&exp_from_date="+exp_from_date+"&exp_to_date="+exp_to_date+"",
        processData: false,
        contentType: false,
        success: function(data){
            $("#table").empty();
            $("#loading-table").addClass("hide");

            if ( !data.error ) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {

                    $(".pagination-sec").removeClass("hide");
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {

                        tr = $('<tr/>');                        
                        tr.append("<td>" + "<span class='title-list-table'>" + item[i].medicine.name + "</span>" + "<p class='small-text-list'>" + item[i].medicine.code + " "+(item[i].medicine.kode_dpho != null ? '/ '+ item[i].medicine.kode_dpho : '')+"</p>" +
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>No. batch : " + item[i].batch_no + "</span>"+
                                '</div>'+
                                '<div class="sub show-md">'+
                                    "<span class='general-rs'>{{ trans('messages.date_of_entry') }}: " + getFormatDate(item[i].date) + "</span>"+
                                '</div>'+
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>{{ trans('messages.quantity') }}: " + item[i].quantity + "</span>"+
                                '</div>'+
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>{{ trans('messages.unit') }}: "+item[i].unit.name+"</span>"+
                                '</div>'+
                                '<div class="sub show-sm">'+
                                    "<span class='general-rs'>{{ trans('messages.purchase_price') }}: "+formatCurrency('{{ $lang }}','Rp',item[i].buy_price)+"</span>"+
                                '</div>'+
                                '<div class="sub show-md">'+
                                    "<span class='general-rs'>{{ trans('messages.exp_date') }}: " + getFormatDate(item[i].exp_date) + "</span>"+
                                '</div>'+
                                "</td>");
                        tr.append("<td class='hide-md'><span>" + getFormatDate(item[i].date) + "</span><p class='small-text-list'>No. Batch "+item[i].batch_no+"</p></td>");
                        tr.append("<td class='hide-md'>" + item[i].quantity + "</td>");
                        tr.append("<td class='hide-lg'>"+item[i].unit.name+"</td>");
                        tr.append("<td class='hide-lg'>" + formatCurrency('{{ $lang }}','Rp',item[i].buy_price) + "</td>");
                        tr.append("<td class='hide-lg'>" + getFormatDate(item[i].exp_date) + "</td>");

                        let hide = item[i].has_transaction ? "hide" : "";
                        var action = "<a data-toggle='modal' data-target='#edit' data-id='"+item[i].id+"' class='btn-table btn-orange "+hide+"'><img src=\"{{ asset('assets/images/icons/action/edit.png') }}\" /></a>"
                            +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red "+hide+"'><img src=\"{{ asset('assets/images/icons/action/delete.png') }}\" /></a>";

                        tr.append("<td>"+
                                action+
                                '<div class="dropdown">'+
                                    '<a class="dropdown-toggle more" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="ion-android-more-vertical"></span></a>'+
                                    '<ul class="dropdown-menu dropdown-menu-right">'+
                                        action+
                                    '</ul>'+
                                '</div>'+
                                "</td>");

                        $("#table").append(tr);
                        no++;
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");

                    $(".pagination-sec").addClass("hide");
                        
                    if(page==1 && q=='' && from_date=='' && to_date=='' && exp_from_date=='' && exp_to_date=='') {
                        $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.empty_medicine_incoming') }}</td></tr>");
                    }
                }
                pages(page, total, per_page, current_page, last_page, from, to, q, from_date, to_date, exp_from_date, exp_to_date);
            } else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date, exp_from_date, exp_to_date); });
            }
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");
            $("#reload .reload").click(function(){ search(page, "false", q, from_date, to_date, exp_from_date, exp_to_date); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "user");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-incoming/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch, keyFromDate, keyToDate, keyExpFromDate, keyExpToDate);
                    notif(true,"{{ trans('validation.success_delete_medicine_incoming') }}");    
                }
            } else {
                notif(false,data.message);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

function filterSearch(arg) {
    var from_date = $("#form-search input[name=from_date]").val();
    var to_date = $("#form-search input[name=to_date]").val();
    var from_exp_date = $("#form-search input[name=exp_from_date]").val();
    var to_exp_date = $("#form-search input[name=exp_to_date]").val();
    
    if (arg) {
        if (arg.from_day) {
            from_date = arg.from_day
        }else if (arg.to_day) {
            to_date = arg.to_day
        }else if(arg.from_exp_day){
            from_exp_date = arg.from_exp_day
        }else if(arg.to_exp_day) [
            to_exp_date = arg.to_exp_day
        ]
    }
    
    search(1,"true", $("input[name=q]").val(), convertDate(from_date), convertDate(to_date), convertDate(from_exp_date), convertDate(to_exp_date));
}

let timer = "";
$("#form-search input[name='q']").on('input', function(e){
    clearTimeout(timer);
    if(e.target.value.length > 2 || e.target.value.length == 0) {
        timer = setTimeout(() => {
            search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
        }, 2000);
    }
});

$("#form-search").submit(function(event) {
    search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
});

$("#form-search input[name='from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
    }
});

$("#form-search input[name='to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
    }
});

$("#form-search input[name='exp_from_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
    }
});

$("#form-search input[name='exp_to_date']").keydown(function (e) {
    if (e.keyCode == 13) {
        search(1,"true", $("input[name=q]").val(), convertDate($("#form-search input[name=from_date]").val()), convertDate($("#form-search input[name=to_date]").val()), convertDate($("#form-search input[name=exp_from_date]").val()), convertDate($("#form-search input[name=exp_to_date]").val()));
    }
});

search(pageRepo,"false",searchRepo,keyFromDate, keyToDate,keyExpFromDate, keyExpToDate);


</script>

@include('medicine_incoming.create')

@include('medicine_incoming.edit')

@include('_partial.confirm_delele')

@include('docs.export_with_date')

@include('docs.import')

@include('docs.import_success')
