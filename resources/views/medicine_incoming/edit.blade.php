<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="{{ asset('assets/images/icons/settings/menu-obat.png') }}" />
                <h5 class="modal-title" id="exampleModalLongTitle">{{ trans('messages.edit_medicine_incoming') }}</h5>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="hide">
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="form-group" id="medicine-group">
                        <label class="control-label">{{ trans('messages.inventory') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="medicine" disabled="disabled">
                                <option value=""></option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="batch_no-group">
                        <label class="control-label">{{ trans('messages.batch_no') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.input_batch_no') }}" name="batch_no">
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group column-group" id="exp_date-group">
                                <label class="control-label">{{ trans('messages.exp_date') }} <span>*</span></label>
                                <div id="datepicker_exp_date" class="input-group date">
                                    <input class="form-control" type="text" placeholder="{{ trans('messages.select_date') }}" name="exp_date" />
                                    <span class="input-group-addon">
                                        <img src="{{ asset('assets/images/icons/action/select-date.png') }}" />
                                    </span>
                                </div>
                                <span class="help-block"></span>  
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="unit-group">
                        <label class="control-label">{{ trans('messages.unit') }} <span>*</span></label>
                        <div class="border-group">
                            <select class="form-control" name="unit">
                                <option value=""></option>
                            </select>
                        </div>
                        <span id="loading-content" class="loading-autocomplete fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        <span class="help-block"></span>
                    </div>
                    
                    <div class="form-group" id="quantity-group">
                        <label class="control-label">{{ trans('messages.quantity') }} <span>*</span></label>
                        <input  type="text" name='quantity' class="form-control" placeholder="{{ trans('messages.input_quantity') }}" onkeydown="number(event)" autocomplete="off" />
                        <span class="help-block"></span>
                        <div id="notes"></div>
                    </div>

                    <div class="form-group" id="price-group">
                        <label class="control-label">{{ trans('messages.purchase_price') }} <span>*</span></label>
                        <input  type="text" name='price' class="form-control" placeholder="{{ trans('messages.input_price') }}" onkeydown="number(event)" autocomplete="off" />
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="company-group">
                        <label class="control-label">{{ trans('messages.distributor') }} <span>*</span></label>
                        <input type="text" name="company" class="form-control" placeholder="{{ trans('messages.input_company') }}">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#form-edit #datepicker_exp_date').datetimepicker({
    locale: '{{ $lang }}',
    format: 'DD/MM/YYYY'
});

$("#form-edit select[name='medicine']").select2({
    placeholder: "{{ trans('messages.choice_inventory') }}"
});

var data_inventory = "";
function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-incoming/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if(!data.error) {
                data_inventory = data
                $('#loading-edit').addClass('hide');
                $('#form-edit').removeClass('hide');
                $("#form-edit select[name='medicine']").html("<option value=''></option> ");
                
                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=batch_no]").val(data.batch_no);
                $("#form-edit input[name='exp_date']").val(unconvertDate(data.exp_date));
                $("#form-edit input[name=company]").val(data.distributor);
                $("#form-edit select[name='medicine']").append("<option value='"+data.medicine.id+"'>"+data.medicine.name+"</option>");

                $("#form-edit select[name=medicine]").val(data.medicine.id).trigger("change");
                $("#form-edit select[name=medicine]").prop("disabled", true);

                listUnitMedicine(data.id_unit, data.id_medicine)                
            } else {
                $('#loading-edit').addClass('hide');
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }
        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    })
}
var list_unit = "";
function listUnitMedicine (val, id_medicine) {
    loading_content("#form-edit #unit-group", "loading");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine/" + id_medicine + "/unit",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            if ( !data.error ) {
                list_unit = data.data;
                loading_content("#form-edit #unit-group", "success");
                $("#form-edit #unit-group select").html("<option value=''></option>");
                if( list_unit.length > 0 ) {
                    for (i = 0; i < list_unit.length; i++) {
                        $("#form-edit select[name='unit']").append('<option value='+list_unit[i].id+'>'+list_unit[i].name+'</option>');
                    }
                }

                if(val != "") {
                    $("#form-edit select[name='unit']").val(val).trigger("change")
                }

                $("#form-edit input[name=price]").val(data_inventory.buy_price);
                $("#form-edit input[name=quantity]").val(data_inventory.quantity).change();

                $("#form-edit select[name='unit']").select2({
                    placeholder: "{{ trans('messages.choice_unit') }}",
                });
            }else {
                loading_content("#form-edit #unit-group", "failed");
                $("#form-edit #unit-group #loading-content").click(function(){ listUnitMedicine(val, id_medicine); });
            }
        },
        error : function() {
            loading_content("#form-edit #unit-group", "failed");
            $("#form-edit #unit-group #loading-content").click(function(){ listUnitMedicine(val, id_medicine); });
        }
    });
}

var obj_unit = "";
var last_el = false;
var val_quantity = 0;

$("#form-edit select[name='unit']").on('change', function() {
    val_quantity = 0;
    $("#form-edit input[name='quantity']").val("")
    $("#form-edit input[name='price']").val("");
    if($(this).val() != "") {
        last_el = false;
        let i = list_unit.find(el => el.id === $(this).val()).weight
        if (i !== list_unit.length - 1) {
            obj_unit = list_unit[i+1]
        } else {
            obj_unit = list_unit[i]
            last_el = true;
        }

        setNotesPriceEdit(val_quantity, obj_unit.name)
    }
});

$("#form-edit input[name='quantity']").on('change input', function(e) {
    val_quantity = e.target.value;
    if(obj_unit != "") {
        if (last_el) {
            setNotesPriceEdit(e.target.value, obj_unit.name)
        }else {
            let calculation = e.target.value * obj_unit.amount_ratio;
            setNotesPriceEdit(calculation, obj_unit.name)
        }
    }
});

function setNotesPriceEdit (amount, unit) {
    $("#form-edit #notes").html("<p>"+amount+" "+unit+"</p>");
}

function resetEdit() {
    resetValidation('form-edit #medicine', 'form-edit #batch_no', 'form-edit #exp_date', 'form-edit #quantity');

    $("#form-edit input[name=id]").val("");
    $("#form-edit select[name='medicine']").val("").trigger("change");
    $("#form-edit input[name=batch_no]").val("");
    $("#form-edit input[name='exp_date']").val("");
    $("#form-edit input[name=quantity]").val("");
}

$('#edit').on('show.bs.modal', function (e) {
    $("#form-edit select[name='medicine']").val("").trigger("change")
    var id = $(e.relatedTarget).attr('data-id');
    edit(id);

    unit = "";
    obj_unit = "";
    $("#form-edit #notes").html("");
    $("#form-edit input[name=quantity]").val("");
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
});

$("#form-edit #medicine-group .border-group").click(function() {
    resetValidation('form-edit #medicine');
});

$('#form-edit input[name=batch_no]').focus(function() {
    resetValidation('form-edit #batch_no');
});

$("#form-edit #exp_date-group").click(function() {
    resetValidation('form-edit #exp_date');
});

$('#form-edit input[name=quantity]').focus(function() {
    resetValidation('form-edit #quantity');
});
$('#form-edit input[name=price]').focus(function() {
    resetValidation('form-edit #price');
});

$('#form-edit input[name=company]').focus(function() {
    resetValidation('form-edit #company');
});

$('#form-edit select[name=unit]').change(function() {
    resetValidation('form-edit #unit');
});


$("#form-edit").submit(function(event) {
    resetValidation('form-edit #medicine', 'form-edit #batch_no', 'form-edit #exp_date', 'form-edit #quantity');
    event.preventDefault();

    let unit = $("#form-edit select[name='unit'] option:selected").val();
    let id_medicine = $("#form-edit select[name='medicine'] option:selected").val();
    let distributor = $("#form-edit input[name=company]").val();
    let batch_no = $("#form-edit input[name=batch_no]").val();
    let quantity = $("#form-edit input[name=quantity]").val();
    let price = $("#form-edit input[name=price]").val();
    let exp_date = $("#form-edit input[name='exp_date']").val();
    var id = $("#form-edit input[name=id]").val();

    if(!validateEdit(unit, id_medicine, distributor, batch_no, quantity, price, exp_date)) return;
    
    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);
    formData.append("id_medicine", id_medicine);
    formData.append("batch_no", batch_no);
    formData.append("distributor", distributor);
    formData.append("buy_price", price);
    formData.append("id_unit", unit);
    formData.append("exp_date", convertDate(exp_date));
    formData.append("quantity", quantity);
    
    $("#form-edit button").attr("disabled", true);
    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/medicine-incoming/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            $("#form-edit button").attr("disabled", false);
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");
            if(!data.error) {
                if(!data.success) {
                        notif(false,""+data.message+"");
                }else {
                    resetEdit();
                    $("#edit").modal("toggle");
                    search(numPage, "false", keySearch, keyFromDate, keyToDate, keyExpFromDate, keyExpToDate);
                    notif(true,"{{ trans('validation.success_edit_medicine_incoming') }}");
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

function validateEdit(unit, id_medicine, distributor, batch_no, quantity, price, exp_date) {
    !unit && formValidate(true, ["form-edit #unit", "{{ trans('messages.choice_unit') }}", true]);
    !id_medicine && formValidate(true, ["form-edit #medicine", "{{ trans('messages.choice_inventory') }}", true]);
    !distributor && formValidate(true, ["form-edit #company", "{{ trans('validation.empty_company_inventory') }}", true]);
    !batch_no && formValidate(true, ["form-edit #batch_no", "{{ trans('validation.empty_no_batch') }}", true]);
    !quantity && formValidate(true, ["form-edit #quantity", "{{ trans('validation.empty_unit_amount') }}", true]);
    !price && formValidate(true, ["form-edit #price", "{{ trans('validation.empty_qty_price') }}", true]);
    !exp_date && formValidate(true, ["form-edit #exp_date", "{{ trans('validation.empty_exp_date') }}", true]);
    
    return unit && id_medicine && distributor && batch_no && quantity && price && exp_date
}

</script>