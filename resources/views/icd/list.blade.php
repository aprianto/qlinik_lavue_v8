@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-medkit"></i>
                    <h3>
                        {{ trans('messages.icd') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_icd") }}</span>
                        </div>
                    </a>
                    <a onclick="syncICD()" id="btnSync" class="disabled">
                        <div class="image">
                            <div class="canvas">
                                <i class="reload fa fa-refresh"></i>
                            </div>
                        </div>
                        <div class="text">
                            <span>Sync ICD</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <div class="input-group date search">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_icd') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>      
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ trans('messages.category') }}</th>
                                    <th>{{ trans('messages.subcategory') }}</th>
                                    <th>{{ trans('messages.code') }}</th>
                                    <th>{{ trans('messages.indonesian_name') }}</th>
                                    <th>{{ trans('messages.english_name') }}</th>
                                    <th>Tipe</th>
                                    <th>{{ trans('messages.action') }}</th>
                                </tr>
                            </thead>
                            <tbody id="table">
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="6" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="6" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/icd",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;
                if(data.is_syncned!=false){
                    $("#btnSync").removeClass('disabled');
                }else{
                    $("#btnSync").addClass('disabled');
                }

                if(total>0) {
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td>" + item[i].category + "</td>");
                        tr.append("<td>" + item[i].subcategory + "</td>");
                        tr.append("<td>" + item[i].code + "</td>");
                        tr.append("<td>" + item[i].indonesian_name + "</td>");
                        tr.append("<td>" + item[i].english_name + "</td>");
                        tr.append("<td>" + (item[i].specialist != 0 ? "Spesialis" : "Non Spesialis") + "</td>");
                        tr.append("<td>"
                            +"<a data-toggle='modal' data-target='#edit' data-id='"+item[i].id+"' class='btn-table btn-orange'><span class='fa fa-pencil'></span> {{ trans('messages.edit') }}</a>"
                            +"<a data-toggle='modal' data-target='#confirmDelete' data-message=\"{{ trans('messages.info_delete') }}\" data-id='"+item[i].id+"' class='btn-table btn-red'><span class='fa fa-trash-o'></span> {{ trans('messages.delete') }}</a>"
                            +"</td>");
                        $("#table").append(tr);
                        no++;
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='6'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);

            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    var id = $id;
    formData= new FormData();
    formData.append("_method", "DELETE");
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/icd/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            if(!data.error) {
                if(data.success==true) {
                    search(numPage, "false", keySearch);
                    notif(true,"{{ trans('validation.success_delete_icd') }}");    
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
    $("#confirmDelete").modal("toggle");
});

$("#form-search").submit(function(event) {
    search(1,"true",$("input[name=q]").val());
});

search(pageRepo,"false", searchRepo);

function syncICD(){
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/bpjs/syncronize-diagnostic-to-icd",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            
            $(".feild button").attr("disabled", false);
    
            $(".feild button").removeClass("loading");
            $(".feild button span").addClass("hide");

            if(!data.error) {
                if(data.success==true) {
                    notif(true,"Data sedang disinkronkan");
                    search(pageRepo,"false", searchRepo);
                } else {
                    notif(false,"Data gagal disinkronkan");
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}", true);
            }
        },
        error: function(){
            notif(false,"{{ trans('validation.failed') }}");
        }
    })
}

</script>

@include('icd.create')

@include('icd.edit')

@include('_partial.confirm_delele')
