<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_icd') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">
                <div class="modal-body">
                    <div class="form-group" id="category-group">
                        <label class="control-label">{{ trans('messages.category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.category') }}" name="category" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="subcategory-group">
                        <label class="control-label">{{ trans('messages.subcategory') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.subcategory') }}" name="subcategory" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="indonesian_name-group">
                        <label class="control-label">{{ trans('messages.indonesian_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.indonesian_name') }}" name="indonesian_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="english_name-group">
                        <label class="control-label">{{ trans('messages.english_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.english_name') }}" name="english_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

function resetAdd() {
    resetValidation('form-add #category','form-add #indonesian_name','form-add #english_name');
    $("#form-add input[name=category]").val("");
    $("#form-add input[name=subcategory]").val("");
    $("#form-add input[name=indonesian_name]").val("");
    $("#form-add input[name=english_name]").val("");
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add input[name=category]').focus(function() {
    resetValidation('form-add #category');
});

$('#form-add input[name=indonesian_name]').focus(function() {
    resetValidation('form-add #indonesian_name');
});

$('#form-add input[name=english_name]').focus(function() {
    resetValidation('form-add #english_name');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #category', 'form-add #indonesian_name', 'form-add #english_name');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    formData.append("validate", false);
    formData.append("category", $("#form-add input[name=category]").val());
    formData.append("subcategory", $("#form-add input[name=subcategory]").val());
    formData.append("indonesian_name", $("#form-add input[name=indonesian_name]").val());
    formData.append("english_name", $("#form-add input[name=english_name]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");
    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/icd",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #category',data.errors.category, true], ['form-add #indonesian_name',data.errors.indonesian_name, true], ['form-add #english_name',data.errors.english_name, true]);
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_add_icd') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>