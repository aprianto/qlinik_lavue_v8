<div class="modal fade window" id="edit" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-pencil"></i> {{ trans('messages.edit_icd') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="loading-edit">
                <div class="card">
                    <span class="three-quarters">Loading&#8230;</span>
                </div>
            </div>
            <div class="modal-body hide" id="reload-edit">
                <div class="card">
                    <span class="reload fa fa-refresh"></span>
                </div>
            </div>
            <form id="form-edit" class="hide">
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="form-group" id="category-group">
                        <label class="control-label">{{ trans('messages.category') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.category') }}" name="category" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="subcategory-group">
                        <label class="control-label">{{ trans('messages.subcategory') }}</label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.subcategory') }}" name="subcategory" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="indonesian_name-group">
                        <label class="control-label">{{ trans('messages.indonesian_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.indonesian_name') }}" name="indonesian_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group" id="english_name-group">
                        <label class="control-label">{{ trans('messages.english_name') }} <span>*</span></label>
                        <input type="text" class="form-control" placeholder="{{ trans('messages.english_name') }}" name="english_name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">

function resetEdit() {
    resetValidation('form-edit #category', 'form-edit #indonesian_name', 'form-edit #english_name');

    $("#form-edit input[name=id]").val("");
    $("#form-edit input[name=category]").val("");
    $("#form-edit input[name=subcategory]").val("");
    $("#form-edit input[name=indonesian_name]").val("");
    $("#form-edit input[name=english_name]").val("");
}

function edit(id) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    $("#reload-edit").addClass("hide");

    formData= new FormData();
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/icd/"+id+"",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading-edit').addClass('hide');

            if(!data.error) {
                $('#form-edit').removeClass('hide');

                $("#form-edit input[name=id]").val(data.id);
                $("#form-edit input[name=category]").val(data.category);
                $("#form-edit input[name=subcategory]").val(data.subcategory);
                $("#form-edit input[name=indonesian_name]").val(data.indonesian_name);
                $("#form-edit input[name=english_name]").val(data.english_name);
            } else {
                $("#reload-edit").removeClass("hide");
                $("#reload-edit .reload").click(function(){ edit(id); });
            }

        },
        error: function(){
            $('#loading-edit').addClass('hide');
            $("#reload-edit").removeClass("hide");

            $("#reload-edit .reload").click(function(){ edit(id); });
        }
    })
}

$('#edit').on('show.bs.modal', function (e) {
    $('#loading-edit').removeClass('hide');
    $('#form-edit').addClass('hide');
    resetEdit();

    var id = $(e.relatedTarget).attr('data-id');
    edit(id);
});


$('#form-edit input[name=category]').focus(function() {
    resetValidation('form-edit #category');
});

$('#form-edit input[name=indonesian_name]').focus(function() {
    resetValidation('form-edit #indonesian_name');
});

$('#form-edit input[name=english_name]').focus(function() {
    resetValidation('form-edit #english_name');
});

$("#form-edit").submit(function(event) {
    event.preventDefault();
    resetValidation('form-edit #category', 'form-edit #indonesian_name', 'form-edit #english_name');

    $("#form-edit button").attr("disabled", true);

    var id = $("#form-edit input[name=id]").val();
    formData= new FormData();
    formData.append("_method", "PATCH");
    formData.append("validate", false);
    formData.append("category", $("#form-edit input[name=category]").val());
    formData.append("subcategory", $("#form-edit input[name=subcategory]").val());
    formData.append("indonesian_name", $("#form-edit input[name=indonesian_name]").val());
    formData.append("english_name", $("#form-edit input[name=english_name]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    $("#form-edit .btn-primary").addClass("loading");
    $("#form-edit .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/icd/"+id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-edit button").attr("disabled", false);

            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-edit #category',data.errors.category, true], ['form-edit #indonesian_name',data.errors.indonesian_name, true], ['form-edit #english_name',data.errors.english_name, true]);
                } else {
                    resetEdit();

                    $("#edit").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_edit_icd') }}");  
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-edit button").attr("disabled", false);
            
            $("#form-edit .btn-primary").removeClass("loading");
            $("#form-edit .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});
</script>