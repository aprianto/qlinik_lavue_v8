<div id="progressBar">
	<div class="loader"></div>
</div> 
<div class="breadcrumbs">
	<ul>
		<li><a href="#/" title="">Home</a></li>
		<li><a href="#/pages/505" title="">505 Server Error</a></li>
	</ul>
</div>


<div class="panel-content">
	<div class="row">
		<div class="col-md-12">
			<div class="widget">
				<div class="error-sec">
					<div class="mockup">
					</div>
					<span>Internal Server Error</span>
					<h2>505</h2>
					<p>Sorry! we are having technical difficulties Check out our status or blog..</p><a href="#/" title="">Return to Home</a>
				</div>
			</div>
		</div>
	</div>
</div>
