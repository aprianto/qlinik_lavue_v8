<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="heading-sec">
	<div class="row">
		<div class="col-md-12">
			<div class="error-content">
				<h2>404</h2>
				<span>{{ trans('messages.404_message') }}</span>
			</div>
		</div>
	</div>
</div>