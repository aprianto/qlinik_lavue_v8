@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <h3>
                        {{ trans('messages.edit_admin') }}
                    </h3>
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec hide">
                    <form role="form" class="sec">
                        <div class="form-group" id="name-group">
                            <label class="control-label">{{ trans('messages.name') }} <span>*</span></label>
							<input type="text" name="name" class="form-control"  />
							<span class="help-block"></span>
						</div>
						<div class="form-group" id="username-group">
                            <label class="control-label">{{ trans('messages.username') }} <span>*</span></label>
                            <input type="text" name="username" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="email-group">
                            <label class="control-label">{{ trans('messages.email') }} <span>*</span></label>
							<input type="text" name="email" class="form-control"  />
							<span class="help-block"></span>
						</div>
                        <div class="form-group" id="old_password-group">
                            <label class="control-label">{{ trans('messages.old_password') }} <span>*</span></label>
                            <input type="password" name="old_password" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="password-group">
                            <label class="control-label">{{ trans('messages.password') }}</label>
                            <input type="password" name="password" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="password_confirmation-group">
                            <label class="control-label">{{ trans('messages.password_confirmation') }}</label>
                            <input type="password" name="password_confirmation" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


function edit() {
	$('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/admin/profile",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading').addClass('hide');
            if(!data.error) {
            	$('.form-elements-sec').removeClass('hide');
                
	            $("input[name=name]").val(data.name);
	            $("input[name=username]").val(data.username);
                $("input[name=email]").val(data.email);
        	} else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ edit(); });
        	}
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ edit(); });
        }
    });
}

edit();

$("input[name=name]").click(function() {
	resetValidation('name');
});

$("input[name=username]").click(function() {
    resetValidation('username');
});

$("input[name=email]").click(function() {
	resetValidation('email');
});

$("input[name=old_password]").click(function() {
	resetValidation('old_password');
});

$("input[name=password]").click(function() {
    resetValidation('password');
});

$("input[name=password_confirmation]").click(function() {
    resetValidation('password_confirmation');
});

$("form").submit(function(event) {
	event.preventDefault();
	resetValidation('name','username','email', 'old_password', 'password', 'password_confirmation');

	$("form button").attr("disabled", true);

    var id = "{{ $id_user }}";
    formData= new FormData();
    formData.append("_method", "PATCH");		    
    formData.append("validate", false);
    formData.append("name", $("input[name=name]").val());
    formData.append("username", $("input[name=username]").val());
    formData.append("email", $("input[name=email]").val());
    formData.append("old_password", $("input[name=old_password]").val());
    formData.append("password", $("input[name=password]").val());
    formData.append("password_confirmation", $("input[name=password_confirmation]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
	$(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/admin",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

			if(!data.error) {
	            if(!data.success) {
	                formValidate(true, ['name',data.errors.name, true], ['username',data.errors.username, true], ['email',data.errors.email, true], ['old_password',data.errors.old_password, true], ['password',data.errors.password, true], ['password_confirmation',data.errors.password_confirmation, true]);
	            } else {
	               
	                notif(true,"{{ trans('validation.success_edit_account') }}");
	            }
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("form button").attr("disabled", false);

        	$(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>
