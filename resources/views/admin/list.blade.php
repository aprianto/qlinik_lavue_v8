@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>  
</div> 


<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <img src="{{ asset('assets/images/icons/sidebar/pasien.png') }}" />
                    <h3>
                        {{ trans('messages.user') }}
                    </h3>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-4">
                                <div class="form-group column-group">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_user') }}" name="q" autocomplete="off" />
                                        <button id="btn-search" class="btn-icon">
                                            <img src="{{ asset('assets/images/icons/action/search.png') }}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-area">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('messages.name') }}</th>                                    
                                <th class="hide-lg">{{ trans('messages.email') }}</th>
                                <th class="hide-lg">{{ trans('messages.phone') }}</th>
                                <th class="hide-lg">{{ trans('messages.date') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            
                        </tbody>
                        <tbody id="loading-table" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="three-quarters">Loading&#8230;</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody id="reload" class="hide">
                            <tr>
                                <td colspan="5" align="center">
                                    <div class="card">
                                        <span class="reload fa fa-refresh"></span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }

    $("#loading-table").removeClass("hide");
    $(".table").removeClass("table-hover");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/users",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {
                $(".table").addClass("table-hover");

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    $(".pagination-sec").removeClass("hide");

                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {
                            tr = $('<tr/>');

                            var role = "";

                            tr.append("<td><b>" + item[i].name + "</b>"+
                                '<div class="sub show-md">'+
                                    "<span class='general'>"+item[i].email+"</span>"+
                                    "<span class='general'>+"+item[i].phone_code+''+item[i].phone+"</span>"+
                                '</div>'+
                                "</td>");

                            tr.append("<td class='hide-lg'>"+item[i].email+"</td>");
                            tr.append("<td class='hide-lg'>+"+item[i].phone_code+''+item[i].phone+"</td>");
                            tr.append("<td class='hide-lg'>"+item[i].created_at.date.substring(0, item[i].created_at.date.length - 10)+"</td>");
                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {

                    $(".pagination-sec").addClass("hide");
                        
                    if(page==1 && q=='') {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.empty_user') }}</td></tr>");
                    } else {
                        $("#table").append("<tr><td align='center' colspan='5'>{{ trans('messages.no_result') }}</td></tr>");
                    }
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);
            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); }); 
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

$("#form-search").submit(function(event) {
    search(1,"true",$("#form-search input[name='q']").val());
});

search(pageRepo,'false', searchRepo);

</script>