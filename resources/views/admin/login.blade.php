@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div>
<div class="account-user-sec">
	<div class="account-sec">
		<div class="account-top-bar">
			<div class="container">
				<div class="logo">
			        <a href="#/" title="" class="text" onclick="loading($(this).attr('href'))">
			        	<img src="{{ $storage }}/images/icons/logo.png" />
			        	<!--<span>Lix</span>Medic.-->
			        </a>
			    </div>
			    <ul class="account-header-link">
			    	<li>
					    <div class="quick-links">
							<ul>
								<li>
									<a title="" class="">
										@if($lang=="id")
										<img src="{{ asset('assets/images/flags/id.png') }}" class="icon-flag" /><span class="text">ID</span> 
										@else
										<img src="{{ asset('assets/images/flags/en.png') }}" class="icon-flag" /><span class="text">EN</span>
										@endif
										<i class="fa fa-chevron-down"></i>
									</a>
									<div class="dialouge" style="display: none;">
										<a class="menu" href="language/id"><img src="{{ asset('assets/images/flags/id.png') }}" class="icon-link flag" /> Indonesia</a>
										<a class="menu" href="language/en"><img src="{{ asset('assets/images/flags/en.png') }}" class="icon-link flag" /> English</a>
									</div>
								</li>
							</ul>
			            </div>
			        </li>
			    </ul>
			</div>
		</div>
		<div class="acount-sec">
			<div class="container">
				<div class="login-sec">
					<div class="login">
						<div class="contact-sec">
							<div class="widget-title">
								<h3>{{ trans('messages.title_admin_login') }}</h3>
								<span>{{ trans('messages.text_admin_login') }}</span>
							</div>
							<div class="account-form alert-notif">
								<form>
									<div class="row">
										<div class="feild col-md-12" id="username-group">
											<input type="text" id="username" name="username" placeholder="{{ trans('messages.username') }}" />
											<span class="help-block"></span>
										</div>
										<div class="feild col-md-12" id="password-group">
											<input type="password" id="password" name="password" placeholder="{{ trans('messages.password') }}" />
											<span class="help-block"></span>
										</div>
										<div class="feild col-md-12">
											<button type="submit">
				                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
				                                {{ trans('messages.login') }}
				                            </button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.footer')
	</div>
</div>


<script type="text/javascript">

$('.quick-links > ul > li > a').on("click", function(){
	$(this).parent().siblings().find('.dialouge').fadeOut();
	$(this).next('.dialouge').fadeIn();
	return false;
});

$("html").on("click", function(){
	$(".dialouge").fadeOut();
});


$("input[name=username]").bind("focus", function () {
    resetValidation('username');
});

$("input[name=password]").bind("focus", function () {
    resetValidation('password');
});

$("input[name=username]").bind("keyup focusout", function () {
	var username = $("#username").val();

    if(username=="") {
    	validateElementForm('username', 'success', 'error', "{{ trans('validation.empty_username') }}", true);
    } else {
    	validateElementForm('username', 'error', 'success', '', false);
 	}

});

$("input[name=password]").bind("keyup focusout", function () {
	var password = $("#password").val();

    if(password=="") {
    	validateElementForm('password', 'success', 'error', "{{ trans('validation.empty_password') }}.", true);
    } else {
    	validateElementForm('password', 'error', 'success', '', false);
 	}

});

$("form").submit(function(event) {
	resetValidation('username', 'password');
	
    var username = $("#username").val();
    var password = $("#password").val();

    var error = false;

    if(username=="") {
    	error = true;
    	validateElementForm('username', 'success', 'error', "{{ trans('validation.empty_username') }}", true);
    } else {
    	validateElementForm('username', 'error', 'success', '', false);
 	}

    if(password=="") {
    	error = true;
    	validateElementForm('password', 'success', 'error', "{{ trans('validation.empty_password') }}", true);
    } else {
    	validateElementForm('password', 'error', 'success', '', false);
 	}

 	if(error==false) {
 		event.preventDefault(); 		

    	$(".feild button").attr("disabled", true);

		$(".feild button").addClass("loading");
        $(".feild button span").removeClass("hide");
 		formData= new FormData();
        formData.append("username", username);
        formData.append("password", password);
        $.ajax({
            url: "{{ $lang }}/admin/login",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                
                $(".feild button").attr("disabled", false);
        
                $(".feild button").removeClass("loading");
                $(".feild button span").addClass("hide");

                if(!data.error) {
	                if(data.success==true) {
	                	notif(true,"{{ trans('validation.success_login') }}");	

						setTimeout(function(){
							redirect('');
						}, 1000);
	                } else {
	                	notif(false,"{{ trans('validation.failed_admin_login') }}");
	                }
                } else {
                	notif(false,"{{ trans('validation.failed') }}");
                }
            },
			error: function(){
                $(".feild button").attr("disabled", false);
				
				$(".feild button").removeClass("loading");
                $(".feild button span").addClass("hide");

				notif(false,"{{ trans('validation.failed') }}");
			}
        })

    }
});


</script>