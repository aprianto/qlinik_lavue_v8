@include('layouts.lib')

<div id="progressBar">
    <div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-bullhorn"></i>
                    <h3>
                        {{ trans('messages.broadcast') }}
                    </h3>
                </div>
                <div class="thumbnail-sec">
                    <a data-toggle="modal" data-target="#add">
                        <div class="image">
                            <div class="canvas">
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div class="text">
                            <span>{{ trans("messages.add_broadcast") }}</span>
                        </div>
                    </a>
                </div>
                <div class="search-sec">
                    <form id="form-search">
                        <div class="row search">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <div class="input-group date search">
                                        <input type="text" class="form-control" placeholder="{{ trans('messages.search_broadcast') }}" name="q" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button id="btn-search" class="button"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>      
                <div class="table-area">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('messages.message') }}</th>
                                    <th>{{ trans('messages.send_to') }}</th>
                                    <th>{{ trans('messages.receiver') }}</th>
                                    <th>{{ trans('messages.status') }}</th>
                                </tr>
                            </thead>
                            <tbody id="table">
                            </tbody>
                            <tbody id="loading-table" class="hide">
                                <tr>
                                    <td colspan="4" align="center">
                                        <div class="card">
                                            <span class="three-quarters">Loading&#8230;</span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody id="reload" class="hide">
                                <tr>
                                    <td colspan="4" align="center">
                                        <div class="card">
                                            <span class="reload fa fa-refresh"></span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination-sec bottom-sec">
                    <ul class="pagination">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/js/function.js') }}"></script>

<script>

$("#form-search input[name='q']").val(searchRepo);

var keySearch = searchRepo;
var numPage = pageRepo;

function search(page, submit, q) {
    formData= new FormData();

    if(q=="" && submit=="false") {
        var q = keySearch;
    }


    $("#loading-table").removeClass("hide");
    $("#reload").addClass("hide");
    $("#table").empty();

    keySearch = q;
    numPage = page;

    repository(['search',q],['page',page]);
    
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/broadcast",
        type: "GET",
        data: "q="+q+"&page="+page+"&per_page=10",
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#table").empty();

            $("#loading-table").addClass("hide");

            if(!data.error) {

                var total = data.total;
                var per_page = data.per_page;
                var current_page = data.current_page;
                var last_page = data.last_page;
                var from = data.from;
                var to = data.to;

                if(total>0) {
                    
                    var item = data.data;
                    var tr;
                    var no = from;
                    for (var i = 0; i < item.length; i++) {
                        try {

                            tr = $('<tr/>');
                            tr.append("<td>" + item[i].message + "</td>");

                            var send_to = "";
                            if(item[i].notification==1) {
                                send_to = send_to+"<span class='category'>{{ trans('messages.notification') }}</span>";
                            }

                            if(item[i].email==1) {
                                send_to = send_to+"<span class='category'>{{ trans('messages.email') }}</span>";
                            }

                            if(item[i].phone==1) {
                                send_to = send_to+"<span class='category'>{{ trans('messages.sms') }}</span>";
                            }

                            tr.append("<td>" + send_to + "</td>");

                            var receiver = "";
                            if(item[i].clinic==1) {
                                receiver = receiver+"<span class='category'>{{ trans('messages.clinic') }}</span>";
                            }

                            if(item[i].doctor==1) {
                                receiver = receiver+"<span class='category'>{{ trans('messages.doctor') }}</span>";
                            }

                            if(item[i].patient==1) {
                                receiver = receiver+"<span class='category'>{{ trans('messages.patient') }}</span>";
                            }

                            tr.append("<td>" + receiver + "</td>");

                            tr.append("<td>" + status(item[i].status) + "</td>");

                            $("#table").append(tr);
                            no++;
                        } catch(err) {}
                    }
                } else {
                    $("#table").append("<tr><td align='center' colspan='4'>{{ trans('messages.no_result') }}</td></tr>");
                    
                }

                pages(page, total, per_page, current_page, last_page, from, to, q);

            } else {
                $("#reload").removeClass("hide");

                $("#reload .reload").click(function(){ search(page, "false", q); });    
            }
            
        },
        error: function(){
            $("#loading-table").addClass("hide");
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ search(page, "false", q); });
        }
    })
}

$("#form-search").submit(function(event) {
    search(1,"true",$("input[name=q]").val());
});

search(pageRepo,"false", searchRepo);

</script>

@include('broadcast.create')
