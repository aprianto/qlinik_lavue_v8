<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_broadcast') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">
                <div class="modal-body">
                    <div class="form-group" id="message-group">
                        <label class="control-label">{{ trans('messages.message') }}</label>
                        <textarea class="form-control" rows="5" placeholder="{{ trans('messages.message') }}" name="message" maxlength="300"></textarea>
                        <span class="char-limit">{{ trans("messages.remaining_character") }}: <span id="char"></span></span>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.send_to') }}</label>
                        <div class="sf-check">
                            <label><input type="checkbox" name="notification"><span></span> {{ trans('messages.notifications') }}</label>
                        </div>
                        <div class="sf-check">
                            <label><input type="checkbox" name="email"><span></span> {{ trans('messages.email') }}</label>
                        </div>
                        <div class="sf-check">
                            <label><input type="checkbox" name="phone"><span></span> {{ trans('messages.sms') }}</label>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="name-group">
                        <label class="control-label">{{ trans('messages.receiver') }}</label>
                        <div class="sf-check">
                            <label><input type="checkbox" name="clinic"><span></span> {{ trans('messages.clinic') }}</label>
                        </div>
                        <div class="sf-check">
                            <label><input type="checkbox" name="doctor"><span></span> {{ trans('messages.doctor') }}</label>
                        </div>
                        <div class="sf-check">
                            <label><input type="checkbox" name="patient"><span></span> {{ trans('messages.patient') }}</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.send') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

$("#form-add textarea[name=message]").bind("keyup focusout", function () {
    var char = parseInt($("textarea").attr("maxlength"))-parseInt($("#form-add textarea[name=message]").val().length);

    $("#form-add #char").html(char);
});

function resetAdd() {
    resetValidation('form-add #message');
    $("#form-add textarea[name=message]").val("");
    $("#form-add #char").html($("textarea").attr("maxlength"));

    $('#form-add input[name="notification"]').prop('checked',false);
    $('#form-add input[name="email"]').prop('checked',false);
    $('#form-add input[name="phone"]').prop('checked',false);

    $('#form-add input[name="clinic"]').prop('checked',false);
    $('#form-add input[name="doctor"]').prop('checked',false);
    $('#form-add input[name="patient"]').prop('checked',false);
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add textarea[name=message]').focus(function() {
    resetValidation('form-add #message');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #message');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    var notification = $('#form-add input[name="notification"]').is(':checked');
    if(notification==true) {
        notification = 1;
    } else {
        notification = 0;
    }

    var email = $('#form-add input[name="email"]').is(':checked');
    if(email==true) {
        email = 1;
    } else {
        email = 0;
    }

    var phone = $('#form-add input[name="phone"]').is(':checked');
    if(phone==true) {
        phone = 1;
    } else {
        phone = 0;
    }

    var clinic = $('#form-add input[name="clinic"]').is(':checked');
    if(clinic==true) {
        clinic = 1;
    } else {
        clinic = 0;
    }

    var doctor = $('#form-add input[name="doctor"]').is(':checked');
    if(doctor==true) {
        doctor = 1;
    } else {
        doctor = 0;
    }

    var patient = $('#form-add input[name="patient"]').is(':checked');
    if(patient==true) {
        patient = 1;
    } else {
        patient = 0;
    }

    formData.append("validate", false);
    formData.append("id_user", "{{ $id_user }}");
    formData.append("level", "{{ $level }}");
    formData.append("message", nl2br($("#form-add textarea[name=message]").val()));
    formData.append("notification", notification);
    formData.append("email", email);
    formData.append("phone", phone);
    formData.append("clinic", clinic);
    formData.append("doctor", doctor);
    formData.append("patient", patient);
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");
    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/broadcast",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #message',data.errors.message, true]);

                    if(data.errors.message) {
                        notif(false,""+data.errors.message+"");
                    } 

                    if(data.errors.message) {
                        notif(false,""+data.errors.message+"");
                    } 
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_send_broadcast') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>