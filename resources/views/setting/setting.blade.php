<?php

function default_location($type) {
	$latitude = '-5.7759362';
	$longitude = '106.1174826';

	if($type=="latitude") {
		$location = $latitude;
	} else {
		$location = $longitude;
	}

    return $location;
}

?>