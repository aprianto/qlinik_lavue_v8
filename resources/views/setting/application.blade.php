@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div> 

<div class="panel-content">
    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header">
                    <i class="icon fa fa-cogs"></i>
                    <h3>
                        {{ trans('messages.application_setting') }}
                    </h3>
                </div>
                <div class="cell" id="loading">
                    <div class="card">
                        <span class="three-quarters">Loading&#8230;</span>
                    </div>
                </div>
                <div class="cell hide" id="reload">
                    <div class="card">
                        <span class="reload fa fa-refresh"></span>
                    </div>
                </div>
                <div class="form-elements-sec hide">
                    <form role="form" class="sec">
                        <div class="form-group" id="notification-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="notification"><span></span> {{ trans('messages.notification') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
						<div class="form-group" id="reminder-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="reminder"><span></span> {{ trans('messages.reminder') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="suspended-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="suspended"><span></span> {{ trans('messages.suspended') }}</label>
                            </div>
							<span class="help-block"></span>
						</div>
                        <div class="form-group" id="redirect-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="redirect"><span></span> {{ trans('messages.redirect') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="version-group">
                            <label class="control-label">{{ trans('messages.version') }}</label>
                            <input type="text" name="version" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="android_version-group">
                            <label class="control-label">{{ trans('messages.android_version') }}</label>
                            <input type="text" name="android_version" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="ios_version-group">
                            <label class="control-label">{{ trans('messages.ios_version') }}</label>
                            <input type="text" name="ios_version" class="form-control"  />
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group" id="mobile_update-group">
                            <div class="sf-check">
                                <label><input type="checkbox" name="mobile_update"><span></span> {{ trans('messages.mobile_update') }}</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">
                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                                {{ trans('messages.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


function setting() {
	$('#loading').removeClass('hide');
    $("#reload").addClass("hide");
    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/app/setting",
        type: "GET",
        processData: false,
        contentType: false,
        success: function(data){
            

            $('#loading').addClass('hide');
            if(!data.error) {
            	$('.form-elements-sec').removeClass('hide');
                
                if(data[0].setting==1) {
                    $('input[name="notification"]').prop('checked',true);
                }

                if(data[1].setting==1) {
                    $('input[name="reminder"]').prop('checked',true);
                }

                if(data[2].setting==1) {
                    $('input[name="suspended"]').prop('checked',true);
                }

                if(data[3].setting==1) {
                    $('input[name="redirect"]').prop('checked',true);
                }

                $("input[name=version]").val(data[4].setting);

                if(data[6].setting==1) {
                    $('input[name="mobile_update"]').prop('checked',true);
                }

                $("input[name=android_version]").val(data[7].setting);

                $("input[name=ios_version]").val(data[8].setting);
        	} else {
                $("#reload").removeClass("hide");
                $("#reload .reload").click(function(){ setting(); });
        	}
        },
        error: function(){
            $('#loading').addClass('hide');
            $("#reload").removeClass("hide");

            $("#reload .reload").click(function(){ setting(); });
        }
    });
}

setting();

$('input[name=notification]').click(function() {
    resetValidation('notification');
});

$('input[name=reminder]').click(function() {
    resetValidation('reminder');
});

$('input[name=suspended]').click(function() {
    resetValidation('suspended');
});

$('input[name=redirect]').click(function() {
    resetValidation('redirect');
});

$('input[name=version]').focus(function() {
    resetValidation('version');
});

$('input[name=mobile_update]').click(function() {
    resetValidation('mobile_update');
});

$('input[name=android_version]').focus(function() {
    resetValidation('android_version');
});

$('input[name=ios_version]').focus(function() {
    resetValidation('ios_version');
});

$("form").submit(function(event) {
	event.preventDefault();
	resetValidation('notification','reminder','suspended', 'redirect', 'version','mobile_update','android_version','ios_version');

	$("form button").attr("disabled", true);

    var notification = $('input[name="notification"]').is(':checked');
    if(notification==true) {
        notification = 1;
    } else {
        notification = 0;
    }

    var reminder = $('input[name="reminder"]').is(':checked');
    if(reminder==true) {
        reminder = 1;
    } else {
        reminder = 0;
    }

    var suspended = $('input[name="suspended"]').is(':checked');
    if(suspended==true) {
        suspended = 1;
    } else {
        suspended = 0;
    }

    var redirect = $('input[name="redirect"]').is(':checked');
    if(redirect==true) {
        redirect = 1;
    } else {
        redirect = 0;
    }

    var mobile_update = $('input[name="mobile_update"]').is(':checked');
    if(mobile_update==true) {
        mobile_update = 1;
    } else {
        mobile_update = 0;
    }

    formData= new FormData();		    
    formData.append("validate", false);
    formData.append("_method", "PATCH");
    formData.append("notification", notification);
    formData.append("reminder", reminder);
    formData.append("suspended", suspended);
    formData.append("redirect", redirect);
    formData.append("version", $("input[name=version]").val());    
    formData.append("mobile_update", mobile_update);
    formData.append("android_version", $("input[name=android_version]").val());
    formData.append("ios_version", $("input[name=ios_version]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    $(".btn-primary").addClass("loading");
	$(".btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/app/setting",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("form button").attr("disabled", false);

            $(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

			if(!data.error) {
	            if(!data.success) {
	                formValidate(true, ['notification',data.errors.notification, true], ['reminder',data.errors.reminder, true], ['suspended',data.errors.suspended, true], ['redirect',data.errors.redirect, true], ['version',data.errors.version, true], ['mobile_update',data.errors.mobile_update, true], ['android_version',data.errors.android_version, true], ['ios_version',data.errors.ios_version, true]);
	            } else {
	               
	                notif(true,"{{ trans('validation.success_setting_application') }}");
	            }
        	} else {
        		notif(false,"{{ trans('validation.failed') }}");
        	}
        },
        error: function(){
        	$("form button").attr("disabled", false);

        	$(".btn-primary").removeClass("loading");
            $(".btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>
