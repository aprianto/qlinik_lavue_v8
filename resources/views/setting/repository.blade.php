<script type="text/javascript">

date = new Date();
monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
dayNow = (date.getDate()+100).toString().slice(-2);
yearNow = date.getFullYear();
dateNow = date.getFullYear()+'-'+monthNow+'-'+dayNow;

dateMonth = new Date();
dateMonth.setMonth(dateMonth.getMonth() - 1);
monthFrom = ((dateMonth.getMonth()+1)+100).toString().slice(-2);
dayFrom = (dateMonth.getDate()+100).toString().slice(-2);
yearFrom = dateMonth.getFullYear();
dateFrom = yearFrom+'-'+monthFrom+'-'+dayFrom;

var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var dateFirst = (firstDay.getDate()+100).toString().slice(-2);
var dateLast = (lastDay.getDate()+100).toString().slice(-2);
var getDateFirst = yearNow+"-"+monthNow+"-"+dateFirst;
var getDateLast = yearNow+"-"+monthNow+"-"+dateLast;

var searchRepo = "{{ Session::get('searchRepo') }}";
var dateRepo = "{{ Session::get('dateRepo') }}";
var polyclinicRepo = "{{ Session::get('polyclinicRepo') }}";
var clinicRepo = "{{ Session::get('clinicRepo') }}";
var doctorRepo = "{{ Session::get('doctorRepo') }}";
var statusRepo = "{{ Session::get('statusRepo') }}";
var birth_dateRepo = "{{ Session::get('birth_dateRepo') }}";
var from_dateRepo = "{{ Session::get('from_dateRepo') }}";
var to_dateRepo = "{{ Session::get('to_dateRepo') }}";
var exp_from_dateRepo = "{{ Session::get('exp_from_dateRepo') }}";
var exp_to_dateRepo = "{{ Session::get('exp_to_dateRepo') }}";

@if(Session::get('date_nowRepo')=="")
var date_nowRepo = dateNow;
@else
var date_nowRepo = "{{ Session::get('date_nowRepo') }}";
@endif

@if(Session::get('from_date_nowRepo')=="")
var from_date_nowRepo = dateNow;
@else
var from_date_nowRepo = "{{ Session::get('from_date_nowRepo') }}";
@endif

@if(Session::get('to_date_nowRepo')=="")
var to_date_nowRepo = dateNow;
@else
var to_date_nowRepo = "{{ Session::get('to_date_nowRepo') }}";
@endif

@if(Session::get('from_date_monthRepo')=="")
var from_date_monthRepo = getDateFirst;
@else
var from_date_monthRepo = "{{ Session::get('from_date_monthRepo') }}";
@endif

@if(Session::get('to_date_monthRepo')=="")
var to_date_monthRepo = getDateLast;
@else
var to_date_monthRepo = "{{ Session::get('to_date_monthRepo') }}";
@endif

@if(Session::get('pageRepo')=="")
var pageRepo = '1';
@else
var pageRepo = "{{ Session::get('ageRepo') }}";
@endif

var backRepo = "{{ Session::get('backRepo') }}";

function repository() {
	for (var i = 0, j = arguments.length; i < j; i++){
		if(arguments[i][0]=="search") {
			searchRepo = arguments[i][1];
		}

		if(arguments[i][0]=="date") {
			dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="date_now") {
			date_nowRepo = arguments[i][1];

			if(date_nowRepo=="now") {
				date = new Date();
				monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
				dayNow = (date.getDate()+100).toString().slice(-2);
				yearNow = date.getFullYear();
				dateNow = date.getFullYear()+'-'+monthNow+'-'+dayNow;

				date_nowRepo = dateNow;
			}
		}

		if(arguments[i][0]=="polyclinic") {
			polyclinicRepo = arguments[i][1];
		}

		if(arguments[i][0]=="clinic") {
			clinicRepo = arguments[i][1];
		}

		if(arguments[i][0]=="doctor") {
			doctorRepo = arguments[i][1];
		}

		if(arguments[i][0]=="status") {
			statusRepo = arguments[i][1];
		}

		if(arguments[i][0]=="birth_date") {
			birth_dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="from_date") {
			from_dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="to_date") {
			to_dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="exp_from_date") {
			exp_from_dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="exp_to_date") {
			exp_to_dateRepo = arguments[i][1];
		}

		if(arguments[i][0]=="from_date_now") {
			from_date_nowRepo = arguments[i][1];

			if(from_date_nowRepo=="now") {
				date = new Date();
				monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
				dayNow = (date.getDate()+100).toString().slice(-2);
				yearNow = date.getFullYear();
				dateNow = date.getFullYear()+'-'+monthNow+'-'+dayNow;

				from_date_nowRepo = dateNow;
			}
		}

		if(arguments[i][0]=="to_date_now") {
			to_date_nowRepo = arguments[i][1];

			if(to_date_nowRepo=="now") {
				date = new Date();
				monthNow = ((date.getMonth()+1)+100).toString().slice(-2);
				dayNow = (date.getDate()+100).toString().slice(-2);
				yearNow = date.getFullYear();
				dateNow = date.getFullYear()+'-'+monthNow+'-'+dayNow;

				to_date_nowRepo = dateNow;
			}
		}

		if(arguments[i][0]=="from_date_month") {
			from_date_monthRepo = arguments[i][1];

			if(from_date_monthRepo=="now") {
				from_date_monthRepo = getDateFirst;
			}
		}

		if(arguments[i][0]=="to_date_month") {
			to_date_monthRepo = arguments[i][1];

			if(to_date_monthRepo=="now") {
				to_date_monthRepo = getDateLast;
			}
		}

		if(arguments[i][0]=="page") {
			pageRepo = arguments[i][1];
		}

		if(arguments[i][0]=="back") {
			backRepo = arguments[i][1];
		}
	}
}

</script>