<div class="modal fade window" id="add" role="dialog" aria-labelledby="addTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-plus"></i> {{ trans('messages.add_insurance') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add" class="">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="detail-profile-form">
                            <div class="column" id="logo">
                                <img src='{{ $storage."/images/patient/photo.png" }}'>
                            </div>
                            <ul>
                                <li>
                                    <label for="image"><i class="fa fa-picture-o"></i> <span>{{ trans('messages.select_logo') }}</span></label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group" id="name-group">
                        <input type="text" class="form-control" placeholder="{{ trans('messages.insurance_name') }}" name="name" autocomplete="off">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
                        {{ trans('messages.save') }}
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<form id="form-add-logo">
    <input accept="image/*" type="file" name="image[]" id="image" class="new-logo" style="visibility: hidden" />
</form>

<script>

$('#form-add-logo .new-logo').change(function (event) {
    if(URL.createObjectURL(event.target.files[0])!=null) {
        $("#form-add #logo").html("<img src=''>");
        $("#form-add #logo img").attr("src",URL.createObjectURL(event.target.files[0]));
    }
});

function resetAdd() {
    resetValidation('form-add #name');
    $("#form-add input[name=name]").val("");

    $("#form-add #logo").html(thumbnail('md','insurance','{{ $storage }}','',''));
}

$('#add').on('show.bs.modal', function (e) {
    resetAdd();
});

$('#form-add input[name=name]').focus(function() {
    resetValidation('form-add #name');
});

$("#form-add").submit(function(event) {
    event.preventDefault();
    resetValidation('form-add #name');

    $("#form-add button").attr("disabled", true);

    formData= new FormData();

    formData.append("validate", false);
    formData.append("name", $("#form-add input[name=name]").val());
    formData.append("level", "admin");
    formData.append("user", "{{ $id_user }}");

    if($("#image").val()!="") {
        var logo = document.getElementById("image");
        file = logo.files[0];
        formData.append("logo", file);
    }

    $("#form-add .btn-primary").addClass("loading");
    $("#form-add .btn-primary span").removeClass("hide");

    $.ajax({
        url: "{{ $api_url }}/{{ $lang }}/insurance",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(data){
            

            $("#form-add button").attr("disabled", false);
    
            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            if(!data.error) {
                if(!data.success) {
                    formValidate(true, ['form-add #name',data.errors.name, true]);
                } else {

                    resetAdd();

                    $("#add").modal("toggle");

                    search(numPage, "false", keySearch);
                    
                    notif(true,"{{ trans('validation.success_add_insurance') }}");
            
                }
            } else {
                notif(false,"{{ trans('validation.failed') }}");
            }
        },
        error: function(){
            $("#form-add button").attr("disabled", false);

            $("#form-add .btn-primary").removeClass("loading");
            $("#form-add .btn-primary span").addClass("hide");

            notif(false,"{{ trans('validation.failed') }}");
        }
    })
});

</script>