<?php

return [
    "email_link" => "Jika kamu punya masalah dengan tombol :link, silahkan klik link di bawah ini.",
    "mail_invoice_receipt" => "Klik link dibawah ini untuk mengunduh bukti pembayaran di :clinic.",
];
