<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'Kolom :attribute harus diisi.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'name' => [
            'required' => 'Kolom nama harus diisi',
        ],
        'username' => [
            'required' => 'Kolom username harus diisi',
            'unique' => 'Username sudah digunakan',
        ],
        'email' => [
            'required' => 'Kolom email harus diisi',
            'email' => 'Alamat email tidak benar',
            'unique' => 'Alamat email sudah digunakan',
        ],
        'password' => [
            'required' => 'Kolom kata sandi harus diisi',
            'min' => 'Kata sandi minimal 6 karakter',
            'confirmed' => 'Kata sandi dan konfirmasi kata sandi tidak sama',
        ],
        'password_confirmation' => [
            'required' => 'Kolom konfirmasi kata sandi harus diisi',
            'min' => 'Konfirmasi kata sandi minimal 6 karakter',
        ],
        'old_password' => [
            'required' => 'Kolom kata sandi lama harus diisi',
            'min' => 'Kata sandi lama minimal 6 karakter',
        ],
        'address' => [
            'required' => 'Kolom alamat harus diisi',
        ],
        'country' => [
            'required' => 'Kolom negara harus diisi',
        ],
        'province' => [
            'required' => 'Kolom provinsi harus diisi',
        ],
        'city' => [
            'required' => 'Kolom kota harus diisi',
        ],
        'postal_code' => [
            'required' => 'Kolom kode pos harus diisi',
        ],
        'photo' => [
            'image' => 'Foto harus berupa gambar',
            'mimes' => 'Foto harus jpeg, jpg, bmp atau bnp',
            'max' => 'Foto maksimal 500kb'
        ],
        'logo' => [
            'image' => 'Logo harus berupa gambar',
            'mimes' => 'Logo harus jpeg, jpg, bmp atau bnp',
            'max' => 'Logo maksimal 500kb'
        ],
        'banner' => [
            'image' => 'Banner harus berupa gambar',
            'mimes' => 'Banner harus jpeg, jpg, bmp atau bnp',
            'max' => 'Banner maksimal 500kb'
        ],
        'phone' => [
            'required' => 'Kolom telepon harus diisi',
            'number' => 'Telepon harus berupa angka',
            'digits_between' => 'Angka telepon harus antara 8 - 15 angka',
            'unique' => 'Nomor telepon sudah digunakan',
        ],
        'commission' => [
            'required' => 'Kolom komisi harus diisi',
        ],
        'id_polyclinic' => [
            'required' => 'Pilih poliklinik',
        ],
        'id_service_category' => [
            'required' => 'Pilih kategori layanan',
        ],
        'code' => [
            'required' => 'Kolom kode harus diisi',
            'max' => 'Kode maksimal 10 karakter',
            'unique' => 'Kode sudah digunakan',
        ],
        'cost' => [
            'required' => 'Kolom tarif harus diisi',
            'number' => 'Tarif harus berupa angka',
            'digits_between' => 'Angka tarif harus antara 3 - 10 angka',
        ],
        'id_patient' => [
            'required' => 'Pilih pasien',
        ],
        'id_doctor' => [
            'required' => 'Pilih dokter',
        ],
        'id_doctor_daily_schedule' => [
            'required' => 'Pilih jadwal',
        ],
        'number' => [
            'required' => 'Kolom nomor harus diisi',
        ],
        'schedule_type' => [
            'required' => 'Pilih jenis rawat',
        ],
        'birth_date' => [
            'required' => 'Kolom tanggal lahir harus diisi',
        ],
        'gender' => [
            'required' => 'Pilih jenis kelamin',
        ],
        'id_type' => [
            'required' => 'Pilih jenis ID',
        ],
        'identity' => [
            'required' => 'Kolom nomor identitas harus diisi',
        ],
        'alamat' => [
            'required' => 'Kolom alamat harus diisi',
        ],
        'id_patient_category' => [
            'required' => 'Pilih kategori pasien',
        ],
        'financing' => [
            'required' => 'Pilih pembiayaan',
        ],
        'code_forgot_password' => [
            'required' => 'Kode salah',
        ],
        'id_clinic' => [
            'required' => 'Pilih klinik',
        ],
        'polyclinic' => [
            'required' => 'Pilih poliklinik',
        ],
        'start_date_active' => [
            'required' => 'Masukkan tanggal mulai aktif',
        ],
        'end_date_active' => [
            'required' => 'Masukkan tanggal akhir aktif',
        ],
        'time_schedule_doctor' => [
            'required' => 'Masukkan jadwal waktu',
        ],
        'anamnese' => [
            'required' => 'Masukkan anamnese',
        ],
        'diagnosis' => [
            'required' => 'Masukkan diagnosis',
        ],
        'service' => [
            'required' => 'Pilih layanan',
        ],
        'responsible_name' => [
            'required' => 'Masukkan nama penanggung jawab',
        ],
        'smoke' => [
            'required' => 'Pilih merokok atau tidak merokok',
        ],
        'date' => [
            'required' => 'Pilih tanggal',
        ],
        'category' => [
            'required' => 'Kolom kategori harus diisi',
        ],
        'subcategory' => [
            'required' => 'Kolom subkategori harus diisi',
        ],
        'indonesian_name' => [
            'required' => 'Kolom nama indonesia harus diisi',
        ],
        'english_name' => [
            'required' => 'Kolom nama inggris harus diisi',
        ],
        'start_date' => [
            'required' => 'Kolom tanggal mulai harus diisi',
        ],
        'end_date' => [
            'required' => 'Kolom tanggal akhir harus diisi',
        ],
        'total' => [
            'required' => 'Kolom total harus diisi',
        ],
        'version' => [
            'required' => 'Kolom versi harus diisi',
        ],
        'bpjs_number' => [
            'required' => 'Kolom nomor harus diisi',
            'unique' => 'Nomor BPJS sudah digunakan oleh pasien lain',
        ],
        'company' => [
            'required' => 'Kolom distributor harus diisi',
        ],
        'illness' => [
            'required' => 'Masukkan riwayat penyakit',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    'license' => 'Centang persetujuan lisensi untuk melakukan pendaftaran.',
    'failed' => 'Gagal',
    'failed_login' => 'Kombinasi email dan password salah',
    'failed_login_email_phone' => 'Email/Nomor telepon dan password salah.',
    'failed_admin_login' => 'Username atau password admin salah.',
    'failed_search_patient' => 'Data pasien tidak ditemukan',
    'failed_size_import' => 'Maksimum ukuran file 10 MB',
    'failed_input_field' => 'Data yang anda masukkan tidak benar',

    'success_add_vaccination_history' => 'Riwayat vaksinasi berhasil ditambahkan.',
    'empty_vaccination_history' => 'Masukan riwayat vaksinasi',
    'empty_vaccination_given_date' => 'Masukan tanggal pemberian vaksin',
    'empty_vaccination_dosage_sequence' => 'Masukan tahap pemberian vaksin',

    'empty_illness' => 'Masukkan riwayat penyakit',
    'empty_allergy_category' => 'Masukkan kategori riwayat alergi',
    'empty_allergy' => 'Masukkan riwayat alergi',
    'empty_anamnese' => 'Masukkan anamnese',
    'empty_complaint' => 'Masukkan keluhan',
    'empty_name' => 'Kolom nama harus diisi',
    'empty_email' => 'Kolom email harus diisi',
    'empty_username' => 'Kolom username harus diisi',
    'empty_phone' => 'Kolom telepon harus diisi',
    'empty_password' => 'Kolom kata sandi harus diisi',
    'empty_password_confirmation' => 'Kolom konfirmasi kata sandi harus diisi',
    'empty_old_password' => 'Masukkan kata sandi lama',
    'empty_start_date' => 'Masukkan tanggal awal',
    'empty_end_date' => 'Masukkan tanggal akhir',
    'empty_start_time' => 'Masukkan waktu awal',
    'empty_end_time' => 'Masukkan waktu akhir',
    'empty_quota' => 'Masukkan maksimal pasien daftar',
    'empty_max_walk_in' => 'Masukkan maksimal pasien walk-in',
    'empty_clinic' => 'Pilih klinik',
    'empty_polyclinic' => 'Pilih poliklinik',
    'empty_doctor' => 'Pilih dokter',
    'empty_patient' => 'Pilih pasien',
    'empty_service' => 'Pilih layanan',
    'empty_implementer' => 'Pilih pelaksana',
    'empty_qty' => 'Masukkan jumlah',
    'empty_payment_type' => 'Pilih jenis pembayaran',
    'empty_bank' => 'Pilih bank',
    'empty_payment' => 'Masukkan total pembayaran',
    'empty_time_schedule' => 'Masukkan jadwal waktu',
    'empty_icd' => 'Masukkan ICD',
    'empty_other_cost' => 'Masukkan biaya admin',
    'empty_medicine' => 'Pilih obat',
    'empty_dose' => 'Masukkan dosis',
    'empty_quantity' => 'Masukkan jumlah',
    'empty_time' => 'Masukkan waktu',
    'empty_bpjs_number' => 'Masukkan nomor BPJS',
    'empty_nik' => 'Masukkan NIK',
    'empty_appointment' => 'Masukkan waktu janji',
    'empty_schedule' => 'Pilih rawat jalan',
    'empty_admin_cost' => 'Masukkan biaya admin',
    'empty_medical_records' => 'Pilih rekam medis',
    'empty_vital' => 'Tanda tanda vital harus diisi',
    'empty_consciousness' => "Pilih status kesadaran",
    'empty_diagnosis' => "Masukkan diagnosis",
    'empty_status' => "Pilih status pulang",
    'empty_printLetter' => "Pilih hasil cetak",
    'empty_ppk' => "Masukkan ppk",
    'empty_category' => "Pilih kategori inventori",
    'empty_name_inventory' => "Masukkan nama inventori",
    'empty_code_inventory' => "Masukkan kode inventori",
    'empty_unit' => "Masukkan satuan",
    'empty_unit_amount' => "Masukkan Jumlah satuan",
    'empty_qty_price' => 'Masukkan jumlah harga',
    'empty_company_inventory' => "Masukkan nama perusahaan",
    'empty_name_unit' => "Masukkan nama satuan",
    'empty_no_batch' => "Masukkan nomor batch",
    'empty_exp_date' => "Pilih tanggal kadaluarsa",
    'empty_type_package' => "Pilih jenis kemasan",

    'empty_unit' => "Satuan tidak ada",
    'empty_qty_dose' => "Masukkan jumlah dosis",
    'empty_time' => 'Pilih waktu',
    'empty_qty_duration' => "Masukkan jumlah durasi",
    'empty_qty_unit' => "Pilih jumlah satuan",
    'empty_instruction_for_use' => "Masukkan petunjuk pemakaian",
    'empty_name_concoction' => "Masukkan nama racikan",
    'empty_strength_of_package' => "Masukkan kekuatan perkemasan",
    'empty_qty_package' => "Masukkan jumlah kemasan",
    'empty_price_inventory' => "Harga inventori belum diatur",
    'empty_stock_inventory' => "Stok inventori habis",
    'empty_qty_change_unit' => "Masukkan jumlah satuan yang dipilih",
    'empty_select_change_unit' => "Pilih satuan",
    'empty_type_of_care' => "Tidak ada jenis perawatan",
    'empty_of_activity' => "Tidak ada kegiatan",
    'empty_service' => "Harga layanan belum diatur",
    'empty_of_tool' => "Sarana harus diisi",
    'empty_of_date' => "Tanggal harus diisi",
    'empty_of_subspecialist' => "Sub spesialis harus diisi",
    'empty_of_specialist' => "Spesialis harus diisi",
    
    'invalid_start_end_date' => 'Tanggal akhir harus lebih besar dari tanggal awal',
    'invalid_start_end_time' => 'Waktu akhir harus lebih besar dari waktu awal',
    'invalid_birth_date' => 'Tanggal lahir tidak benar',
    'invalid_paid' => 'Total yang dibayar tidak boleh melebihi total sisa pembayaran',
    'invalid_payment' => 'Pembayaran tidak valid, periksa total pembayaran Anda.',
    'invalid_price' => 'Harga tidak benar. Pastikan harga tidak kosong dan tidak ada kategori yang sama.',
    'invalid_default_price' => 'Harga regular harus ada.',
    'invalid_cost' => 'Tarif tidak benar. Pastikan tarif tidak kosong dan tidak ada kategori yang sama.',
    'invalid_default_cost' => 'Tarif regular harus ada.',
    'invalid_medicine' => 'Obat tidak benar',
    'invalid_time_appointment' => 'Waktu tidak benar, pastikan waktu berada di antara batas waktu jadwal.',
    'invalid_time_now_appointment' => 'Jam rawat jalan yang dimasukkan sudah terlewati.',
    'invalid_end_time' => 'Jadwal praktek sudah berakhir.',
    'invalid_estimate_time' => 'Waktu estimasi tidak cukup untuk melakukan pelayanan.',
    'invalid_bpjs_number' => 'Nomor BPJS salah atau tidak terdaftar',
    'invalid_nik' => 'NIK salah atau tidak terdaftar',
    'invalid_pcare_credential' => 'Kredensial Pcare belum terpasang',
    'invalid_number' => 'Nomor tidak valid',
    'invalid_select_medicine' => "Obat yang sama telah dipilih",
    'invalid_medicine_concoction' => "Lengkapi data obat racikan",
    
    'min_time_schedule' => 'Minimal masukkan 1 jadwal waktu',
    'min_polyclinic' => 'Pilih minimal 1 poliklinik',
    'min_service_category' => 'Pilih minimal 1 kategori layanan',
    'min_patient_category' => 'Pilih minimal 1 kategori pasien',
    'min_financing' => 'Pilih minimal 1 pembiayaan',
    'min_service' => 'Pilih minimal 1 layanan',
    'min_doctor' => 'Pilih minimal 1 dokter',
    'min_icd' => 'Pilih minimal 1 ICD',
    'min_invoice' => 'Tambah minimal 1 layanan',
    'min_medicine' => "Tambah minimal 1 obat",
    'min_send' => "Pilih minimal 1 media pengiriman",
    'min_receiver' => "Pilih minimal 1 penerima",
    'min_role' => "Pilih minimal 1 peran",
    'min_amount_unit' => "Jumlah minimal 1",

    'quota' => 'Jadwal sudah penuh',
    'max_file_size' => 'Ukuran berkas tidak boleh melebihi 1MB',

    'icd_already_exists' => "ICD sudah ada di daftar.",
    'medicine_already_exists' => "Obat sudah ada di daftar.",
    'service_already_exists' => "Layanan sudah ada di daftar.",
    'doctor_already_exists' => "Dokter sudah terdaftar di klinik.",
    'patient_already_exists' => "Pasien sudah terdaftar di klinik.",
    'appointment_already_exists' => "Dokter sudah ada janji dengan pasien lain, coba masukkan dengan waktu yang lain.",
    'notif_arrears' => "Anda memiliki tunggakan sebesar",

    'no_schedule' => 'Jadwal tidak ditemukan',
    'no_total_paid' => 'Tidak ada total yang harus dibayar',
    'no_unit' => "Satuan inventori tidak tersedia, Silahkan tambah satuan inventori pada menu tambah inventori",

    'wrong_old_password' => 'Kata sandi lama tidak benar',
    'email_not_registered' => 'Email tidak terdaftar',
    'error_code' => 'Kode salah',
    'error_email_code' => 'Tautan Anda mungkin kedaluwarsa, periksa lagi email terbaru.',
    'error_unit_name' => 'Nama satuan tidak boleh sama',
    'error_patient_category_name' => 'Kategori pasien tidak boleh sama',


    'success_register' => 'Akun anda berhasil terdaftar. Silahkan <a href=":link" onclick="loading($(this).attr(href))">masuk</a> untuk menambah klinik.',
    'success_login' => 'Berhasil masuk',
    'success_add_clinic' => 'Klinik berhasil dibuat.',
    'success_edit_clinic' => 'Klinik berhasil diubah.',
    'success_delete_clinic' => 'Klinik berhasil dihapus.',
    'success_add_polyclinic' => 'Poliklinik berhasil ditambahkan.',
    'success_edit_polyclinic' => 'Poliklinik berhasil diubah.',
    'success_delete_polyclinic' => 'Poliklinik berhasil dihapus.',
    'success_select_polyclinic' => 'Poliklinik berhasil diubah.',
    'success_add_service' => 'Layanan berhasil ditambahkan.',
    'success_edit_service' => 'Layanan berhasil diubah.',
    'success_delete_service' => 'Layanan berhasil dihapus.',
    'success_add_service_category' => 'Kategori layanan berhasil ditambahkan.',
    'success_edit_service_category' => 'Kategori layanan berhasil diubah.',
    'success_delete_service_category' => 'Kategori layanan berhasil dihapus.',
    //'success_add_doctor' => 'Dokter berhasil ditambahkan. Tambahkan <a href=":link" onclick="loading($(this).attr(href))">jadwal praktek</a> atau <a href=":link_skip" onclick="loading($(this).attr(href))">lewati</a>.',
    'success_add_doctor' => 'Dokter berhasil ditambahkan.',
    'success_edit_doctor' => 'Dokter berhasil diubah.',
    'success_delete_doctor' => 'Dokter berhasil dihapus.',
    //'success_save_schedule_doctor' => 'Jadwal dokter berhasil disimpan. Informasi jadwal dokter akan dikirm ke email dokter dan akan dilakukan pengingat jadwal yang akan dikirim ke email dokter 12 jam serbelum jadwal praktek dokter dimulai. <a href=":link" onclick="loading($(this).attr(href))">Lihat daftar dokter</a>.',
    'success_save_schedule_doctor' => 'Jadwal dokter berhasil disimpan.',
    'success_add_commission_doctor' => 'Komisi dokter berhasil ditambahkan.',
    'success_edit_commission_doctor' => 'Komisi dokter berhasil diubah.',
    'success_delete_commission_doctor' => 'Berhasil menghapus komisi dokter.',
    'success_add_medicine' => 'Inventori berhasil ditambahkan.',
    'success_edit_medicine' => 'Inventori berhasil diubah.',
    'success_delete_medicine' => 'Inventori berhasil dihapus.',
    'success_add_patient_category' => 'Kategori pasien berhasil ditambahkan.',
    'success_edit_patient_category' => 'Kategori pasien berhasil diubah.',
    'success_delete_patient_category' => 'Kategori pasien berhasil dihapus.',
    'success_add_patient' => 'Pasien berhasil ditambahkan.',
    'success_edit_patient' => 'Pasien berhasil diubah.',
    'success_delete_patient' => 'Pasien berhasil dihapus.',
    'success_add_schedule' => 'Rawat jalan berhasil ditambahkan.',
    'success_edit_schedule' => 'Rawat jalan berhasil diubah.',
    'success_delete_schedule' => 'Rawat jalan berhasil dihapus.',
    'success_add_icd' => 'ICD berhasil ditambahkan.',
    'success_edit_icd' => 'ICD berhasil diubah.',
    'success_delete_icd' => 'ICD berhasil dihapus.',
    'success_save_vital_sign' => 'Tanda - tanda vital berhasil disimpan.',
    'success_save_preliminary_examination' => 'Kajian awal berhasil disimpan',
    'success_save_medical_records' => 'Rekam medis berhasil disimpan.',
    'success_save_invoice' => 'Pembayaran berhasil disimpan.',
    'success_edit_password' => 'Kata sandi berhasil diubah.',
    'success_edit_profile' => 'Profil berhasil diubah.',
    'success_edit_photo_profile' => 'Foto profil berhasil diubah.',
    'success_edit_logo_clinic' => 'Logo klinik berhasil diubah.',
    'success_edit_banner_clinic' => 'Banner klinik berhasil diubah.',
    'success_edit_banner_profile' => 'Banner profile berhasil diubah.',
    'success_send_reset_password' => 'Instruksi atur ulang password sudah dikirim ke email Anda.',
    'success_reset_password' => 'Kata sandi berhasil diubah.',
    'success_resend_email' => 'Kode verifikasi telah dikirim ke email Anda.',
    'success_email_verification' => 'Email berhasil di verifikasi.',
    'success_setup_clinic' => 'Klinik berhasil diatur.',
    'success_edit_clinic_location' => 'Lokasi klinik berhasil diubah.',
    'success_active_patient' => 'Pasien berhasil diaktifkan.',
    'success_non_active_patient' => 'Pasien berhasil dinonaktifkan.',
    'success_edit_account' => 'Akun berhasil diubah.',
    'success_save_clinic_payment' => "Pembayaran klinik berhasil disimpan.",
    'success_delete_clinic_payment' => "Pembayaran klinik berhasil dihapus.",
    'success_send_invoice_receipt' => "Bukti pembayaran berhasil dikirim.",
    'success_delete_schedule' => "Jadwal berhasil dihapus.",
    'success_confirmation' => "Konfirmasi berhasil.",
    'success_cancel' => "Berhasil dibatalkan.",
    'success_send_broadcast' => "Broadcast berhasil dikirim.",
    'success_setting_application' => "Pengaturan aplikasi berhasil disimpan.",
    'success_connect_bpjs' => "BPJS berhasil dihubungkan",
    'success_disconnect_bpjs' => "BPJS telah diputuskan",
    'success_add_payment_type' => 'Jenis pembayaran berhasil ditambahkan.',
    'success_edit_payment_type' => 'Jenis pembayaran berhasil diubah.',
    'success_delete_payment_type' => 'Jenis pembayaran berhasil dihapus.',
    'success_verification' => "Verifikasi berhasil.",
    'success_change_password' => 'Kata sandi berhasil diubah.',
    'success_add_user' => 'Pengguna berhasil ditambahkan.',
    'success_edit_user_role' => 'Peran pengguna berhasil diubah.',
    'success_delete_user' => 'Pengguna berhasil dihapus.',
    'success_add_medicine_incoming' => 'Inventori masuk berhasil ditambahkan.',
    'success_edit_medicine_incoming' => 'Inventori masuk berhasil diubah.',
    'success_delete_medicine_incoming' => 'Inventori masuk berhasil dihapus.',
    'success_add_insurance' => 'Asuransi berhasil ditambahkan.',
    'success_edit_insurance' => 'Asuransi berhasil diubah.',
    'success_delete_insurance' => 'Asuransi berhasil dihapus.',
    'success_select_insurance' => 'Asuransi berhasil diubah.',
    'success_select_unit' => 'Satuan berhasil ditambahkan.',
    'success_update_schedule' => 'Pendaftaran berhasil diubah.'
];
