<?php

return [
    "email_link" => "If you have a problem with the button: link, please click the link below.",
    "mail_invoice_receipt" => "Click the link below to download the proof of payment at :clinic.",
];
