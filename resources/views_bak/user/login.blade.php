@include('layouts.lib')

<div id="progressBar">
	<div class="loader"></div>
</div>
<div class="account-user-sec">
	<div class="account-sec">
		@include('layouts.main-header')
		<div class="acount-sec">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="contact-sec-center">
							<div class="row">
								<div class="col-md-6">	
									<div class="widget-title">
										<h3>{{ trans('messages.title_login') }}</h3>
										<span>{{ trans('messages.text_login') }}</span>
									</div>
									<div class="account-form alert-notif">
										<form>
											<div class="">
												<div class="feild" id="email-group">
													<label>{{ trans('messages.email') }}</label>
													<input type="text" id="email" name="email" placeholder="{{ trans('messages.input_email') }}" />
													<span class="help-block"></span>
												</div>
												<div class="feild" id="password-group">
													<label>{{ trans('messages.password') }}</label>
													<input type="password" id="password" name="password" placeholder="{{ trans('messages.input_password') }}" />
													<span class="hidden-char ion-eye-disabled" onclick="hiddenChar('password')"></span>
													<span class="help-block"></span>
												</div>
												<div class="feild last-item">
													<button type="submit" class="btn-right button-login">
						                                <span class="fa fa-circle-o-notch fa-spin fa-3x fa-fw hide"></span>
						                                {{ trans('messages.login') }}
						                            </button>
												</div>
											</div>
										</form>
									</div>
									<div class="more-option dekstop">
										<span>{{ trans('messages.or') }}</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="creat-an-account">
										<div class="button-forgot-mobile">
											<h4>{{ trans('messages.forgot_password_question') }}</h4>
											<button class="button-transaparent" title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/forgot-password'">{{ trans('messages.reset_password') }}</button>
										</div>

										<div class="more-option mobile">
											<span>{{ trans('messages.or') }}</span>
										</div>
										
										<h3>{{ trans('messages.register_for_free') }}</h3>
										<span class="hide">*{{ trans('messages.info_register_for_free') }}</span>
										<button title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/register'" class="button-login">{{ trans('messages.register_now') }}</button>
										<div class="button-forgot-dekstop">
											<h4>{{ trans('messages.forgot_password_question') }}</h4>
											<button class="button-transaparent" title="" onclick="loading($(this).attr('href'));document.location.href='#/{{ $lang }}/forgot-password'">{{ trans('messages.reset_password') }}</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="wrapper-action">
						<div class="content-action">
							@if(!empty($id_user))
							<li class="action-login"><a href="logout">{{ trans('messages.logout') }}</a></li>
							@else
							<!--<li><a title="" href="{{ url('/') }}/{{ $lang }}/search" onclick="loading($(this).attr('href'))">{{ trans('messages_web.find_clinic') }}</a></li>-->
							<li id="link-login"	><a title="" href="#/{{ $lang }}/doctor/login" onclick="loading($(this).attr('href'))">{{ trans('messages.login_app_doctor') }}</a></li>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- @include('layouts.footer') -->
	</div>
</div>
 