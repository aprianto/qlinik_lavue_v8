<?php

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('home_notif', 'UserController2@home')->name('home');

Route::get('login_notif', 'UserController2@login');
Route::get('manual_login', 'UserController2@manual_login');
Route::post('login_notif', 'UserController2@login');
Route::get('logout_notif', 'UserController2@logout');

Route::get('session', 'BaseController@session');

Route::get('admin', 'AdminController@index');

Route::get('/', 'BaseController@home');
Route::get('blank', 'BaseController@blank');
Route::get('language/{language}', 'BaseController@language');
Route::get('redirect/{lang}/{link}','BaseController@redirect');
Route::get('404', 'BaseController@error');

Route::get('example', 'BaseController@example');
Route::get('sms', 'BaseController@sms');
Route::get('email', 'BaseController@email');

Route::get('{lang}/reminder', 'ReminderController@reminder');

Route::get('download/{path}/{name}', 'BaseController@download');

Route::get('logout', 'UserController@logout');
Route::get('doctor/logout', 'DoctorController@logout');
Route::get('admin/logout', 'AdminController@logout');

Route::group(array('prefix'=>'{lang}'),function(){
    Route::get('home', 'BaseController@dashboard');
	Route::get('catcher', 'BaseController@catcher_id_clinic');
	Route::get('header', 'BaseController@header');
	Route::get('login', 'UserController@login');
	Route::post('login', 'UserController@login');
	Route::get('register', 'UserController@register');
	Route::get('forgot-password', 'UserController@forgot_password');
	Route::get('reset-password/{code}', 'UserController@reset_password');
	Route::get('email/{code}', 'UserController@email_verification'); 
	Route::get('doctor/login', 'DoctorController@login');
	Route::get('doctor/forgot-password', 'DoctorController@forgot_password');
	Route::get('doctor/reset-password/{code}', 'DoctorController@reset_password');
	Route::post('doctor/login', 'DoctorController@login');
	Route::get('profile','UserController@profile');
    Route::get('profile/setting','UserController@setting');
    Route::get('clinic/setting','ClinicController@setting');
	Route::get('clinic/location','ClinicController@location');
	Route::get('clinic/polyclinic','ClinicController@polyclinic');
	Route::get('clinic/insurance','ClinicController@insurance');
	Route::get('doctor/schedule', 'DoctorController@schedule');
	Route::get('doctor/calendar', 'DoctorController@calendar');
    Route::get('schedule/calendar', 'ScheduleController@calendar');
    Route::get('medical/patient/{id}', 'MedicalRecordsController@patient');
	Route::get('payment', 'InvoiceController@payment');
	Route::get('report/income', 'ReportController@income');
	Route::get('report/commission', 'ReportController@commission');
	Route::get('report/service', 'ReportController@service');
    Route::get('report/medicine', 'ReportController@medicine');
    Route::get('report/illness', 'ReportController@illness');
    Route::get('suspended', 'ClinicController@suspended');
    Route::get('admin/login', 'AdminController@login');
    Route::post('admin/login', 'AdminController@login');
    Route::get('admin/setting','AdminController@setting');
    Route::get('setting','SettingController@setting');
    Route::get('payment/clinic','PaymentController@index');
    Route::get('payment/type','PaymentTypeController@index');
    Route::get('help/doctor','HelpController@doctor');
    Route::get('help/commission','HelpController@commission');
    Route::get('help/service','HelpController@service');
    Route::get('help/medicine','HelpController@medicine');
    Route::get('help/patient','HelpController@patient');
    Route::get('help/schedule','HelpController@schedule');
    Route::get('help/medical','HelpController@medical');
	Route::get('help/invoice','HelpController@invoice');
	Route::get('patient/create/{id}','PatientController@showBpjs');
	Route::get('schedule/sick','ScheduleController@sick');
    Route::get('schedule/healthy','ScheduleController@healthy');
    Route::get('medicine/unit', 'MedicineController@unit_create');
    Route::get('medicine/detail/{id}', 'MedicineController@detail_inventory'); 
});

Route::group(array('prefix'=>'{lang}/{type}'),function(){
    Route::get('clinic/{id_clinic}/patient/{id}', 'DocumentController@patient');
    Route::get('schedule/{id}', 'DocumentController@schedule');
    Route::get('medical/records/{id}', 'DocumentController@medical_records');
    Route::get('medical/records/bpjs/{id}/{download}', 'DocumentController@rujuk_lanjut');
    Route::get('medical/records/{id}/medicine', 'DocumentController@medical_records_medicine');
    Route::get('medical/records/{id}/medicine/{id_medicine}', 'DocumentController@medical_records_medicine_recipe');
    Route::get('invoice/{number}', 'DocumentController@invoice');
    Route::get('income/clinic/{number}', 'DocumentController@clinic_income');
    Route::get('income/clinic/{number}/detail/{no}', 'DocumentController@clinic_income_detail');
    Route::get('commission/clinic/{id}', 'DocumentController@clinic_commission');
    Route::get('commission/clinic/{id}/detail/{id_doctor}', 'DocumentController@clinic_commission_detail');
    Route::get('service/clinic/{id}', 'DocumentController@clinic_service');
    Route::get('service/clinic/{id}/detail/{id_service}', 'DocumentController@clinic_service_detail');
    Route::get('medicine/clinic/{id}', 'DocumentController@clinic_medicine');
    Route::get('medicine/clinic/detail/{id_medicine}', 'DocumentController@clinic_medicine_detail');
    Route::get('invoice/doctor/{id}', 'DocumentController@doctor_invoice');
    Route::get('clinic/{id_clinic}/export/{key}', 'DocumentController@exportNoDate'); // To export data to spreadsheet
    Route::get('clinic/{id_clinic}/patient-import', 'DocumentController@importPatient'); // To import data to spreadsheet
});

Route::group(['prefix' => '{lang}', 'midlleware' => ['web']], function() {
    Route::resource('user','UserController');
    Route::resource('clinic','ClinicController');	
    Route::resource('polyclinic','PolyclinicController');
    Route::resource('service/category/default','DefaultServiceCategoryController');
    Route::resource('service/category','ServiceCategoryController');
    Route::resource('service/default','DefaultServiceController');
    Route::resource('service','ServiceController');
    Route::resource('doctor','DoctorController');
    Route::resource('schedule','ScheduleController');
    Route::resource('group/activities','GroupActivitiesController');
    Route::resource('commission','CommissionController');	
    Route::resource('medicine/default','DefaultMedicineController');
    Route::resource('medicine','MedicineController');
    Route::resource('medicine-incoming','MedicineIncomingController');
    Route::resource('medicine-outgoing','MedicineOutgoingController');
    Route::resource('patient/category/default','DefaultPatientCategoryController');
    Route::resource('patient/category','PatientCategoryController');
    Route::resource('patient','PatientController');
    Route::resource('icd','ICDController');
    Route::resource('schedule','ScheduleController');
    Route::resource('vital_sign','VitalSignController');
    Route::resource('medical','MedicalRecordsController');
    Route::resource('invoice','InvoiceController');
    Route::resource('notifications','NotificationsController');
    Route::resource('broadcast','BroadcastController');
    Route::resource('insurance','InsuranceController');
});

Route::get('push', 'BaseController@push');
Route::get('get_push', 'BaseController@get_push');
Route::get('image/{type}/{height}/{width}/{img?}', 'ImageController@image');
Route::get('patient/bpjs/{type}/{number}', 'PatientController@bpjs');
Route::get('language/web/{language}', 'WebController@language');

Route::group(array('prefix'=>'{lang}'),function(){
    Route::get('search', 'WebController@search');
    Route::get('clinic/{id}/{name}', 'WebController@clinic');
    Route::get('doctor/{id}/{name}', 'WebController@doctor');
    Route::get('schedule/clinic/{id_clinic}/doctor/{id_doctor}/polyclinic/{id_poly}', 'WebController@schedule');
    Route::get('schedule/report/{id}', 'WebController@report_schedule');
});



/*
Aprianto
*/  
Route::post('{lang}/setupsession', 'UserController@setupsession');


 

